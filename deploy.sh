git reset --hard origin/master
git clean -f
git pull
git checkout master
cd client ; npm run build ; cd ../server ; npm run build ; cd ../
pm2 kill
pm2 startOrRestart ecosystem.json --env production