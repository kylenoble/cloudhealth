
import currentDomain from './domain.js';

export default class ConsultsService {
  constructor(domain) {
    this.domain = domain || `${currentDomain}/api/consults/`;
    this.call = this.call.bind(this)

  }

get(param) {
    return this.call(`${this.domain}`, {
      method: 'POST',
      body: JSON.stringify(param)
    }).then(res => {
      return Promise.resolve(res)
    }).catch(e => {
      return Promise.reject(e)
    })
}

getAnalyticsData(param) {
  return this.call(`${this.domain}analytics/`, {
    method: 'POST',
    body: JSON.stringify(param)
  }).then(res => {
    return Promise.resolve(res)
  }).catch(e => {
    return Promise.reject(e)
  })
}

getConsult(obj) {
    return this.call(`${this.domain}last/`, {
      method: 'POST',
      body: JSON.stringify(obj)
    }).then(res => {
      return Promise.resolve(res)
    }).catch(e => {
      return Promise.reject(e)
    })
}

save(data){
  return this.call(`${this.domain}save/`, {
  method: 'POST',
  body: JSON.stringify(data)
}).then(res => {
  if (!res || res.message || res.status >= 400) {
    return Promise.reject({error: res.message});
  }

  return Promise.resolve(true)
}).catch(e => {
  console.log(e);
  return Promise.reject(e)
})
}

update(data, id){
  let dta = {}
  dta.id = id
  dta.details = data
  return this.call(`${this.domain}update/`, {
  method: 'POST',
  body: JSON.stringify(dta)
}).then(res => {
  if (!res || res.message || res.status >= 400) {
    return Promise.reject({error: res.message});
  }

  return Promise.resolve(true)
}).catch(e => {
  console.log(e);
  return Promise.reject(e)
})
}



call(url, options){
    // performs api calls sending the required authentication headers
  const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }

  if (this.loggedIn()){
    headers['Authorization'] = 'Bearer ' + this.getToken()
  }

  return fetch(url, {
    headers,
    ...options
  })
  .then(this._checkStatus)
  .then(response => response.json())
}

 _checkStatus(response) {
  // raises an error in case response status is not a success
  if (response.status >= 200 && response.status < 300) {
    return response
  } else {
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
}

loggedIn(){
  // Checks if there is a saved token and it's still valid
  const token = this.getToken()
  return !!token
  //&& !isTokenExpired(token) // handwaiving here
}

getToken(){
  // Retrieves the user token from localStorage
  return localStorage.getItem('id_token')
}
}
