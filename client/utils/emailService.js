import 'isomorphic-fetch'
import currentDomain from './domain.js'

export default class EmailService {
    constructor(domain) {
        this.domain = domain || `${currentDomain}/api/email`
        this.call = this.call.bind(this)
    }

    get(eml) {
        const email = eml
        return this.call(`${this.domain}/${email}`, {
            method: 'GET'
        }).then(res => {
            return Promise.resolve(res)
        }).catch(e => {
            return Promise.reject(e)
        })
    }

    reset(id, ...fields) {
        return this.call(`${this.domain}/${id}`, {
            method: 'PUT',
            body: JSON.stringify(...fields)
        }).then(res => {
            return Promise.resolve(res.user)
        }).catch(e => {
            return Promise.reject(e)
        })
    }

    sendEmail(email, password, name) {
        return this.call(`${this.domain}/sendmail`, {
            method: 'POST',
            body: JSON.stringify({
                email,
                password,
                name
            })
        }).then(res => {
            console.log("Email Servicex");
            console.log(res);
        }).catch(error => {
            console.log("Email Service errorXS");
            console.log(error);
            return Promise.reject(error);
        })
    }

    call(url, options) {
        // performs api calls sending the required authentication headers
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }


        return fetch(url, {
            headers,
            ...options
        })
            .then(this._checkStatus)
            .then(response => response.json())
    }

    sendEmailToHR(email) {
        return this.call(`${this.domain}/sendEmailToHR`, {
            method: 'POST',
            body: JSON.stringify(email)
        }).then(res => {
            console.log("Email Servicex");
            console.log(res);
        }).catch(error => {
            console.log("Email Service error");
            console.log(error);
            return Promise.reject(error);
        })
    }
}
