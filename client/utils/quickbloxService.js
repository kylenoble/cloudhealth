import _ from 'underscore';

export default class QuickbloxService {
    constructor(QB) {
        this.QB = QB;
        this.currentSession = {};
        this.caller = {};
        this.mainVideo = '';
        this.calleesAnwered = [];
        this.remoteStreamCounter = 0;
        this.users = [];
        this.callees = {};
        this.call = {
            callTime: 0,
            callTimer: null,
        };
        this.network = {}
    }

    login(params) {

        var userRequiredParams = {
            'login': params.email,
            'password': "webAppPass"
        };

        let that = this;

        return new Promise(function(resolve, reject) {
            that.QB.createSession(function(csErr, csRes){
                if(csErr) {
                    reject(csErr);
                } else {
                    /** In first trying to login */
                    that.QB.login(userRequiredParams, function(loginErr, loginUser){
                        if(loginErr) {
                            /** Login failed, trying to create account */
                            that.QB.users.create({
                                'login': params.email,
                                'email': params.email,
                                'password': "webAppPass",
                                'full_name': params.name,
                                'tag_list': params.room
                            }, function(createErr, createUser){
                                if(createErr) {
                                    console.log('[create user] Error:', createErr);
                                    reject(createErr);
                                } else {
                                    that.QB.login(userRequiredParams, function(reloginErr, reloginUser) {
                                        if(reloginErr) {
                                            console.log('[relogin user] Error:', reloginErr);
                                        } else {
                                            resolve(reloginUser);
                                        }
                                    });
                                }
                            });
                        } else {
                            /** Update info */
                            if(loginUser.user_tags !== params.room || loginUser.full_name !== params.name ) {
                                that.QB.users.update(loginUser.id, {
                                    'full_name': params.name,
                                    'tag_list': params.room
                                }, function(updateError, updateUser) {
                                    if(updateError) {
                                        console.log('APP [update user] Error:', updateError);
                                        reject(updateError);
                                    } else {
                                        resolve(updateUser);
                                    }
                                });
                            } else {
                                resolve(loginUser);
                            }
                        }
                    });
                }
            });
        });
    }

    connect(params) {
        let that = this;

        return new Promise(function(resolve, reject) {
            if(localStorage.getItem('isAuth')) {
                reject('already authed');
            }

            that.login(params).then(function (user) {
                that.caller = user;

                that.QB.chat.connect({
                    jid: that.QB.chat.helpers.getUserJid( that.caller.id, "51058" ),
                    password: "webAppPass"
                }, function(err, res) {
                    if(err) {
                        if(!_.isEmpty(that.currentSession)) {
                            that.currentSession.stop({});
                            that.currentSession = {};
                        }

                        if(that.call.callTimer) {
                            clearInterval(call.callTimer);
                            call.callTimer = null;
                            call.callTime = 0;
                            that.helpers.network = {};
                        }
                        reject(err);
                    } else {
                        localStorage.setItem('isAuth', true);
                        resolve(that.caller);
                    }
                });
            }).catch(function(error) {
                console.error(error);
                reject(error);
            });
        });
    }

    logout(params) {
        let that = this;
        return new Promise(function(resolve, reject) {
            that.QB.users.delete(that.caller.id, function(err, user){
                if (user) {
                    that.caller = {};
                    that.users = [];

                    that.QB.chat.disconnect();
                    localStorage.removeItem('isAuth');
                    resolve('success');
                } else {
                    console.error('Logout failed:', err);
                    reject(err);
                }
            }); 
        });  
    }

    getUsers() {
        let that = this;

        return new Promise(function(resolve, reject) {
            let returnedUsers = []
            that.QB.users.get({'tags': [that.caller.user_tags], 'per_page': 100}, function(err, result){
                if (err) {
                    reject(err);
                } else {
                    _.each(result.items, function(item) {
                        if( item.user.id !== that.caller.id ) {
                            returnedUsers.push(item.user);
                        }
                    });

                    if(result.items.length < 2) {
                        reject({
                            'title': 'not found',
                            'message': 'Not found users by tag'
                        });
                    } else {
                        resolve(returnedUsers);
                    }
                }
            });
        });
    }
}
