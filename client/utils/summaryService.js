import AuthService from "../utils/authService.js";
import "isomorphic-fetch";
import queryString from "query-string";
import currentDomain from "./domain.js";

const auth = new AuthService();

export default class SummaryService {
  constructor(domain) {
    this.domain = domain || `${currentDomain}/api/summary`;
    this.call = this.call.bind(this);
    this.create = this.create.bind(this);
    this.getProfile = this.getProfile.bind(this);
    this.returnSummary = this.returnSummary.bind(this);
    this.filterSummary = this.filterSummary.bind(this);
    this.getToken = this.getToken.bind(this);
    this.get = this.get.bind(this);
  }

  create(summaries) {
    return this.call(`${this.domain}/`, {
      method: "POST",
      body: JSON.stringify(this.returnSummary(summaries))
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        return Promise.resolve(true);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  returnSummary(summaries) {
    const profile = this.getProfile();
    const userId = profile.id;
    let newSummary = [];

    for (const summary of summaries) {
      newSummary.push(this.filterSummary(summary, userId));
    }

    return newSummary;
  }

  filterSummary(summary, userId) {
    return {
      label: summary.label,
      group: summary.group,
      userId: userId,
      value: summary.value,
      name: summary.name
    };
  }

  update(summary) {
    return this.call(`${this.domain}/update`, {
      method: "PUT",
      body: JSON.stringify(this.returnSummary(summary))
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        return Promise.resolve(true);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  get(fields, id = null) {
    const profile = this.getProfile();

    if (fields) {
      if (!fields.userId) {
        fields.userId = profile.id;
        fields.userType = profile.userType;
      }
    } else {
      fields = { userId: profile.id };
    }
    if (id) {
      fields = { userId: id };
    }
    let url = this.domain + "?" + queryString.stringify(fields);
    return this.call(url, {
      method: "GET"
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        console.log("getting summary done");
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  getGraphData(data) {
    let url = this.domain + "/graphdata?" + queryString.stringify(data);
    return this.call(url, {
      method: "GET"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  // GET ALL summary
  getAll() {
    let url = `${this.domain}/all`;
    return this.call(url, {
      method: "GET"
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        console.log("getting all summary done");
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  // GET ALL summary
  getUserRank(str = null) {
    const profile = this.getProfile();
    let fields = {};
    if (str === "All") {
      fields = { company: str, user_id: profile.id };
    } else {
      fields = { company: profile.company, user_id: profile.id };
    }
    let url = this.domain + "/filtered?" + queryString.stringify(fields);

    return this.call(url, {
      method: "GET"
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        console.log("getting filtered summary done");
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  saveHealthSurveySummary(data) {
    return this.call(`${this.domain}/healthsurvey`, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        return Promise.resolve(true);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  getHealthSurveySummary(id) {
    return this.call(`${this.domain}/healthsurvey/${id}`)
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  call(url, options) {
    // performs api calls sending the required authentication headers
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };

    if (this.loggedIn()) {
      headers["Authorization"] = "Bearer " + this.getToken();
    }

    return fetch(url, {
      headers,
      ...options
    })
      .then(this._checkStatus)
      .then(response => response.json());
  }

  _checkStatus(response) {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      return response;
    } else {
      var error = new Error(response.statusText);
      error.response = response;
      throw error;
    }
  }

  loggedIn() {
    // Checks if there is a saved token and it's still valid
    const token = this.getToken();
    return !!token;
    //&& !isTokenExpired(token) // handwaiving here
  }

  getProfile() {
    // Retrieves the profile data from localStorage
    const profile = localStorage.getItem("profile");
    return profile ? JSON.parse(profile) : {};
  }

  getToken() {
    // Retrieves the user token from localStorage
    return localStorage.getItem("id_token");
  }

  getPercentile() {
    let url = `${this.domain}/all`;
    return this.call(url, {
      method: "GET"
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }
        let result = this._getPercentile(res);
        console.log("getting percentile done");
        return Promise.resolve(result);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  _getPercentile(allsummary) {
    let users = [];
    let score = 0;
    let smrytot = allsummary
      .reduce((users, line, index) => {
        if (!users.hasOwnProperty(line.user_id)) score = 0;
        users[line.user_id] = users[line.user_id] || [];
        if (
          line.name == "weightScore" ||
          line.name == "nutrition" ||
          line.name == "sleep" ||
          line.name == "detox" ||
          line.name == "stress" ||
          line.name == "exercise"
        ) {
          //  users[line.user_id]['score'] = (score += parseFloat(line.value)* 0.167)*2
          users[line.user_id] = {
            score: (score += parseFloat(line.value) * 0.167 * 2)
          };
        }

        return users;
      }, {})
      .reduce((users, line, index) => {
        console.log(line.value);
      }, {});
    console.log("HELLO");
    console.log(smrytot);
    return smrytot;
  }
}
