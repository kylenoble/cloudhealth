import AuthService from "../utils/authService.js";
import "isomorphic-fetch";
import currentDomain from "./domain.js";

const auth = new AuthService();

export default class UserService {
  constructor(domain) {
    this.domain = domain || `${currentDomain}/api/users`;
    this.call = this.call.bind(this);
    this.create = this.create.bind(this);
    this.getAdmin = this.getAdmin.bind(this);
  }

  async getAdmin(id) {
    return await this.call(`${this.domain}/getAdmin/${id}`, {
      method: "GET"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }
  async getAdminByID(id) {
    return await this.call(`${this.domain}/getAdminByID/${id}`, {
      method: "GET"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  create(email, password, company, name, employeeId, department, branch) {
    // Get a token
    return this.call(`${this.domain}/`, {
      method: "POST",
      body: JSON.stringify({
        email,
        password,
        company,
        name,
        employeeId,
        department,
        branch
      })
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }
        auth
          .login(email, password)
          .then(res => {
            return;
          })
          .catch(e => {
            return e;
          });
      })
      .then(res => {
        return Promise.resolve(true);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  update(...fields) {
    let userID;
    let other_user = false;
    const profile = this.getProfile();
    if (fields[0][0].healthsurvey_status === null) {
      userID = profile.id;
      other_user = true;
    } else if (fields[0][0].status) {
      userID = profile.id;
    } else if (fields[0][0].userId) {
      userID = fields[0][0].userId;
      delete fields[0][0].userId; //unsets userid from fields
    } else {
      //userID = fields[0][0].userId;
      userID = profile.id;
      delete fields[0][0].userId; //unsets userid from fields
    }

    return this.call(`${this.domain}/${userID}`, {
      method: "PUT",
      body: JSON.stringify(...fields)
    })
      .then(res => {
        if (other_user === true) {
          this.setProfile(res.user);
          return Promise.resolve(res.user);
        } else {
          return Promise.resolve(res);
        }
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  changePassword(...fields) {
    return this.call(`${this.domain}/changePassword`, {
      method: "PUT",
      body: JSON.stringify(...fields)
    })
      .then(res => {
        if (res.status === 401) {
          return Promise.resolve(res);
        } else {
          this.setProfile(res);
          return Promise.resolve(res);
        }
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  updateAdminPassword(...fields) {
    return this.call(`${this.domain}/updateAdminPassword`, {
      method: "PUT",
      body: JSON.stringify(...fields)
    })
      .then(res => {
        if (res.status === 401) {
          return Promise.resolve(res);
        } else {
          this.setProfile(res);
          return Promise.resolve(res);
        }
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  getData(dta) {
    return this.call(`${this.domain}/data/`, {
      method: "POST",
      body: JSON.stringify(dta)
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  passwordMonitor(id) {
    return this.call(`${this.domain}/monitor/`, {
      method: "POST",
      body: JSON.stringify({ id: id })
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  saveToMonitoring(object){
    return this.call(`${this.domain}/saveToMonitoring/`, {
      method: "POST",
      body: JSON.stringify(object)
    }).then(res=>{
      return Promise.resolve(res)
    }).catch(e=>{
      console.log('ERROR SAVING', e)
      return Promise.reject(e)
    })
  }

  getPatientInfo(string) {
    return this.call(`${this.domain}/info/${string}`, {
      method: "GET"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  getLatest() {
    const profile = this.getProfile();
    return this.call(`${this.domain}/${profile.id}`, {
      method: "GET"
    })
      .then(res => {
        this.setProfile(res);
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  get(usrID = null) {
    let userID = 0;
    if (usrID === null) {
      const profile = this.getProfile();
      userID = profile.id;
    } else {
      userID = usrID;
    }

    return this.call(`${this.domain}/${userID}`, {
      method: "GET"
    })
      .then(res => {
        if (usrID === null) {
          this.setProfile(res.user);
          return Promise.resolve(res.user);
        } else {
          return Promise.resolve(res);
        }
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  getUserFilters(company_name) {
    return this.call(`${this.domain}/filter/${company_name}`, {
      method: "GET"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  getDoctors(company_id) {
    //const profile = this.getProfile();

    const url = `${this.domain}/doctors`;

    return this.call(url, {
      method: "PUT",
      body: JSON.stringify({ company_id })
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  saveActivity(data) {
    return this.call(`${this.domain}/activity`, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  getMDDataByToken(token) {
    return this.call(`${this.domain}/token/${token}`, {
      method: "GET"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  saveOtherInfo(data) {
    return this.call(`${this.domain}/saveOtherHealthInfo`, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  updateOtherInfo(data) {
    return this.call(`${this.domain}/updateOtherHealthInfo`, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  deleteOtherInfo(data) {
    return this.call(`${this.domain}/deleteOtherHealthInfo`, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  getOtherInfo(userId) {
    return this.call(`${this.domain}/health/${userId}`, {
      method: "GET"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  getHealthDocs(userId) {
    return this.call(`${this.domain}/healthdocs/${userId}`, {
      method: "GET"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  deleteFile(data) {
    return this.call(`${this.domain}/deleteFile`, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  call(url, options) {
    // performs api calls sending the required authentication headers
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };

    if (this.loggedIn()) {
      headers["Authorization"] = "Bearer " + this.getToken();
    }

    return fetch(url, {
      headers,
      ...options
    })
      .then(this._checkStatus)
      .then(response => response.json());
  }

  _checkStatus(response) {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      return response;
    } else {
      var error = new Error(response.statusText);
      error.response = response;
      throw error;
    }
  }

  getProfile() {
    // Retrieves the profile data from localStorage
    const profile = localStorage.getItem("profile");
    return profile ? JSON.parse(profile) : {};
  }

  setProfile(profile) {
    // Saves profile data to localStorage
    localStorage.setItem("profile", JSON.stringify(profile));
  }

  loggedIn() {
    // Checks if there is a saved token and it's still valid
    const token = this.getToken();
    return !!token;
    //&& !isTokenExpired(token) // handwaiving here
  }

  getToken() {
    // Retrieves the user token from localStorage
    return localStorage.getItem("id_token");
  }
}
