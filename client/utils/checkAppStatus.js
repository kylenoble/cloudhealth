import Router from "next/router";
import SystemService from "./systemService.js";
import UserService from "./userService.js";

const System = new SystemService();
const user = new UserService();

export default class CheckAppStatus {
  chek_status() {
    return this._getSystemInfo()
      .then(res => {
        if (res && res.app_status === "Maintenance") {
          user
            .getLatest()
            .then(res => {
              if (res) {
                if (
                  res.email === "kyle.nobl@gmail.com" ||
                  res.email === "jm@merakisv.com" ||
                  res.email === "lianiemiagdan@gmail.com" ||
                  res.email === "edmardausan15@gmail.com"
                ) {
                  return true;
                }
                Router.push("/down");
              } else {
                Router.push("/down");
              }
            })
            .catch(e => {
              console.log(e);
              Router.push("/down");
            });

          return;
        } else {
          return true;
        }
      })
      .catch(e => {
        if (e) console.log("Error: " + e);
      });
  }

  _getStatus() {
    return this._getSystemInfo()
      .then(res => {
        if (res && res.app_status === "Active") {
          return Promise.resolve(res.app_status);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _getSystemInfo() {
    let obj = { app_code: "cha01" };
    return System.get(obj).then(res => {
      return Promise.resolve(res);
    });
  }
}
