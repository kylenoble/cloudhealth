/* eslint-disable no-param-reassign */
/* eslint-disable no-undef */
import "isomorphic-fetch";
import queryString from "query-string";
import currentDomain from "./domain.js";

export default class AppointmentsService {
  constructor(domain) {
    this.domain = domain || `${currentDomain}/api/appointments`;
    this.call = this.call.bind(this);
    // this.create = this.create.bind(this);
  }

  create(inputs) {
    // inputs.name,
    //     inputs.email,
    //     inputs.date,
    //     inputs.password,
    //     inputs.doctorId,
    //     inputs.concern,
    //     inputs.date_requested,
    //     inputs.patient_company_id,
    //     inputs.active,
    //     inputs.NoPassword
    const name = inputs.name;
    const email = inputs.email;
    const date = inputs.date;
    const password = inputs.password;
    const doctorId = inputs.doctorId;
    const concern = inputs.concern;
    const date_requested = inputs.date_requested;
    const patient_company_id = inputs.patient_company_id;
    const active = inputs.active;
    const appointment_code = inputs.appointment_code;
    const parent_appointment_code = inputs.parent_appointment_code;
    const NoPassword = inputs.NoPassword;

    // Creating
    //  const profile = this.getProfile()
    //  const doctorId = profile.id
    const roomName = this.generateRoomName(name, name, date);

    return this.call(`${this.domain}/`, {
      method: "POST",
      body: JSON.stringify({
        name,
        email,
        date,
        roomName,
        doctorId,
        password,
        concern,
        date_requested,
        patient_company_id,
        active,
        appointment_code,
        parent_appointment_code,
        NoPassword
      })
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ create_error: res.message });
        }

        return Promise.resolve(res);
      })
      .catch(e => Promise.reject(e));
  }

  update(id, fields) {
    return this.call(`${this.domain}/${id}`, {
      method: "PUT",
      body: JSON.stringify(fields)
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ Update_Error: res.message });
        }

        return Promise.resolve(true);
      })
      .catch(e => Promise.reject(e));
  }

  cancel(id) {
    return this.call(`${this.domain}/cancel`, {
      method: "PUT",
      body: JSON.stringify({ id })
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ Cancel_Error: res.message });
        }

        return Promise.resolve(true);
      })
      .catch(e => Promise.reject(e));
  }

  getAppointment(...fields) {
    return this.call(`${this.domain}/${fields.id}`, {
      method: "GET",
      body: JSON.stringify(fields)
    })
      .then(res => Promise.resolve(res.appointment))
      .catch(e => Promise.reject(e));
  }

  getAppointmentsList(fields) {
    const profile = this.getProfile();
    if (fields) {
      fields.userId = profile.id;
      fields.userType = profile.type;
    } else {
      fields = { userId: profile.id, userType: profile.type };
    }

    const url = `${this.domain}?${queryString.stringify(fields)}`;

    return this.call(url, {
      method: "GET"
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ getAppointmentsList_error: res.message });
        }

        return Promise.resolve(res);
      })
      .catch(e => Promise.reject(e));
  }

  saveAppointmentHistory(data) {
    return this.call(`${this.domain}/history`, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ Update_Error: res.message });
        }

        return Promise.resolve(res);
      })
      .catch(e => Promise.reject(e));
  }

  call(url, options) {
    // performs api calls sending the required authentication headers
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };

    if (this.loggedIn()) {
      headers.Authorization = `Bearer ${this.getToken()}`;
    }

    // eslint-disable-next-line no-undef
    return fetch(url, {
      headers,
      ...options
    })
      .then(this._checkStatus)
      .then(response => response.json());
  }

  _checkStatus(response) {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      return response;
    }
    const error = new Error(response.statusText);

    error.response = response;
    throw error;
  }

  generateRoomName(name, doctor, date) {
    date = String(date.valueOf());
    // date = String(date)

    let result =
      name.replace(/\s/g, "").substring(0, 4) +
      doctor.replace(/\s/g, "").substring(0, 4) +
      date.substring(5, 9);
    result = result.replace(/[^a-zA-Z0-9]/g, "");

    return result;
  }

  getProfile() {
    // Retrieves the profile data from localStorage
    const profile = localStorage.getItem("profile");
    return profile ? JSON.parse(profile) : {};
  }

  loggedIn() {
    // Checks if there is a saved token and it's still valid
    const token = this.getToken();
    return !!token;
    //&& !isTokenExpired(token) // handwaiving here
  }

  getToken() {
    // Retrieves the user token from localStorage
    return localStorage.getItem("id_token");
  }
}
