import currentDomain from './domain.js'

export default class BookAppNotifService {
    constructor(domain) {
        this.domain = domain || `${currentDomain}/api/book_appointment_notification`
        // this.create = this.create.bind(this)
        // this.call = this.call.bind(this)
        // this.loggedIn = this.loggedIn.bind(this)
        // this.getToken = this.getToken.bind(this)
        // this._checkStatus = this._checkStatus.bind(this)
    }

    create = bookAppNotif => {
        return this.call(`${this.domain}`, {
            method: 'POST',
            body: JSON.stringify(bookAppNotif)
        })
            .then(res => {
                if (!res || res.message || res.status >= 400) {
                    return Promise.reject({ book_Appointment_Notification_Service_create_error: res.message })
                }
                return Promise.resolve(true)
            })
            .catch(e => {
                return Promise.reject(e)
            })
    }

    sendBookAppEmailNotification = emailDetails => {
        return this.call(`${this.domain}/bookAppNotifEmail`, {
            method: 'POST',
            body: JSON.stringify(emailDetails)
        })
            .then(res => {
                console.log('Email Servicex')
                console.log(res)
            })
            .catch(error => {
                console.log('Email Service errorXS')
                console.log(error)
                return Promise.reject(error)
            })
    }

    call = (url, options) => {
        // performs api calls sending the required authentication headers
        const headers = {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }

        if (this.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.getToken()
        }

        return fetch(url, {
            headers,
            ...options
        })
            .then(this._checkStatus)
            .then(response => response.json())
    }

    getToken = () => {
        // Retrieves the user token from localStorage
        return localStorage.getItem('id_token')
    }

    loggedIn = () => {
        // Checks if there is a saved token and it's still valid
        const token = this.getToken()
        return !!token
        //&& !isTokenExpired(token) // handwaiving here
    }

    _checkStatus = response => {
        // raises an error in case response status is not a success
        if (response.status >= 200 && response.status < 300) {
            return response
        } else {
            var error = new Error(response.statusText)
            error.response = response
            throw error
        }
    }
}
