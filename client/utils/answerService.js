import AuthService from "../utils/authService.js";
import "isomorphic-fetch";
import queryString from "query-string";
import currentDomain from "./domain.js";

const auth = new AuthService();

export default class AnswerService {
  constructor(domain) {
    this.domain = domain || `${currentDomain}/api/answers`;
    this.call = this.call.bind(this);
    this.create = this.create.bind(this);
    this.getProfile = this.getProfile.bind(this);
    this.returnAnswers = this.returnAnswers.bind(this);
    this.filterAnswer = this.filterAnswer.bind(this);
    this.getToken = this.getToken.bind(this);
    this.getQuery = this.getQuery.bind(this);
  }

  create(answers) {
    return this.call(`${this.domain}/`, {
      method: "POST",
      body: JSON.stringify(this.returnAnswers(answers))
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        return Promise.resolve(true);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  getPieChartData(data) {
    return this.call(
      `${this.domain}/getPieChartData/${data.question_id}/${data.company_name}`,
      {
        method: "GET"
      }
    )
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  returnAnswers(answers) {
    const profile = this.getProfile();
    const userId = profile.id;
    var newAnswers = [];
    for (const answer of answers) {
      newAnswers.push(this.filterAnswer(answer, userId));
    }
    return newAnswers;
  }

  filterAnswer(answer, userId, group) {
    if (!answer.count) {
      answer.count = 0;
    }

    return {
      questionId: answer.id,
      userId: userId,
      value: answer.value,
      count: answer.count
    };
  }

  update(answers) {
    return this.call(`${this.domain}/update`, {
      method: "PUT",
      body: JSON.stringify(this.returnAnswers(answers))
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        return Promise.resolve(true);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  get(query) {
    const profile = this.getProfile();

    return this.call(`${this.domain}/${query.id}`, {
      method: "GET"
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        console.log("getting answers done");
        return Promise.resolve(true);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  getAnswerList(fields, usrId = null, coreImbalances = false) {
    const profile = this.getProfile();

    if (fields) {
      fields.userId = profile.id;
    } else {
      fields = { userId: profile.id, userType: profile.type };
    }

    if (usrId) {
      fields.userId = usrId;
    }

    if (coreImbalances) {
      fields.coreImbalances = true;
    }

    let url = this.domain + "?" + queryString.stringify(fields);

    return this.call(url, {
      method: "GET"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  getGraphData(data) {
    return this.call(`${this.domain}/graphdata/`, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  getCoreImbalances(usrId = null) {
    let url = this.domain + "?" + queryString.stringify({ userID: usrId });
    return this.call(url, {
      method: "GET"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  // flexible queryString

  getQuery(qry) {
    return this.call(`${this.domain}/qry`, {
      method: "PUT",
      body: JSON.stringify({ query: qry })
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  call(url, options) {
    // performs api calls sending the required authentication headers
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };

    if (this.loggedIn()) {
      headers["Authorization"] = "Bearer " + this.getToken();
    }

    return fetch(url, {
      headers,
      ...options
    })
      .then(this._checkStatus)
      .then(response => response.json());
  }

  _checkStatus(response) {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      return response;
    } else {
      var error = new Error(response.statusText);
      error.response = response;
      throw error;
    }
  }

  loggedIn() {
    // Checks if there is a saved token and it's still valid
    const token = this.getToken();
    return !!token;
    //&& !isTokenExpired(token) // handwaiving here
  }

  getProfile() {
    // Retrieves the profile data from localStorage
    const profile = localStorage.getItem("profile");
    return profile ? JSON.parse(profile) : {};
  }

  getToken() {
    // Retrieves the user token from localStorage
    return localStorage.getItem("id_token");
  }
}
