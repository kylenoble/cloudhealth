/* global it, expect, describe */
import React from 'react'
import {
    shallow,
    mount
} from 'enzyme'
import sinon from 'sinon'

import GenerateSummary from '../generateSummary.js';

let nutritionData = [{
        name: 'specialDiet',
        value: 'Yes'
    },
    {
        name: 'foodAllergies',
        value: 'Yes'
    },
    {
        name: 'meatConsumedPercent',
        value: '40'
    },
    {
        name: 'vegetableConsumedPercent',
        value: '0'
    },
    {
        name: 'fruitConsumedPercent',
        value: '20'
    },
    {
        name: 'starchConsumedPercent',
        value: '40'
    },
    {
        name: 'poultryConsumedPercent',
        value: '0'
    },
    {
        name: 'seafoodConsumedPercent',
        value: '0'
    },
    {
        name: 'usualMeal',
        value: 'Fried Food, Grilled Food, Canned goods, Soda, Canned Juices, Tetra packs, 3-in-1 coffee, Energy Drinks, Instant noodles, Microwaveable foods'
    }
]

let sleepData = [{
        name: 'regularSleep',
        value: 'day'
    },
    {
        name: 'averageSleep',
        value: '<6'
    },
    {
        name: 'troubleFallingAsleep',
        value: 'yes'
    },
    {
        name: 'restingFeeling',
        value: 'no'
    },
    {
        name: 'sleepingAids',
        value: 'yes'
    },
]

let exerciseData = [{
        name: 'weeklyExercise',
        value: 'no'
    },
    {
        name: 'limitActivity',
        value: 'yes'
    },
    {
        name: 'postActivitySoreness',
        value: 'yes'
    },
    {
        name: 'workActivity',
        value: 'Mostly sitting or standing'
    },
    {
        name: 'sittingHours',
        value: 'About 4 hours or more'
    },
]

let bodyData = {
    waist: {
        name: 'waist',
        value: '32'
    },
    hip: {
        name: 'hip',
        value: '34'
    },
    height: {
        name: 'height',
        value: '72'
    },
    weight: {
        name: 'weight',
        value: '180'
    }
}

let stressData = [{
        name: 'counseling',
        value: 'yes'
    },
    {
        name: 'therapy',
        value: 'yes'
    },
    {
        name: 'workStress',
        value: '10'
    },
    {
        name: 'familyStress',
        value: '10'
    },
    {
        name: 'socialStress',
        value: '10'
    },
    {
        name: 'financialStress',
        value: '10'
    },
    {
        name: 'healthStress',
        value: '10'
    },
    {
        name: 'otherStress',
        value: '10'
    },
]

let detoxData = [{
        name: 'prescriptionDrugs',
        value: '5'
    },
    {
        name: 'prescriptionScenarios',
        value: 'side effects, efficacious at lowered dose(s)'
    },
    {
        name: 'caffeine',
        value: 'yes'
    },
    {
        name: 'illAlcohol',
        value: 'yes'
    },
    {
        name: 'overTheCounter',
        value: 'Acetaminophen/ Paracetamol or other pain medications'
    },
    {
        name: 'tobacco',
        value: 'yes'
    },
    {
        name: 'odors',
        value: 'yes'
    },
    {
        name: 'dependent',
        value: 'yes'
    }
]


describe('Nutrition Component', () => {
    it('Returns Correct Score when all items are at highest values', () => {
        var summaryGenerator = new GenerateSummary(nutritionData, 'nutrition');

        expect(summaryGenerator.generateSummary()).toEqual('5.000');
    })

    it('Returns Correct Score when specialDiet is no', () => {
        const alteredNutritionData = [{
                name: 'specialDiet',
                value: 'No'
            },
            ...nutritionData.slice(1)
        ];
        var summaryGenerator = new GenerateSummary(alteredNutritionData, 'nutrition');

        expect(summaryGenerator.generateSummary()).toEqual('4.200');
    })

    it('Returns Correct Score when foodAllergies is no', () => {
        const alteredNutritionData = [
            ...nutritionData.slice(0, 1),
            {
                name: 'foodAllergies',
                value: 'No'
            },
            ...nutritionData.slice(2)
        ];
        var summaryGenerator = new GenerateSummary(alteredNutritionData, 'nutrition');

        expect(summaryGenerator.generateSummary()).toEqual('4.200');
    })

    it('Returns Correct Score when meatConsumedPercent is less than 30', () => {
        const alteredNutritionData = [
            ...nutritionData.slice(0, 2),
            {
                name: 'meatConsumedPercent',
                value: '20'
            },
            ...nutritionData.slice(3)
        ];
        var summaryGenerator = new GenerateSummary(alteredNutritionData, 'nutrition');

        expect(summaryGenerator.generateSummary()).toEqual('4.200');
    })

    it('Returns Correct Score when starchConsumedPercent is less than 30', () => {
        const alteredNutritionData = [
            ...nutritionData.slice(0, 5),
            {
                name: 'starchConsumedPercent',
                value: '20'
            },
            ...nutritionData.slice(6)
        ];
        var summaryGenerator = new GenerateSummary(alteredNutritionData, 'nutrition');

        expect(summaryGenerator.generateSummary()).toEqual('4.200');
    })

    it('Returns Correct Score when usualMeal contains none', () => {
        const alteredNutritionData = [
            ...nutritionData.slice(0, nutritionData.length - 1),
            {
                name: 'usualMeal',
                value: ''
            },
        ];
        var summaryGenerator = new GenerateSummary(alteredNutritionData, 'nutrition');

        expect(summaryGenerator.generateSummary()).toEqual('4.100');
    })
})

describe('Sleep Component', () => {
    it('Returns Correct Score when all items are at highest values', () => {
        var summaryGenerator = new GenerateSummary(sleepData, 'sleep');

        expect(summaryGenerator.generateSummary()).toEqual(4.375);
    })

    it('Returns Correct Score when regularSleep is night', () => {
        const alteredSleepData = [{
                name: 'regularSleep',
                value: 'night'
            },
            ...sleepData.slice(1)
        ];
        var summaryGenerator = new GenerateSummary(alteredSleepData, 'sleep');

        expect(summaryGenerator.generateSummary()).toEqual(3.125);
    })

    it('Returns Correct Score when averageSleep is >10', () => {
        const alteredSleepData = [
            ...sleepData.slice(0, 1),
            {
                name: 'averageSleep',
                value: '>10'
            },
            ...sleepData.slice(2)
        ];
        var summaryGenerator = new GenerateSummary(alteredSleepData, 'sleep');

        expect(summaryGenerator.generateSummary()).toEqual(4.125);
    })

    it('Returns Correct Score when averageSleep is 8-10', () => {
        const alteredSleepData = [
            ...sleepData.slice(0, 1),
            {
                name: 'averageSleep',
                value: '8-10'
            },
            ...sleepData.slice(2)
        ];
        var summaryGenerator = new GenerateSummary(alteredSleepData, 'sleep');

        expect(summaryGenerator.generateSummary()).toEqual(3.875);
    })

    it('Returns Correct Score when averageSleep is 6-8', () => {
        const alteredSleepData = [
            ...sleepData.slice(0, 1),
            {
                name: 'averageSleep',
                value: '6-8'
            },
            ...sleepData.slice(2)
        ];
        var summaryGenerator = new GenerateSummary(alteredSleepData, 'sleep');

        expect(summaryGenerator.generateSummary()).toEqual(4.125);
    })

    it('Returns Correct Score when troubleFallingAsleep is no', () => {
        const alteredSleepData = [
            ...sleepData.slice(0, 2),
            {
                name: 'troubleFallingAsleep',
                value: 'no'
            },
            ...sleepData.slice(3)
        ];
        var summaryGenerator = new GenerateSummary(alteredSleepData, 'sleep');

        expect(summaryGenerator.generateSummary()).toEqual(3.875);
    })

    it('Returns Correct Score when restingFeeling is yes', () => {
        const alteredSleepData = [
            ...sleepData.slice(0, 3),
            {
                name: 'restingFeeling',
                value: 'yes'
            },
            ...sleepData.slice(4)
        ];
        var summaryGenerator = new GenerateSummary(alteredSleepData, 'sleep');

        expect(summaryGenerator.generateSummary()).toEqual(4.375);
    })

    it('Returns Correct Score when sleepingAids is no', () => {
        const alteredSleepData = [
            ...sleepData.slice(0, 4),
            {
                name: 'sleepingAids',
                value: 'no'
            }
        ];
        var summaryGenerator = new GenerateSummary(alteredSleepData, 'sleep');

        expect(summaryGenerator.generateSummary()).toEqual(3.125);
    })
})

describe('Exercise Component', () => {
    it('Returns Correct Score when all items are at highest values', () => {
        var summaryGenerator = new GenerateSummary(exerciseData, 'exercise');

        expect(summaryGenerator.generateSummary()).toEqual(5);
    })

    it('Returns Correct Score when weeklyExercise is yes', () => {
        const alteredExerciseData = [{
                name: 'weeklyExercise',
                value: 'yes'
            },
            ...exerciseData.slice(1)
        ];
        var summaryGenerator = new GenerateSummary(alteredExerciseData, 'exercise');

        expect(summaryGenerator.generateSummary()).toEqual(4.2);
    })

    it('Returns Correct Score when limitActivity is no', () => {
        const alteredExerciseData = [
            ...exerciseData.slice(0, 1),
            {
                name: 'limitActivity',
                value: 'no'
            },
            ...exerciseData.slice(2)
        ];
        var summaryGenerator = new GenerateSummary(alteredExerciseData, 'exercise');

        expect(summaryGenerator.generateSummary()).toEqual(4.2);
    })

    it('Returns Correct Score when postActivitySoreness is no', () => {
        const alteredExerciseData = [
            ...exerciseData.slice(0, 2),
            {
                name: 'postActivitySoreness',
                value: 'no'
            },
            ...exerciseData.slice(3)
        ];
        var summaryGenerator = new GenerateSummary(alteredExerciseData, 'exercise');

        expect(summaryGenerator.generateSummary()).toEqual(4.2);
    })

    it('Returns Correct Score when workActivity is not mostly sitting or standing', () => {
        const alteredExerciseData = [
            ...exerciseData.slice(0, 3),
            {
                name: 'workActivity',
                value: 'Mostly walking'
            },
            ...exerciseData.slice(4)
        ];
        var summaryGenerator = new GenerateSummary(alteredExerciseData, 'exercise');

        expect(summaryGenerator.generateSummary()).toEqual(4.2);
    })

    it('Returns Correct Score when sittingHours is 1-3', () => {
        const alteredExerciseData = [
            ...exerciseData.slice(0, 4),
            {
                name: 'sittingHours',
                value: 'About 1-3 hours'
            }
        ];
        var summaryGenerator = new GenerateSummary(alteredExerciseData, 'exercise');

        expect(summaryGenerator.generateSummary()).toEqual(4.6);
    })

    it('Returns Correct Score when sittingHours is <1', () => {
        const alteredExerciseData = [
            ...exerciseData.slice(0, 4),
            {
                name: 'sittingHours',
                value: '<1 hour'
            }
        ];
        var summaryGenerator = new GenerateSummary(alteredExerciseData, 'exercise');

        expect(summaryGenerator.generateSummary()).toEqual(4.2);
    })
})

describe('Weight Component', () => {
    it('Returns Correct Score when all items are set at defaults', () => {
        var summaryGenerator = new GenerateSummary(bodyData, 'body', 'male');

        expect(summaryGenerator.generateBMI()).toEqual(24.41);
        expect(summaryGenerator.generateWeightHip()).toEqual(0.94);
        expect(summaryGenerator.generateBodyScore()).toEqual(3);
    })

    it('Returns Correct Score when height is very short compared to weight', () => {
        const alteredbodyData = bodyData;
        alteredbodyData.height.value = '60';

        var summaryGenerator = new GenerateSummary(alteredbodyData, 'body', 'male');

        expect(summaryGenerator.generateBMI()).toEqual(35.15);
        expect(summaryGenerator.generateWeightHip()).toEqual(0.94);
        expect(summaryGenerator.generateBodyScore()).toEqual(5);
    })

    it('Returns Correct Score when weight is high compared to height', () => {
        const alteredbodyData = bodyData;
        alteredbodyData.weight.value = '250';
        alteredbodyData.height.value = '72';

        var summaryGenerator = new GenerateSummary(alteredbodyData, 'body', 'male');

        expect(summaryGenerator.generateBMI()).toEqual(33.91);
        expect(summaryGenerator.generateWeightHip()).toEqual(0.94);
        expect(summaryGenerator.generateBodyScore()).toEqual(5);
    })

    it('Returns Correct Score when waist is large compared to hip', () => {
        const alteredbodyData = bodyData;
        alteredbodyData.weight.value = '180';
        alteredbodyData.height.value = '72';
        alteredbodyData.waist.value = '38';
        alteredbodyData.hip.value = '32';

        var summaryGenerator = new GenerateSummary(alteredbodyData, 'body', 'male');

        expect(summaryGenerator.generateBMI()).toEqual(24.41);
        expect(summaryGenerator.generateWeightHip()).toEqual(1.19);
        expect(summaryGenerator.generateBodyScore()).toEqual(3);
    })

    it('Returns Correct Score when hip is large compared to waist', () => {
        const alteredbodyData = bodyData;
        alteredbodyData.weight.value = '180';
        alteredbodyData.height.value = '72';
        alteredbodyData.waist.value = '34';
        alteredbodyData.hip.value = '38';

        var summaryGenerator = new GenerateSummary(alteredbodyData, 'body', 'male');

        expect(summaryGenerator.generateBMI()).toEqual(24.41);
        expect(summaryGenerator.generateWeightHip()).toEqual(0.89);
        expect(summaryGenerator.generateBodyScore()).toEqual(3);
    })

    it('Returns Correct Score when hip is large compared to waist for women', () => {
        const alteredbodyData = bodyData;
        alteredbodyData.weight.value = '180';
        alteredbodyData.height.value = '72';
        alteredbodyData.waist.value = '34';
        alteredbodyData.hip.value = '38';

        var summaryGenerator = new GenerateSummary(alteredbodyData, 'body', 'female');

        expect(summaryGenerator.generateBMI()).toEqual(24.41);
        expect(summaryGenerator.generateWeightHip()).toEqual(0.89);
        expect(summaryGenerator.generateBodyScore()).toEqual(3);
    })
})

describe('Stress Component', () => {
    it('Returns Correct Score when all items are at highest values', () => {
        var summaryGenerator = new GenerateSummary(stressData, 'stress');

        expect(summaryGenerator.generateSummary()).toEqual(5);
    })

    it('Returns Correct Score when counseling is no', () => {
        const alteredStressData = [{
                name: 'counseling',
                value: 'no'
            },
            ...stressData.slice(1)
        ];
        var summaryGenerator = new GenerateSummary(alteredStressData, 'stress');

        expect(summaryGenerator.generateSummary()).toEqual(4);
    })

    it('Returns Correct Score when therapy is no', () => {
        const alteredStressData = [
            ...stressData.slice(0, 1),
            {
                name: 'therapy',
                value: 'no'
            },
            ...stressData.slice(2)
        ];
        var summaryGenerator = new GenerateSummary(alteredStressData, 'stress');

        expect(summaryGenerator.generateSummary()).toEqual(4);
    })
})

describe('Detox Component', () => {
    it('Returns Correct Score when all items are at highest values', () => {
        var summaryGenerator = new GenerateSummary(detoxData, 'detox');

        expect(summaryGenerator.generateSummary()).toEqual(5);
    })

    it('Returns Correct Score when pres drugs is 0 and prescriptionScenarios is no side, efficacious', () => {
        const alteredDetoxData = [{
                name: 'prescriptionDrugs',
                value: '0'
            },
            {
                name: 'prescriptionScenarios',
                value: 'no side effects, usually efficacious'
            },
            {
                name: 'caffeine',
                value: 'no'
            },
            {
                name: 'illAlcohol',
                value: 'no'
            },
            ...detoxData.slice(3)
        ];
        var summaryGenerator = new GenerateSummary(alteredDetoxData, 'detox');

        expect(summaryGenerator.generateSummary()).toEqual(3);
    })

    it('Returns Correct Score when therapy is no', () => {
        const alteredDetoxData = [{
                name: 'prescriptionDrugs',
                value: '0'
            },
            {
                name: 'prescriptionScenarios',
                value: 'no side effects, usually efficacious'
            },
            {
                name: 'caffeine',
                value: 'no'
            },
            {
                name: 'illAlcohol',
                value: 'no'
            },
            {
                name: 'overTheCounter',
                value: 'no'
            },
            {
                name: 'tobacco',
                value: 'no'
            },
            {
                name: 'odors',
                value: 'no'
            },
            {
                name: 'dependent',
                value: 'no'
            }
        ];
        var summaryGenerator = new GenerateSummary(alteredDetoxData, 'detox');

        expect(summaryGenerator.generateSummary()).toEqual(1);
    })
})
