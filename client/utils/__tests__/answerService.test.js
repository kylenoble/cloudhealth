/* global it, expect, describe */
import React from 'react'
import { shallow, mount } from 'enzyme'
import sinon from 'sinon'
import 'isomorphic-fetch'
import fetchMock from 'fetch-mock'
import queryString from 'query-string'
require('dotenv').config();

import localStorageMock from '../local-storage-mock.js'
import AnswerService from '../answerService.js'

Object.defineProperty(window, 'localStorage', { value: localStorageMock });

afterEach(() => {
    /** We call restore() on fetchMock to reset any old stubs
         we defined in other tests*/
    localStorage.clear();
});

fetchMock.post({
    name: 'CreateSuccess',
    matcher: function(url, opts) {
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization;
        if (opts.body) {
            opts.body = JSON.parse(opts.body);
        }
        return(url==='http://localhost:4040/api/answers/' && opts && auth && opts.body && opts.body[0].questionId && opts.body[0].value && opts.body[0].userId)
    },
    response: {
        result:{}
    }
})

fetchMock.post({
    name: 'CreateFailedAuth',
    matcher: function(url, opts) {
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization
        return (url==='http://localhost:4040/api/answers/' && !auth)
    },
    response: {
        message: 'Auth Failed - Create',
        status: 403
    }
})

fetchMock.post({
    name: 'CreateFailed',
    matcher: function(url, opts) {
        return (url==='http://localhost:4040/api/answers/' && (!opts.body || !opts.body[0].questionId || !opts.body[0].value || !opts.body[0].userId))
    },
    response: {
        message: 'No Answers Included - Create'
    }
})

fetchMock.mock({
    name: 'UpdateSuccess',
    matcher: function(url, opts) {
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization;
        return(url==='http://localhost:4040/api/answers/update' && opts && auth && opts.body && opts.body[0].questionId && opts.body[0].value && opts.body[0].userId);
    },
    response: {
        result:{}
    }
})

fetchMock.mock({
    name: 'UpdateFailedAuth',
    matcher: function(url, opts) {
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization;
        return (url==="http://localhost:4040/api/answers/update" && !auth);
    },
    response: {
        message: 'Auth Failed - Update',
        status: 403
    }
})

fetchMock.mock({
    name: 'UpdateFailed',
    matcher: function(url, opts) {
        if (!opts.body) {
            return false;
        }
        return (url==="http://localhost:4040/api/answers/update" && !opts.body || !opts.body[0].questionId || !opts.body[0].value || !opts.body[0].userId);
    },
    response: {
        message: 'No Answers Included - Update'
    }
})

fetchMock.get({
    name: 'GetSuccess',
    matcher: function(url, opts) {
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization;
        var newUrl = url.split('?')
        var domain = newUrl[0]
        var params = queryString.parse(newUrl[1])
        return(domain==='http://localhost:4040/api/answers' && auth && params.column && params.value && params.operator);
    },
    response: [
        {"id":1,"question_id":1,"user_id":9,"value":"01/01/1990","name":"birthday","label":"Birthday","group":"personal"},
        {"id":2,"question_id":2,"user_id":9,"value":"Male","name":"gender","label":"Gender","group":"personal"},
        {"id":3,"question_id":3,"user_id":9,"value":"Married","name":"relationship","label":"Relationship","group":"personal"},
        {"id":4,"question_id":4,"user_id":9,"value":"Prefer Not To State","name":"race","label":"Race","group":"personal"}
    ]
})

fetchMock.get({
    name: 'GetFailedAuth',
    matcher: function(url, opts) {
        var newUrl = url.split('?')
        var domain = newUrl[0]
        var params = queryString.parse(newUrl[1])
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization;
        return (domain==='http://localhost:4040/api/answers' && !auth);
    },
    response: {
        message: 'Auth Failed - Get',
        status: 403
    }
})

fetchMock.get({
    name: 'GetFailed',
    matcher: function(url, opts) {
        var newUrl = url.split('?')
        var domain = newUrl[0]
        var params = queryString.parse(newUrl[1])
        return (domain==='http://localhost:4040/api/answers' && (params === {} || !params.column || !params.value || !params.operator));
    },
    response: {
        message: 'No Params Included - Get'
    }
})

describe('Answer Service - Constructor', () => {
  it('Constructor shows Localhost Domain', () => {
    var answers = new AnswerService('http://localhost:4040/api/answers');

    expect(answers.domain).toEqual('http://localhost:4040/api/answers')
  })
})

describe('Answer Service - Create', () => {
  it('Successful Answer Create Returns True', () => {
    var answers = new AnswerService('http://localhost:4040/api/answers');

    localStorage.setItem('id_token', process.env.TestIdToken)
    var profile = {"id":9,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)

    var answer = [
      {
        group: "Head and Mind",
        id: 32,
        label: "Anger irritanbility, aggressiveness",
        name: "anger",
        question_id: 32,
        submitted: true,
        user_id: 9,
        value: 2,
      }
    ]

    return answers.create(answer)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value).toEqual(true);
    })
  })

 it('Empty Answer Returns Failure', () => {
    var answers = new AnswerService('http://localhost:4040/api/answers');

    localStorage.setItem('id_token', process.env.TestIdToken)
    var answer = [{}]

    return answers.create(answer)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value).toEqual(true);
    })
    .catch(e => {
        expect(e.error).toEqual('No Answers Included - Create')
    })
  })

 it('Incorrect Auth Returns Failure', () => {
    var answers = new AnswerService('http://localhost:4040/api/answers');

    var id_token = "token"
    localStorage.setItem('id_token', id_token)

    var answer = [
      {
        group: "Head and Mind",
        id: 32,
        label: "Anger irritanbility, aggressiveness",
        name: "anger",
        question_id: 32,
        submitted: true,
        user_id: 9,
        value: 2,
      }
    ]

    return answers.create(answer)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value).toEqual(true);
    })
    .catch(e => {
        expect(e.response.statusText).toEqual('Forbidden')
    })
  })
})

describe('Answer Service - Update', () => {
  it('Successful Answer Update Returns True', () => {
    var answers = new AnswerService('http://localhost:4040/api/answers');

    localStorage.setItem('id_token', process.env.TestIdToken)
    var profile = {"id":9,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)

    var answer = [
      {
        group: "Head and Mind",
        id: 32,
        label: "Anger irritanbility, aggressiveness",
        name: "anger",
        question_id: 32,
        submitted: true,
        user_id: 9,
        value: 2,
      }
    ]

    return answers.update(answer)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value).toEqual(true);
    })
  })

 it('Empty Answer Returns Failure', () => {
    var answers = new AnswerService('http://localhost:4040/api/answers');

    localStorage.setItem('id_token', process.env.TestIdToken)
    var answer = [{}]

    return answers.update(answer)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value).toEqual(true);
    })
    .catch(e => {
        expect(e.error).toEqual('No Answers Included - Update')
    })
  })

 it('Incorrect Auth Returns Failure', () => {
    var answers = new AnswerService('http://localhost:4040/api/answers');

    var id_token = "token"
    localStorage.setItem('id_token', id_token)

    var answer = [
      {
        group: "Head and Mind",
        id: 32,
        label: "Anger irritanbility, aggressiveness",
        name: "anger",
        question_id: 32,
        submitted: true,
        user_id: 9,
        value: 2,
      }
    ]

    return answers.update(answer)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value).toEqual(true);
    })
    .catch(e => {
        expect(e.response.statusText).toEqual('Forbidden')
    })
  })
})

describe('Answer Service - Get List', () => {
  it('Successful Answer Get Returns Values', () => {
    var answers = new AnswerService('http://localhost:4040/api/answers');

    localStorage.setItem('id_token', process.env.TestIdToken)
    var profile = {"id":9,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)

    var params = {column: "question_id", operator: "lt", value: 5}

    return answers.getAnswerList(params)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value.length).toEqual(4);
        expect(value[0].id).toEqual(1);
    })
  })

 it('Empty Get Params Returns Failure', () => {
    var answers = new AnswerService('http://localhost:4040/api/answers');

    localStorage.setItem('id_token', process.env.TestIdToken)
    var params = {}

    return answers.getAnswerList(params)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value.message).toEqual('No Params Included - Get');
    })
    .catch(e => {
        expect(e.error).toEqual('No Params Included - Get')
    })
  })

 it('Incorrect Auth Returns Failure', () => {
    var answers = new AnswerService('http://localhost:4040/api/answers');

    var id_token = "token"
    localStorage.setItem('id_token', id_token)

    var params = {column: "question_id", operator: "lt", value: 5}

    return answers.getAnswerList(params)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value.message).toEqual('Auth Failed - Get');
    })
    .catch(e => {
        expect(e.response.statusText).toEqual('Forbidden')
    })
  })
})
