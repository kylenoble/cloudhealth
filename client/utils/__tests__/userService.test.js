/* global it, expect, describe */
import React from 'react'
import { shallow, mount } from 'enzyme'
import sinon from 'sinon'
import 'isomorphic-fetch'
import fetchMock from 'fetch-mock'
import queryString from 'query-string'
require('dotenv').config();

import UserService from '../UserService.js'
import localStorageMock from '../local-storage-mock.js'

Object.defineProperty(window, 'localStorage', { value: localStorageMock });

beforeEach(() => {
   process.env.DOMAIN = 'http://localhost:4040';
})

afterEach(() => {
    localStorage.clear()
})

process.env.DOMAIN = 'http://localhost:4040'
import currentDomain from '../domain.js'

fetchMock.post({
    name: 'CreateSuccess',
    matcher: function(url, opts) {
        if (opts.body) {
            opts.body = JSON.parse(opts.body);
        }
        return(url==='http://localhost:4040/api/users/' && opts && opts.body && opts.body.email === 'test1@mail.com' && opts.body.password === 'password1' && opts.body.company === 'other' && opts.body.name === 'Test Name')
    },
    response: {
        id: 9,
        token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXNzd29yZCI6IiQyYSQxMCQ0UC8wZFhCb3Q1YS9HZHdTWFIyTFNlWW9PU0ticE1OZGluMWtGU2lhazFsNjRnb0p4RG13MiIsI"
    }
})

fetchMock.post({
    name: 'CreateFailed',
    matcher: function(url, opts) {
        return (url==='http://localhost:4040/api/users/' && (!opts.body || opts.body.email !== 'test@mail.com' || opts.body.password !== 'password1' || opts.body.company !== 'other' || opts.body.name !== 'Test Name'))
    },
    response: {
        message: 'No Body Included - Create'
    }
})

fetchMock.post({
    name: 'LoginSuccess',
    matcher: function(url, opts) {
        return((url==='http://localhost:4040/api/auth/login' || url === 'undefined/api/auth/login')  && opts && opts.body && opts.body.email === 'test1@mail.com' && opts.body.password === 'password1')
    },
    response: {
        id: 10,
        token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXNzd29yZCI6IiQyYSQxMCQ0UC8wZFhCb3Q1YS9HZHdTWFIyTFNlWW9PU0ticE1OZGluMWtGU2lhazFsNjRnb0p4RG13MiIsI"
    }
})

fetchMock.get({
    name: 'GetUser',
    matcher: function(url, opts) {
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization;
        return ((url==='http://localhost:4040/api/users/9' || url==='undefined/api/users/9') && auth && opts.method === 'GET')
    },
    response: {
        user: {
            company: "247-Inc",
            email: "tj@gmail.com",
            id: 9,
            name: "TJ Johnson",
            type: "Patient"
        }
    }
})

fetchMock.get({
    name: 'GetUserIncorrect',
    matcher: function(url, opts) {
        return ((url==='http://localhost:4040/api/users/10' || url==='undefined/api/users/10') && opts.method === 'GET')
    },
    response: {
        message: 'Error retrieving user data',
        status: 404
    }
})

fetchMock.get({
    name: 'GetFailedAuth',
    matcher: function(url, opts) {
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization;
        return (url==='http://localhost:4040/api/users/9' && !auth && opts.method === 'GET');
    },
    response: {
        message: 'Auth Failed - Get',
        status: 403
    }
})

fetchMock.put({
    name: 'UpdateSuccess',
    matcher: function(url, opts) {
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization;
        return(url==='http://localhost:4040/api/users/9' && opts && auth && opts.body && opts.body.name !== '' && opts.body.email !== '' && opts.body.company !== '')
    },
    response: {
        user: {
            company: "247-Inc",
            email: "tj@gmail.com",
            id: 9,
            name: "T.J. Johnson",
            type: "Patient"
        }
    }
})

fetchMock.put({
    name: 'UpdateFailedAuth',
    matcher: function(url, opts) {
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization
        return (url==='http://localhost:4040/api/users/9' && !auth)
    },
    response: {
        body: 'Auth Failed - Update',
        status: 403
    }
})

fetchMock.put({
    name: 'UpdateFailed',
    matcher: function(url, opts) {
        return (url==='http://localhost:4040/api/users/9' && (!opts.body || opts.body.name === '' || opts.body.email === '' || opts.body.company === ''))
    },
    response: {
        message: 'No User Included - Update',
        status: 400
    }
})


describe('User Service - create', () => {
  it('Successful user create Returns Profile', () => {
    var user = new UserService('http://localhost:4040/api/users');

    return user.create('test1@mail.com', 'password1', 'other', 'Test Name')
    .then(value => {
        /*Perform our assertions as normal */
        expect(value).toEqual(true);
    })
  })

  it('Failed user create Returns Message', () => {
    var user = new UserService('http://localhost:4040/api/users');

    return user.create('test@mail.com', 'password2', '', '')
    .then(value => {
        /*Perform our assertions as normal */
    })
    .catch(error => {
        expect(error.error).toEqual('No Body Included - Create');
    })
  })
})

describe('User Service - Get', () => {
  it('Successful user Get Returns Values', () => {
    var users = new UserService('http://localhost:4040/api/users');

    localStorage.setItem('id_token', process.env.TestIdToken)
    var profile = {"id":9,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)

    return users.get()
    .then(value => {
        /*Perform our assertions as normal */
        expect(value.id).toEqual(9);
        expect(value.email).toEqual('tj@gmail.com');
    })
  })

 it('Incorrect Id Returns Failure', () => {
    var users = new UserService('http://localhost:4040/api/users');

    localStorage.setItem('id_token', process.env.TestIdToken)
    var profile = {"id":10,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)

    return users.get()
    .then(value => {
        /*Perform our assertions as normal */
        expect(value.message).toEqual('Error retrieving user data');
    })
    .catch(e => {
        expect(e.response.statusText).toEqual('Not Found')
    })
  })

 it('Incorrect Auth Returns Failure', () => {
    var users = new UserService('http://localhost:4040/api/users');

    var id_token = "token"
    localStorage.setItem('id_token', id_token)
    var profile = {"id":9,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)

    return users.get()
    .then(value => {
        /*Perform our assertions as normal */
        expect(value.message).toEqual('Auth Failed - Get');
    })
    .catch(e => {
        expect(e.response.statusText).toEqual('Forbidden')
    })
  })
})

describe('user Service - Update', () => {
  it('Successful user Update Returns User', () => {
    var users = new UserService('http://localhost:4040/api/users');

    localStorage.setItem('id_token', process.env.TestIdToken)
    var profile = {"id":9,"email":"kyle.nobl@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)

    var user = [
        {
            company: "247-Inc",
            email: "kyle.nobl@gmail.com",
            id: 9,
            name: "T.J. Johnson",
        }
    ]

    return users.update(user)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value.user.id).toEqual(9);
        expect(value.user.name).toEqual('T.J. Johnson');
    })
  })

 it('Empty user Returns Failure', () => {
    var users = new UserService('http://localhost:4040/api/users');

    var profile = {"id":9,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)
    localStorage.setItem('id_token', process.env.TestIdToken)
    var user = [{}]

    return users.update(user)
    .then(value => {
        /*Perform our assertions as normal */
    })
    .catch(e => {
        expect(e.response.statusText).toEqual('Bad Request')
    })
  })

 it('Incorrect Auth Returns Failure', () => {
    var users = new UserService('http://localhost:4040/api/users');

    var profile = {"id":9,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)
    var id_token = "token"
    localStorage.setItem('id_token', id_token)

    var user = [
        {
            company: "247-Inc",
            email: "kyle.nobl@gmail.com",
            id: 9,
            name: "T.J. Johnson",
        },

    ]


    return users.update(user)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value).toEqual(true);
    })
    .catch(e => {
        expect(e.response.statusText).toEqual('Forbidden')
    })
  })
})
