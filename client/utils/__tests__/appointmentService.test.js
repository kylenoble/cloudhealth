/* global it, expect, describe */
import React from 'react'
import { shallow, mount } from 'enzyme'
import sinon from 'sinon'
import 'isomorphic-fetch'
import fetchMock from 'fetch-mock'
import queryString from 'query-string'
require('dotenv').config();

import localStorageMock from '../local-storage-mock.js'
import AppointmentService from '../appointmentService.js'

Object.defineProperty(window, 'localStorage', { value: localStorageMock });

afterEach(() => {
    /** We call restore() on fetchMock to reset any old stubs
         we defined in other tests*/
    localStorage.clear();
});

fetchMock.post({
    name: 'CreateSuccess',
    matcher: function(url, opts) {
        if (opts.body) {
            opts.body = JSON.parse(opts.body);
        }  
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization;
        return(url==='http://localhost:4040/api/appointments/' && opts && auth && opts.body && opts.body.name !== '' && opts.body.email !== '' && opts.body.date !== '' && opts.body.roomName !== '' && opts.body.doctorId !== '')
    },
    response: [
        {
            datetime: "2017-01-18T21:00:00.000Z",
            doctor_id:10,
            id:5,
            name:"TJ Johnson",
            patient_id:9,
            room_name:"TestDrH7320"
        },
        {
            datetime: "2017-01-19T21:00:00.000Z",
            doctor_id:10,
            id:6,
            name:"TJ Johnson",
            patient_id:9,
            room_name:"TestDrH7325"
        }
    ]
})

fetchMock.post({
    name: 'CreateFailedAuth',
    matcher: function(url, opts) {
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization
        return (url==='http://localhost:4040/api/appointments/' && !auth)
    },
    response: {  
        body: 'Auth Failed - Create',
        status: 403
    }
})

fetchMock.post({
    name: 'CreateFailed',
    matcher: function(url, opts) {  
        return (url==='http://localhost:4040/api/appointments/' && (!opts.body || opts.body.name === '' || opts.body.email === '' || opts.body.date === '' || opts.body.roomName === '' || opts.body.doctorId === ''))
    },
    response: {  
        message: 'No Appointment Included - Create'
    }
})

fetchMock.get({
    name: 'GetSuccess',
    matcher: function(url, opts) {
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization;
        var newUrl = url.split('?')
        var domain = newUrl[0]
        var params = queryString.parse(newUrl[1])
        return(domain==='http://localhost:4040/api/appointments' && auth && params.column && params.value && params.operator);
    },
    response: [
        {
            datetime: "2016-12-26T21:00:00.000Z",
            doctor_id:6,
            id:4,
            name:"Dr Stupdendous",
            patient_id:9,
            room_name:"TjJoDrS600"
        },
        {
            datetime: "2016-12-26T21:00:00.000Z",
            doctor_id:6,
            id:4,
            name:"Dr Stupdendous",
            patient_id:9,
            room_name:"TjJoDrS600"
        }, 
        {
            datetime: "2016-12-26T21:00:00.000Z",
            doctor_id:6,
            id:4,
            name:"Dr Stupdendous",
            patient_id:9,
            room_name:"TjJoDrS600"
        },
        {
            datetime: "2016-12-26T21:00:00.000Z",
            doctor_id:6,
            id:4,
            name:"Dr Stupdendous",
            patient_id:9,
            room_name:"TjJoDrS600"
        }              
    ]
})

fetchMock.get({
    name: 'GetFailedAuth',
    matcher: function(url, opts) {
        var newUrl = url.split('?')
        var domain = newUrl[0]
        var params = queryString.parse(newUrl[1])
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization;
        return (domain==='http://localhost:4040/api/appointments' && !auth);
    },
    response: {  
        message: 'Auth Failed - Get',
        status: 403
    }
})

fetchMock.get({
    name: 'GetFailed',
    matcher: function(url, opts) {
        var newUrl = url.split('?')
        var domain = newUrl[0]
        var params = queryString.parse(newUrl[1])
        return (domain==='http://localhost:4040/api/appointments' && (params === {} || !params.column || !params.value || !params.operator));
    },
    response: {  
        message: 'No Params Included - Get'
    }
})

fetchMock.put({
    name: 'UpdateSuccess',
    matcher: function(url, opts) {
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization;
        return(url==='http://localhost:4040/api/appointments/1' && opts && auth && opts.body && opts.body.active !== '') 
    },
    response: [
        {
            datetime: "2017-01-18T21:00:00.000Z",
            doctor_id:10,
            id:5,
            name:"TJ Johnson",
            patient_id:9,
            room_name:"TestDrH7320"
        },
        {
            datetime: "2017-01-19T21:00:00.000Z",
            doctor_id:10,
            id:6,
            name:"TJ Johnson",
            patient_id:9,
            room_name:"TestDrH7325"
        }
    ]
})

fetchMock.put({
    name: 'UpdateFailedAuth',
    matcher: function(url, opts) {
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization
        return (url==='http://localhost:4040/api/appointments/1' && !auth)
    },
    response: {  
        body: 'Auth Failed - Update',
        status: 403
    }
})

fetchMock.put({
    name: 'UpdateFailed',
    matcher: function(url, opts) { 
        var auth = process.env.AUTH_HEADER === opts.headers.Authorization
        return (url==='http://localhost:4040/api/appointments/1' && auth && (!opts.body || opts.body.active === ''))
    },
    response: {  
        status: 400,
        message: 'No Appointment Included - Update'
    }
})


describe('Appointment Service - Constructor', () => {
  it('Constructor shows Localhost Domain', () => {
    var appointments = new AppointmentService('http://localhost:4040/api/appointments');

    expect(appointments.domain).toEqual('http://localhost:4040/api/appointments')
  })
})

describe('Appointment Service - Create', () => {
  it('Successful Appointment Create Returns True', () => {
    var appointments = new AppointmentService('http://localhost:4040/api/appointments');
    
    localStorage.setItem('id_token', process.env.TestIdToken)
    var profile = {"id":9,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)

    let dateMoment = {
        date: '01/01/2017 13:00:00',
        init: function() {
            this.valueOf = function() {
                return this.date;
            }
            return this;
        }
   }.init();
       
    let appointment = {
        name: 'TJ Johnson',
        email: 'tj@test.com',
        date: dateMoment
    }
    
    return appointments.create(appointment.name, appointment.email, appointment.date)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value.length).toEqual(2);
        expect(value[0].id).toEqual(5);
    })
  })

 it('Empty Appointment Returns Failure', () => {
    var appointments = new AppointmentService('http://localhost:4040/api/appointments');

    localStorage.setItem('id_token', process.env.TestIdToken)
    var profile = {"id":9,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)
    let dateMoment = {
        date: '01/01/2017 13:00:00',
        init: function() {
            this.valueOf = function() {
                return this.date;
            }
            return this;
        }
   }.init();
       
    let appointment = {
        name: 'TJ Johnson',
        date: dateMoment
    }

    return appointments.create(appointment.name, '', appointment.date)
    .then(value => {
        /*Perform our assertions as normal */
    })
    .catch(e => {
        expect(e.error).toEqual('No Appointment Included - Create')
    })
  }) 

 it('Incorrect Auth Returns Failure', () => {
    var appointments = new AppointmentService('http://localhost:4040/api/appointments');
  

    var profile = {"id":9,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)
    var id_token = "token"
    localStorage.setItem('id_token', id_token)

    let dateMoment = {
        date: '01/01/2017 13:00:00',
        init: function() {
            this.valueOf = function() {
                return this.date;
            }
            return this;
        }
   }.init();

    let appointment = {
        name: 'TJ Johnson',
        email: 'tj@test.com',
        date: dateMoment
    }

    return appointments.create(appointment.name, appointment.email, appointment.date)
    .then(value => {
        /*Perform our assertions as normal */
    })
    .catch(e => {
        expect(e.response.statusText).toEqual('Forbidden')
    })
  })   
})

describe('Answer Service - Get List', () => {
  it('Successful Answer Get Returns Values', () => {  
    var appointments = new AppointmentService('http://localhost:4040/api/appointments');
    
    localStorage.setItem('id_token', process.env.TestIdToken)
    var profile = {"id":9,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)

    var params = {column: "question_id", operator: "lt", value: 5}

    return appointments.getAppointmentsList(params)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value.length).toEqual(4);
        expect(value[0].id).toEqual(4);
    })
  })    

 it('Empty Get Params Returns Failure', () => {
    var appointments = new AppointmentService('http://localhost:4040/api/appointments');

    localStorage.setItem('id_token', process.env.TestIdToken)
    var params = {}

    return appointments.getAppointmentsList(params)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value.message).toEqual('No Params Included - Get');
    })
    .catch(e => {
        expect(e.error).toEqual('No Params Included - Get')
    })
  }) 

 it('Incorrect Auth Returns Failure', () => {
    var appointments = new AppointmentService('http://localhost:4040/api/appointments');
  
    var id_token = "token"
    localStorage.setItem('id_token', id_token)

    var params = {column: "question_id", operator: "lt", value: 5}

    return appointments.getAppointmentsList(params)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value.message).toEqual('Auth Failed - Get');
    })
    .catch(e => {
        expect(e.response.statusText).toEqual('Forbidden')
    })
  })   
})

describe('Answer Service - Update', () => {
  it('Successful Answer Update Returns True', () => {  
    var appointments = new AppointmentService('http://localhost:4040/api/appointments');
    
    localStorage.setItem('id_token', process.env.TestIdToken)
    var profile = {"id":9,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)

    let dateMoment = {
        date: '01/01/2017 13:00:00',
        init: function() {
            this.valueOf = function() {
                return this.date;
            }
            return this;
        }
   }.init();

    let appointment = {
        active: true,
        id: 1
    }
    return appointments.update(1, appointment)
    .then(value => {
        /*Perform our assertions as normal */
        expect(value).toEqual(true);
    })
  })    

 it('Empty Answer Returns Failure', () => {
    var appointments = new AppointmentService('http://localhost:4040/api/appointments');

    var profile = {"id":9,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)
    localStorage.setItem('id_token', process.env.TestIdToken)
    let dateMoment = {
        date: '01/01/2017 13:00:00',
        init: function() {
            this.valueOf = function() {
                return this.date;
            }
            return this;
        }
   }.init();
       
    let appointment = {
        active: '',
        id: 1
    }

    return appointments.update(1, appointment)
    .then(value => {
        /*Perform our assertions as normal */
    })
    .catch(e => {
        expect(e.response.statusText).toEqual('Bad Request')
    })
  }) 

 it('Incorrect Auth Returns Failure', () => {
    var appointments = new AppointmentService('http://localhost:4040/api/appointments');
  
    var id_token = "token"
    localStorage.setItem('id_token', id_token)
    var profile = {"id":9,"email":"tj@gmail.com","company":"247-Inc","type":"Patient","name":"TJ Johnson"}
    profile = JSON.stringify(profile)
    localStorage.setItem('profile', profile)    

    let dateMoment = {
        date: '01/01/2017 13:00:00',
        init: function() {
            this.valueOf = function() {
                return this.date;
            }
            return this;
        }
   }.init();

    let appointment = {
        active: true,
        id: 1
    }

    return appointments.update(1, appointment)
    .then(value => {
        /*Perform our assertions as normal */
    })
    .catch(e => {
        expect(e.response.statusText).toEqual('Forbidden')
    })
  })   
})
