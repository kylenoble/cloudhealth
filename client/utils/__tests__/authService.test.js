/* global it, expect, describe */
import React from 'react'
import { shallow, mount } from 'enzyme'
import sinon from 'sinon'
import 'isomorphic-fetch'
import fetchMock from 'fetch-mock'
import queryString from 'query-string'
require('dotenv').config();

import AuthService from '../authService.js'
import localStorageMock from '../local-storage-mock.js'

Object.defineProperty(window, 'localStorage', { value: localStorageMock });

beforeEach(() => {
   process.env.DOMAIN = 'http://localhost:4040';
})

process.env.DOMAIN = 'http://localhost:4040'
import currentDomain from '../domain.js'

fetchMock.post({
    name: 'LoginSuccess',
    matcher: function(url, opts) {
        if (opts.body) {
            opts.body = JSON.parse(opts.body);
        }
        return(url==='http://localhost:4040/api/login' && opts && opts.body && opts.body.email === 'test@mail.com' && opts.body.password === 'password1')
    },
    response: {
        id: 9,
        token: "token"
    }
})

fetchMock.post({
    name: 'LoginSuccess',
    matcher: function(url, opts) {
        return(url==='http://localhost:4040/api/login' && opts && opts.body && opts.body.email === 'test1@mail.com' && opts.body.password === 'password1')
    },
    response: {
        id: 10,
        token: "token"
    }
})

fetchMock.post({
    name: 'LoginFailed',
    matcher: function(url, opts) {
        return (url==='http://localhost:4040/api/login' && (!opts.body || !opts.body.email || !opts.body.password))
    },
    response: {
        message: 'No Body Included - Login'
    }
})

fetchMock.post({
    name: 'LoginFailed',
    matcher: function(url, opts) {
        return(url==='http://localhost:4040/api/login' && opts && opts.body && (opts.body.email !== 'test@mail.com' || opts.body.password !== 'password1'))
    },
    response: {
        message: 'Incorrect Login Information. Please check your email/password.'
    }
})

fetchMock.get({
    name: 'GetUser',
    matcher: function(url, opts) {
        return ((url==='http://localhost:4040/api/users/9' || url==='undefined/api/users/9'))
    },
    response: {
        company: "247-Inc",
        email: "tj@gmail.com",
        id: 9,
        name: "TJ Johnson",
        type: "Patient"
    }
})

fetchMock.get({
    name: 'GetUser',
    matcher: function(url, opts) {
        return ((url==='http://localhost:4040/api/users/10' || url==='undefined/api/users/10'))
    },
    response: {
        message: 'Error retrieving user data'
    }
})


describe('Auth Service - Login', () => {
  it('Successful Auth Login Returns Profile', () => {
    var auth = new AuthService('http://localhost:4040/api');

    return auth.login('test@mail.com', 'password1')
    .then(value => {
        /*Perform our assertions as normal */
        expect(value.id).toEqual(9);
        expect(value.email).toEqual('tj@gmail.com');
    })
  })

 it('Successful Auth Login Returns Profile', () => {
    var auth = new AuthService('http://localhost:4040/api');

    return auth.login('kyle.nobl@gmail.com', 'password1')
    .then(value => {
        /*Perform our assertions as normal */

    })
    .catch(error => {
        expect(error.error).toEqual('Incorrect Login Information. Please check your email/password.')
    })
  })

  it('Failed Auth Login Returns Message', () => {
    var auth = new AuthService('http://localhost:4040/api');

    return auth.login('test@mail.com', 'password2')
    .then(value => {
        /*Perform our assertions as normal */
    })
    .catch(error => {
        expect(error.error).toEqual('Incorrect Login Information. Please check your email/password.');
    })
  })

 it('Empty Credentials Returns Failure', () => {
    var auth = new AuthService('http://localhost:4040/api');

    return auth.login()
    .then(value => {
        /*Perform our assertions as normal */
        expect(value).toEqual(true);
    })
    .catch(e => {
        expect(e.error).toEqual('No Body Included - Login')
    })
  })
})

describe('Auth Service - Logout', () => {
  it('Successful Auth Logout Returns Message', () => {
    var auth = new AuthService('http://localhost:4040/api');

    return auth.logout()
    .then(value => {
        /*Perform our assertions as normal */
        expect(value).toEqual('logged out');
    })
  })
})

describe('Auth Service - _checkStatus', () => {
  it('Successful Auth Logout Returns Message', () => {
    var auth = new AuthService('http://localhost:4040/api');

    expect(() => {
        auth._checkStatus({status: 400, statusText: 'Error'})
    }).toThrowError(Error);
  })
})
