import "isomorphic-fetch";
import Cookies from "universal-cookie";
import currentDomain from "./domain.js";

const cookies = new Cookies();
export default class AuthService {
  constructor(domain) {
    this.domain = domain || `${currentDomain}/api/auth`;
    this.call = this.call.bind(this);
    this.login = this.login.bind(this);
    this.getProfile = this.getProfile.bind(this);
  }

  login(email, password, auto_id = 0) {
    // Get a token
    return this.call(`${this.domain}/login`, {
      method: "POST",
      body: JSON.stringify({
        email,
        password,
        auto_id
      })
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        this.setToken(res.token);
        var userId = res.id;
        return this.call(`${currentDomain}/api/users/${userId}`, {
          method: "GET"
        });
      })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        this.setProfile(res);
        return Promise.resolve(res);
      })
      .catch(error => {
        return Promise.reject(error);
      });
  }

  loggedIn() {
    // Checks if there is a saved token and it's still valid
    const token = this.getToken();
    return !!token;
    //&& !isTokenExpired(token) // handwaiving here
  }

  setProfile(profile) {
    // Saves profile data to localStorage

    localStorage.setItem("profile", JSON.stringify(profile));

    cookies.set("profile", profile, { path: "/" });
  }

  getProfile() {
    // Retrieves the profile data from localStorage
    const profile = localStorage.getItem("profile");
    return profile ? JSON.parse(profile) : {};
  }

  setToken(idToken) {
    // Saves user token to localStorage

    localStorage.setItem("id_token", idToken);
    cookies.set("id_token", idToken, { path: "/" });
  }

  getToken() {
    // Retrieves the user token from localStorage
    return localStorage.getItem("id_token");
  }

  logout() {
    // Clear user token and profile data from localStorage
    localStorage.removeItem("id_token");
    localStorage.removeItem("profile");

    cookies.remove("id_token", { path: "/" });
    cookies.remove("profile", { path: "/" });
    return Promise.resolve("logged out");
  }

  _checkStatus(response) {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      return response;
    } else {
      var error = new Error(response.statusText);
      error.response = response;
      throw error;
    }
  }

  call(url, options) {
    // performs api calls sending the required authentication headers
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };

    if (this.loggedIn()) {
      headers["Authorization"] = "Bearer " + this.getToken();
    }

    return fetch(url, {
      headers,
      ...options
    })
      .then(this._checkStatus)
      .then(response => response.json());
  }
}
