/* eslint-disable jsx-a11y/label-has-for */
import React from "react";
import { css } from "glamor";
import Colors from "../components/NewSkin/Colors";
import GridContainer from "../components/NewSkin/Wrappers/GridContainer";
import Checkbox from '../components/NewSkin/Components/Checkbox'

export default class extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = {
      selectToggle: {},
      data: [],
      filters: {
        branch: [],
        department: [],
        jobgrade: [],
        high_risk: [],
        age_group: []
      },
      branchSelect: {
        id: "",
        name: "branch",
        label: "Business Area",
        type: "select",
        validation: "",
        options: [],
        value: []
      },
      departmentSelect: {
        id: "",
        name: "department",
        label: "Department",
        type: "select",
        validation: "",
        multiple: true,
        options: [],
        value: []
      },
      job_gradeSelect: {
        id: "",
        name: "job_grade",
        label: "Job Grade",
        type: "select",
        validation: "",
        options: [],
        value: []
      },
      high_riskSelect: {
        id: "",
        name: "high_risk",
        label: "Status",
        type: "select",
        validation: "",
        options: [
          {
            name: "High Value",
            value: 1
          },
          {
            name: "High Risk",
            value: 2
          },
          {
            name: "Others",
            value: 0
          }
        ],
        value: []
      },
      age_groupSelect: {
        id: "",
        name: "age_group",
        label: "Age Group",
        type: "select",
        validation: "",
        options: [
          {
            name: "0 - 9",
            value: "0 - 9"
          },
          {
            name: "10 - 19",
            value: "10 - 19"
          },
          {
            name: "20 - 29",
            value: "20 - 29"
          },
          {
            name: "30 - 39",
            value: "30 - 39"
          },
          {
            name: "40 - 49",
            value: "40 - 49"
          },
          {
            name: "50 - 59",
            value: "50 - 59"
          },
          {
            name: "60 - 69",
            value: "60 - 69"
          },
          {
            name: "70 - 79",
            value: "70 - 79"
          }
        ],
        value: []
      },
      filterShow: true,
      activeGraph: "demographics",
      newGraphSelected: false,
      showFilters: false
    };
    this._getData = this._getData.bind(this);
  }

  componentWillMount() {
    this._isMounted = true;
    this._getData();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentDidMount() {
    this._isMounted = true;
    this._getData();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.activeGraph !== this.state.activeGraph) {
      if (this._isMounted) {
        this.setState({
          activeGraph: nextProps.activeGraph,
          newGraphSelected: true
        });
      }

      const branch = this.state.branchSelect;
      const department = this.state.departmentSelect;
      const jobgrade = this.state.job_gradeSelect;
      const high_risk = this.state.high_riskSelect;
      const age_group = this.state.age_groupSelect;

      branch.value = [];
      department.value = [];
      jobgrade.value = [];
      high_risk.value = [];
      age_group.value = [];

      if (this._isMounted)
        this.setState({ branch, department, jobgrade, high_risk, age_group });
    }
  }

  _getData() {
    this.props
      .filters()
      .then(data => {
        // prepare available filter options
        const filters = this.state.filters;
        filters.branch = this._filterList("branch", data);
        filters.department = this._filterList("department", data);
        filters.jobgrade = this._filterList("job_grade", data);

        if (this._isMounted) {
          this.setState(
            {
              filters,
              data
            },
            function () {
              // set options for select dropdown
              this._updateFields();
            }
          );
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _filterList(string, array) {
    const optn = [];
    if (array && array.length > 0) {
      let list = array
        .map(item => item[string])
        .filter((value, index, self) => self.indexOf(value) === index);
      list.sort();
      list = list.forEach(itm => {
        if (itm) {
          optn.push({ name: itm, value: itm });
        }
      });
    }

    return optn;
  }

  _updateFields() {
    const branch = this.state.branchSelect;
    const departments = this.state.departmentSelect;
    const job_grade = this.state.job_gradeSelect;
    const age_group = this.state.age_groupSelect;
    const high_risk = this.state.high_riskSelect;
    branch.options = this.state.filters.branch;
    departments.options = this.state.filters.department;
    job_grade.options = this.state.filters.jobgrade;

    // DEPARTMENT
    if (
      this.props.defaultValue &&
      this.props.defaultValue.department &&
      this.props.defaultValue.department.length > 0
    ) {
      this.props.defaultValue.department.forEach(item => {
        if (departments.value.indexOf(item) === -1) {
          departments.value.push(item);
        }
      });
    }

    // BRANCH
    if (
      this.props.defaultValue &&
      this.props.defaultValue.branch &&
      this.props.defaultValue.branch.length > 0
    ) {
      this.props.defaultValue.branch.forEach(item => {
        if (branch.value.indexOf(item) === -1) {
          branch.value.push(item);
        }
      });
    }

    // AGE GROUP
    if (
      this.props.defaultValue &&
      this.props.defaultValue.age_group &&
      this.props.defaultValue.age_group.length > 0
    ) {
      this.props.defaultValue.age_group.forEach(item => {
        if (age_group.value.indexOf(item) === -1) {
          age_group.value.push(item);
        }
      });
    }

    // HIGH RISK
    if (
      this.props.defaultValue &&
      this.props.defaultValue.high_risk &&
      this.props.defaultValue.high_risk.length > 0
    ) {
      high_risk.value = this.props.defaultValue.high_risk;
      this.props.defaultValue.high_risk.forEach(item => {
        if (high_risk.value.indexOf(item) === -1) {
          high_risk.value.push(item);
        }
      });
    }

    // JOB GRADE
    if (
      this.props.defaultValue &&
      this.props.defaultValue.jobgrade &&
      this.props.defaultValue.jobgrade.length > 0
    ) {
      this.props.defaultValue.jobgrade.forEach(item => {
        if (job_grade.value.indexOf(item) === -1) {
          job_grade.value.push(item);
        }
      });
    }
  }

  _updateFilterList(branch) {
    const data = this.state.data;
    const filters = this.state.filters;
    let filtered = [];
    filtered = data.filter(str => str.branch === branch);
    filters.department = this._filterList(
      "department",
      branch === "" ? data : filtered
    );

    if (this._isMounted) {
      this.setState(
        {
          filters
        },
        function () {
          // set options for select dropdown
          const departmentSelect = this.state.departmentSelect;
          departmentSelect.options = this.state.filters.department;

          //set default option selected
          departmentSelect.value = [];

          if (this._isMounted) this.setState({ departmentSelect });
        }
      );
    }
  }

  _toggleFilter() {
    if (this.state.filterShow) {
      if (this._isMounted) this.setState({ filterShow: false });
    } else {
      if (this._isMounted) this.setState({ filterShow: true });
    }
  }

  _displayDropdown(input) {
    if (
      this.props.filterfor === "demographicsTop" ||
      this.props.filterfor === "demographics"
    ) {
      if (input.name !== "age_group") {
        return this._multiFilter(input);
      }
    } else {
      return this._multiFilter(input);
    }
  }

  _onChange(e) {
    const branch = this.state.branchSelect;
    const department = this.state.departmentSelect;
    const jobgrade = this.state.job_gradeSelect;
    const high_risk = this.state.high_riskSelect;
    const age_group = this.state.age_groupSelect;

    if (this._isMounted) this.setState({ newGraphSelected: false });

    if (e.target.name === "branch") {
      //  update department list
      // this._updateFilterList(e.target.value);
      // branch.value = e.target.value;
      // department.value = [];

      if (e.target.value !== "") {
        if (branch.value.indexOf(e.target.value) === -1) {
          branch.value.push(e.target.value);
        } else {
          branch.value.splice(department.value.indexOf(e.target.value), 1);
        }
      } else {
        branch.value = [];
      }
      if (this._isMounted) this.setState({ branch });
    } else if (e.target.name === "department") {
      if (e.target.value !== "") {
        if (department.value.indexOf(e.target.value) === -1) {
          department.value.push(e.target.value);
        } else {
          department.value.splice(department.value.indexOf(e.target.value), 1);
        }
      } else {
        department.value = [];
      }
      if (this._isMounted) this.setState({ department });
    } else if (e.target.name === "job_grade") {
      if (e.target.value !== "") {
        if (jobgrade.value.indexOf(e.target.value) === -1) {
          jobgrade.value.push(e.target.value);
        } else {
          jobgrade.value.splice(jobgrade.value.indexOf(e.target.value), 1);
        }
      } else {
        jobgrade.value = [];
      }
      if (this._isMounted) this.setState({ jobgrade });
    } else if (e.target.name === "high_risk") {
      const val = parseInt(e.target.value);
      if (e.target.value !== "") {
        if (high_risk.value.indexOf(val) === -1) {
          high_risk.value.push(val);
        } else {
          high_risk.value.splice(high_risk.value.indexOf(val), 1);
        }
      } else {
        high_risk.value = [];
      }
      if (this._isMounted) this.setState({ high_risk });
    } else if (e.target.name === "age_group") {
      if (e.target.value !== "") {
        if (age_group.value.indexOf(e.target.value) === -1) {
          age_group.value.push(e.target.value);
        } else {
          age_group.value.splice(high_risk.value.indexOf(e.target.value), 1);
        }
      } else {
        age_group.value = [];
      }
      if (this._isMounted) this.setState({ age_group });
    }

    const filters = {
      branch: branch.value,
      department: department.value,
      jobgrade: jobgrade.value,
      high_risk: high_risk.value,
      age_group: age_group.value
    };
    //  filter graph data
    if (this.props.filterfor) {
      this.props.filterData(
        e.target.name,
        e.target.value,
        this.props.filterfor,
        filters
      );
    } else {
      this.props.filterData(e.target.name, e.target.value, filters);
    }
  }

  _displayListOptions(input) {
    const html = [];
    const options = input.options;
    for (let i = 0; i < options.length; i++) {
      if (options[i].label === input.value) {
        html.push(
          <option key={i} value={input.value}>
            {options[i].name}
          </option>
        );
      } else {
        html.push(
          <option key={i} value={options[i].label}>
            {options[i].name}
          </option>
        );
      }
    }

    return html;
  }

  _toggleSelection(filtername) {
    let { selectToggle } = this.state;
    selectToggle = {}
    selectToggle[filtername] = !this.state.selectToggle[filtername];
    if (this._isMounted) {
      this.setState({
        selectToggle
      });
    }
  }

  _multiFilter(input) {
    const list = input.options.map((item, i) => {
      let isChecked = false;
      if (input.value.indexOf(item.value) !== -1) {
        isChecked = true;
      } else {
        isChecked = false;
      }

      return (
        <Checkbox
          id={`${item.value}_${this.props.filterfor}_${i}`}
          name={input.name}
          value={item.value}
          onChange={event => this._onChange(event)}
          label={item.name}
          checked={isChecked}
          smallBox
        />
      );
    });

    const heightValue = {
      maxHeight: this.state.selectToggle[input.name] ? "fit-content" : 0,
      opacity: this.state.selectToggle[input.name] ? 1 : 0,
      padding: this.state.selectToggle[input.name] ? '5px 10px' : "0 5px 0 100px"
    }


    return (
      <div
        key={input.name}
        style={{
          flex: 1,
          display: "flex",
          alignSelf: "center",
          alignItems: "flex-start",
          //width: "%",
          margin: 0,
          textAlign: 'left'
        }}
      >
        <div className={filterLabel}>{input.label}</div>

        <div className={departmentInputStyle}>
          <div
            className={trigger}
            onClick={
              input.options.length > 0
                ? () => this._toggleSelection(input.name)
                : null
            }
          >
            {input.value.length === 1
              ? `${input.value.length} Item`
              : input.value.length >= 2
                ? `${input.value.length} Items`
                : `All`}
          </div>

          {
            this.state.selectToggle[input.name] &&
            <div className={itemsWrapper} id="items-wrapper" style={{ ...styles.toggleOffStyle, ...heightValue }}>{list}</div>
          }
        </div>
      </div>
    );
  }

  _departmentFilters(input) {
    const department = input;
    const list = input.options.map((item, i) => {
      let isChecked = false;
      if (department.value.indexOf(item.value) !== -1) {
        isChecked = true;
      } else {
        isChecked = false;
      }
      return (
        <li
          key={i}
          style={{ alignSelf: "left", listStyle: "none", textAlign: 'left' }}
        >
          <input
            name="department"
            value={item.value}
            checked={isChecked}
            onChange={event => this._onChange(event)}
            type="checkbox"
          />{" "}
          <label>{item.name}</label>
        </li>
      );
    });
    const heightValue = {
      maxHeight: this.state.selectToggle[input.name] ? "100%" : 0,
      opacity: this.state.selectToggle[input.name] ? 1 : 0,
      padding: this.state.selectToggle[input.name]
        ? "10px 5px 0 5px"
        : "0 5px 0 100px"
    };
    return (
      <div
        id="department"
        key="df"
        style={{
          flex: 1,
          display: "flex",
          alighSelf: "center",
          alignItems: "flex-start",
          //width: "%",
          margin: 0,
          textAlign: 'left'
        }}
      >
        <div className={filterLabel}>Department</div>
        <div className={departmentInputStyle}>
          <div
            style={{
              flex: 1,
              border: "1px solid rgb(169, 169, 169)",
              textAlign: "left",
              padding: "3px 0 3px 10px"
            }}
            onClick={() => this._toggleSelection()}
          >
            {department.value.length === 1
              ? `${department.value.length} Item`
              : department.value.length >= 2
                ? `${department.value.length} Items`
                : `All`}
          </div>
          <div style={{ ...styles.toggleOffStyle, ...heightValue }}>{list}</div>
        </div>
      </div>
    );
  }

  _showFilters = () => {
    this.setState({
      showFilters: !this.state.showFilters
    });
  }

  render() {
    const currentFields = [
      this.state.branchSelect,
      this.state.departmentSelect,
      this.state.job_gradeSelect,
      this.state.high_riskSelect
    ];
    if (this.state.activeGraph !== "demographics") {
      currentFields.push(this.state.age_groupSelect);
    }

    const flds = currentFields.map(item => this._displayDropdown(item));

    const showFilters = this.state.showFilters;
    const withBackground = this.props.withBackground;

    return (
      <section id={withBackground ? 'with-background' : 'no-background'} className={wrapper(withBackground)}>
        <button
          onClick={this._showFilters}
          className={showFilters ? filterTriggerActive(withBackground) : filterTrigger(withBackground)}>
          <GridContainer columnSize="1fr 20px">
            <span>{showFilters ? 'Hide' : 'Show'} Filters</span>

            <svg
              className={svgStyle(withBackground, showFilters)}
              width="15"
              height="15"
              viewBox="0 0 400 400"
            >
              <use x="0" y="0" xlinkHref="/static/icons/filter_icon.svg#filter" />
            </svg>
          </GridContainer>
        </button>

        {
          showFilters &&
          <section className={filterWrapper(withBackground)}>
            <div
              className={filterDiv}
              id="filter"
            >
              <div className={labelStyle}>
                {this.props.label ? this.props.label : "filter by"}
                {/* <span className={toggleFilter} onClick={this._toggleFilter.bind(this)}>-</span> */}
              </div>

              <div
                className={`filtersList ${filtersList} ${
                  this.state.filterShow ? "" : removePadding
                  }`}
              >
                {this.state.filterShow ? flds : ""}
              </div>
            </div>
          </section>
        }

      </section>
    );

  }
}

const trigger = css({
  cursor: 'pointer',
  flex: 1,
  border: '1px solid rgba(0,0,0,.05)',
  textAlign: 'left',
  padding: '3px 0 3px 10px',
  borderRadius: 3,
  transition: '200ms ease',
  ':hover': {
    borderColor: Colors.blueDarkAccent
  }
})

const itemsWrapper = css({
  position: 'absolute',
  width: '100%',
  bottom: 0,
  left: 0,
  transform: 'translateY(100%)',
  background: '#fff',
  zIndex: 1001,
  border: '1px solid rgba(0,0,0,.05)'
})

const svgStyle = (bg, showFilters) => css({
  transform: showFilters ? 'scaleX(-1)' : 'scaleX(1)',
  fill: bg ? showFilters ? '#fff' : '#777' : '#fff',
  marginLeft: 10
})

const wrapper = (withBG) => css({
  position: 'relative',
  width: '100% !important',
  textAlign: 'right !important',
  padding: withBG ? 10 : 0
})

const filterTrigger = (bg) => css({
  width: 166,
  borderStyle: 'none',
  padding: '8px 15px',
  borderRadius: bg ? 5 : 0,
  background: bg ? '#eee8' : 'rgba(0,0,0,0)',
  color: bg ? '#777' : '#fff',
  cursor: 'pointer',
  transition: bg ? '150ms ease' : null,
  opacity: .8,
  borderLeft: bg ? null : '1px solid #ffffff73',
  fontSize: 'smaller',
  '> svg': {
    fill: bg ? '#777' : '#fff',
    marginLeft: 10
  },
  ':focus': {
    outline: 'none'
  },
  ':hover': {
    opacity: 1,
    background: bg ? '#eee' : null,
    color: bg ? '#000' : null,
  }
})

const filterTriggerActive = (bg) => css(filterTrigger(bg), {
  color: '#fff !important',
  background: bg ? Colors.skyblue : 'rgba(0,0,0,.03)',
  opacity: 1,
  ':hover': {
    color: '#fff',
    background: bg ? Colors.skyblue : 'rgba(0,0,0,.03)',
    opacity: 1,
  }
})

const filterWrapper = (withBG) => css({
  position: 'absolute',
  top: withBG ? 10 : 0,
  right: withBG ? -175 : -185,
  transform: 'translateX(-100%)',
  background: '#fff',
  boxShadow: '0 8px 32px rgba(0,0,0,.18)',
  zIndex: 1001,
  borderRadius: 5,
})

let removePadding = css({
  padding: "0 !important"
});

let filterDiv = css({
  fontSize: "x-small",
  // border: "1px solid #364563",
  width: "100%"
});

let filtersList = css({
  padding: "10px 15px"
  // margin: "5px 0"
});

let labelStyle = css({
  background: "#364563",
  color: "#fff",
  padding: "5px 15px",
  textTransform: "uppercase",
  //fontSize: '12px',
  fontWeight: "bold",
  textAlign: 'left',
  borderRadius: '5px 5px 0 0'
});

let inputStyle = css({
  width: "200px",
  padding: "3px 0 3px 10px",
  margin: "3px 0 3px 10px",
  fontSize: ".8rem",
  backgroundImage: `url(static/icons/dropdownIcon.svg)`,
  backgroundPosition: "right 8px top 8px",
  backgroundSize: 10,
  backgroundRepeat: "no-repeat",
  WebkitAppearance: "none",
  MozAppearance: "none",
  textOverflow: ""
});

const departmentInputStyle = css({
  width: "200px",
  margin: "3px 0 3px 10px",
  fontSize: ".8rem",
  background: "#fff",
  backgroundImage: `url(static/icons/dropdownIcon.svg)`,
  backgroundPosition: "right 10px top 13px",
  backgroundSize: 10,
  backgroundRepeat: "no-repeat",
  position: 'relative'
});

let filterLabel = css({
  display: "inline-block",
  width: "120px",
  //fontSize: '12px',
  fontWeight: "bold",
  fontSize: ".8rem"
});

let toggleFilter = css({
  cursor: "pointer",
  //fontSize: '18px',
  float: "right",
  ":hover": {
    fontWeight: "bold"
  }
});

const styles = {
  toggleOffStyle: {
    opacity: 0,
    maxHeight: 0,
    padding: '5px 10px',
    // overflow: "hidden",
    boxShadow: '0 8px 15px rgba(0,0,0,.2)',
    transition: "all 200ms ease"
  }
};
// let toggleOffStyle = css({
//   display: "none",
//   width: 0,
//   background: "red",
//   transition: "width 2s ease-in-out"
// });

// let toggleOnStyle = css({
//   display: "block",
//   width: "auto"
// });
