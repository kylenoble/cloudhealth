import AuthService from '../utils/authService.js'
import 'isomorphic-fetch'
import queryString from 'query-string'
import currentDomain from './domain.js'

const auth = new AuthService()

export default class AnalysisService {
  constructor(domain) {
    this.domain = domain || `${currentDomain}/api/analysis`
    this.call = this.call.bind(this)
    this.getProfile = this.getProfile.bind(this)
    //this.returnAnswers = this.returnAnswers.bind(this)
    //this.filterAnswer = this.filterAnswer.bind(this)
    this.getToken = this.getToken.bind(this)
    //this.getQuery = this.getQuery.bind(this)
  }

get(qry) {
    const profile = this.getProfile()
    return this.call(`${this.domain}/${qry}`, {
      method: 'GET'
    }).then(res => {
      if (!res || res.message || res.status >= 400) {
        return Promise.reject({error: res.message});
      }
      console.log('getting analyis done')
      return Promise.resolve(res)
    }).catch(e => {
      console.log(e)
      return Promise.reject(e)
    })
  }

update(data){
  return this.call(`${this.domain}/update`,{
    method: 'PUT',
    body: JSON.stringify(data)
  })
  .then((res)=>{
    return Promise.resolve(res)
  })
  .catch((err)=>{
    return Promise.reject(err)
  })
}

save(data){
  return this.call(`${this.domain}/update`,{
    method: 'POST',
    body: JSON.stringify(data)
  })
  .then((res)=>{
    return Promise.resolve(res)
  })
  .catch((err)=>{
    return Promise.reject(err)
  })
}

// flexible queryString

  // getQuery(qry) {
  //   return this.call(`${this.domain}/qry`, {
  //     method: 'PUT',
  //     body: JSON.stringify({query: qry})
  //   }).then(res => {
  //     if (!res || res.message || res.status >= 400) {
  //       return Promise.reject({error: res.message});
  //     }
  //
  //     return Promise.resolve(res)
  //   }).catch(e => {
  //     return Promise.reject(e)
  //   })
  // }

  call(url, options){
    // performs api calls sending the required authentication headers
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }

    if (this.loggedIn()){
      headers['Authorization'] = 'Bearer ' + this.getToken()
    }

    return fetch(url, {
      headers,
      ...options
    })
    .then(this._checkStatus)
    .then(response => response.json())
  }

   _checkStatus(response) {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      return response
    } else {
      var error = new Error(response.statusText)
      error.response = response
      throw error
    }
  }

  loggedIn(){
    // Checks if there is a saved token and it's still valid
    const token = this.getToken()
    return !!token
    //&& !isTokenExpired(token) // handwaiving here
  }

  getProfile(){
    // Retrieves the profile data from localStorage
    const profile = localStorage.getItem('profile')
    return profile ? JSON.parse(profile) : {}
  }

  getToken(){
    // Retrieves the user token from localStorage
    return localStorage.getItem('id_token')
  }
}
