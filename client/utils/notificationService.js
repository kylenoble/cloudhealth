import currentDomain from "./domain.js";

export default class NotificationService {
  constructor(domain) {
    this.domain = domain || `${currentDomain}/api/notifications`;
    this.call = this.call.bind(this);
  }

  getAdmin(id) {
    return this.call(`${this.domain}/admin/${id}`, {
      method: "GET"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  updateAdmin(data) {
    return this.call(`${this.domain}/admin/update`, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        return Promise.resolve(true);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  call(url, options) {
    // performs api calls sending the required authentication headers
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };

    if (this.loggedIn()) {
      headers["Authorization"] = "Bearer " + this.getToken();
    }

    return fetch(url, {
      headers,
      ...options
    })
      .then(this._checkStatus)
      .then(response => response.json());
  }

  _checkStatus(response) {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      return response;
    } else {
      var error = new Error(response.statusText);
      error.response = response;
      throw error;
    }
  }

  loggedIn() {
    // Checks if there is a saved token and it's still valid
    const token = this.getToken();
    return !!token;
    //&& !isTokenExpired(token) // handwaiving here
  }

  getToken() {
    // Retrieves the user token from localStorage
    return localStorage.getItem("id_token");
  }

  sendReportNotification(emailDetails) {
    return this.call(`${this.domain}/sendReportNotification`, {
      method: "POST",
      body: JSON.stringify(emailDetails)
    })
      .then(res => {
        console.log("Email Servicex");
      })
      .catch(error => {
        console.log("Email Service errorXS");
        console.log(error);
        return Promise.reject(error);
      });
  }
}
