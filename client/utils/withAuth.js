import React, { Component } from "react";
import Router from "next/router";
import Loading from "../components/NewSkin/Components/Loading";

import AuthService from "./authService.js";
import "isomorphic-fetch";

export default function withAuth(AuthComponent) {
  const Auth = new AuthService();
  return class Authenticated extends Component {
    static async getInitialProps(ctx) {
      // Ensures material-ui renders the correct css prefixes server-side
      let userAgent;
      if (process.browser) {
        userAgent = navigator.userAgent;
      } else {
        userAgent = ctx.req.headers["user-agent"];
      }

      // Check if Page has a `getInitialProps`; if so, call it.
      const pageProps =
        AuthComponent.getInitialProps &&
        (await AuthComponent.getInitialProps(ctx));
      // Return props.
      return { ...pageProps, userAgent };
    }
    constructor(props) {
      super(props);

      this._showLoading = this._showLoading.bind(this);
      this.state = {
        isLoading: true
      };
    }

    componentDidMount() {
      if (!Auth.loggedIn()) {
        Router.push("/login");
      }
      this.setState({ isLoading: false });
    }

    render() {
      return <div>{this._showLoading()}</div>;
    }

    _showLoading() {
      if (this.state.isLoading) {
        return <Loading />;
      } else {
        return <AuthComponent {...this.props} auth={Auth} />;
      }
    }
  };
}

const styles = {
  loadingContainer: {
    fontFamily: '"Roboto",sans-serif',
    minHeight: "700px",
    display: "flex",
    WebkitFlexDirection: "column",
    flexDirection: "column",
    WebkitAlignItems: "center",
    alignItems: "center",
    WebkitJustifyContent: "center",
    justifyContent: "center",
    backgroundSize: "cover",
    overflow: "hidden"
  }
};
