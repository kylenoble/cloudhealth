
import currentDomain from './domain.js';

export default class CompanyService {
  constructor(domain) {
    this.domain = domain || `${currentDomain}/api/company`;
    this.call = this.call.bind(this)

  }

  list() {
      return this.call(`${this.domain}`, {
        method: 'GET'
      }).then(res => {
        return Promise.resolve(res)
      }).catch(e => {
        return Promise.reject(e)
      })
    }

    get(str, userid=null) {
        return this.call(`${this.domain}/${str}/${userid}`, {
          method: 'GET'
        }).then(res => {
          return Promise.resolve(res)
        }).catch(e => {
          return Promise.reject(e)
        })
      }

      call(url, options){
          // performs api calls sending the required authentication headers
        const headers = {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }

        if (this.loggedIn()){
          headers['Authorization'] = 'Bearer ' + this.getToken()
        }

        return fetch(url, {
          headers,
          ...options
        })
        .then(this._checkStatus)
        .then(response => response.json())
      }

       _checkStatus(response) {
        // raises an error in case response status is not a success
        if (response.status >= 200 && response.status < 300) {
          return response
        } else {
          var error = new Error(response.statusText)
          error.response = response
          throw error
        }
      }

      getProfile() {
        // Retrieves the profile data from localStorage
        const profile = localStorage.getItem('profile')
        return profile ? JSON.parse(profile) : {}
      }

      setProfile(profile){
        // Saves profile data to localStorage
        localStorage.setItem('profile', JSON.stringify(profile))
      }

      loggedIn(){
        // Checks if there is a saved token and it's still valid
        const token = this.getToken()
        return !!token
        //&& !isTokenExpired(token) // handwaiving here
      }

      getToken(){
        // Retrieves the user token from localStorage
        return localStorage.getItem('id_token')
      }
      }
