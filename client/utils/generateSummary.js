export default class GenerateSummary {
    constructor(data, category, gender = 'female') {
        this.data = data;
        this.category = category;
        this.gender = gender;
    }

    generateSummary() {
        console.log('CATEGORY', this.category)
        switch (this.category) {
            case 'readiness':
                return this.generateReadiness();
            case 'downstreamHealth':
                return this.generateDownstreamHealth();
            case 'nutrition':
                return this.generateNutrition();
            case 'sleep':
                return this.generateSleep();
            case 'exercise':
                return this.generateExercise();
            case 'stress':
                return this.generateStress();
            case 'body':
                return this.generateBody();
            case 'detox':
                return this.generateDetox();
            case 'msq':
                return this.generateMSQ();
            default:
                return 'Error';
        }
    }

    generateReadiness() {
        let total = 0;
        for (let item of this.data) {
            const itemNum = Number(item.value);
            total += itemNum;
        }

        return total;
    }

    generateDownstreamHealth() {
        if (this.data !== 'none') {
            return 'high risk';
        }
    }


    generateNutrition() {
        let total = 0;
        for (let item of this.data) {
            switch (item.name) {
                case 'specialDiet':
                    let currentValue = 1;

                    if (item.value.toLowerCase() === 'yes') {
                        currentValue = 5;
                    }

                    total += currentValue * 0.2;
                    break;
                case 'foodAllergies':
                    currentValue = 1;

                    if (item.value.toLowerCase() === 'yes') {
                        currentValue = 5;
                    }

                    total += currentValue * 0.2;
                    break;
                case 'usualMeal':
                    const itemCount = (item.value.indexOf(',') > -1 ? item.value.split(',').length : 1);
                    if (itemCount == 1 && item.value.toLowerCase() === 'none') {

                    }else{
                      total += (itemCount / 2) * 0.2;
                    }
                    break;
                default:
                    currentValue = 1;
                    if (item.name === 'meatConsumedPercent') {
                        if (parseFloat(item.value) > 30) {
                            currentValue = 5;
                        }
                        total += currentValue * 0.2;
                        break;
                    } else if (item.name === 'starchConsumedPercent') {
                        if (parseFloat(item.value) > 30) {
                            currentValue = 5;
                        }
                        total += currentValue * 0.2
                        break;
                    }
            }
        }
        return total.toFixed(3);
    }

    generateSleep() {
        let total = 0;
        for (let item of this.data) {
            switch (item.name) {
                case 'regularSleep':
                    let currentValue = 1;
                    if (item.value.toLowerCase() === 'day') {
                        currentValue = 5;
                    }

                    total += currentValue * 0.3125;
                    break;
                case 'averageSleep':
                    currentValue = 1;
                    if (item.value === '<6') {
                        currentValue = 5;
                    } else if (item.value === '6-8') {
                        currentValue = 3;
                    } else if (item.value === '>10') {
                        currentValue = 3;
                    }

                    total += currentValue * 0.125;
                    break;
                case 'troubleFallingAsleep':
                    currentValue = 1;
                    if (item.value.toLowerCase() === 'yes') {
                        currentValue = 5;
                    }

                    total += currentValue * 0.125;
                    break;
                case 'restedFeeling':
                    currentValue = 1;
                    if (item.value.toLowerCase() === 'no') {
                        currentValue = 5;
                    }

                    total += currentValue * 0.125;
                    break;
                case 'sleepingAids':
                    currentValue = 1;
                    if (item.value.toLowerCase() === 'yes') {
                        currentValue = 5;
                    }

                    total += currentValue * 0.3125;
                    break;
                default:
                    break;
            }
        }
        return total;
    }

    generateExercise() {
        let total = 0;
        for (let item of this.data) {
            switch (item.name) {
                case 'weeklyExercise':
                   let currentValue = 1;
                    if (item.value.toLowerCase() === 'no') {
                        currentValue = 5;
                    }

                    total += currentValue * 0.2;
                    break;
                case 'limitActivity':
                    currentValue = 1;
                    if (item.value.toLowerCase() === 'yes') {
                        currentValue = 5;
                    }

                    total += currentValue * 0.2;
                    break;
                case 'postActivitySoreness':
                   currentValue = 1;
                    if (item.value.toLowerCase() === 'yes') {
                        currentValue = 5;
                    }

                    total += currentValue * 0.2;
                    break;
                case 'workActivity':
                    currentValue = 1;
                    if (item.value === 'Mostly sitting or standing') {
                        currentValue = 5;
                    }

                    total += currentValue * 0.2;
                    break;
                case 'sittingHours':
                    currentValue = 1;
                    if (item.value === 'About 4 hours or more') {
                        currentValue = 5;
                    } else if (item.value === 'About 1-3 hours') {
                        currentValue = 3;
                    }

                    total += currentValue * 0.2;
                    break;
                default:
                    break;
            }
        }
        return total;
    }

    generateBodyScore() {
        let total = 0;

        const bmiScore = this.generateBMI();
        const whrScore = this.generateWeightHip();

        if (bmiScore > 25 || bmiScore < 18.5) {
            total += 2.5;
        } else {
            total += 0.5;
        }

        if (this.gender === 'Male') {
            if (whrScore < 0.95) {
                total += 0.5;
            } else if (whrScore >= 0.96 && whrScore <= 1) {
                total += 1.5;
            } else {
                total += 2.5;
            }
        } else {
            if (whrScore < 0.81) {
                total += 0.5;
            } else if (whrScore >= 0.81 && whrScore <= 0.85) {
                total += 1.5;
            } else {
                total += 2.5;
            }
        }

        return total;
    }

    inToCm(input) {
        return input * 2.54;
    }

    lbToKg(input) {
        return input * 0.45359237;
    }

    generateWeightHip() {
        let waist = this.inToCm(Number(this.data.waist.value));
        let hip = this.inToCm(Number(this.data.hip.value));
        let value = waist / hip;
        return Math.round(value * 100) / 100;
    }

    generateBMI() {
        let height = this.inToCm(Number(this.data.height.value)) / 100;
        let weight = this.lbToKg(Number(this.data.weight.value));
        let value = weight / (height * height);
        return Math.round(value * 100) / 100;
    }

    generateStress() {
        let total = 0;
        for (let item of this.data) {
            switch (item.name) {
                case 'counseling':
                    let currentValue = 1;
                    if (item.value.toLowerCase() === 'yes') {
                        currentValue = 5;
                    }

                    total += currentValue * 0.25;
                    break;
                case 'therapy':
                    currentValue = 1;
                    if (item.value.toLowerCase() === 'yes') {
                        currentValue = 5;
                    }

                    total += currentValue * 0.25;
                    break;
                default:
                    if (item.value <= 2) {
                        total += (1/6) / 2;
                    } else if (item.value > 2 && item.value <= 4) {
                        total += (2/6) / 2;
                    } else if (item.value > 4 && item.value <= 6) {
                        total += (3/6) / 2;
                    } else if (item.value > 6 && item.value <= 8) {
                        total += (4/6) / 2;
                    } else if (item.value > 8) {
                        total += (5/6) / 2;
                    }
                    break;
            }
        }
        return +total.toFixed(2);
    }

    generateDetox() {
        let total = 0;
        for (let item of this.data) {
            switch (item.name) {
                case 'prescriptionDrugs':
                    if (item.value !== 0) {
                        total += 1;
                    }
                    break;
                case 'prescriptionScenarios':
                    if (item.value === 'Experience side effects, drug(s) is (are) efficacious at lowered dose(s)') {
                        total += 3;
                    } else if (item.value === 'Experience side effects, drug(s) is (are) efficacious at usual dose(s)') {
                        total += 2;
                    } else if (item.value === 'Experience no side effects, drug(s) is (are) usually not efficacious') {
                        total += 2;
                    }
                    break;
                case 'caffeine':
                    if (item.value.toLowerCase() === 'yes') {
                        total += 1;
                    }
                    break;
                case 'illAlcohol':
                    if (item.value.toLowerCase() === 'yes') {
                        total += 1;
                    }
                    break;
                case 'overTheCounter':
                    if (item.value.toLowerCase() !== 'no') {
                        total += 2;
                    }
                    break;
                case 'tobacco':
                    if (item.value.toLowerCase() === 'yes') {
                        total += 2;
                    }
                    break;
                case 'odors':
                    if (item.value.toLowerCase() === 'yes') {
                        total += 1;
                    }
                    break;
                case 'dependent':
                    if (item.value.toLowerCase() === 'yes') {
                        total += 2;
                    }
                    break;
                default:
                    break;
            }
        }

        if (total <= 4) {
            return 1;
        } else if ( total > 4 && total <= 9) {
            return 3;
        } else {
            return 5;
        }
    }

    generateMSQ() {
        let total = 0;

        for (let item in this.data) {
            
            if (item !== 'formError' && item !== 'symptomName' && item !== 'groups') {
                
                let itemObject = this.data[item];
                total += Number(itemObject.value || 0);                
            }
        }

        return total;
    }

};
