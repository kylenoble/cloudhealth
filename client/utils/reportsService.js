import currentDomain from "./domain.js";

export default class ReportsService {
  constructor(domain) {
    this.domain = domain || `${currentDomain}/api/reports`;
    this.call = this.call.bind(this);
  }

  saveReport(data) {
    return this.call(`${this.domain}/update`, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        return Promise.resolve(true);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  deleteReport(data) {
    return this.call(`${this.domain}/delete`, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        return Promise.resolve(true);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  updateReport(data) {
    return this.call(`${this.domain}/update`, {
      method: "PUT",
      body: JSON.stringify(data)
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }

        return Promise.resolve(true);
      })
      .catch(e => {
        console.log(e);
        return Promise.reject(e);
      });
  }

  getReport(data) {
    let url = `${this.domain}/get/`;
    return this.call(url, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  getReportDefault() {
    return this.call(`${this.domain}/defaults`, {
      method: "GET"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  getCompliance(company_name) {
    return this.call(`${this.domain}/compliance/${company_name}`, {
      method: "GET"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  getLeadDoctor(id) {
    return this.call(`${this.domain}/get_lead_doctor/${id}`, {
      method: "GET"
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
        return Promise.resolve(e);
      });
  }

  call(url, options) {
    // performs api calls sending the required authentication headers
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };

    if (this.loggedIn()) {
      headers["Authorization"] = "Bearer " + this.getToken();
    }

    return fetch(url, {
      headers,
      ...options
    })
      .then(this._checkStatus)
      .then(response => response.json());
  }

  _checkStatus(response) {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      return response;
    } else {
      var error = new Error(response.statusText);
      error.response = response;
      throw error;
    }
  }

  loggedIn() {
    // Checks if there is a saved token and it's still valid
    const token = this.getToken();
    return !!token;
    //&& !isTokenExpired(token) // handwaiving here
  }

  getToken() {
    // Retrieves the user token from localStorage
    return localStorage.getItem("id_token");
  }
}
