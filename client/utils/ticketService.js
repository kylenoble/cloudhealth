import currentDomain from './domain.js'

export default class TicketService {
    constructor(domain) {
        this.domain = domain || `${currentDomain}/api/ticket`
        this.create = this.create.bind(this)
        this.call = this.call.bind(this)
        this.loggedIn = this.loggedIn.bind(this)
        this.getToken = this.getToken.bind(this)
        this._checkStatus = this._checkStatus.bind(this)
        this.getReplies = this.getReplies.bind(this)
    }

    uploadAttachment(file) {        
        return this.call(`${this.domain}/uploadAttachment`, {
            method: 'POST',
            body: JSON.stringify(file)
        })
            .then(res => {
                if (!res || res.message || res.status >= 400) {
                    return Promise.reject({ error: res.message });
                }
                return Promise.resolve(true)
            })
            .catch(e => {
                return Promise.reject(e)
            })
    }

    removeAttachment(file) {
        return this.call(`${this.domain}/removeAttachment/${file}`, {
            method: 'GET'
        })
            .then(res => {
                if (!res || res.message || res.status >= 400) {
                    return Promise.reject({ error: res.message });
                }
                return Promise.resolve(true)
            })
            .catch(e => {
                return Promise.reject(e)
            })
    }

    // get all the replies of tickets
    getReplies(tnum) {
        return this.call(`${this.domain}/getTicketReplies/${tnum}`, {
            method: 'GET'
        }).then(res => {
            return Promise.resolve(res)
        }).catch(e => {
            return Promise.reject(e)
        })
    }

    // get active companies
    getCompanies() {
        return this.call(`${this.domain}/getCompanies/`, {
            method: 'GET'
        }).then(res => {
            return Promise.resolve(res)
        }).catch(e => {
            return Promise.reject(e)
        })
    }

    // get ticket by ticket_number
    getTickets(tnum) {
        return this.call(`${this.domain}/getTickets/${tnum}`, {
            method: 'GET'
        }).then(res => {
            return Promise.resolve(res)
        }).catch(e => {
            return Promise.reject(e)
        })
    }

    getLatestTicket() {
        return this.call(`${this.domain}/getLatestTicket`, {
            method: 'GET'
        }).then(res => {
            return Promise.resolve(res)
        }).catch(e => {
            return Promise.reject(e)
        })
    }

    create(ticketData) {
        return this.call(`${this.domain}`, {
            method: 'POST',
            body: JSON.stringify(ticketData)
        })
            .then(res => {
                if (!res || res.message || res.status >= 400) {
                    return Promise.reject({ error: res.message });
                }
                return Promise.resolve(true)
            })
            .catch(e => {
                return Promise.reject(e)
            })
    }

    call(url, options) {
        // performs api calls sending the required authentication headers
        const headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

        if (this.loggedIn()) {
            headers['Authorization'] = 'Bearer ' + this.getToken()
        }
        


        return fetch(url, {
            headers,
            ...options
        })
            .then(this._checkStatus)
            .then(response => response.json())
    }

    getToken() {
        // Retrieves the user token from localStorage
        return localStorage.getItem('id_token')
    }

    loggedIn() {
        // Checks if there is a saved token and it's still valid
        const token = this.getToken()
        return !!token
        //&& !isTokenExpired(token) // handwaiving here
    }

    _checkStatus(response) {
        // raises an error in case response status is not a success
        if (response.status >= 200 && response.status < 300) {
            return response
        } else {
            var error = new Error(response.statusText)
            error.response = response
            throw error
        }
    }

    sendEmail(email, name, ticket_number, concern_title) {
        return this.call(`${this.domain}/ticketEmail`, {
            method: 'POST',
            body: JSON.stringify({ email, name, ticket_number, concern_title })
        }).then(res => {
            console.log("Email Servicex");
            console.log(res);
        }).catch(error => {
            console.log("Email Service errorXS");
            console.log(error);
            return Promise.reject(error);
        })
    }

}