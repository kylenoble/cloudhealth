import currentDomain from "./domain.js";

export default class ReportsService {
  constructor(domain) {
    this.domain = domain || `${currentDomain}/api/subscriptions`;
    this.call = this.call.bind(this);
  }

  getProgram(data) {
    return this.call(`${this.domain}/getProgram`, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log("eeeeeee".e);
        return Promise.reject(e);
      });
  }

  get(data) {
    return this.call(`${this.domain}`, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  update(data) {
    return this.call(`${this.domain}/update`, {
      method: "POST",
      body: JSON.stringify(data)
    })
      .then(res => {
        if (!res || res.message || res.status >= 400) {
          return Promise.reject({ error: res.message });
        }
        return Promise.resolve(true);
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  call(url, options) {
    // performs api calls sending the required authentication headers
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };

    if (this.loggedIn()) {
      headers["Authorization"] = "Bearer " + this.getToken();
    }

    return fetch(url, {
      headers,
      ...options
    })
      .then(this._checkStatus)
      .then(response => response.json());
  }

  _checkStatus(response) {
    // raises an error in case response status is not a success
    if (response.status >= 200 && response.status < 300) {
      return response;
    } else {
      var error = new Error(response.statusText);
      error.response = response;
      throw error;
    }
  }

  loggedIn() {
    // Checks if there is a saved token and it's still valid
    const token = this.getToken();
    return !!token;
    //&& !isTokenExpired(token) // handwaiving here
  }

  getToken() {
    // Retrieves the user token from localStorage
    return localStorage.getItem("id_token");
  }
}
