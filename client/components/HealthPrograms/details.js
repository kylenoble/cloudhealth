/* eslint-disable prefer-const */
import React from "react";
import { css } from "glamor";
import CONSTANTS from "../../constants";
import Card from "../NewSkin/Wrappers/Card";
import { ProgramColors, SVGIcons, labelClass, mainFont, CHAIcons } from "../NewSkin/Styles";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Button from "../NewSkin/Components/Button";
import Colors from "../NewSkin/Colors";

const heading = (t1, t2) => {
  return (
    <thead style={{ textAlign: "left" }}>
      <tr>
        <th style={{ textAlign: "left", width: "70%" }}>{t1}</th>
        <th style={{ textAlign: "left", width: "30%" }}>{t2}</th>
      </tr>
    </thead>
  );
};

const addOnsList = content => {
  let addOns = [];
  let addOns_selected = [];
  let status = "Pending";
  let note = "";
  addOns = content.inclusions.description.addons.list;
  if (content.details) {
    content.details.forEach(item => {
      if (item.name === "add_ons") {
        addOns_selected = item.list.map(i => {
          return i
        })
        status = item.status;
        note = item.notes;
      }
    });
  }

  let tr;
  tr = addOns.map((item, i) => {
    return (
      <tr key={`a${i}`}>
        <td className={tdStyle}>{item.description} </td>
        <td className={tdStyle} style={{ fontStyle: "italic" }}>{addOns_selected.length > 0 && addOns_selected.indexOf(item.description) > -1 ? status : addOns_selected.length == 0 ? 'Pending' : 'N/A'}</td>
      </tr>
    );
  });

  return (
    <tbody style={{ textAlign: "left" }}>
      {tr}
      <tr key={`x1`}>
        <td
          rowSpan={2}
          colSpan={3}
          className={tdStyle}
          style={{ background: "transparent" }}
        >
          {note ? (
              <div>
                Notes: <strong>{note}</strong>
              </div>
            ) : null}
        </td>
      </tr>
    </tbody>
  );
};

const _formatString = string => {
  return string.replace(/[^a-z0-9]|\s+|\r?\n|\r/gim, " ");
};

const inclusiontList = content => {
  let inclusions = [];
  let status = "Pending";
  if (content.details && content.details.length > 0) {
    inclusions = content.details;
  } else {
    if(content.inclusions.description.activity !== undefined){
      inclusions = content.inclusions.description.activity.list;
    }
  }

  let tr;

  tr = inclusions.map((item, i) => {
    if (item.name !== "add_ons") {
      return (
        <tr key={`a${i}`}>
          <td className={tdStyle}>{_formatString(item.name)} </td>
          <td
            className={tdStyle}
            style={{ fontStyle: "italic" }}
          >
            {item.status ? item.status : "Pending"}
          </td>
        </tr>
      );
    }
  });

  return <tbody style={{ textAlign: "left" }}>{tr}</tbody>;
};

const eventList = content => {
  let status = "Pending";
  let event = [];
  if (content.event_name) {
    event = [...event, { description: content.event_name }];
    status = content.event_status;
  } else {
    event = content.inclusions.description.health_talk.list;
  }

  const tr = event.map((item, i) => {
    return (
      <tr key={`a${i}`}>
        <td className={tdStyle}>{item.description} </td>
        <td
          className={tdStyle}
          style={{ fontStyle: "italic" }}
        >
          {status}{" "}
        </td>
      </tr>
    );
  });

  return <tbody style={{ textAlign: "left" }}>{tr}</tbody>;
};

const _renderProgramDetails = program => {
  return (
    <div
      className={`programStyle`}
      style={{
        margin: '10px auto',
        width: "80%"
      }}
    >
      <div
        className={labelClass}
        style={{
          fontSize: 18,
          color: ProgramColors(program.subscription_name)
        }}
      >
        Health Talk
      </div>
      <div className={ulStyle}>
        <table className={tableClass}>
          {heading("Name", "Status")}
          {eventList(program)}
        </table>
      </div>
      
      { program.inclusions.description.activity !== undefined &&
      program.inclusions.description.activity.list.length > 0 ?
      <>
      <div
        className={labelClass}
        style={{
          fontSize: 18,
          color: ProgramColors(program.subscription_name)
        }}
      >Activity</div>
      <div className={ulStyle}>
        <table className={tableClass}>
          {heading("Name", "Status")}
          {inclusiontList(program)}
        </table>
      </div></> : null
      }
      <div
        className={labelClass}
        style={{
          fontSize: 18,
          color: ProgramColors(program.subscription_name)
        }}
      >
        Add-Ons
      </div>
      <div className={ulStyle}>
        <table className={tableClass}>
          {heading("Name", "Status")}
          {addOnsList(program)}
        </table>
      </div>
    </div>
  );
};

const Details = ({ program, updateView }) => {
  const programDefaultDetailValues = CONSTANTS[program.program_code];

  return (
    <Card
      styles={{ padding: 0, overflow: "hidden", width: "100%", maxWidth: 1020 }}
    >
      <section
        className={programTitleDiv(ProgramColors(program.subscription_name))}
      >
        <GridContainer
          columnSize={"3fr 1fr"}
          styles={{ justifyContent: "right" }}
        >
          <div style={{ textAlign: "left" }}>
            <svg style={{ width: 35, height: 35, fill: ProgramColors(program.subscription_name), verticalAlign: 'middle' }}>
              <use xlinkHref={CHAIcons(program.subscription_name)} />
            </svg>
            <div className={programTitleStyle}>{program.subscription_name}</div>
          </div>
          <Button
            styles={{ margin: "auto 0", marginLeft: "auto", minWidth: 100 }}
          >
            <button onClick={() => updateView("programs")}>Close</button>
          </Button>
        </GridContainer>
      </section>

      <div className={contentDiv}>
        <h3
          style={{
            color: Colors.blueDarkAccent,
            textTransform: "uppercase",
            fontWeight: 500
          }}
        >
          Inclusions / intervention
        </h3>

        <section style={{ padding: 20, width: "100%" }}>
          {_renderProgramDetails(program)}
        </section>
      </div>
    </Card>
  );
};

export default Details;

const tableClass = css(mainFont, {
  width: "100% !important"
});

let programTitleDiv = bg =>
  css({
    padding: 30,
    background: `linear-gradient(to right, ${bg}50, ${bg})`,
    color: "white",
    textAlign: "center"
  });

let programTitleStyle = css({
  display: "inline-block",
  fontWeight: 600,
  // marginLeft: 30,
  textShadow: "0 2px 5px rgba(0,0,0,.2)",
  fontSize: 20
});

let contentDiv = css({
  position: "relative",
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "center",
  flexWrap: "wrap",
  margin: "10px, 10px, 10px 20px",
  padding: "5px 10px"
  //  border: '1px dotted red'
});

let tdStyle = css({
  border: "1px dotted",
  textTransform: "Capitalize"
});

let ulStyle = css({
  marginBottom: 20
});