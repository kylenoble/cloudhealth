import React from 'react';
import GridContainer from '../NewSkin/Wrappers/GridContainer';
import { css } from 'glamor';
import Colors from '../NewSkin/Colors';
import { CHAIcons, ProgramColors, SVGIcons } from '../NewSkin/Styles'

const Subscription = ({
  programs = [],
  index = 0,
  pending = false,
  item = {},
  name = '',
  respond = () => { },
  viewDetails = () => { },
}) => {
  return (
    <GridContainer onHoverStyles={{ borderColor: Colors.skyblue }} classes={[wrapper(ProgramColors(name))]} styles={{ alignContent: 'start', borderRadius: 10, overflow: 'hidden' }}>
      <figure className={name === 'Telemedicine' ? iconWrapperTelemed : iconWrapper(ProgramColors(name))}>
        {name !== 'Telemedicine' ?
        <svg>
          <use xlinkHref={CHAIcons(name)} />
        </svg> :
        <img src={SVGIcons(name)} alt={name} />
        }
        <div className={programTitleStyle}>{name}</div>
      </figure>
      <section className={contentWrapper}>
        <button className={`${btn(ProgramColors(name))}`} onClick={() => viewDetails(item)}>View Details</button>

        {
          pending &&
          <GridContainer columnSize={'1fr 1fr'} gap={10}>
            <button onClick={() => respond(item.enrollment_id, programs[index], true)} className={`${btn()} ${accept(ProgramColors(name))}`}>Accept</button>
            <button onClick={() => respond(item.enrollment_id, programs[index], false)} className={`${btn()} ${decline}`}>Decline</button>
          </GridContainer>
        }
      </section>

    </GridContainer>
  );
}

const btn = (mainColor = Colors.skyblue) => css({
  borderStyle: 'none',
  background: 'none',
  padding: 10,
  width: '100%',
  fontSize: 15,
  border: '1px solid #eee',
  borderRadius: 5,
  marginTop: 10,
  transition: '250ms ease',
  cursor: 'pointer',
  boxShadow: '0 2px 3px rgba(0,0,0,.050)',
  ':hover': {
    borderColor: mainColor,
    color: mainColor
  },
  ':focus': {
    outline: 'none'
  }
})

const decline = css({
  boxShadow: 'none',
  borderColor: 'white !important',
  ':hover': {
    borderColor: `${Colors.blueDarkAccent} !important`,
    color: `${Colors.blueDarkAccent} !important`
  }
});

const accept = (mainColor = Colors.skyblue) => css({
  background: `${mainColor} !important`,
  borderColor: `${mainColor} !important`,
  color: 'white',
  ':hover': {
    boxShadow: `0 5px 8px ${mainColor}70`,
    color: 'white !important'
  }
})

const contentWrapper = css({
  padding: 10
});

let programTitleStyle = css({
  display: "inline-block",
  fontWeight: 500,
  textShadow: "0 2px 5px rgba(0,0,0,.2)",
  fontSize: 15,
  color: '#fff',
  textAlign: 'left',
  lineHeight: 1.1
});

const iconWrapperTelemed = css({
  posittion: 'relative',
  background: `linear-gradient(to right, ${Colors.skyblue}, ${Colors.pink})`,
  display: 'grid',
  placeContent: 'center',
  margin: 0,
  minHeight: 80,
  '> img': {
    height: 35,
    filter: 'brightness(500%)'
  },
  gridTemplateColumns: '0.2fr 1fr',
  paddingLeft: 15
});

const iconWrapper = (mainColor = Colors.skyblue) => css({
  posittion: 'relative',
  background: `linear-gradient(to right, ${mainColor}50, ${mainColor})`,
  display: 'grid',
  placeContent: 'center',
  margin: 0,
  minHeight: 80,
  '> svg': {
    width: 35, 
    height: 35,
    fill: mainColor, 
    verticalAlign: 'middle', 
    margin: 'auto',    
    filter: 'brightness(500%)'
  },
  gridTemplateColumns: '0.2fr 1fr',
  paddingLeft: 15
});

const wrapper = (mainColor) => css({
  maxWidth: 200,
  minWidth: 150,
  boxShadow: '0 3px 5px rgba(0,0,0,.1)',
  border: '1px solid white',
  ':hover': {
    boxShadow: `0 5px 10px ${mainColor}50`,
    borderColor: mainColor
  }
});


export default Subscription;