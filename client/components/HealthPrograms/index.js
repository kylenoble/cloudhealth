import React from "react";
import { css } from "glamor";
import { confirmAlert } from "react-confirm-alert";
import dateFormat from "dateformat";
import Details from "./details";
import SubscriptionService from "../../utils/subscriptionService";
import Events from "../../components/Events";
import EmailNotification from "../EmailNotification";
import Card from '../NewSkin/Wrappers/Card';
import GridContainer from '../NewSkin/Wrappers/GridContainer';
import { labelClass, noteStyle } from "../NewSkin/Styles";
import Colors from "../NewSkin/Colors";
import Subscription from "./subscription";
import Restricted from "../../components/restricted";
import { Preparing } from "../NewSkin/Components/Loading";

const subscription = new SubscriptionService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedProgram: {},
      programs: [],
      curView: "programs",
      activeTab: 'subscriptions',
      showContent: false
    };
    this._getSelectedProgram = this._getSelectedProgram.bind(this);
    this._updateView = this._updateView.bind(this);
  }

  componentDidMount() {
    this._getSubscriptions();
  }

  _sendEmailNotification = (program, choice) => {
    const { name, id } = this.props.user;
    const to = this.props.user.email;
    const subscription_name = program.subscription_name;
    const subject = "New Notification from CloudHealthAsia";
    let title = "";
    let body = "";
    let email = {};
    let app = {};
    let body_for_app = "";
    let telemed_addtl_instruction =
      "Please update your health timeline to be able to book an appointment with one of our doctors.";

    switch (choice) {
      case "Approved":
        body = `<p>You have accepted your enrollment to Cloudhealthasia’s <strong style="color: #80cde9">${subscription_name}</strong>. You will now receive updates and notifications related to your program activities. `;
        body_for_app = `You have accepted your enrollment to Cloudhealthasia’s ${subscription_name}. You will now receive updates and notifications related to your program activities. `;

        if (program.program_code == "TeleMed") {
          body += telemed_addtl_instruction;
          body_for_app += telemed_addtl_instruction;
        }
        body += `</p>`;

        title = `${choice} Subscription`;

        email = { to, body, title, subject };
        app = { title, name, id, body_for_app };

        EmailNotification({ email, app });
        break;

      case "Declined":
        body = `<p>You have declined your enrollment to CloudHealthAsia’s <strong style="color: #80cde9">${subscription_name}</strong>. You will no longer receive notifications and updates on this program.</p>`;
        body_for_app = `You have declined your enrollment to CloudHealthAsia’s ${subscription_name}. You will no longer receive notifications and updates on this program.`;
        title = `${choice} Subscription`;

        email = { to, body, title, subject };
        app = { title, name, id, body_for_app };

        EmailNotification({ email, app });
        break;

      default:
        break;
    }
  };

  _getSubscriptions = () => {
    let programs = this.state.programs;
    const data = { field: "user_id", id: this.props.user.id };
    subscription.get(data).then(res => {
      if (res.length > 0) {
        programs = res;
      }

      this.setState({
        programs,
        showContent: true
      });
    });
  }

  _setActiveTab = (e) => {
    const tabs = document.querySelectorAll(`.${selected}`);
    tabs.forEach(tab => {
      tab.classList.remove(selected)
    })

    e.target.classList.add(selected);

    this.setState({ activeTab: e.target.id });
  }

  _programsView = () => {
    return (
      <Card>
        <div className="wrap">
          <GridContainer contain columnSize={'1fr 1fr'} styles={{ borderBottom: `2px solid ${Colors.pink}`, width: '100%' }}>
            <div className={`${selector} ${selected}`} id='subscriptions' onClick={(e) => this._setActiveTab(e)}>My Active Subscriptions</div>
            <div className={selector} id='events' onClick={(e) => this._setActiveTab(e)}>Upcoming Events</div>
          </GridContainer>

          <GridContainer styles={{ minHeight: '30vh', alignContent: 'start', width: '100%' }}>

            {
              this.state.activeTab === 'subscriptions' ?
                <div className={subscriptionDiv} style={{ flex: 1 }}>
                  <div className={content}>
                    <div className={subContent}>
                      <div className={noteStyle}>
                        {" "}
                        Note: You were enrolled by your company to these health
                        programs.
                      </div>
                      {this._subscriptionList()}
                    </div>
                  </div>
                </div> :


                <div className={subscriptionDiv} style={{ flex: 1 }}>
                  <div className={content}>
                    <div className={instructionStyle}>The following are subscription-related health talks:</div>
                    <Events user={this.props.user} viewfor="health_talks" />
                  </div>
                </div>
            }

          </GridContainer>
        </div>
      </Card>
    );
  }

  _updateView = (str) => {
    let curView = this.state.curView;
    curView = str;
    this.setState({
      curView
    });
  }

  _subscriptionList = () => {
    const programs = this.state.programs;
    const pending = programs.filter(p => p.enrollment_status === "Pending");
    const approved = programs.filter(p => p.enrollment_status === "Approved");
    const declined = programs.filter(p => p.enrollment_status === "Declined");
    const noImage = "no-image.png";
    const icons = {
      CHIR: "WeightMaintenanceProgram.svg",
      CHIT: "stressManagementProgram.svg",
      DMPB: "health.svg",
      DMPP: "health.svg",
      MMPB: "exercise.svg",
      MMPP: "exercise.svg",
      NMPB: "nutrition.svg",
      NMPP: "nutrition.svg",
      SlMPB: "sleep.svg",
      SlMPP: "sleep.svg",
      StMPB: "sad-stress.svg",
      StMPP: "sad-stress.svg",
      WMPB: "body.svg",
      WMPP: "body.svg",
      TeleMed: "telemed.svg"
    };

    const pendingItems = pending.map((item, i) => {
      let icon = noImage;
      if (icons[item.program_code]) {
        icon = icons[item.program_code];
      }
      let iconTextBGC;
      if (item.program_code.slice(-1) === "P") {
        iconTextBGC = "#e9554b";
      } else {
        iconTextBGC = "#37b492";
      }
      return (
        <section key={i}>
          <Subscription
            programs={programs}
            index={i}
            pending={true}
            item={item}
            name={item.subscription_name}
            respond={this._respond}
            viewDetails={this._viewDetails}
          />
        </section>
      );
    });

    const declinedItems = declined.map((item, i) => {
      let icon = noImage;
      if (icons[item.program_code]) {
        icon = icons[item.program_code];
      }

      let iconTextBGC;
      if (item.program_code.slice(-1) === "P") {
        iconTextBGC = "#e9554b";
      } else {
        iconTextBGC = "#37b492";
      }

      return (
        <section key={i}>
          <Subscription
            item={item}
            name={item.subscription_name}
            respond={this._respond}
            viewDetails={this._viewDetails}
          />
        </section>
      );
    });

    const approvedItems = approved.map((item, i) => {
      let icon = noImage;
      if (icons[item.program_code]) {
        icon = icons[item.program_code];
      }
      let iconTextBGC;
      if (item.program_code.slice(-1) === "P") {
        iconTextBGC = "#e9554b";
      } else {
        iconTextBGC = "#37b492";
      }
      return (
        <section key={i}>
          <Subscription
            item={item}
            name={item.subscription_name}
            respond={this._respond}
            viewDetails={this._viewDetails}
          />
        </section>
      );
    });

    const wrapStyle = {
      borderBottom: '1px solid #ccc3',
      padding: 30
    }

    return (
      <div className={subscriptionItemDiv}>
        <GridContainer contain columnSize={'1fr'} styles={{ width: '100%' }}>
          <section style={wrapStyle}>
            <div className={labelClass}>Pending Subscriptions</div>
            <GridContainer columnSize={'repeat(5, 1fr)'} gap={10} styles={{ width: '100%' }}>
              {pendingItems}
            </GridContainer>
          </section>

          <section style={wrapStyle}>
            <div className={labelClass}>Approved Subscriptions</div>
            <GridContainer columnSize={'repeat(5, 1fr)'} gap={10} styles={{ width: '100%' }}>
              {approvedItems}
            </GridContainer>
          </section>

          <section style={{ ...wrapStyle, border: 'none' }}>
            <div className={labelClass}>Declined Subscriptions</div>
            <GridContainer columnSize={'repeat(5, 1fr)'} gap={10} styles={{ width: '100%' }}>
              {declinedItems}
            </GridContainer>
          </section>

        </GridContainer>
      </div>
    );
  }

  _viewDetails = (obj) => {
    if (obj.program_code == "TeleMed") {
      this._showDetails(obj.inclusions);
    } else {
      let selectedProgram = this.state.selectedProgram;
      // let selectedProgram = obj;
      selectedProgram = obj;
      this.setState({
        selectedProgram,
        curView: "details"
      });
    }
  }

  _getSelectedProgram = () => {
    return this.state.selectedProgram;
  }

  _respond = (enrollment_id, program, b) => {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: "", // Message dialog
      childrenElement: () => this._confirmEnrollment(b), // Custom UI or Component
      buttons: [
        {
          label: b ? "Yes, enroll me in the program" : "Continue",
          onClick: () => this._confirm(enrollment_id, program, b)
        },
        {
          label: "Close"
        }
      ]
    });
  }

  _showDetails = (inclusions) => {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: "", // Message dialog
      childrenElement: () => (
        <p style={{ textAlign: 'justify' }} dangerouslySetInnerHTML={{ __html: inclusions.patient_details }} />
      ), // Custom UI or Component
      buttons: [
        {
          label: "Close"
        }
      ]
    });
  }

  _confirm = (id, program, b) => {
    let choice = "Declined";
    if (b) {
      choice = "Approved";
    }

    const date = new Date();
    const ndate = dateFormat(date, "yyyy-mm-dd h:MM:ss TT");

    let updateData = {};
    if (choice === "Approved") {
      updateData = { id, enrollment_status: choice, date_approved: ndate };
    } else {
      updateData = { id, enrollment_status: choice, date_declined: ndate };
    }

    subscription
      .update(updateData)
      .then(res => {
        if (res) {
          // send email notification
          this._sendEmailNotification(program, choice);

          // res is true or false
          // if successfully updated change the state of  enrollment_status
          this._updateEnrollmentStatusLocalState(choice, id);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _updateEnrollmentStatusLocalState = (c, id) => {
    const programs = this.state.programs;
    programs.find((p, i) => {
      if (p.enrollment_id === id) {
        programs[i].enrollment_status = c;
        return true; // stop searching
      }
    });
    this.setState({ programs });
  }

  _confirmEnrollment = (b) => {
    let message = "";
    if (b) {
      message =
        "You are accepting your enrollment to this program. Since this is a company-sponsored program, accepting your enrollment to any of the health programs will waive your right to conceal your identity to your company. Do you wish to continue with your enrollment?";
    } else {
      message =
        "You are declining your enrollment to this program. You will no longer receive notifications on events and other program-related activities. Should you wish to join the program in the future, please contact your HR department for re-enrollment.";
    }
    return <div className={confirmMessageStyle}>{message}</div>;
  }

  render() {
    if (!this.state.showContent) {
      return <Preparing />
    }
    if (this.state.programs.length == 0) {
      let restrict_msg = `This feature is not included in your current subscription. Please contact the officer in-charge of your account if you wish to subscribe`;
      return (
        <Card>
          <Restricted message={restrict_msg} />
        </Card>)
    }
    return (
      <div className={mainDiv}>
        {this.state.curView === "programs" ? (
          this._programsView()
        ) : (
            <Details
              program={this.state.selectedProgram}
              updateView={this._updateView}
            />
          )}
      </div>
    );
  }
} // end of component class

// CSS Styling


const selector = css({
  padding: '10',
  borderBottom: '5px solid white',
  transition: '250ms ease',
  cursor: 'pointer',
  fontWeight: 500,
  color: Colors.blueDarkAccent
});

const selected = css({
  borderBottomColor: `${Colors.pink}`,
  color: `${Colors.pink}`
})

let mainDiv = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "center",
  flexWrap: "wrap",
  padding: "5px 10px"
  //border: '1px solid red'
});

let subscriptionDiv = css({
  display: "flex",
  flex: "1 0  30%",
  flexWrap: "wrap",
  //flexDirection: 'reverse',
  justifyContent: "flex-start",
  alignItems: "flex-start",
  // borderRight: "1px dotted gray",
  ":last-child": { border: "none" }
});

let title = css({
  width: "auto",
  //height: '30px',
  padding: "5px 15px",
  margin: "5px 10px",
  marginBottom: 20,
  textTransform: "uppercase",
  color: "#fff",
  background: "#1b75bb"
});

let content = css({
  display: "flex",
  justifyContent: "flex-start",
  flexWrap: "wrap",
  padding: "5px 10px",
  "@media (max-width: 865px)": {
    justifyContent: "center"
  },
  width: '100%'
});

let programsDiv = css({
  width: "200px",
  padding: 10,
  margin: 0
});

let iconDiv = css({
  border: "1.5px solid #7ecdea",
  borderRadius: "10px",
  display: "block",
  marginRight: "auto",
  marginLeft: "auto",
  width: "55px",
  height: "60px",
  padding: "3px 0",
  margin: "auto",
  verticalAlign: "center",
  textAlign: "center",
  overflow: "hidden"
});

let iconText = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "center",
  fontSize: "xx-small",
  textTransform: "uppercase",
  color: "#fff"
});

let imageStyle = css({
  display: "block",
  marginRight: "auto",
  marginLeft: "auto",
  width: "90%",
  height: "70%",
  margin: "auto",
  verticalAlign: "center"
});

let programTitleStyle = css({
  letterSpacing: "1.5",
  margin: "5px 0"
});

let linkStyle = css({
  display: "block",
  padding: "2px 5px",
  color: "#80cde9",
  cursor: "pointer",
  //width: 'auto',
  border: "1px dotted transparent",
  ":hover": {
    color: "#eec5e2",
    //backgroundColor: '#eec5e2',
    border: "1px dotted",
    borderRadius: 10,
    transition: "color .5s ease-in"
    //transitionDelay: '.5s',
  }
});

let confirmMessageStyle = css({
  color: "#80cde9"
});

let warning = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "center",
  //marginTop: 20,
  alignItems: "center",
  color: "#d383b6",
  textTransform: "uppercase",
  fontWeight: "bold"
});

let subContent = css({
  position: "relative",
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 100%",
  justifyContent: "flex-start",
  "@media (max-width: 865px)": {
    justifyContent: "center"
  }
});

let subscriptionItemDiv = css({
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 100%",
  "@media (max-width: 865px)": {
    justifyContent: "center"
  }
});

let hdng3 = css({
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  flex: "1 0 100%",
  background: "#1b75bb",
  color: "#fff"
});

let instructionStyle = css({
  padding: 10,
  fontStyle: "italic"
});
