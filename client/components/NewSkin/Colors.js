const skyblue = '#81cce6'
const pink = '#d383b6'
const blueDarkAccent = '#152859'
const pinkDarkAccent = '#8e3372'
const weightColor = '#80cce7'
const nutritionColor = '#f7ad4b'
const sleepColor = '#d083b4'
const detoxColor = '#c7db76'
const stressColor = '#8160a1'
const movementColor = '#f17a5f'
const disabled = '#ccc'
const optimal = '#72a153'
const suboptimal = '#71b2ca'
const neutral = '#dfaf29'
const alarming = '#d77f1a'
const compromised = 'red'

const Colors = { skyblue, pink, blueDarkAccent, pinkDarkAccent, weightColor, nutritionColor, sleepColor, detoxColor, stressColor, movementColor, disabled, optimal, suboptimal, neutral, alarming, compromised }
export default Colors


// ? Legends Color
export const Legends = {
  improving: '#90AF36',
  notImproving: '#d383b6',
  forImmediateFollowUp: '#F9443C',
  followUpNextMonth: '#63C6D1',
  stalledOrNeedsRecovery: '#FEC251'
}