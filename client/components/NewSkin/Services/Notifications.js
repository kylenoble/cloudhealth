/* eslint-disable import/imports-first */
import NotificationService from "../../../utils/notificationService.js";
import AnswerService from "../../../utils/answerService.js";
import moment from "moment-timezone";

const Notification = new NotificationService();
const Answer = new AnswerService();

export const Notifications = user => {
  const id = user.id;
  const company_id = user.company_id;
  const type = user.type;
  const ids = `${id}~${company_id}~${type}`;
  const notif = Notification.getAdmin(ids)
    .then(res => res)
    .catch(e => {
      console.log(e);
    });

  return notif;
};

export const GetPersonalInfo = () => {
  return Answer.getAnswerList({
    column: "question_id",
    operator: "lt",
    value: 11
  })
    .then(res => res.length < 6)
    .catch(e => {
      console.log(e);
    });
};

export const updateNotification = (notification, val = null, user) => {
  console.log("@ updateNotification:", notification);

  const ndate = moment();
  const read_by_data = notification.read_by;
  let result = [];

  if (read_by_data) {
    result = read_by_data.data.filter(rb => rb.id === this.user.id);
  }

  let rd = [];

  if (read_by_data) {
    rd = read_by_data.data;
  }

  let data = {
    id: notification.id,
    read_by: {
      data: rd
    }
  };

  if (result.length > 0) {
    if (val) {
      result[0].status = val;
    }

    data = {
      id: notification.id,
      read_by: read_by_data
    };
  } else {
    data.read_by.data.push({
      type: user.type,
      id: user.id,
      date_read: ndate,
      status: null
    });
  }

  goUpdate(data, user);
};

const goUpdate = (data, user) => {
  Notification.updateAdmin(data).then(() => {
    Notifications(user);
  });
};
