/* eslint-disable import/imports-first */
import React from 'react';
import { contained, combined, centered, on_mobile } from '../Styles';
import { css } from 'glamor';

const ContainedWrapper = ({ contain = false, center = true, children, styles = {}, headerAndFooterHeight = 0, innerPadding = 0, path, from = '' }) => {
  return (
    <main
      data-name="contained-wrapper_main"
      className={
        combined(
          [
            main,
            pageHeight(headerAndFooterHeight),
            center ? centered : null,
            on_mobile([], { width: '100%' })
          ],
          { ...styles })}
    >
      <section
        data-name="contained-wrapper_section"
        className={combined([contain && contained, section], { padding: innerPadding, margin: path === '/dashboard' && 0 })}
      >
        {children}
      </section>

    </main>
  )
};

export default ContainedWrapper;

const main = css({
  width: '100%'
});

const pageHeight = (height) => css({
  // minHeight: `calc(100vh - ${height + 1}px)`,
  minHeight: `calc(100vh - 88px)`,
  marginTop: 75
});

let section = css({
  padding: 35
});
