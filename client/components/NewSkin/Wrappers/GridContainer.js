/* eslint-disable import/imports-first */
import React from "react";
import { grid_container, combined, contained, centered } from "../Styles";
import { css } from "glamor";

const GridContainer = ({
  columnSize = "1fr",
  rowGap = 0,
  columnGap = 0,
  gap = 0,
  contain = false,
  styles = {},
  children,
  paddingOnMobile = 0,
  columnsOnMobile = 1,
  classes = [],
  dataID = "",
  onHoverStyles = {},
  fullWidth = false,
  center = false,
  childStyles = {},
  onHoverEvent = () => { },
  onLeaveEvent = () => { },
  onClickEvent = () => { }
}) => {
  return (
    <section
      data-id={dataID}
      data-name="grid-container"
      onMouseEnter={onHoverEvent}
      onMouseLeave={onLeaveEvent}
      onClick={onClickEvent}
      className={combined(
        [
          onHover(onHoverStyles),
          grid_container(columnSize, rowGap, columnGap, gap),
          gridContainer(paddingOnMobile, columnsOnMobile, fullWidth, center, childStyles),
          contain ? contained : null,
          ...classes
        ],
        { ...styles }
      )}
    >
      {children}
    </section>
  );
};

export default GridContainer;

const onHover = hoverStyles =>
  css({
    transition: "250ms ease",
    ":hover": {
      ...hoverStyles
    }
  });

let gridContainer = (pad, col, fullWidth, center, childStyles) =>
  css(centered, {
    width: fullWidth ? '100% !important' : null,
    marginLeft: center ? 'auto' : null,
    marginRight: center ? 'auto' : null,
    "@media only screen and (max-width: 600px)": {
      gridTemplateColumns: `repeat(${col}, 1fr) !important`,
      padding: pad <= 0 ? 0 : pad
    },
    '& *': {
      ...childStyles
    }
  });
