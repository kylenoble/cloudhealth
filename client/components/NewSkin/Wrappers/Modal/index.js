import React, { useEffect, useContext } from "react";
import { css } from "glamor";
import { scrollClass, combined } from "../../Styles";
import { UserContext } from "../../../../context/UserContext";

let onSmallDevices = false;
// Modal Main Wrapper
const ModalWrapper = ({ children, styles = {}, classes = [] }) => {
  const ctx = useContext(UserContext);
  const screenSize = ctx.screenSize;
  onSmallDevices = screenSize === "xsmall" || screenSize === "small";

  return (
    <section
      className={combined(
        [onSmallDevices ? onMobileWrapper : wrapper, ...classes],
        { ...styles }
      )}
    >
      {children}
    </section>
  );
};

export default ModalWrapper;

// Modal Content wrapper that separates content to the buttons
export const ModalContentWrapper = ({
  children,
  styles = {},
  classes = []
}) => (
  <section
    className={combined(
      [onSmallDevices ? onMobileContentWrapper : card, ...classes],
      { ...styles }
    )}
  >
    {children}
  </section>
);

// Modal text content
export const ModalContent = ({ children, classes = [], styles = {} }) => (
  <section className={combined([content, ...classes], { ...styles })}>
    {children}
  </section>
);

// Wrapper for buttons
export const ModalFooter = ({ children, classes = [], styles = {} }) => (
  <section className={combined([footer, ...classes], { ...styles })}>
    {children}
  </section>
);

// Modal's Main Header
export const ModalHeader = ({ children, classes = [], styles = {} }) => (
  <section className={combined([header, ...classes], { ...styles })}>
    {/* <div style={{ color: 'inherit' }}> */}
    {children}
    {/* </div> */}
  </section>
);

// Modal's Item Title
export const ModalTitle = ({ children, classes = [], styles = {} }) => (
  <section className={combined([title, ...classes], { ...styles })}>
    {children}
  </section>
);

// Styles
const gray = "#00000080";

const onMobileContentWrapper = css(scrollClass, {
  width: "100%",
  height: "auto",
  maxHeight: "40vh",
  overflow: "auto",
  overflowX: "hidden",
  padding: "0 20px",
  boxSizing: "border-box",
  "& > *": {
    height: "inherit"
  }
});

const onMobileWrapper = css({
  width: "95%",
  height: "fit-content",
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -45%)",
  background: "#fff",
  padding: 15,
  borderRadius: 10,
  boxShadow: "0 3px 5px rgba(0,0,0,.2)"
});

let title = css({
  "> *": {
    fontWeight: "600 !important",
    fontSize: "1.1em !important"
  }
});

let footer = css({
  height: "fit-content",
  padding: "20px 10px 0px"
});

let content = css({
  "> *": {
    fontSize: "inherit",
    color: gray,
    padding: "0 !important",
    textAlign: "justify"
  }
});

let header = css({
  "> *": {
    fontWeight: 600,
    fontSize: "inherit",
    color: gray,
    padding: "0 !important",
    lineHeight: "normal",
    "> *": {
      fontWeight: 600,
      fontSize: "inherit",
      color: gray,
      padding: "0 !important",
      lineHeight: "normal"
    }
  }
});

let card = css(scrollClass, {
  width: "100%",
  height: 360,
  overflow: "auto",
  overflowX: "hidden",
  padding: "0 20px",
  boxSizing: "border-box",
  "& > *": {
    height: "inherit"
  }
});

let wrapper = css({
  maxWidth: 750,
  width: "95%",
  height: "fit-content",
  position: "absolute",
  top: "51%",
  left: "50%",
  transform: "translate(-50%, -45%)",
  background: "#fff",
  padding: "30px 35px",
  borderRadius: 15,
  boxShadow: "0 3px 5px rgba(0,0,0,.2)"
});
