import React from 'react';
import { avoidPageBreakInside } from '../Styles';

const PageWrapper = ({ children }) => {
  return (
    <section id="page-wrapper" className={avoidPageBreakInside} style={{ width: '100%', padding: 20 }}>
      <article id="page-content">{children}</article>
    </section>
  );
}

export default PageWrapper;