import React from 'react'
import { disabled_tab, active_tab, default_tab } from '../Styles';
import GridContainer from './GridContainer';
import IconWrapper from '../Icons/IconWrapper';
import { css } from 'glamor';
import Button from '../Components/Button';

const TabItem = ({ children, isActive = false, isDisabled = false, onClickFn = () => { }, withIcon = false, btnType = false }) => {
  const _renderIcon = () => {
    switch (children) {
      case "Weight":
        return <IconWrapper btnType={btnType} isActive={isActive} icon="Weight" />;

      case "Nutrition":
        return <IconWrapper btnType={btnType} isActive={isActive} icon="Nutrition" />;

      case "Sleep":
        return <IconWrapper btnType={btnType} isActive={isActive} icon="Sleep" />;

      case "Detox":
        return <IconWrapper btnType={btnType} isActive={isActive} icon="Detox" />;

      case "Stress":
        return <IconWrapper btnType={btnType} isActive={isActive} icon="Stress" />;

      case "Movement":
        return <IconWrapper btnType={btnType} isActive={isActive} icon="Exercise" />;
      default:
        return ''
    }
  }

  return (
    <React.Fragment>
      {
        btnType ?
          <Button type={isDisabled ? 'disbaled' : isActive ? 'blue' : 'default'} styles={{ margin: '0 auto', minWidth: '150px !important', width: '150px !important' }}>
            <button onClick={onClickFn}>
              <GridContainer
                dataID={`${children}-button-tab`}
                columnSize={withIcon ? '20px 1fr' : '1fr'}
                gap={10}
                styles={{ width: 'fit-content', margin: '0 auto', fontSize: 15, fontWeight: 600 }}
              >
                {withIcon && _renderIcon()}
                <span className={child}>{children}</span>
              </GridContainer>
            </button>
          </Button>

          :
          <article onClick={onClickFn} className={isDisabled ? disabled_tab : isActive ? active_tab : default_tab}>
            <GridContainer columnSize={withIcon ? '35px 1fr' : '1fr'} styles={{ width: 'fit-content', margin: '0 auto' }}>
              {withIcon && _renderIcon()}
              <span className={child}>{children}</span>
            </GridContainer>
          </article>
      }
    </React.Fragment>
  );
}

const child = css({
  display: 'block',
  margin: 'auto auto',
  fontSize: '15px !important'
})

export default TabItem;