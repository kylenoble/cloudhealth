import React from 'react'
import { combined, scrollClass, grid_wrapper, centered } from '../Styles';
import { css } from 'glamor';

const Card = ({ classes = [], styles = {}, children, scrollable = false, maxHeight = 0, square = false, squareSize = 0, centered = false, fullWidth = false }) => {
  return (
    <section
      id="card"
      className={combined(
        [
          card,
          scrollable ? scroll(maxHeight <= 0 ? 'auto' : maxHeight) : null,
          square ? sq(squareSize <= 0 ? 'auto' : squareSize) : null,
          centered ? center : null,
          ...classes,
        ]
        , { ...styles, width: fullWidth ? '100%' : null })}>
      {children}
    </section>
  )
}

export default Card

let center = css(grid_wrapper, centered)

let sq = (size) => css({
  width: size,
  // height: `calc(${size}px - 10px)`,
  // height: size,
  margin: '0 auto'
})

let scroll = (maxHeight) => css(scrollClass, {
  minHeight: '100%',
  maxHeight: maxHeight,
  overflowY: 'auto'
})

let card = css({
  padding: 30,
  background: 'white',
  borderRadius: 15,
  boxSizing: 'border-box',
  boxShadow: '0 3px 5px rgba(0,0,0,.2)',
  "@media print": {
    boxShadow: "none"
  }
})
