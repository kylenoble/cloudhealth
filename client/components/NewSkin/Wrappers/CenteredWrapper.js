import React from 'react';
import { centered, combined, centered_left, centered_right } from '../Styles'
// import { css } from 'glamor';

const CenteredWrapper = ({ to = 'center', styles = {}, classes = [], children }) => {
  const center_to = () => {
    if (to === 'left') {
      return centered_left
    } else if (to === 'right') {
      return centered_right
    } else {
      return centered
    }
  };

  return (
    <div id="centered-wrapper" className={combined([center_to(), ...classes], { width: '100%', ...styles })}>
      {children}
    </div>
  )
};

export default CenteredWrapper;
