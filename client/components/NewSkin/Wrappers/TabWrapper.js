import React from 'react';
import GridContainer from './GridContainer';
import { css } from 'glamor';
import Colors from '../Colors';

const TabWrapper = ({ tabCount = 1, customTabSize = '', children, styles = {}, classes = [], gap = 0, btnType = false, dataID = '' }) => {
  return (
    <GridContainer
      dataID={dataID}
      columnSize={customTabSize ? customTabSize : `repeat(${tabCount}, 1fr)`}
      gap={gap}
      classes={[wrapper(styles, btnType), ...classes]}
    >
      {children}
    </GridContainer>
  );
}


const wrapper = (styles, btnType) => css({
  width: '100%',
  borderBottom: btnType ? 'none' : `2px solid ${Colors.pink}`,
  ...styles
});

export default TabWrapper;