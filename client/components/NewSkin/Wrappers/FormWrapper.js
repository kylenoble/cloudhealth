import React from 'react'
import { css } from 'glamor';
import { combined } from '../Styles';

const FormWrapper = ({ styles = {}, children, textAlign = 'left', onMobileTextAlign = 'center', deviceSize = '' }) => {

  return (
    <section id="form-wrapper" className={combined([wrapper(textAlign, onMobileTextAlign, deviceSize)], { ...styles })}>
      {children}
    </section>
  )
}

export default FormWrapper

let wrapper = (textAlign, onMobileTextAlign, deviceSize) => {
  const screenSize = deviceSize === 'xsmall' || deviceSize === 'small' && true;

  return css({
    width: '100%',
    textAlign,
    '@media(max-width: 768px)': {
      textAlign: onMobileTextAlign,
      '> div': {
        textAlign: onMobileTextAlign,
      }
    },
    '> div': {
      textAlign,
      '> div': {
        margin: '0 !important'
      },
    },
    '& div.input-wrapper': {
      display: 'block !important',
      '> input': {
        borderRadius: '22px !important',
        border: 'none !important',
        width: '300px !important',
        margin: screenSize ? '0 auto !important' : '0 !important',
        display: 'block'
      }
    },
    '& button.loginBtn': {
      margin: '0 !important'
    },
    '& [data-name="grid-container"]': {
      '> input': {
        borderRadius: '22px !important',
        border: 'none !important',
        width: '300px !important',
        margin: screenSize ? '0 auto !important' : '0 !important',
        display: 'block',
        textAlign: 'left'
      }
    }
  })

}

