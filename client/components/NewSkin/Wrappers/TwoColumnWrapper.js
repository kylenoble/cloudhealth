import React from 'react';
import { css } from 'glamor';
import { two_columns__wrapper, combined, on_mobile } from '../Styles';
import LandingImage from '../Icons/LandingImage';

const TwoColumnWrapper = ({ lowReso = false, withImage = true, children, template = '1fr 1fr', styles = {}, onMobileStyles = {}, imageAlign = 'center', imageOnMobileStyles = {} }) => (
  <main
    id="two-column-wrapper"
    className={`${combined([two_columns__wrapper, on_mobile([], { ...onMobileStyles })], { gridTemplateColumns: `${template} !important`, ...styles })} ${mainWrapper}`}
  >
    {children}

    {/* shows landing image on the right side */}
    {withImage ? <LandingImage onModileStyles={imageOnMobileStyles} styles={{ justifyContent: imageAlign }} lowReso={lowReso} /> : null}
  </main>
);

export default TwoColumnWrapper;


let mainWrapper = css({
  '> section:nth-child(1)': {
    gridArea: 'bottom'
  },
  '> section:nth-child(2)': {
    gridArea: 'top'
  },
});
