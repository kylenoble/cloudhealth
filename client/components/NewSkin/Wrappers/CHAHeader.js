import React from 'react';
import { css } from 'glamor';
import Colors from '../Colors';
import Label from '../Components/Label'

const CHAHeader = ({ pageNumber }) => {
  return (
    <section className={logoWrapper}>
      <svg alt="CloudHealthAsia Logo" width="200" height="60" viewBox="0 0 200 60">
        <use x="0" y="0" className={logo} xlinkHref={`/static/icons/Symbols.svg#cha_logo`} />
      </svg>
      <Label fullWidth styles={{ textAlign: 'right', fontWeight: 600, paddingRight: 10, margin: 'auto' }} text={pageNumber ? pageNumber : ''} />
    </section>
  );
}

const logoWrapper = css({
  margin: '10px 0',
  padding: '5px 0',
  borderBottom: `2px solid ${Colors.skyblue}`,
  textAlign: 'left',
  display: 'grid',
  gridTemplateColumns: '1fr 1fr',
  width: '100%'
})

const logo = css({
  transform: 'translateX(-310%) translateY(-95px)',
})

export default CHAHeader;