/* eslint-disable import/imports-first */
/* eslint-disable no-undef */
/* eslint-disable no-nested-ternary */
/* eslint-disable react/react-in-jsx-scope */
import { Component } from "react";
import Head from "./Head";
// import RenderLink from "./Methods/renderLink";
import Logo from "./Icons/Logo";
import TopBarMenus from "./Components/TopBarMenus";
import Colors from "./Colors";
import { UserContext } from "../../context";
import { css } from "glamor";
import { combined, on_mobile } from "./Styles";
import Sidenav from "./Components/Sidenav";

export default class NewHeader extends Component {
  static contextType = UserContext;
  _isMounted = false;
  state = {
    showTopBar: false,
    showSidenav: false,
    closeSidenavAnimate: false,
    notificationCount: 0
  };

  componentDidMount() {
    this._isMounted = true;
    this._checkNotificationsCount()
  }

  componentWillUnmount() {
    // use intervalId from the state to clear the interval
    this._isMounted = false;
  }

  checkPathName = path => {
    switch (path) {
      case "index":
        return false;
      case "login":
        return false;
      case "register":
        return false;
      case "consent_confirmation":
        return false;

      default:
        return true;
    }
  };

  showSidenav = () => {
    if (this._isMounted) {
      if (this.state.showSidenav) {
        this.setState({
          closeSidenavAnimate: true
        });
        setTimeout(() => {
          this.setState({
            showSidenav: !this.state.showSidenav
          });
        }, 350);
      } else {
        this.setState({
          showSidenav: !this.state.showSidenav,
          closeSidenavAnimate: false
        });
      }
    }
  };

  _checkNotificationsCount = () => {
    const timer = setInterval(() => {
      const ctx = this.context;
      const notifications = ctx.notif_personal_info ? ctx.notif_personal_info : ctx.notifications;
      const notYetRead = [];

      if (notifications.length > 0) {
        clearInterval(timer);
        notifications.filter(item => {
          item.ihaveread = true;
          if (!item.read_by) {
            item.ihaveread = false;
            notYetRead.push(item);
          }
          return false;
        });
      }
      if (notYetRead.length > 0) {
        this.setState({ notificationCount: notYetRead.length });
      }
    }, 500);
  }

  render() {
    const user = this.props.user;
    const pathname = this.props.pathname;

    return (
      <div
        //id="page-header"
        className={combined([
          header,
          pathname !== "index"
            ? user
              ? user.type === "Doctor"
                ? blue
                : white
              : white
            : white,
          on_mobile([], { gridTemplateColumns: "1fr" })
        ])}
      >
        <Head notificationCount={this.state.notificationCount} />
        <section>
          <Logo
            onMobileStyles={{
              marginLeft: "auto",
              transform: "translateX(-20%)"
            }}
            link
            white={
              pathname !== "index"
                ? user
                  ? user.type === "Doctor"
                  : false
                : false
            }
          />
        </section>

        {!this.props.timeout && this.checkPathName(pathname) && (
          <button
            className={combined([on_mobile([], { ...hamburger })], {
              display: "none"
            })}
            id="sidenav-trigger"
            onClick={() => this.showSidenav()}
          />
        )}

        {!this.props.timeout && this.checkPathName(pathname) && (
          <TopBarMenus switchDashboard={this.props.switchDashboard} />
        )}

        {this.state.showSidenav && (
          <Sidenav
            switchDashboard={this.props.switchDashboard}
            closeSidenavAnimate={this.state.closeSidenavAnimate}
            showSidenav={this.showSidenav}
          />
        )}
      </div>
    );
  }
}

const hamburger = {
  position: "absolute",
  top: "50%",
  left: 20,
  transform: "translateY(-50%)",
  display: "block !important",
  borderStyle: "none",
  height: 19,
  width: 30,
  background: "rgba(0,0,0,0)",
  borderBottom: "3px solid #ccc",
  cursor: "pointer",
  ":focus": {
    outline: "none"
  },
  "&::before, &::after": {
    content: '""',
    position: "absolute",
    left: 0,
    top: 0,
    height: 3,
    width: "inherit",
    background: "#ccc"
  },
  "&::after": {
    top: 8
  }
};

let white = css({
  background: "#fff !important"
});

let blue = css({
  background: `${Colors.skyblue} !important`
});

let header = css({
  position: "fixed",
  top: 0,
  left: 0,
  display: "grid",
  gridTemplateColumns: "4fr 1fr",
  alignContent: "center",
  width: "100%",
  boxShadow: "0 8px 15px rgba(0,0,0,.1)",
  boxSizing: "border-box",
  zIndex: 100
});
