import React from "react";
import { confirmAlert } from "react-confirm-alert"; // Import
import moment from "moment-timezone";
import AppointmentNotification from "../../Notifications/appointmentNotifications";
import { css } from 'glamor'

export const showContent = (content, user) => {
  confirmAlert({
    closeOnEscape: false,
    closeOnClickOutside: false,
    title: "", // Title dialog
    message: "", // Message dialog
    childrenElement: () => message(content, user), // Custom UI or Component
    buttons: [
      {
        label: "Close",
        onClick: () => {
          console.log("Closed!");
        }
      }
    ]
  });
};

const message = (content, user) => {
  const timeCreated = moment(content.date_created).toISOString();
  return (
    <section id="program-modal-wrapper" className={wrapper}>
      {content.sender.name === "System" ? (
        <AppointmentNotification user={user} content={content} />
      ) : (
          <div>
            <h4 className={notifFrom}><small>From: {content.sender.name} - {moment(timeCreated).format("LLL")}</small></h4>

            <div id="appointment-details-wrapper" className={appointmentDetailsWrapper}>
              <label className={detailsTitle}>{content.subject}</label>
              <p dangerouslySetInnerHTML={{ __html: content.message }} />
            </div>
          </div>
        )}
    </section>
  );
};

const wrapper = css({
  maxWidth: 700,
  width: '100%'
})

let notifFrom = css({
  margin: '10px 0px',
  '> small': {
    background: 'whitesmoke',
    padding: '5px 10px',
    display: 'inline-block',
  }
})

let appointmentDetailsWrapper = css({
  padding: 15,
  background: '#fff',
  overflow: 'hidden',
  borderRadius: 3,
  marginTop: 10,
  boxShadow: '0 5px 10px rgba(0,0,0,.05)',
  '> h4': {
    fontSize: 18
  },
  '> div span': {
    display: 'inline-block',
    minWidth: 135
  },
  '> p': {
    textAlign: 'justify',
    padding: '10px 5px',
  }
})

let detailsTitle = css({
  margin: '-15px -15px 0',
  display: 'block',
  padding: '5px 15px',
  background: '#80cde9',
  textTransform: 'uppercase',
  color: '#fff'
})