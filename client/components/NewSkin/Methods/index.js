export const headerAndFooterHeight = () => {
  let headerHeight = 0
  let footerHeight = 0
  let header = null
  let footer = null

  header = document.querySelector('#page-header');
  footer = document.querySelector('#footer');
  headerHeight = header !== null ? header.offsetHeight : 0;
  footerHeight = footer !== null ? footer.offsetHeight : 0;

  return headerHeight + footerHeight;
}


export const checkScreenResolution = () => {
  const deviceSizes = {
    onXSmallDevice: false,
    onSmallDevice: false,
    onMediumDevice: false,
    onLargeDevice: false,
    onXLargeDevice: false,
  };

  let screen_size = 'xlarge'

  const small = 600; // on/below
  const medium = 768; // on/up
  const large = 992; // on/up
  const xlarge = 1200; // on/up

  const screenWidth = window.innerWidth

  if (screenWidth >= xlarge) {
    // console.log('X-Large Device', screenWidth);

    deviceSizes.onXLargeDevice = true;
    screen_size = 'xlarge'
  } else if (screenWidth >= large && screenWidth < xlarge) {
    // console.log('Large Device', screenWidth);

    deviceSizes.onLargeDevice = true;
    screen_size = 'large'
  } else if (screenWidth >= medium && screenWidth < large) {
    // console.log('Medium Device', screenWidth);

    deviceSizes.onMediumDevice = true;
    screen_size = 'medium'
  } else if (screenWidth >= small && screenWidth < medium) {
    // console.log('Small Device', screenWidth);

    deviceSizes.onSmallDevice = true;
    screen_size = 'small'
  } else if (screenWidth < small) {
    // console.log('X-Small Device', screenWidth);

    deviceSizes.onXSmallDevice = true;
    screen_size = 'xsmall'
  }

  // console.log('screen size', screen_size);

  return screen_size;

}

export const base64Converter = (url, callback, outputFormat) => {
  const img = new Image();
  img.crossOrigin = 'Anonymous';
  img.onload = () => {
    let canvas = document.createElement('CANVAS'),
      ctx = canvas.getContext('2d'),
      dataURL;

    canvas.height = img.height;
    canvas.width = img.width;
    ctx.drawImage(img, 0, 0);
    dataURL = canvas.toDataURL(outputFormat);
    callback(dataURL);
    canvas = null;
  };
  img.src = url;
}
