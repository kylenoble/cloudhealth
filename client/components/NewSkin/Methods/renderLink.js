import React from "react";
import { css } from "glamor";
import currentDomain from "../../../utils/domain";

const RenderLink = props => {
  if (props.pathname !== "index") {
    const avatarUrl = props.user.avatar
      ? `/static/avatar/${props.user.avatar}`
      : `/static/icons/user.svg`;

    const user = props.user;
    let switch_link = "";

    if (user && user.is_admin && user.status === 1) {
      switch_link = (
        <div
          className={`${dropdownContent} dropdown-content`}
          style={{ right: 0, width: "100%" }}
        >
          <a
            style={{ cursor: "pointer" }}
            onClick={() => props.switchDashboard()}
          >
            <img
              className={`${linkIcon}`}
              src="/static/icons/switch.svg"
              alt=""
            />
            Switch to Admin Dashboard
          </a>
        </div>
      );
    }

    return (
      <div className="dropdown-box">
        <div className={`${userAvatarNameWrapper}`}>
          <figure className={`${avatarWrapper}`}>
            <img src={avatarUrl} alt="" />
          </figure>
          <span className={dropdownLink}>{props.user.name}</span>
        </div>

        <div className={`${dropdownContentWrapper}`}>
          <div
            className={`${dropdownContent} dropdown-content`}
            style={{ right: 0, width: "100%" }}
          >
            <a href="" onClick={props.logoutPressed}>
              <img
                className={`${linkIcon}`}
                src="/static/icons/logout.svg"
                alt=""
              />
              Logout
            </a>
            <br />
          </div>
          {switch_link}
        </div>
      </div>
    );
  } else {
    return false;
  }
};

export default RenderLink;

let dropdownLink = css({
  textAlign: "center",
  color: "#fff",
  textDecoration: "none",
  fontFamily: "Roboto-Medium, sans-serif",
  cursor: "pointer"
});

const linkIcon = css({
  width: 15,
  marginRight: 20
});

const userAvatarNameWrapper = css({
  display: "flex",
  alignItems: "center",
  transition: "250ms ease",
  backgroundColor: "#ffffff00",
  padding: "10px 20px",
  borderRadius: 2,
  cursor: "pointer",
  ":hover": {
    backgroundColor: "#ffffff05"
  }
});

const avatarWrapper = css({
  margin: 0,
  marginRight: 10,
  width: 32,
  height: 32,
  borderRadius: "100%",
  overflow: "hidden",
  border: "1px solid #ffffff90",
  boxShadow: "0 4px 8px 0px rgba(0,0,0,.1)",
  position: "relative",
  "> img": {
    position: "absolute",
    top: "50%",
    left: 0,
    transform: "translateY(-50%)",
    width: "100%"
  }
});

const dropdownContentWrapper = css({
  position: "absolute",
  top: 51,
  right: 0,
  width: "18em",
  borderRadius: 2,
  overflow: "hidden",
  boxShadow: "0px 8px 20px 0px rgba(0,0,0,0.2)"
});

const dropdownContent = css({
  display: "flex",
  alignItems: "center",
  position: "relative !important",
  height: "auto !important",
  boxShadow: "none !important",
  "> a": {
    fontSize: 16,
    border: "none !important",
    opacity: 0.5,
    transition: "250ms ease"
  },
  ":hover": {
    "> a": {
      opacity: 1
    }
  }
});
