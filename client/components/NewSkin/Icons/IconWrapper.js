import React from 'react'
import { css } from 'glamor';
import Colors from '../Colors';

const IconWrapper = ({ icon = '', btnType = false, isActive = false }) => {

  const iconColor = icon => {
    switch (icon) {
      case 'Stress':
        return Colors.stressColor
      case 'Exercise':
        return Colors.movementColor
      case 'Detox':
        return Colors.detoxColor
      case 'Sleep':
        return Colors.sleepColor
      case 'Nutrition':
        return Colors.nutritionColor
      case 'Weight':
        return Colors.weightColor

      default:
        return Colors.weightColor
    }
  }

  return (
    <svg
      style={{ fill: btnType ? isActive ? 'white' : iconColor(icon) : iconColor(icon) }}
      className={wrapper}
    >
      <use xlinkHref={`/static/icons/cha_icons.svg#${icon}`} />
    </svg>
  )
}


const wrapper = css({
  width: 25,
  height: 25,
  display: 'grid',
  placeContent: 'center'
})

export default IconWrapper;