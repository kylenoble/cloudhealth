import React from 'react';
import { css } from 'glamor';
import Link from "next/link";
import { combined, on_mobile } from '../Styles';

const Logo = ({ white = false, link = false, styles = {}, classes = [], onMobileClasses = [], onMobileStyles = {} }) => (
  <div>
    {
      link ?
        <Link prefetch href="/">
          <a className={anchor}>
            <figure
              alt="CloudHealthAsia Logo"
              className={
                combined(
                  [
                    logoWrapper,
                    on_mobile([...onMobileClasses], { margin: '0 auto', ...onMobileStyles }),
                    ...classes,
                  ], { ...styles }
                )
              }
            >
              <svg width="200" height="60" viewBox="0 0 200 60">
                <use x="0" y="0" className={logo} xlinkHref={`/static/icons/Symbols.svg#cha_logo${white ? '_white' : ''}`} />
              </svg>
            </figure>
          </a>
        </Link > :
        <figure
          alt="CloudHealthAsia Logo"
          className={
            combined(
              [
                logoWrapper,
                on_mobile([...onMobileClasses], { margin: '0 auto', ...onMobileStyles }),
                ...classes,
              ], { ...styles }
            )
          }
        >
          <svg alt="CloudHealthAsia Logo" width="200" height="60" viewBox="0 0 200 60">
            <use x="0" y="0" className={logo} xlinkHref={`/static/icons/Symbols.svg#cha_logo${white ? '_white' : ''}`} />
          </svg>
        </figure>
    }
  </div>
);

export default Logo;

const anchor = css({
  display: 'inline-block',
  height: 75,
  marginLeft: 100
});

let logoWrapper = css({
  width: 170,
  height: 'inherit',
  overflow: 'hidden',
  margin: 0,
  marginLeft: 30,
  display: 'grid',
  alignContent: 'center',
  justifyContent: 'center',
  '> svg': {
    width: '100%'
  }
});

let logo = css({
  transform: 'translateX(-310%) translateY(-95px)'
})
  ;
