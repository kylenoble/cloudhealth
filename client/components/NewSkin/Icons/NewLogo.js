import React from 'react';
import { css } from 'glamor';

const NewLogo = () => {
  return (
    <svg
      className={logo}
    >
      <use xlinkHref={`/static/icons/cha_icons.svg#Logo`} />
    </svg>
  );
};

const logo = css({
  width: 171,
  height: 53
});

export default NewLogo;