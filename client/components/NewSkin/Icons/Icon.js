import React from 'react';
import { css } from 'glamor';
import { centered, combined } from '../Styles';
import Colors from '../Colors';

const Icon = (
  { icon = '', toggleModal = () => { }, isOpen, whiteIcons = false, atPage }
) => {
  const title = () => {
    if (icon === 'messages') {
      return 'Announcements'
    } else if (icon === 'notification') {
      return 'Notifications';
    } else if (icon === 'confirmed_patients') {
      return 'Confirmed Patients'
    } else {
      return 'Appointments'
    }

  };

  const iconSrc = () => {
    if (icon === 'messages') {
      return `/static/icons/announcement_icon.svg#announcement`;

    } else if (icon === 'notification') {
      return `/static/icons/cha_icons.svg#Notifications`;

    } else if (icon === 'confirmed_patients') {
      return `/static/icons/confirmed_patients.svg#cp`;

    } else if (icon === 'appointments') {
      return `/static/icons/book_appointment_icon.svg#bookapp`;
    }
  }

  return (
    <figure className={combined([iconWrapper, centered])} title={title()}>
      <svg
        id={icon}
        viewBox={icon === 'notification' ? "0 0 30 30" : "0 0 400 400"}
        style={{ width: 30, height: 30 }}
        className={combined([whiteIcons ? white : gray, isOpen ? open(whiteIcons) : null, atPage ? atPageClass : null])}
        onClick={() => toggleModal()}
      >
        <use
          id={icon}
          onClick={() => toggleModal()}
          x="0" y="0"
          xlinkHref={iconSrc()}
        />
      </svg>
    </figure>
  );
};

export default Icon;

let atPageClass = css({
  fill: Colors.pink
})

let open = (whiteIcons) => css({
  fill: `${whiteIcons ? Colors.pink : Colors.skyblue} !important`,
  transform: 'scale(.9) !important'
});

const svgWrapper = css({
  transition: '250ms ease',
  cursor: 'pointer',
  transform: 'scale(1)',
});

let white = css(svgWrapper, {
  fill: '#fff',
  ':hover': {
    fill: Colors.pink
  }
});

let gray = css(svgWrapper, {
  fill: '#8d8d8d',
  ':hover': {
    fill: Colors.skyblue
  }
});


let iconWrapper = css({
  margin: 0,
});
