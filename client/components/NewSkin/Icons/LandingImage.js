import React from 'react';
import { css } from 'glamor';
import { combined, centered, on_mobile } from '../Styles';

const LandingImage = ({ lowReso = false, styles = {}, onModileStyles = {} }) => (
  <section className={combined([centered, on_mobile([], { ...onModileStyles })], { ...styles })}>
    <figure className={combined([imageWrapper, lowReso ? small : null, on_mobile([], { transform: 'scale(1)' })])}>
      <svg style={{ width: '100%', height: 'auto' }} width="450" height="634" viewBox="0 0 450 634">
        <use className={background} x="0" y="0" xlinkHref="/static/icons/Symbols.svg#background" />
      </svg>
    </figure>
  </section>
);

export default LandingImage;

let small = css({
  maxWidth: '250px !important'
});

let imageWrapper = css({
  transform: 'scale(1.2)',
  margin: 0,
  '> svg': {
    width: '100%'
  }
});

let background = css({
  transform: 'translate(-313%, -70%)',
  border: '1px solid red'
});
