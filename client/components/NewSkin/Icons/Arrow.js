import React from 'react';
import { css } from 'glamor';
import Colors from '../Colors';

const Circle = (props) => {
  const year = props.date.split(' ')[0];
  const month = props.date.split(' ')[1];
  return (
    <div id="circle">
      <div className={circleStyles(props.fill, props.index, props.opaque)}>
        {year}
        <div>{month.substr(0, 3)}</div>
      </div>
    </div>
  );
}

const Event = props => {
  const divisible = props.index % 2 === 0;
  const limiter = str => {
    if (str.length > 30) {
      return `${str.substr(0, 30)}...`;
    } else {
      return str;
    }
  }

  return (
    <div
      id={`event-name_${divisible ? 'bottom' : 'top'}`}
      className={wrapper(props.fill, divisible)}
      style={divisible ? { bottom: 0, transform: 'translate(-50%, -200%)' } : { top: 0, transform: 'translate(-50%, 200%)' }}>
      {limiter(props.event)}
    </div>
  )
}

const arrowType = (type, fill) => {
  switch (type) {
    case 'Start':
      return (
        <svg id="svg" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" width="130" height="85.77494692144374" viewBox="0, 0, 400,85.77494692144374">
          <g id="svgg">
            <path id="path0" d="M33.408 1.640 C -11.229 17.710,-12.089 69.207,32.033 84.004 C 37.312 85.774,37.367 85.775,198.494 85.775 L 359.674 85.775 380.037 65.398 C 399.995 45.427,400.375 44.982,399.139 43.019 C 398.445 41.918,389.278 31.817,378.769 20.574 L 359.660 0.132 198.726 0.097 C 43.967 0.063,37.624 0.122,33.408 1.640 " stroke="none" fill={fill} fillRule="evenodd">
            </path>
          </g>
        </svg>
      );
    case 'Middle':
      return (
        <svg
          id="svg" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" width="120" height="85.77494692144374" viewBox="0, 0, 400,95.50827423167848">
          <g id="svgg">
            <path id="path0" d="M-0.000 0.508 C -0.000 0.787,10.240 11.926,22.757 25.261 L 45.513 49.508 22.527 72.508 L -0.458 95.508 177.320 95.508 L 355.098 95.508 377.772 72.819 C 399.995 50.582,400.418 50.086,399.041 47.901 C 398.268 46.674,388.061 35.428,376.359 22.909 L 355.083 0.147 177.541 0.074 C 79.894 0.033,-0.000 0.228,-0.000 0.508 " stroke="none" fill={fill} fillRule="evenodd">
            </path>
          </g>
        </svg>
      );
    case 'End':
      return (
        <svg
          id="svg" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" width="120" height="85.77494692144374" viewBox="0, 0, 400,95.50827423167848">
          <g id="svgg">
            <path id="path0" d="M-0.000 0.498 C -0.000 0.782,10.240 11.926,22.757 25.261 L 45.513 49.508 22.527 72.508 L -0.458 95.508 178.997 95.508 C 381.025 95.508,361.467 96.197,375.861 88.572 C 411.352 69.769,408.042 23.619,369.863 4.930 L 360.757 0.473 180.378 0.227 C 81.170 0.092,-0.000 0.214,-0.000 0.498 " stroke="none" fill={fill} fillRule="evenodd"></path>
          </g>
        </svg>
      );

    default:
      break;
  }
}

const Arrow = ({ fill = Colors.skyblue, index, date, event, active, type }) => {
  const props = { date, event, fill, index };
  const op = props.index % 2 === 0;
  return (
    <section className={`${svgStyles(fill)} ${active && activeStyle(fill)}`} >
      <Circle {...props} opaque={op} />
      <Event {...props} />
      {arrowType(type, fill)}
      <Circle {...props} opaque={!op} />
    </section>
  );
}

export default Arrow;

const wrapper = (fill, div) => css({
  position: 'absolute',
  left: '50%',
  transform: 'translate(-50%, 0%)',
  padding: 10,
  background: fill,
  color: 'white',
  maxWidth: 100,
  borderRadius: 5,
  overflowWrap: 'break-word',
  opacity: 0,
  fontSize: 'small',
  '::before': {
    ...pseudo(fill),
    transform: 'translate(-50%, -150%) rotate(45deg)',
    opacity: div ? 1 : 0,
    height: 10,
    width: 10,
  },
  '::after': {
    ...pseudo(fill),
    transform: 'translate(-50%, 200%) rotate(45deg)',
    opacity: div ? 0 : 1,
    height: 10,
    width: 10,
  },
});

const activeStyle = (fill) => css({
  opacity: '1 !important',
  '> div#event-name_bottom': {
    transform: 'translate(-50%, 0%) !important',
    bottom: '10px !important',
    opacity: 1,
    width: '100%'
  },
  '> div#event-name_top': {
    transform: 'translate(-50%, 0%) !important',
    top: '10px !important',
    opacity: 1,
    width: '100%'
  },
  '> div#circle': {
    '> div': {
      boxShadow: `0 5px 8px ${fill}70`,
    },
    transform: 'scale(1.1)',
  },
  zIndex: 0,
});

const pseudo = (fill) => {
  return {
    content: '""',
    position: 'absolute',
    left: '50%',
    transform: 'translateX(-50%) translateY(150%)',
    height: 40,
    width: 5,
    background: fill,
    opacity: 0
  }
}

const circleStyles = (fill, i, opaque) => {
  const op = i % 2 === 0;
  return css({
    width: 70,
    height: 70,
    border: `5px solid ${fill}`,
    borderRadius: '100%',
    opacity: opaque ? 1 : 0,
    fontSize: 14,
    display: 'grid',
    placeContent: 'center',
    margin: '0 auto',
    position: 'relative',
    color: '#333',
    transition: '200ms ease',
    '::before': {
      ...pseudo(fill),
      opacity: op ? 1 : 0
    },
    '::after': {
      ...pseudo(fill),
      bottom: 0,
      transform: 'translateX(-50%) translateY(-150%)',
      opacity: op ? 0 : 1
    }
  })
}

const svgStyles = fill => css({
  boxShadow: '0 1px 1px rgba(0,0,0,0)',
  zIndex: 0,
  position: 'relative',
  cursor: 'pointer',
  display: 'grid',
  placeContent: 'center',
  lineHeight: '15px',
  opacity: .7,
  transition: '250ms ease',
  '> div#circle': {
    '> div': {
      boxShadow: '0 1px 1px rgba(0,0,0,0)',
      transition: '250ms ease',
    },
    transition: '250ms ease',
  },
  '> div#event-name_bottom': {
    transition: '250ms ease',
  },
  '> div#event-name_top': {
    transition: '250ms ease',
  },
  ':hover': {
    opacity: 1,
    '> div#circle': {
      '> div': {
        boxShadow: `0 5px 8px ${fill}70`,
      },
      transform: 'scale(1.1)',
    },
  }
})