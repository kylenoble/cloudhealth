import { css } from "glamor";
import colors from "./Colors";
import { darken } from "material-ui/utils/colorManipulator";

// CHA New logo source
export const new_logo = "/static/images/CHA New Logo.png";

// Adding Montserrat as a Main Font
export const mainFont = css({
  fontFamily: `Montserrat, sans-serif !important`,
  // fontSize: '15px !important',
  "> *": {
    fontFamily: `Montserrat, sans-serif !important`
  }
});

/* _____  MAIN COLORS  ______ */
export const skyblue_color = css({
  color: colors.skyblue
});

export const skyblue_bg = css({
  backgroundColor: colors.skyblue
});

export const blue__dark_accent_color = css({
  color: colors.blueDarkAccent
});

export const blue__dark_accent_bg = css({
  backgroundColor: colors.blueDarkAccent
});

export const pink_color = css({
  color: colors.pink
});

export const pink_bg = css({
  backgroundColor: colors.pink
});

export const pink__dark_accent_color = css({
  color: colors.pinkDarkAccent
});

export const pink__dark_accent_bg = css({
  backgroundColor: colors.pinkDarkAccent
});
export const Colors = colors;

/* _____  END OF MAIN COLORS  ______ */

/* _____  WRAPPERS  ______ */

// Display Grid Wrapper
export const grid_wrapper = css({
  display: "grid"
});

export const grid_container = (
  column_size = "1fr",
  rowGap = 0,
  columnGap = 0,
  gap = 0
) =>
  css(grid_wrapper, {
    gridTemplateColumns: column_size,
    gridRowGap: rowGap,
    gridColumnGap: columnGap,
    gridGap: gap
  });

export const flex_wrapper = css({
  position: 'relative',
  display: 'flex',
  flex: '1 0 100%',
  justifyContent: 'center',
  alignItems: 'center',
  flexWrap: 'wrap'
})

// Adjustable font-weight
export const fontWeight = size =>
  css({
    fontWeight: size,
    "> *": {
      fontWeight: "inherit"
    }
  });

// Adjustable font-size
export const font = size =>
  css({
    fontSize: `${size}em`,
    "@media(max-width: 768px)": {
      fontSize: `${size}em`,
      "> *": {
        fontSize: `${size}em`
      }
    },
    "> *": {
      fontSize: `${size}em`,
      "> *": {
        fontSize: `${size}em`,
        "> *": {
          fontSize: `${size}em`
        }
      },
      "@media(max-width: 768px)": {
        fontSize: `${size}em`
      }
    }
  });

/* 
  Full-Width Wrapper 
  with minimum height of 100% viewport height
*/
export const full_width = css({
  width: "100%",
  minHeight: "100vh"
});

/*
  Contained Wrapper
  with maximum width of 1200px and 
  with minimum height of 100% viewport height
*/
export const contained = css(default_height, {
  maxWidth: 1200,
  width: "100%",
  margin: "0 auto"
});

/*
  A display Grid wrapper with 2 columns/(fractions)
*/
export const two_columns__wrapper = css(grid_wrapper, {
  gridTemplateColumns: "1fr 1fr",
  gridTemplateAreas: `
      "bottom top"
    `,
  "@media(max-width: 768px)": {
    gridTemplateColumns: "1fr !important",
    gridTemplateAreas: `
      "top"
      "bottom"
    `
  }
});

/* _____ END OF WRAPPERS  ______ */

/*
  Centering content by [0,0] axis
*/
export const centered = css(grid_wrapper, {
  justifyContent: "center",
  alignContent: "center",
  textAlign: "center",
  "> *": {
    textAlign: "center"
  }
});

export const centered_by_margin = css({
  margin: "0 auto"
});

export const to_left = css({
  justifyContent: "left",
  textAlign: "left"
});

export const to_right = css({
  justifyContent: "right",
  textAlign: "right"
});

export const to_center = css({
  justifyContent: "center",
  textAlign: "center"
});

/*

*/
export const centered_to = css(grid_wrapper, {
  alignContent: "center",
  "@media(max-width: 768px)": {
    justifyContent: "center !important",
    textAlign: "center",
    "> *": {
      justifyContent: "center !important",
      textAlign: "center"
    }
  }
});

export const centered_left = css(centered_to, to_left);
export const centered_right = css(centered_to, to_right);

export const contained_wrapper__combined = (classes = [], styles = {}) => {
  const defaultStyle = [two_columns__wrapper, contained];
  return css(classes.length > 0 ? [defaultStyle, ...classes] : defaultStyle, {
    ...styles
  });
};

export const full_width_wrapper__combined = (styles = [], props = {}) => {
  const defaultStyle = [two_columns__wrapper, full_width];
  return css(styles.length > 0 ? [defaultStyle, ...styles] : defaultStyle, {
    ...props
  });
};

export const combined = (styles = [], props = {}) =>
  css([...styles], { ...props });

/* _____  BUTTONS ______ */

export const btn = css({
  borderStyle: "none",
  outline: "none",
  ":focus": {
    outline: "none"
  },
  minWidth: 230,
  maxWidth: 230,
  textAlign: "center"
});

export const btn_basic = css(btn, {
  backgroundColor: "#fff",
  color: "#000",
  borderRadius: 30,
  transition: "250ms ease",
  boxShadow: "0 3px 3px rgba(0,0,0,.1)",
  border: "1px solid rgba(0,0,0,0)",
  cursor: "pointer",
  ":hover": {
    backgroundColor: `#fbfbfb`,
    boxShadow: `0 5px 10px ${Colors.skyblue}70`,
    borderColor: Colors.skyblue,
    color: Colors.skyblue
  }
});

export const btn_pink = css(btn_basic, pink_bg, {
  color: "#fff !important",
  ":hover": {
    backgroundColor: `#e093c4`,
    boxShadow: `0 5px 10px ${colors.pink}90`,
    border: `1px solid ${Colors.pink}`
  },
  "> *": {
    color: "#fff !important",
    "> *": {
      color: "#fff !important"
    }
  }
});

export const btn_darkpink = css(btn_basic, pink__dark_accent_bg, {
  color: "#fff !important",
  ":hover": {
    backgroundColor: `#8e3372d6`,
    boxShadow: `0 5px 10px ${colors.pinkDarkAccent}90`,
    border: `1px solid ${Colors.pinkDarkAccent}`
  },
  "> *": {
    color: "#fff !important",
    "> *": {
      color: "#fff !important"
    }
  }
});

export const btn_skyblue = css(btn_basic, skyblue_bg, {
  color: "#fff !important",
  ":hover": {
    backgroundColor: `#8edcf7`,
    boxShadow: `0 5px 10px ${Colors.skyblue}90`,
    border: `1px solid ${Colors.skyblue}`
  },
  "> *": {
    color: "#fff !important",
    "> *": {
      color: "#fff !important"
    }
  }
});

export const btn_warning = css(btn_basic, {
  color: "#fff !important",
  background: Colors.nutritionColor,
  ":hover": {
    backgroundColor: '#f7ad4bd1',
    boxShadow: `0 5px 10px ${Colors.nutritionColor}90`,
    border: `1px solid ${Colors.nutritionColor}`
  },
  "> *": {
    color: "#fff !important",
    "> *": {
      color: "#fff !important"
    }
  }
});

export const btn_disabled = css(btn_basic, {
  background: "#eeeeee80",
  cursor: "default !important",
  color: "#ccc !important",
  boxShadow: "0 3px 3px rgba(0,0,0,0)",
  "> *": {
    color: "#ccc !important",
    cursor: "default !important",
    "> *": {
      color: "#ccc !important"
    }
  },
  ":hover": {
    boxShadow: "0 3px 3px rgba(0,0,0,0)",
    background: "#eeeeee80 !important",
    borderColor: "rgba(0,0,0,0)"
  }
});

export const closeBtn = css({
  fontSize: 15,
  fontWeight: 600,
  float: "right",
  color: Colors.disabled,
  "> button": {
    padding: "4px 8px !important"
  }
});

/* _____  INPUTS _____ */
export const inputDefault = css(btn, btn_basic, {
  minWidth: 300,
  backgroundColor: "#fff !important"
});

export const small_on_phone = css({
  "@media(max-width: 768px)": {
    width: "50% !important"
  }
});

export const on_mobile = (classes = [], styles = {}) =>
  css(...classes, {
    "@media(max-width: 768px)": { ...styles }
  });

const navHeight = 75;
const footerHeight = 59;
const total = navHeight + footerHeight;

export const default_height = css({
  maxHeight: `calc(100vh - ${total}px)`,
  minHeight: `calc(100vh - ${total}px)`,
  marginTop: 75
});

export const bold = css({
  fontWeight: 600
});

export const black = css({
  fontWeight: 700
});

export const scrollClass = css({
  "&::-webkit-scrollbar-track": {
    borderRadius: 10,
    backgroundColor: "#f2f2f2"
  },
  "&::-webkit-scrollbar": {
    width: 8,
    borderRadius: 10,
    backgroundColor: "#f2f2f2"
  },
  "&::-webkit-scrollbar-thumb": {
    borderRadius: 10,
    backgroundColor: Colors.pink
  }
});

export const modalCustomStyles = {
  content: {
    position: "relative !important",
    top: "0 !important",
    left: "0 !important",
    bottom: "0 !important",
    right: "0 !important",
    width: "100vw",
    height: "100vh",
    border: "none",
    background: Colors.skyblue
  }
};

// Checkbox styles
export const checkBox = {
  width: 20,
  height: 20,
  borderRadius: "100%",
  border: "2px solid #777",
  backgroundColor: "white",
  display: "inline-block",
  margin: "auto",
  transition: "250ms ease",
  cursor: "pointer",
  transform: "scale(1)",
  "> span": {
    opacity: 0
  },
  ":focus": {
    outline: "none"
  },
  ":hover": {
    transform: "scale(1.2)",
    borderColor: Colors.skyblue
  }
}

export const selectStyleOption = css({
  ...checkBox
});

export const sqrDefault = {
  ...checkBox,
  borderRadius: '0 !important'
}

export const sqrSelected = {
  ...checkBox,
  borderRadius: "0 !important",
  backgroundColor: Colors.skyblue,
  borderColor: Colors.skyblue,
  boxShadow: "0 5px 15px #81cce670"
}

export const selectedOptionStyles = css(selectStyleOption, {
  backgroundColor: Colors.skyblue,
  borderColor: Colors.skyblue,
  boxShadow: "0 5px 15px #81cce670"
});

export const sqrDefaultOption = css(selectStyleOption, {
  borderRadius: "0 !important"
});

export const sqrSelectedOption = css(selectStyleOption, {
  borderRadius: "0 !important",
  backgroundColor: Colors.skyblue,
  borderColor: Colors.skyblue,
  boxShadow: "0 5px 15px #81cce670"
});

// Button Style
export const changeButton = css({
  borderStyle: "none",
  textAlign: "center",
  height: "fit-content",
  width: "100%",
  background: "none",
  textTransform: "uppercase",
  color: Colors.blueDarkAccent,
  fontStyle: "italic",
  margin: "auto auto",
  fontSize: 14,
  transition: "250ms ease",
  cursor: "pointer",
  borderRadius: 5,
  fontWeight: 500,
  padding: 10,
  maxWidth: 100,
  ":hover": {
    color: Colors.skyblue
  },
  ":focus": {
    outline: "none"
  },
  ":active": {
    background: "#ccc2",
    color: Colors.skyblue
  }
});

export const disabledChangeButton = css(changeButton, {
  background: "#ccc1",
  cursor: "default",
  color: "#ccc",
  ":hover": {
    color: "#ccc"
  },
  ":active": {
    background: "#ccc1",
    color: "#ccc"
  }
});

export const inputClass = css(mainFont, {
  width: "100%",
  borderStyle: "none",
  padding: "10px 15px",
  border: "1px solid #eee",
  boxShadow: "0 2px 2px rgba(0,0,0,.050)",
  transition: "250ms ease",
  borderRadius: 5,
  cursor: "text",
  ":focus": {
    outline: "none",
    boxShadow: `0 10px 10px ${Colors.skyblue}30`,
    borderColor: Colors.skyblue
  },
  '::placeholder': {
    color: Colors.disabled
  }
});

export const inputClassError = css(inputClass, {
  borderColor: 'red',
  '::placeholder': {
    color: 'red !important'
  }
})

export const inputStyleObj = {
  width: '100% !important',
  borderStyle: 'none  !important',
  padding: '10px 15px  !important',
  border: '1px solid #eee  !important',
  boxShadow: '0 2px 2px rgba(0,0,0,.050)  !important',
  transition: '250ms ease  !important',
  borderRadius: '5px !important',
  fontFamily: `Montserrat, sans-serif !important`,
  fontSize: 18,
  cursor: 'text',
  ':focus': {
    outline: 'none !important',
    boxShadow: `0 10px 10px ${Colors.skyblue}30 !important`,
    borderColor: `${Colors.skyblue} !important`
  },
  '::placeholder': {
    color: Colors.disabled
  }
}

// Input Style
export const inputStyle = css({
  borderStyle: "none",
  border: "1px solid #fff",
  padding: 10,
  background: "none",
  textAlign: "left !important",
  color: Colors.blueDarkAccent,
  fontWeight: 500
});

export const disabledInputStyle = css(inputStyle, {
  boxShadow: "none",
  color: "#777 !important",
  cursor: "default"
});

export const dropdownStyle = css({
  cursor: "pointer"
});

export const labelClass = css({
  fontWeight: 600,
  color: Colors.blueDarkAccent,
  textAlign: "left !important",
  display: "block",
  width: "100%",
  marginBottom: 10,
  fontSize: 18
});

const formMsg = css({
  textAlign: 'center',
  fontSize: "0.8em",
  padding: '5px',
  display: "none",
  margin: '10px auto',
  color: "white",
  borderRadius: 5,
  transition: "color 0.3s ease-in-out"
})

export const goodMessage = css(formMsg, {
  background: 'rgb(3, 210, 127)'
});

export const errorMsg = css(formMsg, {
  background: Colors.movementColor
});

export const formMsgActive = css({
  display: "block !important"
})

export const noteStyle = css({
  padding: '0 15px',
  margin: '20px 0',
  borderLeft: `2px solid ${Colors.movementColor}`,
  color: Colors.movementColor
});

export const condensedTitle = css(labelClass, {
  color: Colors.disabled,
  fontSize: "1.5em",
  letterSpacing: -3
});

export const titleWbackground = css({
  flex: 4,
  background: Colors.skyblue,
  color: 'white',
  fontWeight: 600,
  fontSize: 20,
  textAlign: "center",
  padding: 5,
  borderRadius: 5
})

export const ProgramColors = program => {
  switch (program) {
    case "Sleep Management Program - Basic":
      return Colors.sleepColor;
    case "Movement Program - Basic":
      return Colors.movementColor;
    case "Weight Management Program - Basic":
      return Colors.weightColor;
    case "Nutrition Program - Basic":
      return Colors.nutritionColor;
    case "Detoxification Program - Basic":
      return Colors.detoxColor;
    case "Stress Management Program - Basic":
      return Colors.stressColor;

    default:
      return Colors.skyblue;
  }
};


export const HealthStatusColor = status => {
  switch (status) {
    case 'Optimal':
      return Colors.optimal;
    case 'Suboptimal':
      return Colors.suboptimal;
    case 'Neutral':
      return Colors.neutral;
    case 'Alarming':
      return Colors.alarming;
    case 'Compromised':
      return Colors.compromised;

    default:
      return Colors.disabled
  }
}

const cha_icons = "/static/icons/cha_icons.svg#";
const icons = "/static/icons";

export const CHAIcons = program => {
  switch (program) {
    case "Sleep Management Program - Basic":
      return `${cha_icons}Sleep`;
    case "Movement Program - Basic":
      return `${cha_icons}Exercise`;
    case "Weight Management Program - Basic":
      return `${cha_icons}Weight`;
    case "Nutrition Program - Basic":
      return `${cha_icons}Nutrition`;
    case "Detoxification Program - Basic":
      return `${cha_icons}Detox`;
    case "Stress Management Program - Basic":
      return `${cha_icons}Stress`;

    default:
      return;
  }
};

export const SVGIcons = program => {
  switch (program) {
    case "Sleep Management Program - Basic":
      return `${icons}/sleep.svg`;
    case "Movement Program - Basic":
      return `${icons}/exercise.svg`;
    case "Weight Management Program - Basic":
      return `${icons}/body.svg`;
    case "Nutrition Program - Basic":
      return `${icons}/nutrition.svg`;
    case "Detoxification Program - Basic":
      return `${icons}/detox.svg`;
    case "Stress Management Program - Basic":
      return `${icons}/stress.svg`;
    case "Telemedicine":
      return `${icons}/telemed.svg`;
    default:
      return;
  }
};

//Links
export const link = (defaultColor = Colors.skyblue) =>
  css({
    textAlign: "center",
    padding: "5px",
    cursor: "pointer",
    fontWeight: "600",
    transition: "300ms ease",
    color: defaultColor,
    ":hover": {
      color: Colors.pink
    }
  });

export const statusButton = css({
  width: "100%",
  color: "white",
  padding: 2,
  borderRadius: 8,
  display: "inline-block",
  fontStyle: "normal",
  textTransform: "uppercase",
  fontWeight: 600,
  display: "block",
  fontSize: ".8rem"
});

export const calendarClass = css({
  fontSize: "18px !important",
  "& .m-input-moment": {
    width: "100% !important",
    border: "none !important",
    padding: "0 !important",
    "& .tab": {
      height: "auto !important"
    }
  },
  "& .m-calendar ": {
    "& table td": {
      border: "none !important",
      width: 43,
      height: 43,
      borderRadius: "100%",
      transition: "250ms ease",
      ":hover": {
        background: darken(Colors.skyblue, 0.2)
      }
    },
    "& .prev-month, & .next-month": {
      ":hover": {
        color: "white"
      },
      color: "#ccc !important"
    },
    "& .toolbar .current-date": {
      fontSize: "large !important",
      fontWeight: 600
    }
  }
});

export const ReactTableStyle = css({
  maxWidth: '65vw',
  margin: '0 auto',
  '@media(max-width: 768px)': {
    maxWidth: '80vw !important',
  },
  '& .ReactTable': {
    border: 'none',
    boxShadow: '0 2px 5px rgba(0,0,0,.050)',
    transition: '250ms ease',
    borderRadius: 5,
    overflow: "hidden",
    ":hover": {
      boxShadow: "0 5px 10px rgba(0,0,0,.1)"
    },
    "& .rt-thead.-header": {
      background: Colors.skyblue
    },

    "& .rt-thead .rt-th": {
      borderRight: "none !important"
    },
    "& .-pagination": {
      background: "white",
      border: "1px solid #eee",
      boxShadow: "none",

      "& .-btn": {
        background: "none",
        color: "#ccc",
        ":hover": {
          background: `${Colors.skyblue} !important`,
          color: `white !important`
        }
      },
      "& .-btn[disabled]": {
        background: "none",
        color: "#eee",
        ":hover": {
          background: `none !important`,
          color: "#eee !important"
        }
      }
    },

    "& .rt-tbody .rt-tr-group": {
      borderBottom: "1px solid #eee5"
    },
    "& .rt-tbody .rt-td": {
      borderRight: "none !important"
    }
  }
});

export const chart_tooltip = css({
  background: "rgba(255,255,255,.9)",
  boxShadow: "0 16px 32px -4px rgba(0,0,0,.1)",
  borderRadius: 3,
  minWidth: 100,
  overflow: "hidden",
  transition: "300ms ease",
  "> p": {
    background: Colors.blueDarkAccent,
    display: "block",
    color: "white",
    padding: "5px 10px",
    textAlign: "center",
    fontSize: 15,
    fontWeight: 600
  },
  "> label": {
    padding: 10,
    margin: 0,
    textAlign: "center",
    display: "block",
    color: "#1b75bb",
    fontWeight: 600,
    fontSize: 'smaller',
  }
});

export const noDataStyle = css({
  color: "#ccc",
  fontSize: 16,
  height: "100px",
  display: "grid",
  width: "100%",
  justifyContent: "center",
  alignContent: "center"
});

export const noprint = css({
  "@media print": {
    display: "none"
  }
})


// Tab Styles
export const default_tab = css({
  fontSize: 15,
  fontWeight: 600,
  padding: '10px 0',
  borderBottom: '5px solid white',
  transition: '250ms ease',
  cursor: 'pointer',
  color: Colors.blueDarkAccent,
  lineHeight: '20px',
  textTransform: 'uppercase',
  margin: 0,
  display: 'grid',
  placeContent: 'center'
});

export const active_tab = css(default_tab, {
  color: `${Colors.pink} !important`,
  borderBottomColor: `${Colors.pink} !important`
});

export const disabled_tab = css(default_tab, {
  color: '#ccc !important',
  backgroundColor: '#eee !important',
  cursor: 'default',
  borderBottomColor: '#eee !important'
})

export const appointmentStatus = {
  pending: {
    fontSize: 15,
    fontWeight: 500,
    color: Colors.pinkDarkAccent
  },
  accepted: {
    fontSize: 15,
    fontWeight: 500,
    color: Colors.detoxColor
  },
  completed: {
    fontSize: 15,
    fontWeight: 500,
    color: Colors.optimal
  },
  rescheduled: {
    fontSize: 15,
    fontWeight: 500,
    color: Colors.neutral
  },
  cancelled: {
    fontSize: 15,
    fontWeight: 500,
    color: Colors.compromised
  }
}

export const avoidPageBreakInside = css({
  pageBreakInside: 'avoid !important',
  '@media print': {
    // '& > *': {
    //   pageBreakInside: 'avoid !important',
    //   margin: 0,
    //   padding: 0
    // },
    margin: 0,
    padding: 0,
    pageBreakInside: 'avoid !important',
  }
})