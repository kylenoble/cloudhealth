/* eslint-disable import/imports-first */
import React, { useEffect, useContext } from "react";
import Link from "next/link";
import { on_mobile, combined, btn, Colors } from "../Styles";
import { css } from "glamor";
import { UserContext } from "../../../context";
import UserDetails from "./User/UserDetails";

const Navigation = ({ context }) => {
  const links = [];

  const doctorNavigation = [
    {
      title: "Dashboard",
      link: "/dashboard"
    },
    {
      title: "My Schedule",
      link: `/myschedule`
    },
    {
      title: "Patient Tracker",
      link: `/patient_tracker`
    },
    {
      title: "Manage My Account",
      link: `/account`
    }
  ];

  const patientNavigation = [
    {
      title: "Dashboard",
      link: "/dashboard"
    },
    {
      title: "My Health Profile",
      link: `/healthprofile`
    },
    {
      title: "My Health Activities",
      link: `/activities`
    },
    {
      title: "Manage My Account",
      link: `/account`
    },
    {
      title: "Notifications",
      link: `/notification`
    },
    {
      title: "Health Bulletin",
      link: `/bulletin`
    }
  ];

  //  && props.logoutOnly === false || props.logoutOnly === undefined
  if (context.user.type === "Doctor") {
    links.push(...doctorNavigation);
  } else if (context.user.type === "Patient") {
    links.push(...patientNavigation);
  }

  const navlist = links.map(link => (
    <li
      id={link.title
        .split(" ")
        .join("_")
        .toLowerCase()}
      key={link.title}
      className={combined([listItem, link.link === context.path ? active : ""])}
    >
      <Link prefetch href={link.link}>
        <a
        // onClick={() => props.toggleModal("avatar")}
        >
          {link.title}
        </a>
      </Link>
    </li>
  ));

  return (
    <React.Fragment>
      {context.welcomeIsOpen || context.waiverIsOpen ? null : (
        <ul className={navlistWrapper}>{navlist}</ul>
      )}
    </React.Fragment>
  );
};

const Sidebar = props => {
  const ctx = useContext(UserContext);

  return (
    <nav
      onClick={() => props.showSidenav()}
      id="sidenav"
      className={combined(
        [
          on_mobile([], { display: "block", opacity: 0 }),
          sidenav,
          props.closeSidenavAnimate ? closing : opening
        ],
        { display: "none" }
      )}
    >
      <section
        id="list-wrapper"
        className={combined([
          listWrapper,
          props.closeSidenavAnimate ? slidingout : slidingin
        ])}
      >
        <UserDetails />
        {!ctx.welcomeIsOpen ||
          (!ctx.waiverIsOpen && <hr style={{ margin: 0, marginBottom: 10 }} />)}

        <Navigation context={ctx} />
        <hr style={{ margin: 0, marginTop: 10 }} />

        {ctx.user.type === "Doctor" ? (
          <button className={switchBtn} onClick={() => props.switchDashboard()}>
            Switch to Admin Dashboard
          </button>
        ) : null}

        <button
          id="logout-btn"
          className={combined([btn, logoutBtn])}
          style={
            ctx.user.type === "Patient" ? { borderTop: "1px solid #eee" } : null
          }
          onClick={e => {
            ctx.logout(e);
          }}
        >
          Sign Out
        </button>
      </section>
    </nav>
  );
};

export default Sidebar;

const active = css({
  background: Colors.pink,
  "> *": {
    color: "white !important"
  }
});

const switchBtn = css({
  display: "block",
  background: "white",
  padding: "10px 20px",
  color: "#000",
  borderRadius: 5,
  margin: "20px auto 0",
  border: "none",
  boxShadow: "0 5px 5px rgba(0,0,0,.1)"
});

const logoutBtn = css({
  display: "block",
  background: Colors.skyblue,
  padding: "10px 20px",
  color: "#fff",
  borderRadius: 5,
  margin: "20px auto 0",
  border: "none",
  boxShadow: "0 5px 5px rgba(0,0,0,.1)"
});

const listItem = css({
  "> a": {
    textDecoration: "none",
    padding: "10px 20px",
    display: "block",
    color: "#000"
  }
});

const navlistWrapper = css({
  listStyle: "none",
  padding: 0,
  margin: 0
});

const fadein = css.keyframes("fadein", {
  // optional name
  "0%": { opacity: 0 },
  "100%": { opacity: 1 }
});
const fadeout = css.keyframes("fadeout", {
  // optional name
  "0%": { opacity: 1 },
  "100%": { opacity: 0 }
});

const slidein = css.keyframes("slidein", {
  // optional name
  "0%": { transform: "translateX(-100%)" },
  "100%": { transform: "translateX(0%)" }
});

const slideout = css.keyframes("slideout", {
  // optional name
  "0%": { transform: "translateX(0%)" },
  "100%": { transform: "translateX(-100%)" }
});

const closing = css({
  animation: `${fadeout} 0.3s ease forwards`
});
const opening = css({
  animation: `${fadein} 0.3s ease forwards`
});

const slidingin = css({
  animation: `${slidein} 0.3s ease forwards`
});
const slidingout = css({
  animation: `${slideout} 0.3s ease forwards`
});

const listWrapper = css({
  width: "85%",
  height: "100%",
  background: "#fff",
  transform: "translateX(-100%)",
  boxShadow: "5px 0 15px rgba(0,0,0,.2)"
});

const sidenav = css({
  position: "fixed",
  display: "block",
  background: "rgba(0,0,0,.7)",
  width: "100vw",
  height: "100vh",
  top: 0,
  right: 0,
  zIndex: 100,
  transition: "300ms ease"
});
