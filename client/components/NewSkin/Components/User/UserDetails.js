import React, { useContext, memo } from "react";
import Avatar from "./Avatar";
import { UserContext } from "../../../../context";
import { combined, grid_wrapper, centered, centered_left } from "../../Styles";
import { css } from "glamor";

const UserDetails = ({ avatarSize = 60 }) => {
  const ctx = useContext(UserContext);
  const { name, company, email } = ctx.user;

  return (
    <section
      className={combined([grid_wrapper, detailsWrapper(avatarSize), centered], {
        gridColumnGap: 10
      })}
    >
      <Avatar avatar={ctx.user.avatar} size={avatarSize} />

      <article className={combined([details, grid_wrapper, centered_left])}>
        <label>
          <strong>{name}</strong>
        </label>
        <label>
          <small>{email}</small>
        </label>
        <label>
          <small>{company}</small>
        </label>
      </article>
    </section>
  );
};

export default memo(UserDetails);

let details = css({
  "> label": {
    display: "block",
    textAlign: "left",
    fontSize: '1rem'
  },
  ":nth-child(1)": {
    display: "grid",
    justifyContent: "right"
  }
});

let detailsWrapper = (avatarSize) => css({
  gridTemplateColumns: avatarSize <= 60 ? "70px 1fr" : "120px 1fr",
  padding: 25
});
