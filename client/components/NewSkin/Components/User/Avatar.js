/* eslint-disable import/newline-after-import */
/* eslint-disable import/imports-first */
import React, { useContext, useState, useEffect, memo } from "react";
import { UserContext } from "../../../../context";
import { combined, centered } from "../../Styles";
import { css } from "glamor";
import MenuModal from "../MenuModal";
import Colors from "../../Colors";

const Avatar = props => {
  const { user } = useContext(UserContext);
  const [avatar, setAvatar] = useState(user.avatar);
  const [size, setSize] = useState(props.size);

  useEffect(() => {
    setSize(props.size);

    if (user) {
      setAvatar(props.avatar);
    }
  }, [avatar]);

  return (
    <div className={combined([wrapper, centered])}>
      <figure
        id="avatar"
        className={`${combined([avatarWrapper(size)])} ${props.isOpen ? open : null}`}
        style={{ width: size, height: size }}
        onClick={props.toggleModal ? () => props.toggleModal("avatar") : null}
      >
        <img
          id="avatar"
          src={avatar ? `/static/avatar/${avatar}` : `/static/icons/user.svg`}
          alt=""
        />
      </figure>
      {props.isOpen ? (
        <MenuModal
          icon="avatar"
          toggleModal={props.toggleModal}
          switchDashboard={props.switchDashboard}
          logoutOnly={props.logoutOnly}
          needToChangePassword={props.needToChangePassword}
        />
      ) : null}
    </div>
  );
};

export default memo(Avatar);

let wrapper = css({
  position: "relative",
  padding: '0 10px'
});

let open = css({
  border: `2px solid ${Colors.skyblue}`,
  transform: "scale(.95) !important"
});

let avatarWrapper = (size) => css({
  overflow: "hidden",
  borderRadius: "100%",
  position: "relative",
  margin: 0,
  background: "#8d8d8d",
  cursor: size >= 60 ? "default" : "pointer",
  transition: "250ms ease",
  transform: "scale(1)",
  "> img": {
    position: "absolute",
    top: "50%",
    left: 0,
    width: "100%",
    transform: "translateY(-50%)"
  }
});
