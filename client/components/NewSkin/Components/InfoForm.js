import React, { useState, useEffect } from 'react'
import GridContainer from '../Wrappers/GridContainer';
import Label from './Label';
import { Colors, inputClass, changeButton, inputClassError } from '../Styles';
import { css } from 'glamor';

const InfoForm = ({ data, setData, title, cancelled }) => {
  const [editingForm, setEditingForm] = useState(false);
  const [currentData, setCurrentData] = useState('');
  const [saved, setSaved] = useState(false);
  const [error, setError] = useState(false);

  useEffect(() => {

  }, [currentData])

  const _editing = e => {
    const btn = e.target;
    const status = btn.innerHTML;

    if (status === 'Change') {
      setEditingForm(true);
      btn.innerHTML = 'Cancel'
    } else {
      cancelled(title);

      setCurrentData(null)
      setEditingForm(false);
      btn.innerHTML = 'Change'
    }
  }

  const setCurrent = (e) => {
    const cdata = e.target.value;
    setData(title, cdata, true);
    if (cdata !== data) {
      setCurrentData(cdata);
    } else {
      setCurrentData(null);
      setSaved(false);
    }
  }

  const _save = e => {
    if (currentData.length > 0) {
      setSaved(true);
      setEditingForm(false);
      setError(false);
      setData(title, currentData, true)
      e.target.nextSibling.innerHTML = 'Change'
    } else {
      setError(true);
    }
  }

  const template = '200px 1fr 120px';
  const gridStyles = {
    padding: '20px 0',
    borderBottom: '1px solid #ccc3'
  }

  return (
    <GridContainer columnSize={editingForm && title !== 'Email' ? '200px 1fr 100px 100px' : template} styles={gridStyles} >
      <Label text={title} />
      {
        editingForm ?
          <input
            type="text"
            id={title}
            required
            defaultValue={saved ? currentData : data}
            className={error ? inputClassError : inputClass}
            onChange={setCurrent}
          /> :
          <label style={{ color: saved ? Colors.movementColor : '#000' }}>{saved ? currentData : data}</label>
      }

      {
        editingForm && title !== 'Email' &&
        <button id={`${title}-save-btn`} onClick={_save} disabled={currentData ? false : true} className={currentData ? changeButton : disabledBtn}>Save</button>
      }

      <button id="trigger-btn" onClick={_editing} className={changeButton}>Change</button>
    </GridContainer >
  )
}


const disabledBtn = css(changeButton, {
  color: '#eee !important',
  cursor: 'default !important'
})

export default InfoForm;