import React from "react";
import Card from "../Wrappers/Card";
import { css } from "glamor";
import Colors from "../Colors";
import { combined } from "../Styles";

const FeatureNotInluded = () => {
  return (
    <section className={wrapper}>
      <Card classes={[combined([card])]}>
        <h3>
          This feature is not included in your current subscription. Please
          contact the officer in-charge of your account if you wish to
          subscribe.
        </h3>
      </Card>
    </section>
  );
};

const card = css({
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "50%",
  textAlign: "center"
});

const wrapper = css({
  position: "fixed",
  top: 0,
  left: 0,
  width: "100vw",
  height: "100vh",
  background: Colors.skyblue,
  zIndex: 100
});

export default FeatureNotInluded;
