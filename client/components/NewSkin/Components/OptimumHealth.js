import React from 'react';
import { css } from 'glamor';
import Colors from '../Colors';
import GridContainer from '../Wrappers/GridContainer';
import UserDetails from './User/UserDetails';

const OptimumHealth = ({ withUserDetails = false, avatarSize = 60 }) => {
  return (
    <section className={wrapper}>
      <GridContainer contain columnSize={withUserDetails ? '1fr 400px' : '1fr'}>
        {
          withUserDetails && <UserDetails avatarSize={avatarSize} />
        }
        <h1 className={withUserDetails ? toRight : ''}>Your road to{withUserDetails ? <br /> : ' '}optimum health.</h1>
      </GridContainer>
    </section>
  );
};

const toRight = css({
  textAlign: 'right !important',
  lineHeight: '40px',
  fontWeight: 300,
  padding: '0 20px',
  borderLeft: '1px solid white',
  height: 'fit-content',
  margin: 'auto 0'
})

const wrapper = css({
  width: '100%',
  padding: 10,
  background: Colors.skyblue,
  color: 'white',
  textAlign: 'center',
  marginBottom: 20
});

export default OptimumHealth;
