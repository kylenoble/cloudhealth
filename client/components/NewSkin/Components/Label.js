import React, { useState, useEffect } from 'react'
import Colors from '../Colors';
import { css } from "glamor";

const Label = ({ text = 'sample label', with_error = false, styles = {}, fullWidth = false, background = '', subheader }) => {

  const [styling, setStyling] = useState({})

  useEffect(() => {
    withBG();
  }, []);

  const withBG = () => {
    var style = {
      display: 'block',
      textAlign: 'left',
      margin: 'auto 0',
      textTransform: 'uppercase',
      color: Colors.blueDarkAccent,
      fontWeight: 500,
      width: fullWidth ? '100%' : 'fit-content',
      ...styles
    }

    if (background) {
      const s = { ...style, background, color: 'white', padding: '5px 10px' };
      setStyling(s);
    } else {
      if (subheader) {
        style = { ...style, borderBottom: `1px solid gray`, textTransform: 'none', borderBottomColor: 'gray' };
      }
      setStyling(style);
    }
  }

  return (
    <span
      data-name="label"
      style={styling}
      className={with_error ? label_error : ''}
    >{text}</span>
  )
}

const label_error = css({
  color: `${Colors.movementColor} !important`
})

export default Label;