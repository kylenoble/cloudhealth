import React, { Component } from "react";
import WaiverModal from "../../Modals/waiver";
import WelcomeModal from "../../Modals/welcome";
import TermOfUse from "../../Modals/termOfUse";
import PrivacyPolicy from "../../Modals/privacyPolicy";
import FaqModal from "../../Modals/faq.js";
import Timeout from "../../Modals/timeout";
import { css } from "glamor";
import { combined, font } from "../Styles";
import Colors from "../Colors";
import { UserContext } from "../../../context";

class NewFooter extends Component {
  currentYear = () => {
    const date = new Date();
    const currentYear = date.getFullYear();
    return currentYear;
  };

  _closeModal = str => {
    this.props.closeModal(str);
  };

  _updateStatus = val => {
    console.log("update status", val);
    this.props.updateStatus(val);
  };

  render() {
    return (
      <footer className={footerWrapper} id="footer">
        <section className={combined([linkWrapper, font(0.9)])}>
          <span
            onClick={() => this.props.openModal("privacyPolicyModalIsOpen")}
            className={link}
          >
            Privacy Policy
          </span>

          <span
            onClick={() => this.props.openModal("termOfUseModelIsOpen")}
            className={link}
          >
            Terms of Use
          </span>

          <span
            onClick={() => this.props.openModal("faqModalIsOpen")}
            className={link}
          >
            FAQ & Support
          </span>
        </section>
        <div className={combined([copyRStyle, font(0.7)])}>
          &copy; {this.currentYear()} cloudhealthasia.com
        </div>
        <WaiverModal
          modalIsOpen={this.props.state.waiverModalIsOpen}
          closeModal={this._closeModal}
          openModal={this._openModal}
          updateStatus={this._updateStatus}
        />
        <WelcomeModal
          modalIsOpen={this.props.state.welcomeModalIsOpen}
          closeModal={this._closeModal}
          updateStatus={this.props.updateStatus}
        />
        <TermOfUse
          modalIsOpen={this.props.state.termOfUseModelIsOpen}
          closeModal={this._closeModal}
        />
        <PrivacyPolicy
          modalIsOpen={this.props.state.privacyPolicyModalIsOpen}
          closeModal={this._closeModal}
        />
        <FaqModal
          modalIsOpen={this.props.state.faqModalIsOpen}
          closeModal={this._closeModal}
        />
        <Timeout
          modalIsOpen={this.props.state.timeOutModalIsOpen}
          closeModal={this._closeModal}
          updateStatus={this.props.updateStatus}
          duration={this.props.duration}
        />
      </footer>
    );
  }
}

export default NewFooter;

let linkWrapper = css({
  textAlign: "center"
});

let footerWrapper = css({
  display: "grid",
  padding: 8,
  gridTemplateColumns: "1fr",
  alignContent: "center",
  justifyContent: "center",
  background: "#fff",
  width: "100%",
  borderTop: `10px solid ${Colors.skyblue}`,
  "@media print": {
    display: "none"
  }
  // position: 'absolute',
  // bottom: 0,
  // left: 0,
});

let link = css({
  cursor: "pointer",
  padding: "0px 5px",
  borderRadius: "5px",
  color: "#000",
  ":hover": {
    borderColor: "#3498db",
    color: "#81cce7"
  },
  margin: "0 10px 0 10px",
  fontSize: ".9em"
});

let copyRStyle = css({
  color: "#ccc",
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 100%",
  fontSize: ".9em",
  justifyContent: "center",
  margin: 0,
  marginTop: 7
});
