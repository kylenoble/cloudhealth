import React from 'react';
import GridContainer from '../Wrappers/GridContainer';
import { css } from 'glamor';
import { sqrSelectedOption, sqrDefaultOption } from '../Styles';
import Colors from '../Colors';

const Checkbox = ({
  id = '',
  name = '',
  value = '',
  checked = false,
  label = 'Label',
  onChange = () => { },
  smallBox = false,
  styles = {},
  classes = []
}) => {

  const _clickHandler = (inputID) => {
    const children = document.getElementById(inputID);
    children.click();
  }

  return (
    <GridContainer
      dataID="checkbox-wrapper"
      columnSize="20px 1fr"
      gap={10}
      classes={[!checked && wrapper, ...classes]}
      styles={{ marginRight: 10, ...styles }}
      childStyles={{ textAlign: 'left' }}
      fullWidth
    >
      <div
        onClick={() => _clickHandler(id)}
        className={checked ? smallBox ? selectedOption : sqrSelectedOption : smallBox ? defaultOption : sqrDefaultOption}
      >
        <input
          id={id}
          value={value}
          name={name}
          checked={checked}
          type="checkbox"
          className={inputStyle}
          onChange={onChange}
        />
      </div>

      <label htmlFor={id} style={{ color: checked && Colors.skyblue, cursor: 'pointer' }}>{label}</label>
    </GridContainer>
  );
}

const wrapper = css({
  '& label': {
    textAlign: 'left',
    color: '#888',
  },
  ':hover': {
    '& label': {
      color: Colors.blueDarkAccent,
    },
    '> div': {
      borderColor: Colors.blueDarkAccent
    }
  }
})

const selectedOption = css(sqrSelectedOption, {
  width: 15,
  height: 15,
  margin: '7px auto 0 !important'
});

const defaultOption = css(sqrDefaultOption, {
  width: 15,
  height: 15,
  margin: '7px auto 0 !important'
});

const inputStyle = css({
  opacity: 0,
  position: 'relative',
  zIndex: 0,
  width: 1,
  height: 1
})

export default Checkbox;