import React from 'react';
import { css } from 'glamor';
import { btn_pink, btn_skyblue, btn_basic, combined, font, on_mobile, btn_disabled, btn_warning, noprint, btn_darkpink } from '../Styles';

const Button = ({ type = 'basic', children, styles = {}, classes = [], fontSize = 0.8, uniqueKey = '', center = false, left = false, right = false, fitContent = false }) => {

  const btnType = () => {
    if (type === 'pink') {
      return btn_pink;
    } else if (type === 'blue') {
      return btn_skyblue;
    } else if (type === 'disabled') {
      return btn_disabled;
    } else if (type === 'warning') {
      return btn_warning;
    } else if (type === 'darkpink') {
      return btn_darkpink;
    }
    return btn_basic;
  };

  return (
    <section
      key={uniqueKey}
      data-name="button-wrapper"
      id={`button-wrapper-${type}`}
      className={
        combined([btnType(), font(fontSize), btn, on_mobile([], { margin: '20px auto 0' }), noprint, ...classes],
          {
            cursor: type === 'disabled' ? 'default' : 'pointer',
            margin: center ? '20px auto' : left ? '0 auto 0 0' : right ? '0 0 0 auto' : null,
            maxWidth: fitContent ? 'fit-content !important' : 230,
            ...styles,
          }
        )}
    >
      {children}
    </section>
  );
};

export default Button;

const button = {
  width: '100% !important',
  borderStyle: 'none',
  outline: 'none',
  margin: '0 !important',
  background: 'none !important',
  padding: '10px 20px',
  position: 'relative !important',
  top: 0,
  left: 0,
  fontSize: 'inherit',
  transform: 'none',
  ':focus': {
    outline: 'none'
  }
};

let btn = css({
  height: 'fit-content',
  '& > *': {
    ...button,
    cursor: 'pointer',
    textDecoration: 'none',
    color: 'inherit',
    display: 'block'
  }
});
