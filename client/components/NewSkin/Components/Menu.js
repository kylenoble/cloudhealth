import React, { useContext, useState, useEffect } from "react";
import Icon from "../Icons/Icon";
import { combined, centered } from "../Styles";
import { css } from "glamor";
import MenuModal from "./MenuModal";
import Colors from "../Colors";
import { UserContext } from "../../../context";

const Menu = props => {
  const ctx = useContext(UserContext);
  const [icon] = useState(props.icon);
  const [user, setUser] = useState(ctx.user);
  const [whiteIcons, setWhiteIcons] = useState(false);

  useEffect(() => {
    setUser(ctx.user);
    setWhiteIcons(user ? user.type === "Doctor" : false);
  }, [icon]);

  const _toggleModal = () => {
    props.toggleModal(icon);
  };

  const _Color = style => {
    const atPage = props.atPage;
    const patient = user.type === "Patient";

    switch (style) {
      case "border":
        if (atPage && patient) {
          return Colors.skyblue;
        } else if (!atPage && patient) {
          return "white";
        } else if (atPage && !patient) {
          return "#fff";
        } else if (!atPage && !patient) {
          return Colors.skyblue;
        }

      case "background":
        if (atPage && patient) {
          return `${Colors.skyblue}10`;
        } else if (!atPage && patient) {
          return "white";
        } else if (atPage && !patient) {
          return `#ffffff66`;
        } else if (!atPage && !patient) {
          return Colors.skyblue;
        }
      default:
        break;
    }
  };

  return (
    <article
      className={combined([centered, iconWrapper])}
      style={{
        borderBottom: `3px solid ${_Color("border")}`,
        background: _Color("background")
      }}
    >
      <Icon
        icon={icon}
        toggleModal={_toggleModal}
        isOpen={props.isOpen}
        whiteIcons={whiteIcons}
        atPage={props.atPage}
      />

      {props.notYetRead.length > 0 ? (
        <p className={countLabel}>{props.notYetRead.length}</p>
      ) : null}

      {props.isOpen ? (
        <MenuModal
          toggleModal={_toggleModal}
          icon={props.icon}
          data={props.data}
        />
      ) : null}
    </article>
  );
};

export default Menu;

let countLabel = css({
  width: 18,
  height: 18,
  borderRadius: "100%",
  background: Colors.pink,
  color: "white",
  position: "absolute",
  bottom: -5,
  right: 0,
  top: 45,
  display: "grid",
  alignContent: "center",
  justifyContent: "center",
  fontSize: 8
});

let iconWrapper = css({
  position: "relative",
  minHeight: 35,
  height: 75,
  padding:' 0 10px'
});
