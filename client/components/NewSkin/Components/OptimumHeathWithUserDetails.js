import React from 'react';
import OptimumHealth from './OptimumHealth';

const OptimumHeathWithUserDetails = () => {
  return (
    <OptimumHealth withUserDetails avatarSize={100} />
  )
}

export default OptimumHeathWithUserDetails