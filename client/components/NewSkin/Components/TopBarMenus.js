/* eslint-disable import/imports-first */
/* eslint-disable no-lonely-if */
/* eslint-disable no-undef */
/* eslint-disable no-param-reassign */
import React, { Component } from "react";
// eslint-disable-next-line no-unused-vars
// import Icon from "../Icons/Icon";
import { UserContext } from "../../../context";
import { css } from "glamor";
import Menu from "./Menu";
import Avatar from "./User/Avatar";
import GridContaner from "../Wrappers/GridContainer";

const CHECK_INTERVAL = 5000; // in ms

class TopBarMenus extends Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
  }

  static contextType = UserContext;

  state = {
    messages: false,
    notification: false,
    confirmedPatients: false,
    appointment: false,
    avatar: false,
    notifications: [],
    element: "",
    atNotificationsPage: false,
    atBulletinPage: false,
    atConfirmedPatients: false,
    atAppointment: false,
    notYetRead: [],
    modals: {
      notification: false,
      messages: false,
      confirmedPatients: false,
      avatar: false,
      appointment: false
    }
  };

  componentDidMount() {
    this._isMounted = true;

    if (this.context.state.appointmentList.length > 0) {
      this._toggleModal(this.context.user.type === "Patient" ? "appointments" : "confirmed_patients");
    }
    const ctx_menuModal = this.context;
    const messages = ctx_menuModal.messages;
    const notification = ctx_menuModal.notification;
    const avatar = ctx_menuModal.avatar;
    const path = this.context.path;

    if (path === "/notification") {
      this.setState({ atNotificationsPage: true });
    } else if (path === "/bulletin") {
      this.setState({ atBulletinPage: true });
    } else if (path === "/dashboard?tab=confirmedPatients") {
      this.setState({ atConfirmedPatients: true });
    } else if (path === "/activities") {
      this.setState({ atAppointment: true });
    }

    this.setState({
      messages,
      notification,
      avatar
    });

    document.body.addEventListener("click", e => {
      if ((this.state.confirmedPatients !== true && this.state.appointment !== true) || e.target.id === "confirmed_patients_dismiss") {
        this.setState({
          element: e
        });
        this.documentBodyListener(e);
      }
      if (e.target.getAttribute('data-id') === "accept_appointment") {
        setTimeout(this.setState({ confirmedPatients: true }), 3000)
      }
    });

    const intervalId = setInterval(() => {
      this._checkNotifications();
    }, CHECK_INTERVAL);
    this.setState({ intervalId });
  }

  componentWillUnmount() {
    this._isMounted = false;
    document.body.removeEventListener("click", e =>
      this.documentBodyListener(this.state.element)
    );
    clearInterval(this.state.intervalId);
  }

  _toggleModal = menu => {
    const modals = { ...this.state.modals };

    const toggle = modal => this.setState({ ...modals, [modal]: true });

    if (this._isMounted) {
      switch (menu) {
        case "messages":
          if (this.state.messages) {
            return this.closeModals();
          }
          return toggle("messages");

        case "notification":
          if (this.state.notification) {
            return this.closeModals();
          }
          return toggle("notification");

        case "confirmed_patients":
        case "confirmed_patients_dismiss":
          if (this.state.confirmedPatients) {
            return this.closeModals();
          }
          return toggle("confirmedPatients");

        case "appointments":
          if (this.state.appointment) {
            return this.closeModals();
          }
          return toggle("appointment");

        default:
          if (this.state.avatar) {
            return this.closeModals();
          }
          return toggle("avatar");
      }
    }
  };

  documentBodyListener = e => {
    const id = e.target.id;
    if (
      id === "messages" ||
      id === "notification" ||
      id === "avatar" ||
      id === "confirmed_patients" ||
      id === "appointments" ||
      id === "_list-item" ||
      id === "logout-btn"
    ) {
      return;
    }

    this.closeModals();
  };

  closeModals = () => {
    if (this._isMounted) {
      this.setState({ ...this.state.modals });
    }
  };

  _checkNotifications = () => {
    const ctx = this.context;
    const notifications =
      ctx.notif_personal_info.length > 0
        ? ctx.notif_personal_info
        : ctx.notifications;
    const notYetRead = [];

    if (notifications && notifications.length > 0) {
      notifications.filter(item => {
        item.ihaveread = true;
        if (!item.read_by) {
          item.ihaveread = false;
          notYetRead.push(item);
        }
        return false;
      });
    }
    this.setState({ notYetRead, notifications });
  };

  render() {
    const ctx = this.context;
    const user = ctx.user;
    const ctx_state = ctx.state;

    const waiverIsOpen = ctx_state.waiverModalIsOpen;
    const welcomeIsOpen = ctx_state.welcomeModalIsOpen;
    const welcomeAndWaiverIsOpen = waiverIsOpen || welcomeIsOpen;

    const isMD = user.type === 'Doctor';
    const isTelemedEnabled = this.context.user.telemedicine_enabled;

    return (
      <GridContaner
        columnSize={this.context.user.type === "Patient" ? isTelemedEnabled ? "repeat(4, 1fr)" : "repeat(3, 1fr)" : "repeat(3, 1fr)"}
        styles={{ paddingRight: 50, maxWidth: isMD ? 270 : isTelemedEnabled ? 270 : 'fit-content', marginLeft: 'auto' }}
        gap={30}
      >
        {welcomeAndWaiverIsOpen ||
          ctx_state.needToChangePassword ||
          ctx_state.timeOutModalIsOpen ? (
            <>
              <div className={emptyIcon} data-name="icon-placeholder" />
              <div className={emptyIcon} data-name="icon-placeholder" />
            </>
          ) : (
            <>
              {this.context.user.type !== "Patient" ? (
                <Menu
                  icon={"confirmed_patients"}
                  isOpen={this.state.confirmedPatients}
                  notYetRead={ctx.state.appointmentList}
                  toggleModal={this._toggleModal}
                  data={[]}
                  atPage={this.state.atConfirmedPatients}
                />
              ) : this.context.user.type === "Patient" ?
                  <>
                    {
                      isTelemedEnabled &&
                      <Menu
                        icon={"appointments"}
                        isOpen={this.state.appointment}
                        notYetRead={ctx.state.appointmentList}
                        toggleModal={this._toggleModal}
                        data={[]}
                        atPage={this.state.atAppointment}
                      />
                    }
                    <Menu
                      icon="messages"
                      isOpen={this.state.messages}
                      notYetRead={[]}
                      toggleModal={this._toggleModal}
                      data={[]}
                      atPage={this.state.atBulletinPage}
                    />
                  </>
                  : null}
              <Menu
                icon="notification"
                isOpen={this.state.notification}
                toggleModal={this._toggleModal}
                notYetRead={this.state.notYetRead}
                data={this.state.notifications}
                atPage={this.state.atNotificationsPage}
              />
            </>
          )}
        {!ctx_state.timeOutModalIsOpen ? (
          <Avatar
            avatar={user.avatar}
            size={35}
            isOpen={this.state.avatar}
            toggleModal={this._toggleModal}
            switchDashboard={this.props.switchDashboard}
            logoutOnly={welcomeAndWaiverIsOpen}
            needToChangePassword={ctx.needToChangePassword}
          />
        ) : null}
      </GridContaner>
    );
  }
}

export default TopBarMenus;

TopBarMenus.contextType = UserContext;

let emptyIcon = css({
  width: 35,
  height: 35,
  background: "rgba(0,0,0,0)"
});
