/* eslint-disable no-mixed-operators */
/* eslint-disable import/imports-first */
/* eslint-disable array-callback-return */
/* eslint-disable jsx-a11y/label-has-for */
import React, { Component, useContext } from "react";
import Router from "next/router";
import { css } from "glamor";
import Link from "next/link";
import UserDetails from "./User/UserDetails";
import Colors from "../Colors";
import { UserContext } from "../../../context";
import moment from "moment-timezone";
import { combined } from "../Styles";
import { showContent } from "../Methods/NotificationModal";
import { updateNotification } from "../Services/Notifications";
import Button from "./Button";
import GridContainer from "../Wrappers/GridContainer";

const AccountMenu = props => {
  const ctx = useContext(UserContext);
  const path = ctx.path;
  const user = props.user;
  const links = [];

  const doctorNavigation = [
    {
      title: "Dashboard",
      link: "/dashboard"
    },
    {
      title: "My Schedule",
      link: `/myschedule`
    },
    {
      title: "Patient Tracker",
      link: `/patient_tracker`
    },
    {
      title: "Manage My Account",
      link: `/account`
    }
  ];

  const patientNavigation = [
    {
      title: "Dashboard",
      link: "/dashboard"
    },
    {
      title: "My Health Profile",
      link: `/healthprofile`
    },
    {
      title: "My Health Activities",
      link: `/activities`
    },
    {
      title: "Manage My Account",
      link: `/account`
    },
    {
      title: "Health Bulletin",
      link: `/bulletin`
    }
  ];

  if (user.type === "Doctor") {
    links.push(...doctorNavigation);
  } else if (
    (user.type === "Patient" && props.logoutOnly === false) ||
    props.logoutOnly === undefined
  ) {
    links.push(...patientNavigation);
  }

  const navigation = links.map(link => (
    <li
      id={link.title
        .split(" ")
        .join("_")
        .toLowerCase()}
      key={link.title}
      className={path === link.link ? active : null}
    >
      <Link prefetch href={link.link}>
        <a onClick={() => props.toggleModal("avatar")}>{link.title}</a>
      </Link>
    </li>
  ));

  return (
    <div>
      <UserDetails />
      {!props.logoutOnly && !props.needToChangePassword && (
        <React.Fragment>
          <label className={userAccountLabel}>Account</label>
          <ul className={menus}>{navigation}</ul>
        </React.Fragment>
      )}

      {user.type === "Doctor" ? (
        <button className={switchBtn} onClick={() => props.switchDashboard()}>
          Switch to Admin Dashboard
        </button>
      ) : null}
      <button
        id="logout-btn"
        className={logoutBtn}
        style={user.type === "Patient" ? { borderTop: "1px solid #eee" } : null}
        onClick={e => {
          props.toggleModal("avatar");
          props.logout(e);
        }}
      >
        Sign Out
      </button>
    </div>
  );
};

const ListWrapper = props => {
  const data = props.data ? props.data : [];
  const nyr = data.filter(d => d.ihaveread === false); // not yet read notifications
  const read = data.filter(d => d.ihaveread === true);
  const user = props.user ? props.user : [];
  const notification_count = 10;
  const notif_personal_info = nyr.length == 1 && nyr[0].name === "Personal";

  if (notif_personal_info === true) {
    return (
      <div>
        <ul className={wrapper}>
          <div>
            <li
              id="_list-item"
              className={combined([notYetRead, item])}
              onClick={() => {
                Router.push({
                  pathname: "/account",
                  query: { tab: "Profile", view: "Personal" }
                });
              }}
            >
              <p style={{ fontSize: 14 }} id="_list-item">
                {nyr[0].description}
              </p>
            </li>
          </div>
        </ul>
      </div>
    );
  }

  const showNotif = read.filter((r, i) => {
    if (i < notification_count - nyr.length) {
      return r;
    }
  });

  return (
    <div>
      {data.length > 0 ? (
        <ul className={wrapper}>
          {nyr.length > 0 ? (
            <>
              <div id="new-notifications-wrapper" className={newNotificationsWrapper}>
                <div className={newNotifs}>
                  <small>New Notifications</small>
                </div>
                {nyr.map(d => (
                  <li
                    id="_list-item"
                    key={d.id}
                    className={combined([!d.ihaveread ? notYetRead : null, item])}
                    onClick={() => {
                      showContent(d, props.user);
                      props.toggleModal(props.icon);
                      updateNotification(d, null, user);
                      props.getNotifications();
                    }}
                  >
                    <p style={{ fontSize: 14 }} id="_list-item">
                      {d.subject}
                    </p>
                    <small id="_list-item">
                      {d.sender.name} •{" "}
                      {moment(d.date_created).format("MMMM DD YYYY")}
                    </small>
                  </li>
                ))}
              </div>
              <hr />
            </>
          ) : null}

          {showNotif.map(d => (
            <li
              id="_list-item"
              key={d.id}
              className={combined([!d.ihaveread ? notYetRead : null, item])}
              onClick={() => {
                showContent(d, props.user);
                props.toggleModal(props.icon);
              }}
            >
              <p style={{ fontSize: 14 }} id="_list-item">
                {d.subject}
              </p>
              <small id="_list-item">
                {d.sender.name} •{" "}
                {moment(d.date_created).format("MMMM DD YYYY")}
              </small>
            </li>
          ))}
        </ul>
      ) : (
          <label className={noNotif}>
            <small>
              There are no{" "}
              {props.icon === "messages"
                ? "Announcement"
                : props.icon.split("_").join(" ")}
            </small>
          </label>
        )}
    </div>
  );
};

export default class MenuModal extends Component {
  static contextType = UserContext;

  _renderLabel = icon => {
    switch (icon) {
      case "messages":
        return "Announcements";

      case "notification":
        return "Notifications";

      case "confirmed_patients":
        return "Confirmed Patients";

      case "appointments":
        return "Appointments";

      case "avatar":
        return "Account";

      default:
        break;
    }
  };

  _currentTab = e => {
    this.props.currentTab(e);
  };

  renderAppointmentRequestList(appointmentList) {
    return appointmentList.map((item, i) => {
      const appointmentDateTime = item.datetime
        , remainingTime = item.remainingTime
        , remainingHours = item.remainingHours
        , remainingDays = item.remainingDays

      if (remainingTime < 0) {
        if (remainingTime < -60) {
          return (
            <li key={i} className={aptListStyle}>
              Missed Appointment: <span style={{ fontWeight: 600, color: Colors.compromised }}>Today at {moment(appointmentDateTime).format("hh:mm A")}
              </span>
            </li>
          );
        }
        return (
          <li key={i} className={aptListStyle}>
            You have an appointment <span style={{ fontWeight: 600, color: Colors.neutral }}>today</span>{" "}that started{" "}
            <span style={{ fontWeight: 600, color: Colors.pink }}>
              {Math.abs(remainingTime)} {Math.abs(remainingTime) === 1 ? `minute` : `minutes`} ago.
            </span>
          </li>
        );
      } else {
        if (remainingTime < 60) {
          return (
            <li key={i} className={aptListStyle}>
              You have an appointment <span style={{ fontWeight: 600, color: Colors.neutral }}>today</span> that will start in{" "}
              <span style={{ fontWeight: 600, color: Colors.pink }}>
                {remainingTime} {remainingTime === 1 ? `minute` : `minutes`}.
              </span>
            </li>
          );
        } else {
          if (item.isNextWk) {
            return (
              <li key={i} className={aptListStyle}>
                You have an appointment for <span style={{ fontWeight: 600, color: Colors.neutral }}>next week</span>, <span style={{ fontWeight: 600, color: Colors.pink }}>{moment(appointmentDateTime).format("dddd, hh:mmA")}</span>.
            </li>
            );
          }
          if (moment(appointmentDateTime).isBefore(moment().endOf('day'))) {
            return (
              <li key={i} className={aptListStyle}>
                You have an appointment <span style={{ fontWeight: 600, color: Colors.neutral }}>today</span> at <span style={{ fontWeight: 600, color: Colors.pink }}>{moment(appointmentDateTime).format("hh:mmA")}</span>.
            </li>
            );
          }
          if (moment(appointmentDateTime).isBefore(moment().add('1', 'days').endOf('day'))) {
            return (
              <li key={i} className={aptListStyle}>
                You have an appointment for <span style={{ fontWeight: 600, color: Colors.neutral }}>tomorrow</span>,{" "}
                <span style={{ fontWeight: 600, color: Colors.pink }}>{moment(appointmentDateTime).format("dddd, hh:mmA")}</span>.
              </li>
            );
          }
          if (remainingDays > 1) {
            return (
              <li key={i} className={aptListStyle}>
                You have an appointment on <span style={{ fontWeight: 600, color: Colors.pink }}>{moment(appointmentDateTime).format("dddd, hh:mmA")}</span>.
            </li>
            );
          }
        }
      }

      // return remainingTime < -60 ? (
      //   <li key={item.id} className={aptListStyle}>
      //     <span style={{ fontWeight: 600, color: Colors.pink }}>
      //       Lapsed Appointment{""}
      //     </span>
      //     <p>Schedule : {moment(appointmentDateTime).format("lll")}</p>
      //     <p>Concern: {item.concern}</p>
      //   </li>
      // ) : (
      //   <li className={aptListStyle}>
      //     You have an appointment today
      //     {remainingTime > 0 && remainingTime < 60 ? (
      //       <p>
      //         and will start in{" "}
      //         <span style={{ fontWeight: 600, color: Colors.pink }}>
      //           {remainingTime}
      //         </span>{" "}
      //         minutes.
      //         <p>Schedule : {moment(appointmentDateTime).format("lll")}</p>
      //       </p>
      //     ) : (
      //       <p>
      //         and will start in{" "}
      //         <span style={{ fontWeight: 600, color: Colors.pink }}>
      //           few hours.
      //         </span>{" "}
      //         <p>Schedule : {moment(appointmentDateTime).format("lll")}</p>
      //       </p>
      //     )}
      //     <div style={{ fontWeight: 600, color: Colors.pink }}>
      //       Concern: {item.concern}
      //     </div>
      //     .
      //   </li>
      // );
    });
  }

  render() {
    const { icon } = this.props;
    const ctx = this.context;

    const url =
      icon === "appointments"
        ? "activities#appointment_requests"
        : icon === "confirmed_patients"
          ? "dashboard?tab=confirmedPatients"
          : icon === "messages"
            ? "bulletin"
            : icon;

    return (
      <section className={modalWrapper} data-name="modal-wrapper">
        {icon !== "avatar" &&
          icon !== "appointments" &&
          icon !== "confirmed_patients" ? (
            <label htmlFor="" className={label}>
              {this._renderLabel(icon)}
              <Link href={`/${url}`}>
                <a onClick={() => this.props.toggleModal()}>
                  <small className={seeAllBtn}>View All</small>
                </a>
              </Link>
            </label>
          ) : null}

        {icon === "avatar" ? (
          <AccountMenu
            user={ctx.user}
            logout={ctx.logout}
            toggleModal={this.props.toggleModal}
            switchDashboard={this.props.switchDashboard}
            logoutOnly={this.props.logoutOnly}
            needToChangePassword={this.props.needToChangePassword}
          />
        ) : icon === "appointments" || icon === "confirmed_patients" ? (
          <label
            className={noNotif}
            style={{ padding: 5, textTransform: "unset" }}
          >
            {ctx.state.appointmentList.length > 0 ? <>
              <ul className={wrapper}>{this.renderAppointmentRequestList(ctx.state.appointmentList)}</ul>
              <GridContainer columnSize={'1fr 1fr'} styles={{ marginTop: 5 }}>
                <Button
                  onClick={() => this.props.toggleModal()}
                  styles={{
                    margin: "0 auto",
                    minWidth: 100,
                    width: 150,
                    fontSize: "small"
                  }}
                >
                  <button id="confirmed_patients_dismiss" style={{ padding: 8 }}>Dismiss</button>
                </Button>
                <Button type="blue"
                  onClick={() => this.props.toggleModal()}
                  styles={{
                    margin: "0 auto",
                    minWidth: 120,
                    width: 150,
                    fontSize: "small"
                  }}
                >
                  <a href={url}
                    title={"Click to view appointments"}
                    style={{ padding: 2 }}>
                    Appointments
                </a>
                </Button>
              </GridContainer></>
              : <small>You have no upcoming appointment.</small>
            }
          </label>
        ) : (
              <ListWrapper
                user={ctx.user}
                data={this.props.data}
                icon={this.props.icon}
                toggleModal={this.props.toggleModal}
                getNotifications={ctx.getNotifications}
              />
            )}
      </section>
    );
  }
}

const newNotificationsWrapper = css({
  '& li:last-child': {
    borderBottom: 'none !important'
  }
})

const aptListStyle = css({
  borderBottom: `1px dotted #eee`,
  margin: 5,
  padding: 0,
  textAlign: 'center',
  fontSize: '.8rem'
});

let active = css({
  "> a": {
    fontWeight: 600,
    color: `${Colors.skyblue} !important`
  }
});

let seeAllBtn = css({
  float: "right",
  color: "#ccc",
  transition: "250ms ease",
  ":hover": {
    color: Colors.blueDarkAccent
  }
});

let newNotifs = css({
  padding: "5px 20px",
  background: "#eee"
});

let noNotif = css({
  padding: 20,
  textAlign: "center",
  display: "block",
  textTransform: "uppercase",
  color: "#00000080"
});

let item = css({
  borderLeft: "1px solid white",
  padding: "10px 20px",
  borderBottom: "1px solid #eee",
  fontSize: ".85rem",
  transition: "250ms ease",
  cursor: "pointer",
  color: "#000",
  ":last-child": {
    borderBottom: "none"
  },
  ":hover": {
    background: "#eeeeee80",
    color: Colors.blueDarkAccent
  },
  "> small": {
    color: "#00000080"
  }
});

let notYetRead = css({
  fontWeight: 700,
  fontSize: ".8rem !important"
});

let wrapper = css({
  listStyle: "none",
  margin: 0,
  textAlign: "left",
  maxHeight: 400,
  overflow: "auto",
  "&::-webkit-scrollbar-track": {
    borderRadius: 10,
    backgroundColor: "#f2f2f2"
  },
  "&::-webkit-scrollbar": {
    width: 8,
    borderRadius: 10,
    backgroundColor: "#f2f2f2"
  },
  "&::-webkit-scrollbar-thumb": {
    borderRadius: 10,
    backgroundColor: Colors.pink
  }
});

let logoutBtn = css({
  borderStyle: "none",
  display: "block",
  width: "100%",
  boxSizing: "border-box",
  padding: "10px 20px",
  background: "#fff",
  textAlign: "left",
  cursor: "pointer",
  transition: "250ms ease",
  borderLeft: `2px solid white`,
  color: Colors.pink,
  ":hover": {
    color: Colors.skyblue
  },
  ":focus": {
    outline: "none"
  }
});

let switchBtn = css(logoutBtn, {
  color: Colors.blueDarkAccent,
  borderTop: "1px solid #eee",
  marginTop: 10
});

let menus = css({
  listStyle: "none",
  margin: 0,
  padding: 0,
  "> li a": {
    textDecoration: "none",
    textAlign: "left",
    display: "block",
    padding: "5px 20px",
    color: Colors.blueDarkAccent,
    transition: "250ms ease",
    background: "#fff",
    ":hover": {
      color: Colors.skyblue
    }
  }
});

let userAccountLabel = css({
  margin: "0 -10px",
  marginBottom: 10,
  padding: "5px 20px",
  background: "#eeeeee80",
  display: "block",
  textAlign: "left",
  borderTop: "1px solid #eee",
  borderBottom: "1px solid #eee"
});

let label = css({
  color: Colors.pink,
  display: "block",
  padding: "10px 20px",
  textAlign: "left",
  background: "#fff"
});

let modalWrapper = css({
  position: "absolute",
  top: 65,
  right: -5,
  minWidth: 400,
  padding: 10,
  background: "white",
  boxShadow: "0 10px 15px rgba(0,0,0,.1)",
  zIndex: 101,
  border: "1px solid #eeeeee7d",
  transition: "250ms ease",
  borderRadius: 5,
  "::before": {
    content: '""',
    position: "absolute",
    top: -8,
    right: 23,
    width: 15,
    height: 15,
    background: "white",
    transform: "rotate(45deg)",
    //zIndex: 0,
    borderTop: "1px solid #eeeeee7d",
    borderLeft: "1px solid #eeeeee7d",
    transition: "250ms ease"
  },
  ":hover": {
    borderColor: `${Colors.skyblue}70`,
    "::before": {
      borderTopColor: `${Colors.skyblue}70`,
      borderLeftColor: `${Colors.skyblue}70`
    }
  }
});
