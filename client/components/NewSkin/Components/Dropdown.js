import React, { useState, useEffect } from 'react';
import Button from './Button';
import { css } from 'glamor';
import Colors from '../Colors';
import { combined } from '../Styles';

const Dropdown = ({ doh = '', options = [], onChange = () => { }, onSubmit = () => { }, activeCHIforms = '' }) => {
  const [showList, setShowList] = useState(false);
  // const [btnName, setBtnName] = useState('--');
  const [animate, setAnimate] = useState(false);

  // useEffect(() => {
  //   setBtnName('--')
  //   if (options) {
  //     options.options.forEach(option => {
  //       if (option.value === options.value) {
  //         setBtnName(option.label)
  //       }
  //     });
  //   }
  // }, []);

  const onClickHandler = (label, value) => {
    showListHandler();
    // setBtnName(label);
    onChange(value, doh, false);

    setTimeout(() => {
      onSubmit("addChi", doh);
    }, 100);
  }

  const showListHandler = () => {
    if (animate) {
      setAnimate(false);
      setTimeout(() => {
        setShowList(false);
      }, 300);
    } else {
      setShowList(true);
      setTimeout(() => {
        setAnimate(true);
      }, 100);
    }
  }

  return (
    <section className={dropdownWrapper}>
      <Button styles={{ maxWidth: 'unset', borderRadius: 8, minWidth: 200, fontSize: 'small', width: 'fit-content' }} type="darkpink">
        <button onClick={showListHandler} style={{ padding: '9px 20px' }}>Select CHI</button>
      </Button>

      {
        showList &&
        <article className={combined([listWrapper, animate && expand])}>
          <div className={select}>
            <span key={0}>Select CHI</span>
            <button onClick={showListHandler} />
          </div>

          {
            options.options.map((option, i) => (
              <span
                key={i}
                id={`list-item`}
                className={activeCHIforms[doh].indexOf(option.value) > -1 ? active : listItem}
                disabled={activeCHIforms[doh].indexOf(option.value) > -1}
                data-value={option.value}
                onClick={() => { activeCHIforms[doh].indexOf(option.value) < 0 && onClickHandler(option.label, option.value) }}
              >{option.label}</span>
            ))
          }
        </article>
      }
    </section>
  );
}



const dropdownWrapper = css({
  position: 'relative',
  margin: '0 10px 0 auto',
  width: 'fit-content'
})

const listItem = css({
  padding: '10px',
  transition: '250ms ease',
  cursor: 'pointer',
  '&:hover': {
    color: Colors.skyblue,
    background: '#eeeeee5c'
  }
})

const active = css(listItem, {
  color: Colors.skyblue,
  background: '#eeeeee5c',
  cursor: 'not-allowed'
})

const expand = css({
  // maxHeight: 318,
  maxHeight: 'fit-content',
  opacity: 1
});

const select = css({
  background: Colors.pink,
  color: '#fff',
  cursor: 'default',
  padding: '5px 10px',
  display: 'grid',
  gridTemplateColumns: '1fr 15px',
  '> button': {
    background: 'none',
    borderStyle: 'none',
    color: '#fff',
    fontWeight: 700,
    cursor: 'pointer',
    width: 15,
    height: 15,
    position: 'relative',
    transform: 'rotate(45deg)',
    margin: 'auto',
    opacity: .5,
    transition: '250ms ease',
    ':hover': {
      opacity: 1
    },
    '::before': {
      content: '""',
      position: 'absolute',
      top: 0,
      left: '50%',
      transform: 'translateX(-50%)',
      height: '100%',
      width: 3,
      background: '#fff'
    },
    '::after': {
      content: '""',
      position: 'absolute',
      top: 0,
      left: '50%',
      transform: 'translateX(-50%) rotate(90deg)',
      height: '100%',
      width: 3,
      background: '#fff'
    },
    ':focus': {
      outline: 'none'
    }
  },
  '&:hover': {
    background: Colors.pink,
    color: '#fff',
  }
})

const listWrapper = css({
  display: 'grid',
  position: 'absolute',
  boxShadow: '0 5px 10px rgba(0,0,0,.1)',
  top: 0,
  left: 0,
  width: 'fit-content',
  background: '#fff',
  zIndex: 1009,
  minWidth: 230,
  overflow: 'hidden',
  height: 'fit-content',
  maxHeight: 0,
  transition: '.3s ease',
  opacity: 0,
  fontSize: 'smaller',
  minWidth: 200,
  borderRadius: 8
})

export default Dropdown;