import React from "react";
import { confirmAlert } from "react-confirm-alert"; // Import
import Modal from "react-modal";
import Colors from "../Colors";
import ModalWrapper, {
  ModalContentWrapper,
  ModalHeader,
  ModalContent,
  ModalFooter
} from "../Wrappers/Modal";
import Button from "./Button";
import { fontWeight, font, modalCustomStyles } from "../Styles";

export const showAlert = (content) => {
  confirmAlert({
    closeOnEscape: false,
    closeOnClickOutside: false,
    title: "", // Title dialog
    message: "", // Message dialog
    customUI: ({ onClose }) => message(content, onClose) // Custom UI or Component
  });
};

const displayButtons = (buttons, onClose) => {
  return buttons.map((button, i) => {
    return (
      <ModalFooter key={i}>
        <Button type="pink" styles={{ margin: "0 auto" }}>
          <a
            onClick={() => {
              button.onClick();
              onClose();
            }}
          >
            {button.label}
          </a>
        </Button>
      </ModalFooter>
    );
  });
}

const message = (content, onClose) => {
  return (
    <ModalWrapper>
      <ModalContentWrapper styles={{ height: "fit-content" }}>
        <ModalHeader
          classes={[fontWeight(700), font(1.1)]}
          styles={{ color: Colors.skyblue, textAlign: "left", marginBottom: '20px' }}
        >
          {content.title}
        </ModalHeader>
        <ModalContent>
          <p style={{width: '100%'}}>{content.message}</p>
        </ModalContent>
      </ModalContentWrapper>

      {displayButtons(content.buttons, onClose)}
    </ModalWrapper>
  );
}

const customStyles = {
  ...modalCustomStyles
}
