import React, { useEffect, useState } from 'react'
import { css } from 'glamor';
import Colors from '../Colors';

const UpdateNotification = ({ msg = '', type = 'Success' }) => {
  const [show, setShow] = useState(false);
  const [index, setIndex] = useState(0);

  useEffect(() => {
    setIndex(1002);
    setTimeout(() => {
      setShow(true);
    }, 400);

    setTimeout(() => {
      setShow(false);
      setTimeout(() => {
        setIndex(0);
      }, 500);
    }, 3000);
  }, [msg]);

  return (
    <section className={wrapper} style={{ zIndex: index }}>
      <article className={show ? showMsg : message}>
        <small>{msg}</small>
      </article>
    </section>
  );
}

const Success = css({
  color: '#1db954',
  boxShadow: `0 10px 10px -5px rgba(0,0,0,.19)`,
})

const message = css(Success, {
  display: 'grid',
  placeContent: 'center',
  background: 'white',
  padding: '10px 30px',
  width: 'fit-content',
  textTransform: 'uppercase',
  fontWeight: 700,
  borderRadius: 5,
  transition: '250ms ease',
  transform: 'translateY(-250%)',
});

const showMsg = css(message, {
  transform: 'translateY(0%)'
});

const wrapper = css({
  display: 'grid',
  placeContent: 'center',
  width: '100%',
  position: 'fixed',
  top: 120,
  left: 0,
});

export default UpdateNotification;