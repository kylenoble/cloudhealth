import React from "react";
import { css } from "glamor";
import Router from "next/router";
import Button from './Button'
import { font, fontWeight } from "../Styles";
import GridContainer from '../Wrappers/GridContainer'
import Colors from "../Colors";

export default class MenuButtons extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ...this.props
    };

    this._showTopMenu = this._showTopMenu.bind(this);
    this._updateCurrentView = this._updateCurrentView.bind(this);
  }

  render() {
    return <div className="wrap">{this._showTopMenu()}</div>;
  }

  _showTopMenu() {
    const titles = this.state.titles;

    let html = [];
    titles.forEach((currentItem, index) => {
      html.push(

        <div
          key={index}
          name={currentItem.name}
          onClick={() => this._updateCurrentView(currentItem.name)}
          className={
            this.props.activeView === currentItem.name ? Activeitem : item
          }
        >
          <Button
            classes={[font(0.75), fontWeight(700)]}
            styles={{ padding: '10px 20px' }}
            type={this.props.activeView === currentItem.name ? 'blue' : ''}>
            {currentItem.value}
          </Button>
        </div>
      );
    });

    return (
      <GridContainer contain={this.props.contain} styles={{margin: '0 auto !important'}} columnSize={`repeat(${titles.length}, 1fr)`} gap={10}>
        {html}
      </GridContainer>
    )
  }

  _updateCurrentView(name) {
    const url = `${this.state.baseLink}?tab=${name}`;
    Router.push(url);
  }
}

// MenuButtons
const item = css({
  textAlign: "center",
  textTransform: "uppercase",
  cursor: "pointer",
  color: Colors.blueDarkAccent,
  transition: '300ms ease',
  ":hover": {
    color: Colors.skyblue
  }
});

const Activeitem = css({
  textAlign: "center",
  textTransform: "uppercase"
});

// MenuTabs
let inactiveTab = css({
  fontSize: 15,
  fontWeight: 600,
  paddingBottom: 5,
  color: Colors.blueDarkAccent,
  borderBottom: `5px solid white`,
  transition: "250ms ease",
  ":hover": {
    color: Colors.pink
  },
  cursor: "pointer"
});

let activeTab = css(inactiveTab, {
  color: Colors.pink,
  borderBottom: `5px solid ${Colors.pink}`
});