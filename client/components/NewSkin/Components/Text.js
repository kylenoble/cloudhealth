import React from 'react';
// import { combined, font, fontWeight, on_mobile } from '../Styles';
import { css } from 'glamor';

const Text = ({ children, size = 1, weight = 400, color = '#000', lineHeight = 'normal', display = 'block', styles = {}, onMobileSize = 0.9, onMobileStyles = {} }) => {
  return (
    <div
      id="text-wrapper"
      className={textWrapper(size, weight, color, lineHeight, display, styles, onMobileSize, onMobileStyles)}
    >
      {children}
    </div>
  )
};

const textWrapper = (size, weight, color, lineHeight, display, styles, onMobileSize, onMobileStyles) => {
  return css({
    fontSize: `${size}em`,
    fontWeight: weight,
    color,
    lineHeight,
    display,
    ...styles,
    '> *': {
      fontSize: `${size}em`,
      fontWeight: weight,
      color,
      lineHeight,
      display,
      ...styles,
    },
    '@media(max-width: 768px)': {
      fontSize: `${onMobileSize}em`,
      ...onMobileStyles,
      '> *': {
        fontSize: `${onMobileSize}em`,
        ...onMobileStyles
      }
    }
  });
}

export default Text;
