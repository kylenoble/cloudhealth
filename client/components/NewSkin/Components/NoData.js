import React from 'react'
import { css } from 'glamor';

const NoData = ({ text = 'No Available Data.', icon = 'data' }) => {
  const iconSelector = () => {
    switch (icon) {
      case 'data':
        return '/static/icons/no_data.png'
      case 'event':
        return '/static/icons/no_event.png'

      default:
        return '/static/icons/no_data.png'
    }
  }

  return (
    <section data-name="no-data" className={wrapper}>
      <img draggable={false} src={iconSelector()} alt="" />
      <label htmlFor="">{text}</label>
    </section>
  );
}

const wrapper = css({
  width: '100%',
  height: 'fit-content',
  padding: 20,
  textAlign: 'center',
  '> img': {
    userSelect: 'none',
    opacity: .050,
    width: 100,
    display: 'block',
    margin: '0 auto 10px'
  },
  '> label': {
    color: '#eee',
    fontWeight: 600
  }
});

export default NoData;