import React, { Component } from "react";
import { css } from "glamor";
import WelcomeModal from "../../components/Modals/welcome.js";
import PrivacyPolicyModal from "../../components/Modals/privacyPolicy.js";
import TermOfUse from "../../components/Modals/termOfUse.js";
import FaqModal from "../../components/Modals/faq.js";

export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      welcomeModalIsOpen: false,
      waiverModalIsOpen: false,
      privacyPolicyModalIsOpen: false,
      faqModalIsOpen: false
    };
    // this._openPrivacyPolicy = this._openPrivacyPolicy.bind(this)
    // this._openTermOfUse = this._openTermOfUse.bind(this)
    this._openModal = this._openModal.bind(this);
    this._closeModal = this._closeModal.bind(this);
  }

  render() {
    return (
      <div className={footerDiv}>
        {/* <div className={copyRStyle}> */}
        <span
          onClick={() => this._openModal("privacyPolicyModalIsOpen")}
          className={link}
        >
          Privacy Policy
        </span>{" "}
        .
        <span
          onClick={() => this._openModal("termOfUseModelIsOpen")}
          className={link}
        >
          Terms of Use
        </span>{" "}
        .
        <span
          onClick={() => this._openModal("faqModalIsOpen")}
          className={link}
        >
          FAQ & Support
        </span>
        <WelcomeModal
          modalIsOpen={this.state.welcomeModalIsOpen}
          closeModal={this._closeModal}
          updateStatus={this._updateStatus}
        />
        <TermOfUse
          modalIsOpen={this.state.termOfUseModelIsOpen}
          closeModal={this._closeModal}
        />
        <PrivacyPolicyModal
          modalIsOpen={this.state.privacyPolicyModalIsOpen}
          closeModal={this._closeModal}
        />
        <FaqModal
          modalIsOpen={this.state.faqModalIsOpen}
          closeModal={this._closeModal}
        />
        {/* </div> */}
        <div className={copyRStyle}>&copy; 2019 cloudhealthasia.com</div>
      </div>
    );
  }

  // _openPrivacyPolicy(str){
  //   this.props.openModal(str)
  // }

  _openModal(str) {
    if (
      str === "privacyPolicyModalIsOpen" ||
      str === "termOfUseModelIsOpen" ||
      str === "faqModalIsOpen"
    ) {
      this.setState({
        [str]: true
      });
    } else {
      this.props.openModal(str);
    }
  }

  _closeModal(str) {
    this.setState({
      [str]: false
    });
  }

  // _openModal(str){
  //   this.props.openModal(str)
  // }
}

// Glamor Styling

// glamos styles
let link = css({
  cursor: "pointer",
  padding: "0px 5px",
  // border: "1px solid #52617f",
  borderRadius: "5px",
  color: "#000",
  ":hover": {
    borderColor: "#3498db",
    color: "#81cce7"
  },
  margin: "0 10px 0 10px",
  fontSize: ".9em"
});

let copyRStyle = css({
  color: "#ccc",
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 100%",
  fontSize: ".9em",
  justifyContent: "center",
  margin: 0,
  marginTop: 7
});

let footerDiv = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "center",
  alignItems: "center",
  // height: 75,
  padding: 10,
  // marginTop: 20,
  // borderTop: "10px solid #364563",
  // borderBottom: "5px solid #364563",
  // background: "#52617f",
  background: "#fff",
  color: "white",
  "@media print": {
    display: "none"
  }
});
