/* eslint-disable no-mixed-operators */
/* eslint-disable radix */
/* eslint-disable eqeqeq */
import React from 'react';
import { css } from 'glamor';
import ReactTooltip from 'react-tooltip';

import symptoms from './symptoms.js';

import AnswerService from '../../utils/answerService.js';
import SummaryService from '../../utils/summaryService.js';
import GenerateSummary from '../../utils/generateSummary.js';
import Button from '../NewSkin/Components/Button.js';
import GridContainer from '../NewSkin/Wrappers/GridContainer';
import Colors from '../NewSkin/Colors.js';
import { selectStyleOption, selectedOptionStyles } from '../NewSkin/Styles.js';


const Answer = new AnswerService();
const summary = new SummaryService();

const IntensityValues = () => {
  const values = [];
  for (let i = 0; i <= 10; i++) {
    values.push(i);
  }

  return (
    <GridContainer columnSize={'repeat(11, 1fr)'} gap={10} styles={{ color: grey, fontSize: 16 }}>
      {
        values.map((val, i) => <span key={i}>{val}</span>)
      }
    </GridContainer>
  );
};

const FrequencyValues = () => {
  const values = [];
  for (let i = 0; i <= 4; i++) {
    values.push(i);
  }
  return (
    <GridContainer columnSize={'repeat(5, 1fr)'} gap={10} styles={{ color: grey, fontSize: 16 }}>
      {
        values.map((val, i) => <span key={i}>{val}</span>)
      }
    </GridContainer>
  );
};

const Headers = () => {
  const style = { marginBottom: 20, display: 'block', color: Colors.blueDarkAccent };
  const thStyle = { padding: 30 };
  return (
    <React.Fragment>
      <th style={{ width: 400 }}><span style={style}>Symptom</span></th>
      <th style={thStyle} />
      <th><span style={style}>Intensity</span></th>
      <th style={thStyle} />
      <th><span style={style}>Frequency</span></th>
    </React.Fragment>
  );
};

const Spacer = () => (
  <tr>
    <td colSpan={5} style={{ padding: 5 }} />
  </tr>
);

const FirstHeader = ({ title = '' }) => (
    <tr>
      <td colSpan="1" style={styles.header}>{title}</td>
      <td />
      <td colSpan="1"><IntensityValues /></td>
      <td />
      <td colSpan="1"><FrequencyValues /></td>
    </tr>
  );

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = { ...symptoms };

    this._renderView = this._renderView.bind(this);

    this.scrollToTop = React.createRef();
  }

  componentDidMount() {
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      // eslint-disable-next-line no-undef
      flexibility(document.body);
    }
    // this.props.onRef(this)
    this._setDefaultvalues();

    this.setState({
      symptomName: 'Activity'
    });

    // eslint-disable-next-line no-undef
    document.querySelector('div#Activity').classList.add(catergoryWrapperActive);
  }

  componentWillUnmount() {
    this.props.onRef(undefined);
  }

  
  getSymptomName() {
    const symptomName = this.state.symptomName;
    this.setState({
      symptomName: ''
    });
    return symptomName;
  }

  _setDefaultvalues() {
    this.setState({ ...this.props.survey }, function() {
      this._checkForSubmittedAnswers();
    });
  }
  
  supportsFlexBox() {
    // eslint-disable-next-line no-undef
    const test = document.createElement('test');

    test.style.display = 'flex';

    return test.style.display === 'flex';
  }

  _calculateGroupsCompletion(answers) {
    const groups = this.state.groups;
    for (const answer of answers) {
      groups[answer.group] = {
        label: groups[answer.group].label,
        value: groups[answer.group].value,
        total: 0,
        submitted: 0
      };
      groups[answer.group].submitted += 1;
      groups[answer.group].total += 1;
    }
    return groups;
  }

  
  _renderView() {
    const that = this;

    return (

      <div style={styles.questionGroupContainer}>
        <h1 style={styles.h1}>Rate the severity or intensity of the following symptoms from 0 to 10, with 0 as none and 10 being the most severe.</h1>
        <h1 style={styles.h1}>Frequency: Rate each of the following symptoms based on your typical health profile for the past 30 days</h1>

        <GridContainer columnSize={'1fr 1fr 1fr'} gap={'5px 15px'} styles={{ textAlign: 'left', justifyContent: 'left', maxWidth: 1000, margin: '0 auto' }}>
          <span className={frequency}>0 = never or almost never have the symptom</span>
          <span className={frequency}>1 = occasionally have it, effect is not severe</span>
          <span className={frequency}>2 = occasionally have it, effect is severe</span>
          <span className={frequency}>3 = frequently have it, effect is not severe</span>
          <span className={frequency}>4 = frequently have it, effect is severe</span>
        </GridContainer>

        <div style={styles.answerContainer}>
          <div style={styles.questionContainer} >
            <table
              style={{ width: '100%' }}
            >
              <tbody>
                <tr>
                  <td colSpan="3" style={{ padding: '0' }}>
                    {that._renderQuestionGroups()}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        {
          this.state.formMessage && this.state.formMessage.active ? 
          <span style={this.state.formMessage.active ? { ...styles.formMessage, ...styles.formErrorActive } : styles.formMessage}>{this.state.formMessage.message}&nbsp;</span>
          : 
          <span style={this.state.formError.active ? { ...styles.formError, ...styles.formErrorActive } : styles.formError}>{this.state.formError.message}&nbsp;</span>
        }
        
      </div>
    );
  }

  _displaySymptomCategories() {
    const categoryNav = (type, option, i, percentComplete) => (
      <div
        data-name={`category-wrapper__${type}`}
        key={i}
        className={`${catergoryWrapper} catergory-wrapper`}
        onClick={this._selectSymptom.bind(this)}
        id={option.value}
      >
        <h3
          className={categoryHeader}
          onClick={this._selectSymptom.bind(this)}
          id={option.value}
        >
          {option.label}</h3>
        <span
          className={categoryPercentage}
          // eslint-disable-next-line no-nested-ternary
          style={{ color: type === 'completed' ? Colors.skyblue : type === 'not-completed' ? Colors.pink : '#bbb' }}
          onClick={this._selectSymptom.bind(this)}
          id={option.value}
        >{percentComplete}% Complete</span>
      </div>
    );

    const html = [];
    let i = 0;
    for (const key of Object.keys(this.state.groups)) {
      const option = this.state.groups[key];
      let percentComplete = parseInt(option.submitted / option.total * 100);
      //  percentComplete.parseInt(2)
      if (percentComplete >= 100) {
        percentComplete = 100;
        html.push(
          categoryNav('completed', option, i, percentComplete)
        );
      } else if (percentComplete > 50 && percentComplete < 100) {
        html.push(
          categoryNav('not-complete', option, i, percentComplete)
        );
      } else {
        html.push(
          categoryNav('blank', option, i, percentComplete)
        );
      }
      i++;
    }

    return html;
  }

  _selectSymptom(e) {
    const symptomName = e.target.id;
    this.setState({
      formMessage: {
        active: false
      },
      formError: {
        active: false
      }
    });
    // eslint-disable-next-line no-undef
    const categoryWrappers = document.querySelectorAll('.catergory-wrapper');

    categoryWrappers.forEach(wrapper => {
      wrapper.classList.remove(catergoryWrapperActive);
    });

    if (e.target.nodeName !== 'DIV') {
      e.target.parentNode.classList.add(catergoryWrapperActive);
    } else {
      e.target.classList.add(catergoryWrapperActive);
    }

    if (!symptomName) {
      return;
    }

    this.setState({
      symptomName,
      formError: {
        active: false,
        message: ''
      }
    });
  }

  _renderQuestionGroups() {
    if (this.state.symptomName === 'Mouth, Throat, Lungs') {
      return (
        <table
          data-name="question-groups-table"
          style={styles.tableQGroups}
        >
          <thead>
            <tr>
              <Headers />
            </tr>
          </thead>
          <tbody>
            <FirstHeader title="Mouth/Throat" />
            <Spacer />
            {this._displayQuestions([
              this.state.coughing, this.state.gagging, this.state.soarThroat,
              this.state.swollenTongue, this.state.cankerSores
            ])}
            <tr>
              <td colSpan="3" style={styles.header}>Lungs</td>
            </tr>
            <Spacer />
            {this._displayQuestions([
              this.state.chestCongestion, this.state.asthma, this.state.breathShortness,
              this.state.breathingDifficulty
            ])}
          </tbody>
        </table>
      );
    } else if (this.state.symptomName === 'Activity') {
      return (
        <table style={styles.tableQGroups}>
          <thead>
            <tr>
              <Headers />
            </tr>
          </thead>
          <tbody>
            <FirstHeader title="Joints/Muscles" />
            <Spacer />
            {this._displayQuestions([
              this.state.jointPain, this.state.arthritis, this.state.stiffness,
              this.state.musclePain, this.state.muscleWeakness
            ])}
            <tr>
              <td colSpan="3" style={styles.header}>Energy/Activity</td>
            </tr>
            <Spacer />
            {this._displayQuestions([
              this.state.fatigue, this.state.apathy, this.state.hyperactivity,
              this.state.restlessness
            ])}
            <tr>
              <td colSpan="3" style={styles.header}>Heart</td>
            </tr>
            <Spacer />
            {this._displayQuestions([
              this.state.chestPain, this.state.rapidHeartbeat
            ])}
          </tbody>
        </table>
      );
    } else if (this.state.symptomName === 'Weight and Digestion') {
      return (
        <table style={styles.tableQGroups}>
          <thead>
            <tr>
              <Headers />
            </tr>
          </thead>
          <tbody>
            <FirstHeader title="Digestive Tract" />
            <Spacer />
            {this._displayQuestions([
              this.state.nausea, this.state.diarrhea, this.state.constipation, this.state.bloated,
              this.state.belching, this.state.heartburn, this.state.intestinalPain
            ])}
            <tr>
              <td colSpan="3" style={styles.header}>Weight</td>
            </tr>
            <Spacer />
            {this._displayQuestions([
              this.state.binging, this.state.craving, this.state.excessiveWeight,
              this.state.compulsiveEating, this.state.waterRetention, this.state.underweight
            ])}
          </tbody>
        </table>
      );
    } else if (this.state.symptomName === 'Head and Mind') {
      return (
        <table style={styles.tableQGroups}>
          <thead>
            <tr>
              <Headers />
            </tr>
          </thead>
          <tbody>
            <FirstHeader title="Emotions" />
            <Spacer />
            {this._displayQuestions([
              this.state.moodSwings, this.state.anxiety,
              this.state.anger, this.state.depression
            ])}
            <tr>
              <td colSpan="3" style={styles.header}>Head</td>
            </tr>
            <Spacer />
            {this._displayQuestions([
              this.state.headaches, this.state.faintness, this.state.dizziness,
              this.state.insomnia
            ])}
            <tr>
              <td colSpan="3" style={styles.header}>Mind</td>
            </tr>
            <Spacer />
            {this._displayQuestions([
              this.state.poorMemory, this.state.confusion, this.state.poorConcentration,
              this.state.poorCoordination, this.state.difficultyDecisionMaking, this.state.stuttering,
              this.state.slurredSpeech, this.state.learningDisabilities
            ])}
          </tbody>
        </table>
      );
    } else if (this.state.symptomName === 'Eyes, Ears, Nose') {
      return (
        <table style={styles.tableQGroups}>
          <thead>
            <tr>
              <Headers />
            </tr>
          </thead>
          <tbody>
            <FirstHeader title="Eyes" />
            <Spacer />
            {this._displayQuestions([
              this.state.wateryEyes, this.state.swollenEyes, this.state.eyeBags,
              this.state.blurredVision
            ])}
            <tr>
              <td colSpan="3" style={styles.header}>Nose</td>
            </tr>
            <Spacer />
            {this._displayQuestions([
              this.state.stuffyNose, this.state.sinus, this.state.hayFever,
              this.state.sneezing, this.state.mucous
            ])}
            <tr>
              <td colSpan="3" style={styles.header}>Ears</td>
            </tr>
            <Spacer />
            {this._displayQuestions([
              this.state.itchyEars, this.state.earaches,
              this.state.earDrainage, this.state.earRinging
            ])}
          </tbody>
        </table>
      );
    } else if (this.state.symptomName === 'Other') {
      return (
        <table style={styles.tableQGroups}>
          <thead>
            <tr>
              <Headers />
            </tr>
          </thead>
          <tbody>
            <FirstHeader title="Others" />
            <Spacer />
            {this._displayQuestions([
              this.state.frequentIllness, this.state.frequentUrination,
              this.state.genitalItch
            ])}
            <tr>
              <td colSpan="3" style={styles.header}>Skin</td>
            </tr>
            <Spacer />
            {this._displayQuestions([
              this.state.acne, this.state.hives, this.state.hairLoss,
              this.state.flushing, this.state.excessiveSweating
            ])}
          </tbody>
        </table>
      );
    }
  }

  _displayOptions_intensity(input) {
    const html = [];
    const options = [
      { label: '0', value: 0 },
      { label: '1', value: 1 },
      { label: '2', value: 2 },
      { label: '3', value: 3 },
      { label: '4', value: 4 },
      { label: '5', value: 5 },
      { label: '6', value: 6 },
      { label: '7', value: 7 },
      { label: '8', value: 8 },
      { label: '9', value: 9 },
      { label: '10', value: 10 }
    ];

    for (let i = 0; i < options.length; i++) {
      if (parseInt(options[i].label) == input.value) {
        html.push(
          <span key={i} id={input.name} className={selectedOptionStyles} data-value={options[i].label} onClick={(e) => this._handleClick(e, input, 'Value')}>
            {/* {options[i].label} */}
          </span>
        );
      } else {
        html.push(
          <span key={i} id={input.name} className={selectStyleOption} data-value={options[i].label} onClick={(e) => this._handleClick(e, input, 'Value')}>
            {/* {options[i].label} */}
          </span>
        );
      }
    }
    return html;
  }

  _displayOptions_frequency(input) {
    const html = [];
    const options = [
      { label: '0', value: 0 },
      { label: '1', value: 1 },
      { label: '2', value: 2 },
      { label: '3', value: 3 },
      { label: '4', value: 4 }
    ];

    for (let i = 0; i < options.length; i++) {
      if (parseInt(options[i].label) == input.count) {
        html.push(
          <span key={i} id={input.name} className={selectedOptionStyles} data-value={options[i].label} onClick={(e) => this._handleClick(e, input, 'Count')}>
            {/* {options[i].label} */}
          </span>
        );
      } else {
        html.push(
          <span key={i} id={input.name} className={selectStyleOption} data-value={options[i].label} onClick={(e) => this._handleClick(e, input, 'Count')}>
            {/* {options[i].label} */}
          </span>
        );
      }
    }
    return html;
  }

  checkLabel = label => !!(label === 'Slurred speech' || label === 'Fatigue, sluggishness' || label === 'Apathy, lethargy' || label === 'Canker or cold sores' || label === 'Flushing, hot flashes' || label === 'Binge eating/drinking')


  tooltipWrapper = label => {
    let description = '';

    if (label === 'Slurred speech') {
      description = 'You speak words/phrases/sentences that are intelligible or hard to understand.';
    } else if (label === 'Fatigue, sluggishness') {
      description = 'Slowing down of movements';
    } else if (label === 'Apathy, lethargy') {
      description = 'Lack of interest or emotion/ inability to move or do activities that you previously do not have problems with.';
    } else if (label === 'Canker or cold sores') {
      description = 'One or multiple sores in your mouth ( Filipino: singaw).';
    } else if (label === 'Flushing, hot flashes') {
      description = 'Sudden redness and heat felt in your face.';
    } else if (label === 'Binge eating/drinking') {
      description = 'Too much consumption of food or drink in one sitting.';
    }

    return `
      <div style="max-width: 400px; border-top-left-radius: 3px; border-top-right-radius: 3px; overflow: hidden">
        <small style="padding: 5px; background: #364563; display: block; text-align:center; color: white">${label}</small>
        <p style="padding: 15px;"> ${description}</p>
      </div>`;
  }

  _displayQuestions(inputs) {
    const html = [];
    let i = 0;
    for (const input of inputs) {
      html.push(
        <React.Fragment key={i}>
          <tr>
            <td style={styles.input}>
              {this.checkLabel(input.label) ?
                <span>
                  <ReactTooltip />
                  <span
                    style={{ color: input.error && input.error.active ? Colors.movementColor : Colors.blueDarkAccent }}
                    className={infoWrapper}
                    data-tip={this.tooltipWrapper(input.label)}
                    data-type="light"
                    data-html
                  >{input.label}
                    <small className={info}>i</small>
                  </span></span> :
                <span style={{ color: input.error && input.error.active ? Colors.movementColor : Colors.blueDarkAccent }}>{input.label}</span>
              }
            </td>
            <td />
            <td>
              <GridContainer columnSize={'repeat(11, 1fr)'} gap={10}>
                {this._displayOptions_intensity(input)}
              </GridContainer>
            </td>
            <td />
            <td>
              <GridContainer columnSize={'repeat(5, 1fr)'} gap={10}>
                {this._displayOptions_frequency(input)}
              </GridContainer>
            </td>
          </tr>
          <tr>
            <td colSpan={5}>
              <hr style={{ margin: '8px 0' }} />
            </td>
          </tr>
        </React.Fragment>
      );
      i++;
    }

    return html;
  }

  _handleClick(e, input, type) {
    const name = e.target.id;
    const value = e.target.dataset.value;
    // let value = e.target.innerText;

    const validated = this._validateInput(value, 'select');
    // //if validated is true, error.active should be false and vice versa
    this._updateChange(value, name, !validated, type);
  }

  _updateChange(value, input, validated, type) {
    // const inputName = input.name
    const newInput = this.state[input];

    if (type === 'Count') {
      newInput.count = value;
      // newInput.value = input.value
    } else {
      newInput.value = value;
      // newInput.count = input.count
    }
    newInput.modified = true;

    if (newInput.error) {
      newInput.error.active = validated;
    }
    this.setState({
      newInput
    });
  }

  _validateInput(input) {
    const ck_required = /^[0-9]+/;
    if (input === null || input === '' || !ck_required.test(input)) {
      return false;
    }
    return true;
  }

  _checkForUnansweredAndModified() {
    let notSubmitted = [];
    for (const key of Object.keys(this.state)) {
      const item = this.state[key];
      if (this.state[key].group === this.state.symptomName && key !== 'formError' && key !== 'symptomName' && key !== 'groups') {
        if (!item.submitted || item.modified) {
          notSubmitted.push(item);
        }
      }
    }
    notSubmitted = this._removeDuplicates(notSubmitted);
    return notSubmitted;
  }

  _checkForSubmittedAnswers() {
    let item;
    const groups = this.state.groups;
    let submitted = 0;

    for (const key of Object.keys(this.state)) {
      if (key !== "formError" && key !== "groups" && key !== "symptomName") {
        item = this.state[key];
        groups[item.group] = {
          label: groups[item.group].label,
          value: groups[item.group].value,
          total: groups[item.group].total,
          submitted: 0
        };
        if (item.submitted) {
          submitted++;
          groups[item.group].submitted = submitted;
        }
      }
    }
    this.setState({ groups });
  }

  _removeDuplicates(arr) {
    const uniques = [];
    const itemsFound = {};
    for (let i = 0, l = arr.length; i < l; i++) {
      const stringified = JSON.stringify(arr[i]);
      // eslint-disable-next-line no-continue
      if (itemsFound[stringified]) { continue; }
      uniques.push(arr[i]);
      itemsFound[stringified] = true;
    }
    return uniques;
  }

  _allInputsValid() {
    const results = [];
    for (const key of Object.keys(this.state)) {
      if (!this._validateInput(this.state[key].value) && !this._validateInput(this.state[key].count) && key !== 'formError' && key !== 'symptomName' && key !== 'groups' && this.state[key].group === this.state.symptomName) {
        results.push(this.state[key]);
        this.state[key].error.active = true;
      }
    }    
    if (results.length > 0) {
      return false;
    }
    return true;
  }

  _goToNext() {
    if (!this._allInputsValid()) {
      // add form error validation
      this.setState({
        formError: {
          active: true,
          message: 'Please complete all fields'
        }
      });
      return;
    } 
    const summaryGenerator = new GenerateSummary(this.state, 'msq');
    const msqSummary = [{
      name: 'msq',
      group: 'health',
      label: 'MSQ',
      value: summaryGenerator.generateSummary()
    }];

    const unanswered = this._checkForUnansweredAndModified();
    const newGroups = this._calculateGroupsCompletion(unanswered);

    const newGroupsArray = Object.entries(newGroups);

    const emptyGroupCount = newGroupsArray.filter((val) => val[1].submitted == 0).length;

    if (unanswered.length < 1) {
        //this.props.onClick('post');
      if (this.props.location === 'profile') {
        this.setState({
            // symptomName: '',
          groups: newGroups
        });
      }
    } else {
      Answer.create(unanswered)
          .then(res => {
            if (res) {
              this.setState({
                formError: {
                  active: false
                }
              });
              if (emptyGroupCount < 1) {
                summary.create(msqSummary)
                  .then(result => {
                    console.log('should move on -- 1')
                    if (this.props.location === 'profile') {
                      //this._setDefaultvalues();
                      this.setState({
                        // symptomName: '',
                        groups: newGroups
                      });
                    }
                    this.setState({
                      formMessage: {
                        active: true,
                        message: 'Your profile has been successfully updated'
                      },
                      msq_completed: true
                    }, function(){
                      if(this.state.symptomName === 'Other'){
                        console.log('xxxxx')
                      }
                    })
                  })
                  .catch(error => {
                    console.log(error);
                    this.setState({
                      formError: {
                        active: true,
                        message: 'There was an error updating your profile'
                      }
                    });
                  });
              } else {
                console.log('should move on -- 2');
                if (this.props.location === 'profile') {
                  this.setState({
                    // symptomName: '',
                    groups: newGroups
                  });
                }
                this.setState({
                  formMessage: {
                    active: true,
                    message: 'Your profile has been successfully updated'
                  }
                }, function(){
                  // scrolls user into top section
                  //.current is verification that your element has rendered
                  if (this.scrollToTop.current) {
                    setTimeout(() => {
                      this.scrollToTop.current.scrollIntoView({
                        behavior: "smooth",
                        block: "nearest"
                      });
                    }, 1000);
                  }
                })
              }
            } else {
              this.setState({
                formError: {
                  active: true,
                  message: 'Please complete all fields'
                }
              });
              //console.log(error);
            }
          })
          .catch(e => {
            this.setState({
              formError: {
                active: true,
                message: 'Error submitting form'
              }
            });
            console.log(e);
          });  // you would show/hide error messages with component state here
    }
  }

  render() {
    return (
      <div style={styles.mainContainer}>
        <div style={styles.container}>
          <div style={styles.introInfo}>
            <h1 style={{ marginTop: 0 }} ref={this.scrollToTop}>Symptoms Review</h1>
            <span>The following questions will help us assess your current health issues and the symptoms that you are currently experiencing.<br />Data on this part of the survey form will enable us to set the priority areas in managing your health.</span>
          </div>

          <GridContainer
            columnSize={'repeat(6, 1fr)'}
            gap={5}
            styles={{ borderBottom: `1px solid ${Colors.pink}`, margin: '20px auto', width: '100%' }}
          >
            {this._displaySymptomCategories()}
          </GridContainer>

          {this._renderView()}
          <Button type={this.state.msq_completed === true ? 'blue' : 'basic'}>
            <button onClick={() => this.props.back()}>Back</button>
          </Button>
          <Button type="pink" styles={{ marginLeft: 20 }}>
            <button style={styles.button} onClick={this._goToNext.bind(this)}>
              Update My Profile
            </button>
          </Button>
        </div>
      </div>
    );
  }

}

// const grey = '#737373'
const grey = '#bbb';

const categoryPercentage = css({
  fontSize: 15,
  color: grey
});

const categoryHeader = css({
  fontSize: 16,
  color: Colors.blueDarkAccent,
  fontWeight: 600,
  margin: 0
});

const catergoryWrapper = css({
  cursor: 'pointer',
  paddingBottom: 5,
  borderBottom: `5px solid rgba(0,0,0,0)`,
  transition: '250ms ease',
  '> h3': {
    fontSize: 15
  },
  '> span': {
    display: 'block'
  }
});

const catergoryWrapperActive = css({
  borderBottomColor: `${Colors.pink} !important`,
  '> h3': {
    color: Colors.pink
  }
});

const frequency = css({
  textAlign: 'left !important',
  fontSize: 14,
  color: grey
});


let infoWrapper = css({
  display: 'grid',
  gridTemplateColumns: '90% 10%',
  ':hover > small': {
    borderColor: '#000',
    color: '#000'
  },
  cursor: 'help'
});

let info = css({
  width: 20,
  height: 20,
  borderRadius: '100%',
  border: '2px solid #ccc',
  display: 'grid',
  justifyContent: 'center',
  alignContent: 'center',
  fontWeight: 600,
  transition: '250ms ease',
  color: '#ccc',
});

const styles = {
  mainContainer: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  container: {
    fontFamily: '"Roboto",sans-serif',
    JsDisplay: 'flex',
    display: 'flex',
    flexWrap: 'wrap',      /* NEW, Spec - Opera 12.1, Firefox 20+ */
    flex: '1 0 100%',
    //  flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  h1: {
    color: Colors.blueDarkAccent,
    fontSize: 16,
    fontWeight: 500,
    fontStyle: 'italic',
    margin: 0,
    textAlign: 'left',
  },
  header: {
    fontSize: '17px',
    color: Colors.blueDarkAccent,
    fontWeight: 'bold',
    letterSpacing: '5px',
    textAlign: 'left',
    paddingLeft: '8px'
  },
  tableQGroups: {
    width: '100%',
    tableLayout: 'auto',
    margin: '0'
  },
  introInfo: {
    flex: '1 0 100%',
    fontFamily: '"Roboto",sans-serif',
    color: '#364563',
    fontSize: '14px',
    padding: '10px 20px',
    textAlign: 'center',
  },
  numberDirections: {
    JsDisplay: 'flex',
    display: 'flex',             /* NEW, Spec - Opera 12.1, Firefox 20+ */
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'left',
    justifyContent: 'left',
    textAlign: 'left',
    marginBottom: '20px',
  },
  directionsGroup: {
    JsDisplay: 'flex',
    display: 'flex',             /* NEW, Spec - Opera 12.1, Firefox 20+ */
    flexDirection: 'column',
    flexWrap: 'wrap',
    alignItems: 'left',
    justifyContent: 'left cene',
    textAlign: 'left',
    marginRight: '20px',
    color: '#364563',
    flex: 1,
    fontSize: '14px',
  },
  questionGroupContainer: {
    textAlign: 'center',
    width: '100%'
  },
  answerContainer: {
    JsDisplay: 'flex',
    display: 'flex',             /* NEW, Spec - Opera 12.1, Firefox 20+ */
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    justifyContent: 'center',
    textAlign: 'center',
  },
  title: {
    color: '#364563',
    fontSize: '18px',
    fontFamily: '"Roboto-Medium",sans-serif',
    marginBottom: '5px',
  },
  bodies: {
    marginBottom: '20px',
    JsDisplay: 'flex',
    display: 'flex',             /* NEW, Spec - Opera 12.1, Firefox 20+ */
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
  },
  bodyContainer: {
    flex: '1 0 auto',
    height: '240px',
    minWidth: '216px',
    maxWidth: '300px',
    boxShadow: '1px 2px 3px 0px rgba(0,0,0,0.10)',
    borderRadius: '6px',
    JsDisplay: 'flex',
    display: 'flex',             /* NEW, Spec - Opera 12.1, Firefox 20+ */
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '20px',
    margin: '25px',
    cursor: 'pointer',
  },
  bodyContainerComplete: {
    backgroundColor: 'rgba(183, 217, 124, 0.35)',
  },
  bodyContainerMid: {
    backgroundColor: 'rgba(247, 204, 111, 0.35)',
  },
  bodyContainerLow: {
    backgroundColor: 'rgba(240, 154, 151, 0.35)',
  },
  groups: {
    marginRight: '10px',
  },
  questionContainer: {
    margin: 30,
    flexGrow: 1,
    flexShrink: 1,
    flex: '1 1 25%',
    maxWidth: '100%',
  },
  questionTable: {
    marginLeft: 'auto',
    marginRight: 'auto',
    tableLayout: 'auto'
  },
  input: {
    fontSize: '18px',
    textAlign: 'left',
    paddingLeft: '20px'
  },
  label: {
    color: '#364563',
    fontFamily: '"Roboto-Medium",sans-serif',
    marginBottom: '30px'
  },
  smallLabel: {
    position: 'absolute',
    color: '#364563',
    fontSize: '13px',
    fontFamily: '"Roboto-Medium",sans-serif',
    marginTop: '30px',
    maxWidth: '350px',
  },
  options: {
    fontSize: '14px',
    fontFamily: '"Roboto",sans-serif',
    WebkitAppearance: 'none',
    MozAppearance: 'none',
    appearance: 'none',
    width: '50px',
    backgroundColor: '#fff',
    borderColor: '#d9d9d9 #ccc #b3b3b3',
    borderRadius: '0',
    border: 'none',
    borderBottom: '1px solid #ccc',
    color: '#333',
    borderSpacing: '0',
    borderCollapse: 'separate',
    height: '20px',
    outline: 'none',
    overflow: 'hidden',
    position: 'relative',
    marginRight: '5px',
    paddingLeft: '20px',
    cursor: 'pointer',
  },
  formMessage: {
    fontSize: '1rem',
    marginBottom: '25px',
    color: "rgb(3, 210, 127)",
    transition: 'color 0.3s ease-in-out',
    fontWeight: 500
  },
  formError: {
    display: "none",
    fontSize: '1rem',
    marginBottom: '25px',
    color: '#EA3131',
    transition: 'color 0.3s ease-in-out',
    fontWeight: 500
  },
  formErrorActive: {
    display: 'block'
  },
  formSuccess: {
    display: "none",
    fontSize: "1rem",
    marginBottom: "25px",
    color: "rgb(3, 210, 127)",
    transition: "color 0.3s ease-in-out",
    fontWeight: 500
  },
  formSuccessActive: {
    display: "block"
  },
  button: {
    backgroundColor: '#ED81B9',
    width: '300px',
    // padding: '11px 80px',
    border: 'none',
    borderRadius: '6px',
    color: 'white',
    fontFamily: '"Roboto",sans-serif',
    fontSize: '14px',
    margin: '30px 0px 20px 0px',
    cursor: 'pointer',
  },
};

