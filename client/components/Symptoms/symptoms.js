const symptoms = {
    nausea: {
        id: 63,
        name: 'nausea',
        label: 'Nausea, vomiting',
        group: 'Weight and Digestion',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    diarrhea: {
        id: 64,
        name: 'diarrhea',
        label: 'Diarrhea',
        group: 'Weight and Digestion',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    constipation: {
        id: 65,
        name: 'constipation',
        label: 'Constipation',
        group: 'Weight and Digestion',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    bloated: {
        id: 66,
        name: 'bloated',
        label: 'Bloated feeling',
        group: 'Weight and Digestion',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    belching: {
        id: 67,
        name: 'belching',
        label: 'Belching, passing gas',
        group: 'Weight and Digestion',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    heartburn: {
        id: 68,
        name: 'heartburn',
        label: 'Heartburn, indigestion',
        group: 'Weight and Digestion',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    intestinalPain: {
        id: 69,
        name: 'intestinalPain',
        label: 'Intestinal stomach pain',
        group: 'Weight and Digestion',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    itchyEars: {
        id: 112,
        name: 'itchyEars',
        label: 'Itchy ears',
        group: 'Eyes, Ears, Nose',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    earaches: {
        id: 113,
        name: 'earaches',
        label: 'Earaches, Ear Infections',
        group: 'Eyes, Ears, Nose',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    earDrainage: {
        id: 114,
        name: 'earDrainage',
        label: 'Drainage from ear',
        group: 'Eyes, Ears, Nose',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    earRinging: {
        id: 115,
        name: 'earRinging',
        label: 'Ringing in ears, hearing loss',
        group: 'Eyes, Ears, Nose',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    moodSwings: {
        id: 96,
        name: 'moodSwings',
        label: 'Mood swings',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    anxiety: {
        id: 97,
        name: 'anxiety',
        label: 'Anxiety, fear, nervousness',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    anger: {
        id: 98,
        name: 'anger',
        label: 'Anger irritability, aggressiveness',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    depression: {
        id: 99,
        name: 'depression',
        label: 'Depression',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    fatigue: {
        id: 85,
        name: 'fatigue',
        label: 'Fatigue, sluggishness',
        group: 'Activity',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    apathy: {
        id: 86,
        name: 'apathy',
        label: 'Apathy, lethargy',
        group: 'Activity',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    hyperactivity: {
        id: 87,
        name: 'hyperactivity',
        label: 'Hyperactivity',
        group: 'Activity',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    restlessness: {
        id: 88,
        name: 'restlessness',
        label: 'Restlessness',
        group: 'Activity',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    wateryEyes: {
        id: 116,
        name: 'wateryEyes',
        label: 'Watery or itchy eyes',
        group: 'Eyes, Ears, Nose',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    swollenEyes: {
        id: 117,
        name: 'swollenEyes',
        label: 'Swollen, reddened, or sticky eyelids',
        group: 'Eyes, Ears, Nose',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    eyeBags: {
        id: 118,
        name: 'eyeBags',
        label: 'Bags or dark circles under eyes',
        group: 'Eyes, Ears, Nose',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    blurredVision: {
        id: 119,
        name: 'blurredVision',
        label: 'Blurred or tunnel vision (does not include near- or far-sightedness)',
        group: 'Eyes, Ears, Nose',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    headaches: {
        id: 100,
        name: 'headaches',
        label: 'Headaches',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    faintness: {
        id: 101,
        name: 'faintness',
        label: 'Faintness',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    dizziness: {
        id: 102,
        name: 'dizziness',
        label: 'Dizziness',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    insomnia: {
        id: 103,
        name: 'insomnia',
        label: 'Insomnia',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    chestPain: {
        id: 94,
        name: 'chestPain',
        label: 'Chest pain or pressure',
        group: 'Activity',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    rapidHeartbeat: {
        id: 95,
        name: 'rapidHeartbeat',
        label: 'Rapid or pounding heartbeat',
        group: 'Activity',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    jointPain: {
        id: 89,
        name: 'jointPain',
        label: 'Pain or aches in joints',
        group: 'Activity',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    arthritis: {
        id: 90,
        name: 'arthritis',
        label: 'Arthritis',
        group: 'Activity',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    stiffness: {
        id: 91,
        name: 'stiffness',
        label: 'Stiffness or limitation of movement',
        group: 'Activity',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    musclePain: {
        id: 92,
        name: 'musclePain',
        label: 'Pain or aches in muscles',
        group: 'Activity',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    muscleWeakness: {
        id: 93,
        name: 'muscleWeakness',
        label: 'Feeling of muscle weakness or tiredness',
        group: 'Activity',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    chestCongestion: {
        id: 76,
        name: 'chestCongestion',
        label: 'Chest congestion',
        group: 'Mouth, Throat, Lungs',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    asthma: {
        id: 77,
        name: 'asthma',
        label: 'Asthma, wheezing, bronchitis',
        group: 'Mouth, Throat, Lungs',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    breathShortness: {
        id: 78,
        name: 'breathShortness',
        label: 'Shortness of breath',
        group: 'Mouth, Throat, Lungs',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    breathingDifficulty: {
        id: 79,
        name: 'breathingDifficulty',
        label: 'Difficulty breathing',
        group: 'Mouth, Throat, Lungs',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    poorMemory: {
        id: 104,
        name: 'poorMemory',
        label: 'Poor memory',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    confusion: {
        id: 105,
        name: 'confusion',
        label: 'Confusion, poor comprehension',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    poorConcentration: {
        id: 106,
        name: 'poorConcentration',
        label: 'Poor concentration',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    poorCoordination: {
        id: 107,
        name: 'poorCoordination',
        label: 'Poor physical coordination',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    difficultyDecisionMaking: {
        id: 108,
        name: 'difficultyDecisionMaking',
        label: 'Difficulty in making decisions',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    stuttering: {
        id: 109,
        name: 'stuttering',
        label: 'Stuttering or stammering',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    slurredSpeech: {
        id: 110,
        name: 'slurredSpeech',
        label: 'Slurred speech',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    learningDisabilities: {
        id: 111,
        name: 'learningDisabilities',
        label: 'Learning disabilities',
        group: 'Head and Mind',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    coughing: {
        id: 80,
        name: 'coughing',
        label: 'Chronic coughing',
        group: 'Mouth, Throat, Lungs',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    gagging: {
        id: 81,
        name: 'gagging',
        label: 'Gagging, frequent need to clear throat',
        group: 'Mouth, Throat, Lungs',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    soarThroat: {
        id: 82,
        name: 'soarThroat',
        label: 'Sore throat, hoarseness, loss of voice',
        group: 'Mouth, Throat, Lungs',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    swollenTongue: {
        id: 83,
        name: 'swollenTongue',
        label: 'Swollen or discolored tongue, gum, lips',
        group: 'Mouth, Throat, Lungs',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    cankerSores: {
        id: 84,
        name: 'cankerSores',
        label: 'Canker or cold sores',
        group: 'Mouth, Throat, Lungs',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    stuffyNose: {
        id: 120,
        name: 'stuffyNose',
        label: 'Stuffy Nose',
        group: 'Eyes, Ears, Nose',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    sinus: {
        id: 121,
        name: 'sinus',
        label: 'Sinus problems',
        group: 'Eyes, Ears, Nose',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    hayFever: {
        id: 122,
        name: 'hayFever',
        label: 'Hay fever (allergic rhinitis)',
        group: 'Eyes, Ears, Nose',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    sneezing: {
        id: 123,
        name: 'sneezing',
        label: 'Sneezing attacks',
        group: 'Eyes, Ears, Nose',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    mucous: {
        id: 124,
        name: 'mucous',
        label: 'Excessive mucous formation',
        group: 'Eyes, Ears, Nose',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    acne: {
        id: 125,
        name: 'acne',
        label: 'Acne',
        group: 'Other',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    hives: {
        id: 126,
        name: 'hives',
        label: 'Hives, rashes, dry skin',
        group: 'Other',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    hairLoss: {
        id: 127,
        name: 'hairLoss',
        label: 'Hair loss',
        group: 'Other',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    flushing: {
        id: 128,
        name: 'flushing',
        label: 'Flushing, hot flashes',
        group: 'Other',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    excessiveSweating: {
        id: 129,
        name: 'excessiveSweating',
        label: 'Excessive sweating',
        group: 'Other',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    binging: {
        id: 70,
        name: 'binging',
        label: 'Binge eating/drinking',
        group: 'Weight and Digestion',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    craving: {
        id: 71,
        name: 'craving',
        label: 'Craving certain foods',
        group: 'Weight and Digestion',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    excessiveWeight: {
        id: 72,
        name: 'excessiveWeight',
        label: 'Excessive weight, inability to lose weight',
        group: 'Weight and Digestion',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    compulsiveEating: {
        id: 73,
        name: 'compulsiveEating',
        label: 'Compulsive eating',
        group: 'Weight and Digestion',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    waterRetention: {
        id: 74,
        name: 'waterRetention',
        label: 'Water retention',
        group: 'Weight and Digestion',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    underweight: {
        id: 75,
        name: 'underweight',
        label: 'Underweight, inability to gain weight',
        group: 'Weight and Digestion',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    frequentIllness: {
        id: 130,
        name: 'frequentIllness',
        label: 'Frequent illness, slow recovery from illness',
        group: 'Other',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    frequentUrination: {
        id: 131,
        name: 'frequentUrination',
        label: 'Frequent or urgent urination',
        group: 'Other',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    genitalItch: {
        id: 132,
        name: 'genitalItch',
        label: 'Genital itch or discharge',
        group: 'Other',
        value: null,
        count: null,
        submitted: false,
        modified: false,
        error: {
          active: false
        },
        value: null
    },
    formError: {
        active: false,
        message: ''
    },
    symptomName: '',
    groups: {
        'Activity': {
            label: 'Muscle, Energy, Heart',
            value: 'Activity',
            total: 11,
            submitted: 0
        },
        'Eyes, Ears, Nose': {
            label: 'Eyes, Ears, Nose',
            value: 'Eyes, Ears, Nose',
            total: 13,
            submitted: 0
        },
        'Head and Mind': {
            label: 'Head and Mind',
            value: 'Head and Mind',
            total: 16,
            submitted: 0
        },
        'Mouth, Throat, Lungs': {
            label: 'Mouth, Throat, Lungs',
            value: 'Mouth, Throat, Lungs',
            total: 9,
            submitted: 0
        },
        'Weight and Digestion': {
            label: 'Weight and Digestion',
            value: 'Weight and Digestion',
            total: 13,
            submitted: 0
        },
        'Other': {
            label: 'Others',
            value: 'Other',
            total: 8,
            submitted: 0
        },
    }
}

export default symptoms;
