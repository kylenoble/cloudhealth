import React, { Component } from "react";
import Header from "./Header";
import { css } from "glamor";

const BG_COLOR = "#364563";
const staticPath = "/static/images/";
const CLOUD = `${staticPath}logo-cloud-only.svg`;
const LOGO = `${staticPath}logo.svg`;
const TITLE = `${staticPath}logo-title-only.svg`;
const GEAR = `${staticPath}gear.svg`;
const CHECKMARK = `${staticPath}checkmark.svg`;

export default class extends Component {
  state = {
    showMaintenance: false
  };
  componentDidMount() {
    this.showMaintenance();
  }

  showMaintenance = () => {
    setTimeout(() => {
      this.setState((prevState, props) => ({
        showMaintenance: !prevState.showMaintenance
      }));
    }, 1000);
  };

  render() {
    let mtntitle = this.props.isDone
      ? "Maintenance Done!"
      : `Sorry, we're down for maintenance.`;
    let sub = this.props.isDone
      ? "You will be redirected to your Dashboard Shortly..."
      : `We'll be back shortly.`;

    if (this.props.message) {
      mtntitle = "Sorry, this page is not available";
      sub = "";
    }
    return (
      <div
        className={`${mainWrapper}`}
        style={this.state.showMaintenance ? { opacity: 1 } : { opacity: 0 }}
      >
        <Header
          title={
            this.props.isDone
              ? "Site Maintenance Done"
              : this.props.message
              ? "Page Not Available"
              : "Site Under Maintenance"
          }
          isDone={this.props.isDone}
        />
        <section className={maintenanceWrapper}>
          <figure className={logoContainer}>
            <img
              draggable="false"
              className={`${cloud} ${unselect}`}
              src={CLOUD}
              alt=""
            />
            <img
              draggable="false"
              className={`${title} ${unselect}`}
              src={TITLE}
              alt=""
            />
          </figure>

          <div className={gearWrapper}>
            <figure
              id="gearContainer"
              style={{ marginTop: this.props.isDone ? 5 : 0 }}
              className={`${gearContainer} ${rule}`}
            >
              <img
                draggable="false"
                className={`${gear} ${unselect} ${
                  this.props.isDone ? null : animate
                }`}
                src={this.props.isDone ? CHECKMARK : GEAR}
              />
            </figure>
          </div>

          <article className={messageWrapper}>
            <h1 className={`${unselect}`}>{mtntitle}</h1>
            <h3 className={`${unselect}`}>{sub}</h3>
            <a href="https://cloudhealthasia.com/">cloudhealthasia.com</a>
          </article>
        </section>
      </div>
    );
  }
}

let show = css({
  opacity: "1 !important"
});

let unselect = css({
  userSelect: "none",
  userDrag: "none"
});

let gearWrapper = css({
  width: "100%",
  height: "300px",
  position: "absolute",
  top: 0,
  left: 0
});
let gradientSlide = css.keyframes({
  "0%": { backgroundPosition: "0% 50%" },
  "50%": { backgroundPosition: "100% 50%" },
  "100%": { backgroundPosition: "0% 50%" }
});

let mainWrapper = css({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  height: "100vh",
  width: "100vw",
  maxHeight: "100vh",
  background: BG_COLOR,
  // background: 'linear-gradient(222deg, #076a8e, #364563)',
  // backgroundSize: '400% 400%',
  // animation: `${gradientSlide} 25s ease infinite`,
  overflow: "hidden",
  "::before": {
    content: '""',
    background: `url(${LOGO}) no-repeat`,
    backgroundSize: "130% 130%",
    backgroundPosition: "-55% -95%",
    width: 2000,
    height: 1000,
    position: "absolute",
    bottom: 0,
    right: 0,
    opacity: 0.02
  },
  transition: "2s ease",
  opacity: 0
});

let rule = css({
  "@media only screen and (max-width: 680px)": {
    left: "32% !important",
    top: "-20px"
  },
  "@media only screen and (max-width: 415px)": {
    left: "34% !important",
    top: "-20px"
  },
  "@media only screen and (max-width: 360px)": {
    left: "31.5% !important",
    top: "-21px"
  },
  "@media only screen and (max-width: 376px)": {
    left: "32% !important",
    top: "-21px"
  },
  "@media only screen and (max-width: 320px)": {
    left: "29.5% !important",
    top: "-28px"
  }
});

let maintenanceWrapper = css({
  flex: "1 0 100%",
  position: "relative",
  width: "100%",
  maxWidth: 700,
  height: 350,
  display: "flex",
  flexDirection: "column"
});

let logoContainer = css({
  width: "100%",
  margin: 0,
  display: "flex",
  alignItems: "center",
  justifyContent: "center"
});

let cloud = css({
  flex: "1 0 100%"
});
let title = css({
  position: "absolute",
  top: "20%",
  left: "46.5%",
  width: 140
});

let rotate = css.keyframes({
  "0%": {
    transform: "rotate(0deg)"
  },
  "100%": {
    transform: "rotate(360deg)"
  }
});

let gearContainer = css({
  width: 75,
  position: "absolute",
  top: "-11px",
  left: "40.4%",
  margin: 0,
  transform: "scale(.9)"
});

let gear = css({
  width: "100%",
  transformOrigin: "center center"
});

let animate = css({
  animation: `${rotate} 10s linear infinite`
});

let messageWrapper = css({
  position: "relative",
  top: "-10%",
  display: "flex",
  flexDirection: "column",
  width: "100%",
  textAlign: "center",
  padding: "20px 30px",
  color: "#fff",
  borderRadius: 5,
  "> h1": {
    fontWeight: 600,
    marginBottom: 10
  },
  "> h3": {
    opacity: 0.7,
    margin: 0
  },
  "> a": {
    color: "#81CDE8",
    textDecoration: "none",
    marginTop: 50,
    transition: "300ms ease",
    fontSize: ".8rem",
    ":hover": {
      color: "#d484b7"
    }
  }
});
