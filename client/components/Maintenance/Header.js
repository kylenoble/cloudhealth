import Head from 'next/head'

import React, { Component } from 'react'

export default class Header extends Component {
    render() {
        const favicon = this.props.isDone ? 'checkmark' : 'gear'

        return (
            <div>
                <Head>
                    <title>{this.props.title} | CloudHealth</title>
                    <meta charset="UTF-8" />
                    <meta name="viewport" content="target-densitydpi=device-dpi" />
                    <meta name="viewport" content="width=device-width,height=device-height,initial-scale=1.0" />
                    <meta name="description" content="With CloudHealth’s digital platform, Achieving optimal health is just one click away" />
                    <meta name="keywords" content="Health,Video,Doctor" />
                    <meta name="msapplication-TileImage" content="/static/images/mstile-144x144.png" />
                    <meta name="msapplication-square70x70logo" content="/static/images/mstile-70x70.png" />
                    <meta name="msapplication-square150x150logo" content="/static/images/mstile-150x150.png" />
                    <meta name="msapplication-wide310x150logo" content="/static/images/mstile-310x150.png" />
                    <meta name="msapplication-square310x310logo" content="/static/images/mstile-310x310.png" />
                    <meta name="application-name" content="CloudHealth" />
                    <meta name="msapplication-TileColor" content="#FFFFFF" />

                    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="/static/images/apple-touch-icon-57x57.png" />
                    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/static/images/apple-touch-icon-114x114.png" />
                    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/static/images/apple-touch-icon-72x72.png" />
                    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/static/images/apple-touch-icon-144x144.png" />
                    <link rel="apple-touch-icon-precomposed" sizes="60x60" href="/static/images/apple-touch-icon-60x60.png" />
                    <link rel="apple-touch-icon-precomposed" sizes="120x120" href="/static/images/apple-touch-icon-120x120.png" />
                    <link rel="apple-touch-icon-precomposed" sizes="76x76" href="/static/images/apple-touch-icon-76x76.png" />
                    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="/static/images/apple-touch-icon-152x152.png" />
                    <link rel="icon" type="image/png" href="/static/images/favicon-196x196.png" sizes="196x196" />
                    <link rel="icon" type="image/png" href="/static/images/favicon-96x96.png" sizes="96x96" />
                    <link rel="icon" type="image/png" href="/static/images/favicon-32x32.png" sizes="32x32" />
                    <link rel="icon" type="image/png" href="/static/images/favicon-16x16.png" sizes="16x16" />
                    <link rel="shortcut icon" href="/static/images/favicon.ico" type="image/x-icon" />
                    <link rel="icon" type="image/png" href="/static/images/favicon-128.png" sizes="128x128" />

                    {/* <link rel="icon" href={`/static/images/${favicon}.svg`} type="image/x-icon" /> */}

                    <link defer href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet"></link>
                    <script src="/static/flexibility.js"></script>
                    <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.20.0/polyfill.min.js' />
                    <link rel='stylesheet' type='text/css' href='/static/nprogress.css' />
                    <link rel="stylesheet" type="text/css" href="/static/react-datetime.css"></link>
                    <link rel="stylesheet" type="text/css" href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"></link>
                    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
                    <link rel="stylesheet" href="/static/css/maintenance.css" />

                </Head>
                {this.props.children}
            </div>
        )
    }
}
