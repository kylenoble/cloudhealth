import React from "react";
import Link from "next/link";

import Form from "../Form";

import AnswerService from "../../utils/answerService.js";
import SummaryService from "../../utils/summaryService.js";
import GenerateSummary from "../../utils/generateSummary.js";
import { css } from "glamor";
import Colors from "../NewSkin/Colors";

const answer = new AnswerService();
const summary = new SummaryService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      specialDiet: {
        id: 18,
        name: "specialDiet",
        question:
          "Do you follow any special diet or have diet restrictions or limitations for any reason (health, cultural, religious or other)?",
        type: "select",
        validation: "select",
        options: [{ label: "Yes", value: "yes" }, { label: "No", value: "no" }],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      foodAllergies: {
        id: 19,
        name: "foodAllergies",
        question:
          "Do you have any food allergies, sensitivities or intolerances?",
        type: "select",
        validation: "select",
        options: [{ label: "Yes", value: "yes" }, { label: "No", value: "no" }],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      meatConsumedPercent: {
        id: 24,
        name: "meatConsumedPercent",
        question: "Red Meat (beef, pork, lamb)",
        type: "select",
        validation: "select",
        options: [
          { label: "<10%", value: 0 },
          { label: "10%", value: 10 },
          { label: "20%", value: 20 },
          { label: "30%", value: 30 },
          { label: "40%", value: 40 },
          { label: "50%", value: 50 },
          { label: "60%", value: 60 },
          { label: "70%", value: 70 },
          { label: "80%", value: 80 },
          { label: "90%", value: 90 },
          { label: "100%", value: 100 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default",
        int_value: 0
      },
      vegetableConsumedPercent: {
        id: 25,
        name: "vegetableConsumedPercent",
        question: "Vegetables",
        type: "select",
        validation: "select",
        options: [
          { label: "<10%", value: 0 },
          { label: "10%", value: 10 },
          { label: "20%", value: 20 },
          { label: "30%", value: 30 },
          { label: "40%", value: 40 },
          { label: "50%", value: 50 },
          { label: "60%", value: 60 },
          { label: "70%", value: 70 },
          { label: "80%", value: 80 },
          { label: "90%", value: 90 },
          { label: "100%", value: 100 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default",
        int_value: 0
      },
      fruitConsumedPercent: {
        id: 26,
        name: "fruitConsumedPercent",
        question: "Fruits",
        type: "select",
        validation: "select",
        options: [
          { label: "<10%", value: 0 },
          { label: "10%", value: 10 },
          { label: "20%", value: 20 },
          { label: "30%", value: 30 },
          { label: "40%", value: 40 },
          { label: "50%", value: 50 },
          { label: "60%", value: 60 },
          { label: "70%", value: 70 },
          { label: "80%", value: 80 },
          { label: "90%", value: 90 },
          { label: "100%", value: 100 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default",
        int_value: 0
      },
      starchConsumedPercent: {
        id: 27,
        name: "starchConsumedPercent",
        question: "Starches/Grains/Sugar",
        type: "select",
        validation: "select",
        options: [
          { label: "<10%", value: 0 },
          { label: "10%", value: 10 },
          { label: "20%", value: 20 },
          { label: "30%", value: 30 },
          { label: "40%", value: 40 },
          { label: "50%", value: 50 },
          { label: "60%", value: 60 },
          { label: "70%", value: 70 },
          { label: "80%", value: 80 },
          { label: "90%", value: 90 },
          { label: "100%", value: 100 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default",
        int_value: 0
      },
      poultryConsumedPercent: {
        id: 28,
        name: "poultryConsumedPercent",
        question: "Poultry",
        type: "select",
        validation: "select",
        options: [
          { label: "<10%", value: 0 },
          { label: "10%", value: 10 },
          { label: "20%", value: 20 },
          { label: "30%", value: 30 },
          { label: "40%", value: 40 },
          { label: "50%", value: 50 },
          { label: "60%", value: 60 },
          { label: "70%", value: 70 },
          { label: "80%", value: 80 },
          { label: "90%", value: 90 },
          { label: "100%", value: 100 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default",
        int_value: 0
      },
      seafoodConsumedPercent: {
        id: 29,
        name: "seafoodConsumedPercent",
        question: "Seafood",
        type: "select",
        validation: "select",
        options: [
          { label: "<10%", value: 0 },
          { label: "10%", value: 10 },
          { label: "20%", value: 20 },
          { label: "30%", value: 30 },
          { label: "40%", value: 40 },
          { label: "50%", value: 50 },
          { label: "60%", value: 60 },
          { label: "70%", value: 70 },
          { label: "80%", value: 80 },
          { label: "90%", value: 90 },
          { label: "100%", value: 100 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default",
        int_value: 0
      },
      usualMeal: {
        id: 134,
        name: "usualMeal",
        question: "Select any from below that are part of your usual meal.",
        type: "select",
        validation: "select",
        options: [
          { label: "Fried Food", value: "Fried Food" },
          { label: "Grilled Food", value: "Grilled Food" },
          { label: "Canned goods", value: "Canned goods" },
          { label: "Soda", value: "Soda" },
          { label: "Canned Juices", value: "Canned Juices" },
          { label: "Tetra packs", value: "Tetra packs" },
          { label: "3-in-1 coffee", value: "3-in-1 coffee" },
          { label: "Energy Drinks", value: "Energy Drinks" },
          { label: "Instant noodles", value: "Instant noodles" },
          { label: "Microwaveable foods", value: "Microwaveable foods" },
          { label: "None", value: "None" }
        ],
        error: {
          active: false,
          message: "Please select at least one answer"
        },
        value: "default",
        int_value: 0
      },
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      },
      food_pct_sum: {
        value: 0,
        style: null
      }
    };

    this._handleChange = this._handleChange.bind(this);
    this._goToNext = this._goToNext.bind(this);
  }

  supportsFlexBox() {
    var test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  componentDidMount() {
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      flexibility(document.body);
    }

    this._setDefaultValues();
  }

  _setDefaultValues() {
    const {
      specialDiet,
      foodAllergies,
      meatConsumedPercent,
      vegetableConsumedPercent,
      fruitConsumedPercent,
      starchConsumedPercent,
      poultryConsumedPercent,
      seafoodConsumedPercent,
      usualMeal
    } = this.state;

    specialDiet.value = this.props.survey.specialDiet;
    foodAllergies.value = this.props.survey.foodAllergies;
    meatConsumedPercent.value = this.props.survey.meatConsumedPercent;
    vegetableConsumedPercent.value = this.props.survey.vegetableConsumedPercent;
    fruitConsumedPercent.value = this.props.survey.fruitConsumedPercent;
    starchConsumedPercent.value = this.props.survey.starchConsumedPercent;
    poultryConsumedPercent.value = this.props.survey.poultryConsumedPercent;
    seafoodConsumedPercent.value = this.props.survey.seafoodConsumedPercent;

    meatConsumedPercent.int_value = this._getValue(
      this.props.survey.meatConsumedPercent,
      "meatConsumedPercent"
    );

    vegetableConsumedPercent.int_value = this._getValue(
      this.props.survey.vegetableConsumedPercent,
      "vegetableConsumedPercent"
    );

    fruitConsumedPercent.int_value = this._getValue(
      this.props.survey.fruitConsumedPercent,
      "fruitConsumedPercent"
    );

    starchConsumedPercent.int_value = this._getValue(
      this.props.survey.starchConsumedPercent,
      "starchConsumedPercent"
    );

    poultryConsumedPercent.int_value = this._getValue(
      this.props.survey.poultryConsumedPercent,
      "poultryConsumedPercent"
    );

    seafoodConsumedPercent.int_value = this._getValue(
      this.props.survey.seafoodConsumedPercent,
      "seafoodConsumedPercent"
    );

    usualMeal.value = this.props.survey.usualMeal;

    this.setState(
      {
        specialDiet,
        foodAllergies,
        meatConsumedPercent,
        vegetableConsumedPercent,
        fruitConsumedPercent,
        starchConsumedPercent,
        poultryConsumedPercent,
        seafoodConsumedPercent,
        usualMeal
      },
      function() {
        this._computeSum();
      }
    );
  }

  render() {
    return (
      <div>
        <div style={styles.container}>
          <div style={styles.introInfo}>
            <h1 style={{ marginTop: 0 }}>Nutrition</h1>
            <span>
              Data on your food choices and allergies (if any) will help us
              assess your food choices and feeding patterns <br />
              and will enable us to draft the best food and nutrition plan for
              you.
            </span>
          </div>
          <Form
            from="health-survey"
            nutrition
            // header={
            //   this.props.location == "profile"
            //     ? "Update Nutrition Info Below"
            //     : "Continue Your Profile Below "
            // }
            submitForm={this._goToNext}
            buttonText={
              this.props.location == "profile"
                ? "Update My Profile"
                : "Continue My Profile"
            }
            handleChange={this._handleChange}
            inputs={[
              this.state.specialDiet,
              this.state.foodAllergies,
              this.state.usualMeal
            ]}
            table={this._renderNutritionTable()}
            secondaryOption=""
            formError={this.state.formError}
            formSuccess={this.state.formSuccess}
            back={this.props.back}
          />
        </div>
      </div>
    );
  }

  _renderNutritionTable() {
    var inputs = [
      this.state.meatConsumedPercent,
      this.state.vegetableConsumedPercent,
      this.state.fruitConsumedPercent,
      this.state.starchConsumedPercent,
      this.state.poultryConsumedPercent,
      this.state.seafoodConsumedPercent
    ];
    var less_than_10 = "<10%";

    return (
      <div
        className={tableWrapper}
        style={{
          marginTop: "20px",
          width: "100%",
          maxWidth: 600,
          margin: "20px auto"
        }}
      >
        <table style={styles.myTable}>
          <thead>
            <tr>
              <th colSpan="2" style={{ paddingBottom: 30 }}>
                What percentage of the foods you eat are the following?{" "}
              </th>
            </tr>
          </thead>
          <tbody>
            {inputs.map((input, i) => {
              return [
                <tr key={i}>
                  <td style={styles.myTableTD}>{input.question}</td>
                  <td style={{ paddingBottom: 15 }}>
                    <select
                      id={input.name}
                      value={input.int_value}
                      className={inputClass}
                      onChange={e => this._handleInputChange(e, input)}
                    >
                      <option value="0">{less_than_10}</option>
                      <option value="10">10%</option>
                      <option value="20">20%</option>
                      <option value="30">30%</option>
                      <option value="40">40%</option>
                      <option value="50">50%</option>
                      <option value="60">60%</option>
                      <option value="70">70%</option>
                      <option value="80">80%</option>
                      <option value="90">90%</option>
                      <option value="100">100%</option>
                    </select>
                  </td>
                </tr>
              ];
            })}
            <tr>
              <td>
                <div className={border} />
              </td>
              <td>
                <div className={border} />
              </td>
            </tr>
            <tr>
              <td style={styles.myTableTD}>
                <strong
                  style={{
                    color:
                      this.state.food_pct_sum.value === 100
                        ? Colors.skyblue
                        : Colors.pink
                  }}
                >
                  TOTAL (%)
                </strong>
              </td>
              <td style={this.state.food_pct_sum.style}>
                <strong
                  style={{
                    color:
                      this.state.food_pct_sum.value === 100
                        ? Colors.skyblue
                        : Colors.pink
                  }}
                >
                  {this.state.food_pct_sum.value}%
                </strong>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }

  _getValue(value, inputName) {
    var options = this.state[inputName].options;
    for (const option of options) {
      if (option.label == value) {
        return option.value;
      }
    }
  }

  _getPctValue(value, input) {
    var options = input.options;
    for (const option of options) {
      if (option.label == input.value) {
        return option.label;
      }
    }
  }

  _goToNext() {
    console.log("NEXT");
    this.setState({
      formError: {
        active: false,
        message: ""
      }
    });

    var sum = this.state.food_pct_sum.value;

    if (sum != 100) {
      this.setState({
        formError: {
          active: true,
          message: "Your percent of foods consumed must equal to 100%"
        }
      });
      return;
    }

    var inputs = [
      this.state.specialDiet,
      this.state.foodAllergies,
      this.state.meatConsumedPercent,
      this.state.vegetableConsumedPercent,
      this.state.fruitConsumedPercent,
      this.state.starchConsumedPercent,
      this.state.poultryConsumedPercent,
      this.state.seafoodConsumedPercent,
      this.state.usualMeal
    ];

    const summaryGenerator = new GenerateSummary(inputs, "nutrition");
    let nutritionSummary = [
      {
        name: "nutrition",
        group: "nutrition",
        label: "Nutrition",
        value: summaryGenerator.generateSummary()
      }
    ];

    answer
      .create(inputs)
      .then(res => {
        if (res) {
          summary
            .create(nutritionSummary)
            .then(result => {
              console.log("RESULT", result, inputs);
              console.log("update user profile");
              this.setState({
                formSuccess: {
                  active: true,
                  message: "Your Profile Was Updated"
                }
              });
              this.props.back();
              //  return this.props.onClick('Dashboard')
            })
            .catch(error => {
              console.log(error);
              this.setState({
                formError: {
                  active: true,
                  message: "There was an error updating your profile"
                }
              });
            });
        } else {
          this.setState({
            formError: {
              active: true,
              message: "There was an error updating your profile"
            }
          });
        }
      })
      .catch(e => {
        this.setState({
          formError: {
            active: true,
            message: e.error
          }
        });
      }); // you would show/hide error messages with component state here
  }

  _handleChange(value, input, validated) {
    const inputName = input;
    var newInput = this.state[inputName];

    if (input === "usualMeal") {
      if (newInput.value != "" || newInput.value != "default") {
        if (value === "None") {
          newInput.value = value;
        } else {
          let usualMeal = newInput.value.split(",");

          if (usualMeal.indexOf(value) < 0) {
            usualMeal.push(value);
          } else {
            const location = usualMeal.indexOf(value);
            usualMeal.splice(location, 1);
          }

          var index = usualMeal.indexOf("None");
          if (index > -1) {
            usualMeal.splice(index, 1);
          }

          newInput.value = usualMeal.join(",");
        }
      } else {
        newInput.value = value;
      }
    } else {
      newInput.value = value;
    }

    newInput.error.active = validated;

    this.setState({
      inputName: newInput
    });
  }

  _validateInput(input) {
    if (input !== "" && parseInt(input) >= 0 && parseInt(input) <= 100) {
      return true;
    }
    return false;
  }

  _handleInputChange(e, input) {
    var newInput = this.state[input.name];
    var selected = e.target;
    let name = selected.id;
    let value = selected.value;
    let pctValue = selected.options[selected.selectedIndex].text;

    var validated = this._validateInput(value);
    // //if validated is true, error.active should be false and vice versa
    this._updateChange(value, name, pctValue, !validated);
  }

  _updateChange(value, input, pctValue, validated) {
    const inputName = input;
    var newInput = this.state[inputName];

    newInput.value = pctValue;
    newInput.int_value = value;

    newInput.error.active = validated;

    this.setState(
      {
        inputName: newInput
      },
      function() {
        this._computeSum();
      }
    );
  }

  _computeSum() {
    var foods = [
      this.state.meatConsumedPercent,
      this.state.vegetableConsumedPercent,
      this.state.fruitConsumedPercent,
      this.state.starchConsumedPercent,
      this.state.poultryConsumedPercent,
      this.state.seafoodConsumedPercent
    ];

    var food_pct_sum = 0;
    for (const food of foods) {
      food_pct_sum += parseInt(food.int_value) || 0;
    }
    var style = null;
    if (food_pct_sum != 100) {
      style = { color: "red" };
    }
    this.setState({
      food_pct_sum: { value: food_pct_sum, style: style }
    });
  }
}
const border = css({
  width: "100%",
  height: 1,
  background: "#eee",
  margin: "10px 0 20px"
});

const inputClass = css({
  borderStyle: "none",
  padding: "5px 10px",
  border: "1px solid #eee",
  boxShadow: "0 2px 2px rgba(0,0,0,.050)",
  transition: "250ms ease",
  borderRadius: 5,
  cursor: "text",
  ":focus": {
    outline: "none",
    borderColor: Colors.skyblue,
    boxShadow: `0 5px 10px ${Colors.skyblue}30`
  }
});

const formInput = css({
  padding: "5px 10px",
  ":focus": {
    outline: "none"
  }
});

const tableWrapper = css({
  "> td": {
    border: "none"
  }
});

const styles = {
  container: {
    fontFamily: '"Roboto",sans-serif',
    display: "flex",
    flexWrap: "wrap",
    //  flexDirection: 'column',
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden"
  },
  introInfo: {
    maxWidth: "100%",
    fontFamily: '"Roboto",sans-serif',
    color: "#364563",
    fontSize: "14px",
    padding: "10px 50px",
    textAlign: "justified"
  },
  formContainer: {
    border: "2px solid rgba(151,151,151,0.2)",
    borderRadius: "6px",
    maxWidth: "400px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.20)"
  },
  header: {
    borderBottom: "2px solid rgba(225,232,238,1)",
    padding: "15px 20px",
    fontSize: "18px",
    color: "#364563",
    fontFamily: '"Roboto-Medium",sans-serif',
    width: "360px"
  },
  form: {
    margin: "10px 30px 20px 20px"
  },
  formInput: {
    marginBottom: 20,
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    fontSize: "14px",
    textAlign: "center",
    ":focus": {
      outline: "none"
    }
  },
  options: {
    marginTop: "15px",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif',
    padding: "13px 30px 13px 30px",
    WebkitAppearance: "none",
    MozAppearance: "none",
    appearance: "none",
    width: "350px",
    backgroundColor: "#fff",
    borderColor: "#d9d9d9 #ccc #b3b3b3",
    borderradius: "4px",
    border: "2px solid #E1E8EE",
    cursor: "default",
    borderSpacing: "0",
    borderCollapse: "separate",
    specialDiet: "50px",
    outline: "none",
    overflow: "hidden",
    position: "relative",
    cursor: "pointer"
  },
  optionsDefault: {
    color: "rgba(169, 169, 169, 1)"
  },
  selectArrowZone: {
    cursor: "pointer",
    display: "table-cell",
    position: "relative",
    textAlign: "center",
    verticalAlign: "middle",
    width: "25px",
    paddingRight: "5px"
  },
  selectArrow: {
    borderColor: "#999 transparent transparent",
    borderStyle: "solid",
    borderWidth: "5px 5px 2.5px",
    display: "inline-block",
    specialDiet: "0",
    width: "0",
    position: "absolute",
    right: "-18em",
    top: "-1.75em"
  },
  button: {
    backgroundColor: "#ED81B9",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "0px 0px 20px 0px",
    cursor: "pointer"
  },
  register: {
    textDecoration: "none",
    margin: "20px 10px",
    color: "#43484D",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif'
  },
  myTableTD: {
    textAlign: "left",
    paddingnBottom: 15
  },
  myTable: {
    tableLayout: "auto",
    marginLeft: "auto",
    marginRight: "auto"
  }
};
