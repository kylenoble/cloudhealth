import React from 'react'
import { css } from 'glamor'

const NotificationBanner = props => {
  return (
    <div className={`${allReadWrapper}`}>
      <h3> <strong className={checkmark}></strong> <span>You've read all your {props.type}</span></h3>
    </div>
  )
}

export default NotificationBanner

const GREEN = "#47d764"

let bounce = css.keyframes({
  '0%': { transform: 'scale(1)' },
  '25%': { transform: 'scale(.8)' },
  '50%': { transform: 'scale(1.1)' },
  '100%': { transform: 'scale(1)' }
})

let checkmark = css({
  width: 30,
  height: 30,
  borderRadius: '100%',
  background: GREEN,
  color: 'white',
  display: 'inline-block',
  position: 'relative',
  marginRight: 20,
  '::before': {
    content: '""',
    position: 'absolute',
    top: '50%',
    left: '52%',
    width: 3,
    height: 14,
    background: 'white',
    transform: 'translateY(-50%) rotate(45deg)'
  },
  '::after': {
    content: '""',
    position: 'absolute',
    top: '60%',
    left: '32%',
    width: 3,
    height: 7,
    background: 'white',
    transform: 'translateY(-50%) rotate(-45deg)'
  }
})

let allReadWrapper = css({
  padding: 20,
  // width: '100%',
  // maxWidth: 700,
  textTransform: 'uppercase',
  background: 'white',
  color: GREEN,
  marginBottom: 20,
  borderLeft: `5px solid ${GREEN}`,
  boxShadow: '0 20px 42px -10px rgba(0,0,0,.1),0 1px 15px rgba(0,0,0,.050)',
  overflow: 'hidden',
  animation: `${bounce} .5s .5s`,
  borderRadius: 4,
  '> h3': {
    fontSize: '1.2rem',
    margin: 0,
    display: 'flex',
    alignItems: 'center',
    fontWeight: 600,
    '> span': {
      margin: '0 auto',
      display: 'block'
    }
  },
})