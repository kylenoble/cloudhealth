import React from "react";
import { css } from "glamor";
import DashboardSwitcher from "../../components/DashboardSwitcher";
import NotificationService from "../../utils/notificationService.js";
import AnswerService from "../../utils/answerService.js";
import EventsService from "../../utils/eventsService";
import Announcements from "../../utils/announcementsService";
import AppStatus from "../../utils/checkAppStatus.js";

const answer = new AnswerService();
const NOTIFICATION = [];
const notification = new NotificationService();
const events = new EventsService();
const announcements = new Announcements();
const appStatus = new AppStatus();

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.user = this.props.user;
    this.state = {
      menuOpen: false,
      notReadEventCtr: 0,
      notification: [],
      announcementsNotification: [],
      notReadAnnouncementCtr: 0
    };

    this._goTo = this._goTo.bind(this);
    this._closeMenu = this._closeMenu.bind(this);
  }

  componentWillMount() {
    if (this.user.id) {
      appStatus.chek_status();
      // const intervalId = setInterval(() => {
      //   appStatus.chek_status();
      // }, 10000);

      // this.setState({ intervalId: intervalId });
    }
  }

  componentDidMount() {
    if (this.user.id) {
      this._getPersonalInfo();
      this._getEvents();
      this._getAdminNotifications();
      this._getAnnoucements();
    }
  }

  componentWillUnmount() {
    //clearInterval(this.state.intervalId);
  }

  _getAdminNotifications() {
    const id = this.user.id;
    const company_id = this.user.company_id;
    const type = this.user.type;
    const ids = `${id}~${company_id}~${type}`;
    return notification
      .getAdmin(ids)
      .then(res => {
        this.setState({
          notificationsFromAdmin: res
        });
      })
      .catch(e => {
        return Promise.reject(e);
      });
  }

  _getAnnoucements() {
    let type =
      this.user.type === "Patient"
        ? "Patients"
        : this.user.type === "Doctor"
        ? "Doctors"
        : null;
    type += `_${this.user.company_id}`;
    announcements
      .get(type)
      .then(res => {
        this.setState(
          {
            announcementsNotification: res
          },
          function() {
            this._getAnnouncementForThis();
          }
        );
      })
      .catch(e => {
        console.log(e);
      });
  }

  _getAnnouncementForThis() {
    const notRead = 0;

    this.setState({
      notReadAnnouncementCtr: notRead
    });
  }

  _getEvents() {
    let type =
      this.user.type === "Patient"
        ? "Patients"
        : this.user.type === "Doctor"
        ? "Doctors"
        : null;
    type += `_${this.user.company_id}`;
    events
      .get(type)
      .then(res => {
        if (res.length > 0) {
          this.setState(
            {
              eventNotification: res
            },
            function() {
              this._getEvenForThis();
            }
          );
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _getEvenForThis() {
    const notRead = 0;

    this.setState({
      notReadEventCtr: notRead
    });
  }

  _getPersonalInfo() {
    answer
      .getAnswerList({ column: "question_id", operator: "lt", value: 11 })
      .then(res => {
        if (res.name === "Error") {
          console.log(res);
        } else if (res.length > 0) {
          if (res.length < 6) {
            this._notification(
              "Personal",
              "Please complete your personal profile",
              "Account"
            );
          }
        }
      })
      .catch(e => {
        console.log(e);
      }); // you would show/hide error messages with component state here
  }

  _notification(nme, note, lnk) {
    const currentNotification = this.state.notification;
    let p = true;
    if (currentNotification.length > 0) {
      for (let i = 0; i < currentNotification.length; i++) {
        if (currentNotification[i].name == nme) p = false;
      }
      if (p) NOTIFICATION.push({ name: nme, description: note, link: lnk });
    } else {
      NOTIFICATION.push({ name: nme, description: note, link: lnk });
    }

    this.setState({
      notification: NOTIFICATION
    });
  }

  _closeMenu() {
    this.setState({ menuOpen: false });
  }

  _toggleMenu() {
    let menuOpen = this.state.menuOpen;
    if (menuOpen) {
      menuOpen = false;
    } else {
      menuOpen = true;
    }
    this.setState({ menuOpen });
  }

  _goTo(lnk) {
    this._toggleMenu();
    this.props.updateView(lnk);
  }

  onMouseLeave() {
    this.setState({
      menuOpen: false
    });
  }

  render() {
    let filteredNotificationsFromAdmin = [];
    if (this.state.notificationsFromAdmin) {
      filteredNotificationsFromAdmin = this.state.notificationsFromAdmin.filter(
        n => n.read_by == null
      );
    }

    return (
      <div className={menuDiv} style={{ zIndex: 5 }}>
        <span onClick={() => this._toggleMenu()} className={bt}>
          <img
            alt="Header Icon"
            className={headerIcon}
            src={
              this.state.notReadAnnouncementCtr > 0 ||
              this.state.notReadEventCtr > 0 ||
              this.state.notification.length > 0 ||
              filteredNotificationsFromAdmin.length > 0
                ? "/static/images/3lineWithNotification.svg"
                : this.state.notReadEventCtr > 0 && this.user.type === "Doctor"
                ? "/static/images/3lineWithNotification.svg"
                : "/static/images/3line.svg"
            }
          />
        </span>
        <div
          style={{ zIndex: 2, transform: "scale(1.3)" }}
          className={this.state.menuOpen ? menuOpenStyle : menuWrapper}
          onMouseLeave={() => this.onMouseLeave()}
        >
          <DashboardSwitcher
            user={this.user}
            titlePressed={this._goTo}
            getNotification={this.state.notification}
            notificationsFromAdmin={this.state.notificationsFromAdmin}
            notReadEventCtr={this.state.notReadEventCtr}
            notReadAnnouncementCtr={this.state.notReadAnnouncementCtr}
            openModal={this._openModal}
            useNewMenu={this.props.useNewMenu}
            closeMenu={this._closeMenu}
          />
        </div>
      </div>
    );
  }
}

let menuDiv = css({
  position: "absolute",
  top: "75px",
  "@media print": {
    display: "none"
  }
});

let headerIcon = css({
  color: "rgb(54, 69, 99)",
  height: "20px",
  width: "20px",
  marginRight: "10px"
});

let bt = css({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  padding: "5px 8px 8px 5px",
  borderRadius: "0 0 30px 0",
  position: "relative",
  zIndex: 3,
  background: "#d383b6",
  boxShadow: "-3px 10px 10px #424242",
  cursor: "pointer"
});

const menuOpenStyle = css({
  position: "absolute",
  left: "50px",
  width: "250px",
  backgroundColor: "#80cde9",
  boxShadow: "10px 10px 6px -8px rgba(161, 136, 119, 0.7)",
  padding: "20px 3px",
  borderRight: "1px solid rgba(0, 0, 0, 0.2)",
  borderBottom: "1px solid rgba(0, 0, 0, 0.2)",
  borderRadius: "0 0 20px 0",
  marginLeft: "0px",
  opacity: 1,
  transform: "rotate(0deg)",
  transition: "all 0.2s ease-in"
});

const menuWrapper = css({
  position: "absolute",
  //transform: "scale(1.5)",
  left: "50px",
  width: "250px",
  backgroundColor: "#80cde9",
  marginLeft: "-450px",
  opacity: 0,
  boxShadow: "10px 10px 6px -8px rgba(161, 136, 119, 0.7)",
  transition: "all 0.3s ease-in-out",
  padding: "20px 3px",
  borderRight: "1px solid rgba(0, 0, 0, 0.2)",
  borderBottom: "1px solid rgba(0, 0, 0, 0.2)",
  borderRadius: "0 0 20px 0"
});
