import React from "react";
import Link from "next/link";

import Form from "../Form";

import AnswerService from "../../utils/answerService.js";
import SummaryService from "../../utils/summaryService.js";
import GenerateSummary from "../../utils/generateSummary.js";

const answer = new AnswerService();
const summary = new SummaryService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      regularSleep: {
        id: 30,
        name: "regularSleep",
        question: "When is your regular sleeping time?",
        type: "select",
        validation: "select",
        options: [
          { label: "Day", value: "day" },
          { label: "Night", value: "night" }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      averageSleep: {
        id: 31,
        name: "averageSleep",
        question: "Average number of hours you sleep",
        type: "select",
        validation: "select",
        options: [
          { label: ">10", value: ">10" },
          { label: "8-10", value: "8-10" },
          { label: "6-8", value: "6-8" },
          { label: "<6", value: "<6" }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      troubleFallingAsleep: {
        id: 32,
        name: "troubleFallingAsleep",
        question: "Do you have trouble falling asleep?",
        type: "select",
        validation: "select",
        options: [{ label: "Yes", value: "yes" }, { label: "No", value: "no" }],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      restedFeeling: {
        id: 33,
        name: "restedFeeling",
        question: "Do you feel rested upon awakening?",
        type: "select",
        validation: "select",
        options: [{ label: "Yes", value: "yes" }, { label: "No", value: "no" }],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      sleepingAids: {
        id: 34,
        name: "sleepingAids",
        question: "Do you use sleeping aids?",
        type: "select",
        validation: "select",
        options: [{ label: "Yes", value: "yes" }, { label: "No", value: "no" }],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      }
    };

    this._handleChange = this._handleChange.bind(this);
    this._goToNext = this._goToNext.bind(this);
  }

  supportsFlexBox() {
    var test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  componentDidMount() {
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      flexibility(document.body);
    }

    this._setDefaultValues();
  }

  _setDefaultValues() {
    const {
      regularSleep,
      averageSleep,
      troubleFallingAsleep,
      restedFeeling,
      sleepingAids
    } = this.state;

    regularSleep.value = this.props.survey.regularSleep;
    averageSleep.value = this.props.survey.averageSleep;
    troubleFallingAsleep.value = this.props.survey.troubleFallingAsleep;
    restedFeeling.value = this.props.survey.restedFeeling;
    sleepingAids.value = this.props.survey.sleepingAids;

    this.setState({
      regularSleep,
      averageSleep,
      troubleFallingAsleep,
      restedFeeling,
      sleepingAids
    });
  }

  render() {
    return (
      <div style={styles.container}>
        <div style={styles.introInfo}>
          <h1 style={{ marginTop: 0 }}>Sleep</h1>
          Data on your sleep patterns will help us assess your risk for health
          imbalances
          <br />
          and will enable us to draft the best health to optimize your sleep.
        </div>
        <Form
          from="health-survey"
          sleep
          // header={this.props.location == 'profile' ? "Update Sleep Info Below" : "Continue Your Profile Below"}
          submitForm={this._goToNext}
          buttonText={
            this.props.location == "profile"
              ? "Update My Profile"
              : "Continue My Profile"
          }
          handleChange={this._handleChange}
          inputs={[
            this.state.regularSleep,
            this.state.averageSleep,
            this.state.troubleFallingAsleep,
            this.state.restedFeeling,
            this.state.sleepingAids
          ]}
          secondaryOption=""
          formError={this.state.formError}
          formSuccess={this.state.formSuccess}
          back={this.props.back}
        />
      </div>
    );
  }

  _goToNext() {
    this.setState({
      formError: {
        active: false,
        message: ""
      }
    });

    var inputs = [
      this.state.regularSleep,
      this.state.averageSleep,
      this.state.troubleFallingAsleep,
      this.state.restedFeeling,
      this.state.sleepingAids
    ];

    const summaryGenerator = new GenerateSummary(inputs, "sleep");
    let sleepSummary = [
      {
        name: "sleep",
        group: "sleep",
        label: "Sleep",
        value: summaryGenerator.generateSummary()
      }
    ];

    answer
      .create(inputs)
      .then(res => {
        if (res) {
          summary
            .create(sleepSummary)
            .then(result => {
              console.log("update user profile");
              this.setState({
                formSuccess: {
                  active: true,
                  message: "Your Profile Was Updated"
                }
              });
              this.props.back();
              //  return this.props.onClick('Dashboard')
            })
            .catch(error => {
              console.log(error);
              this.setState({
                formError: {
                  active: true,
                  message: "There was an error updating your profile"
                }
              });
            });
        } else {
          this.setState({
            formError: {
              active: true,
              message: "There was an error updating your profile"
            }
          });
        }
      })
      .catch(e => {
        this.setState({
          formError: {
            active: true,
            message: e.error
          }
        });
      }); // you would show/hide error messages with component state here
  }

  _handleChange(value, input, validated) {
    const inputName = input;
    var newInput = this.state[inputName];
    newInput.value = value;
    newInput.error.active = validated;

    this.setState({
      inputName: newInput
    });
  }
}

const styles = {
  container: {
    width: "100%",
    fontFamily: '"Roboto",sans-serif',
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden"
  },
  introInfo: {
    flex: "1 0 99%",
    textAlign: "center",
    fontFamily: '"Roboto",sans-serif',
    color: "#364563",
    fontSize: "14px",
    padding: "10px 20px"
    //  textAlign: 'justified',
  },
  formContainer: {
    border: "2px solid rgba(151,151,151,0.2)",
    borderRadius: "6px",
    maxWidth: "400px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.20)"
  },
  header: {
    borderBottom: "2px solid rgba(225,232,238,1)",
    padding: "15px 20px",
    fontSize: "18px",
    color: "#364563",
    fontFamily: '"Roboto-Medium",sans-serif',
    width: "360px"
  },
  form: {
    margin: "10px 30px 20px 20px"
  },
  formInput: {
    padding: "13px 30px 13px 30px",
    marginTop: "20px",
    width: "287px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    fontSize: "14px"
  },
  options: {
    marginTop: "15px",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif',
    padding: "13px 30px 13px 30px",
    WebkitAppearance: "none",
    MozAppearance: "none",
    appearance: "none",
    width: "350px",
    backgroundColor: "#fff",
    borderColor: "#d9d9d9 #ccc #b3b3b3",
    borderradius: "4px",
    border: "2px solid #E1E8EE",
    cursor: "default",
    borderSpacing: "0",
    borderCollapse: "separate",
    regularSleep: "50px",
    outline: "none",
    overflow: "hidden",
    position: "relative",
    cursor: "pointer"
  },
  optionsDefault: {
    color: "rgba(169, 169, 169, 1)"
  },
  selectArrowZone: {
    cursor: "pointer",
    display: "table-cell",
    position: "relative",
    textAlign: "center",
    verticalAlign: "middle",
    width: "25px",
    paddingRight: "5px"
  },
  selectArrow: {
    borderColor: "#999 transparent transparent",
    borderStyle: "solid",
    borderWidth: "5px 5px 2.5px",
    display: "inline-block",
    regularSleep: "0",
    width: "0",
    position: "absolute",
    right: "-18em",
    top: "-1.75em"
  },
  button: {
    backgroundColor: "#ED81B9",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "0px 0px 20px 0px",
    cursor: "pointer"
  },
  register: {
    textDecoration: "none",
    margin: "20px 10px",
    color: "#43484D",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif'
  }
};
