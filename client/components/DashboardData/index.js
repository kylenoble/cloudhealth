/* eslint-disable no-nested-ternary */
/* eslint-disable no-shadow */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/sort-comp */
/* eslint-disable radix */
import React, { Component } from "react";
import { css } from "glamor";
import Link from "next/link";
import Router from "next/router";
import ReactTooltip from "react-tooltip";
import Progress from "../Progress";
import DashDataItem from "./dashDataItem.js";
import SummaryService from "../../utils/summaryService.js";
import UserService from "../../utils/userService.js";
import Colors from "../NewSkin/Colors.js";
import { font, fontWeight, combined, on_mobile } from '../NewSkin/Styles';
import GridContainer from "../NewSkin/Wrappers/GridContainer.js";
import CenteredWrapper from "../NewSkin/Wrappers/CenteredWrapper.js";
import Card from "../NewSkin/Wrappers/Card.js";
import Button from "../NewSkin/Components/Button";
import { UserContext } from "../../context/UserContext.js";
import { GetStatus, GetColor, GetPercentileRank } from "../Commons";

const summary = new SummaryService();
const user = new UserService();
let doh = [];

class DashData extends Component {
  constructor(props) {
    super(props);

    this.state = {
      avatar: "",
      fname: "",
      company: "",
      telemedicine_enabled: null,
      survey_isLocked: false,
      rank: "",
      overAllRank: "",
      answersData: "",
      body: {
        bmi: "0.00",
        whr: "0.00",
        value: "",
        HStatus: ""
      },
      nutrition: {
        value: "",
        HStatus: ""
      },
      sleep: {
        value: "",
        HStatus: ""
      },
      health: {
        value: "",
        HStatus: ""
      },
      stress: {
        value: "",
        HStatus: ""
      },
      exercise: {
        value: "",
        HStatus: ""
      },
      overallScore: {
        value: 0,
        HStatus: ""
      },
      animateCharts: false
    };

    this._addData = this._addData.bind(this);
    this._getPercentile = this._getPercentile.bind(this);
    this._getSummary = this._getSummary.bind(this);
    this._getProfile = this._getProfile.bind(this);
    this._addNotification = this._addNotification.bind(this);
  }

  static contextType = UserContext;

  componentDidMount() {
    this._getProfile();
    this._getPercentile();
    this._getSummary();

    this.state.animateCharts ? "" : this.animateChart();
  }

  _addNotification(name, note, lnk) {
    this.props.addnotification(name, note, lnk);
  }

  // GET UPDATED profile info
  _getProfile() {
    user
      .getLatest()
      .then(res => {
        this.setState({
          avatar: res.avatar,
          fname: res.name,
          company: res.company,
          telemedicine_enabled: res.telemedicine_enabled,
          survey_isLocked: res.healthsurvey_status === 'Locked'
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  // GET SUMMARY
  _getSummary() {
    summary
      .get()
      .then(res => {
        if (res.name === "Error") {
          console.log(res);
        } else if (res.length > 0) {
          this._createSummary(res);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  // GET PERCENTILE USING VIEWS TABLE
  _getPercentile() {
    //OVERALL RANK
    summary
      .getUserRank()
      .then(res => {
        if (res.name === "Error") {
          console.log(res);
        } else if (res !== undefined) {
          let position = parseInt(res.rank);
          const usersCount = parseInt(res.users_count);
          let pRank = GetPercentileRank(position, usersCount);

          this.setState({
            // eslint-disable-next-line radix
            rank: `${parseInt(pRank)}%`
          });
        }
      })
      .catch(e => {
        console.log(e);
      });

    //OVERALL RANK
    summary
      .getUserRank("All")
      .then(res => {
        if (res.name === "Error") {
          console.log(res);
        } else if (res !== undefined) {
          let position = parseInt(res.rank);
          const usersCount = parseInt(res.users_count);
          let oRank = GetPercentileRank(position, usersCount);

          this.setState({
            overAllRank: `${parseInt(oRank)}%`
          });
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  // get all answers
  // _getAllAnswer(){
  //   answer.getAnswerList({column: 'question_id', operator: 'gt', value: 0, order: 'asc', by: 'id'})
  //   .then(res => {
  //               this._checkData(res) // this will check if all required forms are filled
  //   })
  //   .catch(e => {
  //       console.log(e)
  //   })
  // }

  // Muscle Energy Heart
  // _checkData(ans) {
  //   let isSymptomsFilled = false;
  //   let isHealthFilled = false;
  //   let isDetoxFilled = false;
  //   const hi = [];
  //   const dtx = [];
  //   const meh = [];
  //   const een = [];
  //   const hm = [];
  //   const mtl = [];
  //   const other = [];
  //   const wd = [];

  //   // Health Issues
  //   for (const data of ans) {
  //     if (Constants.HI.indexOf(data.question_id) != -1) {
  //       if (hi.indexOf(data.group) == -1) {
  //         hi.push(data.group);
  //       }
  //     }
  //   }

  //   // Detox
  //   for (const data of ans) {
  //     if (Constants.DTX.indexOf(data.question_id) != -1) {
  //       if (dtx.indexOf(data.group) == -1) {
  //         dtx.push(data.group);
  //       }
  //     }
  //   }

  //   // get muscles, energy,heart data
  //   for (const data of ans) {
  //     if (Constants.MEH.indexOf(data.question_id) != -1) {
  //       if (meh.indexOf(data.group) == -1) {
  //         meh.push(data.group);
  //       }
  //     }
  //   }

  //   // Eyes Ear Nose
  //   for (const data of ans) {
  //     if (Constants.EEN.indexOf(data.question_id) != -1) {
  //       if (een.indexOf(data.group) == -1) {
  //         een.push(data.group);
  //       }
  //     }
  //   }

  //   // Head and Mind
  //   for (const data of ans) {
  //     if (Constants.HM.indexOf(data.question_id) != -1) {
  //       if (hm.indexOf(data.group) == -1) {
  //         hm.push(data.group);
  //       }
  //     }
  //   }

  //   // Mouth Throat Lungs
  //   for (const data of ans) {
  //     if (Constants.MTL.indexOf(data.question_id) != -1) {
  //       if (mtl.indexOf(data.group) == -1) {
  //         mtl.push(data.group);
  //       }
  //     }
  //   }

  //   // Constants.OTHER
  //   for (const data of ans) {
  //     if (Constants.OTHER.indexOf(data.question_id) != -1) {
  //       if (other.indexOf(data.group) == -1) {
  //         other.push(data.group);
  //       }
  //     }
  //   }

  //   // Weight Digestion
  //   for (const data of ans) {
  //     if (Constants.WD.indexOf(data.question_id) != -1) {
  //       if (wd.indexOf(data.group) == -1) {
  //         wd.push(data.group);
  //       }
  //     }
  //   }

  //   // SYMPTOMS CHECKER

  //   if (
  //     meh.length < 1 ||
  //     een.length < 1 ||
  //     hm.length < 1 ||
  //     mtl.length < 1 ||
  //     other.length < 1 ||
  //     wd.length < 1
  //   ) {
  //     isSymptomsFilled = false;
  //   } else {
  //     isSymptomsFilled = true;
  //   }

  //   if (hi.length > 0) {
  //     isHealthFilled = true;
  //   }

  //   if (dtx.length > 0) {
  //     isDetoxFilled = true;
  //   }

  //   if (
  //     isSymptomsFilled === false ||
  //     isHealthFilled === false ||
  //     isDetoxFilled === false
  //   ) {
  //     this._addData("Health"); // redirect to health form
  //   }

  //   const chk = {
  //     isSymptomsFilled,
  //     isHealthFilled,
  //     isDetoxFilled
  //   };

  //   return chk;
  // }

  _createSummary(summaries) {
    const body = this.state.body;
    const nutrition = this.state.nutrition;
    const sleep = this.state.sleep;
    const health = this.state.health;
    const stress = this.state.stress;
    const exercise = this.state.exercise;
    const overallScore = this.state.overallScore;

    let ltstBodyVal;
    let ltstExerciseVal;
    let ltstStressVal;
    let ltstNutritionVal;
    let ltstSleepVal;
    let ltstHealthVal;

    for (const summary of summaries) {
      const score = summary.value;
      let weightedScore = score * 0.167;

      if (summary.name === "weightScore") {
        ltstBodyVal = weightedScore;
        body.value = score;
        body.HStatus = this._getStatus(score);
      } else if (summary.name === "bmi") {
        weightedScore = 0;
        body.bmi = summary.value;
      } else if (summary.name === "whr") {
        weightedScore = 0;
        body.whr = summary.value;
      } else if (summary.name === "nutrition") {
        ltstNutritionVal = weightedScore;
        nutrition.value = score;
        nutrition.HStatus = this._getStatus(score);
      } else if (summary.name === "sleep") {
        ltstSleepVal = weightedScore;
        sleep.value = score;
        sleep.HStatus = this._getStatus(score);
      } else if (summary.name === "detox") {
        ltstHealthVal = weightedScore;
        health.value = score;
        health.HStatus = this._getStatus(score);
      } else if (summary.name === "stress") {
        ltstStressVal = weightedScore;
        stress.value = score;
        stress.HStatus = this._getStatus(score);
      } else if (summary.name === "exercise") {
        ltstExerciseVal = weightedScore;
        exercise.value = score;
        exercise.HStatus = this._getStatus(score);
      }
    }

    
    const hs =
      ltstBodyVal +
      ltstExerciseVal +
      ltstStressVal +
      ltstNutritionVal +
      ltstSleepVal +
      ltstHealthVal;
    overallScore.value = hs * 2;

    this.setState({
      body,
      nutrition,
      sleep,
      health,
      stress,
      exercise,
      overallScore
    });

    if (this.state.body.bmi < 15 || this.state.body.bmi > 50) {
      this._addNotification(
        "error in bmi",
        "Please Update Weight profile to correct the BMI. (Clik on MY HEALTH PROFILE Button)",
        "HealthProfile"
      );
    }
  }

  _getDOH() {
    doh = [];
    const body = this.state.body;
    const nutrition = this.state.nutrition;
    const sleep = this.state.sleep;
    const detox = this.state.health;
    const stress = this.state.stress;
    const exercise = this.state.exercise;
    if (sleep.HStatus === "Compromised") {
      doh.push({ name: "Sleep", value: "Compromised" });
    } else if (sleep.HStatus === "Alarming") {
      doh.push({ name: "Sleep", value: "Alarming" });
    }

    if (detox.HStatus === "Compromised") {
      doh.push({ name: "Detox", value: "Compromised" });
    } else if (detox.HStatus === "Alarming") {
      doh.push({ name: "Detox", value: "Alarming" });
    }

    if (body.HStatus === "Compromised") {
      doh.push({ name: "Weight", value: "Compromised" });
    } else if (body.HStatus === "Alarming") {
      doh.push({ name: "Weight", value: "Alarming" });
    }

    if (nutrition.HStatus === "Compromised") {
      doh.push({ name: "Nutrition", value: "Compromised" });
    } else if (nutrition.HStatus === "Alarming") {
      doh.push({ name: "Nutrition", value: "Alarming" });
    }

    if (stress.HStatus === "Compromised") {
      doh.push({ name: "Stress", value: "Compromised" });
    } else if (stress.HStatus === "Alarming") {
      doh.push({ name: "Stress", value: "Alarming" });
    }

    if (exercise.HStatus === "Compromised") {
      doh.push({ name: "Movement", value: "Compromised" });
    } else if (exercise.HStatus === "Alarming") {
      doh.push({ name: "Movement", value: "Alarming" });
    }

    const list = doh.map((item, i) => (
      <span key={i} style={{fontStyle: "bold", color: GetColor(item.value) }}>
        {item.name}
        {doh.length > i + 1 ? ", " : ""}
      </span>
    ));

    return <span>{list}</span>;
  }

  tooltipWrapper = description => {
    return `
      <div style="max-width: 400px; border-top-left-radius: 3px; border-top-right-radius: 3px; overflow: hidden">
        <p style="padding: 15px;"> ${description}</p>
      </div>`
  }

  _subOptimalWordings() {
    const wtdScore = this.state.overallScore.value.toFixed(1);
    let _subOptimalWordings = ``;
    if (wtdScore >= 1.0 && wtdScore <= 2.9) {
      _subOptimalWordings = `you maintain a good weight, you have a good sleep hygiene, you manage your stress well, you have good movement and good nutrition and you do not have much toxins in your body. In order to maintain this status or to set new health goals, book an appointment with a functional medicine expert.`;
    } else if (wtdScore >= 3.0 && wtdScore <= 4.9) {
      _subOptimalWordings = `there are still areas in your health that we can optimize to increase your energy, improve your sleep and nutrition, manage your weight and stress well and prevent toxins from accumulating in your body. To know how to best optimize your health, book an appointment with a functional medicine expert.`;
    } else if (wtdScore >= 5.0 && wtdScore <= 5.9) {
      _subOptimalWordings = `you are neither healthy or diseased. There are still areas in your health that we can optimize to increase your energy, improve your sleep and nutrition, manage your weight and stress well and prevent toxins from accumulating in your body.To know how to best optimize your health, book an appointment with a functional medicine expert.`;
    } else if (wtdScore >= 6.0 && wtdScore <= 8.5) {
      _subOptimalWordings = `the symptoms and health issues that you are now experiencing can be attributed to your body's failure to adjust to the different stressors and environmental factors affecting your health. If not given attention to, your health status may continue to decline. You need to book an appointment with a functional medicine expert as soon as possible to help alleviate your symptoms and address your concerns.`;
    } else if (wtdScore >= 8.6) {
      _subOptimalWordings = `you are experiencing symptoms primarily because your body is already failing to cope with the stressors, toxins and the poor sleep and nutrition that you are having. You need to book an appointment with a functional medicine expert as soon as possible to help alleviate your symptoms and address your concerns.`;
    }

    return _subOptimalWordings;
  }

  _subOptimalValue() {
    const wtdScore = this.state.overallScore.value.toFixed(1);
    return GetStatus(wtdScore);
  }

  _getStatus(score) {
    const wtdScore = score * 2;
    return GetStatus(wtdScore);
  }

  animateChart = () => {
    const timeout = setTimeout(() => {
      this.setState({
        animateCharts: true
      });
      clearTimeout(timeout);
    }, 500);
  };

  render() {
    let subopt = this.state.survey_isLocked ? this.state.overallScore.value.toFixed(2) * 10 : 0;

    subopt -= 8; // offset to match colorbar
    const stratificationStyle = css({
      position: "absolute",
      display: "flex",
      alignItems: "flex-end",
      //justifyContent : 'flex-end',
      left: 0,
      top: 0,
      bottom: "5px",
      width: `${subopt}%`,
      minHeight: "50px",
      paddingRight: "10px",
      backgroundImage: 'url("/static/images/marker.svg")',
      backgroundRepeat: "no-repeat, no-repeat",
      backgroundPosition: "bottom right"
    });

    const rankStyle = css({
      marginTop: 15,
      textAlign: "left",
      color: Colors.blueDarkAccent,
      "@media(max-width: 820px)": {
        textAlign: "left"
      }
    });

    const withOverallScore = this.state.overallScore.value > 0;
    const overAllScoreIsNan = isNaN(this.state.overallScore.value);

    const cardProps = {
      square: true,
      squareSize: '100%',
      centered: true,
      classes: [on_mobile([], { width: '100%', padding: 15, minHeight: 175 })]
    }

    return (
      <div id="main-container" className={containerStyle}>
        <ReactTooltip />
        <div id="info" className={info}>
          <div className={dataContainer}>

            <CenteredWrapper classes={[on_mobile([], { paddingRight: 0 })]} styles={{ paddingRight: 20 }}>
              <GridContainer contain gap={20} columnSize={'3fr 5fr'} classes={[on_mobile([], { gridTemplateColumns: '1fr' })]} styles={{ placeContent: 'center' }}>
                <div className={combined([personalInfo, on_mobile([], { width: '100vw', margin: '0 auto' })])}>
                  <div className={avatarStyle}>
                    <img
                      alt={this.state.avatar}
                      className={imageStyle}
                      src={
                        this.state.avatar
                          ? `/static/avatar/${this.state.avatar}`
                          : "/static/icons/user.svg"
                      }
                    />
                  </div>
                  <div className={textInfo}>
                    <div className={textItem}>
                      <span className={nameTitleLabel}>
                        {this.props.user.name.toUpperCase()}
                      </span>
                    </div>
                    <div className={textItem}>
                      <span className={companyTitleLabel}>
                        {this.props.user.company}
                      </span>
                    </div>
                  </div>
                </div>

                <GridContainer contain columnSize={'3fr 1fr'} gap={20} classes={[on_mobile([], { padding: '0 20px' })]} >
                  <Card classes={[font(0.9), fontWeight(500), on_mobile([], { width: '100%', margin: '0 auto' })]}>
                    <div className={straDiv}>
                      <div className={stratificationDiv}>
                        <div
                          id="stratificationColorBar"
                          className={stratificationColorBar}
                        >
                          <div id="marker" className={stratificationStyle}>
                            <div id="optimal-value" className={on_mobile([font(0.8)])} style={{ position: 'absolute', right: 0, transform: 'translateX(110%)', color: Colors.blueDarkAccent, fontSize: '1em', width: '100%', textAlign: 'left' }}>
                              {this.state.survey_isLocked && this._subOptimalValue()}{" "}
                              {isNaN(this.state.overallScore.value) || !this.state.survey_isLocked
                                ? "Incomplete"
                                : this.state.overallScore.value.toFixed(1)}
                            </div>
                          </div>
                        </div>

                        <div className={rankStyle}>
                          <div
                            data-tip={this.state.survey_isLocked && this.state.rank ?
                              this.tooltipWrapper("The higher you are in the rank, the more alarming your health status is") :
                              this.tooltipWrapper("Please complete and submit your health profile")
                            }
                            data-type="light"
                            data-html={true}
                            className={combined([font(1.3), on_mobile([], { fontSize: '1em' })])}
                          >
                            Your Percentile Rank in your company:{" "}
                            {this.state.survey_isLocked ? this.state.rank : '--'}
                          </div>
                          <div
                            data-tip={ this.state.survey_isLocked && this.state.overAllRank ?
                              this.tooltipWrapper("The higher you are in the rank, the more alarming your health status is") :
                              this.tooltipWrapper("Please complete and submit your health profile")
                            }
                            className={combined([font(1.3), on_mobile([], { fontSize: '1em' })])}
                            data-type="light"
                            data-html={true}
                          >
                            Overall Percentile Rank: {this.state.survey_isLocked ? this.state.overAllRank : '--'}
                          </div>
                        </div>
                      </div>
                    </div>
                  </Card>


                  <Card styles={{ maxWidth: 180, width: '100%', margin: '0 auto' }}>
                    <div className={dataItem}>
                      <Progress
                        height={120}
                        width={120}
                        score={
                          isNaN(this.state.overallScore.value) || !this.state.survey_isLocked
                            ? "Incomplete"
                            : this.state.overallScore.value.toFixed(1)
                        }
                        percent={
                          isNaN(this.state.overallScore.value) || !this.state.survey_isLocked
                            ? 0
                            : this.state.animateCharts
                              ? this.state.overallScore.value.toFixed(1) * 10
                              : 0
                        }
                        title="Health Risk Score"
                        status={this._subOptimalValue()}
                        withTitle
                      />
                    </div>
                  </Card>
                </GridContainer>

              </GridContainer>
            </CenteredWrapper>
          </div>
        </div>

        <div id="data-container" style={{ marginTop: 20 }}>
          <CenteredWrapper styles={{ paddingBottom: 20 }}>
            <GridContainer gap={20} contain columnSize={'61% 42%'} paddingOnMobile={20}>

              <div id="charts-wrapper">
                <GridContainer styles={{ width: '100%' }} classes={[on_mobile([], { width: '85vw' })]} gap={20} contain columnSize={'1fr 1fr 1fr'} columnsOnMobile={2}>

                  <Card {...cardProps}>
                    <DashDataItem
                      title="Weight"
                      label="Body Details"
                      icon="static/images/body.svg"
                      value={this.state.survey_isLocked ? this.state.body.value : 0}
                      bmi={this.state.survey_isLocked ? this.state.body.bmi : 0}
                      whr={this.state.survey_isLocked ? this.state.body.whr : 0}
                      status={this.state.survey_isLocked ? this.state.body.HStatus : ""}
                      addData={this._addData}
                      survey_isLocked={this.state.survey_isLocked}
                    />
                  </Card>

                  <Card {...cardProps}>
                    <DashDataItem
                      title="Nutrition"
                      label="Nutrition History"
                      icon="static/images/nutrition.svg"
                      value={this.state.survey_isLocked ? this.state.nutrition.value : 0}
                      status={this.state.survey_isLocked ? this.state.nutrition.HStatus : ""}
                      addData={this._addData}
                      survey_isLocked={this.state.survey_isLocked}
                    />
                  </Card>

                  <Card {...cardProps}>
                    <DashDataItem
                      title="Sleep"
                      label="Sleep Patterns"
                      icon="static/images/sleep.svg"
                      value={this.state.survey_isLocked ? this.state.sleep.value : 0}
                      status={this.state.survey_isLocked ? this.state.sleep.HStatus : ""}
                      addData={this._addData}
                      survey_isLocked={this.state.survey_isLocked}
                    />
                  </Card>

                  <Card {...cardProps}>
                    <DashDataItem
                      title="Detox"
                      label="Health Profile"
                      icon="static/images/health.svg"
                      value={this.state.survey_isLocked ? this.state.health.value : 0}
                      status={this.state.survey_isLocked ? this.state.health.HStatus : ""}
                      addData={this._addData}
                      survey_isLocked={this.state.survey_isLocked}
                    />
                  </Card>

                  <Card {...cardProps}>
                    <DashDataItem
                      title="Stress"
                      label="Stress Level"
                      icon="static/images/stress.svg"
                      value={this.state.survey_isLocked ? this.state.stress.value : 0}
                      status={this.state.survey_isLocked ? this.state.stress.HStatus : ""}
                      addData={this._addData}
                      survey_isLocked={this.state.survey_isLocked}
                    />
                  </Card>

                  <Card {...cardProps}>
                    <DashDataItem
                      title="Movement"
                      label="Movement"
                      icon="static/images/exercise.svg"
                      value={this.state.survey_isLocked ? this.state.exercise.value : 0}
                      status={this.state.survey_isLocked ? this.state.exercise.HStatus : ""}
                      addData={this._addData}
                      survey_isLocked={this.state.survey_isLocked}
                    />
                  </Card>
                </GridContainer>
              </div>

              {
                <section id="heath-status">
                  <Card scrollable classes={[on_mobile([], { fontSize: '1em' })]} styles={{ color: '#777', fontSize: '.8em' }}>
                    <span className={headerStyle}>
                      <h1 className={combined([headerText], { textAlign: 'left' })}>Health Status</h1>
                    </span>

                    <div className={panelContainer}>
                      {!this.state.survey_isLocked ? (
                          <div className={healthStatus}>
                            <p style={{marginBottom: 20}}>By having a complete health profile accomplished, you will see a comprehensive picture of your general health status.</p>
                            <p>
                              Click <span
                              onClick={() => {
                                this._goTo("healthprofile");
                              }}
                              className={linkStyle}
                            >here</span> to complete your health profile.
                            </p>
                          </div>
                        ) : (
                          <div className={healthStatus}>
                            <p style={{ marginBottom: 10 }}>Your overall health risk score is{" "}
                              <strong style={{color: GetColor(this._subOptimalValue())}}>
                                {this.state.overallScore.value.toFixed(1)},{" "}
                                {this._subOptimalValue()}.
                              </strong>
                            </p>

                            <p style={{ marginBottom: 10, textAlign: 'justify' }}>
                              <strong style={{color: GetColor(this._subOptimalValue())}}>{this._subOptimalValue()}</strong> means {this._subOptimalWordings()}
                            </p>

                            <p style={{ textAlign: 'justify' }}>
                              <strong className={bookStyle}>{this._getDOH()} </strong>
                              {doh.length < 1
                                ? ""
                                : doh.length < 2
                                  ? " is the area that greatly affects your health status. "
                                  : "are the areas that greatly affect your health status. "}
                              Please take note however that the goal of this health plan is
                              not to single out certain health issues but to improve your
                              health as a whole. Functional medicine has a holistic approach
                              to health and our goal is for you to achieve optimal status in
                              all these areas.
                          </p>

                          </div>
                        )}
                    </div>
                  </Card>
                </section>
              }

            </GridContainer>

            {
              this.state.telemedicine_enabled === 'Approved' && this.state.survey_isLocked ?
              <Button classes={[on_mobile([], { width: '85vw', fontSize: '0.9em' })]} type={'pink'} styles={{ maxWidth: 'fit-content', margin: '20px auto 0' }}>
                <Link href="/activities">
                  <a className={bookStyle}>
                    Book an online consult now to know how you can best
                    improve your health
                    </a>
                </Link>
              </Button> : null
            }

          </CenteredWrapper>

        </div>
      </div >
    );
  }

  _addData() {
    Router.push("/activities");
  }

  _goTo(lnk) {
    Router.push(`/${lnk}`);
  }
}

let commentStyle = css({
  display: "flex",
  background: "#52617f",
  color: "#fff",
  fontSize: 20,
  textTransform: "capitalize",
  flex: 1,
  height: "100px",
  justifyContent: "center",
  alignItems: "center"
});

const styles = {
  Optimal: {
    fontStyle: "bold",
    color: "#83b85f"
  },
  Suboptimal: {
    fontStyle: "bold",
    color: "#83cde7"
  },
  Neutral: {
    fontStyle: "bold",
    color: "#ffc82f"
  },
  Compromised: {
    fontStyle: "bold",
    color: "#f6921e"
  },
  Alarming: {
    fontStyle: "bold",
    color: "#ec1c24"
  }
};

let bookStyle = css({
  textTransform: "uppercase",
  // cursor: "pointer"
});

let linkStyle = css({
  // padding: "10px 15px",
  textAlign: "center",
  fontStyle: "bold",
  color: Colors.skyblue,
  cursor: "pointer",
  borderBottom: '1px solid white',
  borderRadius: 0,
  transition: '300ms ease',
  boxShadow: '0 1px 1px rgba(0,0,0,0)',
  ":hover": {
    color: Colors.pink,
    borderBottomColor: Colors.pink
  }
});

let healthStatus = css({
  fontSize: "1rem",
  textAlign: "justify"
});

let containerStyle = css({
  // display: "flex",
  // flexDirection: "column",
  // alignItems: "center",
  // justifyContent: "flex-start",
  width: "100vw"
  //  paddingTop: '40px',
  // '@media(max-width: 767px)': {
  //     display: 'flex',
  //     flexDirection: 'column',
  //     margin: '90px auto 0 auto',
  //     paddingTop: 0,
  // }
});

let panelContainer = css({
  // borderTop: "2px solid gray",
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  // justifyContent: "flex-start",
  width: "100%",
  //  backgroundColor: 'rgb(240, 240, 240)',
  "@media(max-width: 767px)": {
    // display: "flex",
    // flexDirection: "column"
  }
});

let info = css({
  // display: "flex",
  // flexDirection: "row",
  // justifyContent: "center",
  // alignItems: "center",
  // maxWidth: 1518,
  width: "100%",
  // marginTop: "30px",
  // marginLeft: '20px',
  padding: "40px 0",
  // borderBottom: "2px solid gray"
  // border: '1px solid rgba(204, 204, 204, 0.3)',
  //  boxShadow: '0px 0px 1px 3px rgba(204,204,204,.1)',
  //  background: 'white',
  background: Colors.skyblue
});

let dataContainer = css({
  // marginTop: 20,
  // display: "flex",
  // flexWrap: "wrap",
  // width: "calc(98% + 10px)",
  // alignItems: "center",
  // padding: "10px 0",
  // justifyContent: "space-between",
  "@media(min-width: 417px)": {
    flexDirection: "row",
    flexWrap: "no-wrap"
  }
});

let personalInfo = css({
  flex: "1 0 500px",
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "center",
  "@media(max-width: 820px)": {
    flex: "1 1 100%",
    flexDirection: "row",
    flexWrap: "no-wrap"
  }
});

let avatarStyle = css({
  display: "flex",
  color: "rgb(54, 69, 99)",
  width: 100,
  height: 100,
  borderRadius: "100%",
  overflow: "hidden",
  position: "relative"
  // border: "5px solid whitesmoke"
});

let imageStyle = css({
  // border: "1px solid rgba(204, 204, 204, 0.3)",
  //    padding: '15px 15px',
  position: "absolute",
  top: "50%",
  left: 0,
  transform: "translateY(-50%)",
  display: "block",
  width: "100%",
  //  color: 'rgb(54, 69, 99)',
  "@media(max-width: 767px)": {
    // height: "75px",
    // width: "75px"
  }
});

let textInfo = css({
  width: "50%",
  display: "flex",
  flexDirection: "column",
  flexWrap: "wrap",
  fontSize: "16px",
  alignItems: "flex-start",
  justifyContent: "flex-start",
  marginLeft: "20px",
  "@media(max-width: 417px)": {
    fontSize: "12px"
  }
});

let textItem = css({
  textAlign: 'left'
});

let nameTitleLabel = css({
  flexDirection: "flex-start",
  fontFamily: '"Roboto-Black",sans-serif',
  // marginLeft: "5px",
  fontSize: "1.3em",
  // color: "rgb(54, 69, 99)"
  color: "#fff",
  fontWeight: 600,
  textAlign: 'left !important',
  display: 'block'
});

let companyTitleLabel = css({
  flexDirection: "flex-start",
  fontFamily: '"Roboto",sans-serif',
  // marginLeft: "5px",
  fontSize: "16px",
  fontWeight: 300,
  // color: "rgb(54, 69, 99)"
  color: "#fff"
});

let stratificationColorBar = css({
  position: "relative",
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  // marginTop: "15px",
  width: "100%",
  height: "20px",
  marginRight: "5px",
  marginBottom: 50,
  backgroundImage: 'url("/static/images/colorbar.svg")',
  backgroundRepeat: "no-repeat, no-repeat",
  //background-position: top right, bottom right;
  backgroundSize: "100%"
});

let straDiv = css({
  display: "flex",
  justifyContent: "space-around",
  flex: "1 0 500px"
  // padding: "15px 0 15px 0"
});

let stratificationDiv = css({
  position: "relative",
  width: "100%",
  //border: '1px solid rgba(204, 204, 204, 0.3)',
  //boxShadow: '-5px 5px 30px #c5c2c2',
  justifyContent: "space-around",
  height: "120px",
  "@media(max-width: 820px)": {
    flex: "1 1 100%",
    flexDirection: "row",
    flexWrap: "no-wrap"
  }
});

let dataItem = css({
  flex: "1 1 100px",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  marginLeft: "auto",
  marginRight: "auto",
  "@media(max-width: 820px)": {
    fontSize: "18px",
    flex: "1 1 100%"
  }
});

let headerStyle = css({
  // display: "flex",
  // height: "30px",
  // maxWidth: 1518,
  // width: "90%",
  // margin: "0 auto",
  // textAlign: "center",
  // alignItems: "center",
  // justifyContent: "center",
  // flexDirection: "row",
  // padding: 20,
  // borderBottom: "2px solid rgba(204, 204, 204, 0.3)",
  // "@media(max-width: 1139px)": {}
});

let headerText = css({
  fontWeight: 500,
  margin: 0,
  marginBottom: 10,
  // background: '#ED81B9',
  // color: "rgb(54, 69, 99)",
  // fontFamily: '"Roboto",sans-serif',
  fontSize: "1rem",
  lineHeight: "30px",
  "@media(max-width: 1139px)": {
    fontSize: "1rem"
  }
});

export default DashData;
