import React, { Component } from "react";
import { css } from "glamor";
import Progress from "../Progress";
import { on_mobile, combined } from "../NewSkin/Styles";
import IconWrapper from "../NewSkin/Icons/IconWrapper";

class DashDataItem extends Component {
  constructor(props) {
    super(props);

    this._addData = this._addData.bind(this);

    this.state = {
      animateCharts: false,
      value: 0
    };
  }

  render() {
    return this._renderItem();
  }

  animateChart = () => {
    const timeout = setTimeout(() => {
      this.setState({
        animateCharts: true
      });
      clearTimeout(timeout);
    }, 500);
  };

  tooltipWrapper = (description, align = 'left') => {
    return `
      <div style="max-width: 300px; min-width: 200px; border-top-left-radius: 3px; border-top-right-radius: 3px; overflow: hidden">
        <span style="padding: 5px; background: #364563; display: block; text-align:center; color: white; font-size: 14px; text-transform: uppercase">${
      this.props.title
      }</span>
        <p style="padding: 15px; text-align: ${align}"> ${description}</p>
      </div>`;
  };

  setValue = () => {
    setTimeout(() => {
      this.setState({
        value: this.props.value * 20
      })
    }, 500);
  }

  _renderItem() {
    this.setValue();
    if (this.props.title == "Weight") {
      this.bmiwhr =
        "BMI: " +
        (this.props.bmi > 0 ? this.props.bmi : "--") +
        "  |  WHR:" +
        (this.props.whr > 0 ? this.props.whr : "--");
    }

    return (
      <div
        key={this.props.title}
        className={categoryDataStyle}
        data-tip={
          !this.props.survey_isLocked
            ? this.tooltipWrapper(
              "Please complete and submit your health profile"
            )
            : this.tooltipWrapper(
              this.props.status, 'center'
            )
        }
        data-type="light"
        data-html={true}
      >
        <span className={headerStyle(this.props.noIcon)}>
          {
            this.props.noIcon ? null : this._renderIcon()
          }
          <h1 className={headerText(this.props.noIcon)}>{this.props.title}</h1>
        </span>

        <span
          className={combined(
            [
              bodySubTxt,
              on_mobile([], { width: "100%", display: "block", marginTop: 10 })
            ],
            { marginTop: 5 }
          )}
        >
          {this.props.title === 'Weight' && this.bmiwhr}

        </span>
        <div className={dataItem} {...dataValue}>
          <Progress
            tracker={this.props.tracker}
            animate={this.state.animateCharts ? "" : this.animateChart()}
            height={this.props.tracker ? 130 : 90}
            width={this.props.tracker ? 130 : 90}
            title={this.props.title}
            score={this.props.value > 0 ? (this.props.value * 2).toFixed(1) : 0}
            // percent={this.state.animateCharts ? this.props.value * 20 : 0}
            percent={this.state.value}
            status={this.props.status}
            withTitle={false}
          />
        </div>
      </div>
    );
  }

  _addData(e) {
    this.props.addData(e.target.id);
  }

  _renderIcon() {
    switch (this.props.title) {
      case "Weight":
        return <IconWrapper icon="Weight" />;

      case "Nutrition":
        return <IconWrapper icon="Nutrition" />;

      case "Sleep":
        return <IconWrapper icon="Sleep" />;

      case "Detox":
        return <IconWrapper icon="Detox" />;

      case "Stress":
        return <IconWrapper icon="Stress" />;

      case "Movement":
        return <IconWrapper icon="Exercise" />;
      default:
        return ''
    }
  }
}

let dataItem = css({
  margin: "5px",
  "@media(min-width: 417px)": {
    textAlign: "center",
    paddingRight: "0",
    paddingLeft: "0"
  }
});

let dataValue = css({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  "& .data-value-good": {
    height: "125px",
    width: "125px",
    color: "#77E9AF",
    fill: "#77E9AF",
    "@media(max-width: 417px)": {
      height: "90px",
      width: "90px"
    }
  },
  "& .data-value-bad": {
    height: "125px",
    width: "125px",
    color: "#F7567C",
    fill: "#F7567C",
    "@media(max-width: 417px)": {
      width: "90px",
      height: "90px"
    }
  }
});

let headerStyle = (noIcon) => css({
  display: "grid",
  gridTemplateColumns: noIcon ? '1fr' : '.3fr 1fr',
  justifyContent: "center",
  alignContent: " center",
  width: "auto"
});

let bodySubTxt = css({
  display: "flex",
  width: "100%",
  fontSize: "small",
  textAlign: "center",
  alignItems: "center",
  justifyContent: "center",
  flexDirection: "row",
  padding: "0 auto",
  marginTop: "-10px",
  "@media(max-width: 1139px)": {}
});

let headerText = (noIcon) => css({
  textTransform: "uppercase",
  color: "rgb(54, 69, 99)",
  fontFamily: '"Roboto",sans-serif',
  fontSize: "1.05rem",
  margin: 0,
  lineHeight: "30px",
  "@media(max-width: 1139px)": {
    fontSize: "1rem"
  },
  width: '100%',
  textAlign: noIcon ? 'center' : 'left'
});

let categoryDataStyle = css({
  flex: "0 1 calc(50% - 10px)",
  transition: "box-shadow 200ms cubic-bezier(0.4, 0.0, 0.2, 1)",
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  justifyContent: "center",
  alignItems: "center",
  background: "white",
  margin: "5px",
  "@media(min-width: 1100px)": {
    flex: "1 1",
    transition: "box-shadow 200ms cubic-bezier(0.4, 0.0, 0.2, 1)",
    width: "100%",
    textAlign: "left",
    justifyContent: "center",
    alignItems: "center",
    display: "flex",
    flexDirection: "row",
    flexWrap: "no-wrap",
    padding: 0
  }
});

export default DashDataItem;
