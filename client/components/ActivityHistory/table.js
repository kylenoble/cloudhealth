import React from 'react'
import ReactTable from "react-table"
import moment from 'moment-timezone'
import { css } from 'glamor'
import { _dates, _accessor, _checkStatus, showIcon, tooltipWrapper, SubscriptionIcon, _autoresize } from './methods'
import { ReactTableStyle } from "../NewSkin/Styles";
import NoData from "../NewSkin/Components/NoData";

const Table = props => {
  // column headers
  const _headers = () => {
    const type = props.type
    if (type === 'My Appointments') {
      return ['Doctor', 'Booked Appointment Date', 'Requested Date', 'Status']
    } else if (type === 'My Events') {
      return ['Event', 'Start Date', 'End Date', 'Status']
    } else {
      return ['Subscription Name', 'Subscription Start Date', 'Subscription End Date', 'Date of Enrollment', 'Enrollment Status']
    }
  }

  // render table columns
  const _renderColumns = () => {
    const type = props.type
    const histories = props.histories

    let columns = []
    let data = []
    const headers = _headers()
console.log(histories)
    if (histories.length > 0) {
      histories.forEach(history => {

        const appointments_data = {
          doctor: history.doctor_name || history.name,
          booked_date: _dates(type, history, null),
          requested_date: _dates(type, history, null),
          status: _checkStatus(history),
          description: history.concern || history.description,
        }

        const subscriptions_data = {
          name: history.name,
          start: history.start_date,
          end: history.end_date,
          date_created: _dates(type, history, 'created'),
          // status: _checkStatus(history)
          status: history.enrollment_status
        }

        const events_data = {
          doctor: history.doctor_name || history.name,
          booked_date: _dates(type, history, 'start'),
          requested_date: _dates(type, history, 'end'),
          status: _checkStatus(history),
          description: history.concern || history.description,
          venue: history.venue,
          host: history.host,
        }

        type === 'My Events' ? data.push(events_data) : type === 'My Subscriptions' ? data.push(subscriptions_data) : data.push(appointments_data)
      })
    }

    if (type !== 'My Subscriptions') {
      if(data.length > 0){
        data.filter((item) => {
          columns = [
            {
              id: 1,
              Header: headers[0],
              accessor: `${_accessor(headers[0])}`,
              Cell: item => (
                <div
                  style={{ padding: 5, cursor: 'help' }}
                  data-tip={tooltipWrapper(item.original, props.type)}
                  data-type="light"
                  data-html={true}
                  onMouseEnter={type === 'My Events' ? _autoresize : null}
                >
                  <div className={`${itemChildStyle} itemChildStyle`}>
                    {item.original.doctor}
                  </div>
                </div>
              )
            },
            {
              id: 2,
              Header: headers[1],
              accessor: `${_accessor(headers[1])}`,
              Cell: item => (
                <div style={{ padding: 5, cursor: 'help' }}
                  data-tip={tooltipWrapper(item.original, props.type)}
                  data-type="light"
                  data-html={true}
                  onMouseEnter={type === 'My Events' ? _autoresize : null}
                >
                  {
                    <div className={itemChildStyle} style={{ textAlign: 'center' }}>
                      {item.original.booked_date[0]}
                      <span className={time} style={{ marginLeft: 10 }}>
                        {item.original.booked_date[1]}
                      </span>
                    </div>
                  }
                </div>
              )
            },
            {
              id: 3,
              Header: headers[2],
              accessor: `${_accessor(headers[2])}`,
              Cell: item => (
                <div style={{ padding: 5, cursor: 'help' }}
                  data-tip={tooltipWrapper(item.original, props.type)}
                  data-type="light"
                  data-html={true}
                  onMouseEnter={type === 'My Events' ? _autoresize : null}
                >
                  {
                    <div className={itemChildStyle} style={{ textAlign: 'center' }}>
                      {item.original.requested_date[0]}
                      <span className={time} style={{ marginLeft: 10 }}>
                        {item.original.requested_date[1]}
                      </span>
                    </div>
                  }

                </div>
              )
            },
            {
              id: 4,
              Header: headers[3],
              accessor: 'status',
              Cell: item => (
                <div
                  className={itemChildStyle}
                  style={{ textAlign: 'center', cursor: 'help' }}
                  data-tip={tooltipWrapper(item.original, props.type)}
                  data-type="light"
                  data-html={true}
                  onMouseEnter={type === 'My Events' ? _autoresize : null}
                >
                  {item.original.status}
                </div>
              )
            },
          ]
        }
        )
      }else{
        return <NoData text="No data" />
      }
    }
    else if (type === 'My Subscriptions') {
      if(data.length > 0){
        data.filter((item) => {
          columns = [
            {
              id: 1,
              Header: headers[0],
              accessor: `${_accessor(headers[0])}`,
              Cell: item => (
                <div style={{ padding: 5, cursor: 'default' }} >
                  <div className={`${itemChildStyleSubs} itemChildStyle`}>
                    {<SubscriptionIcon src={showIcon(item.original.name)} name={item.original.name} />}
                  </div>
                </div>
              )
            },
            {
              id: 2,
              Header: headers[1],
              accessor: `${_accessor(headers[1])}`,
              Cell: item => (
                <div style={{ padding: 5, cursor: 'default' }} >
                  {<div className={itemChildStyle} style={{ textAlign: 'center' }}>{item.original.start}</div>}
                </div>
              )
            },
            {
              id: 3,
              Header: headers[2],
              accessor: `${_accessor(headers[2])}`,
              Cell: item => (
                <div style={{ padding: 5, cursor: 'default' }} >
                  {<div className={itemChildStyle} style={{ textAlign: 'center' }}>{item.original.end}</div>}
                </div>
              )
            },
            {
              id: 4,
              Header: headers[3],
              accessor: `${_accessor(headers[3])}`,
              Cell: item => (
                <div className={itemChildStyle} style={{ textAlign: 'center', cursor: 'default' }} >
                  {`${item.original.date_created[0]} ${item.original.date_created[1]}`}
                </div>
              )
            },
            {
              id: 5,
              Header: headers[4],
              accessor: 'status',
              Cell: item => (
                <div className={itemChildStyle} style={{ textAlign: 'center', cursor: 'default' }} >{_checkStatus(item.original)}</div>
              )
            }
          ]
        }
        )
      }else{
        return <NoData text="No data" />
      }
    }

    return (
      <section className={ReactTableStyle} style={{ textAlign: 'left', maxWidth: '100%' }}>
        <ReactTable
          data={data}
          columns={columns}
          defaultPageSize={5}
          minRows={0}
          className={`-striped -highlight react-table ${type === 'My Subscriptions' ? subs : reactTable}`}
          width={90}
        />
      </section>
    )
  }

  return (
    <div id="table-wrapper">
      {_renderColumns()}
    </div>
  )
}

export default Table


// Styles
let subs = css({
  '> div': {
    '> div': {
      '> div': {
        '> div': {
          '> div.rt-td': {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
          },
          '> div:nth-child(1)': {
            justifyContent: 'left !important'
          }
        }
      }
    }
  }
})

let reactTable = css({
  '> div': {
    '> div': {
      '> div': {
        '> div': {
          '> div.rt-td': {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
          }
        }
      }
    }
  }
})

let historyHeader = css({
  padding: '5px 15px',
  marginBottom: '10px',
  display: 'inline-block',
  background: '#80cde9',
  color: 'white',
  textTransform: 'uppercase'
})

let itemChildStyleSubs = css({
  display: 'flex',
  boxSizing: 'border-box',
  alignItems: 'center',
  fontSize: 13
})

let itemChildStyle = css({
  display: 'flex',
  boxSizing: 'border-box',
  alignItems: 'center',
  justifyContent: 'center',
  fontSize: 13
})

let time = css({
  color: '#969696',
})
