import { css } from 'glamor'
import HistoryItem from './historyItem'
import moment from 'moment-timezone'
import ReactTooltip from 'react-tooltip'


const Status = (props) => {
    const status = props.status
    const color = props.color
    const bColor = props.bColor
    return (
        <span
            style={{
                color: color,
                padding: '5px 10px',
                border: `1px solid ${bColor}`,
                width: 140,
                display: 'block',
                margin: '0 auto',
                background: 'white',
                fontWeight: 600
            }}>{status}</span>
    )
}

export default class extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            headers: [],
            type: '',
            historyType: '',
            histories: [],
            historyWrapperHeight: 0,
            readyToView: true
        }
    }

    componentWillReceiveProps() {
        this.setState((prevState, props) => ({
            readyToView: !prevState.readyToView
        }))
        this.setHistories()
    }

    setHistories = () => {
        setTimeout(() => {
            this.setState((prevState, props) => ({
                readyToView: !prevState.readyToView
            }))
        }, 1)
    }

    convertDate = date => {
        let newDate = date.split(' ')
        return moment(newDate[0]).format('MMMM DD YYYY')
    }


    checkStatus = (args) => {
        const bBlue = 'rgba(128, 205, 233, .3)'
        const bOrange = 'rgba(255, 165, 0, .3)'
        const bRed = 'rgba(255, 0, 0,.3)'
        const bGray = 'rgba(150, 150, 150, .3)'
        const blue = 'rgb(128, 205, 233)'
        const orange = 'rgb(255, 165, 0)'
        const red = 'rgb(255, 0, 0)'
        const gray = 'rgb(150, 150, 150)'

        if (args.active) {
            switch (args.status) {
                case 3:
                    return <Status status="Cancelled" color={red} bColor={bRed} />
                case 2:
                    return <Status status="Rescheduled" color={orange} bColor={bOrange} />
                case null:
                    return <Status status="Accepted" color={blue} bColor={bBlue} />
                case 'Approved':
                    return <Status status="Approved" color={blue} bColor={bBlue} />
                case 'Pending':
                    return <Status status="Pending" color={orange} bColor={bOrange} />
                case 'Declined':
                    return <Status status="Declined" color={red} bColor={bRed} />
                case 'Active':
                    return <Status status="Active" color={blue} bColor={bBlue} />
                default:
                    break;
            }
        } else {
            switch (args.status) {
                case 3:
                    return <Status status="Cancelled" color={red} bColor={bRed} />
                case 2:
                    return <Status status="Rescheduled" color={orange} bColor={bOrange} />
                case null:
                    return <Status status="Pending Approval" color={gray} bColor={bGray} />
                case 'Approved':
                    return <Status status="Approved" color={blue} bColor={bBlue} />
                case 'Pending':
                    return <Status status="Pending" color={orange} bColor={bOrange} />
                case 'Declined':
                    return <Status status="Declined" color={red} bColor={bRed} />
                case 'Active':
                    return <Status status="Active" color={blue} bColor={bBlue} />
                default:
                    break;
            }
        }
    }

    renderHistories = () => {
        const historyList = [...this.props.histories]

        let histories = historyList.map((history, i) => {
            let newApp = {
                name: history.doctor_name,
                desc: history.concern,
                date: history.date_requested,
                date_requested: history.datetime,
                concern: history.concern,
            }

            if (this.props.type == 'My Appointments') {
                if (history.doctor_name) {
                    const args = { status: history.status, active: history.active }
                    const stats = this.checkStatus(args)
                    const newHistory = { ...newApp, status: stats }

                    return <HistoryItem type={this.props.type} key={i} historyDetails={newHistory} />
                }
            } else if (this.props.type == 'My Subscriptions') {
                const args = { status: history.status, active: true }
                const stats = this.checkStatus(args)
                const { status, ...newH } = history
                const newHistory = { ...newH, status: stats }

                return <HistoryItem type={this.props.type} key={i} historyDetails={newHistory} />
            } else {
                let event = {
                    name: history.name,
                    desc: history.description,
                    venue: history.venue,
                    startDate: history.start_date,
                    startTime: history.start_time,
                    endDate: history.end_date,
                    endTime: history.end_time,
                    host: history.host.host,
                }

                return <HistoryItem type={this.props.type} key={i} historyDetails={event} />
            }
        })
        return histories
    }

    render() {
        let headers = this.props.headers

        let header = (
            <div key={'Ehdr'} className={itemHeadWrapper} style={{ fontWeight: '600' }}>
                <div className={eventItemStyle} style={this.props.type == 'My Subscriptions' ? { flexGrow: 3 } : { flexGrow: 2.5 }}>{headers[0]}</div>

                {
                    this.props.type == 'My Appointments' ?
                        <div className={eventItemStyle} style={{ position: 'relative', flexGrow: 5, background: '#f5f5f51f' }}>
                            <div style={{ width: '100%', background: '#eeeeee3e', padding: 5 }}>Date and Time</div>
                            <small className={itemChildStyle} style={{ position: 'relative' }}>{headers[1]}</small>
                            <small className={itemChildStyle} style={{ position: 'relative' }}>{headers[2]}</small>
                        </div>
                        :
                        this.props.type == 'My Events' ?
                            <div className={eventItemStyle} style={{ position: 'relative', flexGrow: 5, background: '#f5f5f51f' }}>
                                <div style={{ width: '100%', background: '#eeeeee3e', padding: 5 }}>Details</div>
                                <small className={itemChildStyle} style={{ position: 'relative' }}>Start Date & TIme</small>
                                <small className={itemChildStyle} style={{ position: 'relative' }}>End Date & TIme</small>
                                <small className={itemChildStyle} style={{ position: 'relative' }}>Location</small>
                            </div> :
                            <div className={eventItemStyle} style={{ position: 'relative', flexGrow: 3, padding: 20 }}>
                                {headers[1]}
                            </div>
                }

                <div className={eventItemStyle} style={{ flexGrow: 2.5 }}>{headers[this.props.type == 'My Appointments' ? 3 : 2]}</div>
            </div>
        )

        const type = this.props.type.split(' ')
        const histories = this.props.histories === null ? <h3 style={{ textAlign: 'center', padding: '20px', textTransform: 'uppercase', color: '#ccc' }}>NO {type[1]} HISTORY</h3> : this.renderHistories()

        return (
            <div className={[null, this.state.readyToView ? ready : null, historyWrapper].join(' ')}>
                <label htmlFor="" className={historyHeader}>
                    <span style={{ fontWeight: '600' }}>{this.props.type}</span> <small>history</small>
                </label>

                {header}

                <div className={[this.state.readyToView ? readyList : null, historyListWrapper].join(' ')}>
                    <ul className={[histories.length > 7 ? putShadow : null, historyList, 'history-list'].join(' ')}>
                        {
                            this.state.readyToView ?
                                this.props.type == 'My Subscriptions' ? null : <ReactTooltip /> : null
                        }
                        {histories}
                    </ul>
                </div>
            </div>
        )
    }
}

let historyList = css({
    transition: '300ms ease',
    maxHeight: '45vh',
    overflowY: 'auto',
    background: 'white',
    '> li:last-child': {
        marginBottom: '20px'
    },
    '> li:nth-child(1)': {
        // marginTop: '20px'
    },
    '> li:nth-child(odd)': {
        background: '#eeeeee2e'
    },
    '> li:nth-child(odd):hover': {
        background: '#eeeeee9e'
    }
})


let putShadow = css({
    boxShadow: 'inset 0 15px 20px -5px rgba(0,0,0,.2)',
})

let animateHeight = css.keyframes({
    '0%': { maxHeight: '100px' },
    '100%': { maxHeight: '504px' },
})


let animateOpacity = css.keyframes({
    '0%': { opacity: '0' },
    '100%': { opacity: '1' },
})

let readyList = css({
    // animation: `${animateOpacity} .5s ease .2s forwards`
})


let ready = css({
    // animation: `${animateHeight} .5s ease forwards`
})

let historyWrapper = css({
    marginBottom: '20px',
    padding: '10px 0px 0px',
    background: 'white',
    boxShadow: '0 8px 15px rgba(0,0,0,.05)',
    overflow: 'hidden',
    height: '100%',
    maxHeight: '504px',
    transition: 'all 1s ease-in-out',
    background: '#364563'
})

let historyListWrapper = css({
    // transition: '500ms ease',
    // transform: 'translateY(-10px)',
    opacity: '1'
})

let border = css({
    position: 'absolute',
    top: '0',
    width: '1px',
    background: '#eee'
})

let historyHeader = css({
    padding: '5px 15px',
    marginBottom: '10px',
    marginLeft: '10px',
    display: 'inline-block',
    background: '#80cde9',
    color: 'white',
    textTransform: 'uppercase'
})

let itemHeadWrapper = css({
    position: 'relative',
    display: 'flex',
    flex: '1 0 100%',
    flexWrap: 'wrap',
    fontSize: 'normal',
    textTransform: 'uppercase',
    alignItems: 'center',
    color: '#52617f',
    // borderBottom: '1px solid #364563',
    // paddingRight: '20px',
    '> div': {
        flex: '1',
    }
})

let historyItem = css({
    position: 'relative',
    display: 'flex',
    flex: '1 0 100%',
    flexWrap: 'wrap',
    fontSize: 'small',
    alignItems: 'center',
    padding: '5px 0px'
})

let eventItemStyle = css({
    justifyContent: 'center',
    textAlign: 'center',
    alignItems: 'stretch',
    display: 'flex',
    flex: '1 0 100%',
    flexWrap: 'wrap',
    height: '100%',
    // color: '#364563',
    color: 'white',
    // borderRight: '1px dotted gray',
    ':last-child': { border: 'none' }
})

let itemChildStyle = css({
    flex: 1,
    padding: 5
})
