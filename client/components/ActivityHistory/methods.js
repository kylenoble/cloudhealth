import moment from 'moment-timezone'
import { css } from 'glamor'

const Status = props => {
  const status = props.status
  const color = props.color
  return (
    <span
      style={{
        color: color,
        padding: '5px 10px',
        width: 140,
        display: 'block',
        margin: '0 auto',
        fontWeight: 600,
        textAlign: 'center'
      }}>{status}</span>
  )
}

// autoresize the tooltip description to match content
export const _autoresize = () => {
  setTimeout(() => {
    const textarea = document.querySelector('#description-wrapper')
    textarea.style.height = `${textarea.scrollHeight}px`
  }, 10);
}

export const SubscriptionIcon = props => {
  const src = props.src || 'exercise'
  const type = props.type || 'Basic'
  const premiumColor = '#e9554b'
  const basicColor = '#37b492'
  const premium = '1px solid #e9554b'
  const basic = '1px solid #37b492'

  return (
    <div>
      <figure
        style={{ border: type == 'Basic' ? basic : type == 'Run' ? basic : type == 'Trial' ? basic : premium, marginRight: 20 }}
        className={iconWrapper} >
        <img src={`/static/icons/${src}.svg`} alt="" />

        <figcaption
          style={type == 'Basic' ? { background: basicColor } :
            type == 'Run' ? { background: basicColor } : type == 'Trial' ? { background: basicColor } : { background: premiumColor }} >
          {type == 'Run' ? 'Basic' : type == 'Trial' ? 'Basic' : type}
        </figcaption>
      </figure >

      {props.name}
    </div>
  )
}

// tooltip content and style
export const tooltipWrapper = (history, type) => {
  let host = ''
  let host_email = ''
  if (history.host) {
    host = history.host.host.name
    host_email = history.host.host.email
  }

  const description = history.description

  const events_html = `
        <div style="max-width: 400px; border-top-left-radius: 3px; border-top-right-radius: 3px; overflow: hidden; text-align: left;">
        <small style="padding: 5px; background: #364563; display: block; text-align:center; color: white">${type == 'My Appointments' ? 'Health Concern' : 'Event Description'}</small>
        <p style="padding: 5px 15px;"><span style="color: #364563">Host:</span> <strong>${host}</strong><br/><label style="color: #999">${host_email}</label></p>
        <p style="padding: 5px 15px;"><span style="color: #364563">Venue:</span> <strong>${history.venue}</strong></p>
        <p style="padding: 5px 15px;">
        <span style="color: #364563">Description:</span>
        <textarea id="description-wrapper" style="width: 100%; resize: none; overflow: hidden; border: none; font-weight: 600">${description}</textarea>
        </p>
      </div>`

  const html = `
      <div style="max-width: 400px; border-top-left-radius: 3px; border-top-right-radius: 3px; overflow: hidden">
        <small style="padding: 5px; background: #364563; display: block; text-align:center; color: white">${type == 'My Appointments' ? 'Health Concern' : 'Event Description'}</small>
        <p style="padding: 15px;"> ${description}</p>
      </div>`

  if (type === 'My Events') {
    return events_html
  } else {
    return html
  }
}

// returns subscription icon source name
export const showIcon = (name) => {
  switch (name) {
    case 'Movement Program - Basic':
      return 'exercise'

    case 'Detoxification Program - Premium':
      return 'health'

    case 'Weight Management Program - Basic':
      return 'body'

    case 'Movement Program - Premium':
      return 'exercise'

    case 'Nutrition Program - Basic':
      return 'nutrition'

    case 'Sleep Management Program - Premium':
      return 'sleep'

    case 'Detoxification Program - Basic':
      return 'health'

    case 'Sleep Management Program - Basic':
      return 'sleep'

    case 'Weight Management Program - Premium':
      return 'body'

    case 'Nutrition Program - Premium':
      return 'nutrition'

    case 'Stress Management Program - Premium':
      return 'sad-stress'

    case 'Stress Management Program - Basic':
      return 'sad-stress'

    case 'Telemedicine':
      return 'telemed'

    case 'CloudHealthAsia Corporate Health Index - Trial':
      return 'stressManagementProgram'

    case 'CloudHealthAsia Corporate Health Index - Run':
      return 'WeightMaintenanceProgram'

    default:
      break;
  }
}

// return status style
export const _checkStatus = (args) => {
  const bBlue = 'rgba(128, 205, 233, .3)'
  const bOrange = 'rgba(255, 165, 0, .3)'
  const bRed = 'rgba(255, 0, 0,.3)'
  const bGray = 'rgba(150, 150, 150, .3)'
  const blue = 'rgb(128, 205, 233)'
  const orange = 'rgb(255, 165, 0)'
  const red = 'rgb(255, 0, 0)'
  const gray = 'rgb(150, 150, 150)'

  if (args.active) {
    switch (args.status) {
      case 3:
        return <Status status="Cancelled" color={red} bColor={bRed} />
      case 2:
        return <Status status="Rescheduled" color={orange} bColor={bOrange} />
      case null:
        return <Status status="Accepted" color={blue} bColor={bBlue} />
      case 'Approved':
        return <Status status="Approved" color={blue} bColor={bBlue} />
      case 'Pending':
        return <Status status="Pending" color={orange} bColor={bOrange} />
      case 'Declined':
        return <Status status="Declined" color={red} bColor={bRed} />
      case 'Active':
        return <Status status="Active" color={orange} bColor={bOrange} />
      case 'Completed':
        return <Status status="Completed" color={blue} bColor={bBlue} />
      case 'Deleted':
        return <Status status="Deleted" color={gray} bColor={bGray} />
      case 'Cancelled':
        return <Status status="Cancelled" color={red} bColor={bRed} />
      case 'Ongoing':
        return <Status status="Ongoing" color={blue} bColor={bBlue} />
      default:
        break;
    }
  } else {
    switch (args.status) {
      case 3:
        return <Status status="Cancelled" color={red} bColor={bRed} />
      case 2:
        return <Status status="Rescheduled" color={orange} bColor={bOrange} />
      case null:
        return <Status status="Pending Approval" color={gray} bColor={bGray} />
      case 'Approved':
        return <Status status="Approved" color={blue} bColor={bBlue} />
      case 'Pending':
        return <Status status="Pending" color={orange} bColor={bOrange} />
      case 'Declined':
        return <Status status="Declined" color={red} bColor={bRed} />
      case 'Active':
        return <Status status="Active" color={orange} bColor={bOrange} />
      case 'Completed':
        return <Status status="Completed" color={blue} bColor={bBlue} />
      case 'Joined':
        return <Status status="Joined" color={blue} bColor={bBlue} />
      case 'Deleted':
        return <Status status="Deleted" color={gray} bColor={bGray} />
      case 'Cancelled':
        return <Status status="Cancelled" color={red} bColor={bRed} />
      case 'Ongoing':
        return <Status status="Ongoing" color={blue} bColor={bBlue} />
      default:
        break;
    }
  }
}

// format accessor
export const _accessor = (accessor) => {
  let acc = ''
  const a = accessor.toLowerCase().split(' ')

  for (const ac of a) {
    acc += `${ac}`
  }
  return acc.trim()
}

// convert time to hh:mm AM/PM
const _convertTime = (time) => {
  let t = time.split('+')[0]
  // Check correct time format and split into components
  t = t.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [t];

  if (t.length > 1) { // If time format correct
    t = t.slice(1);  // Remove full string match value
    t[5] = + t[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
    t[0] = + t[0] % 12 || 12; // Adjust hours
  }
  return t.join(''); // return adjusted time or original string
}

// convert date and time to MMMM DD YYYY + hh:mm AM/PM
export const _dates = (type, history, on) => {
  if (type === 'My Appointments') {
    return [moment(history.datetime).format('MMMM DD YYYY'), moment(history.datetime).format('hh:mm A')]

  } else if (type === 'My Events' && on === 'start') {
    return [moment(history.start_date).format('MMMM DD YYYY'), _convertTime(history.start_time)]

  } else if (type === 'My Events' && on === 'end') {
    return [moment(history.end_date).format('MMMM DD YYYY'), _convertTime(history.end_time)]

  } else if (type === 'My Subscriptions' && on === 'created') {
    return [moment(history.date).format('MMMM DD YYYY'), moment(history.date).format('hh:mm A')]

  } else if (type === 'My Subscriptions' && on === 'approved') {
    return [moment(history.date_approved).format('MMMM DD YYYY'), moment(history.date_approved).format('hh:mm A')]
  }
}

// Styles
let iconWrapper = css({
  minWidth: 40,
  height: 45,
  margin: 0,
  display: 'inline-block',
  position: 'relative',
  padding: 5,
  borderRadius: 5,
  verticalAlign: 'middle',
  overflow: 'hidden',
  '> img': {
    width: '35%',
    position: 'absolute',
    top: '5%',
    left: '50%',
    transform: 'translateX(-50%)'
  },
  '> figcaption': {
    position: 'absolute',
    bottom: 0,
    left: 0,
    background: '#80cde9',
    fontSize: 7,
    width: '100%',
    color: 'white',
    textAlign: 'center'
  }
})
