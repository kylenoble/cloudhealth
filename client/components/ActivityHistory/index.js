import HistoryWrapper from './historyWrapper'
import ActivityHistoryService from '../../utils/activityHistoryService'
import moment from 'moment-timezone'
const activityHistory = new ActivityHistoryService()
import { css } from 'glamor'
import ReactTooltip from 'react-tooltip'

import Table from './table'
import GridContainer from '../NewSkin/Wrappers/GridContainer';
import { Colors } from '../NewSkin/Styles';
import Card from '../NewSkin/Wrappers/Card';
import Restricted from "../../components/restricted";
import { Preparing } from "../NewSkin/Components/Loading";

const APPOINTMENTS_HEADERS = ['Doctor', 'Booked Appointment', 'Requested', 'Status']
const EVENTS_HEADERS = ['Event', 'Details', 'Host']
const SUBSCRIPTIONS_HEADERS = ['Subscription', 'Date', 'Status']

export default class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      events: [],
      appointments: [],
      subscriptions: [],
      readEvents: [],
      doctors: [],
      currentView: 'MyAppointments',
      activeHistory: '',
      headers: '',
      type: '',
      historiesReady: false,
      subscriptionPrograms: '',
      subscriptionsByCompany: '',
      subscriptionProgramsEnrolled: '',
      recentTab: '',
      showTooltip: true,
      showContent: false
    }
  }

  componentWillMount() {
    this.getEvents()
    this.getSubscriptionPrograms()
    this.getSubscriptions()
    this.getAppointments()

  }

  componentDidMount() {
    setTimeout(() => {
      this.updateView(this.state.appointments, APPOINTMENTS_HEADERS, 'My Appointments')
    }, 1000);
  }

  getAppointments = () => {
    let userID = this.props.user.id
    activityHistory
      .getAppointments(userID)
      .then(res => {
        if (res) {
          this.setState({
            appointments: res
          })
          setTimeout(() => {
            this.setState({ activeHistory: res })
          }, 1)
          this.getDoctorsDetails()
        }
      })
      .catch(e => {
        console.log(e)
      })
  }

  // get subscriptions programs
  getSubscriptionPrograms = () => {
    activityHistory
      .getSubscriptionPrograms()
      .then(res => {
        if (res.length >= 1) {
          this.setState((prevState, props) => ({
            subscriptionPrograms: res
          }))
        } else {
        }
      })
      .catch(e => {
        console.log(e)
      })
  }

  // get subscriptions by employee
  getSubscriptions = () => {
    let userID = this.props.user.id
    activityHistory
      .getSubscriptions(userID)
      .then(res => {
        if (res.length > 0) {
          this.setState({ subscriptions: res })
          res.forEach(result => {
            this.getSubscriptionsByCompany(result.company_enrollment_id) // get subscriptions by company
          })
        }
        this.setState({
          showContent: true
        })
      })
      .catch(e => {
        console.log(e)
      })
  }

  // get subscriptions by company
  getSubscriptionsByCompany = (companyEnrollmentId) => {
    activityHistory
      .getSubscriptionsByCompany(companyEnrollmentId)
      .then(res => {
        if (res.length > 0) {
          this.setState({
            subscriptionsByCompany: [...this.state.subscriptionsByCompany, res[0]]
          })
        }
        setTimeout(() => {
          this.getSubscriptionProgramsOfCompany()
        }, 100);
      })
      .catch(e => {
        console.log(e)
      })
  }

  getSubscriptionProgramsOfCompany = () => {
    let companyEnrolledPrograms = [...this.state.subscriptionsByCompany]
    let programs = [...this.state.subscriptionPrograms]
    let patientSubscriptions = [...this.state.subscriptions]
    let subscriptionProgramsEnrolled = []

    const completed_programs = companyEnrolledPrograms.filter(cep => cep.status === 'Completed') // completed programs by company

    completed_programs.forEach((cp, i) => { // cp = subscription_company_enrollment
      let programCodeByCompany = cp['program_code'];
      let pcode = programs.find(program => program['shortcode'] == programCodeByCompany)

      const completed_programs_enrolled = patientSubscriptions.filter(ps => ps.company_enrollment_id === cp.id)

      completed_programs_enrolled.forEach(cpe => { // cpe = subscription_employee_enrollment
        // see = subscription_employee_enrollment
        // sce = subscription_company_enrollment
        let subscription = {
          name: pcode ? pcode.subscription_name : null,
          date: this.convertDate(cpe.date_enrolled), // see
          enrollment_status: cpe.enrollment_status, // see
          status: cp.status, //sce
          date_approved: this.convertDate(cpe.date_approved), // see
          date_declined: this.convertDate(cpe.date_declined), // see
          start_date: this.convertDate(cp.subscription_start), // sce
          end_date: this.convertDate(cp.subscription_end), // sce
          date_created: this.convertDate(cp.subscription_end), // sce
        }

        subscriptionProgramsEnrolled.push(subscription)
      })

    })
    this.setState({ subscriptionProgramsEnrolled })
  }

  getEvents = () => {
    let compId = this.props.user.company_id
    activityHistory
      .getEvents(compId, this.props.user.id)
      .then(res => {
        if (res) {
          this.setState({
            events: res
          })
          setTimeout(() => {
            this.getReadEvents(res)
          }, 100)
        }
      })
      .catch(e => {
        console.log(e)
      })
  }

  getReadEvents = (events) => {
    let readEvents = []
    let userID = this.props.user.id
    var date = new Date()
    let current_date = moment(date).format('YYYY-MM-DD')
    events.map(event => {
      var event_end_date = moment(event.end_date).format('YYYY-MM-DD');
      if (event.company_enrollment_id === null) {
        // var status = 'Pending';
        // if (event.read_by) {
        //   event.read_by.data.filter(r => {
        //     if (r.id == userID && r.status != null) {
        //       if (r.status == 2) {
        //         status = 'Joined';
        //       } else {
        //         status = 'Declined';
        //       }
        //     }
        //   })
        // }
        // if (status !== 'Declined' && event_end_date < current_date || status === 'Declined') {
        //   event.status = status;
        //   const { created_by, ...newEvent } = event
        //   const host = this.getEventHost(created_by)
        //   const updatedEvent = { ...newEvent, host }
        //   readEvents.push(updatedEvent)
        // }
      } else {
        event.status = event.attendance_status;
        //check if user has approved subscription enrollment
        if (event.attendance_status !== null && event_end_date < current_date) {
          const { created_by, ...newEvent } = event
          const host = this.getEventHost(created_by)
          const updatedEvent = { ...newEvent, host }
          readEvents.push(updatedEvent)
        }
      }
    })
    this.setState({ readEvents })
  }

  getEventHost = hostID => {
    let getHost = { host: {} }
    activityHistory
      .getHost(hostID)
      .then(res => {
        getHost.host.name = (res[0]['name']).trim()
        getHost.host.email = (res[0]['email']).trim()
      })
      .catch(err => {
        console.log(err);
      })

    return getHost
  }

  getDoctors = doctor_id => {
    let get = activityHistory
      .getDoctor(doctor_id)
      .then(res => {
        if (res) {
          return res[0]
        }
      })
      .catch(e => {
        console.log(e)
      })

    return get
  }

  getDoctorsDetails = () => {
    let appointments = this.state.appointments
    let newAppointments = []
    //get doctor details and remove futuredated appointments
    for (let i = 0; i < appointments.length; i++) {
      const appointment = appointments[i]
      if (moment(appointment.datetime).format('MM/DD/YYYY') < moment().format('MM/DD/YYYY')) {
        const doctor = this.getDoctors(appointment.doctor_id)
        doctor.then(res => {
          appointment.doctor_name = res.name
          appointment.doctor_comp = res.company
        })
        newAppointments.push(appointment)
      }
    }

    setTimeout(() => {
      this.setState({ appointments: newAppointments })
    }, 100)
  }

  convertDate = date => {
    if (date === null) {
      return null
    } else {
      let newDate = date.split(' ')
      return moment(newDate[0]).format('MMMM DD YYYY')
    }
  }

  onTabClick = e => {
    let tab = e.target

    if (tab.id !== this.state.recentTab) {
      this.setState(() => ({
        historiesReady: false
      }))

      let lis = document.querySelectorAll(`.${selector}`);

      lis.forEach(li => {
        li.classList.remove(selected)
      })

      tab.classList.add(selected)

      const tabID = tab.id.replace(/ /g, '')

      let type = tab.id
      let headers = ''
      let history = ''

      switch (tabID) {
        case 'MyAppointments':
          headers = APPOINTMENTS_HEADERS
          history = this.state.appointments
          break;
        case 'MyEvents':
          headers = EVENTS_HEADERS
          history = this.state.readEvents
          break;

        case 'MySubscriptions':
          headers = SUBSCRIPTIONS_HEADERS
          history = this.state.subscriptionProgramsEnrolled
          break;

        default:
          break;
      }

      this.updateView(history, headers, type)
    }

  }

  updateView = (history, headers, type) => {
    this.setState({
      showTooltip: !this.state.showTooltip,
    })
    const tabID = type.replace(/ /g, '')
    this.setState((prevState, props) => ({
      historiesReady: !prevState.historiesReady,
      activeHistory: prevState.activeHistory = history,
      currentView: prevState.currentView = tabID,
      headers: prevState.headers = headers,
      type: prevState.type = type,
      recentTab: prevState.recentTab = type
    }))

    setTimeout(() => {
      this.setState({
        showTooltip: !this.state.showTooltip,
      })
    }, 1);
  }


  tabNav = () => {
    let titles = ['My Events', 'My Subscriptions']
    let horizontalTabNavigation = (
      <GridContainer columnSize={'1fr 1fr 1fr'} styles={{ borderBottom: `2px solid ${Colors.pink}`, width: '100%', marginBottom: 20 }}>
        <div className={[selector, selected, 'tabdivst'].join(' ')} id="My Appointments" onClick={this.onTabClick}>My Appointments</div>
        {titles.map((title, i) => (
          <div key={i} className={selector} id={title} onClick={this.onTabClick}>
            {title}
          </div>
        ))}
      </GridContainer>
    )

    return horizontalTabNavigation
  }

  render() {
    if(!this.state.historiesReady){
      return <Preparing styles={{margin: 'auto'}}/>
    }
    if(this.state.showContent && this.state.subscriptionsByCompany.length == 0){
      let restrict_msg = `This feature is not included in your current subscription. Please contact the officer in-charge of your account if you wish to subscribe`;
      return (
        <Card>
          <Restricted message={restrict_msg} />
        </Card>
      )
    }
    return (
      <Card>
        {this.tabNav()}
        {
          this.state.historiesReady &&
            <Table ready={this.state.historiesReady} type={this.state.type} histories={this.state.activeHistory}></Table>
        }
        {this.state.showTooltip ? this.state.type !== 'My Subscriptions' ? <ReactTooltip /> : null : null}
      </Card>
    )
  }
}


const selector = css({
  padding: '10',
  borderBottom: '5px solid white',
  transition: '250ms ease',
  cursor: 'pointer',
  fontWeight: 500,
  color: Colors.blueDarkAccent
});

const selected = css({
  borderBottomColor: `${Colors.pink}`,
  color: `${Colors.pink}`
})

let loadingWrapper = css({
  textAlign: 'center',
  width: '100%',
  padding: '30px 0',
  '> img': {
    display: 'inline-block',
    opacity: '.1'
  }
})