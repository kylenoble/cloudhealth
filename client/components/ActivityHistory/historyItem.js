import { css } from 'glamor'
import ReactTooltip from 'react-tooltip'
import moment from 'moment-timezone'


const SubscriptionIcon = (props) => {
    const premiumColor = '#e9554b'
    const basicColor = '#37b492'
    const premium = '1px solid #e9554b'
    const basic = '1px solid #37b492'

    return (
        <figure
            style={props.type == 'Basic' ? { border: basic } :
                props.type == 'Run' ? { border: basic } : props.type == 'Trial' ? { border: basic } : {
                    border: premium
                }}
            className={iconWrapper} >
            <img src={`/static/icons/${props.src}.svg`} alt="" />

            <figcaption
                style={props.type == 'Basic' ? { background: basicColor } :
                    props.type == 'Run' ? { background: basicColor } : props.type == 'Trial' ? { background: basicColor } : { background: premiumColor }} >
                {props.type == 'Run' ? 'Basic' : props.type == 'Trial' ? 'Basic' : props.type}
            </figcaption>
        </figure >
    )
}

export default class HistoryItem extends React.Component {
    constructor(props) {
        super(props)
        this.state = {}
    }

    showIcon = (name) => {
        switch (name) {
            case 'Movement Program - Basic':
                return 'exercise'

            case 'Detoxification Program - Premium':
                return 'health'

            case 'Weight Management Program - Basic':
                return 'body'

            case 'Movement Program - Premium':
                return 'exercise'

            case 'Nutrition Program - Basic':
                return 'nutrition'

            case 'Sleep Management Program - Premium':
                return 'sleep'

            case 'Detoxification Program - Basic':
                return 'health'

            case 'Sleep Management Program - Basic':
                return 'sleep'

            case 'Weight Management Program - Premium':
                return 'body'

            case 'Nutrition Program - Premium':
                return 'nutrition'

            case 'Stress Management Program - Premium':
                return 'sad-stress'

            case 'Stress Management Program - Basic':
                return 'sad-stress'

            case 'CloudHealthAsia Corporate Health Index - Trial':
                return 'stressManagementProgram'

            case 'CloudHealthAsia Corporate Health Index - Run':
                return 'WeightMaintenanceProgram'

            default:
                break;
        }
    }

    convertTime = (time) => {
        let t = time.split('+')[0]
        // Check correct time format and split into components
        t = t.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [t];

        if (t.length > 1) { // If time format correct
            t = t.slice(1);  // Remove full string match value
            t[5] = + t[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
            t[0] = + t[0] % 12 || 12; // Adjust hours
        }
        return t.join(''); // return adjusted time or original string
    }

    render() {
        let history = this.props.historyDetails
        let description = history.desc != '' ? history.desc : `<span style="color: #ccc">No Description</span>`
        const type = history.name.split('-')[1]
        const tooltipWrapper = `
            <div style="max-width: 400px; border-top-left-radius: 3px; border-top-right-radius: 3px; overflow: hidden">
                <small style="padding: 5px; background: #364563; display: block; text-align:center; color: white">${this.props.type == 'My Appointments' ? 'Health Concern' : 'Event Description'}</small>
                <p style="padding: 15px;"> ${description}</p>
            </div>`


        return (

            <li data-tip={tooltipWrapper} data-type="light" data-html={true} className={historyItem} style={this.props.type == 'My Subscriptions' ? { cursor: 'default' } : { cursor: 'help' }}>

                {/* DOCTOR/EVENT NAME/ SUBSCRIPTION NAME*/}
                <div className={eventItemStyle} style={this.props.type == 'My Subscriptions' ? { flexGrow: 3 } : { flexGrow: 2.5 }}>
                    <div style={this.props.type === 'My Appointments' ? { textAlign: 'center', width: '100%' } : { textAlign: 'left', paddingLeft: 40, width: '100%' }}>
                        {
                            this.props.type === 'My Subscriptions' ? <SubscriptionIcon desc={history.desc} src={this.showIcon(history.name)} type={type.trim()} /> : null
                        }
                        <label className={historyName}> {history.name}</label>
                    </div>
                </div>

                {/* DATE */}
                {
                    this.props.type == 'My Appointments' ?
                        <div className={eventItemStyle} style={{ position: 'relative', flexGrow: 5 }}>
                            <div className={itemChildStyle} style={{ textAlign: 'center' }}>
                                {moment(history.date_requested).format('MMMM DD YYYY')}
                                <span className={time} style={{ marginLeft: 10 }}>
                                    {moment(history.date_requested).format('hh:mm A')}
                                </span>

                            </div>
                            <div className={itemChildStyle} style={{ textAlign: 'center' }}>
                                {moment(history.date).format('MMMM DD YYYY')}
                                <span className={time} style={{ marginLeft: 10 }}>
                                    {moment(history.date).format('hh:mm A')}
                                </span>
                            </div>
                        </div>

                        :
                        this.props.type == 'My Events' ?
                            <div className={eventItemStyle} style={{ textAlign: 'center', flexGrow: 5 }}>
                                <div className={itemChildStyle} style={{ textAlign: 'center' }}>
                                    {moment(history.startDate).format('MMMM DD YYYY')}
                                    <span className={time} style={{ display: 'block' }}>
                                        {this.convertTime(history.startTime)}
                                    </span>
                                </div>

                                <div className={itemChildStyle} style={{ textAlign: 'center' }}>
                                    {moment(history.endDate).format('MMMM DD YYYY')}
                                    <span className={time} style={{ display: 'block' }}>
                                        {this.convertTime(history.endTime)}
                                    </span>
                                </div>

                                <div className={itemChildStyle} style={{ display: 'flex' }}>
                                    <span>{history.venue}</span>
                                </div>
                            </div>
                            :
                            <div className={eventItemStyle} style={{ textAlign: 'center', flexGrow: 3 }} >
                                {history.date}
                            </div>
                }

                {/* STATUS */}
                <div className={eventItemStyle} style={{ flexGrow: 2.5 }}>

                    {
                        this.props.type == 'My Events' ?
                            history.host.name : history.status

                    }
                </div>
            </li>
        )
    }
}

let time = css({
    color: '#969696',
})

let tooltipStyle = css({
    padding: '0px !important',
    backgroundColor: 'white !important',
    textAlign: 'left',
    color: 'black !important',
    boxShadow: '0 6px 20px rgba(0,0,0,.05)',
    '::after': {
        borderTopColor: 'white !important',
    }
})

let historyName = css({
    display: 'inline-block',
    verticalAlign: 'middle',
    marginLeft: 10,
    maxWidth: '80%'
})

let iconWrapper = css({
    minWidth: 40,
    height: 40,
    margin: 0,
    display: 'inline-block',
    position: 'relative',
    padding: 5,
    borderRadius: 5,
    verticalAlign: 'middle',
    overflow: 'hidden',
    '> img': {
        width: '35%',
        position: 'absolute',
        top: '5%',
        left: '50%',
        transform: 'translateX(-50%)'
    },
    '> figcaption': {
        position: 'absolute',
        bottom: 0,
        left: 0,
        background: '#80cde9',
        fontSize: 7,
        width: '100%',
        color: 'white',
        textAlign: 'center'
    }
})

let historyItem = css({
    position: 'relative',
    display: 'flex',
    flex: '1 0 100%',
    flexWrap: 'wrap',
    fontSize: 'small',
    alignItems: 'center',
    padding: '5px 0px',
    transition: '300ms ease',
    opacity: '.8',
    ':hover': {
        background: '#eeeeee9e',
        opacity: '1',
    },
    '> div': {
        flex: '1',
    }
})

let eventItemStyle = css({
    textAlign: 'center',
    alignItems: 'stretch',
    justifyContent: 'center',
    display: 'flex',
    flex: '1 0 100%',
    padding: 5,
    height: '100%',
    ':last-child': { border: 'none' }
})

let itemChildStyle = css({
    flex: 1,
    padding: 5,
    boxSizing: 'border-box',
    alignItems: 'center',
    justifyContent: 'center'
})