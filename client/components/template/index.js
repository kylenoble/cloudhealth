/* eslint-disable no-mixed-operators */
/* eslint-disable radix */
/* eslint-disable no-undef */
import React, { Component } from "react";
import moment from "moment-timezone";
import Router, { withRouter } from "next/router";
import { css } from "glamor";

import withAuth from "../../utils/withAuth.js";
import { confirmAlert } from "react-confirm-alert"; // Import
import Header from "../Header";
//import Menu from "../Menu";
import stylesheet from "../../style/globalStyle.scss";
import UserService from "../../utils/userService.js";
import AppointmentService from "../../utils/appointmentService.js";
import { UserContext } from "../../context/UserContext";
import {
  Notifications,
  GetPersonalInfo
} from "../NewSkin/Services/Notifications";
import Loading from "../NewSkin/Components/Loading";
import NewFooter from "../NewSkin/Components/NewFooter";
import ContainedWrapper from "../NewSkin/Wrappers/ContainedWrapper";
import {
  headerAndFooterHeight,
  checkScreenResolution
} from "../NewSkin/Methods";
import Button from "../NewSkin/Components/Button";
import Colors from "../NewSkin/Colors";
import ModalWrapper, {
  ModalFooter,
  ModalContent,
  ModalContentWrapper,
  ModalHeader
} from "../NewSkin/Wrappers/Modal";
import { fontWeight, font } from "../NewSkin/Styles";
import { showAlert } from "../NewSkin/Components/Alert";

import Snack from "./snackbar";

const pInfo = new UserService();
const Appointment = new AppointmentService();
const DURATION = 30; // in mins default is 30 mns
const CHECK_INTERVAL = 2000; // in ms
const STORE_KEY = "lastAction";
const PASSWORD_AGE = 30; // in days

class Template extends Component {
  static contextType = UserContext;

  constructor(props) {
    super(props);
    this._isMounted = false;
    this.auth = props.auth;
    this.user = this.auth.getProfile();
    this.logout = this.logout.bind(this);
    this._openModal = this._openModal.bind(this);
    this._closeModal = this._closeModal.bind(this);

    this.state = {
      openSnackbar: false,
      loading: true,
      notifications: [],
      appointmentList: [],
      notif_personal_info: [],
      pageLoaded: false,
      menuModalsState: {
        messages: false,
        notification: false,
        avatar: false
      },
      changePasswordMessage: "",
      defaultPageHeight: 0,
      screenSize: "xlarge",
      optionToRetain: false,
      timeOutModalIsOpen: false,
      timeleft: DURATION,
      needToChangePassword: false
    };
  }

  componentDidMount() {
    this._isMounted = true;

    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      flexibility(document.getElementById("__next"));
    }

    this.setLastAction(moment());
    this.check();
    this.initListener();

    const intervalId = setInterval(() => {
      this.check();
    }, CHECK_INTERVAL);

    if (this._isMounted) {
      document.body.addEventListener("resize", this.resizing());
      document.body.onresize = this.resizing;
      this.setState({ intervalId });
    }

    localStorage.removeItem("isAuth");

    if (this.auth.loggedIn()) {
      const defaultPageHeight = headerAndFooterHeight();

      if (this.user.type !== "Patient") {
        this._passwordMonitor();
      } else {
        if (this.user.status !== null) {
          this._passwordMonitor();
        }
      }
      setTimeout(() => {
        this.setState({ pageLoaded: true, defaultPageHeight });
      }, 500);
      this._checkUserStatus();
    }
  }

  resizing = () => {
    const screensize = checkScreenResolution();
    this.setState({ screenSize: screensize });
  };

  componentWillUnmount() {
    // use intervalId from the state to clear the interval
    this._isMounted = false;
    document.body.removeEventListener("resize", this.resizing);
    clearInterval(this.state.intervalId);
  }

  async _fetchQueuedPatients() {
    let qry = { active: true };
    const response = Appointment.getAppointmentsList(qry);
    const data = await response;
    if (this._isMounted) {
      var reduced = data.reduce(function(filtered, item) {
        var remainingTime = moment(item.datetime).diff(moment(), "minutes"),
          remainingHours = moment(item.datetime).diff(moment(), "hours"),
          remainingDays = moment(item.datetime).diff(moment(), "days"),
          startOfWeek = moment()
            .startOf("week")
            .add("7", "days"),
          endOfWeek = moment()
            .endOf("week")
            .add("7", "days");

        if (
          remainingTime > -60 &&
          moment(item.datetime).isSameOrBefore(endOfWeek)
        ) {
          //filter only upcoming/ongoing appointments up to next week
          var isNextWk = false;
          if (
            moment(item.datetime).format("MMMM DD YYYY") !==
              moment().format("MMMM DD YYYY") &&
            moment(item.datetime).isBetween(startOfWeek, endOfWeek)
          ) {
            isNextWk = true;
          }
          filtered.push({
            remainingTime: remainingTime,
            remainingHours: remainingHours,
            remainingDays: remainingDays,
            datetime: item.datetime,
            isNextWk: isNextWk
          });
        }
        return filtered;
      }, []);
      this.setState({ appointmentList: reduced, loading: false });
    }
  }

  setLastAction = lastAction => {
    localStorage.setItem(STORE_KEY, lastAction);
  };

  getLastAction = () => localStorage.getItem(STORE_KEY);

  resetTimer = () => {
    this.setLastAction(moment());
  };

  check = () => {
    if (this.auth.loggedIn()) {
      const now = moment();
      const timeleft = moment(this.getLastAction());
      timeleft.add(DURATION, "minutes");
      const diff = timeleft.diff(now, "seconds");
      const isTimeout = diff < 0;

      if (this._isMounted) {
        this.setState({ timeleft: diff });
      }

      if (isTimeout) {
        // _logout PARAMS :  action, redirectTo = null, message = null, allertMessage = null
        this._logout("Session timeout");
        if (this._isMounted) {
          this.setState({
            timeOutModalIsOpen: true,
            needToChangePassword: false
          });
        }
      } else {
        if (this.user.type !== "Patient") {
          this._getNotifications();
          this._fetchQueuedPatients();
        } else {
          if (
            this.user.status !== null &&
            this.state.needToChangePassword === false
          ) {
            this._getNotifications();
            if (this.user.telemedicine_enabled) {
              this._fetchQueuedPatients();
            }
          }
        }
        if (this._isMounted) {
          this.setState({
            loading: false
          });
        }
      }
    } else {
      if (this._isMounted) {
        this.setState({
          needToChangePassword: false
        });
      }
    }
  };

  initListener() {
    if (!this.auth.loggedIn()) return;
    document.body.addEventListener("click", () => this.resetTimer());
    document.body.addEventListener("mouseover", () => this.resetTimer());
    document.body.addEventListener("mouseout", () => this.resetTimer());
    document.body.addEventListener("keydown", () => this.resetTimer());
    document.body.addEventListener("keyup", () => this.resetTimer());
    document.body.addEventListener("keypress", () => this.resetTimer());
  }

  _getNotifications = () => {
    const notifs = Notifications(this.user);
    notifs
      .then(res => {
        if (this._isMounted) {
          this.setState({
            notifications: res
          });
        }
      })
      .catch(err => console.log(err));
  };

  _checkUserStatus() {
    if (this.user.type === "Patient") {
      if (!this.user.is_active) {
        this._inactiveCompany();
        return;
      }
      if (this._isMounted) {
        this.setState({
          waiverModalIsOpen: this.user.status === null,
          welcomeModalIsOpen: this.user.status === null
        });
        if (this.user.status !== null) {
          this._getNotifications();
          const personal_info_notif = GetPersonalInfo();
          personal_info_notif.then(res => {
            if (res) {
              const info = [
                {
                  name: "Personal",
                  description: "Please complete your personal profile",
                  link: "Profile"
                }
              ];
              this.setState(
                {
                  notif_personal_info: info
                },
                function() {
                  if (this.props.router.pathname != "/account") {
                    showAlert(this._personalUpdateAlertInfo());
                  }
                }
              );
            }
          });
        }
      }
    } else {
      this._getNotifications();
    }
    if (this.user.status === 3) {
      this._forceLogOut();
      return;
    }
  }

  _alert(message, link, buttons) {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: ``, // Message dialog
      customUI: ({ onClose }) => this._message(message, buttons, onClose) // Custom UI or Component
    });
  }

  _displayButtons(buttons, onClose) {
    return buttons.map(button => {
      return (
        <ModalFooter>
          <Button type="pink" styles={{ margin: "0 auto" }}>
            <a
              onClick={() => {
                button.onClick();
                onClose();
              }}
            >
              {button.label}
            </a>
          </Button>
        </ModalFooter>
      );
    });
  }

  _message(message, buttons, onClose) {
    return (
      <ModalWrapper>
        <ModalContentWrapper styles={{ height: "fit-content" }}>
          <ModalContent>
            <ModalHeader
              classes={[fontWeight(700), font(1.1)]}
              styles={{ color: Colors.skyblue, textAlign: "center" }}
            >
              You need to change your password.
            </ModalHeader>
            <p className={pStyle}>{message}</p>
          </ModalContent>
          {this._displayButtons(buttons, onClose)}
        </ModalContentWrapper>
      </ModalWrapper>
    );
  }

  _forceLogOut() {
    const alertMessage = `You’re no longer enrolled to this service. Please contact your human resource department or your wellness coordinator for re-enrollment and assistance. Thank you.`;

    // _logout PARAMS :  action, redirectTo = null, message = null, allertMessage = null
    this._logout(
      "Login Attempt -  no longer enrolled",
      "login",
      null,
      alertMessage
    );
  }

  _inactiveCompany() {
    const alertMessage = `Your Company is not Active. Please contact your human resource department or your wellness coordinator for re-enrollment and assistance. Thank you.`;
    // _logout PARAMS :  action, redirectTo = null, message = null, allertMessage = null
    this._logout(
      "Login Attempt -  Company Inactive",
      "login",
      null,
      alertMessage
    );
  }

  _updateStatus(statVal) {
    const data = [{ status: statVal }];
    pInfo.update(data);
    pInfo.getLatest();
  }

  _closeModal(str) {
    if (this._isMounted) {
      this.setState(
        {
          [str]: false
        },
        function() {
          if (str === "waiverModalIsOpen") {
            this._passwordMonitor(true);
          }
        }
      );
    }
  }

  _saveActivity(data) {
    pInfo
      .saveActivity(data)
      .then(result => {
        if (result > 0) console.log("activity saved");
      })
      .catch(err => {
        console.log(`Error saving activity : ${err}`);
      });
  }

  _openModal(modalName) {
    if (this._isMounted) {
      this.setState({
        [modalName]: true
      });
    }
  }

  _passwordMonitor(isInitial) {
    if (!this.auth.loggedIn()) return;

    if (isInitial === true) {
      this.setState({
        changePasswordMessage:
          "For your account protection, you are required to change your password",
        needToChangePassword: true
      });
    } else {
      pInfo
        .passwordMonitor(this.user.id)
        .then(response => {
          if (response.length > 0) {
            const res = response[0];
            const dateLastModified = moment(res.date_modified);
            const currentDate = moment();
            const daysPass = currentDate.diff(dateLastModified, "days");
            if (res.status !== 1) {
              if (this._isMounted) {
                this.setState({
                  changePasswordMessage: "> 1 != 1",
                  needToChangePassword: true
                });
              }
            } else if (daysPass >= PASSWORD_AGE) {
              if (this._isMounted) {
                this.setState({
                  changePasswordMessage: `It has been ${daysPass} days since you modified your password. For your account protection, kindly change your password.`,
                  needToChangePassword: true,
                  optionToRetain: true
                });
              }
            }
          } else {
            if (this._isMounted) {
              this.setState({
                changePasswordMessage:
                  "For your account protection, you are required to change your password",
                needToChangePassword: true
              });
            }
          }
        })
        .catch(e => {
          console.log(e);
        });
    }
  }

  supportsFlexBox() {
    const test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  _updateView(input, nextView) {
    if (nextView) {
      if (this._isMounted) {
        this.setState({
          nextView
        });
      }
    } else if (this._isMounted) {
      this.setState({
        nextView: ""
      });
    }
    if (this._isMounted) {
      this.setState({
        activeView: input,
        menuOpen: false
      });
    }
  }

  _logout = (
    action,
    redirectTo = null,
    message = null,
    allertMessage = null
  ) => {
    this.auth
      .logout()
      .then(() => {
        const data = {
          user_id: this.user.id,
          action,
          details: {
            email: this.user.email,
            company: this.user.company,
            type: this.user.type
          }
        };

        if (allertMessage) {
          //const message = `“You’re no longer enrolled to this service. Please contact your human resource department or your wellness coordinator for re-enrollment and assistance. Thank you.”`;
          const link = "/login";
          const buttons = [
            {
              label: "Ok",
              onClick: () => Router.push(link)
            },
            {
              label: "No"
            }
          ];
          this._alert(allertMessage, link, buttons);
        }
        this._saveActivity(data);

        localStorage.clear();

        if (redirectTo) {
          Router.push(`/${redirectTo}`);
        }
      })
      .catch(err => console.log(err)); // you would show/hide error messages with component state here
  };

  logout(e) {
    e.preventDefault();
    // _logout PARAMS :  action, redirectTo = null, message = null, allertMessage = null
    this._logout("Logout", "login");
  }

  _showAddData(input) {
    if (this._isMounted) {
      this.setState({
        activeView: "Add Data",
        dataCategory: input
      });
    }
  }

  _setActiveView(activeView) {
    if (this._isMounted) {
      this.setState({
        activeView
      });
    }
  }

  _setActiveTab(activeTab) {
    if (this._isMounted) {
      this.setState({
        activeTab
      });
    }
  }

  _retainPassword = () => {
    const data = {
      id: this.user.id,
      action: "Retain Password"
    };

    pInfo
      .saveToMonitoring(data)
      .then(res => {
        this.setState({
          openSnackbar: true,
          changePasswordMessage:
            "Old password retained. We strongly recommend to change your password",
          needToChangePassword: false
        });
      })
      .catch(e => {
        console.log(e);
      });
  };

  _passwordChangeAlertInfo() {
    const { changePasswordMessage, optionToRetain } = this.state;

    const message = `${changePasswordMessage}`;
    const link = "/account?menu=account&tab=Profile&view=Password";
    let buttons = [];
    if (optionToRetain) {
      buttons = [
        {
          label: "Change Password",
          onClick: () => {
            if (this._isMounted) {
              this.setState({ needToChangePassword: false });
            }
            Router.push(link);
            this._isMounted = false;
          }
        },
        {
          label: "Retain My Password",
          onClick: () => this._retainPassword()
        }
      ];
    } else {
      buttons = [
        {
          label: "Change Password",
          onClick: () => {
            if (this._isMounted) {
              this.setState({ needToChangePassword: false });
            }
            Router.push({
              pathname: "/account",
              query: { tab: "Profile", view: "Password" }
            });
            this._isMounted = false;
          }
        }
      ];
    }

    return {
      title: "Action Required",
      message: message,
      buttons: buttons
    };
  }

  gotoAccountPersonalPage() {
    Router.push({
      pathname: "/account",
      query: { tab: "Profile", view: "Personal" }
    });
  }

  _personalUpdateAlertInfo() {
    return {
      title: "Action Required",
      message: "Please complete your personal profile",
      buttons: [
        {
          label: "Update Profile",
          onClick: () => this.gotoAccountPersonalPage()
        }
      ]
    };
  }

  render() {
    if (this._isMounted) {
      if (
        this.state.needToChangePassword === true &&
        this.props.router.pathname != "/account"
      ) {
        showAlert(this._passwordChangeAlertInfo());
      }
    }
    return (
      !this.state.loading && (
        <UserContext.Provider
          value={{
            ...this.context,
            user: this.user,
            notifications: this.state.notifications,
            notif_personal_info: this.state.notif_personal_info,
            logout: this.logout,
            getNotifications: this._getNotifications,
            state: this.state,
            path: this.props.router.pathname,
            screenSize: this.state.screenSize,
            waiverIsOpen: this.state.waiverModalIsOpen,
            welcomeIsOpen: this.state.welcomeModalIsOpen,
            needToChangePassword: this.state.needToChangePassword,
            timeOutModalIsOpen: this.state.timeOutModalIsOpen
          }}
        >
          <div className="wrap">
            {this.state.pageLoaded ? null : <Loading />}
            <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
            <div className="headerDiv">
              <Header
                user={this.user}
                logoutPressed={this.logout}
                timeout={false}
              />
            </div>
            <div className="wrap">
              {/* <Menu
              user={this.user}
              updateView={this._updateView}
              useNewMenu={this.props.useNewMenu}
            /> */}

              <ContainedWrapper
                // contain={fullWidth}
                path={this.props.router.pathname}
                center={false}
                styles={{ background: "none", marginTop: 0 }}
                headerAndFooterHeight={this.state.defaultPageHeight - 65}
              >
                <div
                  className="container"
                  style={{ paddingBottom: 0, minHeight: "82vh" }}
                >
                  {this.state.openSnackbar && (
                    <Snack
                      open={this.state.openSnackbar}
                      variant="warning"
                      message={this.state.changePasswordMessage}
                    />
                  )}
                  {this.props.children}
                </div>
              </ContainedWrapper>
              <NewFooter
                state={this.state}
                openModal={this._openModal}
                closeModal={this._closeModal}
                isMounted={this._isMounted}
                updateStatus={this._updateStatus}
                duration={DURATION}
              />
            </div>
          </div>
        </UserContext.Provider>
      )
    );
  }

  handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    this.setState({
      openSnackbar: false
    });
  };
}

let pStyle = css({
  margin: "5px",
  padding: 5,
  textAlign: "center"
});

export default withAuth(withRouter(Template));
