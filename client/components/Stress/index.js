import React from "react";

import Form from "../Form";

import AnswerService from "../../utils/answerService.js";
import SummaryService from "../../utils/summaryService.js";
import GenerateSummary from "../../utils/generateSummary.js";

const answer = new AnswerService();
const summary = new SummaryService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      counseling: {
        id: 40,
        name: "counseling",
        question: "Have you ever sought counseling?",
        type: "select",
        validation: "select",
        options: [{ label: "Yes", value: "yes" }, { label: "No", value: "no" }],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      therapy: {
        id: 41,
        name: "therapy",
        question: "Are you currently in therapy?",
        type: "select",
        validation: "select",
        options: [{ label: "Yes", value: "yes" }, { label: "No", value: "no" }],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      workStress: {
        id: 42,
        name: "workStress",
        question:
          "Rate your daily work stress from 1-10 (1 - low stress 10 - high stress)",
        type: "select",
        validation: "select",
        options: [
          { label: "1", value: 1 },
          { label: "2", value: 2 },
          { label: "3", value: 3 },
          { label: "4", value: 4 },
          { label: "5", value: 5 },
          { label: "6", value: 6 },
          { label: "7", value: 7 },
          { label: "8", value: 8 },
          { label: "9", value: 9 },
          { label: "10", value: 10 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      familyStress: {
        id: 43,
        name: "familyStress",
        question: "Rate your daily family stress from 1-10",
        type: "select",
        validation: "select",
        options: [
          { label: "1", value: 1 },
          { label: "2", value: 2 },
          { label: "3", value: 3 },
          { label: "4", value: 4 },
          { label: "5", value: 5 },
          { label: "6", value: 6 },
          { label: "7", value: 7 },
          { label: "8", value: 8 },
          { label: "9", value: 9 },
          { label: "10", value: 10 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      socialStress: {
        id: 44,
        name: "socialStress",
        question:
          "Rate your daily social stress (relationships, networks, except family) from 1-10",
        type: "select",
        validation: "select",
        options: [
          { label: "1", value: 1 },
          { label: "2", value: 2 },
          { label: "3", value: 3 },
          { label: "4", value: 4 },
          { label: "5", value: 5 },
          { label: "6", value: 6 },
          { label: "7", value: 7 },
          { label: "8", value: 8 },
          { label: "9", value: 9 },
          { label: "10", value: 10 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      financialStress: {
        id: 45,
        name: "financialStress",
        question: "Rate your daily financial stress from 1-10",
        type: "select",
        validation: "select",
        options: [
          { label: "1", value: 1 },
          { label: "2", value: 2 },
          { label: "3", value: 3 },
          { label: "4", value: 4 },
          { label: "5", value: 5 },
          { label: "6", value: 6 },
          { label: "7", value: 7 },
          { label: "8", value: 8 },
          { label: "9", value: 9 },
          { label: "10", value: 10 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      healthStress: {
        id: 46,
        name: "healthStress",
        question: "Rate your daily health stress from 1-10",
        type: "select",
        validation: "select",
        options: [
          { label: "1", value: 1 },
          { label: "2", value: 2 },
          { label: "3", value: 3 },
          { label: "4", value: 4 },
          { label: "5", value: 5 },
          { label: "6", value: 6 },
          { label: "7", value: 7 },
          { label: "8", value: 8 },
          { label: "9", value: 9 },
          { label: "10", value: 10 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      otherStress: {
        id: 47,
        name: "otherStress",
        question: "Rate your other daily stresses from 1-10",
        type: "select",
        validation: "select",
        options: [
          { label: "1", value: 1 },
          { label: "2", value: 2 },
          { label: "3", value: 3 },
          { label: "4", value: 4 },
          { label: "5", value: 5 },
          { label: "6", value: 6 },
          { label: "7", value: 7 },
          { label: "8", value: 8 },
          { label: "9", value: 9 },
          { label: "10", value: 10 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      }
    };

    this._handleChange = this._handleChange.bind(this);
    this._goToNext = this._goToNext.bind(this);
  }

  supportsFlexBox() {
    var test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  componentDidMount() {
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      flexibility(document.body);
    }

    this._setDefaultValues();
  }

  _setDefaultValues() {
    const {
      counseling,
      therapy,
      workStress,
      familyStress,
      socialStress,
      financialStress,
      healthStress,
      otherStress
    } = this.state;

    counseling.value = this.props.survey.counseling;
    therapy.value = this.props.survey.therapy;
    workStress.value = this.props.survey.workStress;
    familyStress.value = this.props.survey.familyStress;
    socialStress.value = this.props.survey.socialStress;
    financialStress.value = this.props.survey.financialStress;
    healthStress.value = this.props.survey.healthStress;
    otherStress.value = this.props.survey.otherStress;

    this.setState({
      counseling,
      therapy,
      workStress,
      familyStress,
      socialStress,
      financialStress,
      healthStress,
      otherStress
    });
  }

  render() {
    return (
      <div style={styles.container}>
        <div style={styles.introInfo}>
          <h1 style={{ marginTop: 0 }}>Stress</h1>
          <span>
            Managing your stress levels is an important component in optimizing
            your health.
            <br />
            Data on your stressors and current stress levels will help us draft
            the best health plan to manage your stress.
          </span>
        </div>
        <Form
          from="health-survey"
          stress
          // header={this.props.location == 'profile' ? "Update Stress Info Below" : "Continue Your Profile Below"}
          submitForm={this._goToNext}
          buttonText={
            this.props.location == "profile"
              ? "Update My Profile"
              : "Continue My Profile"
          }
          handleChange={this._handleChange}
          inputs={[
            this.state.counseling,
            this.state.therapy,
            this.state.workStress,
            this.state.familyStress,
            this.state.socialStress,
            this.state.financialStress,
            this.state.healthStress,
            this.state.otherStress
          ]}
          secondaryOption=""
          formError={this.state.formError}
          formSuccess={this.state.formSuccess}
          back={this.props.back}
        />
      </div>
    );
  }

  _goToNext() {
    this.setState({
      formError: {
        active: false,
        message: ""
      }
    });

    var inputs = [
      this.state.counseling,
      this.state.therapy,
      this.state.workStress,
      this.state.familyStress,
      this.state.socialStress,
      this.state.financialStress,
      this.state.healthStress,
      this.state.otherStress
    ];

    const summaryGenerator = new GenerateSummary(inputs, "stress");
    let stressSummary = [
      {
        name: "stress",
        group: "stress",
        label: "Stress",
        value: summaryGenerator.generateSummary()
      }
    ];

    answer
      .create(inputs)
      .then(res => {
        if (res) {
          summary
            .create(stressSummary)
            .then(result => {
              console.log("update user profile");
              this.setState({
                formSuccess: {
                  active: true,
                  message: "Your Profile Was Updated"
                }
              });
              this.props.back();
              //  return this.props.onClick('Dashboard')
            })
            .catch(error => {
              console.log(error);
              this.setState({
                formError: {
                  active: true,
                  message: "There was an error updating your profile"
                }
              });
            });
        } else {
          this.setState({
            formError: {
              active: true,
              message: "There was an error updating your profile"
            }
          });
        }
      })
      .catch(e => {
        this.setState({
          formError: {
            active: true,
            message: e.error
          }
        });
      }); // you would show/hide error messages with component state here
  }

  _handleChange(value, input, validated) {
    const inputName = input;
    var newInput = this.state[inputName];
    newInput.value = value;
    newInput.error.active = validated;

    this.setState({
      inputName: newInput
    });
  }
}

const styles = {
  container: {
    width: "100%",
    fontFamily: '"Roboto",sans-serif',
    display: "flex",
    flexWrap: "wrap",
    //  flexDirection: 'column',
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden"
  },
  introInfo: {
    flex: "1 0 100%",
    fontFamily: '"Roboto",sans-serif',
    color: "#364563",
    fontSize: "14px",
    padding: "10px 20px",
    textAlign: "center"
  },
  formContainer: {
    border: "2px solid rgba(151,151,151,0.2)",
    borderRadius: "6px",
    maxWidth: "400px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.20)"
  },
  header: {
    borderBottom: "2px solid rgba(225,232,238,1)",
    padding: "15px 20px",
    fontSize: "18px",
    color: "#364563",
    fontFamily: '"Roboto-Medium",sans-serif',
    width: "360px"
  },
  form: {
    margin: "10px 30px 20px 20px"
  },
  formInput: {
    padding: "13px 30px 13px 30px",
    marginTop: "20px",
    width: "287px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    fontSize: "14px"
  },
  options: {
    marginTop: "15px",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif',
    padding: "13px 30px 13px 30px",
    WebkitAppearance: "none",
    MozAppearance: "none",
    appearance: "none",
    width: "350px",
    backgroundColor: "#fff",
    borderColor: "#d9d9d9 #ccc #b3b3b3",
    borderradius: "4px",
    border: "2px solid #E1E8EE",
    cursor: "default",
    borderSpacing: "0",
    borderCollapse: "separate",
    counseling: "50px",
    outline: "none",
    overflow: "hidden",
    position: "relative",
    cursor: "pointer"
  },
  optionsDefault: {
    color: "rgba(169, 169, 169, 1)"
  },
  selectArrowZone: {
    cursor: "pointer",
    display: "table-cell",
    position: "relative",
    textAlign: "center",
    verticalAlign: "middle",
    width: "25px",
    paddingRight: "5px"
  },
  selectArrow: {
    borderColor: "#999 transparent transparent",
    borderStyle: "solid",
    borderWidth: "5px 5px 2.5px",
    display: "inline-block",
    counseling: "0",
    width: "0",
    position: "absolute",
    right: "-18em",
    top: "-1.75em"
  },
  button: {
    backgroundColor: "#ED81B9",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "0px 0px 20px 0px",
    cursor: "pointer"
  },
  register: {
    textDecoration: "none",
    margin: "20px 10px",
    color: "#43484D",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif'
  }
};
