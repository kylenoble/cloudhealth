import { css } from "glamor";
import { confirmAlert } from "react-confirm-alert"; // Import
import moment from "moment-timezone";
import EventsService from "../../utils/eventsService";
import dateFormat from "dateformat"; // not localize
import {labelClass} from '../NewSkin/Styles'
import { Preparing } from "../NewSkin/Components/Loading";

import EventsTable from "./eventsTable";

const events = new EventsService();

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.user = this.props.user;
    this.viewfor = this.props.viewfor;

    this._getEvents = this._getEvents.bind(this);
    this._updateEvent = this._updateEvent.bind(this);
    this._showDetails = this._showDetails.bind(this);
    this._eventDetails = this._eventDetails.bind(this);
    this._join = this._join.bind(this);
    this._remove = this._remove.bind(this);
  }

  state = {
    user: "",
    events: []
  };

  componentDidMount() {
    this._getEvents();
    this.setState({ user: this.props.user });
    //  this.props.setNofication(3)
  }

  _getEvents() {
    let company_id = null;
    let type =
      this.props.user.type === "Patient"
        ? "Patients"
        : this.props.user.type === "Doctor"
        ? "Doctors"
        : this.props.user.type;
    type += "_" + this.props.user.company_id;

    events
      .get(type, this.props.user.id)
      .then(res => {
        if (res.length > 0) {
          let filtered_result = res;
          if (this.props.viewfor === "bulletin") {
            filtered_result = res.filter(event => event.company_enrollment_id == null)
          }
          this.setState({
            events: filtered_result
          });
        }

        if (this.props.viewfor === "bulletin") {
          this._allEventsRead();
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _allEventsRead = () => {
    const events = this.state.events;
    const notYetRead = events.filter(event => event.read_by == null);
    if (notYetRead.length > 0) {
      this.props.updateRead("read_events", false);
    } else {
      this.props.updateRead("read_events", true);
    }
  };

  _remove(event) {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: "Are you sure you want to remove this?", // Message dialog
      buttons: [
        {
          label: "Yes",
          onClick: () => this._updateEvent(event, 1, this.user)
        },
        {
          label: "No"
        }
      ]
    });
  }

  _join(event, tag) {
    let mess = "";
    if (tag === 2) {
      mess = "Please confirm that you are joining this event.";
    } else {
      mess = "Please confirm that you are declining to join this event.";
    }
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: mess, // Message dialog
      buttons: [
        {
          label: "Yes",
          onClick: () => this._updateEvent(event, tag, this.user)
        },
        {
          label: "No"
        }
      ]
    });
  }

  _showDetails(event) {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: "", // Message dialog
      childrenElement: () => this._eventDetails(event), // Custom UI or Component
      buttons: [
        {
          label: "Close",
          onClick: () => this._updateEvent(event, null, this.user)
        }
      ]
    });
  }

  _updateEvent(event, tag, user) {
    const user_type = user.type;
    var date = new Date();
    let ndate = dateFormat(date, "yyyy-mm-dd h:MMTT");
    let read_by_data = event.read_by;
    let result = [];

    if (read_by_data) {
      result = read_by_data.data.filter(rb => rb.id === this.user.id);
    }

    let rd = [];
    if (read_by_data) {
      rd = read_by_data.data;
    }
    let data = {
      id: event.id,
      read_by: {
        data: rd
      }
    };

    if (result.length > 0) {
      if (tag) {
        result[0].status = tag;
      }

      data = {
        id: event.id,
        read_by: read_by_data
      };
    } else {
      data.read_by.data.push({
        type: user_type,
        id: this.user.id,
        date_read: ndate,
        status: null
      });
    }

    events
      .update(data)
      .then(res => {
        this._getEvents();
      })
      .catch(e => {
        console.log(e);
      });
  }

  _eventDetails(event) {
    return (
      <div style={{width: 600}}>
        <label className={labelClass}>Event Name: {event.name}</label>
        <h4 className={notifFrom}><small>Posted date: {dateFormat(event.date_created, 'fullDate')}</small></h4>

        <div className={appointmentDetailsWrapper}>
          <label className={detailsTitle}>Event Details</label>
          <p style={{marginTop: 10}}>Venue : {event.venue}<br/>
          Start Date : {dateFormat(event.start_date, "fullDate")}{" "}
          {moment(event.start_time, "HH:mm").format("hh:mm A")}<br/>
          End Date : {dateFormat(event.end_date, "fullDate")}{" "}
          {moment(event.end_time, "HH:mm").format("hh:mm A")}
          </p>
          <p dangerouslySetInnerHTML={{ __html: event.description }} />
        </div>
      </div>
    );
  }

  render() {
    return (
      <section style={{ width: "100%" }}>
        {this.state.events ? (
          <EventsTable
            viewFor={this.viewfor}
            user={this.props.user}
            join={this._join}
            updateEvent={this._updateEvent}
            showDetails={this._showDetails}
            events={this.state.events}
            remove={this._remove}
          />
        ) : (
          <Preparing />
        )}
      </section>
    );
  }
} // end of component class

// CSS Styling
let notifFrom = css({
  margin: '10px 0px',
  '> small': {
    background: 'whitesmoke',
    padding: '5px 10px',
    display: 'inline-block',
  }
})

let appointmentDetailsWrapper = css({
  padding: 15,
  background: '#fff',
  overflow: 'hidden',
  borderRadius: 3,
  marginTop: 10,
  boxShadow: '0 5px 10px rgba(0,0,0,.05)',
  '> h4':{
    fontSize: 18
  },
  '> div span': {
    display: 'inline-block',
    minWidth: 135
  },
  '> p': {
    padding: '10px 5px'
  }
})

let detailsTitle = css({
  margin: '-15px -15px 0',
  display: 'block',
  padding: '5px 15px',
  background: '#80cde9',
  textTransform: 'uppercase',
  color: '#fff'
})