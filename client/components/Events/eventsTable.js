import React from 'react'
import ReactTable from "react-table"
import moment from 'moment-timezone'
import { css } from 'glamor'
import { confirmAlert } from 'react-confirm-alert'
import NoData from '../NewSkin/Components/NoData';
import Colors from '../NewSkin/Colors';
import { ReactTableStyle } from "../NewSkin/Styles";
import Button from '../NewSkin/Components/Button';
import GridContainer from '../NewSkin/Wrappers/GridContainer';

const EventsTable = props => {
  const events = props.events
  const headers = ['Event Name', 'Location', 'Event Date', 'Action']

  const _accessor = (accessor) => {
    let acc = ''
    const a = accessor.toLowerCase().split(' ')
    for (const ac of a) { acc += `${ac}` }
    return acc.trim()
  }

  const _dates = (date, time) => {
    // return [moment(date).format('MMMM DD YYYY'), moment(time).format('hh:mm A')]
    return moment(date).format('MMMM DD YYYY')
  }

  const _convertTime = (time) => {
    let t = time.split('+')[0]
    // Check correct time format and split into components
    t = t.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [t];

    if (t.length > 1) { // If time format correct
      t = t.slice(1);  // Remove full string match value
      t[5] = + t[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
      t[0] = + t[0] % 12 || 12; // Adjust hours

      delete t[3] //removes ms
    }
    return t.join(''); // return adjusted time or original string
  }

  const is_read = (event) => {
    const altered_event = { ...event }
    const user_id = props.user.id
    const read_by = altered_event.read_by
    if (read_by) {
      if (read_by.data) {
        const your_read_event = read_by.data.filter(data => data.id === user_id)
        const joined_or_declined = your_read_event.filter(event => event.status !== null)
        altered_event.joined_or_declined = joined_or_declined.length > 0 ? joined_or_declined[0].status : null
        altered_event.is_read = your_read_event.length > 0 ? true : false;
        return altered_event
      }
    } else {
      altered_event.joined_or_declined = null
      altered_event.is_read = false
      return altered_event
    }
  }

  const _renderTable = () => {
    let columns = []
    let data = []
    var date = new Date()
    let current_date = moment(date).format('YYYY-MM-DD')
    if (events.length > 0) {
      events.forEach(event => {
        var event_end_date = moment(event.end_date).format('YYYY-MM-DD');
        const is_read_event = is_read(event)
        console.log(props.viewFor)
        if (is_read_event.joined_or_declined !== 1) { //not archived
          if (props.viewFor === 'bulletin' && event.company_enrollment_id == null) {
            // if (!is_read_event.joined_or_declined) {
            // console.log('%c read event', 'color: green', is_read_event);
            data.push(is_read_event)
            // }
          } else if (props.viewFor === 'health_talks' && event_end_date >= current_date && event.company_enrollment_id != null) {
            // if (event.company_enrollment_id != null && event.attendance_status != null) {
              data.push(is_read_event)
            // } else if (is_read_event.joined_or_declined === 2) {
            //   data.push(is_read_event)
            // }
          }
        }
      })
    }

    if (data.length > 0) {
      data.filter(event => {
        columns = [
          // event name
          {
            id: 1,
            Header: headers[0],
            accessor: `${_accessor(headers[0])}`,
            Cell: event => (
              <div
                style={{ cursor: 'pointer' }}
                className={event.original.is_read ? null : yourReadEvent}
                onClick={() => props.showDetails(event.original)}
              >{event.original.name}</div>
            ),
            // width: 450
          },
          // venue
          // {
          //   id: 2,
          //   Header: headers[1],
          //   accessor: `${_accessor(headers[0])}`,
          //   Cell: event => (
          //     <div
          //       style={{ cursor: 'pointer' }}
          //       className={event.original.is_read ? null : yourReadEvent}
          //       onClick={() => props.showDetails(event.original)}
          //     >{event.original.venue}</div>
          //   ),
          //   width: 200
          // },
          // start date & time
          {
            id: 3,
            Header: headers[2],
            accessor: `${_accessor(headers[0])}`,
            Cell: event => (
              <div
                style={{ cursor: 'pointer' }}
                className={event.original.is_read ? null : yourReadEvent}
                onClick={() => props.showDetails(event.original)}
              >{_dates(event.original.start_date)} {` `} {_convertTime(event.original.start_time)}</div>
            ),
            // width: 200
          },
          // actions
          {
            id: 4,
            Header: headers[3],
            accessor: `${_accessor(headers[0])}`,
            Cell: event => (
              <GridContainer columnSize={event.original.is_read && event.original.joined_or_declined === null && moment(event.original.end_date).isBefore(current_date) ? '1fr' :'1fr 1fr'} gap={10}>
                {
                  (event.original.joined_or_declined === null && moment(event.original.end_date).isAfter(current_date) ?
                    <>
                      <Button styles={{ minWidth: 'fit-content' }} type={event.original.is_read ? 'blue' : 'disabled'}>
                        <span style={{padding: 0}}
                          onClick={event.original.is_read ? () => props.join(event.original, 2) : () => { }}
                        >Join</span>
                      </Button>

                      <Button styles={{ minWidth: 'fit-content' }} type={event.original.is_read ? 'default' : 'disabled'}>
                        <span style={{padding: 0}}
                          onClick={event.original.is_read ? () => props.join(event.original, 3) : () => { }}
                        >Decline</span>
                      </Button>
                    </> : event.original.is_read ?
                      <>
                        {
                          event.original.joined_or_declined === 2 ?
                            <small style={{ color: Colors.skyblue }} className={action}>JOINED</small> :
                            event.original.joined_or_declined === 3 ?
                              <small style={{ color: Colors.compromised }} className={action}>DECLINED</small> : null
                        }
                        <img onClick={() => props.remove(event.original)} title="Archive" className={iconStyle} src={'/static/images/trash.svg'} />
                      </> : null
                  )
                }
              </GridContainer>
            )
          }
        ]
      })
    }

    return (
      <section className={`${ReactTableStyle} ${table}`} style={{ maxWidth: '100%' }}>
        {
          data.length > 0 ?
            <ReactTable
              data={data}
              columns={columns}
              defaultPageSize={4}
              minRows={0}
              className={`-highlight react-table`}
              width={90}
            /> :
            <NoData icon="event" text="No event available" />
        }
      </section>
    )
  }

  return (
    <div>
      {_renderTable()}
    </div>
  )
}

export default EventsTable

const action = css({
  fontWeight: 600,
  textAlign: 'right !important'
})

const table = css({
  '& .ReactTable .rt-thead .rt-resizable-header:last-child': {
    width: '250px !important',
    maxWidth: '250px !important'
  },
  '& .ReactTable .rt-tbody .rt-td:last-child': {
    width: '250px !important',
    maxWidth: '250px !important'
  }
})

let iconStyle = css({
  width: 15,
  height: 15,
  cursor: 'pointer',
  opacity: .5,
  transition: '200ms ease',
  display: 'block',
  margin: 'auto auto',
  filter: 'grayscale(100%)',
  ':hover': {
    filter: 'grayscale(0%)',
    opacity: 1
  }
})

let disabledAction = css({
  background: '#ccc !important',
  color: '#eee !important',
  opacity: .5,
  cursor: 'default',
  ':hover': {
    color: '#eee !important'
  }
})

let actionWrapper = css({
  display: 'grid',
  // alignItems: 'center',
  // justifyContent: 'center',
  // '> div': {
  //   display: 'flex',
  //   alignItems: 'center',
  //   justifyContent: 'center',
  // }
})

let yourReadEvent = css({
  fontWeight: 600,
  color: '#80cde9',
  display: 'grid',
  placeContent: 'center'
})

let button = css({
  width: 'auto',
  margin: '0 5px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  padding: '0px 10px',
  fontSize: 'small',
  background: '#80cde9',
  color: '#fff',
  height: '30px',
  cursor: 'pointer',
  ':hover': {
    color: '#000'
  }
})
