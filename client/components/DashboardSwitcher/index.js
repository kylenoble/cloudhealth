import React from "react";
import { css } from "glamor";
import ReactTooltip from "react-tooltip";
import Router, { withRouter } from "next/router";

const baseLink = "/dashboard";

class DashboardSwitcher extends React.Component {
  constructor(props) {
    super(props);
    this.user = this.props.user;
    this.state = {
      notification: false,
      eventNotification: false,
      announcementNotifiation: false
    };

    this._showViewTitle = this._showViewTitle.bind(this);
    this._updateCurrentView = this._updateCurrentView.bind(this);
    this._openModal = this._openModal.bind(this);
  }

  componentWillUnmount() {
    // use intervalId from the state to clear the interval
    //clearInterval(this.state.intervalId);
  }

  componentDidMount() {
    this._getNotification_head();
    // const intervalId = setInterval(() => {
    //   this._getNotification_head();
    // }, 5000);

    // this.setState({ intervalId });
  }

  _getNotification_head() {
    const notification = this.props.getNotification
      ? this.props.getNotification
      : null;
    const notificationsFromAdmin = this.props.notificationsFromAdmin
      ? this.props.notificationsFromAdmin
      : null;
    const filteredNotificationsFromAdmin = notificationsFromAdmin
      ? notificationsFromAdmin.filter(
          notification => notification.read_by == null
        )
      : null;
    const notReadAnnouncementCtr = this.props.notReadAnnouncementCtr
      ? this.props.notReadAnnouncementCtr
      : null;

    if (notification) {
      if (notification.length > 0) {
        this.setState({
          notification: true
        });
      }
    }

    if (filteredNotificationsFromAdmin) {
      if (filteredNotificationsFromAdmin.length > 0) {
        this.setState({
          notification: true
        });
      }
    }

    if (this.props.notReadEventCtr) {
      if (this.props.notReadEventCtr > 0) {
        this.setState({
          eventNotification: true,
          announcementNotifiation: true
        });
      }
    }

    if (this.props.notReadAnnouncementCtr) {
      if (this.props.notReadAnnouncementCtr > 0) {
        this.setState({
          announcementNotifiation: true
        });
      }
    }
  }

  _openModal() {
    this.props.openModal("privacyPolicyModalIsOpen");
  }

  render() {
    return (
      <div>
        <ReactTooltip className={`${tooltipStyle}`} />
        {this._showViewTitle()}
        <div className={subMenuDiv}>
          {/*
                <span onClick={this._openModal} className={subLinkStyle}>Privacy Policy</span>
              */}
        </div>
      </div>
    );
  }

  _showViewTitle() {
    let titles;
    if (this.user.type === "Patient") {
      titles = [
        {
          icon: "/static/icons/dash.svg",
          iconActive: "/static/icons/activeDash.svg",
          title: "Dashboard"
        },
        {
          icon: "/static/icons/healthprofile.svg",
          iconActive: "/static/icons/activeHealthprofile.svg",
          title: "HealthProfile"
        },
        {
          icon: "/static/icons/healthactivities.svg",
          iconActive: "/static/icons/activeHealthactivities.svg",
          iconNotify: "/static/icons/healthactivitiesWithNotification.svg",
          title: "Activities"
        },
        // {
        //     icon: '/static/icons/marketplace.svg',
        //     iconActive: '/static/icons/activeMarketplace.svg',
        //     title: 'Marketplace'
        // },
        {
          icon: "/static/icons/account.svg",
          iconActive: "/static/icons/activeAccount.svg",
          title: "Account"
        },
        // {
        //     icon: '/static/icons/appointments.svg',
        //     iconActive: '/static/icons/activeAppointments.svg',
        //     title: 'Appointments'
        // },
        // {
        //     icon: '/static/icons/history.svg',
        //     iconActive: '/static/icons/activeHistory.svg',
        //     title: 'History'
        // }
        {
          icon: "/static/icons/bulletin.svg",
          iconActive: "/static/icons/activeBulletin.svg",
          iconNotify: "/static/icons/bulletinWithNotification.svg",
          title: "Bulletin"
        },
        {
          icon: "/static/icons/notification.svg",
          iconActive: "/static/icons/activeNotification.svg",
          iconNotify: "/static/icons/withNotification.svg",
          title: "Notification"
        }
      ];
    } else {
      titles = [
        {
          icon: "/static/icons/dash.svg",
          iconActive: "/static/icons/activeDash.svg",
          iconNotify: "/static/icons/dashWithNotification.svg",
          title: "DoctorDashboard"
        },
        {
          icon: "/static/icons/myschedule.svg",
          iconActive: "/static/icons/myscheduleActive.svg",
          title: "MySchedule"
        },
        {
          icon: "/static/icons/tracker.svg",
          iconActive: "/static/icons/activeTracker.svg",
          title: "Tracker"
        },
        {
          icon: "/static/icons/my_profile.svg",
          iconActive: "/static/icons/my_profile_active.svg",
          title: "Account"
        },
        {
          icon: "/static/icons/notification.svg",
          iconActive: "/static/icons/activeNotification.svg",
          iconNotify: "/static/icons/withNotification.svg",
          title: "Notification"
        }
        // {
        //     icon: '/static/icons/appointments.svg',
        //     iconActive: '/static/icons/activeAppointments.svg',
        //     title: 'Appointments'
        // },
        // {
        //     icon: '/static/icons/history.svg',
        //     iconActive: '/static/icons/activeHistory.svg',
        //     title: 'History'
        // },
        // {
        //     icon: '/static/icons/profile.svg',
        //     iconActive: '/static/icons/activeProfile.svg',
        //     title: 'Profile'
        // },
      ];
    }
    const html = [];
    titles.forEach((currentItem, index) => {
      html.push(
        <div
          key={index}
          id={currentItem.title}
          onClick={this._updateCurrentView}
          className={
            this.props.activeView === currentItem.title ? itemActive : item
          }
        >
          {currentItem.title === "DoctorDashboard" ? (
            <img
              id={currentItem.title}
              className={icon}
              src={
                this.props.activeView === currentItem.title
                  ? currentItem.iconActive
                  : currentItem.title == "DoctorDashboard"
                  ? this.state.eventNotification
                    ? currentItem.iconNotify
                    : currentItem.icon
                  : currentItem.icon
              }
            />
          ) : currentItem.title === "Activities" ? (
            <img
              id={currentItem.title}
              className={icon}
              src={
                this.props.activeView === currentItem.title
                  ? currentItem.iconActive
                  : currentItem.title == "Activities"
                  ? this.state.eventNotification
                    ? currentItem.iconNotify
                    : currentItem.icon
                  : currentItem.icon
              }
            />
          ) : currentItem.title === "Bulletin" ? (
            <img
              id={currentItem.title}
              className={icon}
              src={
                this.props.activeView === currentItem.title
                  ? currentItem.iconActive
                  : currentItem.title == "Bulletin"
                  ? this.state.announcementNotifiation
                    ? currentItem.iconNotify
                    : currentItem.icon
                  : currentItem.icon
              }
            />
          ) : (
            <img
              id={currentItem.title}
              className={icon}
              src={
                this.props.activeView === currentItem.title
                  ? currentItem.iconActive
                  : currentItem.title == "Notification"
                  ? this.state.notification
                    ? currentItem.iconNotify
                    : currentItem.icon
                  : currentItem.icon
              }
            />
          )}

          {currentItem.title == "Notification"
            ? this.state.notification
              ? this.state.notification
              : null
            : null}
        </div>
      );
    });

    return html;
  }

  _updateCurrentView(e) {
    e.preventDefault();

    this.props.closeMenu();

    if (e.target.id == "Notification") {
      this.setState({
        notification: false
      });
    }

    // if (this.props.useNewMenu) {
    //   if (e.target.id === "Dashboard") {
    //     Router.push("/dashboard");
    //   } else if (e.target.id === "HealthProfile") {
    //     Router.push("/healthprofile");
    //   } else if (e.target.id === "Activities") {
    //     Router.push("/activities");
    //   } else if (e.target.id === "Account") {
    //     Router.push("/account");
    //   } else if (e.target.id === "Bulletin") {
    //     Router.push("/bulletin");
    //   } else if (e.target.id === "Notification") {
    //     Router.push("/notification");
    //   } else {
    //     this.props.titlePressed(e.target.id);
    //   }
    // } else {
    //   this.props.titlePressed(e.target.id);
    // }

    if (this.user.type === "Patient") {
      if (e.target.id === "Dashboard") {
        Router.push(`${baseLink}`);
      } else if (e.target.id === "HealthProfile") {
        Router.push(`${baseLink}?menu=healthprofile`);
      } else if (e.target.id === "Activities") {
        Router.push(`${baseLink}?menu=activities`);
      } else if (e.target.id === "Account") {
        Router.push(`${baseLink}?menu=account`);
      } else if (e.target.id === "Bulletin") {
        Router.push(`${baseLink}?menu=bulletin`);
      } else if (e.target.id === "Notification") {
        Router.push(`${baseLink}?menu=notification`);
      }
    } else if (e.target.id === "DoctorDashboard") {
      Router.push(`${baseLink}`);
    } else if (e.target.id === "Notification") {
      Router.push(`${baseLink}?menu=notification`);
    } else if (e.target.id === "Account") {
      Router.push(`${baseLink}?menu=account`);
    } else if (e.target.id === "MySchedule") {
      Router.push(`${baseLink}?menu=schedule`);
    } else if (e.target.id === "Tracker") {
      Router.push(`${baseLink}?menu=patient_tracker`);
    }
  }
}

export default withRouter(DashboardSwitcher);

const container = css({
  display: "flex",
  flexDirection: "column",
  paddingTop: "40px",
  margin: "50px 0 0 0",
  boxShadow: "0px 0px 1px 3px rgba(204,204,204,.1)",
  minWidth: "200px",
  backgroundColor: "white"
});

const icon = css({
  width: "250px",
  margin: "5px 5px",
  //  height: '50px',
  marginLeft: "10px",
  "@media(max-width: 820px)": {
    //marginLeft: '-180px',
  }
});

const dashItem = css({
  fontFamily: '"Roboto-Black",sans-serif',
  fontSize: "25px",
  color: "white",
  letterSpacing: 0,
  lineHeight: "14px",
  padding: "10px 20px",
  margin: "5px 5px",
  cursor: "pointer",
  "@media(max-width: 700px)": {
    display: "none"
  }
});

const itemActive = css({
  fontFamily: '"Roboto-Black",sans-serif',
  color: "#358ED7",
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  justifyContent: "flex-start",
  alignItems: "center",
  margin: "5px 15px",
  cursor: "pointer",
  "@media(max-width: 820px)": {
    //marginLeft: '-180px',
    width: 100
  }
});

const item = css({
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  justifyContent: "flex-start",
  alignItems: "center",
  margin: "5px 15px",
  color: "#BDC7CF",
  cursor: "pointer",
  "@media(max-width: 820px)": {
    //marginLeft: '-180px',
    width: 100
  }
});

let subMenuDiv = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "center"
});

const subLinkStyle = css({
  cursor: "pointer",
  padding: "5px",
  color: "#fff",
  ":hover": {
    borderRadius: "10px",
    padding: "5px",
    border: "1px solid #3498db",
    color: "#81cce7"
  },
  margin: "0 10px 0 10px",
  fontSize: "x-small"
});

// let subLinkStyle = css({
//   color: '#fff',
//   background: 'transparent', ':hover': {
//     background: '#BDC7CF',
//     color: 'blue'
//   },
//   cursor: 'pointer'
// })

const tooltipStyle = css({
  borderradius: "10px",
  padding: "15px",
  opacity: "0.9 !important",
  filter: "alpha(opacity=50)",
  maxWidth: "200px",
  color: "#000 !important",
  backgroundcolor: "#80cde9 !important",

  "&.place-top": {
    "&:after": {
      borderTopcolor: "#80cde9 !important",
      bordertopStyle: "solid !important",
      bordertopwidth: "6px !important"
    }
  }
});
