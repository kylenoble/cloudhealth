import React, { Component } from "react";
import { css } from "glamor";
import moment from "moment-timezone";
import request from "axios";
// superagent error causing error in upgraded libraries.  sulution is to replace by axios
import currentDomain from "../../utils/domain";

import GridContainer from "../NewSkin/Wrappers/GridContainer";

// api
const UPLOAD_ATTACHMENT_URL = `${currentDomain}/api/certificate/uploadAttachment`;
const REMOVE_ATTACHMENT_URL = `${currentDomain}/api/certificate/removeAttachment/`;
const SUBMIT_CERTIFICATE_URL = `${currentDomain}/api/certificate/`;
const UPDATE_CERTIFICATE_URL = `${currentDomain}/api/certificate/update/`;

// imported components
import InputMoment from "../calender/input-moment";
import Button from "../NewSkin/Components/Button";

class AddCertificate extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    m: moment(),
    date: "",
    attachment: "",
    file: "",
    recentAttachmentPath: "",
    name: "",
    level: "",
    status: "",
    day: "",
    month: "",
    year: ""
  };

  componentDidMount() {
    this.props.is_edit ? this._getEditValues() : this._defaultValues();
  }

  _defaultValues = () => {
    this.setState({
      recentAttachmentPath: "",
      name: null,
      date: null,
      level: null,
      status: null,
      attachment: null
    });
  };

  _getEditValues = () => {
    const {
      attachment,
      training_name,
      date_completed,
      certification_level,
      verification_status
    } = this.props.editValues;
    setTimeout(() => {
      this.setState({
        recentAttachmentPath: attachment
          ? `/static/certificates/${attachment}`
          : null,
        name: training_name,
        date: date_completed,
        level: certification_level,
        status: verification_status,
        attachment,
        m: moment(date_completed)
      });
    }, 100);
  };

  _handleChange = m => {
    setTimeout(() => {
      this.setState({
        m,
        date: m.toISOString(true)
      });
    }, 100);
  };

  _handleFileUpload = e => {
    e.preventDefault();
    e.target.previousSibling.click();
  };

  _handleChooseFile = e => {
    let file = e.target.files[0];
    this.setState({
      attachment: file.name,
      file
    });

    this._handleImageUpload(file);
  };

  _handleImageUpload = file => {
    const prefix = Date.now();

    this.setState({
      attachment: `${prefix}-${file.name}`
    });

    const fd = new FormData();
    fd.append("prefix", prefix);
    fd.append("file", file);

    let upload = "";
    upload = request.post(UPLOAD_ATTACHMENT_URL, fd);

    upload.then(response => {
      if (response.status === 200) {
        setTimeout(() => {
          this.setState({
            recentAttachmentPath:
              "../../../static/certificates/" + this.state.attachment
          });
        }, 1);
        console.log("Attachment successfully uploaded.");
      } else {
        console.log("Error Uploading Attachment");
      }
    });
  };

  _handleRemoveAttachment = e => {
    e.preventDefault();
    let recent = this.state.attachment;
    request
      .get(REMOVE_ATTACHMENT_URL + recent)
      .then(() => {
        this.setState({
          recentAttachmentPath: null,
          attachment: null
        });
        console.log("Attachment removed successfully!");
      })
      .catch(err => {
        console.log(err);
      });
  };

  _handleSubmit = e => {
    e.preventDefault();
    if (this.name.value.length > 0) {
      const data = {
        user_id: this.props.user.id,
        name: this.name.value,
        date: this.state.date,
        level: this.level.value,
        status: this.status.value,
        attachment: this.state.attachment
      };

      this.props.is_edit ? this._update(data) : this._create(data);
    } else {
      this.name.focus();
    }
  };

  _update = data => {
    const id = this.props.editValues.id;

    request.put(UPDATE_CERTIFICATE_URL + id, data).then(res => {
      document.querySelector("#upload-certificate").reset();
      this.props.closeModal("addCertificateModalIsOpen");
      this.props.getCertificates();
    });
  };

  _create = data => {
    request.post(SUBMIT_CERTIFICATE_URL, data).then(res => {
      document.querySelector("#upload-certificate").reset();
      this.props.closeModal("addCertificateModalIsOpen");
      this.props.getCertificates();
    });
  };

  _onFocus = e => {
    const sibling = e.target.previousSibling;
    sibling.classList.add(displayCalendar);

    setTimeout(() => {
      sibling.classList.add(showCalendar);
    }, 100);
  };

  _confirmDate = e => {
    e.preventDefault();
    document.querySelector("#calendar-wrapper").classList.remove(showCalendar);
    setTimeout(() => {
      document
        .querySelector("#calendar-wrapper")
        .classList.remove(displayCalendar);
    }, 300);
  };

  _reset = e => {
    this._confirmDate(e)
    this.setState({
      date: null
    })
  };

  onChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  render() {
    const {
      attachment,
      training_name,
      date_completed,
      certification_level,
      verification_status
    } = this.props.editValues;

    const is_edit = this.props.is_edit;
    return (
      <div className={`${outside}`}>
        <h1>
          {is_edit ? "Edit" : "Add"} Certificate{" "}
          <span
            onClick={() => this.props.closeModal("addCertificateModalIsOpen")}
            className={closeBtn}
          />
        </h1>
        <GridContainer columnSize={'1fr 1fr'} styles={{padding: 20}}>
          <section>
            <form id="upload-certificate" className={`${formWrapper}`}>
              <div className={`${formInputWrapper}`}>
                <label>
                  Name of Training{" "}
                  <small style={{ color: pink, fontSize: 10 }}>REQUIRED</small>
                </label>
                <input
                  type="text"
                  id="name"
                  required
                  ref={name => (this.name = name)}
                  defaultValue={training_name}
                  onChange={e => this.onChange(e)}
                />
              </div>

              <div className={`${formInputWrapper}`}>
                <label>
                  Date Completed{" "}
                  <small style={{ color: pink, fontSize: 10 }}>REQUIRED</small>
                </label>
                <div id="calendar-wrapper" className={`${calendarWrapper}`}>
                  <InputMoment
                    moment={this.state.m}
                    onChange={this._handleChange}
                    minStep={1}
                    dateTimeBtn={false}
                    is_profile={true}
                  />
                  <GridContainer columnSize={'1fr 1fr'} styles={{marginTop: 10}}>
                    <Button type="blue" styles={{minWidth: 150, maxWidth: 150}}>
                      <button
                      className={`${btn} ${btn_confirm}`}
                      onClick={this._confirmDate}
                    >
                      Confirm
                    </button>
                    </Button>
                    <Button styles={{minWidth: 150, maxWidth: 150}}>
                      <button
                      className={`${btn} ${btn_confirm}`}
                      onClick={this._reset}
                    >
                      Reset
                    </button>
                    </Button>
                  </GridContainer>
                </div>
                <input
                  onChange={() => { }}
                  type="text"
                  onFocus={this._onFocus}
                  value={this.state.date ? moment(this.state.date).format("LL") : ""}
                />
              </div>

              <div className={`${formInputWrapper}`}>
                <label>Level of Certification</label>
                <input
                  type="text"
                  ref={level => (this.level = level)}
                  defaultValue={certification_level}
                  onChange={() => { }}
                />
              </div>

              <div className={`${formInputWrapper}`}>
                <label>Verification Status</label>
                <input
                  type="text"
                  ref={status => (this.status = status)}
                  defaultValue={verification_status}
                  onChange={() => { }}
                />
              </div>

              <button
                type="submit"
                disabled={this.state.name ? false : true}
                onClick={this._handleSubmit}
                className={`${btn} ${submitBtn} ${
                  this.state.name ? enabled : disabled
                  }`}
              >
                {is_edit ? "UPDATE" : "SUBMIT"}
              </button>
            </form>
          </section>

          <section style={{position: 'relative', margin: '20px auto', height: '80%'}}>
            {this.state.recentAttachmentPath ? null : (
              <figure className={`${addAttachmentWrapper}`}>
                <input
                  name="file"
                  ref={attachment => (this.attachment = attachment)}
                  onChange={this._handleChooseFile}
                  accept=".jpg, .jpeg, .png, .pdf"
                  multiple={false}
                  id="file-upload"
                  type="file"
                  name=""
                  id=""
                  hidden
                />
                <img
                  style={{ width: 100 }}
                  onClick={this._handleFileUpload}
                  src="/static/images/add_attachment.png"
                  alt=""
                />
                <small>Add Attachment</small>
              </figure>
            )}
            {this.state.attachment ? (
              this.state.attachment.split(".").reverse()[0] === "pdf" ? (
                <img src="/static/icons/pdf.png" alt="" />
              ) : (
                  <img
                    style={{maxWidth: 300}}
                    src={
                      this.state.recentAttachmentPath
                        ? this.state.recentAttachmentPath
                        : ""
                    }
                    alt=""
                  />
                )
            ) : null}
            {this.state.recentAttachmentPath ? (
              <button
                onClick={this._handleRemoveAttachment}
                className={`${btn} ${btn_remove}`}
              />
            ) : null}
          </section>
        </GridContainer>
      </div>
    );
  }
}

export default AddCertificate;

const blue = "#80cde9";
const pink = "#d484b7";

let btn_confirm = css({
  marginTop: 20,
  padding: "10px 20px",
  background: blue,
  color: "white",
  borderRadius: 5,
  transition: "250ms ease",
  ":hover": {
    background: "#8ae0ff"
  }
});

let showCalendar = css({
  opacity: "1 !important",
  "> div": {
    transform: "translateY(0%) !important"
  }
});

let displayCalendar = css({
  display: "flex !important",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  zIndex: 100
});

let calendarWrapper = css({
  display: "none",
  position: "absolute",
  top: "50%",
  left: "50%",
  width: "100%",
  height: "100%",
  opacity: 0,
  transform: "translate(-50%, -50%)",
  zIndex: 1,
  background: "rgba(0,0,0,.5)",
  boxShadow: "0 8px 32px rgba(0,0,0,.1)",
  transition: "300ms ease",
  "> div": {
    transition: "300ms ease",
    background: "white",
    boxShadow: "0 8px 32px rgba(0,0,0,.3)",
    transform: "translateY(-100%)"
  },
  "> button": {
    display: "block"
  }
});

let submitBtn = css({
  marginTop: 20,
  padding: "10px 20px",
  width: "100%",
  boxSizing: "border-box",
  color: "white"
});

let enabled = css({
  background: blue,
  cursor: "pointer !important",
  ":hover": {
    background: `${blue}98`
  }
});

let disabled = css({
  background: "#eee",
  cursor: "not-allowed !important"
});

let closeBtn = css({
  position: "absolute",
  top: 0,
  right: 0,
  background: "#ffffff00",
  padding: 21,
  cursor: "pointer",
  transition: "250ms ease",
  ":hover": {
    background: "#ffffff40"
  },
  "::before": {
    content: '""',
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%) rotate(45deg)",
    width: 2,
    height: 10,
    background: "white"
  },
  "::after": {
    content: '""',
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%) rotate(-45deg)",
    width: 2,
    height: 10,
    background: "white"
  }
});

let formWrapper = css({
  width: "100%",
  padding: "0 10px"
});

let formInputWrapper = css({
  padding: "10px 0px",
  "> label": {
    display: "block",
    color: blue,
    textAlign: 'left'
  },

  "> input": {
    borderStyle: "none",
    outline: "none",
    padding: "5px 10px",
    width: "100%",
    borderBottom: `1px solid #eee`,
    transition: "250ms ease",
    ":focus": {
      borderBottom: `1px solid ${blue}`
    }
  }
});

let addAttachmentWrapper = css({
  border: "2px dashed black",
  borderRadius: 10,
  opacity: 0.1,
  width: 140,
  margin: 0,
  display: "inline-block",
  cursor: "pointer",
  transition: "250ms ease",
  ":hover": {
    opacity: 0.2
  },
  "> small": {
    display: " block"
  }
});

let btn_remove = css({
  position: "absolute",
  top: 0,
  right: 10,
  transform: "translate(50%, -50%) rotate(45deg) scale(.9)",
  width: 30,
  height: 30,
  borderRadius: "100%",
  background: "black",
  border: "2px solid white",
  boxShadow: "0 15px 10px -5px rgba(0,0,0,.2)",
  transition: "250ms ease",
  ":hover": {
    transform: "translate(50%, -50%) rotate(45deg) scale(1)"
  },
  "::before": {
    content: '""',
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 2,
    height: 10,
    background: "white"
  },
  "::after": {
    content: '""',
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%) rotate(90deg)",
    width: 2,
    height: 10,
    background: "white"
  }
});

let btn = css({
  borderStyle: "none",
  outline: "none",
  cursor: "pointer",
  transition: "250ms ease"
});

let imgWrapper = css({
  // position: "relative",
  // textAlign: "center"
});

let attachmentWrapper = css({
  // margin: 0,
  // marginBottom: 10,
  // display: "flex",
  // justifyContent: "center",
  // alignItems: "center",
  // position: "relative",
  // "> div": {
  //   height: 200,
  //   margin: 0,
  //   marginBottom: 30,
  //   display: "flex",
  //   justifyContent: "center",
  //   alignItems: "center",

  //   "> img": {
  //     width: "auto",
  //     maxHeight: 200,
  //     boxShadow: "0 10px 10px rgba(0,0,0,.1)"
  //   }
  // }
});

let wrapper = css({
  // padding: 20,
  // margin: 0,
  // boxSizing: "border-box",
  // overflowY: "auto",
  // display: "flex",
  // flexDirection: "column",
  // alignItems: "center",
  // justifyContent: "center"
});

let outside = css({
  margin: -20,
  "> h1": {
    padding: "10px 20px",
    margin: 0,
    maxHeight: 42,
    fontSize: 20,
    textTransform: "uppercase",
    background: "#80cde9",
    color: "white",
    position: "relative"
  }
});
