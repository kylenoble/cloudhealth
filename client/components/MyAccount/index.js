import React from "react";
import Router, { withRouter } from "next/router";
//import AccountMenu from "./accountMenu";
import Profile from "../../components/Profile";
import CPassword from "../../components/Password/changePassword.js";
import AuthService from "../../utils/authService.js";
import Main from "./main";
import { UserContext } from "../../context";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Button from "../NewSkin/Components/Button";
import Card from '../NewSkin/Wrappers/Card'
import OptimumHealth from "../NewSkin/Components/OptimumHealth";
import TrainingsAndCertifications from "./trainingsAndCertifications";
const baseLink = "/account";

const auth = new AuthService();
class MyAccount extends React.Component {
  static contextType = UserContext;

  constructor(props) {
    super(props);
    this.user = {};
    this.state = {
      view: props.router.query.tab,
      nextView: this.props.nextView,
      currentTab: this.props.router.query.view || 'Account'
    };

    this._renderView = this._renderView.bind(this);
    this._updateData = this._updateData.bind(this);
    this._updateView = this._updateView.bind(this);
    this._defaultView = this._defaultView.bind(this);
    this._logout = this._logout.bind(this);
  }

  componentWillMount() {
    Router.push({
      pathname: '/account',
      query: { tab: 'Profile', view: this.state.currentTab }
    })
    this.user = this.context.user;
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ view: nextProps.router.query.tab });
  }

  _changeView = (route) => {
    if (this.context.needToChangePassword) return;

    this.setState({
      currentTab: route
    })
    Router.push({
      pathname: '/account',
      query: { tab: 'Profile', view: route }
    });
  }

  render() {
    const currentTab = this.state.currentTab;
    return <div className="wrap">
      {
        this.user.type === 'Patient' && <OptimumHealth />
      }

      <GridContainer center styles={{ marginTop: 20 }}>
        <GridContainer columnSize={'1fr 5fr'} styles={{ padding: 20, alignItems: 'start' }} gap={30}>
          <GridContainer gap={20}>
            <Button
              styles={{ minWidth: 250, maxWidth: 'fit-content' }}
              type={this.context.needToChangePassword ? 'disabled' : currentTab === 'Account' ? 'blue' : ''}
            >
              <button id="account-btn" onClick={() => this._changeView('Account')}>Account</button>
            </Button>

            {
              this.user.type === 'Patient' ?
                <Button
                  styles={{ minWidth: 250, maxWidth: 'fit-content' }}
                  type={this.context.needToChangePassword ? 'disabled' : currentTab === 'Personal' ? 'blue' : ''}
                >
                  <button id="personal-btn" onClick={() => this._changeView('Personal')}>Personal Information</button>
                </Button> :

                <Button
                  styles={{ minWidth: 250, maxWidth: 'fit-content' }}
                  type={this.context.needToChangePassword ? 'disabled' : currentTab === 'Trainings' ? 'blue' : ''}
                >
                  <button id="personal-btn" onClick={() => this._changeView('Trainings')}>Trainings and Certifications</button>
                </Button>
            }

            <Button
              styles={{ minWidth: 250, maxWidth: 'fit-content' }}
              type={currentTab === 'Password' || this.context.needToChangePassword ? 'blue' : ''}
            >
              <button id="personal-btn" onClick={() => this._changeView('Password')}>Change Password</button>
            </Button>
          </GridContainer>

          <Card>
            {this._renderView()}
          </Card>
        </GridContainer>
      </GridContainer>
    </div>;
  }

  _updateView(view) {
    this.setState({
      view: view
    });
  }

  _defaultView() {
    this.setState({
      view: "menu"
    });
  }

  // MANAGE RENDERING
  _renderView() {
    // check what to view
    const view = this.context.needToChangePassword ? 'Password' : this.state.view;
    const nextView = this.state.nextView;
    if (view === "Profile") {
      return (
        <Profile
          user={this.user}
          back={this._defaultView}
          setActiveView={this.props.setActiveView}
          nextView={nextView}
          previousView={this.props.previousView}
          setPreviousView={this.props.setPreviousView}
        />
      );
    } else if (view === "Password") {
      return (
        <CPassword
          location="profile"
          logout={() => this._logout()}
          back={this._defaultView}
        />
      );
    } else if (view === "forced") {
      return (
        <CPassword
          location="profile"
          logout={() => this._logout()}
          message="You are required to change password."
        />
      );
    }
  }

  _updateData(sec) {
    console.log("changing of tab happen here", sec);
    this.setState({
      view: sec
    });
  }

  _logout() {
    auth
      .logout()
      .then(() => {
        Router.push("/login");
      })
      .catch(e => console.log(e)); // you would show/hide error messages with component state here
  }
}

export default withRouter(MyAccount);
