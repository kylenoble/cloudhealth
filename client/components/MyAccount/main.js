import React, { Component } from 'react';
import { css } from 'glamor'
import ProfileData from './profileData'
import TrainingsAndCertifications from './trainingsAndCertifications'

class Main extends Component {
  state = {
    applyWrapper: false
  }

  render() {
    const { user, updateData } = this.props
    return (
      <section className={`${wrapper}`}>
        <ProfileData user={user} updateData={updateData} applyWrapper={this.state.applyWrapper} />
        {
          user.type === 'Doctor' ? <TrainingsAndCertifications user={user} applyWrapper={this.state.applyWrapper} /> : null
        }

      </section>
    );
  }
}

export default Main;

let wrapper = css({
  paddingTop: 80,
  display: 'flex',
  width: '100%',
  flexDirection: 'column'
})