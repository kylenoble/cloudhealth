import { css } from "glamor";
import Router, { withRouter } from "next/router";

const baseLink = "/account";

export default class extends React.Component {
  constructor(props) {
    super(props);

    this._updateData = this._updateData.bind(this);
  }

  _showEditItmes() {
    let editItem = [
      {
        label: "A. Edit Profile",
        group: "Profile"
      },
      {
        label: "B. Change Password",
        group: "Password"
      }
    ];
    let html = [];
    editItem.forEach((edit_item, index) => {
      html.push(
        <div key={index} className={item}>
          <span
            className={label}
            id={edit_item.group}
            onClick={this._updateData}
          >
            {edit_item.label}
          </span>
        </div>
      );
    });

    return html;
  }

  _updateData(e) {
    Router.push(`${baseLink}?menu=account&?tab=${e.target.id}`);
  }

  render() {
    return <div className={divWrap}>{this._showEditItmes()}</div>;
  }
}

let divWrap = css({
  display: "flex",
  flex: "1 0 100%",
  height: 300,
  flexDirection: "column",
  justifyContent: "center",
  alignItems: "center"
});

const item = css({
  padding: "10px",
  width: 200,
  display: "flex",
  background: "pink",
  borderRadius: "6px",
  width: "39%",
  margin: "2px",
  justifyContent: "center"
});

const label = css({
  textAlign: "left",
  cursor: "pointer",
  padding: "10px",
  borderRadius: "5px",
  ":hover": {
    backgroundColor: "#ED81B9",
    color: "white"
  }
});

const btn = css({
  textAlign: "center",
  padding: "5px",
  fontSize: "small",
  background: "#80cde9",
  color: "#fff",
  width: "30%",
  border: "1px solid",
  cursor: "pointer"
});
const Incomplete = css({
  textAlign: "center",
  padding: "5px",
  fontSize: "small",
  background: "Orange", //#eec5e2
  color: "#fff",
  width: "30%",
  border: "1px solid",
  cursor: "pointer"
});
