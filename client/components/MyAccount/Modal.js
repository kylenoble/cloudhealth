import Modal from "react-modal";
import { css } from "glamor";

import React from "react";

const AccountModal = props => {
  const closeModal = () => {
    props.closeModal("faqModalIsOpen");
  };

  return (
    <Modal
      isOpen={props.modalIsOpen}
      //   onAfterOpen={this.afterOpenModal}
      onRequestClose={closeModal}
      shouldCloseOnOverlayClick={false}
      style={customStyles}
      ariaHideApp={false}
    >
      {props.children}
    </Modal>
  );
};

export default AccountModal;

const customStyles = {
  zIndex: "109 !important",
  border: "none !important",
  content: {
    border: "none !important",
    maxWidth: "90%",
    minWidth: "70%",
    maxHeight: "85vh",
    top: "55%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    boxShadow: "0px 8px 30px rgba(0,0,0,.2)"
  }
};
