import React, { Component } from "react";
import { css } from "glamor";
import Router from "next/router";
import currentDomain from "../../utils/domain";
import ContainedWrapper from "../NewSkin/Wrappers/ContainedWrapper";

const baseLink = "/account";

class ProfileData extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  checkData = data => {
    return data ? data : "";
  };

  _Route = e => {
    Router.push(`${baseLink}?menu=account&tab=${e.target.id}`);
  };

  _updateData = e => {
    this.props.updateData(e.target.id);
  };

  render() {
    const { avatar, name, email, expertise, company, type } = this.props.user;
    return (
      <ContainedWrapper
        headerAndFooterHeight={256}
        styles={{ marginTop: 0 }}
        center={false}
      >
        <article className={`${this.props.applyWrapper ? wrapper : ""}`}>
          <label className={`${sectionLabel}`}>My Profile Details</label>
          <section className={`${dataWrapper}`}>
            <article className={`${editWrapper}`}>
              <figure className={`${avatarWrapper}`}>
                <img src={`/static/avatar/${avatar}`} alt="" />
              </figure>
              <button id="Profile" className={btn} onClick={this._Route}>
                Edit Profile
              </button>
              <button id="Password" className={btn} onClick={this._Route}>
                Change Password
              </button>
            </article>

            <main className={`${mainWrapper}`}>
              <p>
                <span>Name:</span> {this.checkData(name)}
              </p>
              <p>
                <span>Email:</span> {this.checkData(email)}
              </p>
              <p>
                <span>Company:</span> {this.checkData(company)}
              </p>
              {type === "Doctor" ? (
                <p>
                  <span>Expertise:</span> {this.checkData(expertise)}
                </p>
              ) : null}
            </main>
          </section>
        </article>
      </ContainedWrapper>
    );
  }
}

export default ProfileData;

const pink = "#d484b7";
const blue = "#80cde9";
const size = 90;

let wrapper = css({
  margin: 40,
  padding: 20,
  boxShadow: "0 8px 20px rgba(0,0,0,.050)"
});

let btn = css({
  height: 30,
  marginRight: 20,
  padding: "2px 20px",
  borderStyle: "none",
  outline: "none",
  cursor: "pointer",
  borderRadius: 3,
  background: "#364563",
  color: "#ffffff85",
  transition: "250ms ease",
  boxShadow: "0 1px 1px rgba(0,0,0,0)",
  ":hover": {
    boxShadow: "0 8px 10px rgba(0,0,0,.1)",
    color: "white"
  }
});

let mainWrapper = css({
  paddingLeft: size + 20,
  "> p": {
    paddingBottom: 5,
    display: "flex",
    "> span": {
      color: blue,
      width: 85,
      display: "block"
    }
  }
});

let editWrapper = css({
  display: "flex",
  marginBottom: 20,
  alignItems: "center"
});

let avatarWrapper = css({
  width: size,
  height: size,
  borderRadius: "100%",
  overflow: "hidden",
  position: "relative",
  margin: 0,
  marginRight: 20,
  "> img": {
    width: "100%",
    position: "absolute",
    top: "50%",
    left: 0,
    transform: "translateY(-50%)"
  }
});

let dataWrapper = css({
  padding: 30,
  paddingLeft: 100
});

let sectionLabel = css({
  padding: "10px 30px 10px 100px",
  background: pink,
  color: "white",
  textTransform: "uppercase"
});
