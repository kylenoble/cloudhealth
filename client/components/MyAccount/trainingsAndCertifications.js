import React, { Component } from "react";
import { css } from "glamor";
import CertificateTable from "./certificateTable";
import request from "axios";
import currentDomain from "../../utils/domain";
import { confirmAlert } from "react-confirm-alert";

// API
const GET_CERTIFICATE_URL = `${currentDomain}/api/certificate/`;
const GET_ONE_CERTIFICATE_URL = `${currentDomain}/api/certificate/`;
const DELETE_CERTIFICATE_URL = `${currentDomain}/api/certificate/delete/`;

// imported components
import AddCertificate from "./addCertificate";
import AccountModal from "./Modal";
import Colors from "../NewSkin/Colors";
import Button from "../NewSkin/Components/Button";
import GridContainer from "../NewSkin/Wrappers/GridContainer";

class TrainingsAndCertifications extends Component {
  state = {
    certificates: [],
    addCertificateModalIsOpen: false,
    is_edit: false,
    selectedCertificate: ""
  };

  componentDidMount() {
    this._getCertificates();
  }

  _getCertificates = () => {
    request
      .get(GET_CERTIFICATE_URL)
      .then(res => {
        this.setState({
          certificates: res.data
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  _openModal = modal => {
    if (modal === "addCertificateModalIsOpen") {
      this.setState({
        is_edit: false,
        [modal]: true
      });
    }
  };

  _closeModal = modal => {
    this.setState({
      [modal]: false
    });
  };

  _editCertificate = id => {
    request.get(GET_ONE_CERTIFICATE_URL + id).then(res => {
      this.setState({
        selectedCertificate: res.data,
        addCertificateModalIsOpen: true,
        is_edit: true
      });
    });
  };

  _cancel = id => {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: "Do you want to remove this certificate?", // Message dialog
      //  childrenElement: '',       // Custom UI or Component
      confirmLabel: "Yes, remove this certificate.", // Text button confirm
      cancelLabel: "I am not sure", // Text button cancel
      //onCancel: () => this._setNotifiationAsRead(id),      // Action after Cancel
      customUI: ({ onClose }) => {
        return (
          <div className={customContent}>
            <p>Do you want to remove this certificate?</p>
            <GridContainer columnSize={'1fr 1fr'} styles={{marginLeft: 'auto'}}>
              <Button styles={{margin: 'auto'}}>
                <button onClick={() => onClose()}>NO</button>
              </Button>
              <Button type="blue" styles={{margin: 'auto'}}>
                <button
                  onClick={() => {
                    this._deleteCertificate(id);
                    onClose();
                  }}
                >
                  YES
                </button>
              </Button>
            </GridContainer>
          </div>
        );
      }
    });
  };

  _deleteCertificate = id => {
    request.put(`${DELETE_CERTIFICATE_URL}${id}`).then(res => {
      this._getCertificates();
    });
  };

  render() {
    return (
      <GridContainer fullWidth>
        <Button styles={{ marginBottom: 20 }}>
          <button
            id="upload"
            onClick={() => this._openModal("addCertificateModalIsOpen")}
          >
            <img style={{ width: 20, marginRight: 10 }} src="/static/images/upload.png" alt="" />
            Upload Certificate
          </button>
        </Button>

        <CertificateTable
          data={this.state.certificates}
          edit={this._editCertificate}
          delete={this._cancel}
        />

        <AccountModal
          modalIsOpen={this.state.addCertificateModalIsOpen}
          closeModal={this._closeModal}
        >
          <AddCertificate
            editValues={
              this.state.is_edit ? this.state.selectedCertificate : []
            }
            is_edit={this.state.is_edit}
            user={this.props.user}
            closeModal={this._closeModal}
            getCertificates={this._getCertificates}
          />
        </AccountModal>
      </GridContainer>
    );
  }
}

export default TrainingsAndCertifications;

const pink = "#d484b7";

let customContent = css({
  "> p": {
    marginBottom: 20
  },
  "> div": {
    textAlign: "right",
    "> button": {
      borderStyle: "none",
      outline: "none",
      padding: "8px 15px",
      background: "white",
      border: `1px solid ${Colors.pink}`,
      borderRadius: 8,
      margin: 5,
      cursor: "pointer",
      color: Colors.pink,
      fontSize: ".9em",
      ":hover": {
        color: Colors.skyblue,
        borderColor: Colors.skyblue
      }
    },
    "> button:nth-child(1)": {
      border: "none !important"
    }
  }
});

let editWrapper = css({
  display: "flex",
  alignItems: "center",
  padding: "0px 30px",
  marginTop: 30
});

let btn = css({
  height: 30,
  marginRight: 20,
  padding: "2px 20px",
  borderStyle: "none",
  outline: "none",
  cursor: "pointer",
  borderRadius: 3,
  background: "#364563",
  color: "#ffffff85",
  transition: "250ms ease",
  boxShadow: "0 1px 1px rgba(0,0,0,0)",
  ":hover": {
    boxShadow: "0 8px 10px rgba(0,0,0,.1)",
    color: "white"
  }
});

let tableWrapper = css({
  padding: 30,
  position: "relative",
  zIndex: 0
});

let itemChildStyle = css({
  display: "flex",
  boxSizing: "border-box",
  alignItems: "center",
  justifyContent: "center",
  fontSize: 13
});

let wrapper = css({
  margin: 40,
  padding: 20,
  boxShadow: "0 8px 20px rgba(0,0,0,.050)"
});

let sectionLabel = css({
  padding: "10px 30px 10px 100px",
  background: pink,
  color: "white",
  textTransform: "uppercase"
});
