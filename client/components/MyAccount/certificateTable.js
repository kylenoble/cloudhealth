import React from "react";
import ReactTable from "react-table";
import { css } from "glamor";
import moment from "moment-timezone";
import currentDomain from "../../utils/domain";
import NoData from '../NewSkin/Components/NoData';
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import { ReactTableStyle } from "../NewSkin/Styles";

export default function certificateTable(props) {
  const _accessor = accessor => {
    let acc = "";
    const a = accessor.toLowerCase().split(" ");

    for (const ac of a) {
      acc += `${ac}`;
    }
    return acc.trim();
  };

  const headers = [
    "Certificate",
    "Name of Training",
    "Date Completed",
    "Level of Certification",
    "Verification Status"
  ];

  let data = [];
  if (props.data && props.data.length > 0) {
    data = props.data;
  }
  let columns = [];

  if (data.length > 0) {
    data.filter(() => {
      columns = [
        {
          id: 1,
          Header: headers[0],
          accessor: `${_accessor(headers[0])}`,
          Cell: item => (
            <div style={{ padding: 5 }}>
              <div className={`${itemChildStyle} itemChildStyle`}>
                <a
                  href={
                    item.original.attachment
                      ? `/static/certificates/${item.original.attachment}`
                      : null
                  }
                  target={item.original.attachment ? "_blank" : ""}
                  rel="noopener noreferrer"
                >
                  {item.original.attachment ? (
                    item.original.attachment.split(".").reverse()[0] ===
                      "pdf" ? (
                        <img
                          style={{ width: 65 }}
                          src="/static/icons/pdf.png"
                          alt=""
                        />
                      ) : (
                        <img
                          style={{ width: 100 }}
                          src={`/static/certificates/${item.original.attachment}`}
                          alt=""
                        />
                      )
                  ) : (
                      <img
                        style={{ width: 25, opacity: 0.2 }}
                        src="/static/images/no-attachment.svg"
                        alt=""
                      />
                    )}
                </a>
              </div>
            </div>
          )
        },
        {
          id: 2,
          Header: headers[1],
          accessor: `${_accessor(headers[1])}`,
          Cell: item => (
            <div style={{ padding: 5 }}>
              <div className={`${itemChildStyle} itemChildStyle`}>
                {item.original.training_name}
              </div>
            </div>
          )
        },
        {
          id: 3,
          Header: headers[2],
          accessor: `${_accessor(headers[2])}`,
          Cell: item => (
            <div style={{ padding: 5 }}>
              {
                <div className={itemChildStyle} style={{ textAlign: "center" }}>
                  {moment(item.original.date_completed).format("LL")}
                </div>
              }
            </div>
          )
        },
        {
          id: 4,
          Header: headers[3],
          accessor: `${_accessor(headers[3])}`,
          Cell: item => (
            <div style={{ padding: 5 }}>
              {
                <div className={itemChildStyle} style={{ textAlign: "center" }}>
                  {item.original.certification_level}
                </div>
              }
            </div>
          )
        },
        {
          id: 5,
          Header: headers[4],
          accessor: "status",
          Cell: item => (
            <div className={itemChildStyle} style={{ textAlign: "center" }}>
              {item.original.verification_status}
            </div>
          )
        },
        {
          id: 6,
          Header: "Action",
          accessor: "action",
          Cell: item => (
            <div className={itemChildStyle} style={{ textAlign: "center" }}>
              <button
                className={`${btn}`}
                onClick={() => props.edit(item.original.id)}
              >
                <img src="/static/images/edit.svg" />
              </button>
              <button
                className={`${btn}`}
                onClick={() => props.delete(item.original.id)}
              >
                <img src="/static/images/trash.svg" />
              </button>
            </div>
          )
        }
      ];
    });
  }

  return (
    <GridContainer fullWidth classes={[ReactTableStyle]} styles={{ maxWidth: '100%' }}>
      {
        data.length > 0 ?
          <ReactTable
            data={data}
            columns={columns}
            defaultPageSize={5}
            minRows={0}
            className={`-striped -highlight react-table`}
            width={90}
          /> : <NoData text="You have no Trainings and Certifications" />
      }
    </GridContainer>
  );
}

let btn = css({
  padding: 5,
  margin: 10,
  borderStyle: "none",
  outline: "none",
  background: "none",
  cursor: "pointer",
  transition: "200ms ease",
  opacity: 0.6,
  filter: "grayscale(100%)",
  ":hover": {
    opacity: 1,
    filter: "grayscale(0%)"
  }
});

let itemChildStyle = css({
  display: "flex",
  boxSizing: "border-box",
  alignItems: "center",
  justifyContent: "center",
  wordWrap: "break-word"
});

let reactTable = css({
  "> div": {
    "> div": {
      "> div": {
        "> div": {
          "> div.rt-td": {
            display: "flex",
            alignItems: "center",
            justifyContent: "center"
          }
        }
      }
    }
  }
});
