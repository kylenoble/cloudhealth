import moment from 'moment';
import React, { Component } from 'react';
import cx from 'classnames';
import range from 'lodash/range';
import chunk from 'lodash/chunk';
import { css } from 'glamor'

const Day = ({ i, w, d, className, is_profile, ...props }) => {
  const prevMonth = w === 0 && i > 7;
  const nextMonth = w >= 4 && i <= 14;
  const cls = cx({
    'prev-month': prevMonth,
    'next-month': nextMonth,
    'current-day': !prevMonth && !nextMonth && i === d
  });

  if(is_profile){
    return !nextMonth && <td className={cls} {...props}>{i}</td>;
  }
  return <td className={cls} {...props}>{i}</td>;
};

export default class Calendar extends Component {
  selectDate = (i, w) => {
    const prevMonth = w === 0 && i > 7;
    const nextMonth = w >= 4 && i <= 14;
    const m = this.props.moment;

    if (prevMonth) m.subtract(1, 'month');
    if (nextMonth) m.add(1, 'month');

    m.date(i);

    this.props.onChange(m);
  };

  prevMonth = e => {
    e.preventDefault();
    this.props.onChange(this.props.moment.subtract(1, 'month'));
  };

  nextMonth = e => {
    e.preventDefault();
    this.props.onChange(this.props.moment.add(1, 'month'));
  };

  render() {
    const m = this.props.moment;
    const d = m.date();
    const d1 = m.clone().subtract(1, 'month').endOf('month').date();
    const d2 = m.clone().date(1).day();
    const d3 = m.clone().endOf('month').date();
    const days = [].concat(
      range(d1 - d2 + 1, d1 + 1),
      range(1, d3 + 1),
      range(1, 42 - d3 - d2 + 1)
    );
    const weeks = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

    const is_profile = this.props.is_profile || false

    return (
      <div className={cx('m-calendar', this.props.className)} style={{height: is_profile ? 'unset' : ''}}>
        <div className={`toolbar ${toolbar}`}>
          <button type="button" className="prev-month" onClick={this.prevMonth}>
            <i className={this.props.prevMonthIcon} />
          </button>

          {
            is_profile ?
              <div className={`${dropdownWrapper}`}>
                <MonthDropdown current={m.format('MMMM')} onChange={this.onChange} />
                <YearDropdown current={m.format('YYYY')} onChange={this.onChange} />
              </div> :
              <span>
                <span className="current-date">{m.format('MMMM')} </span>
                <span className="current-date">{m.format('YYYY')}</span>
              </span>
          }

          <button type="button" className={`next-month ${nextBtn}`} onClick={this.nextMonth}>
            <i className={this.props.nextMonthIcon} />
          </button>
        </div>

        <table>
          <thead>
            <tr>
              {weeks.map((w, i) => <td key={i}>{w}</td>)}
            </tr>
          </thead>

          <tbody>
            {chunk(days, 7).map((row, w) =>
              <tr key={w}>
                {row.map(i =>
                  <Day
                    key={i}
                    i={i}
                    d={d}
                    w={w}
                    is_profile={is_profile}
                    onClick={() => this.selectDate(i, w)}
                  />
                )}
              </tr>
            )}
          </tbody>
        </table>
      </div>
    );
  }

  onChange = e => {
    if (e.target.id == 'month') {
      this.props.onChange(this.props.moment.month(e.target.value))
    } else {
      this.props.onChange(this.props.moment.year(e.target.value))
    }
  }
}

const MonthDropdown = props => {
  const months = 'January February March April May June July August September October November December'

  const alteredMonths = months.split(' ').map(m => {
    let mon = ''
    props.current == m ? mon = { value: m, selected: true } : mon = { value: m, selected: false }
    return mon
  })

  return (
    <div className={`${dropdown}`}>
      <select
        name="month"
        id="month"
        value={props.current}
        onChange={(e) => props.onChange(e)}
      >
        {
          alteredMonths.map((month, i) => {

            return <option
              key={i}
              // selected={month.selected}
              value={month.value}
            >{month.value}</option>
          })
        }
      </select>
    </div>
  )
}

const YearDropdown = props => {
  const currentYear = new Date().getFullYear()
  const years = []

  for (let i = 1900; i <= currentYear; i++) {
    years.push(i)
  }

  const alteredYear = years.map(y => {
    let year = ''
    props.current == y ? year = { value: y, selected: true } : year = { value: y, selected: false }
    return year
  })

  return (
    <div className={`${dropdown}`}>
      <select
        name="year"
        id="year"
        value={props.current}
        onChange={(e) => props.onChange(e)}
      >
        {
          alteredYear.map(year => {
            return <option
              key={year.value}
              value={year.value}
            // selected={year.selected}
            >{year.value}</option>
          })
        }
      </select>
    </div>
  )
}

let nextBtn = css({
  justifySelf: 'end',
})

let toolbar = css({
  display: 'grid',
  gridTemplateColumns: '1fr 3fr 1fr',
})

let dropdownWrapper = css({
  display: 'grid',
  gridTemplateColumns: '2fr 1fr'
})

let dropdown = css({
  // padding: 10,
  '> label': {
    display: 'block',
    textTransform: 'uppercase',
    color: '#ccc'
  },
  '> select': {
    border: ' none',
    outline: ' none',
    width: '100%',
    boxSizing: 'border-box'
  }
})