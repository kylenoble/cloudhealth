import React from "react";
import { css } from "glamor";
import Router, { withRouter } from "next/router";

const baseLink = "/account";

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};

    this._showViewTitle = this._showViewTitle.bind(this);
    this._updateCurrentView = this._updateCurrentView.bind(this);
  }

  render() {
    return <div className={switcherStyle}>{this._showViewTitle()}</div>;
  }

  _showViewTitle() {
    const titles = ["Account", "Personal"];
    let html = [];
    titles.forEach((currentItem, index) => {
      html.push(
        <h1
          key={index}
          onClick={this._updateCurrentView}
          style={
            this.props.nextView === currentItem
              ? { ...styles.dashItem, ...styles.dashItemActive }
              : styles.dashItem
          }
        >
          {currentItem}
        </h1>
      );
    });

    return html;
  }

  _updateCurrentView(e) {
    Router.push(
      `${baseLink}?menu=account&tab=Profile&view=${e.target.innerText}`
    );
    // e.preventDefault();
    // this.props.activePressed(e.target.innerText);
  }
}

const switcherStyle = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "space-around"
});

const container = css({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "space-between",
  borderBottom: "1px solid #E1E8EE",
  flexWrap: "wrap",
  margin: "0",
  marginTop: "20px",
  maxWidth: "40em",
  "@media(max-width: 700px)": {
    marginTop: "10px"
  }
});

const styles = {
  dashItem: {
    fontFamily: "Roboto-Medium",
    fontSize: "14px",
    color: "#BDC7CF",
    letterSpacing: 0,
    lineHeight: "14px",
    padding: "10px 10px",
    margin: "-1px 15px",
    cursor: "pointer",
    textAlign: "center"
  },
  dashItemActive: {
    color: "#358ED7",
    borderBottom: "2px solid #358ED7"
  }
};
