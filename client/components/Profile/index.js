import React from "react";
import { css } from "glamor";
import Router, { withRouter } from "next/router";

import EmailInfo from "../EmailInfo";
import PersonalInfo from "../PersonalInfo";
import ProfileSwitcher from "./profileSwitcher";
import CPassword from '../Password/changePassword';
import AuthService from "../../utils/authService.js";
import TrainingsAndCertifications from "../MyAccount/trainingsAndCertifications";

const auth = new AuthService();
const baseLink = "/account";

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.user = this.props.user;
    this.state = {
      nextView: this.props.router.query.view || "Account",
      previousView: this.props.previousView
    };

    this._updateView = this._updateView.bind(this);
    this._updateCurrentView = this._updateCurrentView.bind(this);
    this._returnProfileSwitcher = this._returnProfileSwitcher.bind(this);
    this._goTo = this._goTo.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ nextView: nextProps.router.query.view || "Account" });
  }

  _returnProfileSwitcher() {
    return (
      <ProfileSwitcher
        nextView={this.state.nextView}
        activePressed={this._updateView}
        user={this.user}
      />
    );
  }

  _updateView(view) {
    this._updateCurrentView(view);
  }

  _updateCurrentView(view) {
    this.setState({
      nextView: view
    });
  }

  _logout = () => {
    auth
      .logout()
      .then(() => {
        Router.push("/login");
      })
      .catch(e => console.log(e)); // you would show/hide error messages with component state here
  }

  _returnStep() {
    if (this.state.nextView === "Account" || !this.state.nextView) {
      return (
        <EmailInfo
          user={this.user}
          onClick={this._updateStep}
          location="profile"
        />
      );
    } else if (this.state.nextView === "Personal") {
      return (
        <PersonalInfo
          user={this.user}
          onClick={this._updateStep}
          location="profile"
        />
      );
    } else if (this.state.nextView === "Password") {
      return (
        <CPassword
          location="profile"
          logout={() => this._logout()}
        />
      )
    } else if (this.state.nextView === 'Trainings') {
      return <TrainingsAndCertifications user={this.user} />
    }
  }

  _updateStep(step) {
    console.log(`${step} updated`);
  }

  _goTo(lnk) {
    Router.push(`${baseLink}?menu=${lnk}`);
  }

  render() {
    return (
      <div className={container}>
        {this._returnStep()}
      </div>
    );
  }
}

export default withRouter(Profile);

const container = css({
  fontFamily: '"Roboto",sans-serif',
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 100%",
  alignItems: "center",
  justifyContent: "center"
});