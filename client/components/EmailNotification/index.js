import BookAppNotifService from "../../utils/book_Appointment_Notification_Service";
import moment from "moment-timezone";

const bookAppNotifService = new BookAppNotifService();

const date = moment(new Date()).format("lll")

const EmailNotification = props => {

  const _sendAppNotification = () => {
    const { name, id, title, body_for_app } = props.app
    const sender = { type: "Patient", name: "System", id }
    const patient = { type: "Patient", name, id, date, patientName: name, subscription: true };
    const message = body_for_app;
    const subject = title;
    const notificationDetails = { sender, patient, message, subject }

    _sendSystemNotification(notificationDetails);
  };

  const _sendSystemNotification = notificationDetails => {
    bookAppNotifService
      .create(notificationDetails)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
  };

  const renderMessage = () => {
    const { to, body, title, subject } = props.email;

    const content = `
      <p style="font-size: 16px;" >Please see below notification for your account:</p >
      <div style="border: 1px solid #ccc; padding: 20px; border-radius: 5px; background: #eee1; box-shadow: 0 8px 32px rgba(0,0,0,.050)">
        <p style="font-size: 16px; color: #9e9e9e; margin: 0">${date}</p>

        <h5 style="font-size: 1.64rem; font-weight: 400; margin: 0.82rem 0 0.656rem 0">${title}</h5>

        ${body}
      </div>
      <p style="font-size: 16px">Please log-in to your account for further details.</p>
      <a title="Login to CloudHealthAsia" href="https://cloudhealthasia.com/login" target="_blank" style="text-decoration: none; padding: 10px 20px; background: #81cce6; color: #fff; border-radius: 5px; margin-top: 20px; display: block; width: 200px; font-weight: 500;">Login to CloudHealthAsia</a>
    `

    const emailDetails = { to, subject, content, text: 'This is the notification of your Subscription' }

    _sendEmailNotification(emailDetails)
  }

  const _sendEmailNotification = emailDetails => {

    bookAppNotifService
      .sendBookAppEmailNotification(emailDetails)
      .then(res => {
        console.log('%c Sending email notification success!', 'color: green', res);
      })
      .catch(err => {
        console.log(err);
      });

    _sendAppNotification()
  };

  return renderMessage()
}

export default EmailNotification