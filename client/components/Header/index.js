/* eslint-disable no-lone-blocks */
/* eslint-disable react/sort-comp */
/* eslint-disable template-curly-spacing */
/* eslint-disable no-undef */
import React from "react";
import NProgress from "nprogress";
import Router from "next/router";
import { css } from "glamor";
import currentDomain from "../../utils/domain";

// From New Skin
import NewHeader from "../NewSkin/NewHeader";
import Loading from "../NewSkin/Components/Loading";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      pageLoaded: true
    };

    this._startloader = this._startloader.bind(this);
    this._switchDashboard = this._switchDashboard.bind(this);

    Router.onRouteChangeStart = () => {
      console.log(
        "%c Loading Started...",
        "color: orange",
        NProgress.start().status
      );
      NProgress.start();
      this.setState({ loading: true, pageLoaded: false });
    };

    Router.onRouteChangeComplete = () => {
      NProgress.done();

      // setTimeout(() => {
        this.setState({ loading: false, pageLoaded: true });
      // }, 500);

      console.log("%c Loading Done...", "color: green");
    };

    Router.onRouteChangeError = () => {
      NProgress.done();
      this.setState({ loading: false, pageLoaded: true });
    };
  }

  componentDidMount() {
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      flexibility(document.body);
    }
  }

  _startloader() {
    this.props.startLoader();
  }

  supportsFlexBox() {
    const test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  _switchDashboard() {
    const token = this.props.user.admin_access_token;
    const time = Math.floor(new Date().getTime() / 1000);
    const x = btoa(`${btoa(token)}&&1c${time}`);
    const url = `https://admin.cloudhealthasia.com/login/fromAdminDashboard/${x}`;
    window.location = url;
  }
  render() {
    {
      /* OLD HEADER */
    }
    {
      /* <Header
      pathname={this.props.pathname}
      user={this.props.user}
      loading={this.state.loading}
      switchDashboard={this._switchDashboard}
      logoutPressed={this.props.logoutPressed}
    /> */
    }
    return (
      <React.Fragment>
        {this.state.pageLoaded ? null : <Loading path={this.props.pathname} />}
        <NewHeader
          pathname={this.props.pathname}
          user={this.props.user}
          loading={this.state.loading}
          switchDashboard={this._switchDashboard}
          logoutPressed={this.props.logoutPressed}
          timeout={this.props.timeout}
        />
      </React.Fragment>
    );
  }

  _renderLink() {
    // if (this.props.pathname === 'index') {
    //   if (this.props.user.name) {
    //     return (
    //       <span onClick={() => { this._startloader() }}>
    //         <Link prefetch href='/dashboard'>
    //           <a style={styles.link}>Go To Dashboard</a>
    //         </Link>
    //       </span>
    //     )
    //   } else {
    //     return (
    //       <div>
    //         <Link prefetch href='/login'>
    //           <a style={styles.link}>Login</a>
    //         </Link>
    //       </div>
    //     )
    //   }
    // } else if (this.props.pathname === 'login') {
    //   return (
    //     <Link prefetch href='/register'>
    //       <a style={styles.link}>Register</a>
    //     </Link>
    //   )
    // } else if (this.props.pathname === 'register') {
    //   if (this.props.step === 'login' || !this.props.user) {
    //     if (this.props.logged) {
    //       return (
    //         <a style={styles.link} href="" onClick={this.props.logoutPressed}>Logout</a>
    //       )
    //     } else {
    //       return (
    //         <Link prefetch href='/login'>
    //           <a style={styles.link}>Login</a>
    //         </Link>
    //       )
    //     }

    //   } else {
    //     return (
    //       <Link prefetch href='/dashboard'>
    //         <a style={styles.link}>Go To Dashboard</a>
    //       </Link>
    //     )
    //   }
    // }
    // else {
    //   let switch_link = ''
    //   if (this.props.user && this.props.user.is_admin) {
    //     switch_link = <div className="dropdown-content" style={{ height: '4em', top: '70px', right: '0em', width: '18em' }}>
    //       <a style={{ cursor: 'pointer' }} onClick={this._switchDashboard}>Switch to Admin Dashboard</a>
    //     </div>
    //   }
    //   return (
    //     <div className="dropdown-box">
    //       <span className={dropdownLink}>{this.props.user.name}</span>
    //       <div className="dropdown-content" style={{ height: '4em', right: '0em', width: '18em' }}>
    //         <a href="" onClick={this.props.logoutPressed}>Logout</a><br />
    //       </div>
    //       {switch_link}
    //     </div>
    //   )
    // }
    if (this.props.pathname !== "index") {
      const avatarUrl = this.props.user.avatar
        ? `/static/avatar/${this.props.user.avatar}`
        : `/static/icons/user.svg`;
      const user = this.props.user;
      let switch_link = "";
      if (user && user.is_admin && user.status === 1) {
        switch_link = (
          <div
            className={`${dropdownContent} dropdown-content`}
            style={{ right: 0, width: "100%" }}
          >
            <a style={{ cursor: "pointer" }} onClick={this._switchDashboard}>
              <img
                className={`${linkIcon}`}
                src="/static/icons/switch.svg"
                alt=""
              />
              Switch to Admin Dashboard
            </a>
          </div>
        );
      }
      return (
        <div className="dropdown-box">
          <div className={`${userAvatarNameWrapper}`}>
            <figure className={`${avatarWrapper}`}>
              <img src={avatarUrl} alt="" />
            </figure>
            <span className={dropdownLink}>{this.props.user.name}</span>
          </div>

          <div className={`dropdown-content ${dropContent}`}>
            <div className={`${dropdownContentWrapper}`}>
              <div
                className={`${dropdownContent}`}
                style={{ right: 0, width: "100%" }}
              >
                <a href="" onClick={this.props.logoutPressed}>
                  <img
                    className={`${linkIcon}`}
                    src="/static/icons/logout.svg"
                    alt=""
                  />
                  Logout
                </a>
                <br />
              </div>
              {switch_link}
            </div>
          </div>
        </div>
      );
    }
  }

  _selectLogo() {
    if (this.props.pathname === "index") {
      return "/static/images/logo-dark.svg";
    }
    return "/static/images/logo-dark.svg";
  }
}

const dropContent = css({
  width: "auto !important",
  height: "auto !important",
  transform: "translateX(-40%) !important"
});

const linkIcon = css({
  width: 15,
  marginRight: 20
});

const userAvatarNameWrapper = css({
  display: "flex",
  alignItems: "center",
  transition: "250ms ease",
  backgroundColor: "#ffffff00",
  padding: "10px 20px",
  borderRadius: 2,
  cursor: "pointer",
  ":hover": {
    backgroundColor: "#ffffff05"
  }
});

const avatarWrapper = css({
  margin: 0,
  marginRight: 10,
  width: 32,
  height: 32,
  borderRadius: "100%",
  overflow: "hidden",
  border: "1px solid #ffffff90",
  boxShadow: "0 4px 8px 0px rgba(0,0,0,.1)",
  position: "relative",
  "> img": {
    position: "absolute",
    top: "50%",
    left: 0,
    transform: "translateY(-50%)",
    width: "100%"
  }
});

const dropdownContentWrapper = css({
  position: "relative",
  right: 0,
  width: "18em",
  borderRadius: 2,
  overflow: "hidden",
  boxShadow: "0px 8px 20px 0px rgba(0,0,0,0.2)"
});

const dropdownContent = css({
  display: "flex",
  alignItems: "center",
  position: "relative !important",
  height: "auto !important",
  boxShadow: "none !important",
  "> a": {
    fontSize: 16,
    border: "none !important",
    opacity: 0.5,
    transition: "250ms ease"
  },
  ":hover": {
    "> a": {
      opacity: 1
    }
  }
});

let dropdownLink = css({
  // marginRight: "2em",
  textAlign: "center",
  color: "#fff",
  textDecoration: "none",
  fontFamily: "Roboto-Medium, sans-serif",
  cursor: "pointer"
});
