import React from "react";
import Head from "next/head";
import { css } from "glamor";
import Logo from "../NewSkin/Logo";
import Loading from "../NewSkin/Components/Loading";

const header = props => {
  const _renderLink = () => {
    if (props.pathname !== "index") {
      const avatarUrl = props.user.avatar
        ? `/static/avatar/${props.user.avatar}`
        : `/static/icons/user.svg`;
      const user = props.user;
      let switch_link = "";
      if (user && user.is_admin && user.status === 1) {
        switch_link = (
          <div
            className={`${dropdownContent} dropdown-content`}
            style={{ right: 0, width: "100%" }}
          >
            <a style={{ cursor: "pointer" }} onClick={props.switchDashboard()}>
              <img
                className={`${linkIcon}`}
                src="/static/icons/switch.svg"
                alt=""
              />
              Switch to Admin Dashboard
            </a>
          </div>
        );
      }
      return (
        <div className="dropdown-box">
          <div className={`${userAvatarNameWrapper}`}>
            <figure className={`${avatarWrapper}`}>
              <img src={avatarUrl} alt="" />
            </figure>
            <span className={dropdownLink}>{props.user.name}</span>
          </div>

          <div className={`${dropdownContentWrapper}`}>
            <div
              className={`${dropdownContent} dropdown-content`}
              style={{ right: 0, width: "100%" }}
            >
              <a href="" onClick={props.logoutPressed}>
                <img
                  className={`${linkIcon}`}
                  src="/static/icons/logout.svg"
                  alt=""
                />
                Logout
              </a>
              <br />
            </div>
            {switch_link}
          </div>
        </div>
      );
    }
  };

  return (
    <div
      id="page-header"
      data-style="display: absolute;"
      style={styles.container}
    >
      <Head>
        <title>CloudHealth</title>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="target-densitydpi=device-dpi" />
        <meta
          name="viewport"
          content="width=device-width,height=device-height,initial-scale=1.0"
        />
        <meta
          name="description"
          content="With CloudHealth’s digital platform, Achieving optimal health is just one click away"
        />
        <meta name="keywords" content="Health,Video,Doctor" />
        <link
          rel="apple-touch-icon-precomposed"
          sizes="57x57"
          href="/static/images/apple-touch-icon-57x57.png"
        />
        <link
          rel="apple-touch-icon-precomposed"
          sizes="114x114"
          href="/static/images/apple-touch-icon-114x114.png"
        />
        <link
          rel="apple-touch-icon-precomposed"
          sizes="72x72"
          href="/static/images/apple-touch-icon-72x72.png"
        />
        <link
          rel="apple-touch-icon-precomposed"
          sizes="144x144"
          href="/static/images/apple-touch-icon-144x144.png"
        />
        <link
          rel="apple-touch-icon-precomposed"
          sizes="60x60"
          href="/static/images/apple-touch-icon-60x60.png"
        />
        <link
          rel="apple-touch-icon-precomposed"
          sizes="120x120"
          href="/static/images/apple-touch-icon-120x120.png"
        />
        <link
          rel="apple-touch-icon-precomposed"
          sizes="76x76"
          href="/static/images/apple-touch-icon-76x76.png"
        />
        <link
          rel="apple-touch-icon-precomposed"
          sizes="152x152"
          href="/static/images/apple-touch-icon-152x152.png"
        />
        <link
          rel="icon"
          type="image/png"
          href="/static/images/favicon-196x196.png"
          sizes="196x196"
        />
        <link
          rel="icon"
          type="image/png"
          href="/static/images/favicon-96x96.png"
          sizes="96x96"
        />
        <link
          rel="icon"
          type="image/png"
          href="/static/images/favicon-32x32.png"
          sizes="32x32"
        />
        <link
          rel="icon"
          type="image/png"
          href="/static/images/favicon-16x16.png"
          sizes="16x16"
        />
        <link
          rel="icon"
          type="image/png"
          href="/static/images/favicon-128.png"
          sizes="128x128"
        />
        <meta name="application-name" content="CloudHealth" />
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta
          name="msapplication-TileImage"
          content="/static/images/mstile-144x144.png"
        />
        <meta
          name="msapplication-square70x70logo"
          content="/static/images/mstile-70x70.png"
        />
        <meta
          name="msapplication-square150x150logo"
          content="/static/images/mstile-150x150.png"
        />
        <meta
          name="msapplication-wide310x150logo"
          content="/static/images/mstile-310x150.png"
        />
        <meta
          name="msapplication-square310x310logo"
          content="/static/images/mstile-310x310.png"
        />
        <link
          rel="shortcut icon"
          href="/static/images/favicon.ico"
          type="image/x-icon"
        />
        <link
          rel="icon"
          href="/static/images/favicon.ico"
          type="image/x-icon"
        />
        <link
          defer
          href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900"
          rel="stylesheet"
        />
        <script src="/static/flexibility.js" />
        <script
          type="text/javascript"
          src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.20.0/polyfill.min.js"
        />
        <link rel="stylesheet" type="text/css" href="/static/nprogress.css" />
        <link
          rel="stylesheet"
          type="text/css"
          href="/static/react-datetime.css"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"
        />
        <link
          rel="stylesheet"
          href="https://www.w3schools.com/w3css/4/w3.css"
        />
        <link rel="stylesheet" type="text/css" href="/static/react-table.css" />
      </Head>

      <div style={styles.logoContainer}>
        <Logo />
      </div>

      <div style={styles.linkContainer}>
        {props.user ? _renderLink() : null}
      </div>
      {props.loading ? <Loading /> : null}
    </div>
  );
};

export default header;

let dropdownLink = css({
  // marginRight: "2em",
  textAlign: "center",
  color: "#fff",
  textDecoration: "none",
  fontFamily: "Roboto-Medium, sans-serif",
  cursor: "pointer"
});

const logo = css({
  backgroundColor: "#eee",
  width: 150,
  height: 40,
  display: "inline-block"
});

const linkIcon = css({
  width: 15,
  marginRight: 20
});

const userAvatarNameWrapper = css({
  display: "flex",
  alignItems: "center",
  transition: "250ms ease",
  backgroundColor: "#ffffff00",
  padding: "10px 20px",
  borderRadius: 2,
  cursor: "pointer",
  ":hover": {
    backgroundColor: "#ffffff05"
  }
});

const avatarWrapper = css({
  margin: 0,
  marginRight: 10,
  width: 32,
  height: 32,
  borderRadius: "100%",
  overflow: "hidden",
  border: "1px solid #ffffff90",
  boxShadow: "0 4px 8px 0px rgba(0,0,0,.1)",
  position: "relative",
  "> img": {
    position: "absolute",
    top: "50%",
    left: 0,
    transform: "translateY(-50%)",
    width: "100%"
  }
});

const dropdownContentWrapper = css({
  position: "absolute",
  top: 51,
  right: 0,
  width: "18em",
  borderRadius: 2,
  overflow: "hidden",
  boxShadow: "0px 8px 20px 0px rgba(0,0,0,0.2)"
});

const dropdownContent = css({
  display: "flex",
  alignItems: "center",
  position: "relative !important",
  height: "auto !important",
  boxShadow: "none !important",
  "> a": {
    fontSize: 16,
    border: "none !important",
    opacity: 0.5,
    transition: "250ms ease"
  },
  ":hover": {
    "> a": {
      opacity: 1
    }
  }
});

const styles = {
  container: {
    JsDisplay: "flex",
    display: "-webkit-box" /* OLD - iOS 6-, Safari 3.1-6 */,
    display: "-moz-box" /* OLD - Firefox 19- (buggy but mostly works) */,
    display: "-ms-flexbox" /* TWEENER - IE 10 */,
    display: "-webkit-flex" /* NEW - Chrome */,
    display: "flex" /* NEW, Spec - Opera 12.1, Firefox 20+ */,
    alignItems: "center",
    padding: "10px",
    height: "75px",
    WebkitJustifyContent: "center",
    justifyContent: "center",
    flexDirection: "row",
    WebkitFlexDirection: "row",
    // backgroundColor: "#364563",
    backgroundColor: "#fff",
    zIndex: 101,
    position: "absolute",
    width: "100%",
    top: 0
  },
  linkContainer: {
    marginLeft: "auto"
  },
  link: {
    marginRight: "2em",
    textAlign: "center",
    color: "white",
    textDecoration: "none",
    fontFamily: '"Roboto-Medium",sans-serif',
    cursor: "pointer"
  },
  selectArrowZone: {
    cursor: "pointer",
    display: "table-cell",
    position: "relative",
    textAlign: "center",
    verticalAlign: "middle",
    width: "25px",
    paddingRight: "5px"
  },
  selectArrow: {
    borderColor: "#fff",
    borderStyle: "solid",
    borderWidth: "5px 5px 2.5px",
    display: "inline-block",
    height: "0",
    width: "0",
    position: "absolute",
    right: "-18em",
    top: "-1.75em"
  },
  logoContainer: {
    left: "10px",
    marginLeft: "10px"
  },
  logo: {
    marginRight: "10px",
    color: "white"
  }
};
