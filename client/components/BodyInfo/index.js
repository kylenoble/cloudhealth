import React from "react";
import Switch from "@material-ui/core/Switch";
import Form from "../Form";
import AnswerService from "../../utils/answerService.js";
import SummaryService from "../../utils/summaryService.js";
import GenerateSummary from "../../utils/generateSummary.js";
import Colors from "../NewSkin/Colors";
import { confirmAlert } from "react-confirm-alert"; // Import
import { converInchesToFeet } from "../Commons/others";

const answer = new AnswerService();
const summary = new SummaryService();

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.lbs = 2.20462;
    this.ft = 12;
    this.cm = 2.54;
    this._isMounted = false;
    this.state = {
      switcher: {
        heightfeet: false,
        kilogram: false,
        heightCM: false
      },
      height: {
        id: 49,
        name: "height",
        label: "Height (in inches)",
        question: "Height (in)",
        type: "number",
        validation: "number",
        options: [],
        error: {
          active: false,
          message: "Please enter a height (eg. 70)"
        },
        value: ""
      },
      heightfeet: {
        id: 49,
        name: "heightfeet",
        label: "Height (in feet and inches)",
        hint: {
          title: "How to Input Height",
          description:
            "Input Feet and Inches seperated by period ( . ). Ex: if your Height is 5 feet 6 inches, input as 5.6"
        },
        question: "Height (in feet and inches)",
        type: "number",
        validation: "withRange",
        range: {
          start: 1,
          end: this.ft
        },
        options: [],
        error: {
          active: false,
          message: "Please enter a height (eg. 5,6)"
        },
        value: ""
      },

      heightCM: {
        id: 49,
        name: "heightCM",
        label: "Height (in centimeters)",
        question: "Height (in centimeters)",
        type: "number",
        validation: "number",
        options: [],
        error: {
          active: false,
          message: "Please enter a height (eg. 5,6)"
        },
        value: ""
      },

      weight: {
        id: 50,
        name: "weight",
        label: "Weight (in lbs)",
        question: "Weight (lbs)",
        type: "number",
        validation: "number",
        options: [],
        error: {
          active: false,
          message: "Please enter a weight (eg. 170)"
        },
        value: ""
      },
      kilogram: {
        id: 50,
        name: "kilogram",
        label: "Weight (in kg)",
        question: "Weight (kg)",
        type: "number",
        validation: "number",
        options: [],
        error: {
          active: false,
          message: "Please enter a weight (eg. 170)"
        },
        value: ""
      },
      waist: {
        id: 51,
        name: "waist",
        label: "Estimate Waist Measurement (in inches)",
        question: "Estimate Waist Measurement (in)",
        hint: {
          title: "How to Measure your Waist",
          description:
            "Stand up straight and breathe out. Use a tape measure to check the distance around the smallest part of your waist, just above your belly button. This is your waist circumference."
        },
        type: "number",
        validation: "number",
        options: [],
        error: {
          active: false,
          message: "Please enter a waist size (eg. 30)"
        },
        value: ""
      },
      hip: {
        id: 52,
        name: "hip",
        label: "Estimate Hip Measurement (in inches)",
        question: "Estimate Hip Measurement (in)",
        hint: {
          title: `How to Measure your Hip`,
          description: `Stand up straight and breathe out. Use a tape measure to check the distance around the largest part of your hips — the widest part of your buttocks. This is your hip circumference.`
        },
        type: "number",
        validation: "number",
        options: [],
        error: {
          active: false,
          message: "Please enter a hip size (eg. 34)"
        },
        value: ""
      },
      gender: "",
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      },
      fields: []
    };

    this._handleChange = this._handleChange.bind(this);
    this._goToNext = this._goToNext.bind(this);
  }

  supportsFlexBox() {
    const test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  componentWillMount() {
    this._isMounted = true;
  }

  componentDidMount() {
    this._isMounted = true;
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      flexibility(document.body);
    }

    this._updateFields();

    this._setDefaultValues();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  _updateFields() {
    let { fields, switcher } = this.state;

    fields = [
      switcher.heightfeet
        ? this.state.heightfeet
        : switcher.heightCM
        ? this.state.heightCM
        : this.state.height,
      switcher.kilogram ? this.state.kilogram : this.state.weight,
      this.state.waist,
      this.state.hip
    ];

    if (this._isMounted) {
      this.setState({
        fields
      });
    }
  }

  _setDefaultValues = () => {
    const {
      height,
      heightfeet,
      heightCM,
      weight,
      kilogram,
      waist,
      hip
    } = this.state;
    let { gender } = this.state;
    const convert = converInchesToFeet(this.props.survey.height);
    const kls = Math.round(this.props.survey.weight / this.lbs);
    const cm = this.props.survey.height * this.cm;
    height.value = this.props.survey.height;
    heightfeet.value = `${convert.feet}.${convert.inches}`;
    heightCM.value = parseFloat(cm).toFixed(2);
    weight.value = this.props.survey.weight;
    kilogram.value = kls;
    waist.value = this.props.survey.waist;
    hip.value = this.props.survey.hip;
    gender = this.props.survey.gender;

    if (this._isMounted) {
      this.setState({
        height,
        heightfeet,
        heightCM,
        weight,
        kilogram,
        waist,
        hip,
        gender
      });
    }
  };

  _handleSwitch(switchName) {
    const { switcher } = this.state;
    if (switchName === "weight") {
      switcher[switchName] = !switcher[switchName];
    } else if (switchName === "heightCM") {
      switcher[switchName] = !switcher[switchName];
      switcher.heightfeet = false;
    } else if (switchName === "heightfeet") {
      switcher[switchName] = !switcher[switchName];
      switcher.heightCM = false;
    } else {
      switcher[switchName] = !switcher[switchName];
    }

    this.setState({ switcher });
    this._updateFields();
  }

  render() {
    const { fields } = this.state;
    return (
      <div style={styles.container}>
        <div style={styles.introInfo}>
          <h1 style={{ margin: "0 auto 10px" }}>Weight</h1>
          <span style={{ fontSize: 14 }}>
            Entering your body measurements will help us assess your risk for
            cardiometabolic and endocrine issues <br />
            and will enable us determine the best health plan for you.
          </span>
          <div>
            <span className={styles.switchContainer}>
              <Switch
                checked={this.state.switcher.heightfeet}
                onChange={() => this._handleSwitch("heightfeet")}
                inputProps={{ "aria-label": "secondary checkbox" }}
              />
              Height in feet
              <Switch
                checked={this.state.switcher.heightCM}
                onChange={() => this._handleSwitch("heightCM")}
                inputProps={{ "aria-label": "secondary checkbox" }}
              />
              Height in centimeters
            </span>
            <span>
              <Switch
                checked={this.state.switcher.kilogram}
                onChange={() => this._handleSwitch("kilogram")}
                inputProps={{ "aria-label": "secondary checkbox" }}
              />
              Weight in kg
            </span>
          </div>
        </div>
        <Form
          from="health-survey"
          weight
          submitForm={this._goToNext}
          buttonText={
            this.props.location === "profile"
              ? "Update My Profile"
              : "Continue My Profile"
          }
          handleChange={this._handleChange}
          inputs={fields}
          secondaryOption=""
          formError={this.state.formError}
          formSuccess={this.state.formSuccess}
          back={this.props.back}
        />
      </div>
    );
  }

  _goToNext() {
    if (this._isMounted) {
      this.setState({
        formError: {
          active: false,
          message: ""
        }
      });
    }

    const inputs = [
      this.state.height,
      this.state.weight,
      this.state.waist,
      this.state.hip
    ];
    const bodyInfo = {
      waist: this.state.waist,
      hip: this.state.hip,
      height: this.state.height,
      weight: this.state.weight
    };
    const summaryGenerator = new GenerateSummary(
      bodyInfo,
      "body",
      this.state.gender
    );
    const bodySummary = [
      {
        name: "whr",
        group: "body",
        label: "WHR",
        value: summaryGenerator.generateWeightHip()
      },
      {
        name: "bmi",
        group: "body",
        label: "BMI",
        value: summaryGenerator.generateBMI()
      },
      {
        name: "weightScore",
        group: "body",
        label: "Weight Score",
        value: summaryGenerator.generateBodyScore()
      }
    ];

    const limit = summaryGenerator.generateBMI();

    if (limit >= 15 && limit <= 50) {
      if (limit >= 18 && limit <= 25) {
        console.log("normal");
        this.submitAnswer(inputs, bodySummary);
      } else {
        console.log("Not normal");
        confirmAlert({
          closeOnEscape: false,
          closeOnClickOutside: false,
          title: "", // Title dialog
          message:
            "BMI is outside of the normal range. Please check the unit measurements of your height and weight, otherwise, click CONFIRM.", // Message dialog
          buttons: [
            {
              label: "CONFIRM",
              onClick: () => {
                this.submitAnswer(inputs, bodySummary);
              }
            },
            {
              label: "Cancel"
            }
          ]
        });
        return false;
      }
    } else {
      if (this._isMounted) {
        this.setState({
          formError: {
            active: true,
            message:
              "BMI outside of range. Please check the unit measurements of your height and weight"
          }
        });
      }
      return;
    }
  }

  submitAnswer(inputs, bodySummary) {
    answer
      .create(inputs)
      .then(res => {
        if (res) {
          summary
            .create(bodySummary)
            .then(result => {
              if (result) {
                console.log("update user profile");
                if (this._isMounted) {
                  this.setState({
                    formSuccess: {
                      active: true,
                      message: "Your Profile Was Updated"
                    }
                  });
                }
                this.props.back();
                //  return this.props.onClick('Dashboard')
              }
            })
            .catch(error => {
              console.log(error);
              if (this._isMounted) {
                this.setState({
                  formError: {
                    active: true,
                    message: "There was an error updating your profile"
                  }
                });
              }
            });
        } else {
          if (this._isMounted) {
            this.setState({
              formError: {
                active: true,
                message: "There was an error updating your profile"
              }
            });
          }
        }
      })
      .catch(e => {
        if (this._isMounted) {
          this.setState({
            formError: {
              active: true,
              message: e.error
            }
          });
        }
      }); // you would show/hide error messages with component state here
  }

  _handleChange(value, input, validated) {
    const inputName = input;
    const newInput = this.state[inputName];

    if (input === "heightfeet") {
      if (value !== "") {
        const { height, heightfeet, heightCM } = this.state;
        const valArr = value.split(".");
        let inches = Math.round(valArr[0] * this.ft);
        if (valArr[1]) {
          inches += Math.round(valArr[1]);
        }

        heightfeet.error.active = true;
        heightfeet.error.message =
          "feet is from 1 to 7, inches is from 0 to 11";
        height.value = inches;
        heightCM.value = parseFloat(inches * this.cm).toFixed(2);
        this.setState({ height, heightfeet, heightCM });
      }
    } else if (input === "height") {
      const { heightfeet, heightCM } = this.state;
      const convert = converInchesToFeet(value);
      const cm = value * this.cm;
      heightCM.value = parseFloat(cm).toFixed(2);
      heightfeet.value = `${convert.feet}.${convert.inches}`;
    } else if (input === "heightCM") {
      const { heightfeet, height } = this.state;
      const cmtoInches = Math.round(value / this.cm);

      const convert = converInchesToFeet(cmtoInches);
      height.value = cmtoInches;
      heightfeet.value = `${convert.feet}.${convert.inches}`;
      this.setState({ height, heightfeet });
    } else if (input === "weight") {
      const { kilogram, weight } = this.state;
      weight.value = value;
      kilogram.value = Math.round(value / this.lbs);
      this.setState({ weight, kilogram });
    } else if (input === "kilogram") {
      const { kilogram, weight } = this.state;
      weight.value = Math.round(value * this.lbs);
      kilogram.value = value;
      this.setState({ weight, kilogram });
    }

    newInput.value = value;
    newInput.error.active = validated;

    this.setState({
      inputName: newInput
    });
    console.log(this.state);
  }
}

const styles = {
  switchContainer: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    padding: 10
  },
  container: {
    width: "100%",
    fontFamily: '"Roboto",sans-serif',
    display: "flex",
    flexWrap: "wrap",
    //  flexDirection: 'column',
    alignItems: "center",
    justifyContent: "center"
  },
  introInfo: {
    flex: "1 0 100%",
    // fontFamily: '"Roboto",sans-serif',
    color: Colors.blueDarkAccent,
    fontSize: "1rem",
    padding: "10px 20px",
    textAlign: "center",
    marginBottom: 30
  },
  formContainer: {
    border: "2px solid rgba(151,151,151,0.2)",
    borderRadius: "6px",
    maxWidth: "400px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.20)"
  },
  header: {
    borderBottom: "2px solid rgba(225,232,238,1)",
    padding: "15px 20px",
    fontSize: "18px",
    color: "#364563",
    fontFamily: '"Roboto-Medium",sans-serif',
    width: "360px"
  },
  form: {
    margin: "10px 30px 20px 20px"
  },
  formInput: {
    padding: "13px 30px 13px 30px",
    marginTop: "20px",
    width: "287px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    fontSize: "14px"
  },
  options: {
    marginTop: "15px",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif',
    padding: "13px 30px 13px 30px",
    WebkitAppearance: "none",
    MozAppearance: "none",
    appearance: "none",
    width: "350px",
    backgroundColor: "#fff",
    borderColor: "#d9d9d9 #ccc #b3b3b3",
    borderradius: "4px",
    border: "2px solid #E1E8EE",
    borderSpacing: "0",
    borderCollapse: "separate",
    height: "50px",
    outline: "none",
    overflow: "hidden",
    position: "relative",
    cursor: "pointer"
  },
  optionsDefault: {
    color: "rgba(169, 169, 169, 1)"
  },
  selectArrowZone: {
    cursor: "pointer",
    display: "table-cell",
    position: "relative",
    textAlign: "center",
    verticalAlign: "middle",
    width: "25px",
    paddingRight: "5px"
  },
  selectArrow: {
    borderColor: "#999 transparent transparent",
    borderStyle: "solid",
    borderWidth: "5px 5px 2.5px",
    display: "inline-block",
    height: "0",
    width: "0",
    position: "absolute",
    right: "-18em",
    top: "-1.75em"
  },
  button: {
    backgroundColor: "#ED81B9",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "0px 0px 20px 0px",
    cursor: "pointer"
  },
  register: {
    textDecoration: "none",
    margin: "20px 10px",
    color: "#43484D",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif'
  }
};
