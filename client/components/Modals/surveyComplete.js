import Modal from "react-modal";
import Button from '../NewSkin/Components/Button.js';
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: this.props.modalIsOpen
    };

    this.closeModal = this.closeModal.bind(this);
  }

  closeModal() {
    this.props.closeModal("surveyCompleteModalOpen");
  }

  render() {
    return (
      <Modal
        isOpen={this.props.modalIsOpen}
        onRequestClose={this.closeModal}
        shouldCloseOnOverlayClick={false}
        style={customStyles}
        ariaHideApp={false}
      >
        <div style={styles.letterStyle}>
          <p>
            Congratulations on finishing the health survey. You are now a step
            closer to achieving optimum health. The data that you have submitted
            will now be analyzed by our medical team to come up with the
            recommended health plan for you and your company. You will be
            notified once your company enrolls you to our corporate health
            programs. You may also be asked to update your profile in the future
            in order to track your health progress. Thank you.
          </p>
          <Button styles={{margin: '20px 0 auto auto'}}>
            <button onClick={this.closeModal}>
              Close
            </button>
          </Button>
        </div>
      </Modal>
    );
  }
}

// jsx styles
const customStyles = {
  content: {
    width: "50%",
    height: "auto",
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: "30px 50px",
    boxShadow: "-3px 3px 30px #424242"
  }
};
const styles = {
  letterStyle: {
    textAlign: "justify",
    fontFamily: '"Roboto",sans-serif'
  },
  button: {
    backgroundColor: "#77E9AF",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "30px 0px 20px 0px",
    cursor: "pointer"
  },
  button_yes: {
    backgroundColor: "#3498db",
    fontFamily: '"Roboto",sans-serif',
    color: "white",
    fontSize: "14px",
    padding: "8px 15px 8px 15px",
    cursor: "pointer",
    borderRadius: "6px"
  }
};
