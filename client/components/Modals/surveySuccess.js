import Modal from "react-modal";
import Button from '../NewSkin/Components/Button.js';
import GridContainer from '../NewSkin/Wrappers/GridContainer';

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: this.props.modalIsOpen
    };

    this.closeModal = this.closeModal.bind(this);
  }

  closeModal() {
    this.props.closeModal("surveySuccessModalOpen");
  }

  render() {
    return (
      <Modal
        isOpen={this.props.modalIsOpen}
        onRequestClose={this.closeModal}
        shouldCloseOnOverlayClick={false}
        style={customStyles}
        ariaHideApp={false}
      >
        <div style={styles.letterStyle}>
          <GridContainer columnSize="150px 1fr" gap={20}>
            <img width="150" src="/static/icons/survey_completed_icon.svg" alt="" />
            <p style={{ textAlign: 'justify' }}>
              Thank you for completing the health survey. Please review your
              answers thoroughly. You can no longer edit your responses after
              submission. You may visit your dashboard after submission of responses for the analysis of
              your profile.
          </p>
          </GridContainer>
          <Button right>
            <button onClick={this.closeModal}>
              Close
            </button>
          </Button>
        </div>
      </Modal>
    );
  }
}

// jsx styles
const customStyles = {
  content: {
    width: "50%",
    height: "auto",
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: "30px 50px",
    boxShadow: "0 8px 15px rgba(0,0,0,.1)",
    border: 'none'
  }
};
const styles = {
  letterStyle: {
    textAlign: "justify",
    fontFamily: '"Roboto",sans-serif'
  },
  button: {
    backgroundColor: "#77E9AF",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "30px 0px 20px 0px",
    cursor: "pointer"
  },
  button_yes: {
    backgroundColor: "#3498db",
    fontFamily: '"Roboto",sans-serif',
    color: "white",
    fontSize: "14px",
    padding: "8px 15px 8px 15px",
    cursor: "pointer",
    borderRadius: "6px"
  }
};
