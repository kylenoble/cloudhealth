/* eslint-disable import/newline-after-import */
/* eslint-disable react/react-in-jsx-scope */
import React from 'react';
import Modal from 'react-modal';
import { css } from 'glamor';
import { modalCustomStyles } from '../NewSkin/Styles';
import ModalWrapper, { ModalContent, ModalContentWrapper, ModalHeader, ModalFooter, ModalTitle } from '../NewSkin/Wrappers/Modal';
import Button from '../NewSkin/Components/Button';
export default class extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: this.props.modalIsOpen
    };

    this.closeModal = this.closeModal.bind(this);
  }


  closeModal() {
    //this.props.updateStatus(1)
    this.props.closeModal('termOfUseModelIsOpen');
  }

  render() {
    return (
      <Modal
        isOpen={this.props.modalIsOpen}
        onAfterOpen={this.afterOpenModal}
        onRequestClose={this.closeModal}
        shouldCloseOnOverlayClick={false}
        style={customStyles}
        ariaHideApp={false}
      >
        <ModalWrapper>
          <ModalContentWrapper>
            <div style={styles.letterStyle} id="termsOfUse">

              <ModalHeader styles={{marginBottom: 25}}>
                <h3 style={{textAlign: 'center', fontWeight: 500, fontSize: '1.3rem'}}>CloudHealthAsia Terms of Use Agreement</h3>
              </ModalHeader>

              <ModalContent>
                <ModalTitle>
                  <h3>Acceptance</h3>
                </ModalTitle>
                <p style={{margin: '0 auto 30px'}}>
                  <strong>It is important that you read all the following terms and conditions carefully</strong>. This Terms of Use Agreement (“Agreement”) is a legal agreement between you and CloudHealthAsia (“Owner”), the owner and operator of this website (the “Website”). It states the terms and conditions under which you may access and use the website and all written and other materials displayed or made available through the website (“content”). Your use of the <strong>CloudHealthAsia</strong> website constitutes your acceptance of these terms and conditions of use. If you have not already done so, please take some time to familiarize yourself with the terms. By accessing the site, you are agreeing to be bound by these site terms of use, all applicable laws and regulations and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, do not use this site. The Owner may revise this Agreement at any time by updating this posting. Use of the website after such changes are posted and will signify your acceptance of these revised terms. You should visit this page periodically to review this Agreement.
              </p>

                <div>
                  <ModalTitle>
                    <h3>Medical Emergency</h3>
                  </ModalTitle>
                  <p style={{margin: '0 auto 30px'}}>Do NOT use this website for medical emergencies. If you have a medical emergency, call a divhysician or a qualified health care provider, or CALL 911 immediately. Under no circumstances should you attempt self-treatment based on anything you have seen or read on this website. This website is not intended for acute care management.</p>
              </div>

                <div>
                  <ModalTitle>
                    <h3>No physician-patient relationship</h3>
                  </ModalTitle>
                  <p style={{margin: '0 auto 30px'}}>Unless you already have a consult with one of our functional medicine doctors, under no circumstance shall it be that the medical information displayed in this website establish a physician-patient relationship. Overview of your health profile without an actual consult is considered general information.
                  </p>
              </div>

                <div>
                  <ModalTitle>
                    <h3>Patient portal services</h3>
                  </ModalTitle>
                  <p style={{margin: '0 auto 30px'}}>If you are an existing patient, <strong>CloudHealthAsia</strong> may allow you to communicate with your physician(s) by video conferencing or by some other electronic means. Any use or disclosure of personal information provided by you shall be in accordance with our privacy policy.</p>
              </div>

                <div>
                  <ModalTitle>
                    <h3>Indemnity</h3>
                  </ModalTitle>
                  <p style={{margin: '0 auto 30px'}}>You agree to indemnify, defend, and hold harmless <strong>CloudHealthAsia</strong>, its physicians, directors, employees, agents, licensors and their respective successors , from and against any and all claims, demands, liabilities, costs or expenses whatsoever, including , without limitation, legal fees and disbursements , resulting directly or indirectly from (i) your breach of any of the terms and conditions of this Agreement; (ii) your access to, use, misuse, reliance upon, or to time or; (iii) your use of, reliance on, publication, communication, distribution, uploading, or downloading of anything (including Content) on or from the website.</p>
              </div>

                <div>
                  <ModalTitle>
                    <h3>Use of the Website</h3>
                  </ModalTitle>
                  <p style={{margin: '0 auto 30px'}}><strong>CloudHealthAsia</strong> authorizes you to access and use our website for your personal noncommercial use in accordance with the terms and conditions of this Agreement.</p>
              </div>

                <div>
                  <ModalTitle>
                    <h3>Disclaimer and Copyright</h3>
                    <h4 style={{fontSize: '1rem !important', fontStyle: 'italic'}}>Copyright</h4>
                  </ModalTitle>
                  <p style={{margin: '0 auto 30px'}}>All information, text, documents, materials, graphics, photography, designs, logos, layouts, icons and computer codes (collectively, “content”) of this site is (and shall continue to be) owned exclusively by the <strong>CloudHealthAsia</strong> and is protected under applicable copyrights, patents, trademarks, and/or other proprietary intellectual property rights. The copying, redistribution, use or publication of any such content, or any part of this site, are prohibited. Under no circumstances will you acquire any ownership rights or other interest in any content by or through your use of this site.</p>
              </div>

                <div>
                  <ModalTitle>
                  <h4 style={{fontSize: '1rem !important', fontStyle: 'italic'}}>Disclaimer</h4>
                  </ModalTitle>
                  <p style={{margin: '0 auto 30px'}}>The information on this site is provided solely for users’ general knowledge and is provided “as is”. <strong>CloudHealthAsia</strong> hereby grants you a limited, non-transferable, nonexclusive right to access and use of the site solely for your personal use to find out information about your health and our business. This authorizes you to view and the materials on the site solely for your personal, non-commercial use. You may not modify, copy, distribute, transmit, display, publish, or create derivative works from the Information or use the Information for any commercial purpose whatsoever. There is no warranty, representation or guarantee with respect to the accuracy, timeliness or completeness of the information. <strong>CloudHealthAsia</strong> does not warrant that the site and/or the information will be free from errors, defects, program limitations, viruses or other harmful components or that the site and the information will be accessible and perform in accordance with your expectations. The use of the site is at your own risk.</p>
              </div>
              </ModalContent>
            </div>
          </ModalContentWrapper>
          <ModalFooter>
            <Button type="pink" styles={{ marginTop: '0 !important', margin: '0 auto' }}>
              <button style={styles.button_yes} onClick={this.closeModal}>Close</button>
            </Button>
          </ModalFooter>
        </ModalWrapper>
      </Modal>
    );
  }
}


//glamos styles
const listStyle = css({
  margin: '10px 0 5px 0',
  padding: 5,
});

const pStyle = css({
  margin: '0px'
});

// jsx styles
const customStyles = { ...modalCustomStyles };
// const customStyles = {
//   content : {
//     width: '80%',
//     height: '70vh',
//     top                   : '50%',
//     left                  : '50%',
//     right                 : 'auto',
//     bottom                : 'auto',
//     marginRight           : '-50%',
//     transform             : 'translate(-50%, -50%)',
//     padding               : '0 50px 0 50px',
//     boxShadow             : '-3px 3px 30px #424242'
//   }
// };
const styles = {
  letterStyle: {
    textAlign: 'justify',
  },
  button_yes: {
    backgroundColor: '#3498db',
    color: 'white',
    fontSize: '14px',
    padding: '8px 15px 8px 15px',
    cursor: 'pointer',
    borderRadius: '6px',
  }
};
