import { css } from 'glamor'
import FaqNavigationItem from './FaqNavigationItem'

import TicketService from '../../../utils/ticketService'
import Colors from '../../NewSkin/Colors';
const ticketService = new TicketService()

export default class FaqHeader extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            faqToggle: false,
            navTitles: this.props.navTitles
        }
    }

    handleToggle = e => {
        this.setState({
            faqToggle: !this.state.faqToggle
        })
    }

    activeNav = e => {
        let lis = Array.from(document.querySelectorAll('li'))
        lis.forEach(li => {
            li.classList.remove(clicked)
        })
        e.target.classList.add(clicked)

        this.props.showFaq(e)
    }

    render() {
        return (
            <li>
                <p onClick={this.handleToggle} className={`${faqNavItem} ${faqHead} ${noselect} ${this.state.faqToggle ? active : ''}`}>
                    {this.props.navHeaders}
                    <span className={headerIcon}>{this.state.faqToggle ? '-' : '+'}</span>
                </p>
                <ul
                    style={{
                        listStyle: 'none',
                        padding: '0',
                        margin: '0',
                        height: 'auto',
                        overflow: 'hidden'
                    }}
                    className={this.state.faqToggle ? expanded : notExpanded}
                >
                    {this.state.navTitles.map((faqNavigationContent, index) => (
                        <FaqNavigationItem key={index} faqNavigationContent={faqNavigationContent} activeNav={this.activeNav} />
                    ))}
                </ul>
            </li>
        )
    }
}

let active = css({
    // background: '#364563',
    background: Colors.pink,
    color: 'white',
    ':hover': {
        background: `${Colors.pinkDarkAccent} !important`,
    }
})

let noselect = css({
    userSelect: 'none',
    ':selection': {
        background: 'none',
        color: 'black',
        boxShadow: 'none'
    }
})

let clicked = css({
    fontWeight: '600 !important',
    // boxShadow: 'inset 0 5px 10px rgba(20,20,20,.1), inset 0 -5px 10px rgba(20,20,20,.1)',
    position: 'relative',
    ':before': {
        width: '100%',
        background: '#eee'
    }
})

let headerIcon = css({
    float: 'right'
})

let expanded = css({
    maxHeight: '600px',
    transition: 'max-height 300ms ease'
})

let notExpanded = css({
    maxHeight: '0px',
    transition: 'max-height 300ms ease'
})

let faqNavItem = css({
    padding: '15px',
    borderBottom: '1px solid #ccc',
    cursor: 'pointer',
    margin: '0px'
})

let faqHead = css({
    fontWeight: '600',
    // background: '#f6f6f6',
    transition: '300ms ease',
    ':hover': {
        background: '#f1f1f1'
    }
})
