import { css } from "glamor";
import TicketService from "../../../utils/ticketService.js";
import currentDomain from "../../../utils/domain.js";
import request from "axios";
const GET_TICKET_URL = `${currentDomain}/api/ticket/getTickets/`;
const GET_REPLIES_URL = `${currentDomain}/api/ticket/getTicketReplies/`;

const ticket = new TicketService();

export default class CheckTicketStatus extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTicket: "",
      searchValue: "",
      ticketReplies: [],
      searchedTicketData: [],
      ticketRepliesUpdated: false,
      tnum: "",
      tickets: "",
      replies: [],
      ticketArr: [],
      typing: "",
      showTicket: false,
      statusTooltip: "",
      open: "",
      pending: "",
      resolved: "",
      closed: "",
      changeTooltip: false,
      showTooltip: false,
      showTooltipContent: false,
      statusColor: ""
    };
  }

  componentDidMount() {
    this.tooltips();
  }

  tooltips = () => {
    this.setState({
      open:
        "Newly created tickets are marked as Open. CloudHealthAsia Support team is yet to resolve the report and will contact you shortly for additional information.",
      pending: "Ticket is awaiting reply from a customer or any 3rd party.",
      resolved:
        "Support Team is reasonably sure that they have provided the customer with a solution to the problem reported.",
      closed:
        "Resolved tickets are changed to Closed Status once customer has acknowledged that the error or problem has been solved. Resolved tickets will be automatically be closed after 48 or 72 hours even without the customer’s acknowledgement ."
    });
  };

  // REQUESTS
  // Get ticket
  _getTickets = tnum => {
    request
      .get(GET_TICKET_URL + tnum)
      .then(res => {
        // console.log(res.body);
        setTimeout(() => {
          if (res.body.length < 1) {
            setTimeout(() => {
              this.setState({
                typing: "No Ticket exist.",
                tickets: "",
                ticketArr: "",
                showTicket: false
              });
            }, 1);
          } else {
            this.setState({
              typing: "Ticket found.",
              tickets: res.body[0],
              ticketArr: res.body,
              showTicket: true
            });
            this.statusTooltip();
          }
        }, 1);
      })
      .catch(err => {
        console.log(err);
      });
  };

  // Get Ticket replies of a ticket
  _getTicketReplies = tnum => {
    request
      .get(GET_REPLIES_URL + tnum)
      .then(res => {
        this.setState({
          replies: res.body
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  _handleFocus = e => {
    e.target.parentNode.classList.add(focused);
  };

  _handleBlur = e => {
    if (e.target.value.length <= 0) {
      e.target.parentNode.classList.remove(focused);
    }
  };

  _handleChange = e => {
    let el = e.target.value;

    setTimeout(() => {
      if (el) {
        clearTimeout(timer);
        this.setState({
          typing: "Searching...",
          tnum: el
        });
        setTimeout(() => {
          this.setState({
            showTicket: false
          });
        }, 100);
        let timer = setTimeout(() => {
          this._getTickets(this.state.tnum);
          this._getTicketReplies(this.state.tnum);
        }, 1000);
      }
    }, 1);
  };

  _handleSubmit = e => {
    e.preventDefault();

    setTimeout(() => {
      this._getTickets(this.state.tnum);
      this._getTicketReplies(this.state.tnum);
    }, 500);
  };

  statusTooltip = () => {
    let status = this.state.tickets.status;

    switch (status) {
      case "Open":
        this.setState({
          statusTooltip: this.state.open,
          statusColor: "green"
        });
        break;

      case "Pending":
        this.setState({
          statusTooltip: this.state.pending,
          statusColor: "orange"
        });
        break;

      case "Resolved":
        this.setState({
          statusTooltip: this.state.resolved,
          statusColor: "blue"
        });
        break;

      case "Closed":
        this.setState({
          statusTooltip: this.state.closed,
          statusColor: "grey"
        });
        break;

      default:
        break;
    }
  };

  _handleMouseEnter = () => {
    this.setState({
      showTooltip: true
    });

    setTimeout(() => {
      this.setState({
        showTooltipContent: true
      });
    }, 50);
  };

  _handleMouseLeave = () => {
    this.setState({
      showTooltipContent: false
    });

    setTimeout(() => {
      this.setState({
        showTooltip: false
      });
    }, 210);
  };

  dtConvert = datetime => {
    let splited = datetime.split("T");
    let date = splited[0];
    let time = splited[1].split(".000Z");

    // Convert Date
    let newDate = new Date(date).toUTCString().split(" ");
    let NewDate = newDate[2] + " " + newDate[1] + ", " + newDate[3];

    // Convert Time
    let cTime = time[0]
      .toString()
      .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (cTime.length > 1) {
      cTime = cTime.slice(1);
      cTime[5] = +cTime[0] < 12 ? " AM" : " PM";
      cTime[0] = +cTime[0] % 12 || 12;
    }
    let cnvTime = cTime.join("");

    return NewDate + " " + cnvTime;
  };

  render() {
    return (
      <section className={ticketFormWrapper}>
        <h2>Check Ticket Status</h2>
        <form onSubmit={this._handleSubmit}>
          <section className={row}>
            <article
              className={[
                formInputWrapper,
                left,
                searchTicket,
                afterClass
              ].join(" ")}
            >
              <input
                ref={searchTicket => (this.searchTicket = searchTicket)}
                onChange={this._handleChange}
                onBlur={this._handleBlur}
                onFocus={this._handleFocus}
                type="text"
                required="true"
              />
            </article>
            <button className={[btn, searchBtn, right].join(" ")}>
              Search
            </button>
          </section>
        </form>

        <article
          className={[
            resultWrapper,
            this.state.showTicket ? showTicket : hideTicket
          ].join(" ")}
        >
          {this.state.ticketArr.length > 0 ? (
            <div className={[threadWrapper].join(" ")}>
              <h3>
                Subject:{" "}
                <span>
                  {this.state.tickets.subject}{" "}
                  <small>- {this.state.tickets.fullname}</small>
                </span>
              </h3>

              <p className={half}>
                <label>Concern:</label>{" "}
                <span>{this.state.tickets.concern}</span>
              </p>

              <p className={half}>
                <label>Status:</label>
                <span
                  className={[status, this.state.statusColor].join(" ")}
                  onMouseEnter={this._handleMouseEnter}
                  onMouseLeave={this._handleMouseLeave}
                >
                  {this.state.tickets.status}
                  {this.state.showTooltip ? (
                    <span
                      className={[
                        "statTooltip",
                        tooltip,
                        this.state.showTooltipContent ? showTooltip : null
                      ].join(" ")}
                    >
                      {this.state.statusTooltip}
                    </span>
                  ) : null}
                </span>{" "}
              </p>

              <p className={msg}>
                Message: <span>{this.state.tickets.message}</span>
              </p>

              <ul className={this.state.replies.length > 4 ? shadowed : null}>
                {this.state.replies.length > 0 ? (
                  this.state.replies.map((reply, i) => {
                    return (
                      <li key={i} className={replyWrapper}>
                        <p> {reply.reply_message}</p>
                        <small>
                          Replied: {this.dtConvert(reply.datetime_reply)} |
                          CloudHealthAsia Admin
                        </small>
                      </li>
                    );
                  })
                ) : (
                  <p className={noReply}>No reply yet.</p>
                )}
              </ul>
            </div>
          ) : null}
        </article>

        <h3>{this.state.ticketArr.length > 0 ? "" : this.state.typing}</h3>
      </section>
    );
  }
}

let half = css({
  // width: '50%',
  // float: 'left'
});

let tooltip = css({
  position: "absolute",
  top: "-20px",
  right: "0",
  padding: "15px",
  border: "1px solid #d383b6",
  background: "#fff",
  boxShadow: "0 8px 32px rgba(0,0,0,.1)",
  width: "300px",
  transform: "translateX(100%)",
  zIndex: "100",
  transition: "200ms ease",
  opacity: "0",
  fontSize: "14px"
});

let showTooltip = css({
  top: "0px",
  opacity: "1"
});

let status = css({
  cursor: "pointer",
  padding: "2px 5px",
  border: "1px solid #d383b6",
  position: "relative",
  boxSizing: "border-box",
  marginLeft: "4px",
  fontSize: "12px"
});

let shadowed = css({
  boxShadow: "inset 0 10px 20px rgba(0,0,0,.030)"
});

let msg = css({
  marginBottom: "10px"
});

let noReply = css({
  padding: "10px",
  borderLeft: "3px solid #eee",
  color: "#000 !important"
});

let hideTicket = css({
  opacity: "0 !important",
  transform: "translateY(-20px)"
});

let showTicket = css({
  opacity: "1 !important",
  transform: "translateY(0px)"
});

let threadWrapper = css({
  boxShadow: "0 5px 10px rgba(0,0,0,.05)",
  padding: "10px 20px",
  display: "table",
  clear: "both",
  width: "100%",
  "> ul": {
    listStyle: "none",
    maxHeight: "270px",
    overflowY: "auto",
    padding: "0px"
  },
  "> h3": {
    borderBottom: "1px solid #ccc",
    marginBottom: "10px",
    paddingBottom: "10px",
    "> span": {
      color: "#d383b6",
      display: "inline-block"
    }
  },
  " p": {
    color: "#555",
    position: "relative",
    "> span": {
      color: "#d383b6"
    },
    "> span.green": {
      color: "white !important",
      borderColor: "#34db89",
      background: "#34db89",
      "> .statTooltip": {
        borderColor: "#34db89",
        color: "#34db89"
      }
    },
    "> span.blue": {
      color: "white !important",
      borderColor: "#81cde8",
      background: "#81cde8",
      "> .statTooltip": {
        borderColor: "#81cde8",
        color: "#81cde8"
      }
    },
    "> span.orange": {
      color: "white !important",
      borderColor: "orange",
      background: "orange",
      "> .statTooltip": {
        borderColor: "orange",
        color: "orange"
      }
    },
    "> span.grey": {
      color: "whitesmoke !important",
      borderColor: "#888",
      background: "#888",
      "> .statTooltip": {
        borderColor: "#888",
        color: "#888"
      }
    },
    "> label": {
      width: "60px",
      display: "inline-block"
    }
  },
  " p:nth-child(4)": {
    "> span": {
      display: "block",
      paddingLeft: "10px"
    }
  }
});

let replyWrapper = css({
  padding: "5px 10px",
  marginBottom: "10px",
  borderLeft: "3px solid whitesmoke",
  transition: "250ms ease",
  "> small": {
    color: "#ccc"
  },
  "> p": {
    color: "#000 !important"
  },
  ":hover": {
    borderLeftColor: "#d383b6"
  }
});

let resultWrapper = css({
  width: "100%",
  transition: "500ms ease"
});

let btn = css({
  borderStyle: "none",
  padding: "12px 20px",
  borderRadius: "2px",
  boxShadow: "0 1px 4px rgba(0,0,0,.1)",
  cursor: "pointer",
  fontSize: "14px",
  verticalAlign: "middle"
});

let searchBtn = css({
  width: "20% !important",
  background: "#81cde8",
  color: "white",
  transition: "250ms ease",
  ":hover": {
    opacity: ".8",
    boxShadow: "0 2px 4px rgba(0,0,0,.2)"
  }
});

let ticketFormWrapper = css({
  "> h2": {
    marginBottom: "20px",
    borderBottom: "1px solid #81cde8",
    padding: "10px",
    color: "#81cde8",
    textTransform: "uppercase"
  }
});

let formInputWrapper = css({
  width: "48%",
  padding: "5px",
  position: "relative",
  borderBottom: "2px solid #ccc",
  marginTop: "10px",
  marginBottom: "15px",
  verticalAlign: "bottom",
  "> input": {
    border: "none",
    padding: "5px",
    paddingBottom: "0px",
    width: "100%",
    position: "relative",
    zIndex: "100",
    background: "none",
    ":invalid": {
      outline: "none",
      boxShadow: "none",
      color: "red"
    }
  },
  "> select": {
    border: "none",
    padding: "5px 0px",
    paddingLeft: "2px",
    paddingBottom: "0px",
    width: "100%",
    position: "relative",
    zIndex: "101",
    background: "none",
    ":focus": {
      outline: "none",
      border: "none"
    }
  },
  "::before": {
    content: " ",
    position: "absolute",
    bottom: "-2px",
    left: "50%",
    height: "2px",
    width: "0%",
    background: "#81cde8",
    transform: "translateX(-50%)",
    transition: "250ms ease"
  }
});

let left = css({
  float: "left"
});
let right = css({
  float: "right"
});

let row = css({
  width: "100%",
  display: "table",
  clear: "both"
});

let searchTicket = css({
  width: "75% !important",
  "::after": {
    content: "Search Ticket Number"
  }
});

let afterClass = css({
  "::after": {
    position: "absolute",
    top: "10px",
    left: "10px",
    zIndex: "100",
    transition: "250ms ease",
    color: "#ccc"
  }
});

let focused = css({
  "::before": {
    width: "100% !important"
  },
  "::after": {
    top: "-10px !important",
    fontSize: "12px",
    color: "#81cde8 !important"
  }
});
