import { css } from "glamor";

export default class FaqNavigationItem extends React.Component {
  activeNav = e => {
    this.props.activeNav(e);
  };

  render() {
    return (
      <li
        id={this.props.faqNavigationContent.title.replace(/\s/g, "")}
        className={[faqNavItem, noselect, "nav-item"].join(" ")}
        onClick={this.activeNav}
      >
        <span className={noselect}>
          {this.props.faqNavigationContent.title}
        </span>
      </li>
    );
  }
}

let noselect = css({
  userSelect: "none"
});

let faqNavItem = css({
  padding: "15px",
  paddingLeft: "30px",
  borderBottom: "1px solid #ccc",
  cursor: "pointer",
  transition: "250ms ease",
  position: "relative",
  "&:last-child": {
    borderBottom: "none !important"
  },
  ":hover": {
    background: "#eee",
    "::before": {
      background: "#ddd"
    }
  },
  ":before": {
    content: '" "',
    position: "absolute",
    top: "0",
    left: "0",
    height: "100%",
    width: "5px",
    zIndex: "100",
    background: "white",
    transition: "250ms ease"
  },
  ":after": {
    content: '" "',
    position: "absolute",
    top: "0",
    left: "0",
    height: "100%",
    width: "100%",
    zIndex: "102",
    background: "rgba(0,0,0,0)"
  },
  "> span": {
    position: "relative",
    zIndex: "101",
    userSelect: "none",
    position: "relative"
  }
});

let active = css({
  fontWeight: "600"
});

let notActive = css({
  fontWeight: "normal"
});
