import { css } from "glamor";

export let half = css({
  // width: '50%',
  // float: 'left'
});

export let tooltip = css({
  position: "absolute",
  top: "-20px",
  right: "0",
  padding: "15px",
  border: "1px solid #d383b6",
  background: "#fff",
  boxShadow: "0 8px 32px rgba(0,0,0,.1)",
  width: "300px",
  transform: "translateX(100%)",
  zIndex: "100",
  transition: "200ms ease",
  opacity: "0",
  fontSize: "14px"
});

export let showTooltip = css({
  top: "0px",
  opacity: "1"
});

export let status = css({
  cursor: "pointer",
  padding: "2px 5px",
  border: "1px solid #d383b6",
  position: "relative",
  boxSizing: "border-box",
  marginLeft: "4px",
  fontSize: "12px"
});

export let shadowed = css({
  boxShadow: "inset 0 10px 20px rgba(0,0,0,.030)"
});

export let msg = css({
  marginBottom: "10px"
});

export let noReply = css({
  padding: "10px",
  borderLeft: "3px solid #eee",
  color: "#000 !important"
});

export let hideTicket = css({
  opacity: "0 !important",
  transform: "translateY(-20px)"
});

export let showTicket = css({
  opacity: "1 !important",
  transform: "translateY(0px)"
});

export let threadWrapper = css({
  boxShadow: "0 5px 10px rgba(0,0,0,.05)",
  padding: "10px 20px",
  display: "table",
  clear: "both",
  width: "100%",
  boxSizing: "border-box",
  "> .name": {
    fontSize: 16,
    color: "#81cde8"
  },
  "> ul": {
    listStyle: "none",
    maxHeight: "260px",
    overflowY: "auto",
    padding: "0px"
  },
  "> h3": {
    borderBottom: "1px solid #ccc",
    marginBottom: "10px",
    paddingBottom: "10px",
    "> span": {
      color: "#d383b6"
    }
  },
  " p": {
    color: "#555",
    position: "relative",
    "> span": {
      display: "inline-block !important",
      color: "#d383b6",
      paddingLeft: "5px !important"
    },
    "> span.green": {
      color: "white !important",
      borderColor: "#34db89",
      background: "#34db89",
      "> .statTooltip": {
        borderColor: "#34db89",
        color: "#34db89"
      }
    },
    "> span.blue": {
      color: "white !important",
      borderColor: "#81cde8",
      background: "#81cde8",
      "> .statTooltip": {
        borderColor: "#81cde8",
        color: "#81cde8"
      }
    },
    "> span.orange": {
      color: "white !important",
      borderColor: "orange",
      background: "orange",
      "> .statTooltip": {
        borderColor: "orange",
        color: "orange"
      }
    },
    "> span.grey": {
      color: "whitesmoke !important",
      borderColor: "#888",
      background: "#888",
      "> .statTooltip": {
        borderColor: "#888",
        color: "#888"
      }
    },
    "> label": {
      width: "60px",
      display: "inline-block"
    }
  },
  " p:nth-child(4)": {
    "> span": {
      display: "block",
      paddingLeft: "10px"
    }
  }
});

export let replyWrapper = css({
  padding: "5px 10px",
  marginBottom: "10px",
  borderLeft: "3px solid whitesmoke",
  transition: "250ms ease",
  boxSizing: "border-box",
  "> small": {
    color: "#ccc"
  },
  "> p": {
    color: "#000 !important"
  },
  ":hover": {
    borderLeftColor: "#d383b6"
  }
});

export let resultWrapper = css({
  width: "100%",
  transition: "500ms ease",
  "> *": {
    boxSizing: "border-box"
  }
});

export let btn = css({
  borderStyle: "none",
  padding: "12px 20px",
  borderRadius: "2px",
  boxShadow: "0 1px 4px rgba(0,0,0,.1)",
  cursor: "pointer",
  fontSize: "14px",
  verticalAlign: "middle"
});

export let searchBtn = css({
  width: "20% !important",
  background: "#81cde8",
  color: "white",
  transition: "250ms ease",
  ":hover": {
    opacity: ".8",
    boxShadow: "0 2px 4px rgba(0,0,0,.2)"
  }
});

export let ticketFormWrapper = css({
  "> h2": {
    margin: "10px 0px 20px",
    borderBottom: "1px solid #81cde8",
    padding: "15px 10px",
    color: "#81cde8",
    textTransform: "uppercase"
  }
});

export let formInputWrapper = css({
  width: "48%",
  padding: "5px",
  position: "relative",
  borderBottom: "2px solid #ccc",
  marginTop: "10px",
  marginBottom: "15px",
  verticalAlign: "bottom",
  "> input": {
    border: "none",
    padding: "5px",
    paddingBottom: "0px",
    width: "100%",
    position: "relative",
    zIndex: "100",
    background: "none",
    boxSizing: "border-box",
    ":invalid": {
      outline: "none",
      boxShadow: "none",
      color: "red"
    },
    ":focus": {
      outline: "none"
    }
  },
  "> select": {
    border: "none",
    padding: "5px 0px",
    paddingLeft: "2px",
    paddingBottom: "0px",
    width: "100%",
    position: "relative",
    zIndex: "101",
    background: "none",
    ":focus": {
      outline: "none",
      border: "none"
    }
  },
  ":before": {
    content: '" "',
    position: "absolute",
    bottom: "-2px",
    left: "50%",
    height: "2px",
    width: "0%",
    background: "#81cde8",
    transform: "translateX(-50%)",
    transition: "250ms ease"
  }
});

export let left = css({
  float: "left"
});
export let right = css({
  float: "right"
});

export let row = css({
  width: "100%",
  display: "table",
  clear: "both"
});

export let searchTicket = css({
  width: "75% !important",
  ":after": {
    content: '"Search Ticket Number"'
  }
});

export let afterClass = css({
  ":after": {
    position: "absolute",
    top: "10px",
    left: "10px",
    zIndex: "100",
    transition: "250ms ease",
    color: "#ccc"
  }
});

export let focused = css({
  ":before": {
    width: "100% !important"
  },
  ":after": {
    top: "-10px !important",
    fontSize: "12px",
    color: "#81cde8 !important"
  }
});

// export {
//     focused,
//     afterClass,
//     searchTicket,
//     row,
//     right,
//     left,
//     formInputWrapper,
//     ticketFormWrapper,
//     searchBtn,
//     btn,
//     resultWrapper,
//     replyWrapper,
//     threadWrapper,
//     showTicket,
//     hideTicket,
//     noReply,
//     msg,
//     shadowed,
//     status,
//     showTooltip,
//     tooltip,
//     half
// }
