import { css } from "glamor";
import TicketService from "../../../../utils/ticketService.js";
import currentDomain from "../../../../utils/domain.js";
import request from "axios";
import {
  focused,
  afterClass,
  searchTicket,
  row,
  right,
  left,
  formInputWrapper,
  ticketFormWrapper,
  searchBtn,
  btn,
  resultWrapper,
  replyWrapper,
  threadWrapper,
  showTicket,
  hideTicket,
  noReply,
  msg,
  shadowed,
  status,
  showTooltip,
  tooltip,
  half
} from "./styles";
import { scrollClass } from "../../../NewSkin/Styles.js";

const GET_TICKET_URL = `${currentDomain}/api/ticket/getTickets/`;
const GET_REPLIES_URL = `${currentDomain}/api/ticket/getTicketReplies/`;

const ticket = new TicketService();

export default class CheckTicketStatus extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchTicket: "",
      searchValue: "",
      ticketReplies: [],
      searchedTicketData: [],
      ticketRepliesUpdated: false,
      tnum: "",
      tickets: "",
      replies: [],
      ticketArr: [],
      typing: "",
      showTicket: false,
      statusTooltip: "",
      open: "",
      pending: "",
      resolved: "",
      closed: "",
      changeTooltip: false,
      showTooltip: false,
      showTooltipContent: false,
      statusColor: "",
      timer: null
    };
  }

  componentDidMount() {
    this.tooltips();
  }

  tooltips = () => {
    this.setState({
      open:
        "Newly created tickets are marked as Open. CloudHealthAsia Support team is yet to resolve the report and will contact you shortly for additional information.",
      pending: "Ticket is awaiting reply from a customer or any 3rd party.",
      resolved:
        "Support Team is reasonably sure that they have provided the customer with a solution to the problem reported.",
      closed:
        "Resolved tickets are changed to Closed Status once customer has acknowledged that the error or problem has been solved. Resolved tickets will be automatically be closed after 48 or 72 hours even without the customer’s acknowledgement ."
    });
  };

  // REQUESTS
  // Get ticket
  _getTickets = tnum => {
    request
      .get(`${GET_TICKET_URL}${tnum}`)
      .then(res => {
        setTimeout(() => {
          if (res.data.length < 1) {
            setTimeout(() => {
              this.setState({
                typing: "No ticket exists",
                tickets: "",
                ticketArr: "",
                showTicket: false
              });
            }, 1);
          } else {
            this.setState({
              typing: "Ticket found.",
              tickets: res.data[0],
              ticketArr: res.data,
              showTicket: true
            });

            this.statusTooltip();
          }
        }, 1);
      })
      .catch(err => {
        console.log(err);
      });
  };

  // Get Ticket replies of a ticket
  _getTicketReplies = tnum => {
    request
      .get(`${GET_REPLIES_URL}${tnum}`)
      .then(res => {
        this.setState({
          replies: res.data
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  _handleFocus = e => {
    e.target.parentNode.classList.add(focused);
  };

  _handleBlur = e => {
    if (e.target.value.length <= 0) {
      e.target.parentNode.classList.remove(focused);
    }
  };

  _handleChange = e => {
    let el = e.target.value;

    const set_timeout = setTimeout(() => {
      if (el) {
        this.setState({ showTicket: false });
        clearTimeout(set_timeout);
        clearTimeout(this.state.timer);
        this.setState({
          typing: "Searching...",
          tnum: el
        });
        let timer = setTimeout(() => {
          this._getTickets(this.state.tnum);
          this._getTicketReplies(this.state.tnum);
        }, 1000);

        this.setState({
          timer
        });
      }
    }, 1);
  };

  _handleSubmit = e => {
    e.preventDefault();

    setTimeout(() => {
      this._getTickets(this.state.tnum);
      this._getTicketReplies(this.state.tnum);
    }, 500);
  };

  statusTooltip = () => {
    let status = this.state.tickets.status;

    switch (status) {
      case "Open":
        this.setState({
          statusTooltip: this.state.open,
          statusColor: "green"
        });
        break;

      case "Pending":
        this.setState({
          statusTooltip: this.state.pending,
          statusColor: "orange"
        });
        break;

      case "Resolved":
        this.setState({
          statusTooltip: this.state.resolved,
          statusColor: "blue"
        });
        break;

      case "Closed":
        this.setState({
          statusTooltip: this.state.closed,
          statusColor: "grey"
        });
        break;

      default:
        break;
    }
  };

  _handleMouseEnter = () => {
    this.setState({
      showTooltip: true
    });

    setTimeout(() => {
      this.setState({
        showTooltipContent: true
      });
    }, 50);
  };

  _handleMouseLeave = () => {
    this.setState({
      showTooltipContent: false
    });

    setTimeout(() => {
      this.setState({
        showTooltip: false
      });
    }, 210);
  };

  dtConvert = datetime => {
    let splited = datetime.split("T");
    let date = splited[0];
    let time = splited[1].split(".000Z");

    // Convert Date
    let newDate = new Date(date).toUTCString().split(" ");
    let NewDate = newDate[2] + " " + newDate[1] + ", " + newDate[3];

    // Convert Time
    let cTime = time[0]
      .toString()
      .match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (cTime.length > 1) {
      cTime = cTime.slice(1);
      cTime[5] = +cTime[0] < 12 ? " AM" : " PM";
      cTime[0] = +cTime[0] % 12 || 12;
    }
    let cnvTime = cTime.join("");

    return NewDate + " " + cnvTime;
  };

  render() {
    return (
      <section className={ticketFormWrapper}>
        {/* <h2>Check Ticket Status</h2> */}
        <form onSubmit={this._handleSubmit}>
          <section className={row}>
            <article
              className={[
                formInputWrapper,
                left,
                searchTicket,
                afterClass
              ].join(" ")}
            >
              <input
                ref={searchTicket => (this.searchTicket = searchTicket)}
                onChange={this._handleChange}
                onBlur={this._handleBlur}
                onFocus={this._handleFocus}
                type="text"
                required
              />
            </article>
            <button className={[btn, searchBtn, right].join(" ")}>
              Search
            </button>
          </section>
        </form>


        <section className={scrollClass} style={{ height: this.props.modalContentHeight - 63, overflow: 'auto' }}>
          {
            this.state.showTicket &&
            <article
              className={[
                resultWrapper,
                this.state.showTicket ? showTicket : hideTicket
              ].join(" ")}
            >
              {this.state.ticketArr.length > 0 ? (
                <div className={[threadWrapper].join(" ")}>
                  <small className="name">
                    <strong>{this.state.tickets.fullname}</strong> |{" "}
                    {this.state.tickets.company_name}
                  </small>
                  <h3>
                    Subject: <span>{this.state.tickets.subject}</span>
                  </h3>

                  <p className={half}>
                    <label>Concern:</label>{" "}
                    <span>{this.state.tickets.concern}</span>
                  </p>

                  <p className={half}>
                    <label>Status:</label>
                    <span
                      className={[status, this.state.statusColor].join(" ")}
                      onMouseEnter={this._handleMouseEnter}
                      onMouseLeave={this._handleMouseLeave}
                    >
                      {this.state.tickets.status}
                      {this.state.showTooltip ? (
                        <span
                          className={[
                            "statTooltip",
                            tooltip,
                            this.state.showTooltipContent ? showTooltip : null
                          ].join(" ")}
                        >
                          {this.state.statusTooltip}
                        </span>
                      ) : null}
                    </span>{" "}
                  </p>

                  <p className={msg}>
                    Message: <span>{this.state.tickets.message}</span>
                  </p>

                  <ul className={this.state.replies.length > 3 ? shadowed : null}>
                    {this.state.replies.length > 0 ? (
                      this.state.replies.map((reply, i) => {
                        return (
                          <li key={i} className={replyWrapper}>
                            <p> {reply.reply_message}</p>
                            <small>
                              Replied: {this.dtConvert(reply.datetime_reply)} |
                              CloudHealthAsia Admin
                        </small>
                          </li>
                        );
                      })
                    ) : (
                        <p className={noReply}>No reply yet.</p>
                      )}
                  </ul>
                </div>
              ) : null}
            </article>
          }
          {this.state.ticketArr.length > 0 ? "" : <h3>{this.state.typing}</h3>}
        </section>
      </section>
    );
  }
}
