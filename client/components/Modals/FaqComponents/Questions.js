import { css } from 'glamor'

let highlight = css({
    color: '#d383b6'
})

const questions = {
    MyProfile: [],

    MyAccount: [
        {
            question: `I forgot my password and can't log in`,
            answer: (
                <p>
                    <span>You can reset your password by clicking the</span> <strong className={highlight}>I forgot my Password</strong>{' '}
                    <span>link found on the Login Page.</span>
                </p>
            )
        },
        {
            question: `I can't log in. The website prompts Access Denied`,
            answer: (
                <p>
                    <ul>
                        <li>Your account is already inactive due to revoked access, or</li>
                        <li>your company's subscription to CloudHealthAsia is already inactive.</li>
                    </ul>
                </p>
            )
        },
        {
            question: 'My profile name enrolled to the system is wrong. How do i change it?',
            answer: (
                <p>
                    <span>You may edit your profile details in the </span>
                    <strong className={highlight}>Manage My Account</strong> module. Log-in to your account then go to{' '}
                    <strong className={highlight}>Manage My Account &#8594; Account </strong>
                    <span>
                        Some fields in the profile module are locked. If you wish to change them, contact your HR officer so they can change it for
                        you.
                    </span>
                </p>
            )
        }
    ],

    MyHealthProfile: [
        {
            question:
                'I was advised to complete my health survey, however, upon logging in to my account, the website prompts that survey is already completed. What should I do?',
            answer: (
                <p>
                    If you have the details on which survey(s) you were tagged as incomplete, go to the survey form, answer the unanswered item(s)
                    then submit the form by clicking <strong className={highlight}>Update My Profile</strong>
                </p>
            )
        },
        {
            question: 'I accidentally clicked the "submit" button and the health survey is already locked but i wish to change some answers.',
            answer: (
                <p>
                    If you accidentally submitted your profile without reviewing it, you may ask your HR account officer to unlock your profile so you
                    can edit your answers. Unlocking of profiles are under your HR officer's discretion.
                </p>
            )
        }
    ],

    "Accessibility/Compatibility": [
        {
            question:
                'I have problems using the platform thru my mobile phone.',
            answer: (
                <p>
                    CloudHealthAsia is not yet compatible with mobile. We are working hard to make it available in the near future. Kindly use your laptop or desktop in the mean time.
                </p>
            )
        },
    ]
}

export default questions
