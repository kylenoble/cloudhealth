import React, { useState, useEffect, useContext } from 'react';
import { combined, grid_wrapper, contained, centered, on_mobile } from '../../NewSkin/Styles';
import { css } from 'glamor';
import Colors from '../../NewSkin/Colors';
import Button from '../../NewSkin/Components/Button';
import { UserContext } from '../../../context/UserContext';

const NavTab = (props) => {
  const [activeTab, setActiveTab] = useState('faq');
  const [tabs, setTabs] = useState([]);
  const ctx = useContext(UserContext)
  let isMounted = false

  useEffect(() => {
    isMounted = true;

    if (isMounted) {
      console.log(ctx);

    }
  }, [])

  const setTab = (e) => {
    const tab = e.target.id;
    props.tabChanged(tab);
    setActiveTab(tab);

    switch (tab) {
      case 'faq':
        setTabs(props.faqTabs);
        document.querySelector('#faq').classList.add(activeMainTab);
        document.querySelector('#help').classList.remove(activeMainTab);
        break;
      case 'help':
        setTabs(props.helpTabs);
        document.querySelector('#help').classList.add(activeMainTab);
        document.querySelector('#faq').classList.remove(activeMainTab);
        break;
      default:
        setTabs(props.faqTabs);
        break;
    }

  };

  const handleSecondaryTab = e => {
    const button = e.target.parentNode;
    const spans = document.getElementsByClassName(secondaryTab);

    for (let i = 0; i < spans.length; i++) {
      const span = spans[i];
      const parent = span.parentNode
      parent.classList.remove(active)
    }
    button.classList.add(active)

  }

  const onMobile = ctx.screenSize === 'xsmall' || ctx.screenSize === 'small';

  return (
    <section className={combined([contained])}>
      <article className={combined([wrapper, centered])}>
        <div id="faq" className={combined([mainTab])} onClick={setTab}>
          <img className={combined([icon], on_mobile([], { display: 'none' }))} src="/static/icons/info.svg" alt="" />
          Frequently Asked Questions
        </div>
        <div id="help" className={combined([mainTab])} onClick={setTab}>
          <img className={combined([icon], on_mobile([], { display: 'none' }))} src="/static/icons/customer-support.svg" alt="" />
          Help and Support
        </div>
      </article>

      <article>
        <ul className={combined(
          [tabListWrapper(activeTab === 'faq' ? onMobile ? 2 : 3 : 2), on_mobile([], { height: 'fit-content' })])}>
          {
            tabs.map(tabs => {
              return (
                <li key={tabs.id}>
                  <Button styles={{ minWidth: 'fit-content', width: '100%' }}>
                    <span
                      id={tabs.id}
                      onClick={(e) => {
                        handleSecondaryTab(e)
                        activeTab === 'faq' ? props.showFaq(e) : props.showHelpAndSupport(e)
                      }}
                      className={secondaryTab}
                    >
                      {tabs.title}
                    </span>
                  </Button>
                </li>
              )
            })
          }
        </ul>
      </article>
    </section>
  );
};

const active = css({
  backgroundColor: `${Colors.pink} !important`,
  color: 'white !important',
  '&:hover': {
    borderColor: `${Colors.pinkDarkAccent} !important`,
    boxShadow: `0 5px 10px ${Colors.pink}60`
  }
})

const secondaryTab = css({
  padding: '15px 20px'
});

const tabListWrapper = col => css(grid_wrapper, centered, {
  gridTemplateColumns: `repeat(${col}, 1fr)`,
  listStyle: 'none',
  height: 46,
  margin: '16px 0',
  '> li': {
    display: 'grid',
    placeContent: 'center'
  }
});

const icon = css({
  width: 25,
  marginRight: 10,
  transform: 'scale(.8)'
});

const activeMainTab = css({
  borderBottom: `3px solid ${Colors.pink} !important`,
  color: `${Colors.pink} !important`,
  ':hover': {
    borderBottomColor: Colors.pink
  },
  '> img': {
    transform: 'scale(1)'
  }
})

const mainTab = css({
  transition: '300ms ease',
  borderBottom: '3px solid white',
  cursor: 'pointer',
  ':hover': {
    borderBottomColor: '#eee',
    color: 'black',
    '> img': {
      opacity: 1
    }
  },
  '> img': {
    transition: 'transform 300ms ease',
    opacity: 0.8,
  }
});

const wrapper = css(grid_wrapper, {
  gridTemplateColumns: '1fr 1fr',
  borderBottom: `1px solid ${Colors.pink}`,
  '> div': {
    padding: 15,
    color: '#555'
  }
});

export default NavTab;