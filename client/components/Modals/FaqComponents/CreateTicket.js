/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable no-undef */
/* eslint-disable no-unused-expressions */
/* eslint-disable react/sort-comp */
/* eslint-disable import/newline-after-import */
/* eslint-disable import/imports-first */
import React from "react";
import { css } from "glamor";
import currentDomain from "../../../utils/domain.js";
import request from "axios";
import path from "path";
import moment from "moment-timezone";
import Button from "../../NewSkin/Components/Button.js";
import { scrollClass, font } from "../../NewSkin/Styles.js";
const CLOUDINARY_UPLOAD_URL = `${currentDomain}/api/ticket/uploadAttachment`;
const GET_COMPANY_URL = `${currentDomain}/api/ticket/getActiveCompanies`;
const CREATE_TICKET_URL = `${currentDomain}/api/ticket`;
const REMOVE_ATTACHMENT_URL = `${currentDomain}/api/ticket/removeAttachment/`;
const SEND_EMAIL_URL = `${currentDomain}/api/ticket/ticketEmail/`;

export default class CreateTicket extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ticket_num: "",

      fullname: "",
      email: "",
      company: "",
      concern: "",
      subject: "",
      message: "",
      attachment: "",
      concern_title: "",

      fileUpload: false,
      filename: "",
      fullPath: "",
      companies: [],
      cantSubmit: true,

      newTicket: {
        ticket_number: "",
        name: "",
        email: "",
        company: "",
        companyName: "",
        concern: "",
        concern_title: "",
        subject: "",
        message: "",
        attachment: ""
      },

      tickets: [],
      bigFilename: "",
      doneSubmitting: false,
      attachmentForUpload: "",
      uploadedAttachmentFilename: "",
      viewSelectedAttachment: false,
      recentAttachmentFilename: "",
      recentAttachmentPath: "",
      attachmentErrMsg: "",
      longFilename: "",
      otherCompanyIsSelected: false,
      otherCompanyName: "",
      otherCompanyId: ""
    };
  }

  // REQUESTS
  // Creating New Ticket
  _creatingTicket = ticketData => {
    request
      .post(CREATE_TICKET_URL, ticketData)
      .then(() => {
        if (this.state.filename.length > 0) {
          this._handleRemoveAttachment();
        }
        console.log("Ticket successfully sent.");
      })
      .catch(err => {
        console.log(err);
      });
  };

  // Get active companies
  _getActiveCompanies = () => {
    request
      .get(GET_COMPANY_URL)
      .then(res => {
        this.setState({
          companies: res.body
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  // Attachment uploading
  _handleImageUpload = file => {
    const date = this._yyyymmdd();
    const time = this._getCurrentTime();
    const prefix = date + time;

    const fd = new FormData();
    let upload = request;
    fd.append("prefix", prefix);
    fd.append("file", file);

    upload = request.post(CLOUDINARY_UPLOAD_URL, fd, {
      onUploadProgress: ProgressEvent => {
        console.log(
          `Upload Progress ${Math.round(
            (ProgressEvent.loaded / ProgressEvent.total) * 100
          )}`
        );
      }
    });

    upload.then(response => {
      if (response.status === 200) {
        setTimeout(() => {
          this.setState({
            recentAttachmentPath: `../../../static/ticket_attachments/${
              this.state.recentAttachmentFilename
            }`
          });
        }, 1);
        if (response.status === 200) {
          console.log("Attachment successfully uploaded.");
        }
      }
    });
  };

  // Removing Attachment
  _handleRemoveAttachment = () => {
    const recent = this.state.recentAttachmentFilename;
    request
      .get(REMOVE_ATTACHMENT_URL + recent)
      .then(() => {
        console.log("Attachment removed successfully!");
      })
      .catch(err => {
        console.log(err);
      });
  };

  // Sending email confirmation
  _sendEmailConfirmation = () => {
    const recipient = this.state.newTicket.email;
    const name = this.state.newTicket.name;
    const ticket_number = this.state.newTicket.ticket_number;
    const concern_title = this.state.concern_title;

    const emailData = { recipient, name, ticket_number, concern_title };

    request
      .post(SEND_EMAIL_URL, emailData)
      .then(() => {
        console.log("Confirmation Email successfully sent.");
      })
      .catch(err => {
        console.log(err);
      });
  };

  componentDidMount() {
    setTimeout(() => {
      this._getActiveCompanies();
    }, 100);
  }

  _yyyymmdd = () => {
    const now = new Date();
    const y = now.getFullYear();
    const m = now.getMonth() + 1;
    const d = now.getDate();
    const mm = m < 10 ? `0${m}` : m;
    const dd = d < 10 ? `0${d}` : d;
    return `${y}${mm}${dd}`;
  };

  _handleFocus = e => {
    e.target.parentNode.classList.add(focused);
  };

  _handleBlur = e => {
    if (e.target.value.length <= 0) {
      e.target.parentNode.classList.remove(focused);
    }
  };

  _handleUpload = e => {
    e.preventDefault();
    e.target.previousSibling.click();
  };

  // Submitting New Ticket
  _handleSubmittingTicket = () => {
    // e.preventDefault()
    this._creatingTicket(this.state.newTicket);
    this._sendEmailConfirmation();

    setTimeout(() => {
      if (this.state.attachmentForUpload) {
        this._handleImageUpload(this.state.attachmentForUpload);
      }
      this._handleDoneSubmit();
    }, 100);
  };

  _checkExtname = e => {
    const date = this._yyyymmdd();
    const time = this._getCurrentTime();
    const file = e.target.files[0];
    const filename = file.name;
    const recentFile = `${date + time}-${filename}`;
    let longFilename = "";

    // Checking if the attachment's filename is greater than 50 chars
    filename.length > 50
      ? (longFilename = `${filename.substr(0, 50)}...`)
      : (longFilename = filename);

    // Allowed file size
    const filesize = 500000;
    const sizeOfFile = file.size;
    // Check file size
    const checkFileSize = sizeOfFile <= filesize;
    // Allowed Filetypes
    const filetypes = /jpeg|jpg|png|gif/;
    // Check extension
    const extname = filetypes.test(path.extname(filename).toLowerCase());
    // Check mimetype
    const mimetype = filetypes.test(file.type);
    // Check if ext and mimetype is true
    if (extname && mimetype) {
      if (checkFileSize) {
        this.setState({
          fileUpload: true,
          filename: longFilename,
          longFilename: filename,
          bigFilename: "",
          attachmentForUpload: e.target.files[0],
          recentAttachmentFilename: recentFile
        });

        setTimeout(() => {
          this._handleImageUpload(this.state.attachmentForUpload);
        }, 1);
      } else {
        this.setState({
          bigFilename: e.target.files[0].name,
          filename: "",
          attachmentErrMsg: " exceeds the maximum upload size of 500kB."
        });
      }
    } else {
      this.setState({
        bigFilename: e.target.files[0].name,
        attachmentErrMsg: " is not an image. Only images are allowed."
      });
    }
  };

  _getCurrentTime = () => {
    const time = new Date();
    const hr = time.getHours();
    const min = time.getMinutes();
    const sec = time.getSeconds();
    return [hr, min, sec].join("");
  };

  // ----
  _handleChooseFile = e => {
    const file = e.target.files[0];
    this.setState({
      attachmentForUpload: file
    });

    this._checkExtname(e);

    const el = e.target.value;

    setTimeout(() => {
      this.setState({
        uploadedAttachmentFilename: el
      });
      this._handleChange();
    }, 1);
  };

  // removing attachment
  _removeFile = e => {
    e.preventDefault();
    this._handleRemoveAttachment();

    const fileInput = e.target.parentNode.parentNode.nextSibling;
    fileInput.value = "";

    setTimeout(() => {
      this.setState({
        attachmentForUpload: "",
        fileUpload: false,
        fullPath: "",
        uploadedAttachmentFilename: "",
        recentAttachmentFilename: ""
      });
    }, 1);
  };

  _handleDoneSubmit = () => {
    const ticketForm = document.querySelector("#ticket-form");
    const inputWrapper = document.querySelectorAll("#input-wrapper");
    ticketForm.reset();

    for (let i = 0; i < inputWrapper.length; i++) {
      inputWrapper[i].classList.remove(focused);
    }

    this.setState({
      fileUpload: false,
      filename: "",
      cantSubmit: true,
      doneSubmitting: true
    });
  };

  _handleChange = () => {
    if (
      this.fullname.value.length > 0 &&
      this.email.value.length > 0 &&
      this.company.value.length > 0 &&
      this.concern.value.length > 0 &&
      this.message.value.length > 0 &&
      this.subject.value.length > 0
    ) {
      this.setState({
        cantSubmit: false
      });
    } else {
      this.setState({
        cantSubmit: true
      });
    }

    const concern = this.concern.value;

    this.setState({
      concern_title: concern,
      newTicket: {
        ticket_number: this.props.ticketNumber,
        name: this.fullname.value,
        email: this.email.value,
        company: this.company.value,
        concern: this.concern.value,
        subject: this.subject.value,
        message: this.message.value,
        attachment: this.state.filename,
        date_created: moment().toISOString()
      }
    });
  };

  _handleCloseNotif = e => {
    const id = e.target.id;
    if (id === "fileNotif") {
      this.setState({
        bigFilename: ""
      });
    } else {
      this.setState({
        doneSubmitting: false,
        newTicket: {
          ticket_number: "",
          name: "",
          email: "",
          company: "",
          concern: "",
          subject: "",
          message: "",
          attachment: ""
        }
      });
    }
  };

  _viewAttachment = () => {
    this.setState({
      viewSelectedAttachment: true
    });
  };

  _closeViewAttachment = () => {
    this.setState({
      viewSelectedAttachment: false
    });
  };

  onMouseDown = () => {
    this.props.latestTicket();
  };

  onMouseUp = e => {
    e.preventDefault();

    setTimeout(() => {
      console.log("on mouse Up", this.props.ticketNumber);
      this._handleChange();
      setTimeout(() => {
        this._handleSubmittingTicket();
      }, 1);
    }, 300);
  };

  render() {
    return (
      <div
        className={ticketFormWrapper}
        style={{ height: this.props.modalContentHeight - 1 }}
      >
        {// Viewing Added Attachment
        this.state.viewSelectedAttachment ? (
          <div className={viewAttachmentWrapper}>
            <button
              onClick={this._closeViewAttachment}
              className={[notifCloseBtn, viewing].join(" ")}
            >
              x
            </button>
            <figure>
              <img src={this.state.recentAttachmentPath} alt="" />
            </figure>
          </div>
        ) : null}
        {// Notification for Adding Attachment
        this.state.bigFilename.length > 1 ? (
          <div className={fileSizeNotif}>
            <article className={error}>
              <button
                id="fileNotif"
                onClick={this._handleCloseNotif}
                className={notifCloseBtn}
              >
                x
              </button>
              <label>
                <strong>{this.state.bigFilename}</strong>
                {this.state.attachmentErrMsg}
              </label>
            </article>
          </div>
        ) : null}

        {this.state.doneSubmitting ? (
          <div className={fileSizeNotif}>
            <article className={success}>
              <button
                id="emailNotif"
                onClick={this._handleCloseNotif}
                className={notifCloseBtn}
              >
                x
              </button>
              <p>
                An email confirmation was sent to{" "}
                <strong>{this.state.newTicket.email}.</strong>
              </p>
            </article>
          </div>
        ) : null}

        {/* <h2 className={font(1.2)}>Submit a Ticket</h2> */}
        <form
          id="ticket-form"
          className={submitTicketForm}
          onSubmit={this.onMouseUp}
          encType="multipart/form-data"
        >
          <section className={formWrapper}>
            <div className={row}>
              <article
                id="input-wrapper"
                className={[formInputWrapper, left, fname, afterClass].join(
                  " "
                )}
              >
                <input
                  ref={fullname => (this.fullname = fullname)}
                  onChange={this._handleChange}
                  onBlur={this._handleBlur}
                  onFocus={this._handleFocus}
                  type="text"
                  required
                />
              </article>

              <article
                id="input-wrapper"
                className={[formInputWrapper, right, email, afterClass].join(
                  " "
                )}
              >
                <input
                  ref={email => (this.email = email)}
                  onChange={this._handleChange}
                  onBlur={this._handleBlur}
                  onFocus={this._handleFocus}
                  type="email"
                  required
                />
              </article>
            </div>

            <div className={row}>
              <article
                id="input-wrapper"
                className={[formInputWrapper, left, company, afterClass].join(
                  " "
                )}
              >
                <input
                  ref={company => (this.company = company)}
                  onChange={this._handleChange}
                  onBlur={this._handleBlur}
                  onFocus={this._handleFocus}
                  type="text"
                />
              </article>

              <article
                id="input-wrapper"
                className={[formInputWrapper, right, concern, afterClass].join(
                  " "
                )}
              >
                <select
                  ref={concern => (this.concern = concern)}
                  onChange={this._handleChange}
                  onBlur={this._handleBlur}
                  onFocus={this._handleFocus}
                >
                  <option value="" />
                  <option value="Technical Support">Technical Support</option>
                  <option value="Customer Service">Customer Service</option>
                  <option value="Sales">Sales</option>
                  <option value="Billing">Billing</option>
                </select>
              </article>
            </div>

            <div className={row}>
              <article
                id="input-wrapper"
                className={[formInputWrapper, left, subject, afterClass].join(
                  " "
                )}
              >
                <input
                  ref={subject => (this.subject = subject)}
                  onChange={this._handleChange}
                  onBlur={this._handleBlur}
                  onFocus={this._handleFocus}
                  type="text"
                />
              </article>
            </div>

            <div className={row}>
              <div id="messageWrapper" className={messageWrapper}>
                <h4 style={{ marginTop: "0px" }}>Message:</h4>
                <textarea
                  ref={message => (this.message = message)}
                  onChange={this._handleChange}
                  cols="30"
                  rows="3"
                />
              </div>
            </div>

            <div className={row}>
              {this.state.fileUpload ? (
                <div className={uploadedWrapper}>
                  <p className={[uploadedFile].join(" ")}>
                    <img
                      onClick={this._viewAttachment}
                      src={this.state.recentAttachmentPath}
                      alt=""
                    />

                    <span title={this.state.longFilename}>
                      {this.state.filename}
                    </span>

                    <button onClick={this._removeFile}>x</button>
                  </p>
                </div>
              ) : null}

              {this.state.fileUpload ? null : (
                <Button
                  type="pink"
                  styles={{ margin: "auto", display: "block" }}
                >
                  <input
                    name="file"
                    ref={attachment => (this.attachment = attachment)}
                    onChange={this._handleChooseFile}
                    accept=".jpg, .jpeg, .png"
                    multiple={false}
                    id="file-upload"
                    type="file"
                    name=""
                    id=""
                    hidden
                  />
                  <button
                    onClick={
                      this.state.fileUpload
                        ? this._handleFileUpload
                        : this._handleUpload
                    }
                  >
                    Choose File
                  </button>
                </Button>
              )}

              <Button
                type="blue"
                styles={{ margin: "10px auto 0px", display: "block" }}
              >
                <button
                  onMouseDown={this.onMouseDown}
                  disabled={this.state.cantSubmit}
                  className={[
                    // btn,
                    // submitBtn,
                    this.state.cantSubmit ? disabled : null
                  ].join(" ")}
                >
                  Submit Ticket
                </button>
              </Button>
            </div>
          </section>
        </form>
      </div>
    );
  }
}

let submitTicketForm = css({
  position: "relative"
});

let error = css({
  border: "2px solid orange",
  color: "orange"
});

let success = css({
  border: "2px solid #34db89",
  color: "#34db89"
});

let uploadedWrapper = css({
  // display: "inline-block"
  textAlign: "center"
});

let viewing = css({
  fontSize: "17px",
  zIndex: "101",
  ":hover": {
    color: "#81cde8"
  }
});

let viewAttachmentWrapper = css({
  position: "absolute",
  top: "0",
  left: "0",
  width: "100%",
  height: "100%",
  background: "rgba(0,0,0,.85)",
  zIndex: "100",
  overflow: "hidden",
  "> figure": {
    margin: "0px",
    height: "100%",
    width: "auto",
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    boxShadow: "0 0 32px rgba(0,0,0,.8)",
    "> img": {
      height: "100%"
    }
  }
});

let notifCloseBtn = css({
  background: "none",
  borderStyle: "none",
  fontWeight: "600",
  position: "absolute",
  top: "5px",
  right: "10px",
  color: "#ccc",
  cursor: "pointer",
  transition: "300ms ease",
  ":hover": {
    color: "black"
  }
});

let fileSizeNotif = css({
  width: "100%",
  height: "100%",
  background: "rgba(0,0,0,.5)",
  position: "absolute",
  left: "50%",
  top: "50%",
  transform: "translate(-50%, -50%)",
  zIndex: "100",
  "::after": {
    width: "100%",
    height: "100%",
    position: "absolute",
    top: "0",
    left: "0",
    background: "black"
  },
  "> article": {
    background: "white",
    width: "40%",
    padding: "30px",
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    boxShadow: "0 8px 32px rgba(0,0,0,.2)",
    borderRadius: "3px",
    textAlign: "center"
  }
});

let disabled = css({
  background: "#ccc !important",
  boxShadow: "none !important",
  color: "#eee",
  cursor: "default",
  opacity: ".5",
  ":hover": {
    opacity: ".5 !important"
  }
});

let uploadedFile = css({
  display: "inline-block",
  background: "white",
  margin: "0px",
  marginRight: "10px",
  border: "1px solid white",
  borderRadius: "3px",
  boxShadow: "0 2px 4px rgba(0,0,0,.1)",
  transition: "300ms ease, width 500ms ease",
  verticalAlign: "middle",
  width: "auto",
  overflow: "hidden",
  maxHeight: "43px",
  position: "relative",
  "> img": {
    height: "43px",
    display: "inline-block",
    verticalAlign: "middle",
    cursor: "pointer"
  },
  ":hover": {
    borderColor: "#81cde8"
  },
  "> button": {
    position: "absolute",
    top: "50%",
    right: "10px",
    transform: "translateY(-50%)",
    background: "none",
    border: "none",
    cursor: "pointer",
    fontWeight: "bold",
    color: "#ccc",
    transition: "300ms ease",
    ":hover": {
      color: "#81cde8"
    }
  },
  "> span": {
    marginRight: "10px",
    cursor: "pointer",
    padding: "10px 20px",
    display: "inline-block",
    verticalAlign: "middle"
  }
});

let ticketFormWrapper = css(scrollClass, {
  overflow: "auto",
  paddingRight: 20,
  paddingBottom: 20,
  boxSizing: "border-box",
  "> h2": {
    margin: "10px 0px",
    borderBottom: "1px solid #81cde8",
    padding: "10px 0",
    color: "#81cde8",
    textTransform: "uppercase",
    display: "table",
    clear: "both",
    width: "100%"
  }
});

let formWrapper = css({
  width: "100%"
});

let row = css({
  width: "100%",
  display: "table",
  clear: "both",
  "@media screen and (max-width: 500px)": {
    "> article": {
      maxWidth: "100% !important",
      float: "left !important"
    }
  }
});

let focused = css({
  ":before": {
    width: "100% !important"
  },
  ":after": {
    top: "-10px !important",
    fontSize: "12px",
    color: "#81cde8 !important"
  }
});

let messageWrapper = css({
  margin: "15px 0px",
  "> h4": {
    color: "#81cde8",
    marginBottom: "10px"
  },
  "> textarea": {
    width: "100%",
    border: "1px solid #ccc",
    borderRadius: "3px",
    padding: "10px",
    transition: "250ms ease",
    boxSizing: "border-box",
    maxWidth: 798,
    ":focus": {
      outline: "none",
      borderColor: "#81cde8"
    }
  }
});

let formInputWrapper = css({
  width: "100%",
  maxWidth: "48%",
  padding: "5px",
  position: "relative",
  borderBottom: "2px solid #ccc",
  marginTop: "10px",
  marginBottom: "15px",
  verticalAlign: "bottom",
  boxSizing: "border-box",
  "> input": {
    border: "none",
    padding: "5px",
    paddingBottom: "0px",
    width: "100%",
    position: "relative",
    zIndex: "101",
    background: "none",
    ":invalid": {
      outline: "none",
      boxShadow: "none",
      color: "red"
    },
    ":focus": {
      outline: "none"
    }
  },
  "> select": {
    border: "none",
    padding: "5px 0px",
    paddingLeft: "2px",
    paddingBottom: "0px",
    width: "100%",
    position: "relative",
    zIndex: "101",
    background: "none",
    ":focus": {
      outline: "none",
      border: "none"
    }
  },
  ":before": {
    content: '" "',
    position: "absolute",
    bottom: "-2px",
    left: "50%",
    height: "2px",
    width: "0%",
    background: "#81cde8",
    transform: "translateX(-50%)",
    transition: "250ms ease"
  }
});

let left = css({
  float: "left"
});

let right = css({
  float: "right"
});

let afterClass = css({
  ":after": {
    position: "absolute",
    top: "10px",
    left: "10px",
    zIndex: "100",
    transition: "250ms ease",
    color: "#ccc"
  }
});

let fname = css({
  ":after": {
    content: '"Name"'
  }
});

let company = css({
  ":after": {
    content: '"Company"'
  }
});

let email = css({
  ":after": {
    content: '"Email Address"'
  }
});

let concern = css({
  ":after": {
    content: '"Type of Concern"'
  }
});

let subject = css({
  maxWidth: "100% !important",
  ":after": {
    content: '"Subject"'
  }
});
