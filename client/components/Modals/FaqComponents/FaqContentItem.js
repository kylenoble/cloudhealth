import { css } from 'glamor'

export default class CollapseItem extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            collapsibleToggle: false,
            faqContentData: this.props.faqContent
        }

        this.handleToggle = this.handleToggle.bind(this)
    }

    handleToggle() {
        this.setState({
            collapsibleToggle: !this.state.collapsibleToggle
        })
    }

    render() {
        return (
            <li id="collapsible-item" className={[collapsibleItem, this.state.collapsibleToggle ? active : null].join(' ')}>
                <div
                    onClick={this.handleToggle}
                    className={[collapsibleHeader, this.state.collapsibleToggle ? activeHeader : null].join(' ')}>
                    {this.state.faqContentData.question}
                </div>

                <div
                    className={[contentWrapper, this.state.collapsibleToggle ? openCollapsible : closeCollapsible].join(' ')} >
                    <div className={collapsibleContent}>{this.state.faqContentData.answer}</div>
                </div>
            </li>
        )
    }
}

let hovered = '#3498db !important'

let blue = '#d383b6 !important'

let activeHeader = css({
    background: blue,
    color: 'white'
})

let active = css({
    borderColor: blue
})

let contentWrapper = css({
    maxHeight: '0px',
    transition: 'max-height 500ms ease',
    overflow: 'hidden',
    float: 'left',
    width: '100%'
})

let openCollapsible = css({
    maxHeight: '1000px !important'
})

let closeCollapsible = css({
    maxHeight: '0px !important'
})

let collapsibleItem = css({
    width: '100%',
    marginBottom: '20px',
    boxShadow: '0 1px 5px rgba(85, 85, 85, 0.1)',
    background: '#fff',
    border: '1px solid #ccc',
    display: 'table',
    clear: 'both'
})

let collapsibleHeader = css({
    float: 'left',
    width: '100%',
    padding: '10px',
    cursor: 'pointer',
    transition: '250ms ease',
    position: 'relative',
    background: '#f6f6f6',
    fontWeight: '600',
    boxSizing: 'border-box',
    marginTop: '0px'
    , ':hover': {
        background: '#eee'
    }
})


let collapsibleBody = css({
    transition: '300ms ease',
    height: 'auto',
    overflow: 'hidden'
})

let collapsibleContent = css({
    padding: '15px',
    margin: '0px',
    '> p': {
        margin: '0px'
    }
})

const collapsible = {
    body: {
        transition: '300ms ease',
        height: 'auto',
        overflow: 'hidden'
    }
}