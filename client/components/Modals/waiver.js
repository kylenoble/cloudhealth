/* eslint-disable react/react-in-jsx-scope */
import React from 'react';
import Modal from "react-modal";
// import { css } from "glamor";
import ModalWrapper, { ModalContentWrapper, ModalFooter, ModalHeader, ModalContent } from '../NewSkin/Wrappers/Modal/';
import { modalCustomStyles, font } from "../NewSkin/Styles";
import Button from '../NewSkin/Components/Button';

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: this.props.modalIsOpen
    };

    this.closeModal = this.closeModal.bind(this);
  }

  closeModal(val) {
    this.props.updateStatus(val);
    this.props.closeModal("waiverModalIsOpen");
  }

  _openPrivacyPolicy() {
    this.props.openModal("privacyPolicyModalIsOpen");
  }

  render() {
    return (
      <Modal
        isOpen={this.props.modalIsOpen}
        onAfterOpen={this.afterOpenModal}
        onRequestClose={this.closeModal}
        shouldCloseOnOverlayClick={false}
        style={customStyles}
        ariaHideApp={false}
      >
        <ModalWrapper>
          <ModalContentWrapper>
            <div style={styles.letterStyle} className="modalContainer">
              <ModalHeader styles={{ fontSize: '1em' }}>
                <h5 style={styles.title}>DATA PRIVACY CONSENT/AGREEMENT </h5>
              </ModalHeader>

              <ModalContent>
                <p>
                  CloudHealthAsia recognizes its responsibilities under the Republic
                  Act No. 10173 , also known as the Data Privacy Act of 2012, with
                  respect to the data it collects, records, organizes, updates, uses
                  and consolidates from its clients. The Personal data obtained from
                  this portal is entered and stored within CloudHealthAsia's
                  information and communication system and will only be accessible to
                  the patient and the CloudHealthAsia team (Doctors, system
                  administrator and medical practitioners).</p>
                <br />
                <p>
                  Your company has enrolled you to CloudHealthAsia as part of your
                  corporate wellness program. Your personal data in this system is
                  only accessible to you and the CloudHealthAsia team. Your company
                  however, can request for your health profile which may be used in
                  enrolling you to different health packages or for administrative
            purposes.{" "}
                  <strong>
                    Sharing your information with the company would allow them to view
                    your personal profile, health risk scores and health progress.
                    Doctor’s notes and other documents covered by doctor-patient
                    confidentiality are NOT in any case covered in this waiver and
                    would be subject to your approval upon submission of a formal
                    request.
            </strong>{" "}
                  Should you decide not to disclose your profile, your identity will
                  be concealed to your company. Health data however, will still be
                  used in aggregate reports and analysis.
          </p><br />
                <p>Do you wish to share your data to your company?</p>
              </ModalContent>
              {/* <p style={styles.btn}>

          </p> */}
            </div>
          </ModalContentWrapper>
          <ModalFooter styles={{ textAlign: 'center' }}>
            <Button
              type="pink"
              classes={[font(0.9)]} styles={{ maxWidth: 'auto', marginTop: 0, width: '100%' }}
            >
              <button
                onClick={() => this.closeModal(1)}
              >
                Yes, I am allowing my company to access my personal health profile
            </button>
            </Button>

            <Button
              type="pink"
              classes={[font(0.9)]}
              styles={{ maxWidth: 'auto', marginTop: 10, width: '100%' }}
            >
              <button onClick={() => this.closeModal(4)} >No, I want to remain anonymous</button>
            </Button>

          </ModalFooter>
        </ModalWrapper>
      </Modal>
    );
  }
}

// glamos styles
// const link = css({
//   cursor: "pointer",
//   color: "#000",
//   ":hover": {
//     borderRadius: "10px",
//     padding: "5px",
//     border: "1px solid #3498db",
//     color: "#3498db"
//   }
// });

// jsx style
const customStyles = {
  ...modalCustomStyles
  // content: {
  //   width: "75%",
  //   height: "86vh",
  //   top: "56%",
  //   left: "50%",
  //   right: "auto",
  //   bottom: "auto",
  //   marginRight: "-50%",
  //   transform: "translate(-50%, -50%)",
  //   padding: "0",
  //   boxShadow: "-3px 3px 30px #424242",
  //   lineHeight: "1.8"
  // }
};
const styles = {
  letterStyle: {
    textAlign: "justify",
    // padding: "20px 35px",
    fontSize: 18
  },
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    flexWrap: "wrap",
    maxWidth: "40em",
    marginTop: "80px",
    color: "#364563",
    lineHeight: "34px"
  },
  dashItem: {
    fontFamily: "Roboto-Medium",
    fontSize: "18px",
    color: "#BDC7CF",
    letterSpacing: 0,
    lineHeight: "14px",
    padding: "20px 20px",
    margin: "-1px 25px",
    cursor: "pointer"
  },
  dashItemActive: {
    color: "#358ED7",
    borderBottom: "2px solid #358ED7"
  },
  button: {
    backgroundColor: "#77E9AF",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "30px 0px 20px 0px",
    cursor: "pointer"
  },
  button_yes: {
    backgroundColor: "#3498db",
    fontFamily: '"Roboto",sans-serif',
    color: "white",
    fontSize: "15px",
    padding: "8px 25px 8px 25px",
    cursor: "pointer",
    borderRadius: "6px",
    fontWeight: "bold"
  },
  button_no: {
    backgroundColor: "#3498db",
    fontFamily: '"Roboto",sans-serif',
    color: "white",
    fontSize: "14px",
    padding: "8px 11px 8px 11px",
    cursor: "pointer",
    borderRadius: "6px"
  },
  title: {
    textAlign: "left",
    marginBottom: 20
  },
  btn: {
    textAlign: "center"
  }
};
