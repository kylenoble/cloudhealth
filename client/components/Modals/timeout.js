import React from "react";
import Modal from "react-modal";
import Router from "next/router";
import Colors from "../NewSkin/Colors";
// import { css } from "glamor";
import ModalWrapper, {
  ModalContentWrapper,
  ModalHeader,
  ModalContent,
  ModalFooter
} from "../NewSkin/Wrappers/Modal";
import { modalCustomStyles } from "../NewSkin/Styles";
import Button from "../NewSkin/Components/Button";
import { fontWeight, font } from "../NewSkin/Styles";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: this.props.modalIsOpen
    };

    this.closeModal = this.closeModal.bind(this);
  }

  closeModal() {
    Router.push("/login");
  }

  render() {
    return (
      <Modal
        isOpen={this.props.modalIsOpen}
        onAfterOpen={this.afterOpenModal}
        onRequestClose={this.closeModal}
        shouldCloseOnOverlayClick={false}
        style={customStyles}
        ariaHideApp={true}
      >
        <ModalWrapper>
          <ModalContentWrapper styles={{ height: "fit-content" }}>
            <ModalContent>
              <ModalHeader
                classes={[fontWeight(700), font(1.1)]}
                styles={{ color: Colors.skyblue, textAlign: "center" }}
              >
                You have been logged out due to inactivity
              </ModalHeader>
              <p>
                For security reasons, you are automatically logged out when you
                have been inactive for more than {this.props.duration} minutes.
              </p>
            </ModalContent>
          </ModalContentWrapper>

          <ModalFooter>
            <Button type="pink" styles={{ margin: "0 auto" }}>
              <button onClick={() => this.closeModal()}>Login</button>
            </Button>
          </ModalFooter>
        </ModalWrapper>
      </Modal>
    );
  }
}

const customStyles = {
  ...modalCustomStyles,
  zIndex: '99999 !important'
};
const styles = {
  letterStyle: {
    // textAlign: "justify",
    // padding: "20px 35px",
    // fontSize: "16px"
  },
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    flexWrap: "wrap",
    maxWidth: "40em",
    marginTop: "80px",
    color: "#364563",
    lineHeight: "34px"
  },
  dashItem: {
    fontFamily: "Roboto-Medium",
    fontSize: "18px",
    color: "#BDC7CF",
    letterSpacing: 0,
    lineHeight: "14px",
    padding: "20px 20px",
    margin: "-1px 25px",
    cursor: "pointer"
  },
  dashItemActive: {
    color: "#358ED7",
    borderBottom: "2px solid #358ED7"
  },
  button: {
    backgroundColor: "#77E9AF",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "30px 0px 20px 0px",
    cursor: "pointer"
  },
  button_yes: {
    backgroundColor: "#3498db",
    fontFamily: '"Roboto",sans-serif',
    color: "white",
    fontSize: "15px",
    padding: "8px 25px 8px 25px",
    cursor: "pointer",
    borderRadius: "6px",
    fontWeight: "bold"
  },
  button_no: {
    backgroundColor: "#3498db",
    fontFamily: '"Roboto",sans-serif',
    color: "white",
    fontSize: "14px",
    padding: "8px 11px 8px 11px",
    cursor: "pointer",
    borderRadius: "6px"
  },
  title: {
    textAlign: "center",
    fontWeight: "bold"
  },
  welcome: {
    fontWeight: "bold",
    fontSize: "18px"
  },
  btn: {
    textAlign: "center"
  }
};
