import React from "react";
import Modal from "react-modal";
import Colors from "../NewSkin/Colors";
// import { css } from "glamor";
import ModalWrapper, {
  ModalContentWrapper,
  ModalHeader,
  ModalContent,
  ModalFooter
} from "../NewSkin/Wrappers/Modal";
import { modalCustomStyles } from "../NewSkin/Styles";
import Button from "../NewSkin/Components/Button";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: this.props.modalIsOpen
    };

    this.closeModal = this.closeModal.bind(this);
  }

  closeModal() {
    this.props.closeModal("welcomeModalIsOpen");
  }

  render() {
    return (
      <Modal
        isOpen={this.props.modalIsOpen}
        onAfterOpen={this.afterOpenModal}
        onRequestClose={this.closeModal}
        shouldCloseOnOverlayClick={false}
        style={customStyles}
        ariaHideApp={false}
      >
        <ModalWrapper>
          <ModalContentWrapper>
            <div style={styles.letterStyle} className="modalContainer">
              <ModalHeader styles={{ padding: 0, fontSize: "1em" }}>
                <h3 style={{marginBottom: 20}}>Welcome to CloudHealthAsia!</h3>
              </ModalHeader>
              <ModalContent
                style={{
                  fontSize: "18px !important",
                  textAlign: "justify !important"
                }}
              >
                <p>
                  Congratulations on your first step to your journey towards
                  optimum health. Before you proceed, please read the Personal
                  Data Collection Statement below and agree to the terms and
                  conditions. The next page also contains the Data Privacy
                  Waiver for your review. Please read it carefully before
                  proceeding. Afterwhich, you are required to change your
                  password then you may start creating your health profile by
                  completing your personal information under 
                  <span style={{fontWeight: 500}}> Manage My Account </span> and 
                  by answering the health survey under <span style={{fontWeight: 500}}> My Health Profile</span>.
                </p>
                <br />
                <h4 style={styles.title}>
                  Personal and Health Information Collection Statement
                </h4>

                <p>
                  CloudHealthAsia is a joint venture between Romlas Holdings and
                  Meraki Social Ventures.
                </p>
                <br />

                <p>
                  You may contact CloudHealthAsia by writing to LifeScience
                  Institute 8F Accralaw Tower 30th street cor. 2nd ave. BGC
                  Taguig, or by emailing client.services@cloudhealthasia.com.
                  You have the right to gain access to the information held by
                  CloudHealthAsia about you.
                </p>
                <br />
                <p>
                  Our Privacy Policy contains information on how you may request
                  access to, and correction of, your personal information and
                  how you may complain about a breach of your privacy and how we
                  will deal with such a complaint.
                </p>
                <br />

                <p>
                  CloudHealthAsia needs to collect information about you for the
                  primary purpose of creating a personalized health and wellness
                  plan. In order to fully assess your health, we need to collect
                  some personal information from you. This information will also
                  be used for the administrative purposes of running the
                  practice such as billing you or through an insurer or
                  compensation agency. Information will be used within the
                  practice for handover if another practitioner provides you
                  with assistance.
                </p>
                <br />

                <p>
                  CloudHealthAsia may disclose information regarding existing
                  diagnosis and laboratory results to your Doctor or other
                  treatment providers only with your consent. In the case of
                  insurance or compensation claims, it may be necessary to
                  disclose information and/or collect information that affects
                  your treatment and return to work. CloudHealthAsia will not
                  disclose your information to commercial companies, however
                  specific service or product information as deemed suitable for
                  your management, may be forwarded to you by us, unless you
                  instruct CloudHealthAsia not to forward this type of
                  information. Data gathered however can be used in internal
                  reports of the company and may be used in researches and
                  publications, under standard protocols for research studies
                  and clinical trials upon consent of the client.
                </p>
                <br />

                <p>
                  Information at CloudHealthAsia is stored securely and only
                  practice staff has access to it. CloudHealthAsia takes all
                  reasonable steps to ensure that information collected about
                  you is accurate, complete and up-to-date. You may have access
                  to your information on request and if you believe that any of
                  the information is inaccurate, we may be able to amend it
                  accordingly. If you do not provide relevant personal or health
                  information, in part or in full, it may result in incomplete
                  assessment. This may impact on the wellness plan and the
                  interventions that will be provided. Any concerns that you may
                  have about this statement or about your management can be
                  directed to client.services@cloudhealthasia.com.{" "}
                </p>
              </ModalContent>
            </div>
          </ModalContentWrapper>
          <ModalFooter styles={{ textAlign: "center" }}>
            <ModalHeader
              styles={{
                textAlign: "center",
                marginBottom: 20,
                fontSize: "0.9em !important"
              }}
            >
              <p style={{ color: Colors.skyblue }}>
                {" "}
                Do you consent with your personal data being processed as
                described above? You must agree in order to create a health
                profile.
              </p>
            </ModalHeader>
            <Button
              type="pink"
              styles={{ margin: '0 auto' }}
            >
              <button onClick={this.closeModal}>I AGREE</button>
            </Button>
          </ModalFooter>
        </ModalWrapper>
      </Modal>
    );
  }
}

const customStyles = {
  ...modalCustomStyles
  // content: {
  //   width: "75%",
  //   height: "86vh",
  //   top: "56%",
  //   left: "50%",
  //   right: "auto",
  //   bottom: "auto",
  //   marginRight: "-50%",
  //   transform: "translate(-50%, -50%)",
  //   padding: "0",
  //   // boxShadow: "-3px 3px 30px #424242",
  //   lineHeight: "1.8",
  // }
};
const styles = {
  letterStyle: {
    // textAlign: "justify",
    // padding: "20px 35px",
    // fontSize: "16px"
  },
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    flexWrap: "wrap",
    maxWidth: "40em",
    marginTop: "80px",
    color: "#364563",
    lineHeight: "34px"
  },
  dashItem: {
    fontFamily: "Roboto-Medium",
    fontSize: "18px",
    color: "#BDC7CF",
    letterSpacing: 0,
    lineHeight: "14px",
    padding: "20px 20px",
    margin: "-1px 25px",
    cursor: "pointer"
  },
  dashItemActive: {
    color: "#358ED7",
    borderBottom: "2px solid #358ED7"
  },
  button: {
    backgroundColor: "#77E9AF",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "30px 0px 20px 0px",
    cursor: "pointer"
  },
  button_yes: {
    backgroundColor: "#3498db",
    fontFamily: '"Roboto",sans-serif',
    color: "white",
    fontSize: "15px",
    padding: "8px 25px 8px 25px",
    cursor: "pointer",
    borderRadius: "6px",
    fontWeight: "bold"
  },
  button_no: {
    backgroundColor: "#3498db",
    fontFamily: '"Roboto",sans-serif',
    color: "white",
    fontSize: "14px",
    padding: "8px 11px 8px 11px",
    cursor: "pointer",
    borderRadius: "6px"
  },
  title: {
    textAlign: "center",
    fontWeight: "bold",
    marginBottom: 20
  },
  welcome: {
    fontWeight: "bold",
    fontSize: "18px"
  },
  btn: {
    textAlign: "center"
  }
};
