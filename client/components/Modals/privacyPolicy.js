/* eslint-disable import/newline-after-import */
import React from 'react';
import Modal from 'react-modal';
import { css } from 'glamor';
import { modalCustomStyles } from '../NewSkin/Styles';
import ModalWrapper, { ModalHeader, ModalContent, ModalFooter, ModalContentWrapper } from '../NewSkin/Wrappers/Modal/';
import Button from '../NewSkin/Components/Button';
export default class extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: this.props.modalIsOpen
    };

    this.closeModal = this.closeModal.bind(this);
  }



  closeModal() {
    //this.props.updateStatus(1)
    this.props.closeModal('privacyPolicyModalIsOpen')
  }

  render() {
    return (
      <Modal
        isOpen={this.props.modalIsOpen}
        onAfterOpen={this.afterOpenModal}
        onRequestClose={this.closeModal}
        shouldCloseOnOverlayClick
        style={customStyles}
        ariaHideApp={false}
      >
        <ModalWrapper>
          <ModalContentWrapper>
            <div style={styles.letterStyle}>
              <ModalHeader styles={{ paddingLeft: 10 }}>
                <h3>CloudHealthAsia Privacy Policy</h3>
              </ModalHeader>
              {/* <h3><em>About our privacy policy</em></h3> */}

              <ModalContent styles={{ paddingLeft: 10 }}>
                <p> CloudHealthAsia recognizes the importance of keeping the personal information that you entrust to us private and confidential. This policy outlines how your personal information is handled and how CloudhealthAsia protects and uses the information that you provide through our platform. </p>

                <p> CloudHealthAsia is committed to following the general principles and rules of data privacy protection in the Philippines. We recognize are responsibilities under the Republic Act No. 10173 , also known as the Data Privacy Act of 2012, with respect to the data it collects, records, organizes, updates, uses and consolidates from its clients. By using our platform and submitting personal information through our platform, the user expressly consents to the collection, use and disclosure of the user’s personal information in accordance with this privacy policy. </p>
                <ol type='I'>
                  <li className={listStyle}>Information we collect
                  <p className={pStyle}>
                      We receive information from your company health manager and what you directly provide to us. In this privacy policy, we refer to all of this as the ‘information’.
                    The following are the information your company and you as the user provide to us.</p>
                    <p className={pStyle}>From the Company:</p>
                    <ul>
                      <li>Full Name</li>
                      <li>Birth date</li>
                      <li>Gender</li>
                      <li>Email Address</li>
                      <li>Company related details (Branch, Department, Job Grade or Level)</li>
                    </ul>
                    <p className={pStyle}>From the data collection and health survey in the platform:</p>
                    <ul>
                      <li>Ethnicity</li>
                      <li>Civil Status</li>
                      <li>Health information (Past diagnoses, current symptoms, anthropometrics, lifestyle habits) pertinent to the creation of a health profile</li>
                    </ul>
                  </li>

                  <li className={listStyle}>How we use the information
                  <p className={pStyle}> We use all the information to:</p>
                    <ol>
                      <li>Generate the user’s individual health profile that would be used in creating his personalized health plan </li>
                      <li>Generate corporate aggregate reports that would be used in creating the appropriate health and wellness plan for your company. </li>
                      <li>Generate epidemiological data for in-house researches and analysis following research protocols. </li>
                      <li>Perform analysis on how you use our platform</li>
                      <li>Provide and communicate with you during the implementation of your health plan.</li>
                      <li>Fulfill your requests regarding our services</li>
                      <li>Respond to your inquiries</li>
                      <li>Provide technical support for the services we offer</li>
                      <li>Enforce the legal terms that govern your use of our platform and/or for the purposes for which you provided the Information</li>
                      <li>Prevent fraudulent or potentially illegal activities on or through our platform</li>
                      <li>Protect the safety of our users</li>
                      <li>Establish, exercise or defend legal claims</li>
                    </ol>
                  </li>

                  <li className={listStyle}>How we process the information
                  <p className={pStyle}>
                      We collect, record, organize, store, update/modify, retrieve, consult, user or consolidate your personal information during your enrollment in the system and to our services. The Personal data obtained from CloudHealthAsia is entered and stored within CloudHealth’s information and communication system and will only be accessible to the patient and the CloudHealthAsia team (Doctors, system administrator and medical practitioners).
                  </p>
                  </li>

                  <li className={listStyle}>Information sharing and disclosure
                <p className={pStyle}>
                      Information CloudHealthAsia collects from its clients concerning their health will be kept strictly confidential and secure at all times.
                  </p>
                    <p>
                      We may disclose Information in accordance with the purposes stated above as follows:
                  </p>
                    <ul>
                      <li>To other medical teams and service providers where health information is needed for epidemiological data or any other researches, provided your personal information shall be held under strict confidentiality and shall only be used for the declared purpose. </li>
                      <li>To the technical support team in processing support requests</li>
                      <li>To the sales and operations team when fulfilling orders and user requests</li>
                      <li>To our host provider who maintains our databases</li>
                      <li>To the system administrator in cases of need to investigate, prevent or take action regarding potentially illegal activities involving potential threats to any person or us, the services or violations of our policies or the law</li>
                      <li>In response to legal process, for example, information is subpoenaed by Court or similar requests.</li>
                      <li>With third parties in order to investigate, prevent or take action regarding potentially illegal activities, violations of our policies.</li>
                    </ul>
                  </li>

                  <li className={listStyle}>Complaints, concerns and questions
                  <p className={pStyle}>
                      For complaints, concerns and questions regarding the processing of your personal information, please contact us at client.services@cloudhealthasia.com
                  </p>
                  </li>

                  <li className={listStyle}>Your rights as the data subject
                  <p className={pStyle}>
                      As the provider of the information, you have the following rights under the Data Privacy Act:
                  </p>
                    <ul>
                      <li>The right to access your personal information</li>
                      <li>The right to make corrections to your personal information</li>
                      <li>The right to object to the processing of your personal information</li>
                      <li>The right to suspend, withdraw or order the blocking, removal or destruction of your personal information</li>
                      <li>The right to be informed on the reasons for the disclosure of the personal information</li>
                      <li>The right to be informed of the existence of processing of your personal information, the manner by which such data were processed and the names and addresses of the recipients of the personal information</li>
                      <li>The right to damages</li>
                      <li>The right to obtain from the personal information controller a copy of data undergoing processing in an electronic or structured format</li>
                      <li>The right to file a complaint before the National Privacy Commission</li>
                    </ul>
                  </li>

                  <li className={listStyle}>Updates and Effective Date
                  <p className={pStyle}>
                      CloudHealthAsia reserves the right to update this Privacy Policy. We will however make sure to notify you about the material changes either by sending an email message to your registered email address or through a notification or announcement posted in our platform. We then encourage you to regularly check and review this policy so you are always informed on what information we collect, how we use it, and with and whom we share it.
                  </p>
                  </li>

                </ol>
              </ModalContent>
            </div>
          </ModalContentWrapper>
          <ModalFooter>
            <Button type="pink" styles={{ marginTop: '0 !important', margin: '0 auto' }}>
              <button style={styles.button_yes} onClick={this.closeModal}>Close</button>
            </Button>
          </ModalFooter>
        </ModalWrapper>
      </Modal>
    );
  }
}


//glamos styles

let listStyle = css({
  margin: '10px 0 5px 0',
  padding: 5,
});

let pStyle = css({
  margin: '0px'
});

// jsx styles
const customStyles = { ...modalCustomStyles };

const styles = {
  letterStyle: {
    textAlign: 'justify',
    fontFamily: '"Roboto",sans-serif',
  },
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    maxWidth: '40em',
    marginTop: '80px',
    color: '#364563',
    lineHeight: '34px',
  },
  dashItem: {
    fontFamily: "Roboto-Medium",
    fontSize: "18px",
    color: "#BDC7CF",
    letterSpacing: 0,
    lineHeight: "14px",
    padding: "20px 20px",
    margin: '-1px 25px',
    cursor: 'pointer',
  },
  dashItemActive: {
    color: '#358ED7',
    borderBottom: '2px solid #358ED7',
  },
  button: {
    backgroundColor: '#77E9AF',
    width: '300px',
    padding: '11px 80px',
    border: 'none',
    borderRadius: '6px',
    color: 'white',
    fontFamily: '"Roboto",sans-serif',
    fontSize: '14px',
    margin: '30px 0px 20px 0px',
    cursor: 'pointer',
  },
  button_yes: {
    backgroundColor: '#3498db',
    fontFamily: '"Roboto",sans-serif',
    color: 'white',
    fontSize: '14px',
    padding: '8px 15px 8px 15px',
    cursor: 'pointer',
    borderRadius: '6px',
  },
  button_no: {
    backgroundColor: '#3498db',
    fontFamily: '"Roboto",sans-serif',
    color: 'white',
    fontSize: '14px',
    padding: '8px 11px 8px 11px',
    cursor: 'pointer',
    borderRadius: '6px',
  },
  title: {
    textAlign: 'center',
    fontWeight: 'bold'
  }
};
