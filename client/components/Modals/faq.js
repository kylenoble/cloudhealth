/* eslint-disable no-nested-ternary */
/* eslint-disable radix */
/* eslint-disable import/imports-first */
/* eslint-disable react/react-in-jsx-scope */
import React from 'react';
import Modal from "react-modal";
import { css } from "glamor";
import FaqContentItem from "./FaqComponents/FaqContentItem";
import CreateTicket from "./FaqComponents/CreateTicket";
import CheckTicketStatus from "./FaqComponents/CheckTicketStatus";
import currentDomain from "../../utils/domain.js";
import questions from "./FaqComponents/Questions";
import ModalWrapper, { ModalContentWrapper, ModalFooter } from "../NewSkin/Wrappers/Modal";
import { modalCustomStyles, combined, on_mobile, font } from "../NewSkin/Styles";
import Button from "../NewSkin/Components/Button";
import request from 'axios';
import NavTab from './FaqComponents/NavTab';

const GET_TICKETS_URL = `${currentDomain}/api/ticket/getLatestTicket`;

// let faqcContent
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: this.props.modalIsOpen,
      faqContentData: [],
      faqNavigationData: [],
      TicketNavigationData: [],
      faqNavHeaders: [],
      MyProfileFaq: [],
      MyAccountFaq: [],
      MyHealthProfileFaq: [],
      AnotherData: [],
      activeFaqContent: [],
      faqContent: [],
      changeView: false,
      nextView: "",

      ticket_number: "",

      latestTicket: "",
      recentTicket: "",
      ticketZero: "",
      recentTicketNums: "",
      questions
    };

    this.closeModal = this.closeModal.bind(this);
    this.showFaq = this.showFaq.bind(this);
  }

  componentWillMount() {
    this.setState({
      faqNavHeaders: ["Frequently Asked Questions", "Help and Support"],

      faqNavigationData: [
        {
          id: 'MyAccount',
          title: "My Account"
        },
        {
          id: 'MyHealthProfile',
          title: "My Health Profile"
        },
        {
          id: 'Accessibility/Compatibility',
          title: "Accessibility/Compatibility"
        }
      ],

      TicketNavigationData: [
        {
          id: 'SubmitATicket',
          title: "Submit a Ticket"
        },
        {
          id: 'CheckTicketStatus',
          title: "Check Ticket Status"
        }
      ]
    });
  } // <-- componentWillMount

  componentDidMount() {
    // this._getLatestTicket()
    this.setState({
      // activeFaqContent: this.state.questions.MyProfile,
      nextView: "",
      changeView: true
    });
  }

  componentDidUpdate() {
    // console.log('recent ticket @ faq: ',this.state.recentTicket)
  }

  _getLatestTicket = () => {
    console.log("Getting Latest Ticket");

    request
      .get(GET_TICKETS_URL)
      .then(result => {
        if (result.data.length > 0) {
          this._latestTicket(result.data);
        } else {
          this._firstTicket();
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  _firstTicket = () => {
    const date = this._yyyymmdd();
    this._newTicketNumber(date);
  };

  _latestTicket = latest => {
    this.setState({
      latestTicket: latest[0].ticket_number
    });

    setTimeout(() => {
      this._generateNewTicketNumber();
    }, 1);
  };

  _yyyymmdd = () => {
    const now = new Date();
    const y = now.getFullYear();
    const m = now.getMonth() + 1;
    const d = now.getDate();
    const mm = m < 10 ? `0${m}` : m;
    const dd = d < 10 ? `0${d}` : d;
    return `${y}${mm}${dd}`;
  };

  _generateNewTicketNumber = () => {
    const date = this._yyyymmdd(); // get yymmdd date format
    const latest = this.state.latestTicket.split("-"); // split ticket number to date and ticket no.

    const recentNumber = parseInt(latest[1]);

    if (recentNumber >= 99) {
      this.setState({
        ticketZero: ""
      });
    } else if (recentNumber >= 9) {
      this.setState({
        ticketZero: "0"
      });
    } else {
      this.setState({
        ticketZero: "00"
      });
    }

    const latestTicketDate = latest[0];

    if (date === latestTicketDate) {
      // checking if the latest ticket's date is the same with the current date
      this._newTicketNumberSameDate([
        latestTicketDate,
        this.state.ticketZero,
        recentNumber
      ]);
    } else {
      this._newTicketNumber(date);
    }
  };

  _newTicketNumber = date => {
    const tnum = `${date}-001`;

    setTimeout(() => {
      this.setState({
        ticket_number: tnum,
        recentTicket: tnum
      });
    }, 100);
  };

  _newTicketNumberSameDate = ticketnum => {
    const ticketDate = ticketnum[0];
    const ticketZero = ticketnum[1];
    const ticketNo = ticketnum[2];

    const tnum = `${ticketDate}-${ticketZero}${parseInt(ticketNo) + 1}`;

    setTimeout(() => {
      this.setState({
        ticket_number: tnum,
        recentTicket: tnum
      });
    }, 100);
  };

  closeModal() {
    this.setState({
      nextView: "",
      changeView: true
    });
    this.props.closeModal("faqModalIsOpen");
  }


  showFaq(e) {
    const activeNav = e.target.id;

    this.setState({
      nextView: activeNav,
      activeFaqContent: this.state.questions[activeNav],
      changeView: false
    });

    setTimeout(() => {
      this.setState({
        changeView: true
      });
    }, 1);
  } // <-- showFaq function

  tabChanged = () => {
    this.setState({
      nextView: ''
    })
  }

  showHelpAndSupport = (e) => {
    const id = e.target.id

    switch (id) {
      case 'SubmitATicket':
        this.setState({
          nextView: 'SubmitATicket'
        });
        break;
      case 'CheckTicketStatus':
        this.setState({
          nextView: 'CheckTicketStatus'
        });
        break;
      default:
        break;
    }
  };

  render() {
    const modalContentHeight = 250;
    return (
      <Modal
        isOpen={this.props.modalIsOpen}
        onAfterOpen={this.afterOpenModal}
        onRequestClose={this.closeModal}
        shouldCloseOnOverlayClick={false}
        style={customStyles}
        ariaHideApp={false}
      >
        <ModalWrapper classes={[on_mobile([], { width: '95%' })]} styles={{ top: '52%' }} >
          <NavTab
            showFaq={this.showFaq}
            faqTabs={this.state.faqNavigationData}
            helpTabs={this.state.TicketNavigationData}
            tabChanged={this.tabChanged}
            showHelpAndSupport={this.showHelpAndSupport}
          />

          <div>
            <div id="faqwrapper" className={faqWrapper}>
              <div>
                {this.state.nextView.length > 0 ? (
                  <div className={collapsible} style={{ minHeight: modalContentHeight }}>
                    {this.state.nextView === "SubmitATicket" ? (
                      <ModalContentWrapper styles={{ padding: 0, paddingRight: 10, overflow: 'auto', height: modalContentHeight }}>
                        <CreateTicket
                          ticketNumber={this.state.ticket_number}
                          latestTicket={this._getLatestTicket}
                          closeModal={this.closeModal}
                          modalContentHeight={modalContentHeight}
                        />
                      </ModalContentWrapper>
                    ) : this.state.nextView === "CheckTicketStatus" ? (
                      <CheckTicketStatus modalContentHeight={modalContentHeight} />
                    ) : this.state.changeView ? (
                      <ModalContentWrapper styles={{ padding: 0, paddingRight: 10, overflow: 'auto', height: modalContentHeight }}>
                        <section>
                          {
                            this.state.activeFaqContent.map((faqContent, index) => (
                              <FaqContentItem key={index} faqContent={faqContent} />
                            ))
                          }
                        </section>
                      </ModalContentWrapper>
                    ) : null}
                  </div>
                ) :
                  <ModalContentWrapper styles={{ padding: 0, paddingRight: 10, overflow: 'auto', height: modalContentHeight }}>
                    <DefaultView />
                  </ModalContentWrapper>
                }
              </div>
            </div>

          </div>
          <ModalFooter>
            <Button type="pink" styles={{ marginTop: '0 !important', margin: '0 auto' }}>
              <button className={button__yes} onClick={this.closeModal}>Close</button>
            </Button>
          </ModalFooter>
        </ModalWrapper>
      </Modal>
    );
  }
}

const DefaultView = () => (
  <div id="default-view" className={defaultView}>
    <h2 className={combined([font(2), on_mobile([], { fontSize: '1rem' })], { marginTop: 0 })}>Welcome to CloudHealthAsia Support!</h2>
    <h1 style={{ fontSize: 18 }}>
      <span>Need help in any of our platform's features?</span>

      <br />
      Check out our{" "}
      <strong className={highlight}>Frequently Asked Questions.</strong>
    </h1>
    <h1 style={{ fontSize: 18 }}>
      <span>Can't find what you're looking for?</span>

      <br />
      Go to the{" "}
      <strong className={highlight}>Help and Support</strong> and{" "}
      <strong className={highlight}>Submit a Ticket</strong>.
                        </h1>
  </div>
)

let defaultView = css({
  "> h1": {
    marginBottom: "30px",
  }
});

let highlight = css({
  color: "#3498db"
});

let button__yes = css({
  position: "absolute",
  bottom: "10px",
  left: "10px",
  borderStyle: "none",
  backgroundColor: "#3498db",
  color: "white",
  fontSize: "14px",
  padding: "8px 15px 8px 15px",
  cursor: "pointer",
  transition: "250ms ease",
  borderRadius: "3px",
  ":hover": {
    background: "#297eb7"
  }
});

// Collapsible Styles
let collapsible = css({
  listStyle: "none",
  margin: "0",
  padding: 0
});

let faqWrapper = css({
  display: "table",
  clear: "both",
  width: "100%",
  fontSize: "14px",
  textAlign: "left"
});

let faqNavWrapper = css({
  width: "30%",
  padding: "5px",
  float: "left"
});

let faqContentWrapper = css({
  width: "67%",
  padding: "5px",
  float: "right"
});

let faqNavList = css({
  border: "1px solid #ccc",
  listStyle: "none",
  padding: "0px",
  margin: "0px",
  // boxShadow: "0 1px 5px rgba(85, 85, 85, 0.15)",
  background: "#fff",
  borderRadius: 10,
  overflow: 'auto',
  maxHeight: 350,
  '> li': {
    '&:last-child p': {
      borderBottom: 'none !important'
    }
  }
});

// jsx styles
const customStyles = { ...modalCustomStyles };

const styles = {
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    flexWrap: "wrap",
    maxWidth: "40em",
    marginTop: "80px",
    color: "#364563",
    lineHeight: "34px"
  },
  dashItem: {
    fontFamily: "Roboto-Medium",
    fontSize: "18px",
    color: "#BDC7CF",
    letterSpacing: 0,
    lineHeight: "14px",
    padding: "20px 20px",
    margin: "-1px 25px",
    cursor: "pointer"
  },
  dashItemActive: {
    color: "#358ED7",
    borderBottom: "2px solid #358ED7"
  },
  button: {
    backgroundColor: "#77E9AF",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "30px 0px 20px 0px",
    cursor: "pointer"
  },
  button_yes: {
    borderStyle: "none",
    backgroundColor: "#3498db",
    fontFamily: '"Roboto",sans-serif',
    color: "white",
    fontSize: "14px",
    padding: "8px 15px 8px 15px",
    cursor: "pointer"
  },
  button_no: {
    backgroundColor: "#3498db",
    fontFamily: '"Roboto",sans-serif',
    color: "white",
    fontSize: "14px",
    padding: "8px 11px 8px 11px",
    cursor: "pointer",
    borderRadius: "6px"
  },
  title: {
    textAlign: "center",
    fontWeight: "bold"
  }
};
