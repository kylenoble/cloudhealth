export default {
  ITEMS: {
    imbalances: [
      { isChecked: false, value: "assimilation", label: "Assimilation" },
      { isChecked: false, value: "communication", label: "Communication" },
      {
        isChecked: false,
        value: "defenseAndRepair",
        label: "Defense and Repair"
      },
      { isChecked: false, value: "energy", label: "Energy" },
      {
        isChecked: false,
        value: "structuralIntegrity",
        label: "Structural Integrity"
      },
      { isChecked: false, value: "transport", label: "Transport" }
    ],
    healthoutcome: [
      { isChecked: false, value: "improving", label: "Improving" },
      { isChecked: false, value: "notImproving", label: "Not improving" },
      {
        isChecked: false,
        value: "forImmediateFollowUp",
        label: "For immediate follow-up"
      },
      {
        isChecked: false,
        value: "followUpNextMonth",
        label: "Follow-up next month"
      },
      {
        isChecked: false,
        value: "stalledOrNeedsRecovery",
        label: "Stalled or needs recovery"
      }
    ],
    cns: [
      {
        isChecked: false,
        value: "IgGFoodReactionFoodIntolerance",
        label: "CNS Food Intolerance Test (IgG  Food Reaction)"
      }
    ],
    cellmaxLife: [
      {
        isChecked: false,
        value: "DNAHereditaryCancerRiskTest",
        label: "Cellmax DNA Hereditary Cancer Risk Test"
      }
    ],
    genosense: [
      { isChecked: false, value: "ADHDSensor", label: "ADHD Sensor" },
      { isChecked: false, value: "alzheimerSensor", label: "Alzheimer Sensor"},
      { isChecked: false, value: "AMDSensor", label: "AMD Sensor" },
      { isChecked: false, value: "boneSensor", label: "Bone Sensor"},
      { isChecked: false, value: "breastHealth", label: "Breast Health" },
      { isChecked: false, value: "burnoutSensor", label: "Burnout Sensor" },
      { isChecked: false, value: "CardiovascularSensor", label: "Cardiovascular Sensor" },
      { isChecked: false, value: "DepressionSensor", label: "Depression Sensor" },
      { isChecked: false, value: "diabetesSensor", label: "Diabetes Sensor" },
      { isChecked: false, value: "FemaleSensorPregnancy", label: "Female Sensor Pregnancy" },
      { isChecked: false, value: "femSensor40", label: "Fem Sensor 40+ (Sensors: Breast Health, Bone Health, Toxo, Thrombo, Cardiovascular)"},
      { isChecked: false, value: "Andro40", label: "Andro 40+ (Sensors: Prostate Health, Bone Health, Toxo, Thrombo, Cardiovascular)" },
      { isChecked: false, value: "gastrointestinalSensor", label: "Gastrointestinal Sensor (Sensors: Gluten, IBD, Lactose)"},
      { isChecked: false, value: "glaucomaSensor", label: "Glaucoma Sensor" },
      { isChecked: false, value: "GlutenIntoleranceSensor", label: "Gluten Intolerance Sensor" },
      { isChecked: false, value: "HIVResistanceSensor", label: "HIV Resistance Sensor" },
      { isChecked: false, value: "hypertension Sensor", label: "Hypertension Sensor"},
      { isChecked: false, value: "IBDSensor", label: "IBD Sensor" },
      { isChecked: false, value: "ironSensor", label: "Iron Sensor" },
      { isChecked: false, value: "jointSensor", label: "Joint Sensor" },
      { isChecked: false, value: "LactoseIntoleranceSensor", label: "Lactose Intolerance Sensor"},
      { isChecked: false, value: "nutriPharmacoToxoSensor", label: "Nutrition + Pharmaco + Toxo Sensor"},
      { isChecked: false, value: "nutritionSensor", label: "Nutrition Sensor" },
      { isChecked: false, value: "GenosenseOBPack", label: "Genosense OB Pack (Sensors: Bone Health, Breast Health, Thrombo, Hormonal Replacement Therapy )" },
      { isChecked: false, value: "performanceSensor", label: "Performance Sensor" },
      { isChecked: false, value: "periodontitisSensor", label: "Periodontitis Sensor" },
      { isChecked: false, value: "pharmacoSensor", label: "Pharmaco Sensor" },
      { isChecked: false, value: "premiumSensor", label: "Premium Sensor (Male/Female)" },
      { isChecked: false, value: "premiumPlusSensor", label: "Premium Plus Sensor (Male/Female)" },
      { isChecked: false, value: "primaryPreventiveNutriPharmacoToxo", label: "Primary Preventive + Nutrition + Pharmaco +Toxo" },    
      { isChecked: false, value: "primaryPreventiveSensor", label: "Primary Preventive Sensor (Sensors: Thrombo, Cardiovascular, Diabetes, Hypertension)" },
      { isChecked: false, value: "prostateHealth", label: "Prostate Health" },
      { isChecked: false, value: "thromboSensor", label: "Thrombosis Sensor" },
      { isChecked: false, value: "toxoSensor", label: "Toxo Sensor" },   
      { isChecked: false, value: "weightSensor", label: "Weight Sensor" },
      { isChecked: false, value: "beautySensor", label: "Beauty Sensor" }
    ],
    gpl: [
      // Nutritional & Metabolic Assessments
      { isChecked: false, value: "oat", label: "Organic Acids Test (OAT) (72) - Urine" },
      { isChecked: false, value: "moat", label: "Microbial Organic Acids Test (MOAT) (20) - Urine" },
      { isChecked: false, value: "glyphosate", label: "Glyphosate" },
      { isChecked: false, value: "glyphosateAddOn", label: "Glyphosate (Add-on if ordered with OAT, MOAT, or GPL-TOX)" },
      { isChecked: false, value: "advancedCholesterolProfile", label: "Advanced Cholesterol Profile - Serum" },
      { isChecked: false, value: "calciumMagnesiumTest", label: "Calcium + Magnesium Test" },
      { isChecked: false, value: "calciumMagnesiumTestAddOn", label: "Calcium + Magnesium Test (Add-on price when ordered with OAT, MOAT, or GPL-TOX)" },
      { isChecked: false, value: "phospholipaseA2", label: "Phospholipase A2 - Urine" },
      { isChecked: false, value: "phospholipaseA2AddOn", label: "Phospholipase A2 - Urine (Add-on if ordered with OAT, MOAT, or Gluten/Casein Peptides)" },
      { isChecked: false, value: "uricAcidTest", label: "Uric Acid Test (24 hr. or Random)" },
      { isChecked: false, value: "uricAcidTestAddOn", label: "Uric Acid Test (24 hr. or Random) (Add-on price when ordered with another urine test)" },
      { isChecked: false, value: "gplTox", label: "GPL-TOX" },
      { isChecked: false,  value: "gplToxAddOn", label: "GPL-TOX (Add-on if ordered with OAT, MOAT, or Gluten and Casein Peptide Test)" },
      { isChecked: false, value: "immuneDeficiencyProfile", label: "Immune Deficiency Profile - Serum" },
      { isChecked: false, value: "streptococcusAntibodiesProfile", label: "Streptococcus Antibodies Profile - Serum" },
      { isChecked: false, value: "copperZincProfileSerum", label: "Copper / Zinc Profile - Serum" },
      { isChecked: false, value: "enviroTOX", label: "Enviro-TOX (OAT, GPL-TOX & Glyphosate)" },
      { isChecked: false, value: "omega3IndexCompleteTest", label: "Omega-3 Index Complete Test" },
      { isChecked: false, value: "mycoTOX", label: "GPL MycoTOX" },
      { isChecked: false, value: "glutenCaseinPeptidesTest", label: "Gluten/Casein Peptides Test (GPL)" },
      { isChecked: false, value: "celiacDiseaseTestCalcium", label: "Celiac Disease Test Calcium (GPL)" }
    ],
    caam: [{ isChecked: false, value: "faber", label: "FABER - IgE Allergy Test" }],
    rgcc: [
      { isChecked: false, value: "chemosnip", label: "Chemosnip" },
      { isChecked: false, value: "metastat", label: "Metastat" },
      { isChecked: false, value: "oncocount", label: "Oncocount" },
      { isChecked: false, value: "onconomics", label: "Onconomics" },
      { isChecked: false, value: "onconomicsPlus", label: "Onconomics Plus" },
      { isChecked: false, value: "onconomicsExtracts", label: "Onconomics Extracts" },
      { isChecked: false, value: "oncotrace", label: "OncoTrace" },
      { isChecked: false, value: "oncotrail", label: "Oncotrail" }      
    ],
    genoMind: [{ isChecked: false, value: "genomindProfessionalPGx", label: "Genomind Professional PGx" }],
    gih: [
      { isChecked: false, value: "bacteriologyCultureProfile", label: "Bacteriology Culture Profile" },
      { isChecked: false, value: "calprotectinStool", label: "Calprotectin Stool" },
      { isChecked: false, value: "celiac&GlutenSensitivitySerum", label: "Celiac & Gluten Sensitivity Serum" },
      { isChecked: false, value: "comprehensiveClostridiumCulture", label: "Comprehensive Clostridium Culture" },
      { isChecked: false, value: "comprehensiveParasitologyx1", label: "Comprehensive Parasitology x 1" },
      { isChecked: false, value: "comprehensiveParasitologyx2", label: "Comprehensive Parasitology x 2" },
      { isChecked: false, value: "comprehensiveParasitologyx3", label: "Comprehensive Parasitology x 3" },
      { isChecked: false, value: "comprehensiveStoolAnalysis", label: "Comprehensive Stool Analysis" },
      { isChecked: false, value: "comprehensiveStoolAnalysisParasitologyx1", label: "Comprehensive Stool Analysis w/Parasitology x 1" },
      { isChecked: false, value: "comprehensiveStoolAnalysisParasitologyx2", label: "Comprehensive Stool Analysis w/Parasitology x 2" },
      { isChecked: false, value: "comprehensiveStoolAnalysisParasitologyx3", label: "Comprehensive Stool Analysis w/Parasitology x 3" },
      { isChecked: false, value: "elastaseStool", label: "Elastase, stool" },
      { isChecked: false, value: "gIPathogenProfileMultiplexPCR", label: "GI Pathogen Profile, multiplex PCR" },
      { isChecked: false, value: "h.PyloriAntigenStool", label: "H. Pylori Antigen, stool" },
      { isChecked: false, value: "intestinalPermeability", label: "Intestinal Permeability" },
      { isChecked: false, value: "lactoferrinStool", label: "Lactoferrin, stool" },
      { isChecked: false, value: "lysozymeStool", label: "Lysozyme, stool" },
      { isChecked: false, value: "macroscopicExamination,stool", label: "Macroscopic Examination, stool" },
      { isChecked: false, value: "microbiologyProfile", label: "Microbiology Profile" },
      { isChecked: false, value: "parasitologyx1", label: "Parasitology x 1" },
      { isChecked: false, value: "parasitologyx2", label: "Parasitology x 2" },
      { isChecked: false, value: "parasitologyx3", label: "Parasitology x 3" },
      { isChecked: false, value: "secretorylgAStool", label: "Secretory lgA, stool" },
      { isChecked: false, value: "shigaToxinsStool", label: "Shiga Toxins, stool" },
      { isChecked: false, value: "stoolChemistry", label: "Stool Chemistry" },
      { isChecked: false, value: "toxigenicCDifficileCultureStool", label: "Toxigenic C. difficile Culture, stool" },
      { isChecked: false, value: "toxigenicCDifficileDNAStool", label: "Toxigenic C. difficile DNA; stool" },
      { isChecked: false, value: "yeastCulture", label: "Yeast Culture" },
      { isChecked: false, value: "zonulinSerum", label: "Zonulin; serum" },
      { isChecked: false, value: "zonulinStooladdonCSA,CSAP", label: "Zonulin; stool add-on w/CSA, CSAP" },
      { isChecked: false, value: "zonulinStool", label: "Zonulin; stool" }
    ],
    tee: [
      { isChecked: false, value: "comprehensiveBloodElements", label: "Comprehensive Blood Elements" },
      { isChecked: false, value: "creatinineClearance", label: "Creatinine Clearance" },
      { isChecked: false, value: "fecalMetals", label: "Fecal Metals" },
      { isChecked: false, value: "hairElements", label: "Hair Elements" },
      { isChecked: false, value: "hairToxicElementExposureProfile", label: "Hair Toxic Element Exposure Profile" },
      { isChecked: false, value: "implantProfileWholeblood", label: "Implant Profile; whole blood" },
      { isChecked: false, value: "RBCElements", label: "Red Blood Cell (RBC) Elements" },
      { isChecked: false, value: "serumElements", label: "Serum Elements" },
      { isChecked: false, value: "urineEssentialElements", label: "Urine Essential Elements" },
      { isChecked: false, value: "urineMercury", label: "Urine Mercury" },
      { isChecked: false, value: "urineToxic&EssentialElements", label: "Urine Toxic & Essential Elements" },
      { isChecked: false, value: "urineToxicMetals", label: "Urine Toxic Metals" },
      { isChecked: false, value: "wholeBloodElements", label: "Whole Blood Elements" },
      { isChecked: false, value: "titaniumWholeblood", label: "Titanium; whole blood" },
      { isChecked: false, value: "titaniumaddonWholeBloodelements", label: "Titanium add-on w/Whole Blood elements" }
    ],
    nutritional: [
      { isChecked: false, value: "fatSolubleVitamins", label: "Fat Soluble Vitamins" },
      { isChecked: false, value: "fattyAcidsErythrocytes", label: "Fatty Acids; Erythrocytes" },
      { isChecked: false, value: "folateMetabolismPlasma", label: "Folate Metabolism; plasma" },
      { isChecked: false, value: "methylationProfilePlasma", label: "Methylation Profile; plasma" },
      { isChecked: false, value: "methylationProfileFolateMetabolism", label: "Methylation Profile + Folate Metabolism" },
      { isChecked: false, value: "plasmaAminoAcids", label: "Plasma Amino Acids" },
      { isChecked: false, value: "urineAminoAcids", label: "Urine Amino Acids" },
      { isChecked: false, value: "urineFluoride", label: "Urine Fluoride" },
      { isChecked: false, value: "urineHalidesIodineFluorideBromine)", label: "Urine Halides (Iodine, Fluoride, & Bromine)" },
      { isChecked: false, value: "urineHalidesPrePostLoadingcombo", label: "Urine Halides; Pre & Post Loading combo" },
      { isChecked: false, value: "urineIodine", label: "Urine Iodine" },
      { isChecked: false, value: "urineIodinePrePostLoadingcombo", label: "Urine Iodine; Pre & Post Loading combo" },
      { isChecked: false, value: "vitaminD25OHD2&D3Serum", label: "Vitamin D (25OH D2 & D3); serum" }
    ],
    EnvExposureDetox: [
      { isChecked: false, value: "dNAMethylationWholeBlood", label: "DNA Methylation Whole Blood" },
      { isChecked: false, value: "dNAOxidativeDamageAssay", label: "DNA Oxidative Damage Assay" },
      { isChecked: false, value: "hepaticDetoxProfile", label: "Hepatic Detox Profile" },
      { isChecked: false, value: "rBCGlutathione", label: "RBC Glutathione" },
      { isChecked: false, value: "urinePorphyrins", label: "Urine Porphyrins" }
    ],
    Cardiometabolic: [
      { isChecked: false, value: "cardioMetabolicProfile;serum", label: "CardioMetabolic Profile; serum" },
      { isChecked: false, value: "cardiovascularRiskProfile", label: "Cardiovascular Risk Profile" },
      { isChecked: false, value: "metabolomicProfile", label: "Metabolomic Profile" },
      { isChecked: false, value: "oxidizedLDL", label: "Oxidized LDL" }
    ],
    Bloodspot: [
      { isChecked: false, value: "celiac&GlutenSensitivityBloodSpot", label: "Celiac & Gluten Sensitivity Blood Spot" },
      { isChecked: false, value: "dNAMethylationBloodSpot", label: "DNA Methylation Blood Spot" },
      { isChecked: false, value: "metabolomicProfileBloodSpot", label: "Metabolomic Profile; Blood Spot" },
      { isChecked: false, value: "vitaminDBloodSpot", label: "Vitamin D Blood Spot" }
    ],
    Endocrinology: [
      { isChecked: false, value: "Neuro-BiogenicAmines,Comprehensive;Urine", label: " Neuro-Biogenic Amines, Comprehensive; Urine" },
      { isChecked: false, value: "neuro-BiogenicAmines;Urine", label: "Neuro-Biogenic Amines; Urine" },
      { isChecked: false, value: "thyroidProfileSerum", label: "Thyroid Profile; serum" },
      { isChecked: false, value: "salivaryHormones", label: "Salivary Hormones" }
    ],
    Others: [
      { isChecked: false, value: "comprehensiveDrinkingWater", label: "Comprehensive Drinking Water" },
      { isChecked: false, value: "drinkingWaterAnalysis", label: "Drinking Water Analysis" },
      { isChecked: false, value: "drinkingWaterFluorideLevel", label: "Drinking Water Fluoride Level" }
    ],
    Neurohormones: [
      { isChecked: false, value: "neuroHormoneCompletePlus", label: "NeuroHormone Complete Plus" },
      { isChecked: false, value: "neuroHormoneCompleteProfile", label: "NeuroHormone Complete Profile" },
      { isChecked: false, value: "neuroAdrenalProfile", label: "NeuroAdrenal Profile" }
    ],
    HormonesAndOtherAnalytes: [
      { isChecked: false, value: "comprehensivePlusProfile", label: "Comprehensive Plus Profile" },
      { isChecked: false, value: "womenHealth&BreastProfile", label: "Women’s Health & Breast Profile" },
      { isChecked: false, value: "comprehensiveHormoneProfile", label: "Comprehensive Hormone Profile" },
      { isChecked: false, value: "shortComprehensiveProfile", label: "Short Comprehensive Profile" },
      { isChecked: false, value: "basicHormoneProfile", label: "Basic Hormone Profile" },
      { isChecked: false, value: "comprehensiveAdrenalFunctionProfile", label: "Comprehensive Adrenal Function Profile" },
      { isChecked: false, value: "adrenalFunctionProfile", label: "Adrenal Function Profile" },
      { isChecked: false, value: "diurnalCortisolProfile", label: "Diurnal Cortisol Profile" },
      { isChecked: false, value: "melatoninProfile", label: "Melatonin Profile" },
      { isChecked: false, value: "singleMelatonin", label: "Single Melatonin" },
      { isChecked: false, value: "eachAdditionalMelatonin", label: "Each Additional Melatonin" },
      { isChecked: false, value: "threeAnalytes*", label: "Three Analytes*" },
      { isChecked: false, value: "twoAnalytes*", label: "Two Analytes*" },
      { isChecked: false, value: "singleAnalyte*", label: "Single Analyte* " }
    ],
    Neurotransmitters: [
      { isChecked: false, value: "neurobasicProfile", label: "Neurobasic Profile" },
      { isChecked: false, value: "comprehensiveNeuroBiogenicAminesProfile", label: "Comprehensive Neuro-Biogenic Amines Profile" } 
    ],
    DNA: [
      { isChecked: false, value: "findWhyWeightControlTest", label: "FindWhy Weight Control Test" }
    ]
  }
}
