/**
need  to feed
1) appointment details
**/

import { css } from "glamor";
import moment from "moment-timezone";
import ConsultsService from "../../utils/consultsService";
import AppointmentService from "../../utils/appointmentService";

const appointment = new AppointmentService();
const consult = new ConsultsService();

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appointment: this.props.appointment,
      consultation: []
    };

    this._getConsultations = this._getConsultations.bind(this);

    console.log("PROPS AND STATE");
    console.log(this.state.appointment.appointment_code);
  }

  componentDidMount() {
    this._getConsultations(this.state.appointment.appointment_code);
  }

  _getConsultations(patientId) {
    let str = "appointment_code-" + patientId;
    consult
      .getConsult(str)
      .then(res => {
        console.log(res);
      })
      .catch(e => {
        console.log("errrrrrrrrrrrrrr");
        console.log(e);
      });
  }

  render() {
    return <div>Report wil go here</div>;
  }
}
