/* eslint-disable no-nested-ternary */
/* eslint-disable radix */
/* eslint-disable no-restricted-syntax */
import React from "react";
import { css } from "glamor";
import moment from "moment-timezone";
import Modal from "react-modal";
import ReactTooltip from "react-tooltip";
import { tooltipWrapper } from "../Commons/tooltip";
import { confirmAlert } from "react-confirm-alert"; // Import
import Form from "../Form/specialForm";
import DIAGNOSTICS from "./diagnostics";
import ConsultsService from "../../utils/consultsService";
import Slots from "../Appointments/slots";
import AppointmentService from "../../utils/appointmentService";
import ConsultReport from "./consultReport";
import ConcernService from "../../utils/concernService";
import {
  inputStyleObj,
  labelClass,
  closeBtn,
  scrollClass
} from "../NewSkin/Styles";
import Colors from "../NewSkin/Colors";
import Button from "../NewSkin/Components/Button";

const consults = new ConsultsService();
const appointment = new AppointmentService();
const concern = new ConcernService();

//const diagnostics = Diagnostics
export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      duration: 0,
      loading: false,
      reschduleDate: "",
      modal: { scheduleSlots: false },
      appointment: this.props.appointment_detail,
      otherButton: {
        finalize: { label: "Finalize", name: "finalize", locked: false }
      },
      btnLocked: false,
      fields: [],
      inputs: {
        concern: {
          id: 1,
          group: "A",
          groupLabel: "",
          name: "concern",
          label: "A. Health Concern",
          question: "Health Concern",
          type: "autocomplete",
          validation: "required",
          disabled: false,
          data: [],
          options: [],
          error: {
            active: false,
            message: "Please enter Health Concern"
          },
          value: this.props.appointment_detail.concern
        },
        history: {
          id: 2,
          group: "B",
          groupLabel: "",
          name: "history",
          label: "B. History, Current Symptoms, Conditions and Concerns",
          question: "History, Current Symptoms, Conditions and Concerns",
          type: "textArea",
          validation: "",
          options: [],
          error: {
            active: false,
            message: "Please enter History"
          },
          value: ""
        },
        assimilation: {
          id: 3,
          group: "C",
          groupLabel: "C.	Imbalance ID (Check all applicable IDs)",
          name: "assimilation",
          label: <span>Assimilation</span>,
          question: "Assimilation",
          type: "checkbox",
          validation: "",
          isChecked: false,
          options: [],
          error: {
            active: false,
            message: "Please enter Assimilation "
          },
          value: "assimilation"
        },
        assimilation_details: {
          id: 4,
          group: "C",
          groupLabel: "C.	Imbalance ID (Check all applicable IDs)",
          name: "assimilation_details",
          question: "Assimilation details",
          type: "textArea",
          validation: "",
          options: [],
          error: {
            active: false,
            message: "Please enter Details"
          },
          value: ""
        },
        communication: {
          id: 5,
          group: "C",
          groupLabel: "C.	Imbalance ID (Check all applicable IDs)",
          name: "communication",
          label: <span>Communication</span>,
          question: "Communication",
          type: "checkbox",
          validation: "",
          isChecked: false,
          options: [
            { label: "Asian", value: "Asian", isChecked: false },
            { label: "Asian2", value: "Asian2", isChecked: false }
          ],
          error: {
            active: false,
            message: "Please enter Communication "
          },
          value: "communication"
        },
        communication_details: {
          id: 6,
          group: "C",
          groupLabel: "C.	Imbalance ID (Check all applicable IDs)",
          name: "communication_details",
          question: "Communication details",
          type: "textArea",
          validation: "",
          options: [],
          error: {
            active: false,
            message: "Please enter Health Concern"
          },
          value: ""
        },
        defenseAndRepair: {
          id: 7,
          group: "C",
          groupLabel: "C.	Imbalance ID (Check all applicable IDs)",
          name: "defenseAndRepair",
          label: <span>Defense and Repair</span>,
          question: "Defense and Repair",
          type: "checkbox",
          validation: "",
          isChecked: false,
          options: [],
          error: {
            active: false,
            message: "Please enter Defense and Repair "
          },
          value: "defenseAndRepair"
        },
        defenseAndRepair_details: {
          id: 8,
          group: "C",
          groupLabel: "C.	Imbalance ID (Check all applicable IDs)",
          name: "defenseAndRepair_details",
          question: "Defense and repair details",
          type: "textArea",
          validation: "",
          options: [],
          error: {
            active: false,
            message: "Please enter Defense and repair details"
          },
          value: ""
        },
        energy: {
          id: 9,
          group: "C",
          groupLabel: "C.	Imbalance ID (Check all applicable IDs)",
          name: "energy",
          label: <span>Energy</span>,
          question: "Energy",
          type: "checkbox",
          validation: "",
          isChecked: false,
          options: [],
          error: {
            active: false,
            message: "Please enterEnergyr "
          },
          value: "energy"
        },
        energy_details: {
          id: 10,
          group: "C",
          groupLabel: "C.	Imbalance ID (Check all applicable IDs)",
          name: "energy_details",
          question: "Energy details",
          type: "textArea",
          validation: "",
          options: [],
          error: {
            active: false,
            message: "Please enter Energy details"
          },
          value: ""
        },
        structuralIntegrity: {
          id: 11,
          group: "C",
          groupLabel: "C.	Imbalance ID (Check all applicable IDs)",
          name: "structuralIntegrity",
          label: <span>Structural Integrity</span>,
          question: "Structural Integrity",
          type: "checkbox",
          validation: "",
          isChecked: false,
          options: [],
          error: {
            active: false,
            message: "Please Structural Integrity "
          },
          value: "structuralIntegrity"
        },
        structuralIntegrity_details: {
          id: 12,
          group: "C",
          groupLabel: "C.	Imbalance ID (Check all applicable IDs)",
          name: "structuralIntegrity_details",
          question: "Structural Integrity details",
          type: "textArea",
          validation: "",
          options: [],
          error: {
            active: false,
            message: "Please enter Structural Integrity details"
          },
          value: ""
        },
        transport: {
          id: 13,
          group: "C",
          groupLabel: "C.	Imbalance ID (Check all applicable IDs)",
          name: "transport",
          label: <span>Transport</span>,
          question: "Transport",
          type: "checkbox",
          validation: "",
          isChecked: false,
          options: [],
          error: {
            active: false,
            message: "Please Enter Transport "
          },
          value: "transport"
        },
        transport_details: {
          id: 14,
          group: "C",
          groupLabel: "C.	Imbalance ID (Check all applicable IDs)",
          name: "transport_details",
          question: "Transport details",
          type: "textArea",
          validation: "",
          options: [],
          error: {
            active: false,
            message: "Please enter Transport details"
          },
          value: ""
        },
        medications_supplements: {
          id: 15,
          group: "D",
          groupLabel: "D.	Medications / Supplements",
          name: "medications_supplements",
          label: <span>Medications / Supplements</span>,
          question: "Medications / Supplements",
          type: "checkbox",
          validation: "",
          isChecked: false,
          options: [],
          error: {
            active: false,
            message: "Please Enter Medications / Supplements "
          },
          value: "medications_supplements"
        },
        medications_supplements_details: {
          id: 16,
          group: "D",
          groupLabel: "D.	Medications / Supplements",
          name: "medications_supplements_details",
          question: "Medications / Supplements",
          type: "textArea",
          validation: "",
          options: [],
          error: {
            active: false,
            message: "Please enter Transport Medications / Supplements"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations: {
          id: 17,
          group: "E",
          groupLabel: "E.	Health Plan Summary",
          name: "functionalDiagnosticsRecommendations",
          label: <span>Functional Diagnostics Recommendations</span>,
          question: "Functional Diagnostics Recommendations",
          type: "checkbox",
          validation: "",
          isChecked: false,
          options: [],
          error: {
            active: false,
            message: "Please Enter Functional Diagnostics Recommendations"
          },
          value: "functionalDiagnosticsRecommendations"
        },
        functionalDiagnosticsRecommendations_cns: {
          id: 18,
          group: "E",
          groupLabel: "CNS",
          label: "Cambridge Nutritional Sciences (CNS)",
          name: "functionalDiagnosticsRecommendations_cns",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.cns,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_cellmaxLife: {
          id: 19,
          group: "E",
          groupLabel: "cellmaxLife",
          label: "Cellmax Life",
          name: "functionalDiagnosticsRecommendations_cellmaxLife",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.cellmaxLife,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_genosense: {
          id: 20,
          group: "E",
          groupLabel: "Genosense",
          label: "Genosense",
          name: "functionalDiagnosticsRecommendations_genosense",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.genosense,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_gpl: {
          id: 21,
          group: "E",
          groupLabel: "GPL",
          label: "Great Plains Laboratory",
          name: "functionalDiagnosticsRecommendations_gpl",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.gpl,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_caam: {
          id: 22,
          group: "E",
          groupLabel: "CAAM",
          label: "Centri Associati di Allergologia Molecolare (CAAM)",
          name: "functionalDiagnosticsRecommendations_caam",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.caam,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_rgcc: {
          id: 23,
          group: "E",
          groupLabel: "RGCC",
          label: "RGCC",
          name: "functionalDiagnosticsRecommendations_rgcc",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.rgcc,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_Genomind: {
          id: 24,
          group: "E",
          groupLabel: "Genomind",
          label: "Genomind",
          name: "functionalDiagnosticsRecommendations_Genomind",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.genoMind,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_gih: {
          id: 25,
          group: "E",
          groupLabel: "Gastrointestinal Health",
          label: "Gastrointestinal Health",
          name: "functionalDiagnosticsRecommendations_gih",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.gih,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_tee: {
          id: 26,
          group: "E",
          groupLabel: "Toxic & Essential Elements",
          label: "Toxic & Essential Elements",
          name: "functionalDiagnosticsRecommendations_tee",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.tee,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_Nutritional: {
          id: 27,
          group: "E",
          groupLabel: "Nutritional",
          label: "Nutritional",
          name: "functionalDiagnosticsRecommendations_Nutritional",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.nutritional,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_EED: {
          id: 28,
          group: "E",
          groupLabel: "Env. Exposure & Detox",
          label: "Env. Exposure & Detox",
          name: "functionalDiagnosticsRecommendations_EED",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.EnvExposureDetox,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_Cardiometabolic: {
          id: 29,
          group: "E",
          groupLabel: "Cardiometabolic",
          label: "Cardiometabolic",
          name: "functionalDiagnosticsRecommendations_Cardiometabolic",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.Cardiometabolic,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_Bloodspot: {
          id: 30,
          group: "E",
          groupLabel: "Bloodspot",
          label: "Bloodspot",
          name: "functionalDiagnosticsRecommendations_Bloodspot",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.Bloodspot,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_endocrinology: {
          id: 31,
          group: "E",
          groupLabel: "Endocrinology",
          label: "Endocrinology",
          name: "functionalDiagnosticsRecommendations_Endocrinology",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.Endocrinology,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_others: {
          id: 32,
          group: "E",
          groupLabel: "Endocrinology",
          label: "Endocrinology",
          name: "functionalDiagnosticsRecommendations_others",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.Others,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_Neurohormones: {
          id: 33,
          group: "E",
          groupLabel: "Neurohormones",
          label: "Neurohormones",
          name: "functionalDiagnosticsRecommendations_Neurohormones",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.Neurohormones,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_HormonesAndOtherAnalytes: {
          id: 34,
          group: "E",
          groupLabel: "Hormones And Other Analytes",
          label: "Hormones And Other Analytes",
          name: "functionalDiagnosticsRecommendations_HormonesAndOtherAnalytes",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.HormonesAndOtherAnalytes,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_Neurotransmitters: {
          id: 35,
          group: "E",
          groupLabel: "Neurotransmitters",
          label: "Neurotransmitters",
          name: "functionalDiagnosticsRecommendations_Neurotransmitters",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.Neurotransmitters,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        functionalDiagnosticsRecommendations_DNA: {
          id: 36,
          group: "E",
          groupLabel: "DNA",
          label: "DNA",
          name: "functionalDiagnosticsRecommendations_DNA",
          question: "IgG Food Reaction Test Food Print",
          type: "checkboxgroup",
          validation: "",
          options: DIAGNOSTICS.ITEMS.DNA,
          error: {
            active: false,
            message: "IgG Food Reaction Test Food Print"
          },
          value: ""
        },
        otherRecommendations: {
          id: 37,
          group: "E",
          groupLabel: "E.	Health Plan Summary",
          name: "otherRecommendations",
          label: <span>Other Recommendations</span>,
          question: "Other Recommendations",
          type: "checkbox",
          validation: "",
          isChecked: false,
          options: [],
          error: {
            active: false,
            message: "Please Enter Other Recommendations"
          },
          value: "otherRecommendations"
        },
        otherRecommendations_details: {
          id: 38,
          group: "E",
          groupLabel: "E.	Health Plan Summary",
          name: "otherRecommendations_details",
          question: "Other Recommendations Details",
          type: "textArea",
          validation: "",
          options: [],
          error: {
            active: false,
            message: "Please enter Other Recommendations"
          },
          value: ""
        },
        healthoutcome: {
          id: 39,
          group: "F",
          groupLabel: "F.	Health Outcome",
          name: "healthoutcome",
          label: <span>Health Outcome</span>,
          question: "Health Outcome",
          type: "checkbox",
          validation: "",
          isChecked: false,
          options: [],
          error: {
            active: false,
            message: "Please Enter Health Outcome"
          },
          value: "healthoutcome"
        },
        healthoutcome_details: {
          id: 40,
          group: "F",
          groupLabel: "Health Outcome",
          name: "healthoutcome_details",
          question: "Health Outcome",
          type: "radio",
          validation: "",
          options: DIAGNOSTICS.ITEMS.healthoutcome,
          error: {
            active: false,
            message: "Health Outcome"
          },
          value: ""
        },
        targethealthoutomes: {
          id: 41,
          group: "F",
          groupLabel: "G.	Health Plan Summary",
          name: "targethealthoutomes",
          label: <span>Target Health Outcomes</span>,
          question: "Target Health Outcomes",
          type: "checkbox",
          validation: "",
          isChecked: false,
          options: [],
          error: {
            active: false,
            message: "Please Target Health Outcomes"
          },
          value: "targethealthoutomes"
        },
        targethealthoutomes_details: {
          id: 42,
          group: "F",
          groupLabel: "G.	Target Health Outcomes",
          name: "targethealthoutomes_details",
          question: "Target Health Outcomes",
          type: "textArea",
          validation: "",
          options: [],
          error: {
            active: false,
            message: "Please enter Other Recommendations"
          },
          value: ""
        },
        nextsteps: {
          id: 43,
          group: "G",
          groupLabel: "G.	Next Steps",
          name: "nextsteps",
          label: <span>Next Steps</span>,
          question: "Next Steps",
          type: "checkbox",
          validation: "",
          isChecked: false,
          options: [],
          error: {
            active: false,
            message: "Please Next Steps"
          },
          value: "nextsteps"
        },
        nextsteps_reschedule: {
          id: 44,
          group: "G",
          groupLabel: "G.	Next Appoinment Date",
          label: "Select Next Appoinment Date if needed",
          name: "nextsteps_reschedule",
          question: "Next Appointment Date",
          type: "button",
          validation: "",
          error: {
            active: false,
            message: "Please enter Next Steps"
          },
          value: ""
        },
        nextsteps_details: {
          id: 45,
          group: "G",
          groupLabel: "G.	Next Steps",
          name: "nextsteps_details",
          question: "Next Steps",
          type: "textArea",
          validation: "",
          options: [],
          error: {
            active: false,
            message: "Please enter Next Steps"
          },
          value: ""
        }
      },

      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      }
    };

    this._goToNext = this._goToNext.bind(this);
    this._updatFields = this._updatFields.bind(this);
    this._consultForm = this._consultForm.bind(this);
    //this._patientInfo = this._patientInfo.bind(this)
    this._setAppointment = this._setAppointment.bind(this);
    this.modalSwitch = this.modalSwitch.bind(this);
    this._getConsulstations = this._getConsulstations.bind(this);
    this._lockButton = this._lockButton.bind(this);
  }

  componentDidMount() {
    this._getConsulstations();
    this._updatFields();
    this._getConcernList();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.duration) {
      if (this.state.duration !== nextProps.duration) {
        this.setState(
          {
            duration: nextProps.duration
          },
          () => {
            //this._goToNext();
          }
        );
      }
    }
  }

  componentWillUnmount() {
    // use intervalId from the state to clear the interval
    //clearInterval(this.state.intervalId);
  }

  _setConcernListLabel = list => {
    const fdr = "functionalDiagnosticsRecommendations_";
    const fdrs = [
      `${fdr}cns`,
      `${fdr}rgcc`,
      `${fdr}genosense`,
      `${fdr}gpl`,
      `${fdr}cellmaxLife`,
      `${fdr}caam`,
      "healthoutcome_details"
    ];

    const labeling = options => {
      return options.map(option => {
        if (typeof option.label === "string") {
          option.label = <span>{option.label}</span>;
          return option;
        } else {
          return option;
        }
      });
    };

    fdrs.map(f => {
      const options = labeling(list[f].options);
      list[f].options = options;
    });

    this.setState({
      fieldsUpdated: true
    });

    return list;
  };

  _getConcernList() {
    concern
      .get()
      .then(result => {
        const conscernList = [];
        result.forEach(item => {
          conscernList.push(item.name);
        });
        const inputs = this.state.inputs;
        inputs.concern.data = conscernList;

        this._setConcernListLabel(inputs);

        this.setState({ inputs });
      })
      .catch(e => {
        console.log(e);
      });
  }

  _addToList(str) {
    const data = { name: str };
    return concern
      .add(data)
      .then(r => Promise.resolve(r))
      .catch(e => {
        console.log(e);
      });
  }

  _consultForm() {
    const styles = {
      formContainer: {
        width: "100%",
        JsDisplay: "flex",
        display: "-webkit-box" /* OLD - iOS 6-, Safari 3.1-6 */,
        display: "-moz-box" /* OLD - Firefox 19- (buggy but mostly works) */,
        display: "-ms-flexbox" /* TWEENER - IE 10 */,
        display: "-webkit-flex" /* NEW - Chrome */,
        display: "flex" /* NEW, Spec - Opera 12.1, Firefox 20+ */,
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "flex-start",
        textAlign: "left",
        fontSize: 18
      },
      inputContainer: {
        display: "flex",
        flexFlow: "column wrap",
        justifyContent: "flex-start",
        alignItems: "flex-start",
        marginBottom: "10px",
        fontSize: 18
      },
      inputLabel: {
        color: Colors.blueDarkAccent,
        fontSize: 18,
        marginBottom: "10px",
        marginTop: "10px",
        width: "100%",
        textAlign: "left"
      }
    };
    // if (this.state.btnLocked) {
    //   return (
    //     <div className={reportDiv}>
    //       {/**this._patientInfo()**/}
    //       {this._consultDetails()}
    //       <ConsultReport appointment={this.state.appointment} />
    //     </div>
    //   );
    // }
    const tooltipCancel = { description: "Not yet available" };
    return (
      <div className={formDiv}>
        {/**this._patientInfo()**/}

        <Form
          user={this.props.user}
          submitForm={this._goToNext}
          buttonText={"Save"}
          btnLocked={this.state.btnLocked}
          otherButton={[this.state.otherButton.finalize]}
          handleChange={this._handleChange.bind(this)}
          inputs={this.state.fields}
          formError={this.state.formError}
          formSuccess={this.state.formSuccess}
          location={this.props.location}
          styles={styles}
          addToList={this._addToList}
        />
        <div style={{ display: "flex", justifyContent: "center" }}>
          <ReactTooltip />
          <Button type="disabled">
            <button
              data-type="light"
              data-html
              data-tip={tooltipWrapper(tooltipCancel)}
            >
              Preview as PDF
            </button>
          </Button>
        </div>
      </div>
    );
  }

  // FOR FUTURE USE
  // _consultDetails() {
  //   const consult = this.state.fields;

  //   consult.forEach(item => {
  //     console.log(item.value);
  //   });
  // }

  _updatFields() {
    const additionalFields = [];
    const currentFields = [
      this.state.inputs.concern,
      this.state.inputs.history,
      this.state.inputs.assimilation,
      this.state.inputs.communication,
      this.state.inputs.defenseAndRepair,
      this.state.inputs.energy,
      this.state.inputs.structuralIntegrity,
      this.state.inputs.transport,
      this.state.inputs.medications_supplements,
      this.state.inputs.functionalDiagnosticsRecommendations,
      this.state.inputs.otherRecommendations,
      this.state.inputs.healthoutcome,
      this.state.inputs.targethealthoutomes,
      this.state.inputs.nextsteps
      // temporary remove autohide
      //
      // this.state.inputs.concern,
      // this.state.inputs.history,
      // this.state.inputs.assimilation,
      // this.state.inputs.assimilation_details,
      // this.state.inputs.communication,
      // this.state.inputs.communication_details,
      // this.state.inputs.defenseAndRepair,
      // this.state.inputs.defenseAndRepair_details,
      // this.state.inputs.energy,
      // this.state.inputs.energy_details,
      // this.state.inputs.structuralIntegrity,
      // this.state.inputs.structuralIntegrity_details,
      // this.state.inputs.transport,
      // this.state.inputs.transport_details,
      // this.state.inputs.medications_supplements,
      // this.state.inputs.medications_supplements_details,
      // this.state.inputs.functionalDiagnosticsRecommendations,
      // this.state.inputs.functionalDiagnosticsRecommendations_genosense,
      // this.state.inputs.functionalDiagnosticsRecommendations_cns,
      // this.state.inputs.functionalDiagnosticsRecommendations_rgcc,
      // this.state.inputs.otherRecommendations,
      // this.state.inputs.otherRecommendations_details,
      // this.state.inputs.healthoutcome,
      // this.state.inputs.healthoutcome_details,
      // this.state.inputs.targethealthoutomes,
      // this.state.inputs.targethealthoutomes_details,
      // this.state.inputs.nextsteps,
      // this.state.inputs.nextsteps_details,
      //  this.state.inputs.nextsteps_reschedule
    ];
    currentFields.forEach(item => {
      if (item.type === "checkbox") {
        if (item.isChecked) {
          if (item.value === "nextsteps") {
            additionalFields.push(this.state.inputs[`${item.name}_reschedule`]);
            additionalFields.push(this.state.inputs[`${item.name}_details`]);
          } else if (item.value === "functionalDiagnosticsRecommendations") {
            additionalFields.push(this.state.inputs[`${item.name}_cns`]);
            additionalFields.push(this.state.inputs[`${item.name}_rgcc`]);
            additionalFields.push(this.state.inputs[`${item.name}_genosense`]);
            additionalFields.push(this.state.inputs[`${item.name}_gpl`]);
            additionalFields.push(this.state.inputs[`${item.name}_caam`]);
            additionalFields.push(
              this.state.inputs[`${item.name}_cellmaxLife`]
            );
          } else {
            additionalFields.push(this.state.inputs[`${item.name}_details`]);
          }
        }
      }
    });

    // sort fields
    const allFields = additionalFields.concat(currentFields);
    const sortedfields = allFields.sort(this.objectSort("id"));
    const ns = this.sortObj(sortedfields, "asc");
    this.setState({
      fields: Object.values(ns)
    });
  }

  sortObj(obj, order) {
    "use strict";

    // eslint-disable-next-line one-var
    let key;
    // eslint-disable-next-line prefer-const
    let tempArry = [];
    let i;
    // eslint-disable-next-line prefer-const
    let tempObj = {};

    // eslint-disable-next-line guard-for-in
    for (key in obj) {
      tempArry.push(key);
    }

    tempArry.sort((a, b) => a.toLowerCase().localeCompare(b.toLowerCase()));

    if (order === "desc") {
      for (i = tempArry.length - 1; i >= 0; i--) {
        tempObj[tempArry[i]] = obj[tempArry[i]];
      }
    } else {
      for (i = 0; i < tempArry.length; i++) {
        tempObj[tempArry[i]] = obj[tempArry[i]];
      }
    }

    return tempObj;
  }

  _handleChange(value, input, validated, event) {
    const inputName = input;

    const newInput = this.state.inputs[inputName];
    newInput.value = value;
    newInput.error.active = validated;

    if (inputName === "functionalDiagnosticsRecommendations_genosense") {
      newInput.options.forEach((item, i) => {
        if (item.value === value) {
          newInput.options[i].isChecked = event.target.checked;
        }
      });
    } else if (inputName === "functionalDiagnosticsRecommendations_cns") {
      newInput.options.forEach((item, i) => {
        if (item.value === value) {
          newInput.options[i].isChecked = event.target.checked;
        }
      });
    } else if (inputName === "functionalDiagnosticsRecommendations_rgcc") {
      newInput.options.forEach((item, i) => {
        if (item.value === value) {
          newInput.options[i].isChecked = event.target.checked;
        }
      });
    } else if (inputName === "functionalDiagnosticsRecommendations_gpl") {
      newInput.options.forEach((item, i) => {
        if (item.value === value) {
          newInput.options[i].isChecked = event.target.checked;
        }
      });
    } else if (inputName === "functionalDiagnosticsRecommendations_caam") {
      newInput.options.forEach((item, i) => {
        if (item.value === value) {
          newInput.options[i].isChecked = event.target.checked;
        }
      });
    } else if (
      inputName === "functionalDiagnosticsRecommendations_cellmaxLife"
    ) {
      newInput.options.forEach((item, i) => {
        if (item.value === value) {
          newInput.options[i].isChecked = event.target.checked;
        }
      });
    } else if (inputName === "healthoutcome_details") {
      newInput.options.forEach((item, i) => {
        newInput.options[i].isChecked = false;
        if (item.value === value) {
          newInput.options[i].isChecked = event.target.checked;
        }
      });
    }

    if (newInput.type === "checkbox") {
      if (newInput.name === "healthoutcome") {
        if (newInput.isChecked === false) {
          this.state.inputs.healthoutcome_details.options.forEach(item => {
            // eslint-disable-next-line no-param-reassign
            item.isChecked = false;
          });
        } else {
          this.state.inputs.healthoutcome_details.value = "";
        }
      }

      newInput.isChecked = event.target.checked;
    }

    if (inputName === "nextsteps_reschedule") {
      this.modalSwitch("scheduleSlots", true);
    }

    this.setState(
      {
        inputName: newInput
      },
      () => {
        this._updatFields();
      }
    );
  }

  _getConsulstations() {
    const obj = {
      patient_id: this.state.appointment.patient_id,
      field: "appointment_code",
      field_value: this.state.appointment.appointment_code
    };

    consults
      .getConsult(obj)
      .then(res => {
        if (res.length > 0) {
          this.setState({ loading: true });
          const inputs = res[0].details;
          this._lockButton(res[0]);
          this._assignNewObjects(inputs, res[0].status);
        } else {
          // let str = 'patient_id-'+this.state.appointment.patient_id
          // consults.getConsult(str)
          // .then(res=>{
          //   if(res.length > 0) this._showLastConsultData(res[res.length-1])
          // })
        }
      })
      .catch(e => {
        console.log(e);
      });
    this._updatFields();
  }

  _showLastConsultData(res) {
    const inputs = this.state.inputs;

    inputs.history.value = res.details.history;
    Object.keys(res.details).forEach(e => {
      if (
        e !== "functionalDiagnosticsRecommendations_details" &&
        e !== "healthoutcome_details" &&
        e !== "nextsteps_reschedule"
      ) {
        inputs[e].isChecked = true;
        inputs[e].value = res.details[e];
      } else if (e === "functionalDiagnosticsRecommendations_details") {
        const list = res.details[e].split(",");
        const option = inputs.functionalDiagnosticsRecommendations_cns.options.concat(
          inputs.functionalDiagnosticsRecommendations_genosense.options,
          inputs.functionalDiagnosticsRecommendations_rgcc.options,
          inputs.functionalDiagnosticsRecommendations_gpl.options,
          inputs.functionalDiagnosticsRecommendations_caam.options,
          inputs.functionalDiagnosticsRecommendations_cellmaxLife.options
        );

        list.forEach(l => {
          const index = option.findIndex(x => x.value === l);
          option[index].isChecked = true;
        });
      } else if (e === "healthoutcome_details") {
        const index = inputs[e].options.findIndex(
          x => x.value === res.details[e]
        );
        inputs[e].options[index].isChecked = true;
      } else if (e === "nextsteps_reschedule") {
        inputs[e].value = res.details[e];
        inputs[e].label = `Next Appointment Date will be on  ${moment(
          res.details[e]
        ).format("lll")}`;
      }
    });

    this.setState({ inputs });
    this._updatFields();
  }

  _lockButton(obj) {
    let btnLocked = this.state.btnLocked;
    const otherButton = this.state.otherButton;

    if (obj.status === 1) {
      btnLocked = true; // temporay false for testing
      otherButton.finalize.locked = true; // temporay false for testing
      this.props.toggleConference(false); // temporay true for testing
    } else {
      btnLocked = false;
      otherButton.finalize.locked = false;
    }

    this.setState({
      btnLocked
    });
  }

  _assignNewObjects(obj, status) {
    const inputs = this.state.inputs;
    if (this.state.btnLocked) {
      Object.keys(inputs).forEach(item => {
        inputs[item].disabled = true;
      });
    }

    Object.keys(obj).forEach(item => {
      if (item === "nextsteps_reschedule") {
        if (status === 0) {
          if (obj[item] === "") {
            inputs[item].label = "Select Next Appoinment Date if needed";
          } else {
            this.setState({ reschduleDate: obj[item] });
            inputs[item].value = obj[item];
            inputs[item].label = `Next Appointment Date will be on  ${moment(
              obj[item]
            ).format("lll")}`;
          }
        }
      } else if (item === "functionalDiagnosticsRecommendations_details") {
        const option = inputs.functionalDiagnosticsRecommendations_cns.options.concat(
          inputs.functionalDiagnosticsRecommendations_genosense.options,
          inputs.functionalDiagnosticsRecommendations_rgcc.options,
          inputs.functionalDiagnosticsRecommendations_gpl.options,
          inputs.functionalDiagnosticsRecommendations_caam.options,
          inputs.functionalDiagnosticsRecommendations_cellmaxLife.options
        );

        if (obj[item]) {
          const items = obj[item].split(",");
          items.forEach(itm => {
            const i = option.findIndex(x => x.value === itm);
            option[i].isChecked = true;
          });
        }
      } else if (item === "healthoutcome_details") {
        const option = inputs.healthoutcome_details.options;
        inputs[item].value = obj[item];
        const i = option.findIndex(x => x.value === obj[item]);
        if (i > -1) {
          option[i].isChecked = true;
        }
      } else if (
        inputs[item].type === "checkbox" ||
        inputs[item].type === "radio"
      ) {
        // if(item == 'nextsteps'){
        //   if(status === 0){
        //     inputs[item].isChecked = true
        //   }
        // }
        inputs[item].isChecked = true;
      } else {
        inputs[item].value = obj[item];
      }
    });

    this.setState(
      {
        inputs
      },
      () => {
        this._doneLoading();
        this._updatFields();
      }
    );
  }

  _doneLoading() {
    this.setState({ loading: false });
  }

  _goSave(t_obj, final) {
    const obj = t_obj;
    obj.field = "appointment_id";
    obj.field_value = this.state.appointment.id;

    consults.getConsult(obj).then(res => {
      if (res.length > 0) {
        if (res[0].status === 1) {
          this._saveConsultation(final);
        } else {
          this._updateConsultation(res[0].id, final);
        }
      } else {
        this._saveConsultation(final);
      }
    });

    setTimeout(() => {
      const formSuccess = this.state.formSuccess;
      if (formSuccess.active) {
        formSuccess.active = false;
        formSuccess.message = "";
        this.setState({ formSuccess });
      }
    }, 5000);

    if (final) {
      this.props.back(true);
    }
  }

  _goToNext(e) {
    // Required
    if (this.state.inputs.healthoutcome_details.value === "") {
      // eslint-disable-next-line no-undef
      this.setState({
        formError: {
          active: true,
          message: "F. Health Outcome is required. Please update accordingly."
        }
      });
      return;
    }
    // check if consults exist
    const obj = { patient_id: this.state.appointment.patient_id };
    let final = false;

    if (e) {
      final = e.target.name === "finalize";
    }

    if (final) {
      confirmAlert({
        closeOnEscape: false,
        closeOnClickOutside: false,
        title: "", // Title dialog
        message: "Do you wish to finalize your consult notes?", // Message dialog
        buttons: [
          {
            label: "Yes",
            onClick: () => this._goSave(obj, final)
          },
          {
            label: "I'm not sure."
          }
        ]
      });
    } else {
      this._goSave(obj, final);
    }
  }

  _setAppointment(dt) {
    let reschduleDate = this.state.reschduleDate;
    reschduleDate = dt;
    const nextsteps_reschedule = this.state.inputs.nextsteps_reschedule;
    nextsteps_reschedule.value = dt;
    nextsteps_reschedule.label = `Next Appoinment will be on ${moment(
      dt
    ).format("LLL")}`;
    this.setState(
      {
        nextsteps_reschedule,
        reschduleDate
      },
      () => {
        this.modalSwitch("scheduleSlots", false);
      }
    );
  }

  _createAppointment(dt) {
    let inputs;
    if (this.state.appointment.concern !== "" && dt) {
      inputs = {
        NoPassword: true,
        name: this.state.appointment.name,
        email: this.state.appointment.email,
        date: dt,
        doctorId: this.state.appointment.doctor_id,
        concern: this.state.appointment.concern,
        date_requested: moment()._d,
        patient_company_id: this.props.user.company_id,
        appointment_code: `${this.state.appointment.appointment_code}_rs`,
        active: true
      };
    } else {
      // console.log("ERROR: PASWORD and CONCERN MUST NOT EMPTY");
      this.setState({
        formError: {
          active: true,
          message: "PASWORD and CONCERN MUST NOT EMPTY"
        },
        buttonText: "Try Again"
      });
      return;
    }

    // check if slot is still availableSched
    this._checkIfStillAvailable(moment(inputs.date).toISOString())
      .then(res => {
        if (res) {
          // eslint-disable-next-line no-undef
          alert("not available");
        } else {
          this._create(inputs);
        }
      })
      .catch(e => console.log(e));
  }

  _create(inputs) {
    appointment
      .create(inputs)
      .then(() => {
        this.setState({
          reschduleDate: ""
        });
      })
      .catch(e => {
        console.log("Appoinment Create Error");
        console.log(e);
      });
  }

  _updateAppointment() {
    const data = {
      status: 1
    };

    appointment
      .update(this.state.appointment.id, data)
      .then(() => {})
      .catch(e => {
        console.log("Appoinment Create Error");
        console.log(e);
      });
  }

  _checkIfStillAvailable(dte) {
    const qry = { active: false, apmt_date: dte };

    return appointment
      .getAppointmentsList(qry)
      .then(res => {
        if (res && res.length > 0) {
          return Promise.resolve(true);
        }
        return Promise.resolve(false);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _saveConsultation(final) {
    const inputs = this.state.inputs;
    const data = this._getNeededValues(inputs);
    data.appointment_code = this.state.appointment.appointment_code;
    if (final) {
      data.status = 1;
      this._updateAppointment();
    } else {
      data.status = 0;
    }
    data.last_updated = moment()._d;
    if (this.state.reschduleDate !== "") {
      data.details.nextsteps_reschedule = this.state.reschduleDate;
    }

    consults
      .save(data)
      .then(res => {
        if (res) {
          if (this.state.reschduleDate !== "" && final) {
            data.details.nextsteps_reschedule = this.state.reschduleDate;
            //return
            this._createAppointment(this.state.reschduleDate);
          }
          this._getConsulstations();
          //alert('CONSULTATION SAVED');
          const formSuccess = this.state.formSuccess;
          formSuccess.active = true;
          formSuccess.message = "Updated";
          this.setState({
            formSuccess
          });
        }
        this._getConsulstations();
      })
      .catch(e => {
        console.log("ERROR ");
        console.log(e);
      });
  }

  _updateConsultation(id, final) {
    const inputs = this.state.inputs;
    const data = this._getNeededValues(inputs);

    if (final) {
      data.status = 1;
      this._updateAppointment();
    } else {
      data.status = 0;
    }
    data.last_updated = moment()._d;
    if (this.state.reschduleDate !== "") {
      data.details.nextsteps_reschedule = this.state.reschduleDate;
    }

    consults
      .update(data, id)
      .then(res => {
        if (res) {
          if (this.state.reschduleDate !== "" && final) {
            data.details.nextsteps_reschedule = this.state.reschduleDate;
            //return
            this._createAppointment(this.state.reschduleDate);
          }
          this._getConsulstations();
          //alert('CONSULTATION UPDATED');
          const formSuccess = this.state.formSuccess;
          formSuccess.active = true;
          formSuccess.message = "Updated";
          this.setState({
            formSuccess
          });
        }
      })
      .catch(e => {
        console.log("ERROR ");
        console.log(e);
      });
  }

  _getNeededValues(inputs) {
    const data = {
      appointment_id: this.state.appointment.id,
      patient_id: parseInt(this.state.appointment.patient_id),
      employee_id: parseInt(this.state.appointment.employee_id),
      doctor_id: this.state.appointment.doctor_id,
      consult_date: moment()._d,
      details: {},
      callduration: this.state.duration
    };

    if (inputs) {
      Object.keys(inputs).forEach(item => {
        if (inputs[item].group === "A" || inputs[item].group === "B") {
          if (item === "concern") {
            data.concern = inputs[item].value;
          } else {
            data.details[item] = inputs[item].value;
          }
        } else if (inputs[item].type === "checkbox") {
          if (inputs[item].isChecked) {
            data.details[item] = inputs[item].value;
            if (item === "nextsteps") {
              data.details[`${item}_reschedule`] =
                inputs[`${item}_reschedule`].value;
              data.details[`${item}_details`] = inputs[`${item}_details`].value;
            } else if (item === "functionalDiagnosticsRecommendations") {
              const functionalDiagnosticsRecommendationsSelections = [];
              const option = inputs[`${item}_cns`].options.concat(
                inputs[`${item}_genosense`].options,
                inputs[`${item}_rgcc`].options,
                inputs[`${item}_gpl`].options,
                inputs[`${item}_caam`].options,
                inputs[`${item}_cellmaxLife`].options
              );
              option.forEach(i => {
                if (i.isChecked) {
                  functionalDiagnosticsRecommendationsSelections.push(i.value);
                }
              });
              data.details[
                `${item}_details`
              ] = functionalDiagnosticsRecommendationsSelections.join();
            } else {
              data.details[`${item}_details`] = inputs[`${item}_details`].value;
            }
          }
        }
      });
    }
    return data;
  }

  objectSort(property) {
    let sortOrder = 1;
    if (property[0] === "-") {
      sortOrder = -1;
      // eslint-disable-next-line no-param-reassign
      property = property.substr(1);
    }
    return function(a, b) {
      // eslint-disable-next-line no-nested-ternary
      const result =
        a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;
      return result * sortOrder;
    };
  }

  modalSwitch(name, bol) {
    const modal = this.state.modal;
    modal[name] = bol;
    this.setState({
      modal
    });
  }

  render() {
    return (
      <div className={maindiv}>
        {this.state.loading ? null : this._consultForm()}
        <Modal
          isOpen={this.state.modal.scheduleSlots}
          onRequestClose={() => this.modalSwitch("scheduleSlots", false)}
          shouldCloseOnOverlayClick={false}
          style={customStyles}
        >
          <div className={`modal-header ${labelClass}`}>
            Select an appointment date
            <Button classes={[closeBtn]} styles={{ minWidth: "unset" }}>
              <button onClick={() => this.modalSwitch("scheduleSlots", false)}>
                Close
              </button>
            </Button>
          </div>
          <div
            className={scrollClass}
            style={{
              minHeight: 400,
              maxHeight: 400,
              overflow: "auto",
              alignContent: "start"
            }}
          >
            <Slots
              doctorId={this.state.appointment.doctor_id}
              setDatetime={this._setAppointment}
            />
          </div>
        </Modal>
      </div>
    );
  }
} // end of class

// STYLING

let maindiv = css({
  display: "flex",
  width: "100%",
  flexWrap: "wrap"
});

const pseudoElemStyle = {
  content: '""',
  position: "absolute",
  top: 0,
  left: -1,
  width: 15,
  height: 15,
  background: "white",
  border: "1px solid #ccc",
  borderRadius: 3,
  transition: "250ms ease",
  cursor: "pointer",
  zIndex: 100
};

let formDiv = css({
  width: "100%",
  fontSize: 18,
  '& input[type="text"], & textarea': {
    ...inputStyleObj
  },

  "& span": {
    textAlign: "left !important",
    fontWeight: 600,
    color: Colors.blueDarkAccent,
    margin: "20px 5px 10px",
    display: "block"
  },

  "& input": {
    position: "relative",
    "&:checked": {
      "&::before": {
        borderColor: `${Colors.skyblue} !important`,
        background: `${Colors.skyblue} !important`,
        boxShadow: "0 5px 15px #81cce670"
      },
      "& + span": {
        color: `${Colors.skyblue} !important`
      }
    },
    color: "red",
    "&::before": {
      ...pseudoElemStyle
    }
  },

  "& label": {
    cursor: "pointer",
    display: "grid",
    gridTemplateColumns: "30px 1fr",
    "& > span": {
      marginTop: 0,
      fontWeight: "normal",
      color: "#000",
      transition: "250ms ease"
    }
  },

  '& input[type="radio"]': {
    position: "relative",
    "&::before": {
      ...pseudoElemStyle,
      borderRadius: "100%"
    }
  },

  '& button[name="finalize"]': {
    backgroundColor: `${Colors.pink} !important`,
    marginTop: `20px !important`,
    borderRadius: `30px !important`,
    maxWidth: 230,
    boxShadow: "0 3px 3px rgba(0,0,0,.1)",
    transition: "250ms ease",
    "&:hover": {
      backgroundColor: `#d383b6e8 !important`,
      boxShadow: `0 5px 10px ${Colors.pink}90`
    }
  }
});

let btn = css({
  //float: 'right',
  padding: "0 5px",
  backgroundColor: "white",
  border: "2px solid #80cde9",
  color: "#80cde9",
  borderRadius: "5px",
  margin: "0px",
  fontSize: 18,
  cursor: "pointer",
  ":hover": {
    backgroundColor: "#80cde9",
    border: "2px solid #80cde9",
    color: "white"
  }
});

const customStyles = {
  content: {
    textAlign: "center",
    zIndex: 6,
    width: "70%",
    height: 500,
    //  height: '55vh',
    top: "55%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: 20,
    boxShadow: "-3px 3px 30px #424242"
  }
};

let reportDiv = css({
  //border: '1px solid red',

  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  width: "100%"
});

const disabledButton = css({
  backgroundColor: "gray",
  width: "250px",
  padding: "11px 50px",
  border: "none",
  borderRadius: "6px",
  color: "white",
  fontSize: "1rem",
  display: "block",
  margin: "auto",
  marginTop: "0px",
  marginBottom: "20px"
});
