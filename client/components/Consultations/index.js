import { css } from "glamor";
import moment from "moment-timezone";
import Draggable from "react-draggable";
import VideoCall from "../../components/VideoCall/index.js";
import ConsultForm from "./consultForm";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

import AnswersService from "../../utils/answerService.js";
import SummaryService from "../../utils/summaryService.js";
import UserService from "../../utils/userService.js";
import Constants from "../../constants";
import Card from "../NewSkin/Wrappers/Card";
import Button from "../NewSkin/Components/Button";

const Answer = new AnswersService();
const summary = new SummaryService();
const User = new UserService();
const hoursPerSlot = Constants.HOURSPERSLOT;

import PatientProfile from "../DoctorDashboard/patientProfile";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import {
  scrollClass,
  default_tab,
  active_tab,
  closeBtn
} from "../NewSkin/Styles";
import TabWrapper from "../NewSkin/Wrappers/TabWrapper";
import Colors from "../NewSkin/Colors";
import PatientDetails from "../DoctorDashboard/patientProfile/patientDetails";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "consultForm",
      duration: 0,
      enableConference: true,
      appointment: this.props.appointment_detail,
      queuedPatients: this.props.queuedPatients,
      activeDrags: 0,
      miniOn: true,
      videoDefaultLocation: { x: 0, y: 0 },
      minimized: false
    };

    this._toggleConference = this._toggleConference.bind(this);
    this._getDuration = this._getDuration.bind(this);
    this._swithcTab = this._swithcTab.bind(this);
    this.onStart = this.onStart.bind(this);
    this.onStop = this.onStop.bind(this);
  }

  componentWillMount() {
    const width = window.innerWidth;
    this.setState({
      videoDefaultLocation: { x: width - 340, y: 90 }
    });
  }

  _toggleConference(b) {
    this.setState({ enableConference: b });
  }

  _swithcTab(tabname) {
    this.setState({ activeTab: tabname });
  }

  _getDuration(durtn) {
    let duration = this.state.duration;
    duration = durtn;
    this.setState({ duration });
  }

  _otherLiks() {
    return (
      <TabWrapper tabCount={2}>
        <div
          onClick={() => {
            this._swithcTab("consultForm");
          }}
          className={
            this.state.activeTab === "consultForm" ? active_tab : default_tab
          }
        >
          Consultation form
        </div>

        <div
          onClick={() => {
            this._swithcTab("healthprofile");
          }}
          className={
            this.state.activeTab === "healthprofile" ? active_tab : default_tab
          }
        >
          Patient Health Profile
        </div>
      </TabWrapper>
    );
  }

  _getAnswersByDistinctQuestion(userid) {
    return Answer.getAnswerList(
      {
        column: "question_id",
        operator: "gt",
        value: 0,
        distinct_question: true
      },
      userid
    )
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(err => {
        console.log(err);
      });
  }

  _getPatientInfoByID(userID) {
    return User.get(userID)
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(err => {
        console.log(err);
      });
  }

  _getSummary(userid) {
    return summary.get({ userId: userid, distinct: true }).then(res => {
      if (res.name === "Error") {
        console.log(res);
      } else {
        return Promise.resolve(res);
      }
    });
  }

  _renderPatientDetails() {
    let info = this.props.appointment_detail;
    return <PatientDetails info={info} />;
  }

  onStart() {
    this.setState({ activeDrags: ++this.state.activeDrags });
  }

  onStop() {
    this.setState({ activeDrags: --this.state.activeDrags });
  }

  minimize = () => {
    const width = window.innerWidth;
    const height = window.innerHeight;

    if (this.state.minimized) {
      if (!this.state.miniOn) {
        this.setState({
          videoDefaultLocation: { x: 770, y: 90 }
        });
      } else {
        this.setState({
          videoDefaultLocation: { x: width - 340, y: 90 }
        });
      }

      setTimeout(() => {
        this.setState({
          minimized: false
        });
      }, 100);
    } else {
      // minimizing
      this.setState({
        videoDefaultLocation: { x: width - 250, y: height - 70 },
        minimized: true
      });
    }
  };

  resizing = () => {
    const width = window.innerWidth;

    if (this.state.miniOn) {
      this.setState({ miniOn: !this.state.miniOn });

      this.setState({
        videoDefaultLocation: { x: 770, y: 90 }
      });
    } else {
      this.setState({
        videoDefaultLocation: { x: width - 340, y: 90 }
      });

      setTimeout(() => {
        this.setState({ miniOn: !this.state.miniOn });
      }, 50);
    }
  };

  render() {
    let index = this.props.queuedPatients.findIndex(
      x => x.id == this.state.appointment.id
    );
    let b = moment(); //now

    let a = moment(this.props.queuedPatients[index].datetime).add(
      hoursPerSlot,
      "hours"
    );

    if (
      this.props.queuedPatients[index].time_extension &&
      this.props.queuedPatients[index].time_extension > 0
    ) {
      a.add(this.props.queuedPatients[index].time_extension, "minutes");
    }

    let lapsed = moment(a).isBefore(moment());
    const dragHandlers = { onStart: this.onStart, onStop: this.onStop };

    const minimizedPosition = this.state.minimized && {
      position: this.state.videoDefaultLocation
    };
    const miniOnPosition = !this.state.miniOn && {
      position: this.state.videoDefaultLocation
    };

    return (
      <div className={maindiv}>
        <GridContainer
          columnSize={"1fr"}
          gap={20}
          styles={{ width: "90%", maxWidth: "100%", margin: "0 auto" }}
        >
          <Card styles={{ padding: "30px" }}>
            <Button
              classes={[closeBtn]}
              styles={{ minWidth: "unset", marginTop: -22, marginLeft: 3 }}
            >
              <button title="Close" onClick={() => this.props.back()}>
                X
              </button>
            </Button>
            <section className={scrollClass}>
              {this._renderPatientDetails()}
              {this._otherLiks()}
              {this.state.activeTab === "healthprofile" ? (
                <PatientProfile
                  active={true}
                  getAnswers={this._getAnswersByDistinctQuestion}
                  getPatientInfo={this._getPatientInfoByID}
                  userid={this.props.appointment_detail.patient_id}
                  getScores={this._getSummary}
                  //back={this._back}
                  activeTab={this.props.activeTab}
                  setActiveView={this.props.setActiveView}
                  user={this.props.user}
                  restricted={false}
                  noInfo={true}
                />
              ) : (
                  <ConsultForm
                    back={this.props.back}
                    duration={this.state.duration}
                    user={this.props.user}
                    toggleConference={this._toggleConference}
                    appointment_detail={this.state.appointment}
                  />
                )}
            </section>
          </Card>

          {this.state.enableConference && !lapsed && (
            <Draggable
              handle="strong"
              defaultPosition={this.state.videoDefaultLocation}
              onStart={this.onStart}
              onStop={this.onStop}
              {...miniOnPosition}
              {...minimizedPosition}
            >
              <div
                className={`box no-cursor ${videoWrapper(
                  this.state.miniOn,
                  this.state.minimized
                )}`}
              >
                {!this.state.minimized && (
                  <div className={videoTopMenuStyle}>
                    {this.state.miniOn ? (
                      <strong className={["cursor", DraggableStyle].join(" ")}>
                        Drag
                      </strong>
                    ) : (
                        <span />
                      )}
                    <span onClick={this.resizing}>
                      {this.state.miniOn ? `Enlarge` : `Reduce`}
                    </span>

                    <span
                      className={min}
                      onClick={this.minimize}
                      title="Minimize"
                    />
                  </div>
                )}

                <Card
                  styles={{
                    overflow: "hidden",
                    margin: "0 auto",
                    padding: 0,
                    display: "flex",
                    flexWrap: "wrap",
                    justifyContent: "center",
                    position: "relative",
                    borderRadius: "0 0 5px 5px"
                  }}
                >
                  {/* <div style={{ flex: 1 }}> */}
                  <MuiThemeProvider>
                    <VideoCall
                      getDuration={this._getDuration}
                      user={this.props.user}
                      queuedPatients={this.props.queuedPatients}
                      appointment_detail={this.state.appointment}
                      updateRoot={this.props.updateRoot}
                      minimized={this.state.minimized}
                      miniOn={this.state.miniOn}
                      toggleConference={this._toggleConference}
                    />
                  </MuiThemeProvider>
                  {/* </div> */}

                  {this.state.minimized && (
                    <span
                      className={this.state.minimized ? max : min}
                      onClick={this.minimize}
                      title="Maximize"
                    >
                      {
                        this.state.minimized &&
                        <svg
                          id="maximize"
                          viewBox={"0 0 650 650"}
                          style={{ width: 30, height: 30 }}
                        >
                          <use
                            id='maximize'
                            x="0" y="0"
                            xlinkHref={'/static/icons/maximize_icon.svg#maximize'}
                          />
                        </svg>
                      }
                    </span>
                  )}
                </Card>
              </div>
            </Draggable>
          )}
        </GridContainer>
      </div>
    );
  }
} // end of class

// STYLING

const min = css({
  position: "relative",
  background: "rgba(0,0,0,0)",
  transition: "250ms ease",
  cursor: "pointer",
  "::before": {
    content: '""',
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 15,
    height: 2,
    background: "#fff"
  },
  ":hover": {
    background: "rgba(255,255,255,.2)"
  }
});

const max = css(min, {
  width: 30,
  height: 30,
  margin: 'auto',
  padding: 5,
  paddingLeft: 10,
  opacity: 1,
  transition: '250ms ease',
  '> svg': {
    fill: '#ccc'
  },
  ':hover': {
    '> svg': {
      fill: Colors.skyblue
    }
  },
  '::before': {
    content: ''
  }
  // "::before": {
  //   content: '""',
  //   position: "absolute",
  //   top: "50%",
  //   left: "50%",
  //   transform: "translate(-50%, -50%)",
  //   width: 20,
  //   height: 20,
  //   borderRadius: 3,
  //   border: `1px solid #ccc`
  // },
  // ":hover": {
  //   "::before": {
  //     borderColor: Colors.skyblue
  //   }
  // }
});

const videoWrapper = (miniOn, minimized) =>
  css({
    transition: "all 100ms ease",
    width: minimized ? 250 : miniOn ? 320 : 500,
    zIndex: 100,
    position: "fixed",
    top: -80,
    left: -20,
    background: "#fff",
    fontSize: miniOn ? 12 : 16,
    overflow: "hidden",
    boxShadow: "0 3px 5px rgba(0,0,0,.1)",
    border: "1px solid #eee",
    borderRadius: 8,
    ":hover": {
      borderColor: Colors.blueDarkAccent,
      boxShadow: `0 8px 10px rgba(0,0,0,.1)`
    }
  });

const detailsWrapper = css({
  maxWidth: "70%",
  "& > *": {
    textAlign: "left !important"
  },
  "& > span": {
    color: Colors.blueDarkAccent,
    fontWeight: 500
  }
});

const photoWrapper = css({
  margin: "auto auto",
  width: 100,
  height: 100,
  borderRadius: "100%",
  overflow: "hidden",
  position: "absolute",
  top: "50%",
  transform: "translateY(-50%)",
  "> img": {
    width: "110%",
    position: "absolute",
    top: "50%",
    left: 0,
    transform: "translateY(-50%)"
  }
});

let maindiv = css({
  display: "flex",
  width: "100%",
  flexWrap: "wrap"
});

let videoDiv = css({
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  position: "fixed",
  top: 10,
  left: 10,
  zIndex: 100
});

let formLinkStyle = css({
  display: "flex",
  justifyContent: "center",
  backgroundColor: "#364563",
  width: "100%",
  [`& ul`]: {
    listStyleType: "none",
    margin: "0",
    padding: "0",
    overflow: "hidden",
    backgroundColor: "#364563"
  },

  [`& ul li`]: {
    float: "left"
  },

  [`& ul li a`]: {
    display: "block",
    color: "white",
    textAlign: "center",
    padding: "5px 16px",
    textDecoration: "none"
  },

  ["& li a:hover"]: {
    cursor: "pointer",
    color: "#80cde9"
    //backgroundColor: "#80cde9",
  }
});

let wrapperInfo = css({
  width: "100%"
});

let patientPhoto = css({
  borderRadius: "100%"
});

let patientDetailsWrap = css({
  margin: "10px auto 10px 20px",
  display: "inline-block",
  width: "400px"
});

let infoSpan = css({
  display: "block"
});

let infoLabel = css({
  color: "#000"
});

let avatarStyle = css({
  marginRight: "auto",
  marginLeft: "auto",
  borderRadius: "25%",
  border: "1px solid rgba(204, 204, 204, 0.3)",
  height: "100px",
  width: "100px",
  align: "center",
  verticalAlign: "center",
  border: "2px solid #36c4f1",
  ":hover": {
    backgroundColor: "#ED81B9",
    border: "2px solid #ED81B9",
    color: "white"
  },
  //  color: 'rgb(54, 69, 99)',
  "@media(max-width: 767px)": {
    height: "75px",
    width: "75px"
  }
});

const DraggableStyle = css({
  cursor: "grab",
  fontWeight: "normal",
  ":active": {
    cursor: "grabbing"
  }
});

const videoTopMenuStyle = css({
  display: "grid",
  gridTemplateColumns: "1fr 1fr 40px",
  justifyContent: "space-around",
  cursor: "pointer",
  background: Colors.blueDarkAccent,
  color: "#fff",
  // borderRadius: 10,
  fontSize: "x-small",
  textTransform: "uppercase"
});
