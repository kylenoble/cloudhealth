import React from "react";

import Form from "../Form";

import AnswerService from "../../utils/answerService.js";
import Button from "../NewSkin/Components/Button";
import Colors from "../NewSkin/Colors";

const Answer = new AnswerService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      birthday: {
        id: 1,
        name: "birthday",
        label: "Birthday",
        question: "Ex. mm/dd/yyyy",
        type: "birthday",
        validation: "birthday",
        disabled: true,
        options: [],
        error: {
          active: false,
          message: "Please enter a valid birthday i.e. mm/dd/yyyy"
        },
        value: ""
      },
      gender: {
        id: 2,
        name: "gender",
        label: "Gender",
        question: "Select your gender",
        type: "input",
        validation: "default",
        disabled: true,
        options: [
          {
            label: "Female",
            value: "Female"
          },
          {
            label: "Male",
            value: "Male"
          },
          {
            label: "Trans*",
            value: "Trans*"
          }
        ],
        error: {
          active: false,
          message: "Please select your gender"
        },
        value: "default"
      },
      relationship: {
        id: 3,
        name: "relationship",
        question: "Select your Relationship Status",
        type: "select",
        validation: "required",
        label: 'Relationship Status',
        options: [
          {
            label: "Married",
            value: "Married"
          },
          {
            label: "Single",
            value: "Single"
          },
          {
            label:
              "Partnered (lived or living with common-law wife or husband, single parent)",
            value: "Partnered"
          }
        ],
        error: {
          active: false,
          message: "Please select a relationship"
        },
        value: "default"
      },
      race: {
        id: 4,
        name: "race",
        label: 'Race',
        question: "Select your Race",
        type: "select",
        validation: "required",
        options: [
          {
            label: "Asian",
            value: "Asian"
          },
          {
            label: "Australian/Pacific Islander",
            value: "Australian/Pacific Islander"
          },
          {
            label: "Black or African origin",
            value: "Black or African origin"
          },
          {
            label: "Caucasian",
            value: "Caucasian"
          },
          {
            label: "Hispanic, Latino, or Spanish origin",
            value: "Hispanic, Latino, or Spanish origin"
          },
          {
            label: "Other",
            value: "Other"
          }
        ],
        error: {
          active: false,
          message: "Please select a race"
        },
        value: "default"
      },
      raceOther: {
        id: 4,
        name: "raceOther",
        label: "Other Race",
        question: "Enter Race",
        type: "required",
        validation: "required",
        options: [],
        error: {
          active: false,
          message: "Please enter a value for race"
        },
        dependency: {
          name: "race",
          value: "Other"
        },
        value: ""
      },
      psa: {
        id: 5,
        name: "psa",
        label: 'PSA',
        question:
          "Have you had a PSA done? (A Prostate Specific Antigen (PSA) is used to screen for prostate cancer and other inflammatory conditions of the prostate)",
        type: "select",
        validation: "select",
        options: [
          {
            label: "Yes",
            value: "Yes"
          },
          {
            label: "No",
            value: "no"
          }
        ],
        dependency: {
          name: "gender",
          value: "Male"
        },
        error: {
          active: false,
          message: "Please select an answer"
        },
        value: "default"
      },
      psaLevel: {
        id: 6,
        name: "psaLevel",
        label: 'PSA Level',
        question: "What is your PSA level?",
        type: "select",
        validation: "select",
        options: [
          {
            label: "0-2",
            value: "0-2"
          },
          {
            label: "2-4",
            value: "2-4"
          },
          {
            label: "4-10",
            value: "4-10"
          },
          {
            label: ">10",
            value: ">10"
          }
        ],
        dependency: {
          name: "psa",
          value: "Yes"
        },
        error: {
          active: false,
          message: "Please select an answer"
        },
        value: "default"
      },
      maleIssues: {
        id: 7,
        name: "maleIssues",
        question: "Select all that apply:",
        label: 'Male Issues',
        type: "select",
        validation: "select",
        options: [
          {
            label: "No symptoms",
            value: "No symptoms"
          },
          {
            label: "Prostate Enlargement",
            value: "prostate enlargement"
          },
          {
            label: "Prostate Infection",
            value: "prostate infection"
          },
          {
            label: "Change in Libido",
            value: "change in libido"
          },
          {
            label: "Impotence",
            value: "impotence"
          },
          {
            label: "Difficulty Obtaining an Erection",
            value: "difficulty obtaining an erection"
          },
          {
            label: "Difficulty Maintaining an Erection",
            value: "difficulty maintaining an erection"
          }
        ],
        dependency: {
          name: "gender",
          value: "Male"
        },
        error: {
          active: false,
          message: "Please select an answer"
        },
        value: ""
      },
      menstrual: {
        id: 8,
        name: "menstrual",
        question: "Select menstrual cycle",
        label: 'Menstrual Cycle',
        type: "select",
        validation: "select",
        options: [
          {
            label: "Regularly Menstruating",
            value: "regularly menstruating"
          },
          {
            label: "Irregularly Menstruating",
            value: "irregularly menstruating"
          },
          {
            label: "Surgical Menopause, Biologic/Natural Menopause",
            value: "Surgical Menopause, Biologic/Natural Menopause"
          }
        ],
        dependency: {
          name: "gender",
          value: "Female"
        },
        error: {
          active: false,
          message: "Please select an answer"
        },
        value: "default"
      },
      pregnant: {
        id: 9,
        name: "pregnant",
        question: "Are you Currently Pregnant?",
        type: "select",
        label: 'Pregnant',
        validation: "select",
        options: [
          {
            label: "Yes",
            value: "Yes"
          },
          {
            label: "No",
            value: "no"
          }
        ],
        dependency: {
          name: "menstrual",
          notValue: "Surgical Menopause, Biologic/Natural Menopause"
        },
        error: {
          active: false,
          message: "Please select an answer"
        },
        value: "default"
      },
      numberChildren: {
        id: 10,
        name: "numberChildren",
        label: 'Number of Children',
        question: "How many children do you have?",
        type: "select",
        validation: "select",
        options: [
          {
            label: "0",
            value: 0
          },
          {
            label: "1",
            value: 1
          },
          {
            label: "2",
            value: 2
          },
          {
            label: "3",
            value: 3
          },
          {
            label: "4",
            value: 4
          },
          {
            label: "5",
            value: 5
          },
          {
            label: "6",
            value: 6
          },
          {
            label: "7",
            value: 7
          },
          {
            label: "8+",
            value: "8+"
          }
        ],
        dependency: {
          name: "relationship",
          notValue: "Single"
        },
        error: {
          active: false,
          message: "Please select an answer"
        },
        value: "default"
      },
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      },
      fields: [],
      formFor: null,
      editInfo: false
    };

    this._handleChange = this._handleChange.bind(this);
    this._goToNext = this._goToNext.bind(this);
  }

  // eslint-disable-next-line react/sort-comp
  supportsFlexBox() {
    // eslint-disable-next-line no-undef
    const test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  componentDidMount() {
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      // eslint-disable-next-line no-undef
      flexibility(document.body);
    }

    //  if(this.props.logged){
    Answer.getAnswerList({ column: "question_id", operator: "lt", value: 11 })
      .then(res => {
        if (res.name === "Error") {
          console.log(res);
        } else if (res.length > 0) {
          this.setState(
            {
              formFor: "new" // replace "profile" with "new" to record all changes
            },
            () => {
              this._createAnswers(res);
            }
          );
        } else {
          this.setState({
            formFor: "new"
          });
        }
      })
      .catch(e => {
        console.log(e);
      }); // you would show/hide error messages with component state here
    //  }
    this._updateFields();
  }

  _createAnswers(answers) {
    const birthday = this.state.birthday;
    const gender = this.state.gender;
    const relationship = this.state.relationship;
    const race = this.state.race;
    const raceOther = this.state.raceOther;
    const psa = this.state.psa;
    const psaLevel = this.state.psaLevel;
    const maleIssues = this.state.maleIssues;
    const menstrual = this.state.menstrual;
    const pregnant = this.state.pregnant;
    const numberChildren = this.state.numberChildren;

    for (const answer of answers) {
      if (answer.name === "birthday") {
        birthday.value = answer.value;
      } else if (answer.name === "gender") {
        gender.value = answer.value;
      } else if (answer.name === "psa") {
        psa.value = answer.value;
      } else if (answer.name === "psaLevel") {
        psaLevel.value = answer.value;
      } else if (answer.name === "relationship") {
        relationship.value = answer.value;
      } else if (answer.name === "maleIssues") {
        maleIssues.value = answer.value;
      } else if (answer.name === "race") {
        if (race.options.filter(e => e.value === answer.value).length > 0) {
          race.value = answer.value;
        } else {
          race.value = "Other";
          raceOther.value = answer.value;
        }
      } else if (answer.name === "menstrual") {
        menstrual.value = answer.value;
      } else if (answer.name === "pregnant") {
        pregnant.value = answer.value;
      } else if (answer.name === "numberChildren") {
        numberChildren.value = answer.value;
      }
    }

    this.setState({
      birthday,
      gender,
      relationship,
      race,
      raceOther,
      psa,
      psaLevel,
      maleIssues,
      menstrual,
      pregnant,
      numberChildren
    });

    if (this.state.race.value === "default") {
      this.setState({
        formFor: "new"
      });
    }
    this._updateFields();
  }

  _toggleEditProfile = (e) => {
    const btn = e.target;

    if (btn.innerHTML === 'Cancel Edit') {
      btn.innerHTML = 'Edit Profile';
      btn.parentNode.style = `background: ${Colors.pink} !important`

      this.setState({ 
        formSuccess: {
          active: false,
          message: ""
        }
      })
    } else {
      btn.innerHTML = 'Cancel Edit';
      btn.parentNode.style = `background: ${Colors.pinkDarkAccent} !important`
    }
    this.setState({ editInfo: !this.state.editInfo })
  }

  render() {
    return (
      <div style={styles.container}>
        <div style={styles.introInfo}>
          <span>
            Your birthday, gender, race, and relationship will only be shared
            if your doctor requires it for treatment or family history.
            </span>
          <div style={{ textAlign: 'right', marginTop: 20 }}>
            <Button type="pink" styles={{ marginLeft: 'auto' }}>
              <button onClick={(e) => this._toggleEditProfile(e)}>Edit Profile</button>
            </Button>
          </div>
        </div>
        <Form
          btnLocked={!this.state.editInfo}
          account
          personalInfo
          editInfo={this.state.editInfo}
          submitForm={this._goToNext}
          buttonText={
            this.state.formFor === "profile"
              ? "Update My Profile"
              : "Add Information"
          }
          handleChange={this._handleChange}
          inputs={this.state.fields}
          secondaryOption=""
          formError={this.state.formError}
          formSuccess={this.state.formSuccess}
        />
      </div>
    );
  }

  _updateFields() {
    let currentFields;
    if (this.state.gender.value.toUpperCase() === "FEMALE") {
      currentFields = [
        this.state.birthday,
        this.state.gender,
        this.state.menstrual,
        this.state.pregnant,
        this.state.relationship,
        this.state.numberChildren,
        this.state.race
      ];
    } else {
      currentFields = [
        this.state.birthday,
        this.state.gender,
        this.state.psa,
        this.state.psaLevel,
        this.state.maleIssues,
        this.state.relationship,
        this.state.numberChildren,
        this.state.race
      ];
    }
console.log(currentFields)
    if (this.state.race.value === "Other") {
      currentFields.push(this.state.raceOther);
    }

    const fields = [];
    for (let i = 0; i <= currentFields.length - 1; i++) {
      const input = currentFields[i];
      if (!input.dependency) {
        fields.push(input);
      } else if (input.dependency) {
        if (input.dependency.value) {
          if (
            this.state[input.dependency.name].value.toUpperCase() === input.dependency.value.toUpperCase()
          ) {
            fields.push(input);
          }
        } else if (
          this.state[input.dependency.name].value.toUpperCase() === input.dependency.notValue.toUpperCase()
        ) {
          // console.log('no match')
        } else if (this.state[input.dependency.name].value !== "default") {
          fields.push(input);
        }
      } else {
        fields.push(input);
      }
    }
    //      console.log(fields);
    this.setState({
      fields
    });
    console.log(fields)
  }

  _goToNext() {
    this.setState({
      formError: {
        active: false,
        message: ""
      }
    });

    console.log(this.state.formFor);
    //return;

    if (this.state.formFor === "profile") {
      const inputs = this.state.fields;
      Answer.update(inputs)
        .then(res => {
          if (res) {
            console.log("update user profile");
            this.setState({
              formSuccess: {
                active: true,
                message: "Your Profile Was Updated"
              }
            });
            return this.props.onClick("personal");
          }
          this.setState({
            formError: {
              active: true,
              message: "There was an error updating your profile"
            }
          });
        })
        .catch(e => {
          if (e.error === "This email is already in use") {
            const newInput = this.state.email;
            newInput.error.active = true;
            newInput.error.message = e.error;
            this.setState({
              inputName: newInput
            });
          } else {
            this.setState({
              formError: {
                active: true,
                message: e.error
              }
            });
          }
        }); // you would show/hide error messages with component state here
    } else {
      //var inputs = this.state.fields
      let inputs = [
        this.state.birthday,
        this.state.gender,
        this.state.psa,
        this.state.psaLevel,
        this.state.maleIssues,
        this.state.menstrual,

        this.state.pregnant,

        this.state.relationship,
        this.state.numberChildren
      ];

      if (this.state.race.value === "Other") {
        inputs.push(this.state.raceOther);
      } else {
        inputs.push(this.state.race);
      }
      if (this.state.race.value === "Other") {
        inputs = [
          ...inputs.slice(0, inputs.length - 2),
          ...inputs.slice(inputs.length - 1)
        ];
      }

      Answer.create(inputs)
        .then(res => {
          if (res) {
            console.log("create user profile");
            this.setState({
              formSuccess: {
                active: true,
                message: "Your Profile Was Updated"
              },
              formFor: "profile"
            });
          } else {
            this.setState({
              formError: {
                active: true,
                message: "There was an error submitting your answers"
              }
            });
          }
        })
        .catch(e => {
          this.setState({
            formError: {
              active: true,
              message: e.error
            }
          });
        }); // you would show/hide error messages with component state here
    }
  }

  _handleChange(value, input, validated) {
    const inputName = input;
    const newInput = this.state[inputName];

    if (input === "maleIssues") {
      if (newInput.value !== "") {
        const healthIssues = newInput.value.split(",");

        if (healthIssues.indexOf(value) < 0) {
          healthIssues.push(value);
        } else {
          const location = healthIssues.indexOf(value);
          healthIssues.splice(location, 1);
        }

        newInput.value = healthIssues.join(",");
      } else {
        newInput.value = value;
      }
    } else {
      newInput.value = value;
    }

    newInput.error.active = validated;
    this._updateFields();
    this.setState({
      inputName: newInput
    });
  }
}

const styles = {
  container: {
    // fontFamily: '"Roboto",sans-serif',
    // display: "flex",
    // flexDirection: "column",
    // alignItems: "center",
    // justifyContent: "center",
    width: '100%'
  },
  introInfo: {
    // maxWidth: "360px",
    fontFamily: '"Roboto",sans-serif',
    color: "#364563",
    fontSize: "14px",
    margin: "0 0 20px",
    textAlign: "justified",
    width: '100%'
  },
  formContainer: {
    border: "2px solid rgba(151,151,151,0.2)",
    borderRadius: "6px",
    maxWidth: "400px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.20)"
  },
  header: {
    borderBottom: "2px solid rgba(225,232,238,1)",
    padding: "15px 20px",
    fontSize: "18px",
    color: "#364563",
    fontFamily: '"Roboto-Medium",sans-serif',
    width: "360px"
  },
  form: {
    margin: "10px 30px 20px 20px"
  },
  formInput: {
    padding: "13px 30px 13px 30px",
    marginTop: "20px",
    width: "287px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    fontSize: "14px"
  },
  options: {
    marginTop: "15px",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif',
    padding: "13px 30px 13px 30px",
    WebkitAppearance: "none",
    MozAppearance: "none",
    appearance: "none",
    width: "350px",
    backgroundColor: "#fff",
    borderColor: "#d9d9d9 #ccc #b3b3b3",
    borderradius: "4px",
    border: "2px solid #E1E8EE",
    borderSpacing: "0",
    borderCollapse: "separate",
    height: "50px",
    outline: "none",
    overflow: "hidden",
    position: "relative",
    cursor: "pointer"
  },
  optionsDefault: {
    color: "rgba(169, 169, 169, 1)"
  },
  selectArrowZone: {
    cursor: "pointer",
    display: "table-cell",
    position: "relative",
    textAlign: "center",
    verticalAlign: "middle",
    width: "25px",
    paddingRight: "5px"
  },
  selectArrow: {
    borderColor: "#999 transparent transparent",
    borderStyle: "solid",
    borderWidth: "5px 5px 2.5px",
    display: "inline-block",
    height: "0",
    width: "0",
    position: "absolute",
    right: "-18em",
    top: "-1.75em"
  },
  button: {
    backgroundColor: "#ED81B9",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "0px 0px 20px 0px",
    cursor: "pointer"
  },
  register: {
    textDecoration: "none",
    margin: "20px 10px",
    color: "#43484D",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif'
  }
};
