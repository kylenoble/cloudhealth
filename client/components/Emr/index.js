import React, { Component } from "react";
import "./emrStyle.scss";
import PatientInfo from "./patient";

// const Constants.PI = [1, 2, 3, 4, 5, 6, 7, 8]
// const Constants.HI = [53]
// const Constants.DTX = [54, 55, 56, 57, 58, 59, 60, 61, 62]
// const Constants.MEH = [85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95]
// const Constants.EEN = [112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124]
// const Constants.HM = [96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111]
// const Constants.MTL = [76, 77, 78, 79, 80, 81, 82, 83, 84]
// const Constants.OTHER = [125, 126, 127, 128, 129, 130, 131, 132]
// const Constants.WD = [63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75]

class Emr extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hlthAns: this.props.hlthAns,
      summary: this.props.summary,
      answersData: "",
      body: {
        bmi: "0.00",
        whr: "0.00",
        value: ""
      },
      nutrition: {
        value: ""
      },
      sleep: {
        value: ""
      },
      health: {
        value: ""
      },
      stress: {
        value: ""
      },
      exercise: {
        value: ""
      },
      overallScore: {
        value: 0
      },
      personalInfo: {
        empId: "",
        company: "",
        type: "",
        department: "",
        branch: "",
        name: "",
        gender: "",
        maritalStatus: "",
        birthday: "",
        age: "",
        race: "",
        psaLevel: "",
        psa: "",
        maleIssues: "",
        menstrualCycle: ""
      }
    };
  }

  componentDidMount() {
    this._createpersonalInfo();
    this._createSummary(this.props.summary);
  }

  _createpersonalInfo() {
    var userData = this.props.userData;
    var personalInfo = this.state.personalInfo;

    personalInfo.empId = userData.employee_id;
    personalInfo.company = userData.company;
    personalInfo.name = userData.name;
    personalInfo.branch = userData.branch;
    personalInfo.department = userData.department;
    personalInfo.type = userData.type;

    for (const answers of this.props.hlthAns) {
      if (Constants.PI.indexOf(answers.question_id) != -1) {
        if (answers.question_id == 1) personalInfo.birthday = answers.value;
        if (answers.question_id == 2) personalInfo.gender = answers.value;
        if (answers.question_id == 3)
          personalInfo.maritalStatus = answers.value;
        if (answers.question_id == 4) personalInfo.race = answers.value;
        if (answers.question_id == 5) personalInfo.psa = answers.value;
        if (answers.question_id == 6) personalInfo.psaLevel = answers.value;
        if (answers.question_id == 7) personalInfo.maleIssues = answers.value;
        if (answers.question_id == 8) personalInfo.menstrual = answers.value;
      }
    }

    this.setState({
      personalInfo: personalInfo
    });
  }

  _createSummary(summaries) {
    var body = this.state.body;
    var nutrition = this.state.nutrition;
    var sleep = this.state.sleep;
    var health = this.state.health;
    var stress = this.state.stress;
    var exercise = this.state.exercise;
    var overallScore = this.state.overallScore;

    var personalInfo = this.state.personalInfo;

    let ltstBodyVal;
    let ltstExerciseVal;
    let ltstStressVal;
    let ltstNutritionVal;
    let ltstSleepVal;
    let ltstHealthVal;

    let itemCount = 0;

    let healthScore = 0;

    for (const summary of summaries) {
      let score = summary.value;
      let weightedScore = score * 0.167;

      if (summary.name === "weightScore") {
        ltstBodyVal = weightedScore;
        body.value = score;
      } else if (summary.name === "bmi") {
        weightedScore = 0;
        body.bmi = summary.value;
      } else if (summary.name === "whr") {
        weightedScore = 0;
        body.whr = summary.value;
      } else if (summary.name === "nutrition") {
        ltstNutritionVal = weightedScore;
        nutrition.value = score;
      } else if (summary.name === "sleep") {
        ltstSleepVal = weightedScore;
        sleep.value = score;
      } else if (summary.name === "detox") {
        ltstHealthVal = weightedScore;
        health.value = score;
      } else if (summary.name === "stress") {
        ltstStressVal = weightedScore;
        stress.value = score;
      } else if (summary.name === "exercise") {
        ltstExerciseVal = weightedScore;
        exercise.value = score;
      }
    }

    var hs =
      ltstBodyVal +
      ltstExerciseVal +
      ltstStressVal +
      ltstNutritionVal +
      ltstSleepVal +
      ltstHealthVal;
    overallScore.value = hs * 2;

    this.setState({
      body: body,
      nutrition: nutrition,
      sleep: sleep,
      health: health,
      stress: stress,
      exercise: exercise,
      overallScore: overallScore
    });
  }

  render() {
    return (
      <div className="maindiv">
        {/* <style dangerouslySetInnerHTML={{ __html: stylesheet }} /> */}
        <PatientInfo
          userData={this.state.personalInfo}
          hlthAns={this.state.hlthAns}
          summary={this.state.summary}
          answersData={this.state.answersData}
        />
      </div>
    );
  }
}

export default Emr;
