import React, {Component} from 'react';

class Patient extends Component {
  constructor(props) {
    super(props)
    this.state = {
     personal: this.props.userData
    }
  }



  personal(){
  //  console.log(this.props);
  let personal = this.state.personal
// this part will be replace by map function
    return(
      <div className="personalStyle">
        <div className="personalItem">Employee ID : {this.props.userData.employee_id}</div>
        <div className="personalItem">Company : {this.props.userData.company}</div>
        <div className="personalItem">Branch : {this.props.userData.branch}</div>
        <div className="personalItem">department : {this.props.userData.department}</div>
        <div className="personalItem">Name : {this.props.userData.name}</div>
        <div className="personalItem">Birthday</div>
        <div className="personalItem">Race</div>
        <div className="personalItem">Marital Status</div>
        <div className="personalItem">Number of Children</div>
        <div className="personalItem">PSA</div>
        <div className="personalItem">Male Issues</div>
        <div className="personalItem">Readiness Score</div>
        <div className="personalItem">Health Issues</div>
      </div>
    )
  }

  render() {
  //  console.log(this.props);
      return(
        <div className="container">
          {this.personal()}

        </div>
      )
  }
}

export default Patient
