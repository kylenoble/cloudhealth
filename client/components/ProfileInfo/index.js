import React from 'react'
import Link from 'next/link'

import Form from '../Form'

import AnswerService from '../../utils/answerService.js'
import CompanyService from '../../utils/companyService.js'
import UserService from '../../utils/userService.js';

const user = new UserService();
const cmpy = new CompanyService();
const answer = new AnswerService()

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeView: this.props.activeView,
      birthday: {
        id: 1,
        name: 'birthday',
        label: 'Enter Birthday',
        question: 'Ex. mm/dd/yyyy',
        type: 'birthday',
        //validation: 'birthday',
        options: [],
        error: {
          active: false,
          message: 'Please enter a valid birthday i.e. mm/dd/yyyy'
        },
        value: ''
      },
      gender: {
        id: 2,
        name: 'gender',
        question: 'Select your gender',
        type: 'select',
        validation: 'required',
        options: [
          {
            label: 'Female',
            value: 'Female'
          },
          {
            label: 'Male',
            value: 'Male'
          },
          {
            label: 'Trans*',
            value: 'Trans*'
          },
        ],
        error: {
          active: false,
          message: 'Please select your gender'
        },
        value: 'default'
      },
      relationship: {
        id: 3,
        name: 'relationship',
        question: 'Select your Relationship Status',
        type: 'select',
        validation: 'required',
        options: [
          {
            label: 'Married',
            value: 'Married'
          },
          {
            label: 'Single',
            value: 'Single'
          },
          {
            label: 'Partnered (lived or living with common-law wife or husband, single parent)',
            value: 'Partnered'
          },
        ],
        error: {
          active: false,
          message: 'Please select a relationship'
        },
        value: 'default'
      },
      race: {
        id: 4,
        name: 'race',
        question: 'Select your Race',
        type: 'select',
        validation: 'required',
        options: [
          {
            label: 'Asian',
            value: 'Asian'
          },
          {
            label: 'Australian/Pacific Islander',
            value: 'Australian/Pacific Islander'
          },
          {
            label: 'Black or African origin',
            value: 'Black or African origin'
          },
          {
            label: 'Caucasian',
            value: 'Caucasian'
          },
          {
            label: 'Hispanic, Latino, or Spanish origin',
            value: 'Hispanic, Latino, or Spanish origin'
          },
          {
            label: 'Other',
            value: 'Other'
          },
        ],
        error: {
          active: false,
          message: 'Please select a race'
        },
        value: 'default'
      },
      raceOther: {
        id: 4,
        name: 'raceOther',
        label: 'Race',
        question: 'Enter Race',
        type: 'required',
        validation: 'required',
        options: [],
        error: {
          active: false,
          message: 'Please enter a value for race'
        },
        dependency: {
          name: 'race',
          value: 'Other'
        },
        value: ''
      },
      psa: {
        id: 5,
        name: 'psa',
        question: 'Have you had a PSA done? (A Prostate Specific Antigen (PSA) is used to screen for prostate cancer and other inflammatory conditions of the prostate)',
        type: 'select',
        validation: 'select',
        options: [
          {
            label: 'Yes',
            value: 'Yes'
          },
          {
            label: 'No',
            value: 'no'
          },
        ],
        dependency: {
          name: 'gender',
          value: 'Male'
        },
        error: {
          active: false,
          message: 'Please select an answer'
        },
        value: 'default'
      },
      psaLevel: {
        id: 6,
        name: 'psaLevel',
        question: 'What is your PSA level?',
        type: 'select',
        validation: 'select',
        options: [
          {
            label: '0-2',
            value: '0-2'
          },
          {
            label: '2-4',
            value: '2-4'
          },
          {
            label: '4-10',
            value: '4-10'
          },
          {
            label: '>10',
            value: '>10'
          },
        ],
        dependency: {
          name: 'psa',
          value: 'Yes'
        },
        error: {
          active: false,
          message: 'Please select an answer'
        },
        value: 'default'
      },
      maleIssues: {
        id: 7,
        name: 'maleIssues',
        question: 'Select all that apply:',
        type: 'select',
        validation: 'select',
        options: [
          {
            label: 'No symptoms',
            value: 'No symptoms'
          },
          {
            label: 'Prostate Enlargement',
            value: 'prostate enlargement'
          },
          {
            label: 'Prostate Infection',
            value: 'prostate infection'
          },
          {
            label: 'Change in Libido',
            value: 'change in libido'
          },
          {
            label: 'Impotence',
            value: 'impotence'
          },
          {
            label: 'Difficulty Obtaining an Erection',
            value: 'difficulty obtaining an erection'
          },
          {
            label: 'Difficulty Maintaining an Erection',
            value: 'difficulty maintaining an erection'
          },
        ],
        dependency: {
          name: 'gender',
          value: 'Male'
        },
        error: {
          active: false,
          message: 'Please select an answer'
        },
        value: ''
      },
      menstrual: {
        id: 8,
        name: 'menstrual',
        question: 'Select menstrual cycle',
        type: 'select',
        validation: 'select',
        options: [
          {
            label: 'Regularly Menstruating',
            value: 'regularly menstruating'
          },
          {
            label: 'Irregularly Menstruating',
            value: 'irregularly menstruating'
          },
          {
            label: 'Surgical Menopause, Biologic/Natural Menopause',
            value: 'Surgical menopause, Biologic/Natural Menopause'
          },
        ],
        dependency: {
          name: 'gender',
          value: 'Female'
        },
        error: {
          active: false,
          message: 'Please select an answer'
        },
        value: 'default'
      },
      pregnant: {
        id: 9,
        name: 'pregnant',
        question: 'Are you Currently Pregnant?',
        type: 'select',
        validation: 'select',
        options: [
          {
            label: 'Yes',
            value: 'Yes'
          },
          {
            label: 'No',
            value: 'no'
          },
        ],
        dependency: {
          name: 'menstrual',
          notValue: 'Surgical menopause, Biologic/Natural Menopause'
        },
        error: {
          active: false,
          message: 'Please select an answer'
        },
        value: 'default'
      },
      numberChildren: {
        id: 10,
        name: 'numberChildren',
        question: 'How many children do you have?',
        type: 'select',
        validation: 'select',
        options: [
          {
            label: '0',
            value: 0
          },
          {
            label: '1',
            value: 1
          },
          {
            label: '2',
            value: 2
          },
          {
            label: '3',
            value: 3
          },
          {
            label: '4',
            value: 4
          },
          {
            label: '5',
            value: 5
          },
          {
            label: '6',
            value: 6
          },
          {
            label: '7',
            value: 7
          },
          {
            label: '8+',
            value: '8+'
          },
        ],
        dependency: {
          name: 'relationship',
          notValue: 'Single'
        },
        error: {
          active: false,
          message: 'Please select an answer'
        },
        value: 'default'
      },
      formError: {
        active: false,
        message: ''
      },
      formSuccess: {
        active: false,
        message: ''
      },
      fields: [],



      companies: [],
      departments: [],
      branches: [],
      fields: [],
      fullName: {
        id: '',
        name: 'fullName',
        question: 'Enter Name',
        type: 'text',
        validation: 'name',
        options: [],
        error: {
          active: false,
          message: 'Please enter a valid full name'
        },
        value: ''
      },
      email: {
        id: '',
        name: 'email',
        question: 'Enter valid Email',
        type: 'email',
        disabled: true,
        validation: 'email',
        options: [],
        error: {
          active: false,
          message: 'Please enter a valid email address'
        },
        value: ''
      },
      password: {
        id: '',
        name: 'password',
        question: 'Create Password',
        type: 'password',
        validation: 'password',
        options: [],
        error: {
          active: false,
          message: 'Passwords must have at least 1 number, 1 Uppercase letter, 1 Lowercase letter, and a minimum of 8 characters'
        },
        value: ''
      },
      company: {
        id: '',
        name: 'company',
        question: 'Enter Company Code',
        type: 'company',
        validation: 'company',
        options: [],
        error: {
          active: false,
          message: 'Please enter a valid Company code!'
        },
        value: ''
      },
      department: {
        id: '',
        name: 'department',
        question: 'Select your Department',
        type: 'dropdown',
        disabled: false,
        validation: 'required',
        options: [],
        error: {
          active: false,
          message: 'Please select a department'
        },
        value: ''
      },
      branch: {
        id: '',
        name: 'branch',
        question: 'Select branch',
        type: 'dropdown',
        disabled: false,
        validation: 'required',
        options: [],
        error: {
          active: false,
          message: 'Please select a branch'
        },
        value: ''
      },
      employeeId: {
        id: '',
        name: 'employeeId',
        question: 'Enter Employee Id',
        type: 'required',
        validation: 'required',
        options: [],
        error: {
          active: false,
          message: 'Please enter a valid employee id address'
        },
        value: ''
      },
      formError: {
        active: false,
        message: ''
      },
      formSuccess: {
        active: false,
        message: ''
      },
      updatePersonal: false,
      updateAccount: false
    }

    this._handleChange = this._handleChange.bind(this);
    this._goToNext = this._goToNext.bind(this)
  }

  supportsFlexBox() {
    var test = document.createElement('test');

    test.style.display = 'flex';

    return test.style.display === 'flex';
  }

  componentDidMount() {
    if (this.supportsFlexBox()) {
        // Modern Flexbox is supported
    } else {
        // Modern Flexbox is not supported
        flexibility(document.body);
    }

    this._updateFields()
  //  if(this.props.logged){
    answer.getAnswerList({column: 'question_id', operator: 'lt', value: 11})
    .then(res => {
        if (res.name === 'Error') {
            console.log(res)
        } else {
          if (res.length > 0) {
            this._createAnswers(res)
          }
        }
    })
    .catch(e => {
        console.log(e)
    })  // you would show/hide error messages with component state here
//  }

cmpy.get('branches')
.then(res => {
    if (res.name === 'Error') {
        console.log(res)
    } else {
      if (res.length > 0) {
        this.setState({
          branches: res
        })
      }
    }
})
.catch(e => {
    console.log(e)
})  // you would show/hide error messages with component state here

cmpy.list()
.then(res => {
    if (res.name === 'Error') {
        console.log(res)
    } else {
      if (res.length > 0) {
          this.setState({
            companies: res
          })

          this._getAnswers()
          this._updateFields()
      }
    }
})
.catch(e => {
    console.log(e)
})  // you would show/hide error messages with component state here

  }

  _getAnswers() {

    let userProfile = user.getProfile()
    if(userProfile.id){
      this.setState({
        updateAccount: true
      })
    }
    if (userProfile.name) {
      var name = this.state.fullName;
      var email = this.state.email;
      var company = this.state.company;
      var employeeId = this.state.employeeId;
      var department = this.state.department;
      var branch = this.state.branch;

      name.value = userProfile.name;
      email.value = userProfile.email;
      company.value = userProfile.company;
      employeeId.value = userProfile.employee_id;
      department.value = userProfile.department;
      branch.value = userProfile.branch;

      this.setState({
        fullName: name,
        email: email,
        company: company,
        employeeId: employeeId,
        department: department,
        branch: branch
      });

    }
  }


  _createAnswers(answers) {
    if(this.state.birthday.value){
      this.setState({
        updatePersonal: true
      })
    }
    var birthday = this.state.birthday;
    var gender = this.state.gender;
    var relationship = this.state.relationship;
    var race = this.state.race;
    var psa = this.state.psa;
    var psaLevel = this.state.psaLevel;
    var maleIssues = this.state.maleIssues;
    var menstrual = this.state.menstrual;
    var pregnant = this.state.pregnant;
    var numberChildren = this.state.numberChildren;

    for (const answer of answers) {
      if (answer.name === 'birthday') {
        birthday.value = answer.value;
      } else if (answer.name === 'gender') {
        gender.value = answer.value;
      } else if (answer.name === 'psa') {
        psa.value = answer.value;
      } else if (answer.name === 'psaLevel') {
        psaLevel.value = answer.value;
      } else if (answer.name === 'relationship') {
        relationship.value = answer.value;
      } else if (answer.name === 'maleIssues') {
        maleIssues.value = answer.value;
      } else if (answer.name === 'race') {
        race.value = answer.value;
      } else if (answer.name === 'menstrual') {
        menstrual.value = answer.value;
      } else if (answer.name === 'pregnant') {
        pregnant.value = answer.value;
      } else if (answer.name === 'numberChildren') {
        numberChildren.value = answer.value;
      }
    }

    this.setState({
      'birthday': birthday,
      'gender': gender,
      'relationship': relationship,
      'race': race,
      'psa': psa,
      'psaLevel': psaLevel,
      'maleIssues':maleIssues,
      'menstrual':menstrual,
      'pregnant':pregnant,
      'numberChildren':numberChildren
    });
  }

  render() {
    return(
      <div>
        <div style={styles.container}>
          <div style={styles.introInfo}>
          <h2>Profile Info</h2>
            <span>Your birthday, gender, race, and relationship
              will only be shared if your doctor
              requires it for treatment or family history.
            </span>
          </div>
          <Form
          companies = {this.state.companies}
          departments = {this.state.departments}
            header={this.props.location == 'profile' ? "Update Your Personal Info Below" : "Continue Your Profile Below"}
            submitForm={this._goToNext}
            buttonText={this.props.location == 'profile' ? "Update My Profile" : "Continue My Profile"}
            handleChange = {this._handleChange}
            inputs={this.state.fields}
            secondaryOption=""
            formError={this.state.formError}
            formSuccess={this.state.formSuccess}
          />
        </div>
      </div>
    )
  }

  _updateFields() {
    let currentFields = [
      this.state.email, this.state.company, this.state.employeeId,

      this.state.fullName,
      this.state.birthday,
      this.state.gender,
      this.state.psa,
      this.state.psaLevel,
      this.state.maleIssues,
      this.state.menstrual,
      this.state.relationship,
      this.state.numberChildren,
      this.state.race
    ]

    if (this.state.race.value == 'Other') {
      currentFields.push(this.state.raceOther);
    }

    let fields = []
    for (let i = 0; i <= currentFields.length - 1; i++) {
      let input = currentFields[i];

      if(i == 3 && this.state.branch.options.length > 1){
        let br = this.state.branch
        br.type = 'dropdown';
        br.disabled = false;
        fields.push(this.state.branch)
      }else if(i == 3) {
        if(this.state.branch.value && !this.state.branch.disabled ||this.state.branch.options.length > 1){
        let br = this.state.branch
        br.type = 'input';
        br.disabled = true;
        fields.push(this.state.branch)
        }
      }

      if(i == 3 && this.state.department.options.length > 1){
        let dpt = this.state.department
        dpt.type = 'dropdown';
        dpt.disabled = false;
        fields.push(this.state.department)
      }else if(i == 3) {
        if(this.state.department.value && !this.state.department.disabled ||this.state.department.options.length > 1){
        let dpt = this.state.department
        dpt.type = 'input';
        dpt.disabled = true;
        fields.push(this.state.department)
      }
      }

      if (!input.dependency) {
        fields.push(input)
      } else if (input.dependency) {
        if (input.dependency.value) {
          if (this.state[input.dependency.name].value !== input.dependency.value){
            // console.log('no match')
          } else {
            fields.push(input)
          }
        } else {
          if (this.state[input.dependency.name].value === input.dependency.notValue) {
            // console.log('no match')
          } else if (this.state[input.dependency.name].value !== 'default') {
            fields.push(input)
          }
        }
      } else {
        fields.push(input)
      }
    }
//      console.log(fields);
    this.setState({
     fields: fields
    })
  }

  _goToNext() {

    var inputs = this.state.fields
    let personalFields = []
    let accountFields = []
    for(const input of inputs){
      if(input.id){
        personalFields.push(input)
      }else{
        accountFields.push(input)
      }
    }

    this.setState({
      formError: {
        active: false,
        message: ''
      }
    })
if(personalFields){
  console.log("personalFields");


    if (this.state.updatePersonal) {
      console.log("UPDATE PERSONAL");
      answer.update(personalFields)
      .then(res => {
        if (res) {
          console.log('update user profile')
          this.setState({
            formSuccess: {
              active: true,
              message: 'Your Profile Was Updated'
            }
          })
          return this.props.onClick('personal')
        } else {
          this.setState({
            formError: {
              active: true,
              message: 'There was an error updating your profile'
            }
          })
        }
      })
      .catch(e => {
        if (e.error === "This email is already in use") {
          var newInput = this.state['email']
          newInput.error.active = true
          newInput.error.message = e.error
          this.setState({
            inputName: newInput
          })
        } else {
          this.setState({
            formError: {
              active: true,
              message: e.error
            }
          })
        }
      })  // you would show/hide error messages with component state here
    } else {
      var inputs = this.state.fields
      if (this.state.race.value === 'Other') {
        inputs = [
          ...inputs.slice(0, inputs.length - 2),
          ...inputs.slice(inputs.length - 1)
        ]
      }

    answer.create(personalFields)
      .then(res => {
        if (res) {
          console.log('should move on')
          this.props.onClick('readiness')
        } else {
          this.setState({
            formError: {
              active: true,
              message: 'There was an error submitting your answers'
            }
          })
        }
      })
      .catch(e => {
        this.setState({
          formError: {
            active: true,
            message: e.error
          }
        })
      })  // you would show/hide error messages with component state here
    }
}

    // ACOUNT FIELDS AREA

    if(accountFields){
      console.log('ACCOUNT FIELDS');
      console.log(accountFields);
        var company_name = '';
      for(let i=0; i < this.state.companies.length; i++){
        if(this.state.companies[i].code == this.state.company.value){
          company_name = this.state.companies[i].company_name
        }
      }
    //  company_name = this.state.branch.value == '' ? company_name : company_name+"/"+this.state.branch.value;
      if (this.state.updateAccount) {
        console.log("UPDATE ACCOUNT");
        var inputs = []
        if (this.state.password.value !== '') {
            inputs = [{
            'email': this.state.email.value,
            'password': this.state.password.value,
            'company':  company_name, // THIS PART WAS MODIFIED TO GET THE VALUE FROM THE OPTIONS OF THE COMPANY SELECTION
            'name': this.state.fullName.value,
            'employee_id': this.state.employeeId.value,
            'department' : this.state.department.value == 'Select' ? "" : this.state.department.value,
            'branch': this.state.branch.value
          }]
        } else {
          inputs = [{
            'email': this.state.email.value,
            'company': company_name, // THIS PART WAS MODIFIED TO GET THE VALUE FROM THE OPTIONS OF THE COMPANY SELECTION
            'name': this.state.fullName.value,
            'employee_id': this.state.employeeId.value,
            'department' : this.state.department.value == 'Select' ? "": this.state.department.value,
            'branch': this.state.branch.value
          }]
        }
        user.update(inputs)
        .then(res => {
          if (res) {
            console.log('update user profile')
            this.setState({
              formSuccess: {
                active: true,
                message: 'Your Profile Was Updated'
              }
            })
            return this.props.onClick('email')
          } else {
            this.setState({
              formError: {
                active: true,
                message: 'There was an error updating your profile'
              }
            })
          }
        })
        .catch(e => {
          if (e.error === "This email is already in use") {
            var newInput = this.state['email']
            newInput.error.active = true
            newInput.error.message = e.error
            this.setState({
              inputName: newInput
            })
          } else {
            this.setState({
              formError: {
                active: true,
                message: e.error
              }
            })
          }
        })  // you would show/hide error messages with component state here
      } else {
        // THIS PART WAS MODIFIED TO GET THE VALUE FROM THE OPTIONS OF THE COMPANY SELECTION
        user.create(this.state.email.value, this.state.password.value, company_name, this.state.fullName.value, this.state.employeeId.value, this.state.department.value, this.state.branch.value)
        .then(res => {
          if (res) {
            console.log('should move on')
            return this.props.onClick('personal')
          } else {
            this.setState({
              formError: {
                active: true,
                message: 'There was an error creating your account'
              }
            })
          }
        })
        .catch(e => {
          if (e.error === "This email is already in use") {
            var newInput = this.state['email']
            newInput.error.active = true
            newInput.error.message = e.error
            this.setState({
              inputName: newInput
            })
          } else {
            this.setState({
              formError: {
                active: true,
                message: e.error
              }
            })
          }
        })  // you would show/hide error messages with component state here
      }
    }


  }

  _handleChange(value, input, validated) {
    const inputName = input
    var newInput = this.state[inputName]
    var depts = [];
    var brnchs = [];
    let dept = this.state.department
    let brnch = this.state.branch
    let idholder = ''
    if (input === 'maleIssues') {
      if (newInput.value != '') {
        let healthIssues = newInput.value.split(',')

        if (healthIssues.indexOf(value) < 0) {
          healthIssues.push(value)
        } else {
          const location = healthIssues.indexOf(value)
          healthIssues.splice(location, 1)
        }

        newInput.value = healthIssues.join(',');
      } else {
        newInput.value = value
      }
    } else {
      newInput.value = value
    }

    if(input == 'company' && !validated){
      console.log("COMPANY VALIDATE");
      for(let i=0; i < this.state.companies.length; i++){

        if(this.state.companies[i].code == value){
          console.log("comy");
          depts.push({label: this.state.companies[i].dept_name, value: this.state.companies[i].dept_name})
          idholder = this.state.companies[i].id
        }
      }

      for(let x=0; x < this.state.branches.length; x++){
          if(this.state.branches[x].cmpy_id == idholder){
            console.log("brn");
            brnchs.push({label: this.state.branches[x].br_name, value: this.state.branches[x].br_name})
          }
      }

      if(depts.length > 1 && !validated){
        dept.options = depts
      }else {
        dept.options = []
        dept.value = ''
      }

      if(brnchs.length > 1 && !validated){
        brnch.options = brnchs
      }else {
        brnch.options = []
        brnch.value = ''
      }


    }else {
      if(this.state['company'].error.active){
        dept.options = []
      }
    }

    newInput.error.active = validated
    this._updateFields()
    this.setState({
      inputName: newInput
    })
  }

}

const styles = {
  container: {
    fontFamily: '"Roboto",sans-serif',
    display: '-webkit-flex',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  introInfo: {
    maxWidth: '360px',
    fontFamily: '"Roboto",sans-serif',
    color: '#364563',
    fontSize: '14px',
    margin: '20px',
    textAlign: 'justified',
  },
  formContainer: {
    border: '2px solid rgba(151,151,151,0.2)',
    borderRadius: '6px',
    maxWidth: '400px',
    display: '-webkit-flex',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    boxShadow: '0px 1px 3px 0px rgba(0,0,0,0.20)',
  },
  header: {
    borderBottom: '2px solid rgba(225,232,238,1)',
    padding: '15px 20px',
    fontSize: '18px',
    color: '#364563',
    fontFamily: '"Roboto-Medium",sans-serif',
    width: '360px',
  },
  form: {
    margin: '10px 30px 20px 20px',
  },
  formInput: {
    padding: '13px 30px 13px 30px',
    marginTop: '20px',
    width: '287px',
    border: '2px solid #E1E8EE',
    borderRadius: '6px',
    fontSize: '14px',
  },
  options: {
    marginTop: '15px',
    fontSize: '14px',
    fontFamily: '"Roboto",sans-serif',
    padding: '13px 30px 13px 30px',
    WebkitAppearance: 'none',
    MozAppearance: 'none',
    appearance: 'none',
    width: '350px',
    backgroundColor: '#fff',
    borderColor: '#d9d9d9 #ccc #b3b3b3',
    borderradius: '4px',
    border: '2px solid #E1E8EE',
    cursor: 'default',
    borderSpacing: '0',
    borderCollapse: 'separate',
    height: '50px',
    outline: 'none',
    overflow: 'hidden',
    position: 'relative',
    cursor: 'pointer'
  },
  optionsDefault: {
    color: 'rgba(169, 169, 169, 1)'
  },
  selectArrowZone: {
    cursor: 'pointer',
    display: 'table-cell',
    position: 'relative',
    textAlign: 'center',
    verticalAlign: 'middle',
    width: '25px',
    paddingRight: '5px',
  },
  selectArrow: {
    borderColor: '#999 transparent transparent',
    borderStyle: 'solid',
    borderWidth: '5px 5px 2.5px',
    display: 'inline-block',
    height: '0',
    width: '0',
    position: 'absolute',
    right: '-18em',
    top: '-1.75em',
  },
  button: {
    backgroundColor: '#ED81B9',
    width: '300px',
    padding: '11px 80px',
    border: 'none',
    borderRadius: '6px',
    color: 'white',
    fontFamily: '"Roboto",sans-serif',
    fontSize: '14px',
    margin: '0px 0px 20px 0px',
    cursor: 'pointer',
  },
  register: {
    textDecoration: 'none',
    margin: '20px 10px',
    color: '#43484D',
    fontSize: '14px',
    fontFamily: '"Roboto",sans-serif',
  }
}
