import React, {Component} from 'react'
import { css } from 'glamor'

import BodyInfo from '../BodyInfo'
import Exercise from '../Exercise'
import Sleep from '../Sleep'
import Nutrition from '../Nutrition'
import Stress from '../Stress'
import HealthProfile from '../HealthProfile'

class AddData extends Component {
    constructor(props) {
        super(props)

        this.state = {
        }

        this._backToDashboard = this._backToDashboard.bind(this)
    }

    render () {
        return (
            <div className={containerStyle}>
                <div className={buttonContainer}>
                    <button className={button} onClick={this._backToDashboard}>
                        Back
                    </button>
                </div>
                {this._renderAddDataCategory()}
            </div>
        )
    }

    _renderAddDataCategory() {
        if (this.props.dataCategory === 'Body') {
            return (
                <BodyInfo
                    location='profile'
                    onClick={this._backToDashboard}
                />
            )
        } else if (this.props.dataCategory === 'Exercise') {
            return (
                <Exercise
                    location='profile'
                    onClick={this._backToDashboard}
                />
            )
        } else if (this.props.dataCategory === 'Sleep') {
            return (
                <Sleep
                    location='profile'
                    onClick={this._backToDashboard}
                />
            )
        } else if (this.props.dataCategory === 'Nutrition') {
            return (
                <Nutrition
                    location='profile'
                    onClick={this._backToDashboard}
                />
            )
        } else if (this.props.dataCategory === 'Stress') {
            return (
                <Stress
                    location='profile'
                    onClick={this._backToDashboard}
                />
            )
        } else if (this.props.dataCategory === 'Health') {
            return (
                <HealthProfile
                    location='profile'
                    onClick={this._backToDashboard}
                />
            )
        }

    }

    _backToDashboard() {
        this.props.updateView('HealthProfile')
    }
}

let containerStyle = css({
    display: 'flex',
    flexDirection: 'column',
    flexFlow: 'column wrap',
    alignItems: 'center',
    justifyContent: 'flex-start',
    margin: '0 auto',
    width: '70em',
    paddingTop: '20px',
    minHeight: '80vh',
    backgroundColor: 'white',
    '@media(max-width: 767px)': {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        flexFlow: 'row wrap',
        margin: '0 auto',
        paddingTop: '30px',
    }
})

let buttonContainer = css({
    width: 'calc(100% - 20px)',
    display: 'flex',
    flexDirection: 'column',
    flexFlow: 'column wrap',
    alignItems: 'flex-end',
    justifyContent: 'center',
    padding: '65px 20px 0 0',
})

let button = css({
    backgroundColor: 'white',
    width: '100px',
    padding: '11px 30px',
    border: '2px solid #ED81B9',
    borderRadius: '6px',
    color: '#ED81B9',
    fontFamily: '"Roboto",sans-serif',
    fontSize: '14px',
    cursor: 'pointer',
    textAlign: 'center',
})

export default AddData
