import React, { useState, useEffect } from 'react'
import Card from '../NewSkin/Wrappers/Card';
import DashDataItem from '../DashboardData/dashDataItem';

const DashData = ({ currentTracker, summaries }) => {
  const [healthData, setHealthData] = useState({});

  useEffect(() => {
    if (currentTracker === 'weight') {
      return setHealthData(summaries['body']);
    } else if (currentTracker === 'detox') {
      return setHealthData(summaries['health']);
    } else if (currentTracker === 'movement') {
      return setHealthData(summaries['exercise']);
    }

    setHealthData(summaries[currentTracker]);
  }, [currentTracker, summaries, healthData]);

  return (
    <Card >
      <DashDataItem
        noIcon
        tracker
        title={`${currentTracker.charAt(0).toUpperCase()}${currentTracker.slice(1)}`}
        icon={`${currentTracker.charAt(0).toUpperCase()}${currentTracker.slice(1)}`}
        value={healthData ? healthData.value : 0}
        status={healthData ? healthData.HStatus : ""}
      />
    </Card>
  );
}

export default DashData;