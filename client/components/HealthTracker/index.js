import { css } from "glamor";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Label,
  Legend
} from "recharts";
import dateformat from "dateformat";
import AnalysisService from "../../utils/analysisService.js";
import Restricted from "../../components/restricted";
import GridContainer from "../NewSkin/Wrappers/GridContainer.js";
import TabWrapper from "../NewSkin/Wrappers/TabWrapper.js";
import TabItem from "../NewSkin/Wrappers/TabItem.js";
import DashData from "./DashData.js";
import Colors from "../NewSkin/Colors.js";
import SummaryService from "../../utils/summaryService";
import Button from "../NewSkin/Components/Button.js";
import { chart_tooltip } from "../NewSkin/Styles.js";
const summary = new SummaryService();
const analyis = new AnalysisService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = this.initialState;

    this._resetButtonLabel = this._resetButtonLabel.bind(this);
  }

  get initialState() {
    return {
      userType: this.props.userType,
      saveEnable: false,
      updateItem: false,
      dataLoaded: false,
      mode: {
        update: false
      },
      buttons: {
        saveBtn: {
          disabled: false,
          label: "Save"
        }
      },
      formMessage: {
        success: "",
        error: ""
      },
      // data: {
      trackerData: {
        weight: {
          name: "Weight",
          display: true,
          weight: { name: "Weight", data: [] },
          bmi: { name: "BMI", data: [] },
          vfr: { name: "VFR", data: [] },
          muscleMass: { name: "Muscle Mass", data: [] },
          fatMass: { name: "Fat Mass", data: [] }
        },
        nutrition: {
          name: "Nutrition",
          display: false,
          weight: { name: "Weight", data: [] },
          height: { name: "Height", data: [] },
          bmi: { name: "BMI", data: [] },
          vfr: { name: "VFR", data: [] },
          muscleMass: { name: "Muscle Mass", data: [] },
          totalBodyFat: { name: "Total Body Fat", data: [] },
          fatMass: { name: "Fat Mass", data: [] },
          msqScore: { name: "MSQ Score", data: [] }
        },
        detox: {
          name: "Detox",
          display: false,
          msqScore: { name: "MSQ Score", data: [] }
        },
        movement: {
          name: "Movement",
          display: false,
          Height: { name: "Height", data: [] },
          bmi: { name: "BMI", data: [] },
          vfr: { name: "VFR", data: [] },
          muscleMass: { name: "Muscle Mass", data: [] },
          totalBodyFat: { name: "Total Body Fat", data: [] },
          fatMass: { name: "Fat Mass", data: [] },
          muscleAndFatDistribution: { name: "Muscle/Far Dist", data: [] },
          msqScore: { name: "MSQ Score", data: [] },
          hoursOfExercise: { name: "Hours of Exercise", data: [] },
          hoursOfSitting: { name: "Hours of Sitting", data: [] }
        },
        sleep: {
          name: "Sleep",
          display: false,
          sleepHours: { name: "Sleep Hours", data: [] },
          sleepQuality: { name: "Sleep Quality", data: [] },
          sleepMedicationUse: { name: "Sleep Medicatin Use", data: [] },
          msqScore: { name: "MSQ Score", data: [] }
        },
        stress: {
          name: "Stress",
          display: false,
          msqScore: { name: "MSQ Score", data: [] }
        }
        // }
      },
      currentTracker: "weight"
    };
  }

  componentDidMount() {
    this._getAnalysis();
    this._getSummary();
  }

  _getSummary = () => {
    const props = this.props;
    const isMD = props.userType !== "Patient";
    const id = isMD ? props.patientInfo.id : props.userid;

    summary
      .get(null, id)
      .then(res => {
        if (res.name === "Error") {
          console.log(res);
        } else if (res.length > 0) {
          this.createSummary(res);
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  createSummary = summaries => {
    const state = {
      body: {
        bmi: "0.00",
        whr: "0.00",
        value: "",
        HStatus: ""
      },
      nutrition: {
        value: "",
        HStatus: ""
      },
      sleep: {
        value: "",
        HStatus: ""
      },
      health: {
        value: "",
        HStatus: ""
      },
      stress: {
        value: "",
        HStatus: ""
      },
      exercise: {
        value: "",
        HStatus: ""
      },
      overallScore: {
        value: 0,
        HStatus: ""
      }
    };

    const body = state.body;
    const nutrition = state.nutrition;
    const sleep = state.sleep;
    const health = state.health;
    const stress = state.stress;
    const exercise = state.exercise;
    const overallScore = state.overallScore;

    let ltstBodyVal;
    let ltstExerciseVal;
    let ltstStressVal;
    let ltstNutritionVal;
    let ltstSleepVal;
    let ltstHealthVal;

    for (const summary of summaries) {
      const score = summary.value;
      let weightedScore = score * 0.167;

      if (summary.name === "weightScore") {
        ltstBodyVal = weightedScore;
        body.value = score;
        // body.HStatus = _getStatus(score);
      } else if (summary.name === "bmi") {
        weightedScore = 0;
        body.bmi = summary.value;
      } else if (summary.name === "whr") {
        weightedScore = 0;
        body.whr = summary.value;
      } else if (summary.name === "nutrition") {
        ltstNutritionVal = weightedScore;
        nutrition.value = score;
        // nutrition.HStatus = _getStatus(score);
      } else if (summary.name === "sleep") {
        ltstSleepVal = weightedScore;
        sleep.value = score;
        // sleep.HStatus = _getStatus(score);
      } else if (summary.name === "detox") {
        ltstHealthVal = weightedScore;
        health.value = score;
        // health.HStatus = _getStatus(score);
      } else if (summary.name === "stress") {
        ltstStressVal = weightedScore;
        stress.value = score;
        // stress.HStatus = _getStatus(score);
      } else if (summary.name === "exercise") {
        ltstExerciseVal = weightedScore;
        exercise.value = score;
        // exercise.HStatus = _getStatus(score);
      }
    }

    const hs =
      ltstBodyVal +
      ltstExerciseVal +
      ltstStressVal +
      ltstNutritionVal +
      ltstSleepVal +
      ltstHealthVal;
    overallScore.value = hs * 2;

    const summary = {
      body,
      nutrition,
      sleep,
      health,
      stress,
      exercise,
      overallScore
    };

    this.setState({ summaries: summary });
  };

  _setMode(bol) {
    let mode = this.state.mode;
    mode.update = bol;
    this.setState({
      mode
    });
  }

  _getAnalysis() {
    let trackerData = this.state.trackerData;
    analyis
      .get(this.props.userid)
      .then(res => {
        if (res.health_tracker != null) {
          trackerData = res.health_tracker;
          this.setState({
            trackerData
          }, () => {
            this._setMode(true);
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
    this.setState({
      dataLoaded: true
    });
  }

  _toggleCat(name) {
    let trackerData = this.state.trackerData;

    // HIDE ALL CHART
    Object.keys(trackerData).forEach((item, i) => {
      trackerData[item].display = false;
      //trackerData[item].update = false
    });

    // DISPLAY ONLY SELECTED
    trackerData[name].display = true;
    this.setState({
      saveEnable: false,
      updateItem: false,
      currentTracker: name
    }, () => {
      setTimeout(() => {
        this._getAnalysis()
      }, 1000)
    });
  } // end of _toggleCat

  _enAbleUpdate(name) {
    this.setState({
      updateItem: true,
      saveEnable: true
    });
  } // end of _enAbleUpdate

  _saveData(item, final) {
    let mode = this.state.mode;
    let buttons = this.state.buttons;
    let trackerData = this.state.trackerData;

    buttons.saveBtn.label = "Saving.....";
    if (final) {
      buttons.saveBtn.label = "Saving as final.....";
      trackerData[item].final = true;
    }

    let data = {
      user_id: this.props.userid,
      health_tracker: JSON.stringify(trackerData),
      health_tracker_status: final ? 1 : 0
    };

    if (this.state.mode.update) {
      buttons.saveBtn.label = "Updating.....";
      data.health_tracker_date_modified = new Date();
      analyis
        .update(data)
        .then(res => {
          if (res.message) {
            this._setMessage(res.message, false);
          } else if (res.length > 0) {
            if (final) {
              this._setMessage("Successfully Finalized", true);
            } else {
              this._setMessage("Successfully Updated", true);
            }
          }
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      buttons.saveBtn.label = "Saving.....";
      data.health_tracker_date_created = new Date();
      analyis
        .save(data)
        .then(res => {
          if (res.message) {
            this._setMessage(res.message, false);
            this._setMode(true);
          } else {
            if (final) {
              this._setMessage("Successfully Finalized", true);
            } else {
              this._setMessage("Successfully Saved as Draft", true);
            }
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    mode.update = true; // set to true to avoid duplicate entry
    this.setState({
      buttons,
      mode
    });

    var that = this;
    setTimeout(function () {
      that._resetButtonLabel();
    }, 1000);
  }

  _resetButtonLabel() {
    let buttons = this.state.buttons;
    buttons.saveBtn.label = "Save";
    this.setState({
      buttons
    });
  }

  _setMessage(mess, good) {
    let formMessage = this.state.formMessage;
    if (good) {
      formMessage.success = mess;
    } else {
      formMessage.error = mess;
    }

    this.setState({
      formMessage
    });
  }

  _chartList() {
    let trackerData = this.state.trackerData;
    const isMD = this.props.userType !== "Patient";
    let list = Object.keys(trackerData).map((item, i) => {
      return (
        <TabItem
          btnType={isMD}
          key={i}
          withIcon
          isActive={this.state.currentTracker === item}
          onClickFn={() => this._toggleCat(item)}
        >
          {trackerData[item].name}
        </TabItem>
      );
    });

    return (
      <GridContainer dataID="health-tracker-wrapper" styles={{ width: "100%" }}>
        <TabWrapper
          dataID="health-tracker-tab-wrapper"
          btnType={isMD}
          gap={isMD ? 10 : 0}
          tabCount={6}
          styles={{ marginTop: isMD ? 20 : 0 }}
        >
          {list}
        </TabWrapper>

        {this.state.userType !== "Patient" ? (
          <section>
            <GridContainer
              columnSize={
                //trackerData[this.state.currentTracker].display &&
                !this.state.saveEnable &&
                  !this.props.restricted
                  ? "1fr"
                  : "1fr 1fr"
              }
              gap={20}
              styles={{ margin: "20px auto", maxWidth: 500 }}
            >
              {//trackerData[this.state.currentTracker].display &&
                !this.state.saveEnable &&
                  !this.props.restricted ? (
                    <Button styles={{ margin: "0 auto" }}>
                      <button
                        onClick={() =>
                          this._enAbleUpdate(this.state.currentTracker)
                        }
                      >
                        Details
                  </button>
                    </Button>
                  ) : null}

              {//trackerData[this.state.currentTracker].display &&
                this.state.saveEnable &&
                  !trackerData[this.state.currentTracker].final ? (
                    <Button type="pink">
                      <button
                        onClick={() =>
                          this._saveData(this.state.currentTracker, false)
                        }
                      >
                        {this.state.buttons.saveBtn.label}
                      </button>
                    </Button>
                  ) : null}

              {//trackerData[this.state.currentTracker].display &&
                this.state.saveEnable ? (
                  <Button>
                    <button
                      onClick={() => {
                        this._resetValues(this.state.currentTracker);
                      }
                      }
                    >
                      Close Details
                  </button>
                  </Button>
                ) : null}

              {/**trackerData[item].display && this.state.saveEnable && !trackerData[item].final ? <span className={btn} onClick={()=>this._saveData(item, true)}>Finalize</span>  : null **/}
            </GridContainer>
          </section>
        ) : null}

        <GridContainer
          columnSize="253px 1fr"
          gap={30}
          styles={{ marginTop: 30 }}
        >
          <section>
            <DashData
              summaries={this.state.summaries !== undefined && this.state.summaries}
              currentTracker={this.state.currentTracker}
            />
          </section>

          {this.state.currentTracker &&
            this._chartItem(
              trackerData[this.state.currentTracker],
              this.state.currentTracker
            )}
        </GridContainer>
      </GridContainer>
    );
  }

  _inputChange(e) {
    let nm = e.target.name.split("_");
    let trackerData = this.state.trackerData;
    if (nm[0] === "dte") {
      if (nm.length === 4) {
        trackerData[nm[2]][nm[3]].data[nm[1]].date = dateformat(e.target.value, "mm/dd/yyyy");
      } else {
        let value = document.getElementById(`${nm[1]}_${nm[2]}`).value || 0;
        trackerData[nm[1]][nm[2]].data.push({
          date: dateformat(e.target.value, "mm/dd/yyyy"),
          value: parseFloat(value)
        });
      }
    } else {
      if (e.target.value) {
        trackerData[nm[1]][nm[2]].data[nm[0]].value = e.target.value;
      } else {
        trackerData[nm[1]][nm[2]].data[nm[0]].value = null;
      }
    }

    this.setState({
      trackerData
    });
  }

  _resetValues(currentTracker) {
    this.setState(this.initialState, () => {
      this._toggleCat(currentTracker)
      this.setState({ saveEnable: false })
    });
  }

  _inputBlur(e) {
    let nm = e.target.name.split("_");
    if (nm.length < 3 && e.target.value != "") {
      let dateval = document.getElementById(`dte_${nm[0]}_${nm[1]}`).value;
      if (!dateval) {
        let now = new Date();
        let cdte = dateformat(now, "mm/dd/yyyy");
        dateval = cdte;
      }
      let trackerData = this.state.trackerData;
      var element = document.getElementsByName(e.target.name);
      let names = element[0].name.split("_");
      if (e.target.value) {
        trackerData[names[0]][names[1]].data.push({
          date: dateformat(dateval, "mm/dd/yyyy"),
          value: parseFloat(e.target.value)
        });
      }
      this.setState(
        {
          trackerData
        },
        function () {
          element[0].value = null;
        }
      );
    }
  }

  _inputs(data, item, nme, final) {
    let doh = nme;
    let inme = item;
    let now = new Date();
    // let cdte = dateformat(now, "mm/dd/yyyy");
    let cdte = dateformat(now, "yyyy-mm-dd");
    //let trackerData = this.state.data.trackerData
    let inputs = data.data.map((item, i) => {
      return (
        <div className={inputGroup} key={i}>
          <input
            id={`dte_${i}_${doh}_${inme}`}
            disabled={final ? true : false}
            className={dteinpt}
            value={dateformat(item.date, 'yyyy-mm-dd')}
            onChange={this._inputChange.bind(this)}
            // onBlur={this._inputBlur.bind(this)}
            name={`dte_${i}_${doh}_${inme}`}
            type="date"
          />
          <input
            id={`${i}_${doh}_${inme}`}
            disabled={final ? true : false}
            className={inpt}
            value={item.value}
            onChange={this._inputChange.bind(this)}
            onBlur={this._inputBlur.bind(this)}
            name={`${i}_${doh}_${inme}`}
            type="number"
            min={0}
          />
        </div>
      );
    });
    return (
      <div className="wrap">
        {inputs}
        {!final ? (
          <div className={inputGroup}>
            <input
              className={dteinpt}
              id={`dte_${doh}_${inme}`}
              name={`dte_${doh}_${inme}`}
              type="date"
              value={cdte}
              onChange={this._inputChange.bind(this)}
            // onBlur={this._inputBlur.bind(this)}
            />
            <input
              className={inpt}
              id={`${doh}_${inme}`}
              name={`${doh}_${inme}`}
              type="number"
              min={0}
              onBlur={this._inputBlur.bind(this)}
            />
          </div>
        ) : null}
      </div>
    );
  }

  _chartItem(obj, nme) {

    const customTooltip = data => {
      if (data && data.active) {
        return (
          <div className={chart_tooltip}>
            <p>{data.label}</p>
            <label>{data.ctype}: {data.payload[0].value}</label>
          </div>
        );
      }
      return null;
    }
    var charData = obj;
    let final = charData.final;
    var colors = [
      "purple",
      "violet",
      "orange",
      "green",
      "violet",
      "red",
      "orange",
      "green",
      "purple",
      "red",
      "orange",
      "black"
    ];

    var list = Object.keys(charData).map((item, i) => {
      var cdata = charData[item];

      var clrNumber = Math.floor(Math.random() * 3 + 0);
      var w = 10;
      if (cdata.data) {
        w = cdata.data.length * 50;
      }

      if (charData[item].name) {
        if (cdata.data && cdata.data.length > 0) {
          var values = cdata.data.map(function (a) {
            return a.value;
          });
        }

        return (
          <GridContainer
            key={i}
            columnSize="150px 3fr"
            classes={[dataItemWrapper]}
          >
            <div className={chartName}>
              {charData[item].name === "Weight"
                ? charData[item].name + " (lbs)"
                : charData[item].name === "Height"
                  ? charData[item].name + " (in)"
                  : charData[item].name}
            </div>

            <div>
              <LineChart
                width={w}
                height={50}
                data={cdata.data}
                margin={{ top: 5, right: 30, left: 20, bottom: -20 }}
              >
                <XAxis dataKey="date" display="none" />

                <Tooltip content={customTooltip} ctype={cdata.name} wrapperStyle={{ zIndex: 1 }} />
                <Line
                  type="monotone"
                  dataKey="value"
                  stroke={colors[i]}
                  strokeWidth={2}
                  dot={{ fill: colors[i], r: 2 }}
                />
              </LineChart>

              {this.state.updateItem ? (
                <div className={inputsDiv}>
                  {this._inputs(cdata, item, nme, final)}
                </div>
              ) : null}
            </div>
          </GridContainer>
        );
      }
    });

    return <section className={listWrapper}>{list}</section>;
  }

  render() {
    if (
      this.props.telemedicine_enabled === "Approved" ||
      (this.props.patientInfo &&
        this.props.patientInfo.telemedicine_enabled === "Approved")
    ) {
      return (
        <div className={wrapper}>
          {this.state.dataLoaded ? this._chartList() : null}
        </div>
      );
    }
    let restrict_msg = `This feature is not included in your current subscription. Please contact the officer in-charge of your account if you wish to subscribe`;
    return <Restricted message={restrict_msg} />;
  }
}

const dataItemWrapper = css({
  borderLeft: "1px solid #eee",
  borderRight: "1px solid #eee",
  "&:first-child": {
    borderTop: "1px solid #eee"
  },
  "&:last-child": {
    borderBottom: "1px solid #eee"
  }
});

const listWrapper = css({
  alignContent: "start",
  "& > section:nth-child(2n+1)": {
    background: "#f8f8f8"
  }
});

let inpt = css({
  flex: 1,
  width: 90,
  margin: "0 3px 0 3px",
  textAlign: 'center'
});
let dteinpt = css({
  flex: 1,
  width: 90,
  fontSize: "small",
  margin: "0 3px 0 3px",
  textAlign: 'center'
});

let inputsDiv = css({
  display: "flex",
  flex: "1 0 94%",
  margin: "0 0 0 20px",
  justifyContent: "flex-start"
  //border: '1px solid'
});

let canHideDiv = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "center",
  padding: "10px",
  margin: "0 0 0 0",
  "@media print": {
    background: "#fff"
  }
  //display: 'none'
});

let wrapper = css({
  width: "100%",
  // display: "flex",
  // flex: "1 0 100%",
  // flexWrap: "wrap",
  // justifyContent: "center",
  // // padding: "10px",
  // margin: "0 0 0 0",
  "@media print": {
    background: "#fff"
  }
});

let chartContainer = css({
  //border : '1px solid blue',
  display: "flex",
  //flex: '1',
  width: "100%",
  flexWrap: "wrap",
  justifyContent: "flex-start",
  padding: 0,
  margin: 3,
  "@media screen and (orientation:portrait)": {
    width: "100%"
  }
});

let IndChartDiv = css({
  border: "1px solid #eee"
  // display: "flex",
  // flex: "1 0 80%",
  // flexWrap: "wrap",
  // justifyContent: "flex-start",
  // padding: "0 0 0 10px",
  // margin: 0
});

let chartDiv = css({
  display: "flex",
  width: "70%",
  justifyContent: "flex-start",
  alignItems: "center"
});

let chartName = css({
  borderRight: `1px solid ${Colors.pink}`,
  textAlign: "left !important",
  padding: 10
  // width: "10%",
  // "@media screen and (orientation:portrait)": {
  //   justifyContent: "center",
  //   width: "100%"
  // }
});

let chart = css({
  display: "flex",
  justifyContent: "flex-stat",
  flexWrap: "wrap",
  flex: "1",
  width: "80%"
});

let titleDiv = css({
  display: "flex",
  justifyContent: "flex-start",
  width: "100%",
  "@media screen and (orientation:portrait)": {
    //justifyContent: 'center',
    background: "#36c4f1"
  }
});

let nameStyle = css({
  //border: '1px solid',
  display: "flex",
  width: "150px",
  alignItems: "center",
  justifyContent: "center",
  background: "#36c4f1",
  color: "#fff",
  padding: "3px 30px",
  cursor: "pointer"
});

let btn = css({
  padding: "2px 10px",
  margin: "0 0 0 30px",
  border: "1px dotted gray",
  borderRadius: 5,
  cursor: "pointer"
});

let inputGroup = css({
  display: "flex",
  flexWrap: "wrap",
  width: 150,
  margin: "3px"
});

let testDiv = css({
  width: "100%",
  height: "auto",
  opacity: 1,
  //transition: 'opacity .7s ease-out',
  transition: "all .5s ease-in"
  //  transitionDelay: '.3s'
});

let testDivHiden = css({
  opacity: 0,
  width: "0%",
  border: "2px solid red"
});

let warning = css({
  color: "orange",
  border: "1px dotted orange",
  padding: "3",
  borderRadius: 5
});
