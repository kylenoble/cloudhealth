import React from "react";
import Link from "next/link";
import dateFormat from "dateformat"; // not localize
import Dropzone from "react-dropzone";
import request from "axios";
import { css } from "glamor";
import ReactTooltip from "react-tooltip";
import currentDomain from "../../utils/domain.js";
import UserService from "../../utils/userService.js";
import Form from "../Form";
import CompanyService from "../../utils/companyService.js";
import MDProfileForm from "../Form/mdProfileForm";
import GridContainer from "../NewSkin/Wrappers/GridContainer.js";
import Colors from "../NewSkin/Colors.js";
import Label from "../NewSkin/Components/Label.js";
import { changeButton } from "../NewSkin/Styles.js";
import { tooltipWrapper } from "../Commons/index.js";

const cmpy = new CompanyService();
const user = new UserService();
//const CLOUDINARY_UPLOAD_PRESET = "bmzjbxoq";
const CLOUDINARY_UPLOAD_URL = `${currentDomain}/api/upload/notmultiple`;
const avatarUrl = `/static/avatar/`;

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.domain = `${currentDomain}/images/`;
    this.state = {
      info: {},
      avatar: "",
      uploadError: "",
      companies: [],
      departments: [],
      branches: [],
      fields: [],
      fullName: {
        id: "",
        name: "fullName",
        // question: "Enter Name",
        question: "Name",
        type: "name",
        validation: "name",
        disabled: false,
        options: [],
        error: {
          active: false,
          message: "Please enter a valid full name"
        },
        value: ""
      },
      email: {
        id: "",
        name: "email",
        // question: "Enter valid Email",
        question: "Email",
        type: "email",
        disabled: this.props.user.type === "Patient",
        validation: "email",
        options: [],
        error: {
          active: false,
          message: "Please enter a valid email address"
        },
        value: ""
      },
      password: {
        id: "",
        name: "password",
        question: "Create Password",
        type: "password",
        validation: "password",
        options: [],
        error: {
          active: false,
          message:
            "Passwords must have at least 1 number, 1 Uppercase letter, 1 Lowercase letter, and a minimum of 8 characters"
        },
        value: ""
      },
      company: {
        id: "",
        name: "company",
        label: "Company name",
        // question: "Enter Company Code",
        question: "Company",
        type: "company",
        disabled: true,
        hidden: true,
        validation: "company",
        options: [],
        error: {
          active: false,
          message: "Please enter a valid Company code!"
        },
        value: ""
      },
      department: {
        id: "",
        name: "department",
        // question: "Select your Department",
        question: "Department",
        type: "text",
        disabled: false,
        validation: "text",
        options: [],
        error: {
          active: false,
          message: "Please select a department"
        },
        value: ""
      },
      branch: {
        id: "",
        name: "branch",
        // question: "Select branch",
        question: "Branch",
        type: "text",
        disabled: false,
        validation: "text",
        options: [],
        error: {
          active: false,
          message: "Please select a branch"
        },
        value: ""
      },
      employeeId: {
        id: "",
        name: "employeeId",
        // question: "Enter Employee Id",
        question: "Employee ID",
        type: "name",
        disabled: true,
        validation: "",
        options: [],
        error: {
          active: false,
          message: "Please enter a valid employee ID"
        },
        value: ""
      },
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      }
    };

    this._goToNext = this._goToNext.bind(this);
    this._getAnswers = this._getAnswers.bind(this);
    this._updateFields = this._updateFields.bind(this);
  }

  componentDidMount() {
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      // eslint-disable-next-line no-undef
      flexibility(document.body);
    }

    user.getLatest();

    cmpy
      .get("branches")
      .then(res => {
        if (res.name === "Error") {
          console.log(res);
        } else if (res.length > 0) {
          this.setState({
            branches: res
          });
        }
      })
      .catch(e => {
        console.log(e);
      }); // you would show/hide error messages with component state here

    cmpy
      .get("departments")
      .then(res => {
        if (res.name === "Error") {
          console.log(res);
        } else if (res.length > 0) {
          this.setState({
            departments: res
          });
        }
      })
      .catch(e => {
        console.log(e);
      }); // you would show/hide error messages with component state here

    cmpy
      .list()
      .then(res => {
        if (res.name === "Error") {
          console.log(res);
        } else if (res.length > 0) {
          this.setState({
            companies: res
          });

          this._getAnswers();
          this._updateFields();
        }
      })
      .catch(e => {
        console.log(e);
      }); // you would show/hide error messages with component state here
  }

  onImageDrop(files) {
    if (files.length > 0) {
      this.setState({
        uploadError: ""
      });
      this.handleImageUpload(files[0]);
    } else {
      this.setState({
        uploadError:
          "Only image with extention of .jpg or .png are allowed and must not exceed to 100 KB"
      });
    }
  }

  handleImageUpload(file) {
    const date = new Date();
    const prefix = dateFormat(date, "yymmddhmmss");
    const info = this.state.info;

    const fd = new FormData();
    let upload = "";

    if (info.avatar) {
      fd.append("UserId", this.state.userId);
      fd.append("prefix", prefix);
      fd.append("oldAvatar", info.avatar);
      fd.append("file", file);

      upload = request.post(CLOUDINARY_UPLOAD_URL, fd, {
        onUploadProgress: ProgressEvent => {
          console.log(
            "Upload Progress " +
            Math.round((ProgressEvent.loaded / ProgressEvent.total) * 100)
          );
        }
      });
    } else {
      upload = request;
      fd.append("UserId", this.state.userId);
      fd.append("prefix", prefix);
      fd.append("file", file);

      upload = request.post(CLOUDINARY_UPLOAD_URL, fd, {
        onUploadProgress: ProgressEvent => {
          console.log(
            "Upload Progress " +
            Math.round((ProgressEvent.loaded / ProgressEvent.total) * 100)
          );
        }
      });
    }

    upload.then(response => {
      if (response.status === 200) {
        user
          .getLatest()
          .then(res => {
            info.avatar = res.avatar;
            this.setState({
              info
            });
          })
          .catch(e => {
            console.log(e);
          });
      } else {
        console.log("Error Uploading");
      }
    });
  }

  supportsFlexBox() {
    // eslint-disable-next-line no-undef
    const test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  _getAnswers() {
    const userProfile = user.getProfile();

    this.setState({
      userId: userProfile.id,
      info: userProfile,
      avatar: userProfile.avatar
    });

    if (userProfile.name) {
      // since company field is not editable and it is a required fields
      //the value must be the company code
      let code = "";
      for (let i = 0; i < this.state.companies.length; i++) {
        if (this.state.companies[i].company_name === userProfile.company) {
          code = this.state.companies[i].code;
          break;
        }
      }
      const name = this.state.fullName;
      const email = this.state.email;
      const company = this.state.company;
      const employeeId = this.state.employeeId;
      const department = this.state.department;
      const branch = this.state.branch;

      name.value = userProfile.name;
      email.value = userProfile.email;
      company.value = code;
      company.label = userProfile.company;
      employeeId.value = userProfile.employee_id;
      department.value = userProfile.department;
      branch.value = userProfile.branch;

      this.setState({
        fullName: name,
        email,
        company,
        employeeId,
        department,
        branch
      });
    }
  }

  _handleChange(value, input, validated) {
    const inputName = input;
    const newInput = this.state[inputName];
    newInput.value = value;
    newInput.error.active = validated;
    const depts = [];
    const brnchs = [];
    const dept = this.state.department;
    const brnch = this.state.branch;
    let idholder = "";
    if (input === "company" && !validated) {
      for (let i = 0; i < this.state.companies.length; i++) {
        if (this.state.companies[i].code === value) {
          depts.push({
            label: this.state.companies[i].dept_name,
            value: this.state.companies[i].dept_name
          });
          idholder = this.state.companies[i].id;
        }
      }

      for (let x = 0; x < this.state.branches.length; x++) {
        if (this.state.branches[x].cmpy_id === idholder) {
          brnchs.push({
            label: this.state.branches[x].br_name,
            value: this.state.branches[x].br_name
          });
        }
      }

      if (depts.length > 1 && !validated) {
        dept.options = depts;
      } else {
        dept.options = [];
        dept.value = "";
      }

      if (brnchs.length > 1 && !validated) {
        brnch.options = brnchs;
      } else {
        brnch.options = [];
        brnch.value = "";
      }
    } else if (this.state.company.error.active) {
      dept.options = [];
    }

    this.setState({
      dept: depts,
      brnch: brnchs,
      inputName: newInput
    });

    this._updateFields();
  }

  _updateFields() {
    let currentFields = [];
    if (this.props.location === "profile") {
      currentFields = [
        this.state.fullName,
        this.state.email,
        this.state.company,
        this.state.employeeId
      ];
    } else {
      currentFields = [
        this.state.fullName,
        this.state.email,
        this.state.password,
        this.state.company,
        this.state.employeeId
      ];
    }

    //  currentFields = [this.state.fullName, this.state.email, this.state.password, this.state.company, this.state.employeeId]
    const fields = [];
    for (let i = 0; i <= currentFields.length - 1; i++) {
      if (
        currentFields[i].name === "employeeId" &&
        this.state.branch.options.length > 1
      ) {
        const br = this.state.branch;
        br.type = "text";
        br.disabled = false;
        fields.push(this.state.branch);
      } else if (currentFields[i].name === "employeeId") {
        if (
          (this.state.branch.value && !this.state.branch.disabled) ||
          this.state.branch.options.length > 1
        ) {
          const br = this.state.branch;
          // br.type = "input";
          // br.disabled = true;
          br.type = "text";
          br.disabled = false;

          fields.push(this.state.branch);
        }
      }

      if (
        currentFields[i].name === "employeeId" &&
        this.state.department.options.length > 1
      ) {
        const dpt = this.state.department;
        dpt.type = "text";
        dpt.disabled = false;
        fields.push(this.state.department);
      } else if (currentFields[i].name === "employeeId") {
        if (
          (this.state.department.value && !this.state.department.disabled) ||
          this.state.department.options.length > 1
        ) {
          const dpt = this.state.department;
          // dpt.type = "input";
          // dpt.disabled = true;
          dpt.type = "text";
          dpt.disabled = false;
          fields.push(this.state.department);
        }
      }
      const input = currentFields[i];
      fields.push(input);
    }

    this.setState({
      fields
    });

    this._addOptions('branch', 'branches');
    this._addOptions('department', 'departments');
  }

  _addOptions = (state = {}, optionState = {}) => {
    const opt = this.state[state];
    const opts = this.state[optionState];
    const company_id = this.props.user.company_id;
    const options = []

    opts.forEach(oc => {
      if (oc.cmpy_id === company_id) {
        if (optionState === 'branches') {
          options.push({
            label: oc.location,
            value: oc.br_name,
          })
        } else {
          options.push({
            label: oc.dept_name,
            value: oc.dept_name,
          })
        }
      }
    })

    opt.options = options;
    this.setState({ state: opt })
  }

  _goToNext() {
    this.setState({
      formError: {
        active: false,
        message: ""
      }
    });

    let company_name = "";
    for (let i = 0; i < this.state.companies.length; i++) {
      if (this.state.companies[i].code === this.state.company.value) {
        company_name = this.state.companies[i].company_name;
      }
    }

    if (this.props.location === "profile") {
      let inputs = [];
      if (this.state.password.value !== "") {
        inputs = [
          {
            email: this.state.email.value,
            password: this.state.password.value,
            company: company_name, // THIS PART WAS MODIFIED TO GET THE VALUE FROM THE OPTIONS OF THE COMPANY SELECTION
            name: this.state.fullName.value,
            employee_id: this.state.employeeId.value,
            department:
              this.state.department.value === "Select"
                ? ""
                : this.state.department.value,
            branch: this.state.branch.value
          }
        ];
      } else {
        inputs = [
          {
            email: this.state.email.value,
            company: company_name, // THIS PART WAS MODIFIED TO GET THE VALUE FROM THE OPTIONS OF THE COMPANY SELECTION
            name: this.state.fullName.value,
            employee_id: this.state.employeeId.value,
            department:
              this.state.department.value === "Select"
                ? ""
                : this.state.department.value,
            branch: this.state.branch.value
          }
        ];
      }
      user
        .update(inputs)
        .then(res => {
          if (res) {
            console.log("update user profile");
            this.setState({
              formSuccess: {
                active: true,
                message: "Your Profile Was Updated"
              }
            });
            //  return this.props.onClick('email')
          } else {
            this.setState({
              formError: {
                active: true,
                message: "There was an error updating your profile"
              }
            });
          }
        })
        .catch(e => {
          if (e.error === "This email is already in use") {
            const newInput = this.state.email;
            newInput.error.active = true;
            newInput.error.message = e.error;
            this.setState({
              inputName: newInput
            });
          } else {
            this.setState({
              formError: {
                active: true,
                message: e.error
              }
            });
          }
        }); // you would show/hide error messages with component state here
    } else {
      // THIS PART WAS MODIFIED TO GET THE VALUE FROM THE OPTIONS OF THE COMPANY SELECTION
      user
        .create(
          this.state.email.value,
          this.state.password.value,
          company_name,
          this.state.fullName.value,
          this.state.employeeId.value,
          this.state.department.value,
          this.state.branch.value
        )
        .then(res => {
          if (res) {
            console.log("should move on");
            return this.props.onClick("dashboard");
          }
          this.setState({
            formError: {
              active: true,
              message: "There was an error creating your account"
            }
          });
        })
        .catch(e => {
          if (e.error === "This email is already in use") {
            const newInput = this.state.email;
            newInput.error.active = true;
            newInput.error.message = e.error;
            this.setState({
              inputName: newInput
            });
          } else {
            this.setState({
              formError: {
                active: true,
                message: e.error
              }
            });
          }
        }); // you would show/hide error messages with component state here
    }
  }

  _changeAvatar = () => {
    const avatarInput = document.querySelector('#avatar-input');
    if (avatarInput) {
      avatarInput.click();
    }
  }

  render() {
    let txtColor;
    if (this.state.info.gender === "Male") {
      txtColor = "#36c4f1";
    } else {
      txtColor = "#eec5e2";
    }
    // dynamic styling

    const avatarNoImageStyle = css({
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      background: `${txtColor}`,
      marginRight: "auto",
      marginLeft: "auto",
      fontSize: "60",
      textTransform: "uppercase",
      color: "#fff",
      borderRadius: "25%",
      height: "100px",
      width: "100px",
      align: "center",
      verticalAlign: "center",
      border: "2px solid gray",
      //  color: 'rgb(54, 69, 99)',
      "@media(max-width: 767px)": {
        height: "75px",
        width: "75px"
      }
    });
    return (
      <>
        {
          this.props.user.type === 'Patient' &&
          <span style={{ marginBottom: 50, display: 'block', color: Colors.blueDarkAccent }}>
            Your registration details will be used in your health profile and
            in setting and reviewing your appointments.
            </span>
        }

        <span className={errorStyle}>{this.state.uploadError}</span>
        {this.props.avatar !== "off" ? (
          <GridContainer fullWidth columnSize={'200px 1fr 120px'} styles={{ borderBottom: '1px solid #ccc3', paddingBottom: 20 }}>
            <Label text={'photo'} />

            <section style={{ width: 100 }}>
              <Dropzone
                data-tip="Click or Drag Image here to change."
                onDrop={this.onImageDrop.bind(this)}
                multiple={false}
                maxSize={100000} // limit to 100 KB
                accept="image/jpeg, image/png"
              >
                {({ getRootProps, getInputProps }) => (
                  <div
                    // className="dropStyle"
                    {...getRootProps()}>
                    <input id="avatar-input" {...getInputProps()} />

                    {
                      this.state.info.avatar ? (
                        <figure id="avatar-wrapper" className={avatarWrapper}>
                          <img
                            alt={"Avatar"}
                            className={avatar}
                            src={
                              this.state.info.avatar
                                ? `/static/avatar/${this.state.info.avatar}`
                                : "/static/icons/user.svg"
                            }
                          />
                        </figure>
                      ) : (
                          // <div className={avatarNoImageStyle}>
                          <div id="no-avatar-wrapper" className={avatarWrapper}>
                            <span>{this.state.info.name ? this.state.info.name[0] : null}</span>
                          </div>
                          // </div>
                        )}
                  </div>
                )}
              </Dropzone>
            </section>

            <button onClick={this._changeAvatar} className={changeButton}>Change</button>
          </GridContainer>
        ) : null}

        {this.props.user.type === "Doctor" ? (
          <MDProfileForm user={this.props.user} />
        ) : (
            <Form
              account
              companies={this.state.companies}
              departments={this.state.departments}
              submitForm={this._goToNext}
              buttonText={
                this.props.location === "profile"
                  ? "Update My Profile"
                  : "Continue My Profile"
              }
              handleChange={this._handleChange.bind(this)}
              inputs={this.state.fields}
              secondaryOption={
                this.props.location === "profile" ? (
                  ""
                ) : (
                    <Link href="/login">
                      <a style={styles.register}>I Already Have an Account</a>
                    </Link>
                  )
              }
              formError={this.state.formError}
              formSuccess={this.state.formSuccess}
              location={this.props.location}
              btnLocked={false}
              userInfo={this.state.info}
            />

          )}


      </>
    );
  }
}

const avatarWrapper = css({
  margin: 0,
  width: 100,
  height: 100,
  borderRadius: '100%',
  overflow: 'hidden',
  position: 'relative',
  background: '#eee',
  display: 'grid',
  placeContent: 'center',
  '> span': {
    fontSize: 50,
    color: '#fff'
  }
});

const avatar = css({
  width: '100%',
  position: 'absolute',
  top: '50%',
  left: 0,
  transform: 'translateY(-50%)'
})

let errorStyle = css({
  color: "red"
});

let avatarStyle = css({
  marginRight: "auto",
  marginLeft: "auto",
  borderRadius: "25%",
  maxHeight: "100px",
  maxWidth: "100px",
  Width: "100px",
  alignSelf: "center",
  verticalAlign: "center",
  border: "2px solid #36c4f1",
  ":hover": {
    backgroundColor: "#ED81B9",
    border: "2px solid #ED81B9",
    color: "white"
  },
  //  color: 'rgb(54, 69, 99)',
  "@media(max-width: 767px)": {
    height: "75px",
    width: "75px"
  }
});

const styles = {
  container: {
    fontFamily: '"Roboto",sans-serif',
    display: "-webkit-flex",
    // eslint-disable-next-line no-dupe-keys
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  introInfo: {
    // maxWidth: "360px",
    fontFamily: '"Roboto",sans-serif',
    color: "#364563",
    fontSize: "14px",
    margin: "20px",
    textAlign: "justified"
  },
  register: {
    textDecoration: "none",
    margin: "auto",
    marginBottom: "5px",
    color: "#364563",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif'
  }
};
