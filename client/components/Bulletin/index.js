import { css } from 'glamor'
import Announcements from '../../components/Announcements'
import Events from '../../components/Events'
import NotificationBanner from '../NotificationBanner'
import ContainedWrapper from '../NewSkin/Wrappers/ContainedWrapper';
import { headerAndFooterHeight } from '../NewSkin/Methods';
import GridContainer from '../NewSkin/Wrappers/GridContainer';
import TabWrapper from '../NewSkin/Wrappers/TabWrapper';
import TabItem from '../NewSkin/Wrappers/TabItem';
import Card from '../NewSkin/Wrappers/Card';
import UpdateNotification from '../NewSkin/Components/UpdateNotification';
import { labelClass } from '../NewSkin/Styles';

export default class extends React.Component {

  constructor(props) {
    super(props)
    this.user = this.props.user
  }

  state = {
    read_announcements: false,
    read_events: false,
    headerFooterHeight: 0,
    activeTab: 'Announcements'
  }

  componentDidMount() {
    const headerFooterHeight = headerAndFooterHeight()
    this.setState({ headerFooterHeight })
  }

  _updateAllRead = (bulletin, bool) => {
    this.setState({
      [bulletin]: bool
    })
  }

  _tabHandler = (tab) => {
    this.setState({ activeTab: tab })
  }

  render() {
    return (
      <GridContainer styles={{ padding: this.user.type === 'Patient' ? '40px 0' : 0 }} center contain>
        {
          this.state.read_announcements && <UpdateNotification msg="You've read all your Announcements." />
        }
        {
          this.state.read_events && <UpdateNotification msg="You've read all your Events." />
        }

        <Card styles={{ height: 'fit-content', margin: '0px auto'}} fullWidth>
          <label className={labelClass}>Health Bulletin</label>
          <TabWrapper tabCount={2}>
            <TabItem onClickFn={() => this._tabHandler('Announcements')} isActive={this.state.activeTab === 'Announcements'}>Announcements</TabItem>
            <TabItem onClickFn={() => this._tabHandler('Events')} isActive={this.state.activeTab === 'Events'}>Events</TabItem>
          </TabWrapper>

          <section style={{ marginTop: 20 }}>
            {
              this.state.activeTab === 'Announcements' ?
                <Announcements user={this.user} updateRead={this._updateAllRead} /> :
                <Events user={this.user} updateRead={this._updateAllRead} viewfor='bulletin' />
            }
          </section>
        </Card>
      </GridContainer>
    )

  }

}// end  of class