import React from "react";
import { css } from "glamor";

import HealthInfo from "../HealthInfo";
import IntakeInfo from "../IntakeInfo";
import SymptomsInfo from "../Symptoms";

import SummaryService from "../../utils/summaryService.js";

const summary = new SummaryService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeView: "",
      summary: {}
    };

    this._updateStep = this._updateStep.bind(this);
  }

  componentDidMount() {
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      // eslint-disable-next-line no-undef
      flexibility(document.body);
    }

    this._getSummaries();
  }

  supportsFlexBox() {
    // eslint-disable-next-line no-undef
    const test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  _getSummaries() {
    summary
      .get()
      .then(res => {
        if (res.name === "Error") {
          console.log(res);
        } else if (res.length > 0) {
          this.setState({
            summary: this._createSummary(res)
          });
        }
      })
      .catch(e => {
        console.log(e);
      }); // you would show/hide error messages with component state here
  }

  _createSummary(summaries) {
    const summaryState = {};
    // eslint-disable-next-line no-shadow
    for (const summary of summaries) {
      summaryState[summary.name] = summary;
    }
    console.log(summaryState);
    if (
      !summaryState.downstreamHealth &&
      !summaryState.msq &&
      !summaryState.detox
    ) {
      this.props.onClick();
    }

    return summaryState;
  }

  _renderContent() {
    if (this.state.activeView === "") {
      return (
        <div style={styles.container}>
          <div
            className={
              this.state.summary.downstreamHealth ? selectionActive : selection
            }
            onClick={this._updateCurrentView.bind(this)}
          >
            Health Issues
          </div>
          <div
            className={this.state.summary.detox ? selectionActive : selection}
            onClick={this._updateCurrentView.bind(this)}
          >
            Detoxification
          </div>
          <div
            className={this.state.summary.msq ? selectionActive : selection}
            onClick={this._updateCurrentView.bind(this)}
          >
            Symptoms
          </div>
        </div>
      );
    }
    if (this.state.activeView === "Health Issues") {
      return (
        <div style={styles.container}>
          <HealthInfo onClick={this._updateStep} location="profile" />
        </div>
      );
    } else if (this.state.activeView === "Detoxification") {
      return (
        <div style={styles.container}>
          <IntakeInfo onClick={this._updateStep} location="profile" />
        </div>
      );
    } else if (this.state.activeView === "Symptoms") {
      return (
        <div style={styles.container}>
          <SymptomsInfo onClick={this._updateStep} location="profile" />
        </div>
      );
    }
  }

  _updateCurrentView(e) {
    e.preventDefault();
    this.setState({
      activeView: e.target.innerText
    });
  }

  _updateStep(step) {
    console.log(`${step} updated`);
    this._getSummaries();
    this.setState({
      activeView: ""
    });
  }

  render() {
    return <div style={styles.container}>{this._renderContent()}</div>;
  }
}

let selection = css({
  boxShadow: "0 0px 6px 0 rgba(249,213,232,1)",
  padding: "30px",
  width: "100%",
  margin: "10px",
  cursor: "pointer",
  color: "#F19AC7",
  fontSize: "18px",
  fontFamily: "Roboto-Black, sans-serif",
  ":hover": {
    boxShadow: "0 0 20px 0 rgba(249,213,232,1)"
  }
});

let selectionActive = css({
  boxShadow: "0 0px 6px 0 rgba(10, 10, 10, 0.2)",
  padding: "30px",
  width: "100%",
  margin: "10px",
  cursor: "pointer",
  color: "#364563",
  backgroundColor: "rgba(183, 217, 124, 0.35)",
  fontSize: "18px",
  fontFamily: "Roboto-Black, sans-serif",
  ":hover": {
    boxShadow: "0 0 20px 0 rgba(10, 10, 10, 0.2)"
  }
});

const styles = {
  container: {
    fontFamily: '"Roboto",sans-serif',
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    width: "80%",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "10px",
    textAlign: "center"
  }
};
