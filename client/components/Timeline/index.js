import { css } from "glamor";
import Graph from "./graph";
import GraphDetails from "./details";
import TimelineService from "../../utils/timelineService";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import { Preparing } from "../NewSkin/Components/Loading";

const timelineService = new TimelineService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.userId = this.props.userId; // selected patient idea
    this.userType = this.props.userType; // user type of logged user
    this.state = {
      timelineData: [],
      //prevd:[],
      //currentView: this.props.defaultView,
      currentView: "",
      updateMode: false,
      withChanges: false,
      lockAll: true,
      activeEventId: "",
      showContent: false
    };

    this._getTimelineData = this._getTimelineData.bind(this);
    this._getPrevData = this._getPrevData.bind(this);
    this._checkForChanges = this._checkForChanges.bind(this);
    this._saveChanges = this._saveChanges.bind(this);
  } // constructor

  componentDidMount() {
    this._getTimeline();
    // this._getPrevData()
  }

  _getPrevData() {
    return timelineService
      .get(this.userId)
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _getTimeline() {
    let timelineData = this.state.timelineData;

    timelineService
      .get(this.userId)
      .then(res => {
        timelineData = res;
        this.setState({
          timelineData,
          showContent: true
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  _cancel = () => {
    let timelineData = this.state.timelineData;

    this._getPrevData()
      .then(res => {
        timelineData = res;
        this.setState(
          {
            timelineData,
            withChanges: false,
            lockAll: true
          },
          function() {
            this._setUpdateMode(false);
          }
        );
      })
      .catch(e => {
        console.log(e);
      });
  };

  _setUpdateMode = b => {
    let updateMode = this.state.updateMode;
    updateMode = b;
    this.setState({
      updateMode,
      lockAll: false
    });
  };

  isEquivalent(a, b) {
    // Create arrays of property names
    if (a != undefined && b != undefined) {
      var aProps = Object.getOwnPropertyNames(a);
      var bProps = Object.getOwnPropertyNames(b);

      // If number of properties is different,
      // objects are not equivalent

      //remove percent index, not included in comparison
      // var index = aProps.indexOf('percent');
      //   if (index > -1) {
      //     aProps.splice(index, 1);
      //   }

      if (aProps.length != bProps.length) {
        return false;
      }

      for (var i = 0; i < aProps.length; i++) {
        var propName = aProps[i];
        // If values of same property are not equal,
        // objects are not equivalent
        if (a[propName] != b[propName]) {
          return false;
        } else {
        }
      }
    } else {
      return false;
    }
    // If we made it this far, objects
    // are considered equivalent
    return true;
  }

  _checkForChanges() {
    var aa = this.state.timelineData;
    var bb = this._getPrevData();
    let falsectr = 0;

    aa.forEach((item, i) => {
      var b = bb[i];
      var a = aa[i];
      if (!this.isEquivalent(a, b)) {
        falsectr++;
      }
    });

    let withChanges = this.state.withChanges;

    if (falsectr > 0) {
      withChanges = true;
    } else {
      withChanges = false;
    }

    this.setState({
      withChanges
    });
  }

  _saveChanges = () => {
    let timelineData = this.state.timelineData;
    this.setState({
      showContent: false
    });
    timelineService
      .update(timelineData)
      .then(res => {
        if (res) {
          this._getTimeline();
          this.setState(
            {
              lockAll: true,
              withChanges: false,
              showContent: true
            },
            () => {
              this._setUpdateMode(false);
              this._setActiveView();
            }
          );
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  render() {
    return (
      <GridContainer styles={{ width: "100%" }}>
        {!this.state.showContent ? (
          <Preparing />
        ) : (
          <div>
            {this.state.withChanges && (
              <div className={notificationStyle}>
                <span>
                  Click "Save Changes" to update your health timeline.
                </span>
              </div>
            )}
            {this.state.timelineData.length > 0 && (
              <Graph
                timelineData={this._getTimelineData}
                activeEventId={this.state.activeEventId}
                activeEventHandler={this._activeEvent}
              />
            )}
            <GraphDetails
              timelineDetails={this.state.timelineData}
              timelinePrevData={this._getPrevData}
              updateMode={this.state.updateMode}
              checkForChanges={this._checkForChanges}
              lockAll={this.state.lockAll}
              userId={this.userId}
              userType={this.userType}
              activeEventId={this.state.activeEventId}
              activeEventHandler={this._activeEvent}
              saveEventChanges={this._saveChanges}
              cancelEventChanges={this._cancel}
              withChanges={this.state.withChanges}
              setUpdateMode={this._setUpdateMode}
              //  setActiveView = {this.setActiveView}
            />
          </div>
        )}
      </GridContainer>
    );
  }

  _activeEvent = id => {
    this.setState({ activeEventId: id });
  };

  _viewSwitch(v) {
    this.setState({
      currentView: v
    });
  }

  _getTimelineData() {
    return this.state.timelineData;
  }

  _setActiveView() {
    this.setState({
      currentView: ""
    });
  }

  _renderCurrentView() {
    let currentView = this.state.currentView;
    if (currentView === "") {
      return <Graph timelineData={this._getTimelineData} />;
    } else if (currentView === "description") {
      return (
        <GraphDetails
          timelineDetails={this.state.timelineData}
          timelinePrevData={this._getPrevData}
          updateMode={this.state.updateMode}
          checkForChanges={this._checkForChanges}
          lockAll={this.state.lockAll}
          userId={this.userId}
          userType={this.userType}
          //  setActiveView = {this.setActiveView}
        />
      );
    }
  }
} // class end

let wrapper = css({
  position: "relative",
  // marginTop: '50px',
  display: "flex",
  flex: "1",
  flexWrap: "wrap",
  justifyContent: "center",
  width: "100%"
});

let wrap = css({
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 90%",
  margin: "0 10px",
  //flexWrap: 'wrap',
  justifyContent: "center"
});

const btn = css({
  margin: "0 5px",
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  padding: "0px 15px",
  fontSize: "small",
  background: "#80cde9",
  color: "#fff",
  width: "auto",
  height: "30px",
  cursor: "pointer",
  ":hover": {
    color: "#000"
  }
});

let notificationStyle = css({
  display: "flex",
  flex: "1 0 100%",
  fontWeight: 500,
  padding: 10,
  color: "orange",
  justifyContent: "center",
  alignItems: "center",
  fontSize: 15
});

let instructionStyle = css({
  padding: 10,
  fontStyle: "italic"
});
