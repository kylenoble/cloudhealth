import { css } from 'glamor'
import dateFormat from 'dateformat';
import moment from 'moment-timezone';
import Colors from '../NewSkin/Colors';
import Arrow from '../NewSkin/Icons/Arrow';
import GridContainer from '../NewSkin/Wrappers/GridContainer';
let sp = 0
let fp = 0
export default class extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      timelineData: this.props.timelineData(),
    }
  } // constructor

  componentWillReceiveProps(nextProps) {
    this.setState({
      timelineData: nextProps.timelineData(),
    })
  }


  onHover = (e, item) => {
    const text = e.target.nextSibling;
    text.classList.add(showText);
    this.props.activeEventHandler(item.id)
  }

  onHoverOut = (e, item) => {
    const text = e.target.nextSibling;
    text.classList.remove(showText);
    this.props.activeEventHandler(0)
  }

  _renderTimeline() {
    let totaldays = 0
    let td = this.state.timelineData
    var tdiff = []
    var diffd = []

    if (td.length > 0) {
      let procTimelIneData = td.map((item, i) => {

        if (i > 0 && i < td.length) {
          let d1 = td[i - 1].event_date.split(',')
          let cd1 = `${d1[1]} 01, ${d1[0]}`

          let d2 = td[i].event_date.split(',')
          let cd2 = `${d2[1]} 01, ${d2[0]}`


          var dt1 = new Date(cd1);
          var dt2 = new Date(cd2);

          tdiff[i] = Math.abs(dt2.getTime() - dt1.getTime());
          diffd[i] = Math.ceil(tdiff[i] / (1000 * 3600 * 24));
        } else {
          diffd[i] = 100
        }
        item.dfpe = diffd[i]
        return item
      })

      totaldays = diffd.reduce(function (acc, val) { return acc + val; });
      let step = 0
      return procTimelIneData.map((item, i) => {
        if (!item.del) {
          let d = item.event_date.split(',')
          let convToDate = `${d[1]} 01, ${d[0]}`

          let eventDate = new Date(convToDate)

          if (item.dfpe) {
            step += parseInt(item.dfpe)
          }

          let pos = (step / totaldays) * 80

          const fills = [Colors.detoxColor, Colors.movementColor, Colors.nutritionColor, Colors.sleepColor, Colors.stressColor, Colors.weightColor]
          const generateFill = () => {
            if (i >= fills.length) {
              return fills[i - fills.length];
            } else {
              return fills[i];
            }
          }
          generateFill();

          if (i === 0) {
            return <Arrow key={i}
              type="Start"
              active={true}
              fill={generateFill()}
              index={i}
              date={moment(eventDate).format('YYYY MMMM')}
              event={item.event_name}
            />
          } else if (i === (procTimelIneData.length - 1)) {
            return <Arrow key={i}
              type="End"
              active={true}
              fill={generateFill()}
              index={i}
              date={moment(eventDate).format('YYYY MMMM')}
              event={item.event_name}
            />
          } else {
            return <Arrow key={i}
              type="Middle"
              active={true}
              fill={generateFill()}
              index={i}
              date={moment(eventDate).format('YYYY MMMM')}
              event={item.event_name}
            />
          }
        }
      })//end
    }
  }

  _renderLine() {
    return (
      <svg width='100%' height="150px" enableBackground style={{ background: '#ccc3', padding: 20, borderRadius: 5 }}>
        <line x1={0} y1={80} x2={`100%`} y2={80} style={{ stroke: Colors.pink, strokeWidth: 2 }} markerEnd="url(#arrow)" />
        {this._renderTimeline()}
        Sorry, your browser does not support inline SVG.
        </svg>
    )
  }

  render() {
    return (
      <div className={wrapper}>
        <GridContainer columnSize="repeat(9, 1fr)" gap={'10px 0'} styles={{ alignContent: 'start' }}>
          {this._renderTimeline()}
        </GridContainer>
      </div>
    )
  }
}

const svgText = css({
  opacity: 0,
  transition: '250ms ease',
})

const showText = css({
  opacity: '1 !important'
})

const svgCircle = pos => css({
  transition: '250ms ease',
  transform: `translate(${pos}%, 73px)`,
  cursor: 'pointer',
  ':hover': {
    fill: Colors.skyblue,
    transform: `translate(${pos}%, 73px)`,
  }
})

let lineTextStyle = css({
  padding: 5,
  background: 'blue',
  color: 'green'
})

let wrapper = css({
  // marginTop: '50px',
  minHeight: 300,
  // display: 'flex',
  // flex: '1',
  // flexWrap: 'wrap',
  // justifyContent: 'left',
  // width: '100%',
  // // minHeight:'75vh',
  // overflow: 'visible'
})