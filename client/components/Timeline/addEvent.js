import React from "react";
import { inputClass } from "../NewSkin/Styles";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Button from "../NewSkin/Components/Button";
import _ from "lodash";
import { css } from "glamor";
import Colors from "../NewSkin/Colors";

const AddEvent = props => {
  return (
    <section className={overlay}>
      <section className={addEventWrapper}>
        <article className={header}>
          <p>
            Please enter year and month of each significant event related to
            your health starting from your birth. Elaborate on complications and
            any drugs or health interventions done if possible.
          </p>
        </article>

        <article className={inputWrapper}>
          <input
            id="eName"
            type="text"
            name="eName"
            className={inputClass}
            placeholder="Event (E.g. Birth)"
          />
        </article>

        <article className={inputWrapper}>
          <textarea
            name="eDesc"
            id="eDesc"
            cols="30"
            rows="2"
            className={inputClass}
            placeholder="Description (E.g. Caesarian section with no complications)"
          />
        </article>

        <GridContainer columnSize={"1fr 1fr"} gap={20}>
          <div className={inputWrapper}>
            <label htmlFor="eYear">Year</label>
            {props.year}
          </div>
          <div className={inputWrapper}>
            <label htmlFor="eMonth">Month</label>
            {props.month}
          </div>
        </GridContainer>

        <GridContainer columnSize={"1fr 1fr"} gap={20}>
          <Button styles={{ width: "100%", margin: "20px auto 0" }}>
            <button onClick={props.cancel}>Cancel</button>
          </Button>
          <Button type="pink" styles={{ width: "100%", margin: "20px auto 0" }}>
            <button
              onClick={() => {
                props.addEvent();
                props.cancel();
              }}
            >
              Add Event
            </button>
          </Button>
        </GridContainer>
      </section>
    </section>
  );
};

const inputWrapper = css({
  textAlign: "left",
  marginBottom: 20,
  "> label": {
    display: "block",
    fontWeight: 500,
    color: "#777",
    textAlign: "left"
  },
  "> input, textarea, select": {
    width: "100%",
    maxWidth: 460
  }
});

const header = css({
  marginBottom: 30,
  "> h3": {
    color: Colors.blueDarkAccent,
    fontWeight: 500,
    marginBottom: 10
  },
  "> p": {
    fontSize: 14,
    lineHeight: "20px"
  }
});

const overlay = css({
  position: "absolute",
  top: 0,
  left: 0,
  width: "100%",
  height: "100%",
  background: "rgba(255,255,255,.9)"
});

const addEventWrapper = css({
  position: "fixed",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  background: "white",
  maxWidth: "fit-content",
  borderRadius: 10,
  boxShadow: "0 5px 7px rgba(0,0,0,.2)",
  border: "1px solid white",
  padding: 30,
  margin: "0 auto",
  transition: "250ms ease",
  ":hover": {
    borderColor: Colors.skyblue
  },
  zIndex: 99
});

export default AddEvent;
