import { css } from 'glamor'
import TextareaAutosize from 'react-autosize-textarea';
import _ from 'lodash'
import GridContainer from '../NewSkin/Wrappers/GridContainer';
import Colors from '../NewSkin/Colors';
import Button from '../NewSkin/Components/Button'
import AddEvent from './addEvent';
import { inputClass, scrollClass, mainFont } from '../NewSkin/Styles';

export default class extends React.Component {

  constructor(props) {
    super(props)
    this.userId = this.props.userId
    this.userType = this.props.userType
    this.state = {
      timelineDetails: this.props.timelineDetails,
      lockedInput: [],
      updateMode: this.props.updateMode,
      showAddEvent: false,
      activeActionId: '',
      lockAll: this.props.lockAll
    }

    this._addEvent = this._addEvent.bind(this)

  } // constructor

  componentWillReceiveProps(nextProps) {

    if (nextProps.lockAll) {
      this._lockInput()
    }

    this.setState({
      updateMode: nextProps.updateMode,
      timelineDetails: nextProps.timelineDetails,
    })

  }//componentWillReceiveProps

  componentDidMount() {
    this._lockInput()
  }//componentDidMount

  _lockInput() {
    let lockedInput = this.state.lockedInput
    this.state.timelineDetails.forEach((item, i) => {
      lockedInput[item.id] = true
    })
    this.setState({
      lockedInput
    })

  }//_lockInput

  render() {
    return (
      <div>
        {this._displayDetails()}
      </div>
    )
  } //render

  _onHoverHandler = (id) => {
    this.props.activeEventHandler(id)
    this.setState({ activeActionId: id })
  }

  _displayDetails() {
    let description = this.state.timelineDetails
    let xmonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

    let numberOfYears = 100
    let d = new Date()
    let currYear = d.getFullYear()
    let years = []

    let xyears = () => {
      for (var x = 0; x < numberOfYears; x++) {
        years.push(currYear - x)
      }
      return years
    }

    let yearOptions = xyears().map((item, i) => {
      return <option key={i} value={item}>{item}</option>
    })

    let monthOptions = xmonths.map((item, i) => {
      return <option key={i} value={item}>{item}</option>
    })

    let ydd = () => {
      return (
        <select className={inputClass} id='eYear'>
          <option value='' disabled>Year</option>
          {yearOptions}
        </select>
      )
    }

    let mdd = (
      <select className={inputClass} id='eMonth'>
        <option value='' disabled>Month</option>
        {monthOptions}
      </select>
    )

    let headers = (
      <GridContainer
        columnSize={'2.5fr 3fr 4fr 1fr'}
        styles={{ width: '100%', boxShadow: '0 10px 10px -6px rgba(0,0,0,.1)', paddingBottom: 10, color: '#777', fontWeight: 500, marginTop: 40 }}
      >
        <div>Date</div>
        <div>Event</div>
        <div style={{textAlign: 'left'}}>Description</div>
      </GridContainer>
    )

    let lockedInput = this.state.lockedInput
    let items = <p style={{ width: '100%', textAlign: 'center', padding: 10, color: Colors.disabled, fontStyle: 'italic' }}>No data yet</p>

    if (description.length > 0) {
      items = description.map((item, x) => {
        if (!item.del) {
          let i = item.id
          let arrDate = description[x].event_date.split(",")

          return (
            <GridContainer key={x}
              // onHoverEvent={() => this._onHoverHandler(item.id)}
              // onLeaveEvent={() => this._onHoverHandler(0)}
              dataID={item.id}
              columnSize={'2.5fr 3fr 4fr 1fr'}
              styles={{ width: '100%', borderBottom: `1px solid ${Colors.pink}`, padding: 5, background: this.props.activeEventId === item.id && '#ccc3' }}
              onHoverStyles={{ background: '#ccc3' }}
            >
              <GridContainer columnSize={'1fr 1.5fr'} gap={10}>
                {
                  lockedInput[i] || this.state.lockAll ?
                    <React.Fragment>
                      <span className={dateStyle}>{arrDate[0]}</span>
                      <span className={dateStyleLeft}>{arrDate[1]}</span>
                    </React.Fragment>
                    :
                    <React.Fragment>
                      <select
                        disabled={this.userType == 'Patient' ? lockedInput[i] : true}
                        className={!lockedInput[i] && !this.state.lockAll && this.userType == 'Patient' ? inputClass : lockedInputStyle}
                        id={'eYear' + i}
                        value={arrDate[0]}
                        onChange={() => this._handleInputChange('eYear', 'event_date', i)}
                      >
                        <option value='' disabled>Year</option>
                        {yearOptions}
                      </select>

                      <select
                        disabled={this.userType == 'Patient' ? lockedInput[i] || this.state.lockAll : true}
                        className={!lockedInput[i] && this.userType == 'Patient' ? inputClass : lockedInputStyle}
                        id={'eMonth' + i}
                        value={arrDate[1]}
                        onChange={() => this._handleInputChange('eMonth', 'event_date', i)}
                      >
                        <option value=''>Month</option>
                        {monthOptions}
                      </select>
                    </React.Fragment>
                }
              </GridContainer>

              <div className={eventItemStyle} style={{ flex: 2 }}>
                <input
                  disabled={this.userType == 'Patient' ? lockedInput[i] || this.state.lockAll : true}
                  className={!lockedInput[i] && !this.state.lockAll && this.userType == 'Patient' ? inputClass : lockedInputStyle}
                  onChange={() => this._handleInputChange('eName', 'event_name', i)}
                  // ref={'eName' + i}
                  name={'eName' + i}
                  id={'eName' + i}
                  type='text'
                  placeholder="Event Name"
                  value={description[x].event_name}
                  style={{ color: Colors.blueDarkAccent, fontWeight: 500 }}
                />
              </div>

              <div className={descriptionWrapper} style={{ flex: 4 }}>
                {this.userType === 'Patient' && item.comment != 'null' && item.comment != '' ?
                  <span className={commentIconStyle} onClick={() => this._toggle('eComentDiv' + i)}>!</span> : null}
                <TextareaAutosize
                  placeholder='Describe Event Here'
                  disabled={this.userType == 'Patient' ? lockedInput[i] || this.state.lockAll : true}
                  className={!lockedInput[i] && !this.state.lockAll && this.userType == 'Patient' && (item.comment === 'null' || item.comment == '') ? inputClass : lockedTextArea}
                  // ref={'eDesc' + i}
                  id={'eDesc' + i}
                  name={'eDesc' + i}
                  value={description[x].description}
                  onChange={() => this._handleInputChange('eDesc', 'description', i)}
                  style={{ minHeight: 42, padding: '14px 5px' }}
                />
                {item.comment != 'null' || this.userType == 'Doctor'
                  ?

                  <div style={{ width: '98%' }} id={'eComentDiv' + i} hidden={this.userType === 'Patient' ? true : false}>

                    {lockedInput[i] || this.state.lockAll
                      ?
                      <div className={commentStyle}>
                        {description[x].comment}
                      </div>
                      :
                      <TextareaAutosize
                        placeholder='Place Comment Here'
                        disabled={lockedInput[i] || this.state.lockAll}
                        className={!lockedInput[i] ? unlockedComment : lockedComment}
                        // ref={'eComment' + i}
                        id={'eComment' + i}
                        name={'eComment' + i}
                        value={description[x].comment === 'null' ? '' : description[x].comment}
                        onChange={() => this._handleInputChange('eComment', 'comment', i)}
                      />
                    }

                  </div>

                  :
                  null
                }
              </div>

              {this.userType === 'Patient' && (item.comment === 'null' || item.comment === '')
                ?
                <div id={`action-${item.id}`} className={eventItemStyle} style={{ flex: 0.5 }}>
                  <span onClick={() => this._unlockInput(i)}>
                    <img className={iconStyle} src={"/static/images/edit.svg"} />
                  </span> &nbsp;
                  <span onClick={() => this._removeEvent(i)}>
                    <img className={iconStyle} src={"/static/images/trash.svg"} />
                  </span>
                </div>
                : <div className={eventItemStyle} style={{ flex: 0.5 }}>
                  &nbsp;
                      </div>
              }

              {this.state.updateMode && this.userType === 'Doctor'
                ? <div className={eventItemStyle} style={{ flex: 0.5 }}>
                  <span onClick={() => this._unlockInput(i)}>
                    <img className={iconStyle} src={"/static/images/edit.svg"} />
                  </span>
                  &nbsp;
                            <span onClick={() => this._removeEvent(i)}>
                    <img className={iconStyle} src={"/static/images/trash.svg"} />
                  </span>
                </div>
                : null
              }
            </GridContainer>
            // </div>
          ) // return
        }// if
      }) // items
    }// end of if

    return (
      <div className={['wrap'].join(' ')}>
        {headers}

        <section className={scrollClass} style={{ maxHeight: 400, overflowY: 'auto', width: '100%' }}>
          {items}
        </section>


        <section style={{ margin: '20px auto 0', width: '100%' }}>
          <GridContainer columnSize={this.props.withChanges ? '1fr 1fr 1fr' : '1fr'} styles={{ width: this.props.withChanges ? '70%' : 'fit-content', margin: '0 auto' }}>
            <Button type="pink" style={{ margin: '0 10px' }}>
              <button onClick={this._showAddEvent}>+ Add Event</button>
            </Button>
            {
              this.props.withChanges &&
              <React.Fragment>
                <Button type="blue" style={{ margin: '0 10px' }}>
                  <button onClick={() => this.props.saveEventChanges()}>Save Changes</button>
                </Button>
                <Button style={{ margin: '0 10px' }}>
                  <button onClick={() => this.props.cancelEventChanges(false)}>Cancel</button>
                </Button>
              </React.Fragment>
            }
          </GridContainer>

          {
            this.state.showAddEvent && <AddEvent year={ydd()} month={mdd} addEvent={this._addEvent} cancel={this._showAddEvent} />
          }


        </section>

      </div>
    )
  } //_displayDetails

  _showAddEvent = () => {
    this.setState({
      showAddEvent: !this.state.showAddEvent
    })
  }

  _toggle(id) {

    let dv = document.getElementById(id)
    dv.hidden = !dv.hidden

  }//_toggle

  _unlockInput(i) {
    this.props.setUpdateMode(true)
    let lockedInput = this.state.lockedInput
    lockedInput[i] = !lockedInput[i]

    this.setState({
      lockedInput,
      lockAll: false
    })
  }

  _handleInputChange(name, arrn, i) {

    let timelineDetails = this.state.timelineDetails

    let selectedTimeline = timelineDetails.filter(tl => tl.id == i)

    if (arrn === 'description') {
      selectedTimeline[0][arrn] = document.getElementById(name + i).value
    } else if (name === 'eYear') {
      let temp_val = selectedTimeline[0][arrn].split(',')
      selectedTimeline[0][arrn] = document.getElementById(name + i).value + ',' + temp_val[1]
    } else if (name === 'eMonth') {
      let temp_val = selectedTimeline[0][arrn].split(',')
      selectedTimeline[0][arrn] = temp_val[0] + ',' + document.getElementById(name + i).value
    } else {
      selectedTimeline[0][arrn] = document.getElementById(name + i).value
    }

    this.setState({
      timelineDetails
    }, () => {
      this.props.checkForChanges()
    })

  }

  _removeEvent(i) {

    let timelineDetails = this.state.timelineDetails

    for (var x = 0; x < timelineDetails.length; x++) {
      if (timelineDetails[x].id == i) {
        //timelineDetails.splice(x, 1);
        timelineDetails[x].del = 1
        break;
      }
    }

    this.setState({
      timelineDetails
    }, () => {
      this.props.checkForChanges()
    })

  }// _removeEvent

  _addEvent() {

    let timelineDetails = this.state.timelineDetails
    let id = 0

    if (timelineDetails.length > 0) {
      id = (timelineDetails[timelineDetails.length - 1].id) + 1
    }

    let eComment = ''
    let eYear = document.getElementById('eYear').value
    let eMonth = document.getElementById('eMonth').value
    let eName = document.getElementById('eName').value
    let eDesc = document.getElementById('eDesc').value

    if (document.getElementById('eComment')) {
      eComment = document.getElementById('eComment').value
    }

    let eDate = `${eYear},${eMonth}`

    if (eYear && eMonth && eName && eDesc) {
      timelineDetails.push({
        id: id,
        event_date: eDate,
        event_name: eName,
        description: eDesc,
        comment: eComment,
        user_id: this.userId,
        new_event: 1
      })

      // clear fields
      document.getElementById('eYear').value = ''
      document.getElementById('eMonth').value = ''
      document.getElementById('eName').value = ''
      document.getElementById('eDesc').value = ''
    } else {
      alert('incomplete');
    }

    this.setState({
      timelineDetails
    }, () => {
      this.props.checkForChanges()
      this._lockInput(0)
    })

  } //_addEvent

}// class

const descriptionWrapper = css({
  display: 'grid',
  alignContent: 'center',
  '> textarea': {
    color: '#777'
  }
})

const dateStyle = css({
  color: Colors.blueDarkAccent,
  textAlign: 'right !important',
  fontWeight: 500,
  textTransform: 'uppercase'
})

const dateStyleLeft = css(dateStyle, {
  textAlign: 'left !important',
})

let eventItemStyle = css({
  display: 'flex',
  flexWrap: 'wrap',
  alignItems: 'center',
  padding: 5,
  height: '100%',
  margin: 0,
  justifyContent: 'center',
  ':last-child': { border: 'none' }
})

const center = css({
  textAlign: 'center'
})

let lockedInputStyle = css(center, {
  border: 'none',
  background: 'transparent',
  width: '100%'
})

let lockedTextArea = css(mainFont, {
  boxSizing: 'padding-box',
  overflow: 'hidden',
  minHeight: 42,
  width: ' 95%',
  fontSize: 16,
  // margin: '5px',
  borderRadius: '6px',
  boxShadow: '2px 2px 8px rgba(black, .3)',
  border: 'none',
  background: 'transparent',
})


let unlockedComment = css({
  display: 'block',
  boxSizing: 'padding-box',
  overflow: 'hidden',
  padding: '10px',
  width: ' 100%',
  fontSize: '14px',
  margin: '5px',
  borderRadius: '6px',
  boxShadow: '2px 2px 8px rgba(black, .3)',
  border: '1px solid white',
  fontStyle: 'italic',
  textAlign: 'justify'

})

let lockedComment = css({
  display: 'block',
  boxSizing: 'padding-box',
  overflow: 'hidden',
  padding: '10px',
  width: ' 100%',
  fontSize: '14px',
  margin: '5px',
  borderRadius: '6px',
  boxShadow: '2px 2px 8px rgba(black, .3)',
  border: '1px solid white',
  fontStyle: 'italic',
  textAlign: 'justify',
  background: 'transparent',

})

let iconStyle = css({
  width: 15,
  height: 15,
  margin: 5,
  cursor: 'pointer'

})

let commentIconStyle = css({
  // position: 'relative',
  // width: '2%',
  // padding: '3px 10px',
  // borderRadius: 5,
  // background: '#80cde9',
  // fontWeight: 'bold',
  // color: 'yellow',
  // cursor: 'pointer',
  // ':hover': {
  //   background: 'gray'
  // }
})

let commentStyle = css({
  fontStyle: 'italic',
  padding: 10,
})
