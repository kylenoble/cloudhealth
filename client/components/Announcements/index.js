import { css } from 'glamor'
import { confirmAlert } from 'react-confirm-alert'; // Import
import moment from 'moment-timezone';
import AnnouncementService from '../../utils/announcementsService'
import dateFormat from 'dateformat'; // not localize
import ReactTable from "react-table";
import AdminService from '../../utils/userService'
import NoData from '../NewSkin/Components/NoData';
import { ReactTableStyle } from '../NewSkin/Styles';
import Colors from '../NewSkin/Colors';

const announcements = new AnnouncementService()
const admin = new AdminService()

export default class extends React.Component {

  constructor(props) {
    super(props)
    this.user = this.props.user
    this.state = {
      announcements: [],
    }

  }

  componentDidMount() {
    this._getAnnoucements()
  }

  _getAnnoucements() {
    let type = this.props.user.type === 'Patient' ? 'Patients' : this.props.user.type === 'Doctor' ? 'Doctors' : null
    type += '_' + this.props.user.company_id
    announcements.get(type)
      .then((res) => {
        if (res.length > 0) {
          this.setState({
            announcements: res
          })

          this._allAnnouncementsRead()
        }
      })
      .catch((e) => {
        console.log(e);
      })
  }

  _allAnnouncementsRead = () => {
    const announcements = this.state.announcements
    const notYetRead = announcements.filter(n => {
      if (n.ihaveread == 'no') {
        return n
      }
    })

    if (notYetRead.length > 0) {
      this.props.updateRead('read_announcements', false)
    } else {
      this.props.updateRead('read_announcements', true)
    }

  }



  _remove(announcement) {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: '',                        // Title dialog
      message: 'Are you sure you want to remove this?',               // Message dialog
      buttons: [
        {
          label: 'Yes',
          onClick: () => this._updateAnnouncement(announcement, true)
        },
        {
          label: 'No'
        }
      ]
    })
  }




  _showDetails(announcement) {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: '',                        // Title dialog
      message: '',               // Message dialog
      childrenElement: () => this._announcementDetails(announcement),
      buttons: [
        {
          label: 'Close',
          onClick: () => this._updateAnnouncement(announcement)
        }
      ]
    })
  }

  _updateAnnouncement(announcement, bol = false) {
    var date = new Date();
    let ndate = dateFormat(date, 'yyyy-mm-dd h:MM:ss TT')
    let read_by_data = announcement.read_by
    let result = []
    if (read_by_data) {
      result = read_by_data.data.filter(rb => rb.id === this.user.id);
    }

    let rd = []
    if (read_by_data) {
      rd = read_by_data.data
    }
    let data = {
      id: announcement.id,
      read_by: {
        data: rd
      }
    }

    if (result.length > 0) {
      if (bol) {
        result[0].status = 1
      }

      data = {
        id: announcement.id,
        read_by: read_by_data
      }

    } else {
      data.read_by.data.push({
        type: this.user.type,
        id: this.user.id,
        date_read: ndate,
        status: null
      })
    }

    announcements.update(data)
      .then((res) => {
        this._getAnnoucements()
      })
      .catch((e) => {
        console.log(e);
      })
  }

  _autoresize = () => {
    setTimeout(() => {
      const textarea = document.querySelector('#details-container')
      textarea.style.height = `${textarea.scrollHeight}px`
    }, 100);
  }

  _announcementDetails(announcement) {
    this._autoresize()
    return (
      <div style={{width: 600}}>
        <h4 className={notifFrom}><small>From: {announcement.sender} - {moment(announcement.date_created).format("ll")}</small></h4>

        <div className={appointmentDetailsWrapper}>
          <label className={detailsTitle}>Subject: {announcement.title}</label>
          <textarea disabled id="details-container" className={`${detailsContainer}`} defaultValue={announcement.details}></textarea>
        </div>
      </div>
    )
  }

  renderAnnouncementsTable() {
    let columns = []
    const announcements = this.state.announcements;
    const data = []

    if (announcements.length > 0) {
      let user = this.user;
      announcements.filter(function (item) {
        let sl = [];
        item.ihaveread = 'no'
        if (item.read_by) {
          sl = item.read_by.data.filter(rb => rb.id === user.id)
        }
        if (sl.length > 0) {
          if (sl[0].status !== 1) {
            item.ihaveread = 'yes'
            data.push(item)
          }
        } else {
          data.push(item)
        }
      })
    }

    if (data.length > 0) {
      data.filter(function (item) {
        columns = [
          {
            Header: 'Sender',
            accessor: 'sender',
            Cell: item => (
              <div
                id={`sender-${item.original.id}`}
                onClick={() => { this._showDetails(item.original) }}
                style={item.original.ihaveread == 'no' ? { fontWeight: 600, color: Colors.skyblue } : null}>
                {item.value}
              </div>
            ),
            style: { 'whiteSpace': 'unset', cursor: 'pointer' }
          },
          {
            Header: 'Title',
            accessor: 'title',
            Cell: item => (
              <div
                onClick={() => { this._showDetails(item.original) }}
                style={item.original.ihaveread == 'no' ? { fontWeight: 600, color: Colors.skyblue } : null}>
                {item.value}
              </div>
            ),
            style: { 'whiteSpace': 'unset', cursor: 'pointer' },
            width: 300
          },
          {
            Header: 'Details',
            accessor: 'details',
            Cell: item => (
              <div
                onClick={() => { this._showDetails(item.original) }}
                style={item.original.ihaveread == 'no' ? { fontWeight: 600, color: Colors.skyblue } : null}>
                {item.value.length > 50 ? `${item.value.substr(0, 50)}...` : item.value}
              </div>),
            style: { 'whiteSpace': 'unset', cursor: 'pointer' },
            width: 300
          }, {
            Header: 'Date',
            accessor: 'date_created',
            Cell: item => (
              <div
                onClick={() => { this._showDetails(item.original) }}
                style={item.original.ihaveread == 'no' ? { fontWeight: 600, color: Colors.skyblue } : null}>
                {moment(item.value).format("ll")}
              </div>),
            style: { 'whiteSpace': 'unset', 'textAlign': "center", cursor: 'pointer' },
            width: 200
          }, {
            Header: '',
            accessor: 'remove',
            Cell: item => (
              item.original.ihaveread == 'yes' ? (
                <img
                  onClick={() => {
                    this._remove(item.original);
                  }}
                  className={iconStyle}
                  alt="Remove"
                  src={"/static/images/trash.svg"}
                />
              ) : null
            ),
            width: 50
          }
        ]
      }, this)
    }

    return (
      <>
        {
          data.length > 0 ?
          <section className={ReactTableStyle} style={{ maxWidth: '100%' }}>
            <ReactTable
              data={data}
              columns={columns}
              defaultPageSize={5}
              minRows={0}
              className="-striped -highlight"
              width="100%"
              getTdProps={(state, rowInfo, column, instance) => {
                return {
                  onClick: (e, handleOriginal) => {
                    if (handleOriginal) {
                      handleOriginal()
                    }
                  }
                }
              }}
            />
            </section> :
            <NoData text="No announcement" />
        }
      </>

    )
  }


  render() {
    return (
      this.renderAnnouncementsTable()
    )
  }

}// end of component class

// CSS Styling
let detailsContainer = css({
  width: '100% !important',
  resize: 'none',
  border: 'none !important',
  boxSizing: 'border-box',
  padding: 5,
  background: 'none',
  overflow: 'none !important',
  marginTop: 20
})

let notifFrom = css({
  margin: '10px 0px',
  '> small': {
    background: 'whitesmoke',
    padding: '5px 10px',
    display: 'inline-block',
  }
})

let appointmentDetailsWrapper = css({
  padding: 15,
  background: '#fff',
  overflow: 'hidden',
  borderRadius: 3,
  marginTop: 10,
  boxShadow: '0 5px 10px rgba(0,0,0,.05)',
  '> h4':{
    fontSize: 18
  },
  '> div span': {
    display: 'inline-block',
    minWidth: 135
  },
  '> p': {
    padding: '10px 5px'
  }
})

let detailsTitle = css({
  margin: '-15px -15px 0',
  display: 'block',
  padding: '5px 15px',
  background: '#80cde9',
  textTransform: 'uppercase',
  color: '#fff'
})

let iconStyle = css({
  width: 15,
  height: 15,
  cursor: 'pointer',
  opacity: .5,
  transition: '200ms ease',
  display: 'block',
  margin: 'auto 0',
  filter: 'grayscale(100%)',
  ':hover': {
    filter: 'grayscale(0%)',
    opacity: 1
  }
})