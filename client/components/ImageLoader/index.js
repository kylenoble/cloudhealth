import React, {Component} from 'react'
import {css} from 'glamor'

export default class extends Component{
  render(){
    return (<div style={styles.spinner}><img src= "/static/icons/30.gif"></img></div>)
  }
}

const styles = {
  spinner: {
    position : 'absolute',
    background : 'white',
    top : 0,
    left : 0,
    display: 'flex',
    flex: '1 0 100%',
    width : '100%',
    minHeight:"90vh",
    justifyContent: 'center',
    alignItems: 'center',
    opacity : 0.5,
    zIndex:3
  }
}
