import React from 'react';
import moment from 'moment-timezone';
import jstz from 'jstz';
import { css } from 'glamor'

import AppointmentService from '../../utils/appointmentService.js'

const appointment = new AppointmentService()

export default class extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            patientHistory: [],
            loadingHistory: true,
        };

        this._showPatientHistory = this._showPatientHistory.bind(this)
    }

    componentDidMount() {
        appointment.getAppointmentsList({completed: true})
        .then(res => {
            if (res.name === 'Error') {
                this.setState({
                    loadingHistory: false,
                })
            } else {
                this.setState({
                    patientHistory: res,
                    loadingHistory: false
                })
            }
        })
        .catch(e => {
            console.log(e)
            this.setState({
                loadingHistory: false,
            })
        })  // you would show/hide error messages with component state here
    }

    render () {
        return (
            <div className={container}>
                <div style={styles.tableHeader}>
                    <h1 style={styles.headerItem}>Patient</h1>
                    <h1 style={styles.headerItem}>Date</h1>
                    <h1 style={styles.headerItem}></h1>
                </div>
                {this._renderView()}
            </div>
        )
    }

    _renderView() {
        if (this.state.loadingHistory) {
            return (
                <div>Loading...</div>
            )
        } else {
            return (
                this._showPatientHistory()
            )
        }
    }


    _showPatientHistory() {
        if (this.state.patientHistory.length < 1) {
            return(
                <span key={1} style={styles.emptyState}>No Patient History</span>
            )
        }
        let html = []
        const timezone = jstz.determine();
        this.state.patientHistory.forEach((currentItem, index) => {
            html.push(
                <div key={index} style={styles.itemContainer}>
                    <h1 style={{...styles.patientItem, ...styles.patientItemSmall}}>{currentItem.name}</h1>
                    <h1 style={{...styles.patientItem, ...styles.patientItemLarge}}>{moment(currentItem.datetime).tz(timezone.name()).format('MM/DD/YYYY hh:mma')}</h1>
                    <button style={styles.startAppointment}>View</button>
                </div>
            )
        });

        return html
    }
}

const container = css({
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '80%',
    margin: '80px 2% 0 2%',
    fontFamily: '"Roboto-Medium",sans-serif',
    paddingLeft: '20px',
    background: 'white',
    height: '80%',
    margin: 'auto',
    '@media(max-width: 700px)': {
        marginTop: '100px',
    }
})

const styles = {
  tableHeader: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'left',
    justifyContent: 'left',
    borderBottom: '1px solid #E1E8EE',
    flexWrap: 'wrap',
    width: '100%',
    marginBottom: '10px',
  },
  headerItem: {
    fontFamily: "Roboto-Medium",
    fontSize: "14px",
    color: "#BDC7CF",
    letterSpacing: 0,
    padding: "10px",
    cursor: 'pointer',
    flex: '1 0 auto',
  },
  itemContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    width: '100%',
  },
  patientItem: {
    fontFamily: "Roboto-Medium",
    fontSize: "16px",
    color: "#364563",
    letterSpacing: 0,
    lineHeight: "14px",
    padding: "10px",
    cursor: 'pointer',
    textAlign: 'center',
    flex: '1 0 auto',
  },
  emptyState: {
    marginTop: '30px',
  },
  startAppointment: {
    fontFamily: '"Roboto-Medium",sans-serif',
    backgroundColor: '#03D27F',
    width: '100px',
    height: '42px',
    border: 'none',
    borderRadius: '6px',
    fontSize: '14px',
    color: 'white',
    cursor: 'pointer',
    float: 'right',
    flex: '1 0 auto',
  },
}
