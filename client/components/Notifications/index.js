/* eslint-disable space-before-function-paren */
/* eslint-disable no-confusing-arrow */
/* eslint-disable no-param-reassign */
/* eslint-disable array-callback-return */
//import currentDomain from '../../../utils/domain.js';
import React from "react";
import { css } from "glamor";
import Router from "next/router";
import moment from "moment-timezone"; // localizede
import ReactTable from "react-table";
import { confirmAlert } from "react-confirm-alert"; // Import
import AppointmentNotification from "./appointmentNotifications";
import NotificationService from "../../utils/notificationService.js";
import AnswerService from "../../utils/answerService.js";

import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Card from "../NewSkin/Wrappers/Card";
import { ReactTableStyle, labelClass } from "../NewSkin/Styles";
import NoData from "../NewSkin/Components/NoData";
import UpdateNotification from "../NewSkin/Components/UpdateNotification";

const Notification = new NotificationService();

const answer = new AnswerService();

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.user = this.props.user;
    this.state = {
      notificationsFromAdmin: [],
      notification: [],
      allRead: ""
    };
  }

  componentWillMount() {
    this._getPersonalInfo();
    this._getAdminNotifications();
  }

  _showContent(content) {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: "", // Message dialog
      childrenElement: () => this._message(content), // Custom UI or Component
      buttons: [
        {
          label: "CLOSE",
          onClick: () => this._updateNotification(content)
        }
      ]
    });
  }

  _getPersonalInfo() {
    answer
      .getAnswerList({ column: "question_id", operator: "lt", value: 11 })
      .then(res => {
        if (res.name === "Error") {
        } else if (res.length > 0) {
          if (res.length < 6) {
            console.log("_getPersonalInfo", res);
            this._notification(
              "Personal",
              "Please complete your personal profile",
              "Profile"
            );
          }
        }
      })
      .catch(e => {
        console.log(e);
      }); // you would show/hide error messages with component state here
  }

  _getAdminNotifications() {
    const id = this.user.id;
    const company_id = this.user.company_id;
    const type = this.user.type;
    const ids = `${id}~${company_id}~${type}`;
    Notification.getAdmin(ids)
      .then(res => {
        this.setState({
          notificationsFromAdmin: res
        });
        this._allNotificationsRead();
      })
      .catch(e => {
        console.log(e);
      });
  }

  _allNotificationsRead = () => {
    const notifications = this.state.notificationsFromAdmin;
    const personal_notification = this.state.notification;

    const notYetRead = notifications.filter(n => {
      if (n.ihaveread === "no") {
        return n;
      }
    });

    if (notYetRead.length > 0 || personal_notification.length > 0) {
      this.setState({ allRead: false });
    } else {
      if (notifications.length > 0) {
        this.setState({ allRead: true });
      } else {
        this.setState({ allRead: false });
      }
    }
  };

  _notification(nme, note, lnk) {
    const NOTIFICATION = [];
    const currentNotification = this.state.notification;

    let p = true;
    if (currentNotification.length > 0) {
      for (let i = 0; i < currentNotification.length; i++) {
        if (currentNotification[i].name === nme) {
          p = false;
        }
      }
      if (p) {
        NOTIFICATION.push({ name: nme, description: note, link: lnk });
      }
    } else {
      NOTIFICATION.push({ name: nme, description: note, link: lnk });
    }

    this.setState({
      notification: NOTIFICATION
    });

    this._allNotificationsRead();
  }

  _message(content) {
    const timeCreated = moment(content.date_created).toISOString();
    return (
      <div>
        {content.sender.name === "System" ? (
          <AppointmentNotification user={this.props.user} content={content} />
        ) : (
            <div>
              <h3>From : {content.sender.name}</h3>
              <h3>{content.subject}</h3>
              <h4>{moment(timeCreated).format("LLL")}</h4>
              <br />
              <p dangerouslySetInnerHTML={{ __html: content.message }} />
            </div>
          )}
      </div>
    );
  }

  _updateNotification(notification, val = null) {
    const ndate = moment();
    const read_by_data = notification.read_by;
    let result = [];

    if (read_by_data) {
      result = read_by_data.data.filter(rb => rb.id === this.user.id);
    }

    let rd = [];

    if (read_by_data) {
      rd = read_by_data.data;
    }

    let data = {
      id: notification.id,
      read_by: {
        data: rd
      }
    };

    if (result.length > 0) {
      if (val) {
        result[0].status = val;
      }

      data = {
        id: notification.id,
        read_by: read_by_data
      };
    } else {
      data.read_by.data.push({
        type: this.user.type,
        id: this.user.id,
        date_read: ndate,
        status: null
      });
    }

    this._goUpdate(data);
  }

  _goUpdate(data) {
    Notification.updateAdmin(data).then(() => {
      this._getAdminNotifications();
    });
  }

  _removeNotification(c, val) {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: "Are you sure you want to remove this notification?", // Message dialog
      buttons: [
        {
          label: "REMOVE",
          onClick: () => this._updateNotification(c, val)
        },
        {
          label: "CLOSE"
        }
      ]
    });
  }

  _setNotifiationStatus(notificationId, val) {
    const data = {
      id: notificationId,
      status: val
    };

    Notification.updateAdmin(data).then(res => {
      if (res) this._getAdminNotifications();
    });
  }

  _goTo(lnk) {
    if (lnk == "Profile") {
      Router.push({
        pathname: '/account',
        query: { tab: 'Profile', view: 'Personal' }
      })
    }
    this.props.showAddData(lnk);
  }

  renderNotificationsTable() {
    let columns = [];
    const notification = this.state.notification;
    const notificationsFromAdmin = this.state.notificationsFromAdmin;
    const data = [];

    if (notification.length > 0 && this.user.type === "Patient") {
      notification.forEach(item => {
        data.push({
          sender: "System",
          name: item.name,
          description: item.description,
          link: item.link,
          ihaveread: item.is_read ? "yes" : "no"
        });
      }, []);
    }

    if (notificationsFromAdmin.length > 0) {
      const user = this.user;
      notificationsFromAdmin.filter(item => {
        let sl = [];
        item.ihaveread = "no";
        if (item.read_by) {
          sl = item.read_by.data.filter(rb => rb.id === user.id);
          item.ihaveread = "yes";
        }

        if (sl.length > 0) {
          if (sl[0].status !== 1) {
            data.push(item);
          }
        } else {
          data.push(item);
        }
      });
    }

    if (data.length > 0) {
      data.filter(function () {
        columns = [
          {
            Header: "Sender",
            accessor: "sender",
            Cell: item => (
              <div
                className={
                  item.original.ihaveread === "yes" ? ReadnameStyle : nameStyle
                }
              >
                {typeof item.value === "object" ? (
                  <div style={{ cursor: "pointer" }}>
                    {item.original.sender.name}
                  </div>
                ) : (
                    item.value
                  )}
              </div>
            ),
            style: {
              whiteSpace: "unset",
              textAlign: "center",
              textTransform: "capitalize"
            }
          },
          {
            Header: "Date Created",
            accessor: "date_created",
            Cell: item => (
              <div
                className={
                  item.original.ihaveread === "yes" ? ReadnameStyle : nameStyle
                }
              >
                {typeof item.original.sender === "object" ? (
                  <div
                    style={{ cursor: "pointer" }}
                    onClick={() => this._showContent(item.original)}
                  >
                    {moment(item.value).format("LLL")}
                  </div>
                ) : (
                    item.original.name
                  )}
              </div>
            ),
            style: {
              whiteSpace: "unset",
              textAlign: "center",
              textTransform: "capitalize"
            }
          },
          {
            Header: "Subject",
            accessor: "subject",
            Cell: item => (
              <div
                className={
                  item.original.ihaveread === "yes" ? ReadnameStyle : nameStyle
                }
              >
                {typeof item.original.sender === "object" ? (
                  <div
                    style={{ cursor: "pointer" }}
                    onClick={() => this._showContent(item.original)}
                  >
                    {item.value}
                  </div>
                ) : (
                    <div className={subjectDdiv}>
                      {item.original.description}
                      {item.original.link ? (
                        <span
                          onClick={() => this._goTo(item.original.link)}
                          className={bookStyle}
                        >
                          Click here
                      </span>
                      ) : null}
                    </div>
                  )}
              </div>
            ),
            style: { whiteSpace: "unset", textTransform: "capitalize" }
          },
          {
            Header: "",
            accessor: "remove",
            Cell: item =>
              typeof item.original.sender === "object" &&
                item.original.ihaveread === "yes" ? (
                  <span
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      this._removeNotification(item.original, 1);
                    }}
                  >
                    <img
                      className={iconStyle}
                      alt="Remove"
                      src={"/static/images/trash.svg"}
                    />
                  </span>
                ) : null,
            width: 50
          }
        ];
      }, this);

      return (
        <ReactTable
          data={data}
          columns={columns}
          defaultPageSize={10}
          minRows={0}
          className="-striped -highlight"
          width={90}
          getTdProps={() => ({
            onClick: (e, handleOriginal) => {
              if (handleOriginal) {
                handleOriginal();
              }
            }
          })}
        />
      );
    }
    return <NoData />;
  }

  renderView() {
    const notification = this.state.notification;
    const notificationsFromAdmin = this.state.notificationsFromAdmin;
    let result;
    const header = (
      <div
        key={"hdr"}
        className={detailsWrapper}
        style={{ background: "#52617f", color: "#fff" }}
      >
        <div className={ReadnameStyle}>Sender</div>
        <div className={ReadnameStyle}>Date Created</div>
        <div className={subjectDdiv}>Subject</div>
        <span className={removeStyle} />
      </div>
    );

    if (notification.length > 0) {
      result = notification.map((item, i) => (
        <div key={i} className={detailsWrapper}>
          <div className={ReadnameStyle}>System</div>
          <div className={item.is_read ? ReadnameStyle : nameStyle}>
            {notification[i].name}
          </div>
          <div className={subjectDdiv}>
            {notification[i].description}{" "}
            {notification[i].link ? (
              <span
                onClick={() => {
                  this._goTo(notification[i].link);
                }}
                className={bookStyle}
              >
                Click here
              </span>
            ) : null}
          </div>
        </div>
      ));
    }

    let adminNotifs;
    if (notificationsFromAdmin.length > 0) {
      adminNotifs = notificationsFromAdmin.map((item, i) => {
        let sl = [];
        let notifs = "";
        if (item.read_by) {
          sl = item.read_by.data.filter(rb => rb.id === this.user.id);
        }

        if (sl.length > 0) {
          if (sl[0].status !== 1) {
            notifs = (
              <div key={i} className={detailsWrapper}>
                <div
                  className={sl[0] ? ReadnameStyle : nameStyle}
                  onClick={() => this._showContent(item)}
                >
                  {item.sender.name}
                </div>
                <div
                  className={sl[0] ? ReadnameStyle : nameStyle}
                  onClick={() => this._showContent(item)}
                >
                  {moment(item.date_created).format("LLL")}
                </div>
                <div
                  className={subjectDdiv}
                  onClick={() => this._showContent(item)}
                >
                  <span className={subjectStyle}>{item.subject}</span>
                </div>
                <span
                  className={removeStyle}
                  onClick={() => {
                    this._removeNotification(item, 1);
                  }}
                >
                  {sl[0] ? (
                    <span>
                      <img
                        className={iconStyle}
                        alt="Remove"
                        src={"/static/images/trash.svg"}
                      />
                    </span>
                  ) : (
                      <span className={iconStyle}>&nbsp;</span>
                    )}
                </span>
              </div>
            );
          }
        } else {
          notifs = (
            <div key={i} className={detailsWrapper}>
              <div
                className={sl[0] ? ReadnameStyle : nameStyle}
                onClick={() => this._showContent(item)}
              >
                {item.sender.name}
              </div>
              <div
                className={sl[0] ? ReadnameStyle : nameStyle}
                onClick={() => this._showContent(item)}
              >
                {moment(item.date_created).format("LLL")}{" "}
              </div>
              <div
                className={subjectDdiv}
                onClick={() => this._showContent(item)}
              >
                <span className={subjectStyle}>{item.subject}</span>
              </div>
              <span
                className={removeStyle}
                onClick={() => {
                  this._removeNotification(item, 1);
                }}
              >
                {sl[0] ? (
                  <span>
                    <img
                      className={iconStyle}
                      alt="Remove"
                      src={"/static/images/trash.svg"}
                    />
                  </span>
                ) : (
                    <span className={iconStyle}>&nbsp;</span>
                  )}
              </span>
            </div>
          );
        }

        return notifs;
      });
    }
    const title = (
      <div className={announcementWrapper}>
        <span className={notiTitle}>Notifications</span>
      </div>
    );

    return (
      <div className={wrapper}>
        {title}
        {header}
        {result}
        {adminNotifs}
      </div>
    );
  }

  render() {
    return (
      <GridContainer styles={{alignContent: 'start', paddingTop: 30, maxWidth: '80%', minWidth: '70vw', margin: '0 auto', width: '80% !important'}}>
        {
          this.state.allRead && <UpdateNotification msg="You've read all your notifications" />
        }


        <Card fullWidth>
          <label className={labelClass}>Notifications</label>
          <section className={ReactTableStyle} style={{maxWidth: 'unset', width: '100%'}}>
            {this.renderNotificationsTable()}
          </section>
        </Card>
      </GridContainer>
    );
  }
}

let bookStyle = css({
  marginLeft: "10px",
  textAlign: "center",
  fontStyle: "bold",
  color: "#1c75bb",
  cursor: "pointer"
});

let wrapper = css({
  display: "flex",
  flex: "1 0 100%",
  paddingTop: "50px",
  flexWrap: "wrap",
  justifyContent: "center",
  width: "90%",
  marginLeft: "auto",
  marginRight: "auto"
});

let announcementWrapper = css({
  position: "relative",
  //background:'#daf1f5',
  display: "flex",
  flexWrap: "wrap",
  fontSize: "small",
  alignItems: "center",
  width: "100%"
  // ':nth-child(even)':
  //  {background:'#c3e9f7'}
});

let detailsWrapper = css({
  position: "relative",
  background: "#daf1f5",
  display: "flex",
  flexWrap: "wrap",
  fontSize: "small",
  alignItems: "center",
  width: "70%",
  ":nth-child(even)": { background: "#c3e9f7" }
});

let nameStyle = css({
  // width: "200px",
  // textTransform: "capitalize",
  fontWeight: "bold"
  // margin: "3px 5px",
  // cursor: "pointer",
  // "@media (maxWidth: 944px)": {
  //   flex: "1 0 100%"
  // }
});

let ReadnameStyle = css({
  // width: "200px",
  // textTransform: "capitalize",
  //fontWeight: 'bold',
  // cursor: "pointer",
  // margin: "3px 5px",
  // "@media (maxWidth: 944px)": {
  //   flex: "1 0 100%"
  // }
});

let removeStyle = css({
  position: "absolute",
  right: 5,
  cursor: "pointer",
  ":hover": {
    color: "orange"
  }
});

let subjectDdiv = css({
  // position: "relative",
  // width: "30%",
  textTransform: "capitalize"
  // margin: "3px 5px",
  // "@media (maxWidth: 944px)": {
  //   flex: "1 0 100%"
  // }
});

let subjectStyle = css({
  color: "#52617f",
  fontWeight: "bold",
  cursor: "pointer",
  ":hover": {
    color: "#000"
  }
});

let iconStyle = css({
  display: "block",
  width: 15,
  height: 15,
  marginRight: "auto",
  marginLeft: "auto",
  margin: "auto"
});

let notiTitle = css({
  flex: "1 0 100%",
  //marginTop: '20px',
  padding: 10,
  fontSize: "large",
  fontWeight: "bold"
});

let noDataDiv = css({
  display: "flex",
  background: "#52617f",
  color: "#fff",
  fontSize: 16,
  flex: 1,
  textTransform: "capitalize",
  height: "100px",
  justifyContent: "center",
  alignItems: "center"
});
