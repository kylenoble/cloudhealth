import React, { Component } from 'react'
import { css } from 'glamor'
import moment from 'moment-timezone'
import { labelClass } from '../NewSkin/Styles'

export default class AppointmentNotifications extends Component {
  convertDate = date => {
    return moment(date).format('MMMM DD YYYY hh:mm A')
  }

  render() {
    const { type } = this.props.user
    const { ...notif } = this.props.content

    let date_read = notif.read_by

    if (date_read == null) {
      date_read = '-'
    } else {
      date_read = this.convertDate(date_read.data[0].date_read)
    }

    const is_subcription = notif.recipient.subscription
    return (
      <section id="notification-modal-wrapper" className={appNotifWrapper}>
        <label className={labelClass}>{notif.subject}</label>
        <h4 className={notifFrom}><small>From: {notif.sender.name} - {this.convertDate(notif.date_created)}</small></h4>

        <div className={appointmentDetailsWrapper}>
          <label className={detailsTitle}>{is_subcription ? 'Subscription' : 'Appointment'} Details</label>
          {
            type == 'Patient' ? null :
              <h4>{notif.sender.type == 'admin' ? <span className={detailsLabel}>Client:</span> : <span className={detailsLabel}>Patient name: </span>} <span className={details}>{notif.recipient.patientName}</span></h4>
          }

          {
            is_subcription ? null : <h4><span className={detailsLabel}>Date and Time:</span> <span className={details}>{moment(notif.recipient.appointmentDate).format('MMMM DD YYYY hh:mm A')}</span></h4>
          }

          {
            is_subcription ? null : <h4 className={healthConcern}>{notif.sender.type == 'admin' ? <small>Message:</small> : <small>Health Concern:</small>}</h4>
          }

          <p>{notif.message}</p>
        </div>
      </section>
    )
  }
}

let notifFrom = css({
  margin: '10px 0px',
  '> small': {
    background: 'whitesmoke',
    padding: '5px 10px',
    display: 'inline-block',
  }
})

let detailsLabel = css({
  fontSize: 15,
  textTransform: 'uppercase'
})

let detailsTitle = css({
  margin: '-15px -15px 0',
  display: 'block',
  padding: '5px 15px',
  background: '#80cde9',
  textTransform: 'uppercase',
  color: '#fff'
})

let details = css({
  fontWeight: 500
})

let appointmentDetailsWrapper = css({
  padding: 15,
  background: '#fff',
  overflow: 'hidden',
  borderRadius: 3,
  marginTop: 10,
  boxShadow: '0 5px 10px rgba(0,0,0,.05)',
  '> h4': {
    fontSize: 18
  },
  '> div span': {
    display: 'inline-block',
    minWidth: 135
  },
  '> p': {
    textAlign: 'justify',
    padding: '10px 5px'
  }
})

let healthConcern = css({
  margin: 0,
  marginTop: 15,
  color: '#d383b6'
})

let appNotifWrapper = css({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'left',
  justifyContent: 'center',
  maxWidth: 700,
  width: '100%',
  '> *': {
    flexWrap: 'wrap',
  },
  width: '100%'
})