import React from "react";
import Link from "next/link";

import Form from "../Form";

import AnswerService from "../../utils/answerService.js";

const answer = new AnswerService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      goalOne_Cat: {
        id: "",
        name: "goalOne_Cat",
        question: (
          <span>
            What is your <strong>First</strong> Goal?
          </span>
        ),
        type: "select",
        validation: "select",
        options: [
          { label: "Weight", value: "weight" },
          { label: "Nutrition", value: "nutrition" },
          { label: "Detox", value: "detox" },
          { label: "Sleep", value: "sleep" },
          { label: "Movement", value: "movement" },
          { label: "Stress", value: "stress" },
          { label: "Others/Chronic Illness", value: "other" }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      goalTwo_Cat: {
        id: "",
        name: "goalTwo_Cat",
        question: (
          <span>
            What is your <strong>Second</strong> Goal?
          </span>
        ),
        type: "select",
        validation: "select",
        options: [
          { label: "Weight", value: "weight" },
          { label: "Nutrition", value: "nutrition" },
          { label: "Detox", value: "detox" },
          { label: "Sleep", value: "sleep" },
          { label: "Movement", value: "movement" },
          { label: "Stress", value: "stress" },
          { label: "Others/Chronic Illness", value: "other" }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      goalThree_Cat: {
        id: "",
        name: "goalThree_Cat",
        question: (
          <span>
            What is your <strong>Third</strong> Goal?
          </span>
        ),
        type: "select",
        validation: "select",
        options: [
          { label: "Weight", value: "weight" },
          { label: "Nutrition", value: "nutrition" },
          { label: "Detox", value: "detox" },
          { label: "Sleep", value: "sleep" },
          { label: "Movement", value: "movement" },
          { label: "Stress", value: "stress" },
          { label: "Others/Chronic Illness", value: "other" }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      goalOne: {
        id: 133,
        name: "goalOne",
        label: "Describe health goal or concern",
        question: "Enter your health concern",
        type: "required",
        validation: "required",
        options: [],
        error: {
          active: false,
          message: "Please enter a value for the goal"
        },
        value: ""
      },
      goalTwo: {
        id: 133,
        name: "goalTwo",
        label: "Describe your health concern",
        question: "Enter your health concern",
        type: "required",
        validation: "required",
        options: [],
        error: {
          active: false,
          message: "Please enter a value for the goal"
        },
        value: ""
      },
      goalThree: {
        id: 133,
        name: "goalThree",
        label: "Describe your health concern",
        question: "Enter your health concern",
        type: "required",
        validation: "required",
        options: [],
        error: {
          active: false,
          message: "Please enter a value for the goal"
        },
        value: ""
      },
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      }
    };

    this._handleChange = this._handleChange.bind(this);
    this._goToNext = this._goToNext.bind(this);
  }

  supportsFlexBox() {
    var test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  componentDidMount() {
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      flexibility(document.body);
    }
    this._setDefaultValues();
  }

  _setDefaultValues() {
    const {
      goalOne,
      goalTwo,
      goalThree,
      goalOne_Cat,
      goalTwo_Cat,
      goalThree_Cat
    } = this.state;

    goalOne.value = this.props.survey.goalOne;
    goalTwo.value = this.props.survey.goalTwo;
    goalThree.value = this.props.survey.goalThree;
    goalOne_Cat.value = this.props.survey.goalOne_Cat;
    goalTwo_Cat.value = this.props.survey.goalTwo_Cat;
    goalThree_Cat.value = this.props.survey.goalThree_Cat;

    this.setState({
      goalOne,
      goalTwo,
      goalThree,
      goalOne_Cat,
      goalTwo_Cat,
      goalThree_Cat
    });
  }

  render() {
    let InitialForms = [
      this.state.goalOne_Cat,
      this.state.goalOne,
      this.state.goalTwo_Cat,
      this.state.goalTwo,
      this.state.goalThree_Cat,
      this.state.goalThree
    ];
    let forms = [
      this.state.goalOne_Cat,
      this.state.goalOne,
      this.state.goalTwo_Cat,
      this.state.goalTwo,
      this.state.goalThree_Cat,
      this.state.goalThree
    ];

    return (
      <div style={styles.container}>
        <div style={styles.introInfo}>
          <h1 style={{ marginTop: 0 }}>Health Goals</h1>
          <span>
            We would like to know your health goals so we can set our priorities
            in drafting your health plan
            <br />
            and at the same time measure the effectiveness of our intervention
            based on your desired results.
          </span>
        </div>
        <Form
          from="health-survey"
          healthGoals
          submitForm={this._goToNext}
          buttonText={
            this.props.location == "profile"
              ? "Update My Profile"
              : "Continue My Profile"
          }
          handleChange={this._handleChange}
          inputs={InitialForms}
          secondaryOption=""
          formError={this.state.formError}
          formSuccess={this.state.formSuccess}
          back={this.props.back}
        />
      </div>
    );
  }

  _goToNext() {
    this.setState({
      formError: {
        active: false,
        message: ""
      }
    });

    let goal1 = this.state.goalOne;
    let goal2 = this.state.goalTwo;
    let goal3 = this.state.goalThree;
    goal1.value = this.state.goalOne_Cat.value + "_" + goal1.value;
    goal2.value = this.state.goalTwo_Cat.value + "_" + goal2.value;
    goal3.value = this.state.goalThree_Cat.value + "_" + goal3.value;
    var inputs = [goal1, goal2, goal3];
    console.log("inputs");
    console.log(inputs);
    //  return

    answer
      .create(inputs)
      .then(res => {
        if (res) {
          console.log("update user profile");
          this.setState({
            formSuccess: {
              active: true,
              message: "Your Profile Was Updated"
            }
          });
          this.props.back();
          //return this.props.onClick('post')
        } else {
          this.setState({
            formError: {
              active: true,
              message: "There was an error updating your profile"
            }
          });
        }
      })
      .catch(e => {
        this.setState({
          formError: {
            active: true,
            message: e.error
          }
        });
      }); // you would show/hide error messages with component state here
  }

  _handleChange(value, input, validated) {
    const inputName = input;
    var newInput = this.state[inputName];
    newInput.value = value;
    newInput.error.active = validated;

    this.setState({
      inputName: newInput
    });
  }
}

const styles = {
  container: {
    width: "100%",
    fontFamily: '"Roboto",sans-serif',
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden"
  },
  introInfo: {
    flex: "1 0 100%",
    fontFamily: '"Roboto",sans-serif',
    color: "#364563",
    fontSize: "14px",
    padding: "10px 20px",
    textAlign: "center"
  },
  formContainer: {
    border: "2px solid rgba(151,151,151,0.2)",
    borderRadius: "6px",
    maxWidth: "400px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.20)"
  },
  header: {
    borderBottom: "2px solid rgba(225,232,238,1)",
    padding: "15px 20px",
    fontSize: "18px",
    color: "#364563",
    fontFamily: '"Roboto-Medium",sans-serif',
    width: "360px"
  },
  form: {
    margin: "10px 30px 20px 20px"
  },
  formInput: {
    padding: "13px 30px 13px 30px",
    marginTop: "20px",
    width: "287px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    fontSize: "14px"
  },
  options: {
    marginTop: "15px",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif',
    padding: "13px 30px 13px 30px",
    WebkitAppearance: "none",
    MozAppearance: "none",
    appearance: "none",
    width: "350px",
    backgroundColor: "#fff",
    borderColor: "#d9d9d9 #ccc #b3b3b3",
    borderradius: "4px",
    border: "2px solid #E1E8EE",
    cursor: "default",
    borderSpacing: "0",
    borderCollapse: "separate",
    modifyDiet: "50px",
    outline: "none",
    overflow: "hidden",
    position: "relative",
    cursor: "pointer"
  },
  optionsDefault: {
    color: "rgba(169, 169, 169, 1)"
  },
  selectArrowZone: {
    cursor: "pointer",
    display: "table-cell",
    position: "relative",
    textAlign: "center",
    verticalAlign: "middle",
    width: "25px",
    paddingRight: "5px"
  },
  selectArrow: {
    borderColor: "#999 transparent transparent",
    borderStyle: "solid",
    borderWidth: "5px 5px 2.5px",
    display: "inline-block",
    modifyDiet: "0",
    width: "0",
    position: "absolute",
    right: "-18em",
    top: "-1.75em"
  },
  button: {
    backgroundColor: "#ED81B9",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "0px 0px 20px 0px",
    cursor: "pointer"
  },
  register: {
    textDecoration: "none",
    margin: "20px 10px",
    color: "#43484D",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif'
  }
};
