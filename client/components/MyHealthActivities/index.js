import React from "react";
import { css } from "glamor";
import Router, { withRouter } from "next/router";
import UserService from "../../utils/userService";
import BookAppointment from "../../components/Appointments/bookAppointment";
import HealthPrograms from "../../components/HealthPrograms";
import ActivityHistory from "../ActivityHistory";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import MenuButtons from "../NewSkin/Components/MenuButtons";
import OptimumHealth from "../NewSkin/Components/OptimumHealth";

const userService = new UserService();

class MyHealthActivities extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.user = this.props.user;
    this.state = {
      settingData: true,
      //appointmentRequest: '',
      doctors: "",
      activeView: this.props.router.query.tab || "appointment",
      profile: ""
    };
    this._renderView = this._renderView.bind(this);
    this._getDoctors = this._getDoctors.bind(this);
    this._getProfile = this._getProfile.bind(this);
    this.updateAppoitment = this.updateAppoitment.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this._isMounted) {
      this._getProfile();
      this.setState({
        activeView: nextProps.router.query.tab || "appointment"
      });
    }
  }

  componentDidMount() {
    this._isMounted = true;
    this._getProfile();
    //  this._getAppointmentRequest()
    this._getDoctors();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  // _updateAppointmentRequestList(){
  //     this._getAppointmentRequest()
  // }

  // _getAppointmentRequest(){
  //       appointment.getAppointmentsList()
  //       .then(res => {
  //           if (res.name === 'Error' || res.message === 'Unauthorized') {
  //               console.log("ERROR in _getAppointmentRequest");
  //               console.log(res);
  //           } else {
  //             this.setState({
  //                 appointmentRequest: res
  //             })
  //           }
  //       })
  //       .catch((e) => {
  //         console.log(e);
  //           console.log("ERROR in _getAppointmentRequestBBBB");
  //       });  // you would show/hide error messages with component state here
  //   }

  _getDoctors() {
    const { company_id } = this.user;
    userService
      .getDoctors(company_id)
      .then(res => {
        if (this._isMounted) {
          this.setState({
            doctors: res
          });
        }
      })
      .catch(e => {
        console.log("GET DOCTORS ERROR");
        console.log(e);
      });
  }

  // GET UPDATED profile info
  _getProfile() {
    userService
      .getLatest()
      .then(res => {
        if (this._isMounted) {
          this.setState({
            profile: res
          });
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _headMenu = () => {
    const baseLink = "/activities";
    const titles = [
      { value: "BOOK APPOINTMENT", name: "appointment" },
      { value: "MY HEALTH PROGRAMS", name: "healthprograms" },
      { value: "ACTIVITY HISTORY", name: "activehistory" }
    ];

    return (
      <MenuButtons
        activeView={this.state.activeView}
        user={this.user}
        titles={titles}
        baseLink={baseLink}
        contain={false}
      />
    );
  };

  _stateLoaded() {
    return !!(
      this.state.doctors &&
      this.state.profile &&
      this.state.settingData
    );
  }

  updateAppoitment() {
    console.log("UPDATE appointmentRequest");
  }

  _renderView() {
    const { activeView } = this.state;
    if (activeView === "appointment") {
      if (this._stateLoaded()) {
        return (
          <BookAppointment
            //  appointmentRequest={this.state.appointmentRequest}
            doctors={this.state.doctors}
            profile={this.state.profile}
            updateView={this.props.updateView}
            //updateAppoitment={this._updateAppointmentRequestList}
            updateRoot={this.props.updateroot}
            appointmentRequestsFocus={(this.props.router.asPath).indexOf("#") >= 0}
          />
        );
      }
      return (
        <div className={warning}>
          <span>Loading...</span>
        </div>
      );
    } else if (activeView === "healthprograms") {
      return <HealthPrograms user={this.user} />;
    } else if (activeView === "activehistory") {
      return <ActivityHistory user={this.user} />;
    }
  }

  render() {
    return (
      <div className="wrap">
        <OptimumHealth />
        <GridContainer
          contain
          styles={{
            minHeight: "82vh",
            alignContent: "start",
            marginBottom: 50
          }}
          gap={20}
        >
          {this._headMenu()}
          {this._renderView()}
        </GridContainer>
      </div>
    );
  }
}

export default withRouter(MyHealthActivities);

// styling
let warning = css({
  display: "flex",
  flex: "1 0 100%",

  justifyContent: "center",
  marginTop: 20,
  //alignItems: 'center',
  color: "#d383b6",
  textTransform: "uppercase",
  fontWeight: "bold"
});

const item = css({
  textAlign: "center",
  margin: "3px 0",
  padding: "3px 15px",
  textTransform: "uppercase",
  cursor: "pointer",
  color: "#000",
  fontWeight: "bold",
  letterSpacing: "1.5px",
  border: "1px solid transparent",
  borderRight: "1.5px dotted gray",
  ":last-child": { border: "none" },
  ":hover": {
    border: "1px solid",
    borderRadius: 10
  }
});

const Activeitem = css({
  textAlign: "center",
  margin: "3px 0",
  padding: "3px 15px",
  textTransform: "uppercase",
  color: "#80cde9",
  fontWeight: "bold",
  letterSpacing: "1.5px",
  borderRight: "1.5px dotted gray",
  ":last-child": { border: "none" }
});

let wrapperDiv = css({
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center"
});
