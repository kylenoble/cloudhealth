/*
props:
width
height
percent
score
*/

import React, { Component } from "react";
import { css } from "glamor";
import Colors from "../NewSkin/Colors";

class Progress extends Component {
  constructor(props) {
    super(props);
    this.radius = this.props.height / 2;
    this.totalCircumference = this.radius * Math.PI * 2;
    this.state = {
      rColor: "",
      score: this.props.score
    };
  }

  changeProgressColor = () => {
    const title = this.props.title;

    switch (title) {
      case "Weight":
        return Colors.weightColor;
      case "Nutrition":
        return Colors.nutritionColor;
      case "Sleep":
        return Colors.sleepColor;
      case "Detox":
        return Colors.detoxColor;
      case "Stress":
        return Colors.stressColor;
      case "Movement":
        return Colors.movementColor;

      default:
        return Colors.skyblue;
    }
  };

  componentDidMount() {
    // let color;
    // const status = this.props.status;
    // if (status === "Optimal") {
    //   color = "#83b85f";
    // } else if (status === "Suboptimal") {
    //   color = "#83cde7";
    // } else if (status === "Compromised") {
    //   color = "#f6921e";
    // } else if (status === "Alarming") {
    //   color = "#ec1c24";
    // } else {
    //   color = "#ffc82f";
    // }

    // this.setState({
    //   rColor: color
    // });

    this.setState({
      rColor: this.changeProgressColor()
    });

    this.svgLoaded();
  }

  svgLoaded = () => {
    this.props.animate;
  };

  _calculateFillPercent() {
    const percentOfDegrees =
      this.totalCircumference -
      (this.props.percent / 100) * (this.totalCircumference - this.radius);
    return percentOfDegrees;
  }

  _calculatePointRotation() {
    if (this.props.percent === 0) {
      return `rotate(-105 ${this.radius} ${this.radius})`;
    } else if (this.props.percent === 100) {
      return `rotate(205 ${this.radius} ${this.radius})`;
    }
    const degree = this.props.percent * 3.3 + -115;
    return `rotate(${degree} ${this.radius} ${this.radius})`;
  }

  render() {
    const { score } = this.state;
    return (
      <svg
        width={this.props.width}
        height={this.props.height}
        viewBox={`0 0 ${this.props.height} ${this.props.width}`}
      >
        <defs>
          <linearGradient
            id={`gradient${this.state.rColor}`}
            gradientUnits="userSpaceOnUse"
            x1="60%"
            y1="100%"
            x2="0%"
            y2="60%"
          >
            {" "}
            gradientTransform="matrix(1 0 0 -1 -761.14 398.97)">
            <stop offset="3%" stopColor="#83b85f" />
            <stop offset="97%" stopColor={this.state.rColor} />
          </linearGradient>
        </defs>
        }
        <circle
          cx={this.radius}
          cy={this.radius}
          r={this.radius - this.radius * 0.12}
          fill="none"
          stroke="#e6e6e6"
          //stroke={"green"}
          strokeWidth={this.props.height * 0.12}
          strokeDasharray={this.totalCircumference}
          strokeDashoffset={this.radius}
          transform={`rotate(100 ${this.radius} ${this.radius})`}
        />
        <circle
          cx={this.radius}
          cy={this.radius}
          r={this.radius - this.radius * 0.12}
          fill="none"
          // stroke={`url("#gradient${this.state.rColor}")`}
          stroke={
            this.props.title === "Health Risk Score"
              ? `url("#gradient${this.state.rColor}")`
              : this.state.rColor
          }
          strokeWidth={this.props.height * 0.12}
          strokeDasharray={this.totalCircumference}
          strokeDashoffset={this._calculateFillPercent()}
          transform={`rotate(100 ${this.radius} ${this.radius})`}
          style={{
            transition:
              "stroke-dashoffset 1s ease 0s, stroke-dasharray 1s ease 0s, stroke 1s"
          }}
        />
        <circle
          cx={this.radius}
          cy={this.radius}
          r={this.radius - this.radius * 0.05}
          fill="none"
          strokeWidth={this.props.height * 0.02}
          strokeDasharray={this.totalCircumference}
        />
        {this.props.withTitle ? (
          this.props.title === "Health Risk Score" ? (
            <text
              x={`${this.radius}`}
              y={65}
              fontFamily='"Montserrat", Arial'
              fontSize={(16 / 80) * this.radius}
              fill="rgb(54, 69, 99)"
              textAnchor="middle"
            >
              <tspan dy="1.2em" x="60">
                Health Risk
              </tspan>
              <tspan dy="1.2em" x="60">
                Score
              </tspan>
            </text>
          ) : (
              <text
                x={`${this.radius}`}
                y={`${this.radius}`}
                fontFamily='"Montserrat", Arial'
                fontSize={(16 / 80) * this.radius}
                fill="rgb(54, 69, 99)"
                textAnchor="middle"
              >
                {this.props.title}
              </text>
            )
        ) : null}
        <text
          x={`${this.radius}`}
          y={`${
            this.props.withTitle
              ? this.props.title === "Health Risk Score"
                ? 60
                : this.radius + this.radius / 3.5
              : this.props.tracker ? 75 : 55
            }`}
          fontFamily='"Montserrat", Arial'
          fontSize={`${this.props.withTitle ? (40 / 80) * this.radius : 24}`}
          fill="rgb(54, 69, 99)"
          textAnchor="middle"
        >
          {this.props.score === "Incomplete" ? "0" : this.props.score}
        </text>
      </svg>
    );
  }
}

const circle = css({
  transition: "stroke .8s ease"
});

export default Progress;
