import React from "react";
import Link from "next/link";
import { css } from "glamor";

import AnswerService from "../../utils/answerService.js";
import SummaryService from "../../utils/summaryService.js";
import GenerateSummary from "../../utils/generateSummary.js";
import GridContainer from "../NewSkin/Wrappers/GridContainer.js";
import Button from "../NewSkin/Components/Button.js";
import Colors from "../NewSkin/Colors.js";

const answer = new AnswerService();
const summary = new SummaryService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      prescriptionDrugs: {
        id: 54,
        name: "prescriptionDrugs",
        label: "How many prescription drugs are you presently taking?",
        options: [
          { label: "0", value: 0 },
          { label: "1", value: 1 },
          { label: "2", value: 2 },
          { label: "3", value: 3 },
          { label: "4", value: 4 },
          { label: "5", value: 5 },
          { label: ">5", value: "More Than 5" }
        ],
        error: {
          active: false
        },
        value: "default"
      },
      prescriptionScenarios: {
        id: 55,
        name: "prescriptionScenarios",
        label:
          "Which of the following scenarios best represent your response to prescription medications?",
        options: [
          {
            label:
              "Experience side effects, drug(s) is (are) efficacious at lowered dose(s)",
            value: "side effects, efficacious at lowered dose(s)"
          },
          {
            label:
              "Experience side effects, drug(s) is (are) efficacious at usual dose(s)",
            value: "side effects, efficacious at usual dose(s)"
          },
          {
            label:
              "Experience no side effects, drug(s) is (are) usually not efficacious",
            value: "no side effects, usually not efficacious"
          },
          {
            label:
              "Experience no side effects, drug(s) is (are) usually efficacious",
            value: "no side effects, usually efficacious"
          }
        ],
        error: {
          active: false
        },
        value: "default"
      },
      caffeine: {
        id: 57,
        name: "caffeine",
        label:
          "Do you have strong negative reactions to caffeine or caffeine containing products?",
        options: [
          { label: "Yes", value: "yes" },
          { label: "No", value: "no" },
          { label: "Don't Know", value: "don't know" }
        ],
        error: {
          active: false
        },
        value: "default"
      },
      illAlcohol: {
        id: 58,
        name: "illAlcohol",
        label:
          "Do you feel ill after you consume even small amounts of alcohol? ",
        options: [
          { label: "Yes", value: "yes" },
          { label: "No", value: "no" },
          { label: "Don't Know", value: "don't know" }
        ],
        error: {
          active: false
        },
        value: "default"
      },
      overTheCounter: {
        id: 59,
        name: "overTheCounter",
        label:
          "Are you taking one or more of the following over-the-counter medications?",
        options: [
          {
            label: "Acetaminophen/ Paracetamol or other pain medications",
            value: "Acetaminophen/ Paracetamol or other pain medications"
          },
          {
            label: "Oral contraceptive pills or use of Hormones eg Estradiol",
            value: "Oral contraceptive pills or use of Hormones eg Estradiol"
          },
          { label: "No", value: "no" }
        ],
        error: {
          active: false
        },
        value: ""
      },
      tobacco: {
        id: 60,
        name: "tobacco",
        label:
          "Do you currently use or within the last 6 months had you regularly used tobacco products (cigars, cigarettes)",
        options: [{ label: "Yes", value: "yes" }, { label: "No", value: "no" }],
        error: {
          active: false
        },
        value: "default"
      },
      odors: {
        id: 61,
        name: "odors",
        label:
          "Do you develop symptoms on exposure to fragrances, exhaust fumes or strong odors?",
        options: [
          { label: "Yes", value: "yes" },
          { label: "No", value: "no" },
          { label: "Don't Know", value: "don't know" }
        ],
        error: {
          active: false
        },
        value: "default"
      },
      dependent: {
        id: 62,
        name: "dependent",
        label: "Are you alcohol or chemical dependent?",
        options: [{ label: "Yes", value: "yes" }, { label: "No", value: "no" }],
        error: {
          active: false
        },
        value: "default"
      },
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      },
      prescriptionCount: {
        id: 56,
        name: "prescriptionCount",
        value: 0,
        error: {
          active: false
        }
      }
    };

    this._handleClick = this._handleClick.bind(this);
  }

  supportsFlexBox() {
    var test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  componentDidMount() {
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      flexibility(document.body);
    }

    this._setDefaultValues();
  }

  _setDefaultValues() {
    const {
      prescriptionDrugs,
      prescriptionScenarios,
      caffeine,
      illAlcohol,
      overTheCounter,
      tobacco,
      odors,
      dependent,
      prescriptionCount
    } = this.state;

    prescriptionDrugs.value =
      this.props.survey.prescriptionDrugs || prescriptionDrugs.value;
    prescriptionScenarios.value = this.props.survey.prescriptionScenarios;
    caffeine.value = this.props.survey.caffeine;
    illAlcohol.value = this.props.survey.illAlcohol;
    overTheCounter.value = this.props.survey.overTheCounter;
    tobacco.value = this.props.survey.tobacco;
    odors.value = this.props.survey.odors;
    dependent.value = this.props.survey.dependent;
    prescriptionCount.value = this.props.survey.prescriptionCount;

    this.setState({
      prescriptionDrugs,
      prescriptionScenarios,
      caffeine,
      illAlcohol,
      overTheCounter,
      tobacco,
      odors,
      dependent,
      prescriptionCount
    });
  }

  render() {
    return (
      <div className={maindiv}>
        <div style={styles.container}>
          <div style={styles.introInfo}>
            <h1 style={{ marginTop: 0 }}>Detoxification</h1>
            <span>
              The following questions will assess your risk for any imbalances
              and will give us a better picture of how your body processes all
              <br />
              the toxins and wastes in your body. Answers to these questions
              will help us draft the best health plan for you.
            </span>
          </div>
          <div style={styles.questionContainer}>
            {this._displayInputs([
              this.state.prescriptionDrugs,
              this.state.prescriptionScenarios,
              this.state.caffeine,
              this.state.illAlcohol,
              this.state.overTheCounter,
              this.state.tobacco,
              this.state.odors,
              this.state.dependent
            ])}
          </div>

          <React.Fragment>
            <div
              style={
                this.state.formError.active
                  ? { ...styles.formError, ...styles.formErrorActive }
                  : styles.formError
              }
            >
              {this.state.formError.message}&nbsp;
            </div>
            <div
              style={
                this.state.formSuccess.active
                  ? { ...styles.formSuccess, ...styles.formSuccessActive }
                  : styles.formSuccess
              }
            >
              {this.state.formSuccess.message}&nbsp;
            </div>
            <GridContainer
              columnSize="1fr 1fr"
              gap={20}
              styles={{ margin: "20px auto", maxWidth: 500 }}
            >
              <Button
                styles={{
                  margin: "20px 20px 0",
                  minWidth: "auto",
                  width: 179
                }}
              >
                <button onClick={() => this.props.back()}>Back</button>
              </Button>
              <Button
                type="pink"
                styles={{
                  margin: "20px 20px 0",
                  minWidth: "auto",
                  maxWidth: 230
                }}
              >
                <button
                  style={styles.button}
                  onClick={this._goToNext.bind(this)}
                >
                  Continue My Profile
                </button>
              </Button>
            </GridContainer>
          </React.Fragment>
        </div>
      </div>
    );
  }

  _displayInputs(inputs) {
    var html = [];
    let i = 0;
    for (var input of inputs) {
      html.push(this._displaySelect(input, i));
      i++;
    }
    return html;
  }

  _displaySelect(input, index) {
    const template = wrapper => {
      if (wrapper === "row") {
        if (
          input.name === "prescriptionScenarios" ||
          input.name === "overTheCounter"
        ) {
          return "1fr";
        } else {
          return "1fr 1fr";
        }
      } else {
        if (input.name === "prescriptionDrugs") {
          return "repeat(7, 1fr)";
        } else if (
          input.name === "prescriptionScenarios" ||
          input.name === "overTheCounter"
        ) {
          return "1fr";
        } else if (
          input.name === "caffeine" ||
          input.name === "illAlcohol" ||
          input.name === "tobacco" ||
          input.name === "odors" ||
          input.name === "dependent"
        ) {
          return "1fr 1fr 2fr";
        }

        return "1fr 1fr 1fr";
      }
    };

    const width = () => {
      if (input.name === "prescriptionDrugs") {
        return "70%";
      } else if (
        input.name === "prescriptionScenarios" ||
        input.name === "overTheCounter"
      ) {
        return 670;
      }

      return "100%";
    };

    return (
      <div data-name="select-wrapper" key={index} style={styles.inputContainer}>
        <GridContainer
          columnSize={template("row")}
          styles={{
            width: "100%",
            textAlign: "left",
            justifyContent: "left",
            borderBottom: "1px solid #ccc3",
            padding: "20px 0"
          }}
        >
          <span
            className={questionWrapper}
            style={{
              color: input.error.active
                ? Colors.movementColor
                : Colors.blueDarkAccent
            }}
          >
            {input.label}
          </span>
          <div style={styles.optionsContainer}>
            <GridContainer
              columnSize={template("column")}
              styles={{ width: width(), margin: "0 auto" }}
            >
              {this._displayOptions(input)}
            </GridContainer>
          </div>
          <span
            style={
              input.error.active
                ? { ...styles.validationText, ...styles.validationTextVisible }
                : styles.validationText
            }
          >
            {input.error.message}
          </span>
        </GridContainer>
      </div>
    );
  }

  _styleSelect(input) {
    if (input.error.active === true) {
      return {
        ...styles.options,
        ...styles.optionsDefault,
        ...styles.optionsError
      };
    } else if (input.value === "default") {
      return { ...styles.options, ...styles.optionsDefault };
    } else {
      return { ...styles.options };
    }
  }

  _options = (input, index, type) => {
    const options = input.options;

    const checkType = () => {
      if (type === "default" && input.name === "prescriptionScenarios") {
        return sqrDefaultOption;
      } else if (
        type === "selected" &&
        input.name === "prescriptionScenarios"
      ) {
        return sqrSelectedOption;
      } else if (type === "default" && input.name === "overTheCounter") {
        return sqrDefaultOption;
      } else if (type === "selected" && input.name === "overTheCounter") {
        return sqrSelectedOption;
      }

      if (type === "selected") {
        return selectedOptionStyles;
      } else if (type === "default") {
        return selectStyleOption;
      }
    };

    const template = () => {
      if (input.name === "prescriptionDrugs") {
        return "1fr";
      } else if (
        input.name === "prescriptionScenarios" ||
        input.name === "overTheCounter"
      ) {
        return "40px 10fr";
      } else if (
        input.name === "caffeine" ||
        input.name === "illAlcohol" ||
        input.name === "tobacco" ||
        input.name === "odors" ||
        input.name === "dependent"
      ) {
        return "40px 1fr";
      }

      return "1fr 1fr";
    };

    return (
      <GridContainer
        key={`${index}_${input.name}`}
        columnSize={template()}
        styles={{ textAlign: "left !important" }}
      >
        {input.name !== "prescriptionDrugs" && (
          <button
            data-name={type}
            className={checkType()}
            onClick={this._handleClick.bind(this)}
            id={input.name}
            data-value={options[index].label}
          />
        )}

        <span
          data-id="checkbox-label"
          data-name={type}
          key={index}
          style={{
            textAlign: input.name !== "prescriptionDrugs" ? "left" : "center",
            color: type === "selected" ? Colors.skyblue : '#777'
          }}
        >
          {options[index].label}
        </span>

        {input.name === "prescriptionDrugs" && (
          <button
            data-name={type}
            className={checkType()}
            onClick={this._handleClick.bind(this)}
            id={input.name}
            data-value={options[index].value}
          />
        )}
      </GridContainer>
    );
  };

  _displayOptions(input) {
    var html = [];
    let options = input.options;
    for (var i = 0; i < options.length; i++) {
      if (input.name === "prescriptionDrugs") {
        console.log("XXX", options[i].value.toString(), input.value.toString());
        if (options[i].value == input.value) {
          html.push(this._options(input, i, "selected"));
        } else {
          html.push(this._options(input, i, "default"));
        }
      } else {
        if (options[i].label === input.value) {
          html.push(this._options(input, i, "selected"));
        } else if (input.value.split(",").indexOf(options[i].label) > -1) {
          html.push(this._options(input, i, "selected"));
        } else {
          html.push(this._options(input, i, "default"));
        }
      }
    }

    return html;
  }

  _handleClick(e) {
    let value = e.target.dataset.value;
    let name = e.target.id;

    var validated = this._validateInput(value, "select");
    // //if validated is true, error.active should be false and vice versa
    this._updateChange(value, name, !validated);
  }

  _updateChange(value, input, validated) {
    const inputName = input;
    var newInput = this.state[inputName];
    if (input === "overTheCounter") {
      var prescrCount = this.state.prescriptionCount;

      if (newInput.value != "" && value != "No") {
        let overTheCounter = newInput.value.split(",");

        if (newInput.value == "No") {
          newInput.value = value;
        } else {
          if (overTheCounter.indexOf(value) == -1) {
            overTheCounter.push(value);
          } else {
            const location = overTheCounter.indexOf(value);
            overTheCounter.splice(location, 1);
          }
          newInput.value = overTheCounter.join(",");
        }
        prescrCount.value = overTheCounter.length;
      } else {
        if (newInput.value == "No") {
          newInput.value = "";
        } else {
          newInput.value = value;
        }
        prescrCount.value = value == "No" ? 0 : 1;
      }
    } else {
      newInput.value = value;
    }

    newInput.error.active = validated;
    this.setState({
      inputName: newInput
    });
  }

  _validateInput(input, type) {
    var ck_required = /^[a-zA-Z\d]/;
    if (input === "default" || !ck_required.test(input)) {
      return false;
    }
    return true;
  }

  _allInputsValid() {
    var results = [];
    for (let key of Object.keys(this.state)) {
      if (
        !this._validateInput(this.state[key].value) &&
        key !== "formError" &&
        key !== "formSuccess"
      ) {
        this.state[key].error.active = true;
        results.push(this.state[key]);
      }
    }

    if (results.length > 0) {
      return false;
    }
    return true;
  }

  _goToNext() {
    if (!this._allInputsValid()) {
      // add form error validation
      this.setState({
        formError: {
          active: true,
          message: "Please complete all fields"
        }
      });
      return;
    } else {
      const answers = [
        this.state.prescriptionDrugs,
        this.state.prescriptionScenarios,
        this.state.caffeine,
        this.state.illAlcohol,
        this.state.overTheCounter,
        this.state.prescriptionCount,
        this.state.tobacco,
        this.state.odors,
        this.state.dependent
      ];

      const summaryGenerator = new GenerateSummary(answers, "detox");
      let detoxSummary = [
        {
          name: "detox",
          group: "health",
          label: "Detox",
          value: summaryGenerator.generateSummary()
        }
      ];

      if (this.props.location === "profile") {
        answer
          .create(answers)
          .then(res => {
            if (res) {
              summary
                .create(detoxSummary)
                .then(result => {
                  console.log("update user profile");
                  this.setState({
                    formSuccess: {
                      active: true,
                      message: "Your Profile Was Updated"
                    }
                  });
                  this.props.back();
                  //  return this.props.onClick('Dashboard')
                })
                .catch(error => {
                  console.log(error);
                  this.setState({
                    formError: {
                      active: true,
                      message: "There was an error updating your profile"
                    }
                  });
                });
            } else {
              this.setState({
                formError: {
                  active: true,
                  message: "There was an error updating your profile"
                }
              });
            }
          })
          .catch(e => {
            if (e.error === "This email is already in use") {
              var newInput = this.state["email"];
              newInput.error.active = true;
              newInput.error.message = e.error;
              this.setState({
                inputName: newInput
              });
            } else {
              this.setState({
                formError: {
                  active: true,
                  message: e.error
                }
              });
            }
          }); // you would show/hide error messages with component state here
      } else {
        answer
          .create(answers)
          .then(res => {
            if (res) {
              console.log("should move on");
              //    this.props.onClick('symptoms')
            } else {
              this.setState({
                formError: {
                  active: true,
                  message: "There was an error submitting your answers"
                }
              });
            }
          })
          .catch(e => {
            this.setState({
              formError: {
                active: true,
                message: e.error
              }
            });
          }); // you would show/hide error messages with component state here
      }
    }
  }
}

const selectStyleOption = css({
  width: 20,
  height: 20,
  borderRadius: "100%",
  border: "2px solid #777",
  backgroundColor: "white",
  display: "inline-block",
  margin: "auto auto",
  transition: "250ms ease",
  cursor: "pointer",
  transform: "scale(1)",
  "> span": {
    opacity: 0
  },
  ":focus": {
    outline: "none"
  },
  ":hover": {
    transform: "scale(1.2)",
    borderColor: Colors.skyblue
  }
});

const sqrDefaultOption = css(selectStyleOption, {
  borderRadius: "0 !important"
});

const sqrSelectedOption = css(selectStyleOption, {
  borderRadius: "0 !important",
  backgroundColor: Colors.skyblue,
  borderColor: Colors.skyblue,
  boxShadow: "0 5px 15px #81cce670"
});

const selectedOptionStyles = css(selectStyleOption, {
  backgroundColor: Colors.skyblue,
  borderColor: Colors.skyblue,
  boxShadow: "0 5px 15px #81cce670"
});

let maindiv = css({
  width: "100%",
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center"
});

let selectOptionStyle = css({
  padding: "10px 20px",
  backgroundColor: "white",
  border: "2px solid rgb(237, 129, 185)",
  color: "rgb(237, 129, 185)",
  borderRadius: "5px",
  margin: "5px",
  fontSize: "12px",
  cursor: "pointer",
  ":hover": {
    backgroundColor: "rgb(237, 129, 185)",
    border: "2px solid rgb(237, 129, 185)",
    color: "white"
  }
});

let selectOptionFilledStyle = css({
  padding: "10px 20px",
  backgroundColor: "rgb(237, 129, 185)",
  border: "2px solid rgb(237, 129, 185)",
  color: "white",
  borderRadius: "5px",
  margin: "5px",
  fontSize: "12px",
  cursor: "pointer"
});

const questionWrapper = css({
  position: "relative",
  color: Colors.blueDarkAccent,
  fontSize: "1rem",
  textAlign: "left !important"
});

const styles = {
  container: {
    maxWidth: "100%",
    fontFamily: '"Roboto",sans-serif',
    JsDisplay: "flex",
    display: "-webkit-box" /* OLD - iOS 6-, Safari 3.1-6 */,
    display: "-moz-box" /* OLD - Firefox 19- (buggy but mostly works) */,
    display: "-ms-flexbox" /* TWEENER - IE 10 */,
    display: "-webkit-flex" /* NEW - Chrome */,
    display: "flex" /* NEW, Spec - Opera 12.1, Firefox 20+ */,
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
    margin: "0 30px 0 20px"
  },
  h1: {
    color: "#364563",
    fontSize: "24px",
    fontFamily: '"Roboto-Medium",sans-serif'
  },
  introInfo: {
    flex: "1 0 100%",
    fontFamily: '"Roboto",sans-serif',
    color: "#364563",
    fontSize: "14px",
    padding: "10px 20px",
    textAlign: "justified",
    marginBottom: 20
  },
  questionContainer: {
    JsDisplay: "flex",
    display: "-webkit-box" /* OLD - iOS 6-, Safari 3.1-6 */,
    display: "-moz-box" /* OLD - Firefox 19- (buggy but mostly works) */,
    display: "-ms-flexbox" /* TWEENER - IE 10 */,
    display: "-webkit-flex" /* NEW - Chrome */,
    display: "flex" /* NEW, Spec - Opera 12.1, Firefox 20+ */,
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
    textAlign: "justified",
    margin: "0 auto"
  },
  optionsContainer: {
    display: "flex",
    flexFlow: "row wrap",
    justifyContent: "left",
    alignItems: "center"
  },
  inputContainer: {
    display: "flex",
    flexFlow: "column wrap",
    justifyContent: "center",
    alignItems: "center",
    width: "100%"
  },
  label: {
    color: Colors.blueDarkAccent,
    fontSize: "1rem",
    fontFamily: '"Roboto-Medium",sans-serif',
    marginBottom: "5px",
    width: "100%",
    textAlign: "left"
  },
  options: {
    marginTop: "15px",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif',
    padding: "13px 30px 13px 30px",
    WebkitAppearance: "none",
    MozAppearance: "none",
    appearance: "none",
    width: "300px",
    backgroundColor: "#fff",
    borderColor: "#d9d9d9 #ccc #b3b3b3",
    borderradius: "4px",
    border: "2px solid #E1E8EE",
    cursor: "default",
    borderSpacing: "0",
    borderCollapse: "separate",
    height: "50px",
    outline: "none",
    overflow: "hidden",
    position: "relative",
    cursor: "pointer"
  },
  selectArrowZone: {
    cursor: "pointer",
    display: "table-cell",
    position: "relative",
    textAlign: "center",
    verticalAlign: "middle",
    width: "25px",
    paddingRight: "5px"
  },
  selectArrow: {
    borderColor: "#999 transparent transparent",
    borderStyle: "solid",
    borderWidth: "5px 5px 2.5px",
    display: "inline-block",
    height: "0",
    width: "0",
    position: "absolute",
    right: "-8em",
    top: "-2em"
  },
  validationText: {
    display: "none",
    fontSize: "0.8em",
    marginLeft: "25px",
    marginTop: "5px",
    color: "#EA3131",
    transition: "color 0.3s ease-in-out"
  },
  validationTextVisible: {
    display: "block"
  },
  formError: {
    // display: 'none',
    fontSize: "1rem",
    marginTop: "25px",
    color: "#EA3131",
    transition: "color 0.3s ease-in-out",
    fontWeight: 500,
    width: "100%"
  },
  formErrorActive: {
    display: "block"
  },
  formSuccess: {
    display: "none",
    fontSize: "0.8em",
    marginLeft: "25px",
    marginTop: "5px",
    color: "rgb(3, 210, 127)",
    transition: "color 0.3s ease-in-out"
  },
  formSuccessActive: {
    display: "block"
  },
  button: {
    backgroundColor: "#36c4f1",
    width: "250px",
    // padding: "11px 60px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    cursor: "pointer",
    display: "block",
    margin: "auto",
    marginTop: "0px",
    marginBottom: "20px"
  }
};
