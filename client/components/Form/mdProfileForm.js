import React, { Component } from "react";
import { css } from "glamor";
import Router from "next/router";
import request from "axios";
import AuthService from "../../utils/authService.js";
import currentDomain from "../../utils/domain";
import Button from "../NewSkin/Components/Button.js";
import GridContainer from "../NewSkin/Wrappers/GridContainer.js";
import { inputClass, changeButton } from "../NewSkin/Styles.js";
import Label from "../NewSkin/Components/Label.js";
import UpdateNotification from "../NewSkin/Components/UpdateNotification.js";
import Colors from "../NewSkin/Colors.js";
import InfoForm from "../NewSkin/Components/InfoForm.js";
const auth = new AuthService();

// api
const AUTH_PASSWORD_URL = `${currentDomain}/api/users/authMDPassword`;
const UPDATE_MD_PROFILE = `${currentDomain}/api/users/updateMDProfile/`;

class MDProfileForm extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    is_authenticated: false,
    is_password_open: false,
    is_authenticating: false,
    is_emailChanged: false,
    is_updated: false,
    user: "",
    editing: false,
    infoName: '',
    companies: ['LifeScience', 'Independent Contractor'],
    currentCompany: '',
    isOtherCompany: false,
    isFromOtherCompany: false,
    isUpdating: false
  };

  componentWillMount() {
    this.setState({
      user: this.props.user
    });
  }

  componentDidMount() {
    this._checkCompany();
  }

  _checkCompany = () => {
    const fromGivenCompanies = this.state.companies.filter(c => c === this.props.user.company);
    if (fromGivenCompanies.length <= 0) {
      const companies = [...this.state.companies, this.props.user.company];
      this.setState({
        isFromOtherCompany: true,
        companies
      });
    }
  }

  onChangeEmail = () => {
    if (this.state.is_password_open === false) {
      this.setState({
        is_password_open: !this.state.is_password_open
      });
    }
  };

  _onCancel = () => {
    if (this.state.is_password_open === true) {
      this.setState({
        is_password_open: !this.state.is_password_open
      });
    }
  };

  _authMDProfile = () => {
    this.setState({
      is_authenticating: true
    });

    const auth = {
      email: this.props.user.email,
      password: this.password.value
    };

    request
      .post(AUTH_PASSWORD_URL, auth)
      .then(res => {
        if (res.data.authenticated) {
          this.setState({
            is_authenticated: true,
          });

          document.querySelector('#Email-save-btn').click();

          setTimeout(() => {
            this.setState({
              is_password_open: !this.state.is_password_open,
              is_authenticating: false,
              is_authenticated: false
            });
          }, 500);
        }
      })
      .catch(err => console.log(err));
  };

  _onSubmit = () => {
    const id = this.props.user.id;
    const user = this.state.user;

    const profileData = {
      name: this.state.Name ? this.state.Name.value : user.name,
      email: this.state.Email ? this.state.Email.value : user.email,
      expertise: this.state.Expertise ? this.state.Expertise.value : user.expertise,
      company: this.state.Company ? this.state.Company.value : user.company
    };

    request
      .put(`${UPDATE_MD_PROFILE}${id}`, profileData)
      .then(res => {
        if (res.data.updated) {
          this.setState({
            is_updated: true
          });

          if(this.state.is_password_open){
            setTimeout(() => {
              this.setState({
                is_updated: false
              });
              this._logout();
            }, 2000);
          }
        }
      });
  };

  _logout(e) {
    // e.preventDefault()
    // yay uncontrolled forms!
    auth
      .logout()
      .then(res => {
        // let data = { user_id: this.user.id, action: 'Logout', details: { email: this.user.email, company: this.user.company, type: this.user.type } }
        // this._saveActivity(data)
        Router.push("/login");
      })
      .catch(e => console.log(e)); // you would show/hide error messages with component state here
  }

  _onEmailChange = e => {
    if (e.target.value !== this.props.user.email) {
      this.setState({
        is_emailChanged: true
      });
    } else {
      this.setState({
        is_emailChanged: false
      });
    }
  };


  _editing = (e, info) => {
    const btn = e.target;
    const btnValue = btn.innerHTML.trim();

    if (btnValue === 'Change') {
      btn.innerHTML = 'Cancel';
      this.setState({
        editing: true,
        infoName: info
      });

    } else {
      btn.innerHTML = 'Change';
      this.setState({
        editing: false,
        infoName: '',
        isOtherCompany: false
      });
    }
  }

  _onChangeCompany = e => {
    const company = e.target.value;

    if (company === 'Other') {
      this.setState({ isOtherCompany: true })
    } else {
      this.setState({
        currentCompany: company,
        isOtherCompany: false
      });
    }
  }

  _onChangeOtherCompany = e => {
    const other = e.target.value;
    const companies = [...this.state.companies, other];

    this.setState({
      currentCompany: other,
      companies
    });
  }

  _onChangeData = (title, data, bool) => {
    if (data.length > 0 && data !== this.state.user[title.toLowerCase()]) {
      if (title === 'Email') {
        this.setState({
          [title]: { value: data, isUpdating: bool },
          is_password_open: true,
        });
      } else {
        this.setState({
          [title]: { value: data, isUpdating: bool },
          is_password_open: false,
        });
      }
    } else {
      this.setState({
        [title]: { value: data, isUpdating: false },
        is_password_open: false,
      });
    }

    setTimeout(() => {
      this._checkIfUpdating();
    }, 100)
  }

  _checkIfUpdating = () => {
    const Name = this.state.Name ? this.state.Name.isUpdating : false;
    const Email = this.state.Email ? this.state.Email.isUpdating : false;
    const Expertise = this.state.Expertise ? this.state.Expertise.isUpdating : false;
    const Company = this.state.Company ? this.state.Company.isUpdating : false;

    if (Name || Email || Expertise || Company) {
      this.setState({
        isUpdating: true
      })
    } else {
      this.setState({
        isUpdating: false
      })
    }
  }

  _cancelled = (title) => {
    if (title === 'Email') {
      this.setState({
        is_password_open: false
      });
    }
  }

  _saveCompany = e => {
    if (this.state.currentCompany === this.state.user.company) {
      this.setState({
        Company: { value: null, isUpdating: false },
        editing: false,
        isOtherCompany: false,
        currentCompany: null
      });
    } else {
      this.setState({
        Company: { value: this.state.currentCompany, isUpdating: true },
        editing: false,
        isOtherCompany: false
      });
    }

    e.target.nextSibling.innerHTML = 'Change'
    setTimeout(() => {
      this._checkIfUpdating();
    }, 100)
  }

  render() {
    const template = '200px 1fr 120px';

    const gridStyles = {
      padding: '20px 0',
      borderBottom: '1px solid #ccc3'
    }

    const { name, email, expertise, company } = this.state.user;

    return (
      <>
        {this.state.is_updated ? (
          <UpdateNotification msg="Update Successful" />
        ) : null}

        <GridContainer fullWidth dataID="profile-wrapper" classes={[wrapper]} styles={{ justifyContent: 'left !important' }}>

          <InfoForm data={name} setData={this._onChangeData} title="Name" cancelled={this._cancelled} />

          <GridContainer dataID="email-wrapper" styles={this.state.is_password_open ? gridStyles : {}}>
            <InfoForm data={email} setData={this._onChangeData} title="Email" cancelled={this._cancelled} />

            {this.state.is_password_open ? (
              <article className={formInputWrapper}>
                <GridContainer dataID="password-wrapper">
                  <Label text={<span>Password: <small style={{ color: pink, fontSize: 10 }}>REQUIRED</small></span>} />

                  <input
                    type="password"
                    required
                    id="password"
                    ref={password => (this.password = password)}
                    defaultValue={""}
                    onChange={e => { }}
                    className={inputClass}
                  />
                </GridContainer>

                <Button type='blue' center>
                  <button
                    disabled={!this.state.Email.isUpdating}
                    onClick={this._authMDProfile}
                  >
                    Change Email
                    </button>
                </Button>

                {this.state.is_authenticating ? (
                  <label htmlFor="">
                    {this.state.is_authenticated ? (
                      <small className={`${notification} ${success}`}>
                        Email changed.
                        </small>
                    ) : (
                        <small className={`${notification} ${incorrect}`}>
                          Incorrect Password!
                        </small>
                      )}
                  </label>
                ) : null}
              </article>
            ) : null}
          </GridContainer>

          <InfoForm data={expertise} setData={this._onChangeData} title="Expertise" cancelled={this._cancelled} />

          <GridContainer dataID="company-wrapper" styles={gridStyles}>
            <GridContainer dataID="company-selector-wrapper" columnSize={(this.state.editing && this.state.infoName === 'company') ? '200px 1fr 100px 100px' : template}>
              <Label text="Company" />

              {
                (this.state.editing && this.state.infoName === 'company') ?
                  <>
                    <select
                      className={inputClass}
                      value={this.state.currentCompany ? this.state.currentCompany : company}
                      onChange={(e) => this._onChangeCompany(e)}
                      ref={company => (this.company = company)}
                    >
                      <option value="" disabled>Select Company</option>
                      <option value="Life Science" >Life Science</option>
                      <option value="Independent Contractor" >Independent Contractor</option>
                      {
                        this.state.isFromOtherCompany &&
                        <option value={company}>{company}</option>
                      }

                      {
                        this.state.isOtherCompany &&
                        <option value={this.state.currentCompany}>{this.state.currentCompany}</option>
                      }
                      <option value="Other">Other</option>
                    </select>
                  </>
                  :
                  <label style={{ color: this.state.currentCompany ? Colors.movementColor : '#000' }}>{this.state.currentCompany ? this.state.currentCompany : company}</label>
              }

              {
                (this.state.editing && this.state.infoName === 'company') &&
                <button onClick={this._saveCompany} className={changeButton}>Save</button>
              }

              <button onClick={(e) => this._editing(e, 'company')} className={changeButton}>Change</button>
            </GridContainer>

            {
              this.state.isOtherCompany &&
              <article style={{ marginTop: 20 }}>
                <input onChange={(e) => this._onChangeOtherCompany(e)} type="text" className={inputClass} placeholder="Type here your company name" />
              </article>
            }
          </GridContainer>

          <Button type={this.state.isUpdating ? 'pink' : 'disabled'} center>
            <button onClick={this.state.isUpdating ? () => this._onSubmit() : () => { }}>Save Changes</button>
          </Button>

        </GridContainer>
      </>
    );
  }
}

export default MDProfileForm;

const wrapper = css({
  '& label': {
    textAlign: 'left !important',
    display: 'grid',
    alignContent: 'center',
    justifyContent: 'left'
  }
})

const blue = "#80cde9";
const pink = "#d484b7";

let updateNotif = css({
  padding: 10,
  display: "block",
  border: `1px solid ${blue}`,
  color: blue,
  borderRadius: 5,
  margin: "20px 0",
  textAlign: "center"
});

let submitBtn = css({
  background: blue,
  color: "white",
  width: "100%",
  ":hover": {
    background: `${blue}90`
  }
});

let changeEmailBtnWrapper = css({
  padding: "10px 0"
});

let incorrect = css({
  color: "red"
});

let success = css({
  color: blue
});

let notification = css({
  padding: 20
});

let grayBG = css({
  background: "#ccc !important",
  ":hover": {
    background: "#ccc !important"
  }
});

let blueBG = css({
  background: blue
});

let saveBtn = css({
  background: blue,
  color: "white",
  ":hover": {
    background: `${blue}99`
  }
});

let cancelBtn = css({
  background: "none",
  color: "#ccc",
  ":hover": {
    color: "black"
  }
});

let changeBtn = css({
  background: "none",
  ":hover": {
    color: pink
  }
});

let emailWrapper = css({
  display: "grid",
  gridTemplateColumns: "5fr 1fr",
  "> input": {
    borderStyle: "none",
    outline: "none",
    padding: "5px 10px",
    width: "100%",
    borderBottom: `1px solid #eee`,
    transition: "250ms ease",
    ":focus": {
      borderBottom: `1px solid ${blue}`
    }
  },
  "> button": {
    borderStyle: "none"
  }
});

let emailInput = css({
  "&:disabled": {
    background: "none"
  }
});

let formInputWrapper = css({
  background: '#eee5',
  padding: 20,
  marginTop: 10,
  borderRadius: 10
});

let formWrapper = css({
  width: "100%",
  padding: "0 10px"
  // margin: '0 auto'
});

let btn = css({
  borderStyle: "none",
  outline: "none",
  cursor: "pointer",
  transition: "250ms ease",
  padding: "5px 10px",
  borderRadius: 3
});
