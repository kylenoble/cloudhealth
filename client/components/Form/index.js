import React from "react";
import moment from "moment-timezone";
import Datetime from "react-datetime";
import { css } from "glamor";
import Button from "../NewSkin/Components/Button";
import { combined, sqrSelectedOption, sqrDefaultOption, changeButton, centered, disabledInputStyle, disabledChangeButton } from "../NewSkin/Styles";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Colors from "../NewSkin/Colors";
import NewLogo from "../NewSkin/Icons/NewLogo";
import Label from "../NewSkin/Components/Label";
import { Tooltip } from '../Commons';

const Willingness = props => {
  const lvls = [];
  for (let i = 1; i <= 10; i++) {
    lvls.push(i);
  }

  const levels = lvls.map((lvl, i) => (
    <span key={i} className={spans}>{lvl}</span>
  ));

  const columnSize = () => {
    let template = "2fr repeat(6, 1fr)";
    if (props.stress) {
      template = "repeat(12, 1fr)";
    } else {
      template = "2fr repeat(6, 1fr)";
    }

    return template;
  };

  return (
    <GridContainer gap={50} columnSize="1fr 1fr" styles={{ width: '100%', marginBottom: 10 }}>
      <div />
      <GridContainer columnSize={columnSize()} styles={{ maxWidth: props.stress ? '' : 500 }}>
        {
          props.stress ?
            <React.Fragment>
              <div style={{ opacity: 0 }}>Low</div>
              {levels}
              <div style={{ opacity: 0 }}>High</div>
            </React.Fragment>
            :
            <React.Fragment>
              <span className={spans} style={{ opacity: 0 }}>Willing</span>
              <span className={spans}>5</span>
              <span className={spans}>4</span>
              <span className={spans}>3</span>
              <span className={spans}>2</span>
              <span className={spans}>1</span>
              <span className={spans} style={{ opacity: 0 }}>Not Willing</span>
            </React.Fragment>
        }
      </GridContainer>
    </GridContainer>
  );
};

const accountFormTemplate = '200px 1fr 120px'

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      btnTxt: this.props.buttonText,
      companies: this.props.companies,
      departments: this.props.departments,
      btnLocked: false,
      showContent: false,
      submitted: false,
      formError: {
        active: false,
        message: ''
      }
    };

    this.isHealthSurvey = false;
    this.isAccount = false;
    this._isMounted = this.props.formError || false;

    this._submitForm = this._submitForm.bind(this);
  }

  render() {
    const formStyles = this.isAccount ? { opacity: this.state.showContent ? 1 : 0 } :
      {
        ...styles.form,
        opacity: this.state.showContent ? 1 : 0
      };

    const loaderStyle = this.isAccount ? loaderWithBg : this.isHealthSurvey ? loaderWithoutBg : this.props.login ? loginLoaded : loader;

    return (
      <React.Fragment>

        <div id="form-container" style={styles.formContainer}>
          {
            !this.state.showContent &&
            <section className={loaderStyle}>
              <NewLogo />
              <br />
              <small style={{ color: '#ccc' }}>Preparing Data...</small>
            </section>
          }
          <div style={formStyles}>
            <div style={styles.header}>
              {
                this.isHealthSurvey ? <span className={healthSurveyHeader}>{this.props.header}</span> : <span>{this.props.header}</span>
              }
            </div>

            <div>
              {this.isHealthSurvey && this.props.readiness && <Willingness />}
              {this._displayInputs()}
              {this.props.table ? this.props.table : ""}
            </div>

            <span
              className={this.props.from === 'login' ? errorMsg_login : errorMsg}
              style={
                this.props.formError.active || (this.isAccount && this.state.formError.active)
                  ? { ...styles.validationText, ...styles.validationTextVisible }
                  : styles.validationText
              }
            >
              {(this.props.cpassword && this.state.formError.active) ? `Passwords must have at least 1 number, 1 Uppercase letter, 1 Lowercase letter, and a minimum of 8 characters` : this.state.formError.message || this.props.formError.message}
            </span>

            <span
              id="success"
              style={
                this.props.formSuccess.active
                  ? { ...styles.formSuccess, ...styles.formSuccessActive }
                  : styles.formSuccess
              }
            >
              {this.props.formSuccess.message}
            </span>

            {this.props.btnWrapper ? (
              <Button type={this.props.btnLocked ? "disabled" : "pink"}>
                <button
                  disabled={this.props.btnLocked}
                  style={
                    this.props.btnLocked ? styles.disabledButton : styles.button
                  }
                  onClick={this._submitForm}
                >
                  {this.props.buttonText}
                </button>
              </Button>
            ) : (
                <React.Fragment>
                  {
                    this.isHealthSurvey && !this.props.weight ?
                      <div style={this.state.formError.active ? { ...styles.formError, ...styles.formErrorActive } : styles.formError}>{this.state.formError.message}&nbsp;</div>
                      : null
                  }
                  <GridContainer columnSize={this.props.back ? '1fr 1fr' : '1fr'} gap={20} styles={{ margin: '20px auto', maxWidth: 500, opacity: this.state.showContent ? 1 : 0 }}>
                    {
                      this.props.back &&
                      <Button
                        styles={{
                          margin: "20px auto 0",
                          minWidth: "auto",
                          width: 179
                        }}
                      ><button onClick={() => this.props.back()}>Back</button></Button>
                    }
                    <Button
                      type={this.props.btnLocked ? "disabled" : "pink"}
                      styles={{
                        margin: this.isAccount ? "auto auto" : "20px auto 0",
                        minWidth: "auto",
                        maxWidth: this.props.buttonText === "Reset" ? 200 : 250,
                        width: this.props.buttonText === "Reset" ? 200 : 250
                      }}
                    >
                      <button
                        id="update-btn"
                        disabled={this.props.btnLocked}
                        style={
                          this.props.btnLocked ? styles.disabledButton : styles.button
                        }
                        onClick={this._submitForm}
                      >
                        {this.props.buttonText}
                      </button>
                    </Button>

                    {
                      this.props.secondButton &&
                      <Button
                        type={this.props.secondButton.type || null}
                        styles={this.props.secondButton.style}
                      >
                        <button onClick={() => this.props.secondButton.action()}>{this.props.secondButton.name}</button>
                      </Button>
                    }
                  </GridContainer>
                </React.Fragment>
              )}

            <section style={{ textAlign: "left" }}>
              {this.props.passwordReset}
              {this.props.secondaryOption}
            </section>
          </div>
        </div>
      </React.Fragment>
    );
  }

  supportsFlexBox() {
    const test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  componentDidMount() {
    this._isMounted = true;
    this.isHealthSurvey = this.props.from === 'health-survey';
    this.isAccount = this.props.account;

    if (this.props.btnLocked) {
      this.state.btnLocked = this.props.btnLocked;
    }
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      console.log("flexbox is not supported");
      flexibility(document.getElementById("__next"));
    }
  }

  _checkValueToShowContent = input => {
    console.log('%c Preparing Data...', 'color: green');

    let timer = () => { };
    if (input.value === 'default' || input.value === '') {
      let i = 1;
      if (!this.state.showContent) {
        timer = setInterval(() => {
          if (input.value === 'default' || input.value === '') {
            if (i >= 7) {
              clearInterval(timer);
              if (this.props.loaded) {
                this.props.loaded();
              }
              this.setState({ showContent: true });
            }
          } else {
            clearInterval(timer);
            if (this.props.loaded) {
              this.props.loaded();
            }
            this.setState({ showContent: true });
          }

          i++;
        }, 1000);
      }
    } else {
      this.setState({ showContent: true });
    }
  }

  _handleKeyPress = (event) => {
    if (this.props.from === 'login' && event.key === 'Enter') {
      this._submitForm(event);
    }
  }

  _displayInputs() {
    const html = [];
    for (let i = 0; i < this.props.inputs.length; i++) {
      const input = this.props.inputs[i];
      if (input.type === "dropdown") {
        html.push(this._displayDropdown(input, i));
      } else if (input.type === "select") {
        html.push(this._displaySelect(input, i));
      } else if (input.type === "date") {
        html.push(this._displayDateInput(input, i));
      } else {
        html.push(this._displayInput(input, i));
      }
    }
    return html;
  }

  _checkInputName = (name, isBtn = false) => {
    switch (name) {
      case 'fullName':
        return isBtn ? true : Colors.blueDarkAccent
      // case 'branch':
      //   return isBtn ? true : Colors.blueDarkAccent
      // case 'department':
      //   return isBtn ? true : Colors.blueDarkAccent
      // case 'birthday':
      //   return isBtn ? true : Colors.blueDarkAccent
      // case 'gender':
      //   return isBtn ? true : Colors.blueDarkAccent
      // default:
      //   return isBtn ? false : '#777'
    }
  }


  _displayInput(input, index) {

    if (!this.state.showContent) {
      this._checkValueToShowContent(input);
    }
    const error_style =
      this.props.from === 'health-survey' && this.props.weight ?
        { ...styles.shortGridError, ...styles.validationText, ...styles.validationTextVisible }
        : { ...styles.validationText, ...styles.validationTextVisible };

    let columnSize = '1fr';
    if (this.isAccount) {
      if (this.props.personalInfo) {
        columnSize = '300px 1fr';
      } else if (this.props.cpassword) {
        columnSize = '300px 1fr';
      } else {
        columnSize = accountFormTemplate
      }
    } else if (this.isHealthSurvey) {
      if (this.props.healthGoals) {
        columnSize = '1fr';
      } else if (this.props.weight) {
        columnSize = '400px 250px'
      } else {
        columnSize = '1fr 0fr';
      }
    } else if (this.props.chireport) {
      columnSize = '0fr'
    }
    // this.isAccount ? this.props.personalInfo ? '300px 1fr' : this.props.cpassword ? '300px 1fr' : accountFormTemplate : this.isHealthSurvey ? this.props.healthGoals ? '1fr' : '1fr 0fr' : '1fr'
    return (
      <div key={index} style={styles.inputContainer} className="input-wrapper">

        <GridContainer
          columnSize={columnSize}
          gap={this.isAccount || this.props.chireport ? 0 : !this.isHealthSurvey ? 50 : null}
          rowGap={this.isHealthSurvey ? 10 : 0}
          columnGap={this.isHealthSurvey ? '50px !important' : 0}
          styles={{
            marginBottom: this.isAccount ? 10 : 20,
            width: '100%',
            marginTop: input.name === 'fullName' ? 10 : 0,
            borderBottom: this.isAccount ? '1px solid #ccc3' : this.props.healthGoals ? '1px solid #ccc3' : '',
            paddingBottom: this.isAccount ? 10 : this.props.healthGoals ? 30 : 0
          }}
        >
          {
            this.isAccount && <Label text={this.props.personalInfo ? input.label : input.question} with_error={input.error.active} />
          }


          {
            this.isHealthSurvey ?
              <span className={this.isHealthSurvey ? labelClass : null} style={this.isHealthSurvey ? {} : input.label ? styles.inputLabel : { display: "none" }}>
                <p style={{ color: input.error.active ? Colors.movementColor : Colors.blueDarkAccent, fontSize: '1rem' }}>{input.label.split('(')[0]}
                  <span style={{ color: '#999', fontSize: '.8rem' }} >
                    {input.label.indexOf('(') > -1
                      ? `(${input.label.split('(')[1]}`
                      : input.label.split('(')[1]}
                    {input.hint ? Tooltip(input) : null}
                  </span>
                </p>
              </span>
              :
              this.isAccount && input.name === 'company' ?
                <span
                  data-name="input-name"
                  className={disabledInputStyle}
                >
                  {input.label}
                </span> :
                !this.props.personalInfo ?
                  <span data-status="old" className={this.isHealthSurvey ? labelClass : null} style={this.isHealthSurvey ? {} : input.label ? styles.inputLabel : { display: "none" }}>
                    {input.label}
                  </span> : null
          }

          {
            this.isHealthSurvey ?
              <input
                type={input.type}
                className={combined([inputClass, input.type === 'number' ? centered : ''])}
                onChange={this._handleChange.bind(this, input)}
                value={input.value}
                disabled={input.disabled}
                hidden={input.hidden}
                style={{
                  height: this.props.healthGoals ? '70px' : ''
                }}
              /> :

              this.isAccount ?
                <input
                  data-name={input.name}
                  data-account={true}
                  id={input.name}
                  type={input.type}
                  onChange={this._handleChange.bind(this, input)}
                  value={input.value}
                  disabled={input.disabled ? true : this.props.editInfo || this.props.cpassword ? false : true}
                  hidden={input.hidden}
                  className={combined([this.props.editInfo ? inputClass : this.props.cpassword ? inputClass : disabledInputStyle], { color: this._checkInputName(input.name) })}
                /> :

                <input
                  placeholder={input.question}
                  type={input.type}
                  style={
                    input.disabled
                      ? styles.disabled
                      : input.error.active
                        ? { ...styles.formInput, ...styles.formInputError }
                        : styles.formInput
                  }
                  onChange={this._handleChange.bind(this, input)}
                  value={input.value}
                  disabled={input.disabled}
                  hidden={input.hidden}
                  onKeyPress={input.type === 'password' ? this._handleKeyPress : null}
                />
          }

          {
            this.isAccount && !this.props.personalInfo && this._checkInputName(input.name, true) &&
            <button
              disabled={input.disabled}
              data-status={input.disabled}
              onClick={(e) => this._changeBtnHandler(input.name, e)}
              className={input.disabled ? disabledChangeButton : changeButton}
              style={{ margin: 'auto auto', maxWidth: 100 }}
            >Change</button>
          }

          {(!this.props.healthGoals && !this.props.cpassword && !this.props.chireport) &&
            <span
              className={combined([
                // this.props.cpassword && cpassError,
                this.props.buttonText === "Reset" || this.props.from === 'login' ? errorWrapper : errorMsg,
                this.props.buttonText === "Reset" ? at_reset : null
              ])}
              style={
                input.error.active
                  ? error_style
                  : styles.validationText
              }
            >
              {input.error.message}
            </span>
          }
          {
            this.props.chireport &&
            <span
              style={
                input.error.active
                  ? { ...styles.validationText, ...styles.validationTextVisible }
                  : styles.validationText
              } > {input.error.message} </span>
          }
        </GridContainer>
      </div>
    );
  }

  _changeBtnHandler = (inputId, e) => {
    const input = document.querySelector(`#${inputId}`);
    const btn = e.target;

    if (btn.innerHTML === 'Cancel') {
      input.setAttribute('disabled', true);
      input.classList.remove(inputClass);
      input.blur();
      input.style = 'cursor: default'
      btn.style = `color: ${Colors.blueDarkAccent}`
      btn.innerHTML = 'Change'
      if (btn.previousSibling.tagName === 'SELECT') {
        btn.previousSibling.style = 'cursor: default'
      }
    } else {
      input.removeAttribute('disabled');
      input.classList.add(inputClass);
      input.focus();
      input.style = 'cursor: text'
      btn.style = `color: ${Colors.pink}`
      btn.innerHTML = 'Cancel'
      if (btn.previousSibling.tagName === 'SELECT') {
        btn.previousSibling.style = 'cursor: pointer'
      }
    }
  }

  _displayDateInput(input, index) {
    if (!this.state.showContent) {
      this._checkValueToShowContent(input);
    }
    return (
      <div key={index} style={styles.inputContainer}>
        <Datetime
          inputProps={{ placeholder: input.question }}
          value={input.value}
          onChange={this._handleChange.bind(this, input)}
        />
        <span
          style={
            input.error.active
              ? { ...styles.validationText, ...styles.validationTextVisible }
              : styles.validationText
          }
        >
          {input.error.message}
        </span>
      </div>
    );
  }

  _checkSelectedInput = (input) => {
    const options = input.options
    let val = ''

    const style = {
      padding: '0 15px',
      color: Colors.blueDarkAccent,
      display: 'block',
      fontWeight: 500,
      textAlign: 'left'
    }

    options.forEach(option => {
      if (option.label === input.value) {
        val = <span style={style}>{option.label}</span>
      } else if (input.value.split(",").indexOf(option.value) > -1) {
        val = <span style={style}>{option.label}</span>
      }
    })

    return val;
  }

  _displaySelect(input, index) {
    if (!this.state.showContent) {
      this._checkValueToShowContent(input);
    }
    const error_style =
      input.name === 'usualMeal' ?
        { ...styles.longGridError, ...styles.validationText, ...styles.validationTextVisible }
        : { ...styles.validationText, ...styles.validationTextVisible };
    return (
      <div key={index} style={this.isHealthSurvey ? {} : styles.inputContainer}>
        {
          input.name === 'workStress' && <Willingness stress={this.props.stress} />
        }

        {
          this.isHealthSurvey ?
            <GridContainer
              columnSize={input.name === 'usualMeal' || input.name === 'workActivity' || input.name === 'sittingHours' || this.props.healthGoals ? '1fr' : '1fr 1fr'}
              gap={input.name === 'usualMeal' || this.props.healthGoals ? 10 : 50}
              styles={{
                justifyContent: 'left',
                width: '100%',
                textAlign: 'left !important',
                padding: '20px 0',
                borderBottom: this.props.healthGoals ? '' : '1px solid #ccc3'
              }}>

              <p className={questionWrapper} style={{ color: input.error.active ? Colors.movementColor : Colors.blueDarkAccent }}>{input.question}</p>

              <div
                data-name="wrapper"
                className={input.name !== 'usualMeal' ? optionsContainer(this.props, input) : usualMealWrapper}
                style={input.name !== 'usualMeal' ? !this.props.stress ? this.props.movement && input.name === 'sittingHours' ? { maxWidth: 800, margin: '0 auto' } : !this.props.movement && !this.props.healthGoals ? { maxWidth: 500 } : { marginLeft: 50 } : {} : {}}
              >
                {
                  input.name !== 'usualMeal' && !this.props.sleep && !this.props.stress && !this.props.movement && !this.props.healthGoals &&
                  <span className={spans} style={{ opacity: input.name === 'modifyDiet' ? 1 : 0 }}>
                    {!this.props.sleep && !this.props.stress && !this.props.movement && 'Very Willing'}
                  </span>
                }

                {
                  this.props.stress &&
                  <span className={spans} style={{ opacity: input.name === 'workStress' ? 1 : 0 }}>Low</span>
                }

                {
                  this.props.stress && input.name === 'counseling' &&
                  <span></span>
                }

                {
                  this.props.stress && input.name === 'therapy' &&
                  <span></span>
                }

                {
                  this.props.sleep && input.name !== 'averageSleep' &&
                  <React.Fragment>
                    <div /><div />
                  </React.Fragment>
                }

                {this._displayOptions(input)}

                {
                  this.props.stress &&
                  <span className={spans} style={{ opacity: input.name === 'workStress' ? 1 : 0 }}>High</span>
                }

                {
                  input.name !== 'usualMeal' && !this.props.sleep && !this.props.stress && !this.props.movement && !this.props.healthGoals &&
                  <span className={spans} style={{ opacity: input.name === 'modifyDiet' ? 1 : 0 }}>{!this.props.sleep && !this.props.stress && !this.props.movement && 'Willing'}</span>
                }

                {!this.isHealthSurvey || this.props.weight ?
                  <span
                    className={errorMsg}
                    style={
                      input.error.active
                        ? error_style
                        : styles.validationText
                    }
                  > {input.error.message}</span>
                  : null
                }
              </div>

            </GridContainer> :

            this.props.personalInfo ?
              <GridContainer
                columnSize="300px 1fr"
                styles={{ width: '100%', justifyContent: 'left', padding: '10px 0', borderBottom: '1px solid #ccc3' }}
              >
                <Label text={input.label} with_error={input.error.active} />
                <div style={styles.optionsContainer}>
                  {this.props.editInfo ? this._displayOptions(input) : this._checkSelectedInput(input)}
                </div>
              </GridContainer> :

              <div>
                <span style={styles.label}>{input.question}</span>
                <div style={styles.optionsContainer}>{this._displayOptions(input)}</div>
                {!this.isHealthSurvey || this.props.weight ?
                  <span
                    style={
                      input.error.active
                        ? { ...styles.validationText, ...styles.validationTextVisible }
                        : styles.validationText
                    }
                  >
                    {input.error.message}
                  </span>
                  : null
                }
              </div>
        }

      </div>
    );
  }

  _displayDropdown(input, index) {
    if (!this.state.showContent) {
      this._checkValueToShowContent(input);
    }
    let columnSize = accountFormTemplate
    if (this.props.personalInfo) {
      columnSize = '300px 1fr'
    } else if (this.props.chireport) {
      columnSize = '0fr'
    }

    const error_style =
      this.props.from === 'health-survey' && this.props.weight ?
        { ...styles.shortGridError, ...styles.validationText, ...styles.validationTextVisible }
        : { ...styles.validationText, ...styles.validationTextVisible };

    return (
      <div key={index} >
        <GridContainer
          columnSize={columnSize}
          styles={{
            marginBottom: this.isAccount ? 10 : 20,
            width: '100%',
            marginTop: 0,
            borderBottom: this.isAccount ? '1px solid #ccc3' : 'none',
            paddingBottom: this.isAccount ? 10 : 0
          }}
        >
          {
            this.isAccount && <Label text={this.props.personalInfo ? input.label : input.question} />
          }

          {this.props.personalInfo ? null : input.label ? input.label : null}

          {
            this.isAccount ?
              <select
                data-name={input.name}
                data-account={true}
                id={input.name}
                id={input.name}
                value={input.value || input.value.toUpperCase() || undefined}
                onChange={(e) => this._handleSelect(e)}
                className={combined([this.props.personalInfo ? this.props.editInfo ? inputClass : disabledInputStyle : disabledInputStyle], { color: this._checkInputName(input.name) })}
                disabled={input.disabled ? true : this.props.editInfo ? false : true}
              >
                <option key="a" value="" disabled>{input.question}</option>
                {this._displayListOptions(input)}
              </select> :

              this.props.chireport ?
                <select
                  disabled={input.disabled}
                  id={input.name}
                  style={
                    input.error.active
                      ? { ...styles.formDropdown, ...styles.formInputError }
                      : styles.formDropdown
                  }
                  onChange={this._handleSelect.bind(this)}
                >
                  <option key="a" value="">
                    {input.question}
                  </option>
                  {this._displayListOptions(input)}

                </select> :

                <select
                  disabled={input.disabled}
                  id={input.name}
                  style={
                    input.error.active
                      ? { ...styles.formDropdown, ...styles.formInputError }
                      : styles.formDropdown
                  }
                  value={input.value || undefined}
                  onChange={this._handleSelect.bind(this)}
                >
                  <option key="a" value="">
                    {input.question}
                  </option>
                  {this._displayListOptions(input)}

                </select>
          }

          {
            this.isAccount && !this.props.personalInfo && this._checkInputName(input.name, true) &&
            <button
              disabled={input.disabled}
              data-status={input.disabled}
              onClick={(e) => this._changeBtnHandler(input.name, e)}
              className={input.disabled ? disabledChangeButton : changeButton}
              style={{ margin: 'auto auto', maxWidth: 100 }}
            >Change</button>
          }

          <span
            className={combined([
              // this.props.cpassword && cpassError,
              this.props.buttonText === "Reset" || this.props.from === 'login' ? errorWrapper : errorMsg,
              this.props.buttonText === "Reset" ? at_reset : null
            ])}
            style={
              input.error.active
                ? error_style
                : styles.validationText
            }
          >
            {input.error.message}
          </span>
        </GridContainer>
      </div>
    );
  }

  _styleSelect(input) {
    if (input.error.active === true) {
      return {
        ...styles.options,
        ...styles.optionsDefault,
        ...styles.optionsError
      };
    } else if (input.value === "default") {
      return { ...styles.options, ...styles.optionsDefault };
    } else {
      return { ...styles.options };
    }
  }

  _displayListOptions(input, col = 'label') {

    const html = [];
    const options = input.options;

    options.forEach((option, i) => {
      html.push(
        <option key={i} value={option.value}>
          {option[`${col}`]}
        </option>
      );
    });

    // for (let i = 0; i < options.length; i++) {
    //   html.push(
    //     <option key={i} value={options[i][`${col}`]}>
    //       {options[i][`label`]}
    //     </option>
    //   );
    // }

    return html;
  }

  _optionWrapper = (input, key, filled = false, dataName) => {
    const options = input.options;


    const optionClass = () => {
      if (this.props.healthGoals && filled) {
        return sqrSelectedOption;
      } else if (this.props.healthGoals && !filled) {
        return sqrDefaultOption;
      }

      if (input.name === 'workActivity' && filled) {
        return sqrSelectedOption;
      } else if (input.name === 'workActivity' && !filled) {
        return sqrDefaultOption;
      } else if (input.name === 'sittingHours' && filled) {
        return sqrSelectedOption;
      } else if (input.name === 'sittingHours' && !filled) {
        return sqrDefaultOption;
      } else if (input.name !== 'usualMeal' && filled) {
        return selectOptionStyleFilledSurvey;
      } else if (input.name !== 'usualMeal' && !filled) {
        return selectOptionStyleSurvey;
      } else if (input.name === 'usualMeal' && filled) {
        return selectOptionFilledStyle;
      } else if (input.name === 'usualMeal' && !filled) {
        return selectOptionStyle;
      }
    };

    return (
      <div
        key={key}
        data-name={`${dataName}-wrapper`}
        className={input.name === 'usualMeal' ? selectWrapperUsualMeal : selectWrapper(this.props, input)}
      >
        <button
          data-name={dataName}
          id={input.name}
          className={optionClass()}
          name={options[key].value}
          onClick={this._handleClick(!filled)}
          style={{ marginRight: 10 }}
        >
          {
            !this.isHealthSurvey && options[key].label
          }
          {
            input.name === 'usualMeal' ? options[key].label : null
          }
        </button>
        {
          this.props.nutrition && input.name !== 'usualMeal' &&
          <label htmlFor={options[key].value} className={optionName} style={{ color: dataName === 'default__survey-option' ? '#777' : Colors.skyblue }}>{options[key].value}</label>
        }

        {
          this.props.sleep &&
          <label htmlFor={options[key].value} className={optionName} style={{ color: '#777' }}>{options[key].value}</label>
        }

        {
          this.props.stress && input.name === 'counseling' &&
          <label htmlFor={options[key].value} className={optionName} style={{ color: '#777' }}>{options[key].value}</label>
        }

        {
          this.props.stress && input.name === 'therapy' &&
          <label htmlFor={options[key].value} className={optionName} style={{ color: '#777' }}>{options[key].value}</label>
        }

        {
          this.props.movement &&
          <label htmlFor={options[key].value} className={optionName} style={{ color: dataName === 'default__survey-option' ? '#777' : Colors.skyblue }}>{options[key].value}</label>
        }

        {
          this.props.healthGoals &&
          <label htmlFor={options[key].value} className={optionName} style={{ color: dataName === 'default__survey-option' ? '#777' : Colors.skyblue }}>{options[key].label}</label>
        }
      </div>
    );
  }

  _displayOptions(input) {
    const html = [];
    const options = input.options;
    for (let i = 0; i < options.length; i++) {
      if (options[i].label === input.value) {
        html.push(
          this.isHealthSurvey ?
            this._optionWrapper(input, i, true, 'selected__survey-option')
            :
            <button
              data-name="selected__personal-info-option"
              key={i}
              id={input.name}
              className={selectOptionFilledStyle}
              name={options[i].value}
              onClick={this._handleClick()}
            >
              {options[i].label}
            </button>
        );
      } else if (input.value.split(",").indexOf(options[i].value) > -1) {
        html.push(
          this.isHealthSurvey ?
            this._optionWrapper(input, i, true, 'submitted__survey-option')
            :
            <button
              data-name="submitted_personal-info-option"
              key={i}
              id={input.name}
              className={selectOptionFilledStyle}
              name={options[i].value}
              onClick={this._handleClick()}
            >
              {options[i].label}
            </button>
        );
      } else {
        html.push(
          this.isHealthSurvey ?
            this._optionWrapper(input, i, false, 'default__survey-option')
            :

            this.props.personalInfo ?
              <button
                data-name="default__personal-info-option"
                key={i}
                id={input.name}
                className={selectOptionStyle}
                name={options[i].value}
                onClick={this._handleClick()}
              >
                {options[i].label}
              </button> :


              <button
                data-name="survey-button"
                key={i}
                id={input.name}
                className={this.isHealthSurvey && input.name !== 'usualMeal' ? selectOptionStyleSurvey : selectOptionStyle}
                name={options[i].value}
                onClick={this._handleClick()}
              >
                {options[i].label}
              </button>
        );
      }
    }

    return html;
  }

  _handleSelect = (e) => {
    const value = e.target.value;
    const name = e.target.id;

    const validated = this._validateInput(value, "dropdown");
    // //if validated is true, error.active should be false and vice versa
    this.props.handleChange(value, name, !validated);
  }

  _handleClick = param => e => {
    const value = e.target.name;
    const name = e.target.id;

    const validated = this._validateInput(value, "select");
    // //if validated is true, error.active should be false and vice versa
    this.props.handleChange(value, name, !validated);
  }

  _handleChange(input, event) {
    this.setState({
      btnTxt: this.props.buttonText
    });
    let value;
    if (input.type === "date") {
      value = event;
      input.question = "";
    } else {
      value = event.target.value;
    }
    const validated = this._validateInput(value, input.validation, input);
    //if validated is true, error.active should be false and vice versa
    this.props.handleChange(value, input.name, !validated);
  }

  _validateInput(input, type) {
    if (type === "email") {
      const ck_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      if (!ck_email.test(input)) {
        return false;
      }
      return true;
    } else if (type === "company") {
      let test = false;
      for (let i = 0; i < this.props.companies.length; i++) {
        if (this.props.companies[i].code == input) {
          test = true;
        }
      }
      return test;
    } else if (type === "name") {
      const ck_name = /^(.*)\s(.*)$/i;
      if (!ck_name.test(input) || !input) {
        return false;
      }
      return true;
    } else if (type === "password") {
      //  if (this.props.location !== 'profile') {
      const ck_password = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
      if (!ck_password.test(input)) {
        return false;
      }
      //  }
      return true;
    } else if (type === "birthday") {
      const eighteenYearsAgo = moment().subtract(1, "month");
      if (input) {
        const birthday = moment(input);

        if (!birthday.isValid()) {
          return false;
        } else if (eighteenYearsAgo.isAfter(birthday)) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    } else if (type === "number") {
      var ck_required = /^[0-9]+/;
      if (!ck_required.test(input)) {
        return false;
      }
      return true;
    } else if (type === "date") {
      const date = moment(input);

      if (!date.isValid()) {
        return false;
      }
      return true;
    } else if (type === "select") {
      if (input === "default" || input === "") {
        return false;
      }
      return true;
    } else if (type === "dropdown") {
      if (input === "default" || input === "") {
        return false;
      }
      return true;
    } else if (type === "text") {
      if (input === "default" || input === "") {
        return false;
      }
      return true;
    } if (type === "required") {
      var ck_required = /^[a-zA-Z\d]/;
      if (input === "default" || !ck_required.test(input)) {
        return false;
      }
      return true;
    } else if (type === "withRange") {
      let isValid = false;
      const arrVal = input.split('.');
      const val1 = parseInt(arrVal[0]);
      const val2 = parseInt(arrVal[1]);
      if (val1 && val1 > 0 && val1 < 8) {
        isValid = true;
        if (val2) {
          if (val2 < 0 || val2 > 11) {
            isValid = false;
          }

        }
      } else {
        isValid = false
      }
      var ck_required = /^[0-9]+/;
      if (!isValid || input === "default" || !ck_required.test(input)) {
        return false;
      }
      return true;
    }
  }

  _allInputsValid() {
    const results = [];
    for (let i = 0; i < this.props.inputs.length; i++) {

      results.push(
        this._validateInput(
          this.props.inputs[i].value,
          this.props.inputs[i].validation
        )
      );
    }

    if (results.indexOf(false) !== -1) {
      return false;
    }
    return true;
  }

  _removeInputClass = () => {
    const dataAccounts = document.querySelectorAll('[data-account]');
    dataAccounts.forEach(da => {
      da.classList.remove(inputClass);
    })
  }

  _submitForm(e) {
    this.isAccount && this._removeInputClass();

    if (!this._allInputsValid()) {
      console.log("could not validate");
      for (let i = 0; i < this.props.inputs.length; i++) {
        const validated = this._validateInput(
          this.props.inputs[i].value,
          this.props.inputs[i].type
        );
        this.props.handleChange(
          this.props.inputs[i].value,
          this.props.inputs[i].name,
          !validated
        );
      }
      this.setState({
        formError: {
          active: true,
          message: 'Please complete all fields'
        }
      });
      return;
    } else {
      this.setState({
        formError: {
          active: false
        }
      });
      this.props.submitForm();
    }
  }
}

const cpassError = css(errorMsg, {
  marginLeft: '0 !important',
  fontSize: '0.7rem !important',
  marginTop: '10px !important'
})

const optionName = css({
  margin: '0 !important',
  textAlign: 'left'
});

const selectWrapperUsualMeal = css({
  display: 'grid',
  gridTemplateColumns: '1fr',
});

const usualMealSelectWrapper = css({
  display: 'grid',
  gridTemplateColumns: '1fr',
  // placeContent: 'center',
});

const usualMealWrapper = css({
  display: 'grid',
  gridTemplateColumns: 'repeat(6, 1fr)',
  gridGap: 10,
  // maxWidth: 600,
  margin: '0 auto'
});

const selectWrapper = (props, input) => {
  if (props.stress) {
    if (input.name === 'counseling' || input.name === 'therapy') {
      const template = '1fr 1fr';
      return gridTemplate(template);
    }
    const template = '1fr';
    return gridTemplate(template);
  } else if (props.sleep) {
    const template = '1fr 1fr';
    return gridTemplate(template);
  } else if (props.nutrition) {
    const template = '1fr 3fr';
    return gridTemplate(template);
  } else if (props.movement) {
    const template = '1fr 6fr';
    return gridTemplate(template);
  } else if (props.healthGoals) {
    const template = '1fr 6fr';
    return gridTemplate(template);
  }
};

const gridTemplate = (template) => {
  return css({
    display: 'grid',
    gridTemplateColumns: template,
    placeContent: 'center',
    '& > label': {
      textTransform: 'capitalize',
      margin: '0 30px 0 10px'
    }
  });
};

const labelClass = css({
  textAlign: 'left !important',
  display: 'block',
  width: '100%',
  display: 'grid',
  alignContent: 'center'
});

const inputClass = css({
  borderStyle: 'none',
  padding: '10px 15px',
  border: '1px solid #eee !important',
  boxShadow: '0 2px 2px rgba(0,0,0,.050) !important',
  transition: '250ms ease',
  borderRadius: 5,
  cursor: 'text',
  textAlign: 'left !important',
  background: 'none',
  ':focus': {
    outline: 'none',
    borderColor: `${Colors.skyblue} !important`,
    boxShadow: `0 5px 10px ${Colors.skyblue}50 !important`
  }
});

const fade = css.keyframes('fade', {
  '0%': {
    opacity: .2
  },
  '50%': {
    opacity: 1
  },
  '100%': {
    opacity: .2
  }
});

const loader = css({
  position: 'absolute',
  top: 0,
  left: 0,
  height: '100%',
  width: '100%',
  display: 'grid',
  placeContent: 'center',
  textAlign: 'center',
  zIndex: 100,
  '> svg': {
    animation: `${fade} 1s ease infinite`
  },
  '> small': {
    textAlign: 'center',
    color: 'white !important'
  }
});

const loaderWithoutBg = css(loader, {
  position: 'relative',
  background: 'transparent',
  '> small': {
    color: '#ccc !important'
  }
});

const loaderWithBg = css(loader, {
  padding: 30,
  background: 'white',
  '> small': {
    color: '#ccc !important'
  }
});

const loginLoaded = css(loader, {
  background: Colors.skyblue
});

const errorMsg = css({
  background: Colors.movementColor,
  padding: '5px',
  color: 'white',
  textAlign: 'center',
  gridColumn: '2/12',
  margin: '0',
  // marginTop: '10px !important',
  borderRadius: 5
});

const errorMsg_login = css(errorMsg, {
  padding: '10px',
  marginBottom: '25px !important'
});

const spans = css({
  fontSize: '.8rem',
  color: '#777'
});

const questionWrapper = css({
  position: 'relative',
  color: Colors.blueDarkAccent,
  fontSize: '1rem',
  textAlign: "left !important"
});

const selectOptionStyleSurvey = css({
  width: 20,
  height: 20,
  borderRadius: '100%',
  border: '2px solid #777',
  background: 'none',
  display: 'block',
  margin: 'auto auto',
  cursor: 'pointer',
  transition: '250ms ease',
  transform: 'scale(1)',
  ':hover': {
    transform: 'scale(1.2)',
    borderColor: Colors.skyblue
  },
  ':focus': {
    outline: 'none'
  }
});

const selectOptionStyleFilledSurvey = css(selectOptionStyleSurvey, {
  borderColor: Colors.skyblue,
  background: Colors.skyblue,
  boxShadow: `0 5px 15px ${Colors.skyblue}70`,
});

const optionsContainer = (props, input) => {
  if (props.nutrition) {
    const template = 'repeat(4, 1fr)';
    return optionsGridWrapper(template);
  } else if (props.sleep) {
    const template = 'repeat(4, 1fr)';
    return optionsGridWrapper(template);
  } else if (props.stress) {
    if (input.name === 'counseling' || input.name === 'therapy') {
      const template = 'repeat(4, 1fr)';
      return optionsGridWrapper(template);
    }
    const template = 'repeat(12, 1fr)';
    return optionsGridWrapper(template);
  } else if (props.readiness) {
    const template = '2fr repeat(6, 1fr)';
    return optionsGridWrapper(template);
  } else if (props.movement) {
    if (input.name === 'sittingHours') {
      const template = '1fr 1fr 1fr';
      return optionsGridWrapper(template);
    }
    const template = '1fr 1fr';
    return optionsGridWrapper(template);
  } else if (props.healthGoals) {
    const template = '1fr 1fr 1fr';
    return optionsGridWrapper(template);
  }
};

const optionsGridWrapper = template => {
  return css({
    display: 'grid',
    gridTemplateColumns: template,
    placeContent: 'center'
  });
};

const noAccountError = css({
  padding: 10,
  color: "white !important",
  borderRadius: 5,
  background: "#fffafd",
  color: "#8e3372"
});

let at_reset = css({
  right: "-230px !important"
});

const errorWrapper = css({
  position: "absolute",
  top: "40%",
  right: -230,
  width: 240,
  transform: "translateY(-80%)",
  background: "#fffafd",
  padding: 10,
  margin: "0 !important",
  color: "#8e3372",
  fontSize: ".6em !important",
  borderRadius: 5,
  lineHeight: "14px",
  border: "1px solid #8e3372",
  textAlign: "justify",

  "&::before": {
    content: '""',
    width: 12,
    height: 12,
    background: "#fffafd",
    position: "absolute",
    left: -7,
    top: "50%",
    border: "1px solid #8e3372",
    borderTopColor: "rgba(0,0,0,0)",
    borderRightColor: "rgba(0,0,0,0)",
    transform: "translateY(-50%) rotate(45deg)"
  }
});

let selectOptionStyle = css({
  padding: "10px 20px",
  backgroundColor: "#fff",
  border: "2px solid #777",
  color: "#000",
  borderRadius: "5px",
  margin: "5px",
  fontSize: ".8rem",
  cursor: "pointer",
  transition: '250ms ease',
  ":hover": {
    // backgroundColor: "#ED81B9",
    borderColor: Colors.skyblue,
    color: Colors.skyblue
  },
  ':focus': {
    outline: 'none'
  }
});

let selectOptionFilledStyle = css({
  padding: "10px 20px",
  backgroundColor: Colors.skyblue,
  border: `2px solid ${Colors.skyblue}`,
  color: "#fff",
  borderRadius: "5px",
  margin: "5px",
  fontSize: ".8rem",
  cursor: "pointer",
  fontWeight: 600,
  boxShadow: `0 5px 15px ${Colors.skyblue}70`,
  ':focus': {
    outline: 'none'
  }
});

let healthSurveyHeader = css({
  fontSize: 18,
  textAlign: 'left',
  marginTop: 10
});

const styles = {
  disabled: {
    padding: "13px 30px 13px 30px",
    marginTop: "20px",
    width: "247px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    fontSize: "14px",
    outline: "none",
    background: "#d3d3d3"
  },
  formContainer: {
    width: '100%',
    position: 'relative',
    // border: '2px solid rgba(151,151,151,0.2)',
    // borderRadius: '6px',
    maxWidth: "100%",
    // JsDisplay: "flex",
    // display: "-webkit-box" /* OLD - iOS 6-, Safari 3.1-6 */,
    // display: "-moz-box" /* OLD - Firefox 19- (buggy but mostly works) */,
    // display: "-ms-flexbox" /* TWEENER - IE 10 */,
    // display: "-webkit-flex" /* NEW - Chrome */,
    // display: "flex" /* NEW, Spec - Opera 12.1, Firefox 20+ */,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    textAlign: "center",
    // borderBottom: "2px solid rgba(225,232,238,1)",
    // padding: "15px 20px",
    fontSize: "18px",
    color: "#364563",
    fontFamily: '"Roboto-Medium",sans-serif',
    // width: "95%",
    display: "flex",
    flexDirection: "column"
  },
  form: {
    margin: "10px 30px 20px 20px"
  },
  formError: {
    // display: 'none',
    fontSize: '1rem',
    marginTop: '25px',
    color: '#EA3131',
    transition: 'color 0.3s ease-in-out',
    fontWeight: 500
  },
  formErrorActive: {
    display: "block"
  },
  formSuccess: {
    display: "none",
    fontSize: "0.8em",
    margin: '10px auto 0',
    color: "white",
    padding: '10px 30px',
    borderRadius: 5,
    transition: "color 0.3s ease-in-out",
    background: 'rgb(3, 210, 127)'
  },
  formSuccessActive: {
    display: "block",
    maxWidth: 'fit-content',
  },
  formDropdown: {
    padding: "13px 30px 13px 30px",
    marginTop: "20px",
    width: "247px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    fontSize: "14px",
    outline: "none"
  },
  formInput: {
    padding: "13px 30px 13px 30px",
    marginTop: "20px",
    width: "247px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    fontSize: "14px",
    outline: "none"
  },
  formInputError: {
    border: "2px solid #EA3131",
    transition: "color 0.3s ease-in-out"
  },
  optionsContainer: {
    display: "flex",
    flexFlow: "row wrap",
    justifyContent: "left",
    alignItems: "center"
  },
  inputContainer: {
    display: "flex",
    flexFlow: "column wrap",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "10px",
    position: "relative"
  },
  label: {
    color: "#364563",
    fontSize: "15px",
    fontFamily: '"Roboto-Medium",sans-serif',
    marginBottom: "5px",
    marginTop: "10px",
    width: "100%",
    textAlign: "center"
  },
  inputLabel: {
    color: "#364563",
    fontSize: "15px",
    fontFamily: '"Roboto-Medium",sans-serif',
    marginBottom: "-10px",
    marginTop: "10px",
    width: "100%",
    textAlign: "center"
  },
  selectOption: {
    padding: "10px 20px",
    backgroundColor: "white",
    border: "2px solid #77E9AF",
    color: "#77E9AF",
    borderRadius: "5px",
    margin: "5px",
    fontSize: "12px"
  },
  options: {
    backgroundColor: "#fff"
  },
  optionsDefault: {
    color: "rgba(169, 169, 169, 1)"
  },
  optionsError: {
    border: "2px solid #EA3131",
    transition: "color 0.3s ease-in-out"
  },
  selectArrowZone: {
    cursor: "pointer",
    display: "table-cell",
    position: "relative",
    textAlign: "center",
    verticalAlign: "middle",
    width: "25px",
    paddingRight: "5px"
  },
  selectArrow: {
    borderColor: "#999 transparent transparent",
    borderStyle: "solid",
    borderWidth: "5px 5px 2.5px",
    display: "inline-block",
    height: "0",
    width: "0",
    position: "absolute",
    right: "-17em",
    top: "-2.75em"
  },
  validationText: {
    display: "none",
    fontSize: "0.8em",
    // marginLeft: "25px",
    marginTop: "5px",
    // color: "#EA3131",
    transition: "color 0.3s ease-in-out"
  },
  validationTextVisible: {
    display: "block"
  },
  button: {
    backgroundColor: "#36c4f1",
    width: "250px",
    padding: "11px 50px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    // fontFamily: '"Roboto",sans-serif',
    // fontSize: "1rem",
    cursor: "pointer",
    display: "block",
    margin: "auto",
    marginTop: "0px",
    marginBottom: "20px"
  },
  disabledButton: {
    backgroundColor: "gray",
    width: "250px",
    padding: "11px 50px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontSize: "1rem",
    display: "block",
    margin: "auto",
    marginTop: "0px",
    marginBottom: "20px"
  },
  longGridError: {
    gridColumn: '1/7'
  },
  shortGridError: {
    gridColumn: '2/3',
    marginTop: '0 !important'
  }
};
