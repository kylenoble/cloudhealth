import React from "react";
import Link from "next/link";
import moment from "moment";
import Datetime from "react-datetime";
import TextareaAutosize from "react-autosize-textarea";
import { css } from "glamor";

import Autocomplete from "./Autocomplete";
import { inputClass, labelClass } from "../NewSkin/Styles";
import Button from "../NewSkin/Components/Button";

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      btnTxt: this.props.buttonText,
      companies: this.props.companies,
      departments: this.props.departments,
      btnLocked: false
    };

    this._submitForm = this._submitForm.bind(this);
    this._selectedSuggestion = this._selectedSuggestion.bind(this);
  }

  render() {
    let formContainerReplacement = null;
    if (this.props.styles && this.props.styles.formContainer) {
      formContainerReplacement = this.props.styles.formContainer;
    }
    return (
      <div
        style={
          formContainerReplacement
            ? formContainerReplacement
            : styles.formContainer
        }
      >
        {this.props.header ? (
          <div style={styles.header}>
            <span>{this.props.header}</span>
          </div>
        ) : null}

        <div style={styles.form}>
          {this._displayInputs()}
          {this.props.table ? this.props.table : ""}
        </div>
        <span
          style={
            this.props.formError.active
              ? { ...styles.formError, ...styles.formErrorActive }
              : styles.formError
          }
        >
          {this.props.formError.message}
        </span>
        <span
          style={
            this.props.formSuccess.active
              ? { ...styles.formSuccess, ...styles.formSuccessActive }
              : styles.formSuccess
          }
        >
          {this.props.formSuccess.message}
        </span>
        {!this.props.btnLocked ? (
          <Button
            type={this.props.btnLocked ? "disabled" : "pink"}
            styles={{ marginTop: 20 }}
          >
            <button
              disabled={this.props.btnLocked}
              style={this.props.btnLocked ? styles.disabledButton : styles.btn}
              onClick={this._submitForm}
            >
              {this.props.buttonText}
            </button>
          </Button>
        ) : null}

        {this.props.passwordReset}
        {this.props.otherButton && !this.props.btnLocked
          ? this._displayOtherButton(this.props.otherButton)
          : null}
      </div>
    );
  }

  _displayOtherButton(obj) {
    return obj.map((item, i) => {
      return (
        <button
          name={item.name}
          key={i}
          disabled={item.locked}
          style={item.locked ? styles.lockedButton : styles.btn}
          onClick={this._submitForm}
        >
          {item.label}
        </button>
      );
    });
  }

  supportsFlexBox() {
    const test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  componentDidMount() {
    if (this.props.btnLocked) {
      this.state.btnLocked = this.props.btnLocked;
    }
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      console.log("flexbox is not supported");
      flexibility(document.getElementById("__next"));
    }
  }

  objectSort(property) {
    let sortOrder = 1;
    if (property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }
    return function(a, b) {
      const result =
        a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;
      return result * sortOrder;
    };
  }

  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }

  _displayInputs() {
    const html = [];
    // sort fields

    const inputProps = this.props.inputs;

    const groups = this.removeDuplicates(inputProps, "group");

    groups.forEach((item, i) => {
      if (item.group) {
        html.push(<span key={i + "a"}>{item.groupLabel}</span>);
        inputProps.forEach((inputProps, i) => {
          if (inputProps.group === item.group) {
            const input = inputProps;
            if (input.type === "autocomplete") {
              html.push(this._displayAutoComplete(input, i));
            } else if (input.type === "textArea") {
              html.push(this._displayTextArea(input, i));
            } else if (input.type === "checkbox") {
              html.push(this._displayCheckBox(input, i));
            } else if (input.type === "checkboxgroup") {
              html.push(this._displayCheckBoxGroup(input, i));
            } else if (input.type === "radio") {
              html.push(this._displayRadioButtonGroup(input, i));
            } else if (input.type === "dropdown") {
              html.push(this._displayDropdown(input, i));
            } else if (input.type === "select") {
              html.push(this._displaySelect(input, i));
            } else if (input.type === "date") {
              html.push(this._displayDateInput(input, i));
            } else if (input.type === "button") {
              html.push(this._displayButton(input, i));
            } else {
              html.push(this._displayInput(input, i));
            }
          }
        });
      } else {
        inputProps.forEach((inputProps, i) => {
          if (!inputProps.group) {
            const input = inputProps;

            if (input.type === "autocomplete") {
              html.push(this._displayAutoComplete(input, i));
            } else if (input.type === "textArea") {
              html.push(this._displayTextArea(input, i));
            } else if (input.type === "checkbox") {
              html.push(this._displayCheckBox(input, i));
            } else if (input.type === "checkboxgroup") {
              html.push(this._displayCheckBoxGroup(input, i));
            } else if (input.type === "dropdown") {
              html.push(this._displayDropdown(input, i));
            } else if (input.type === "select") {
              html.push(this._displaySelect(input, i));
            } else if (input.type === "date") {
              html.push(this._displayDateInput(input, i));
            } else if (input.type === "radio") {
              html.push(this._displayRadioButtonGroup(input, i));
            } else {
              html.push(this._displayInput(input, i));
            }
          }
        });
      }
    });

    // for (var i = 0; i < inputProps.length; i++) {
    //   var input = inputProps[i]
    //
    //   if (input.type === 'textArea') {
    //     html.push(this._displayTextArea(input, i))
    //   }else if(input.type === 'checkbox') {
    //     html.push(this._displayCheckBox(input, i))
    //   }else if(input.type === 'checkboxgroup') {
    //     html.push(this._displayCheckBoxGroup(input, i))
    //   } else if(input.type === 'dropdown') {
    //     html.push(this._displayDropdown(input, i))
    //   } else if (input.type === 'select') {
    //     html.push(this._displaySelect(input, i))
    //   } else if (input.type === 'date') {
    //     html.push(this._displayDateInput(input, i))
    //   } else {
    //     html.push(this._displayInput(input, i))
    //   }
    // }
    return html;
  }

  _selectedSuggestion(value) {
    let notValid = true;
    if (value == "") {
      notValid = true;
    } else {
      notValid = false;
    }

    this.props.handleChange(value, "concern", notValid);
  }

  _displayAutoComplete(input, index) {
    let inputContainerReplacement = null;
    let inputLabelReplacement = null;
    if (this.props.styles && this.props.styles.inputContainer) {
      inputContainerReplacement = this.props.styles.inputContainer;
    }
    if (this.props.styles && this.props.styles.inputLabel) {
      inputLabelReplacement = this.props.styles.inputLabel;
    }

    return (
      <div
        key={index}
        style={
          inputContainerReplacement
            ? inputContainerReplacement
            : styles.inputContainer
        }
      >
        <span
          style={
            this.props.appointment
              ? {}
              : input.label
              ? inputLabelReplacement
                ? inputLabelReplacement
                : styles.inputLabel
              : { display: "none" }
          }
          className={this.props.appointment ? labelClass : ""}
        >
          {input.label}
        </span>
        <Autocomplete
          appointment={this.props.appointment}
          input={input}
          user={this.props.user}
          suggestions={input.data}
          _event={this._selectedSuggestion}
          addToList={this.props.addToList}
        />

        <span
          style={
            input.error.active
              ? { ...styles.validationText, ...styles.validationTextVisible }
              : styles.validationText
          }
        >
          {input.error.message}
        </span>
      </div>
    );
  }

  _displayTextArea(input, index) {
    let inputContainerReplacement = null;
    let inputLabelReplacement = null;
    if (this.props.styles && this.props.styles.inputContainer) {
      inputContainerReplacement = this.props.styles.inputContainer;
    }
    if (this.props.styles && this.props.styles.inputLabel) {
      inputLabelReplacement = this.props.styles.inputLabel;
    }
    return (
      <div
        key={index}
        style={
          inputContainerReplacement
            ? inputContainerReplacement
            : styles.inputContainer
        }
      >
        <span style={input.label ? styles.inputLabel : { display: "none" }}>
          {input.label}
        </span>
        <TextareaAutosize
          style={
            input.disabled
              ? styles.disabled
              : input.error.active
              ? { ...styles.formInput, ...styles.formInputError }
              : styles.formInput
          }
          placeholder={input.question}
          type={input.type}
          //style={input.disabled? styles.disabled: input.error.active ? {...styles.formInput,...styles.formInputError} : styles.formInput}
          onChange={this._handleChange.bind(this, input)}
          value={input.value}
          disabled={input.disabled}
          hidden={input.hidden}
        />
        <span
          style={
            input.error.active
              ? { ...styles.validationText, ...styles.validationTextVisible }
              : styles.validationText
          }
        >
          {input.error.message}
        </span>
      </div>
    );
  }

  _displayInput(input, index) {
    let inputContainerReplacement = null;
    let inputLabelReplacement = null;
    if (this.props.styles && this.props.styles.inputContainer) {
      inputContainerReplacement = this.props.styles.inputContainer;
    }
    if (this.props.styles && this.props.styles.inputLabel) {
      inputLabelReplacement = this.props.styles.inputLabel;
    }
    return (
      <div
        key={index}
        style={
          inputContainerReplacement
            ? inputContainerReplacement
            : styles.inputContainer
        }
      >
        <span
          style={
            input.label
              ? inputLabelReplacement
                ? inputLabelReplacement
                : styles.inputLabel
              : { display: "none" }
          }
        >
          {input.label}
        </span>
        <input
          data-id="appointment"
          placeholder={input.question}
          type={input.type}
          style={
            this.props.appointment
              ? {}
              : input.disabled
              ? styles.disabled
              : input.error.active
              ? { ...styles.formInput, ...styles.formInputError }
              : styles.formInput
          }
          onChange={this._handleChange.bind(this, input)}
          value={input.value}
          disabled={input.disabled}
          hidden={input.hidden}
          className={this.props.appointment ? inputClass : ""}
        />
        <span
          style={
            input.error.active
              ? { ...styles.validationText, ...styles.validationTextVisible }
              : styles.validationText
          }
        >
          {input.error.message}
        </span>
      </div>
    );
  }

  _displayButton(input, index) {
    let inputContainerReplacement = null;
    let inputLabelReplacement = null;
    if (this.props.styles && this.props.styles.inputContainer) {
      inputContainerReplacement = this.props.styles.inputContainer;
    }
    if (this.props.styles && this.props.styles.inputLabel) {
      inputLabelReplacement = this.props.styles.inputLabel;
    }
    return (
      <div
        key={index}
        style={
          inputContainerReplacement
            ? inputContainerReplacement
            : styles.inputContainer
        }
      >
        <button
          disabled={input.disabled}
          key={index}
          id={input.name}
          className={
            input.disabled
              ? LockedSelectOptionFilledStyle
              : selectOptionFilledStyle
          }
          name={input.name}
          onClick={this._handleClick.bind(this)}
        >
          {input.label}
        </button>
        <span
          style={
            input.error.active
              ? { ...styles.validationText, ...styles.validationTextVisible }
              : styles.validationText
          }
        >
          {input.error.message}
        </span>
      </div>
    );
  }

  _displayCheckBox(input, index) {
    let inputContainerReplacement = null;
    let inputLabelReplacement = null;
    if (this.props.styles && this.props.styles.inputContainer) {
      inputContainerReplacement = this.props.styles.inputContainer;
    }
    if (this.props.styles && this.props.styles.inputLabel) {
      inputLabelReplacement = this.props.styles.inputLabel;
    }
    return (
      <div
        key={index}
        style={
          inputContainerReplacement
            ? inputContainerReplacement
            : styles.inputContainer
        }
      >
        <label className={checkBoxLabelStyle}>
          <input
            className={checkboxStyle}
            name={input.value.replace(/ /g, "")}
            type={input.type}
            value={input.value}
            disabled={input.disabled}
            hidden={input.hidden}
            checked={input.isChecked}
            onChange={this._handleChange.bind(this, input)}
          />
          {input.label}
        </label>
        <span
          style={
            input.error.active
              ? { ...styles.validationText, ...styles.validationTextVisible }
              : styles.validationText
          }
        >
          {input.error.message}
        </span>
      </div>
    );
  }

  _displayDateInput(input, index) {
    return (
      <div key={index} style={styles.inputContainer}>
        <Datetime
          inputProps={{ placeholder: input.question }}
          value={input.value}
          onChange={this._handleChange.bind(this, input)}
        />
        <span
          style={
            input.error.active
              ? { ...styles.validationText, ...styles.validationTextVisible }
              : styles.validationText
          }
        >
          {input.error.message}
        </span>
      </div>
    );
  }

  _displaySelect(input, index) {
    return (
      <div key={index} style={styles.inputContainer}>
        <span style={styles.label}>{input.question}</span>
        <div style={styles.optionsContainer}>{this._displayOptions(input)}</div>
        <span
          style={
            input.error.active
              ? { ...styles.validationText, ...styles.validationTextVisible }
              : styles.validationText
          }
        >
          {input.error.message}
        </span>
      </div>
    );
  }

  _displayCheckBoxGroup(input, index) {
    let inputContainerReplacement = null;
    let inputLabelReplacement = null;
    if (this.props.styles && this.props.styles.inputContainer) {
      inputContainerReplacement = this.props.styles.inputContainer;
    }
    if (this.props.styles && this.props.styles.inputLabel) {
      inputLabelReplacement = this.props.styles.inputLabel;
    }
    return (
      <div
        key={index}
        style={
          inputContainerReplacement
            ? inputContainerReplacement
            : styles.inputContainer
        }
      >
        <div style={{ margin: "0 0 0 15px" }}>
          {input.label ? (
            <span
              style={{
                fontWeight: "bold",
                padding: 0,
                margin: "0 0 -10px 15px"
              }}
            >
              {input.label}
            </span>
          ) : null}
          {this._displayChecboxes(input)}
        </div>
        <span
          style={
            input.error.active
              ? { ...styles.validationText, ...styles.validationTextVisible }
              : styles.validationText
          }
        >
          {input.error.message}
        </span>
      </div>
    );
  }

  _displayRadioButtonGroup(input, index) {
    let inputContainerReplacement = null;
    let inputLabelReplacement = null;
    if (this.props.styles && this.props.styles.inputContainer) {
      inputContainerReplacement = this.props.styles.inputContainer;
    }
    if (this.props.styles && this.props.styles.inputLabel) {
      inputLabelReplacement = this.props.styles.inputLabel;
    }
    return (
      <div
        key={index}
        style={
          inputContainerReplacement
            ? inputContainerReplacement
            : styles.inputContainer
        }
      >
        <div style={{ margin: "0 0 0 15px" }}>
          {input.label ? (
            <span
              style={{
                fontWeight: "bold",
                padding: 0,
                margin: "0 0 -10px 15px"
              }}
            >
              {input.label}
            </span>
          ) : null}
          {this._displayRadioButton(input)}
        </div>
        <span
          style={
            input.error.active
              ? { ...styles.validationText, ...styles.validationTextVisible }
              : styles.validationText
          }
        >
          {input.error.message}
        </span>
      </div>
    );
  }

  _displayRadioButton(input) {
    const html = [];
    const options = input.options;
    for (let i = 0; i < options.length; i++) {
      html.push(
        <label className={checkBoxLabelStyle} key={i}>
          <input
            className={checkboxStyle}
            name="rd"
            type="radio"
            value={options[i].value}
            disabled={input.disabled}
            checked={options[i].isChecked}
            onChange={this._handleChange.bind(this, input)}
          />
          {options[i].label}
        </label>
      );
    }

    return <div className={checkBoxDiv}>{html}</div>;
  }

  _displayChecboxes(input) {
    const html = [];
    const options = input.options;
    for (let i = 0; i < options.length; i++) {
      html.push(
        <label className={checkBoxLabelStyle} key={i}>
          <input
            className={checkboxStyle}
            name={options[i].value.replace(/ /g, "")}
            type="checkbox"
            value={options[i].value}
            disabled={input.disabled}
            checked={options[i].isChecked}
            onChange={this._handleChange.bind(this, input)}
          />
          {options[i].label}
        </label>
      );
    }

    return <div className={checkBoxDiv}>{html}</div>;
  }

  _displayDropdown(input, index) {
    return (
      <div key={index} style={styles.inputContainer}>
        {input.label ? input.label : null}
        <select
          disabled={input.disabled}
          id={input.name}
          style={
            input.error.active
              ? { ...styles.formDropdown, ...styles.formInputError }
              : styles.formDropdown
          }
          value={input.value}
          onChange={this._handleSelect.bind(this)}
        >
          <option key="a" value="">
            Select {input.question}
          </option>
          {this._displayListOptions(input)}
        </select>
        <span
          style={
            input.error.active
              ? { ...styles.validationText, ...styles.validationTextVisible }
              : styles.validationText
          }
        >
          {input.error.message}
        </span>
      </div>
    );
  }

  _styleSelect(input) {
    if (input.error.active === true) {
      return {
        ...styles.options,
        ...styles.optionsDefault,
        ...styles.optionsError
      };
    } else if (input.value === "default") {
      return { ...styles.options, ...styles.optionsDefault };
    } else {
      return { ...styles.options };
    }
  }

  _displayListOptions(input) {
    const html = [];
    const options = input.options;
    for (let i = 0; i < options.length; i++) {
      if (options[i].label === input.value) {
        html.push(
          <option key={i} value={input.value}>
            {options[i].label}
          </option>
        );
      } else {
        html.push(
          <option key={i} value={options[i].value}>
            {options[i].label}
          </option>
        );
      }
    }

    return html;
  }

  _displayOptions(input) {
    const html = [];
    const options = input.options;
    for (let i = 0; i < options.length; i++) {
      if (options[i].label === input.value) {
        html.push(
          <button
            key={i}
            id={input.name}
            className={selectOptionFilledStyle}
            name={options[i].value}
            onClick={this._handleClick.bind(this)}
          >
            {options[i].label}
          </button>
        );
      } else if (input.value.split(",").indexOf(options[i].value) > -1) {
        html.push(
          <button
            key={i}
            id={input.name}
            className={selectOptionFilledStyle}
            name={options[i].value}
            onClick={this._handleClick.bind(this)}
          >
            {options[i].label}
          </button>
        );
      } else {
        html.push(
          <button
            key={i}
            id={input.name}
            className={selectOptionStyle}
            name={options[i].value}
            onClick={this._handleClick.bind(this)}
          >
            {options[i].label}
          </button>
        );
      }
    }

    return html;
  }

  _handleSelect(e) {
    const value = e.target.value;
    const name = e.target.id;

    const validated = this._validateInput(value, "dropdown");
    // //if validated is true, error.active should be false and vice versa
    this.props.handleChange(value, name, !validated);
  }

  _handleClick(e) {
    const value = e.target.name;
    const name = e.target.id;

    const validated = this._validateInput(value, "select");
    // //if validated is true, error.active should be false and vice versa
    this.props.handleChange(value, name, !validated);
  }

  _handleChange(input, event) {
    this.setState({
      btnTxt: this.props.buttonText
    });
    let value;
    if (input.type === "date") {
      value = event;
      input.question = "";
    } else {
      value = event.target.value;
    }
    const validated = this._validateInput(value, input.validation);
    //if validated is true, error.active should be false and vice versa
    this.props.handleChange(value, input.name, !validated, event);
  }

  _validateInput(input, type) {
    if (type === "email") {
      const ck_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      if (!ck_email.test(input)) {
        return false;
      }
      return true;
    } else if (type === "company") {
      let test = false;
      for (let i = 0; i < this.props.companies.length; i++) {
        if (this.props.companies[i].code == input) {
          test = true;
        }
      }
      return test;
    } else if (type === "name") {
      const ck_name = /^(.*)\s(.*)$/i;
      if (!ck_name.test(input) || !input) {
        return false;
      }
      return true;
    } else if (type === "password") {
      //  if (this.props.location !== 'profile') {
      const ck_password = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
      if (!ck_password.test(input)) {
        return false;
      }
      //  }
      return true;
    } else if (type === "birthday") {
      const eighteenYearsAgo = moment().subtract(18, "years");
      const birthday = moment(input);

      if (!birthday.isValid()) {
        return false;
      } else if (eighteenYearsAgo.isAfter(birthday)) {
        return true;
      } else {
        return false;
      }
    } else if (type === "number") {
      var ck_required = /^[0-9]+/;
      if (!ck_required.test(input)) {
        return false;
      }
      return true;
    } else if (type === "date") {
      const date = moment(input);

      if (!date.isValid()) {
        return false;
      }
      return true;
    } else if (type === "select") {
      if (input === "default") {
        return false;
      }
      return true;
    } else if (type === "dropdown") {
      if (input === "default" || input === "") {
        return false;
      }
      return true;
    } else if (type === "autocomplete") {
      var ck_required = /^[a-zA-Z\d]/;
      if (input === "default" || !ck_required.test(input)) {
        return false;
      }
      return true;
    } else if (type === "required") {
      var ck_required = /^[a-zA-Z\d]/;
      if (input === "default" || !ck_required.test(input)) {
        return false;
      }
      return true;
    }
    {
      return true;
    }
  }

  _allInputsValid() {
    const results = [];
    for (let i = 0; i < this.props.inputs.length; i++) {
      results.push(
        this._validateInput(
          this.props.inputs[i].value,
          this.props.inputs[i].validation
        )
      );
    }

    if (results.indexOf(false) !== -1) {
      return false;
    }
    return true;
  }

  _submitForm(e) {
    if (!this._allInputsValid()) {
      console.log("could not validate");
      for (let i = 0; i < this.props.inputs.length; i++) {
        const validated = this._validateInput(
          this.props.inputs[i].value,
          this.props.inputs[i].type
        );
        this.props.handleChange(
          this.props.inputs[i].value,
          this.props.inputs[i].name,
          !validated
        );
      }
      return;
    } else {
      this.props.submitForm(e);
    }
  }
}

let selectOptionStyle = css({
  padding: "10px 20px",
  backgroundColor: "#fff",
  border: "2px solid #36c4f1",
  color: "#000",
  borderRadius: "5px",
  margin: "5px",
  cursor: "pointer",
  ":hover": {
    backgroundColor: "#ED81B9",
    border: "2px solid #ED81B9",
    color: "white"
  }
});

let selectOptionFilledStyle = css({
  padding: "10px 20px",
  backgroundColor: "#eec5e2",
  border: "2px solid #d383b6",
  color: "#000",
  borderRadius: "5px",
  margin: "5px",
  cursor: "pointer"
});

let LockedSelectOptionFilledStyle = css({
  padding: "10px 20px",
  backgroundColor: "gray",
  color: "#fff",
  borderRadius: "5px",
  margin: "5px",
  cursor: "pointer"
});

const styles = {
  disabled: {
    padding: "13px 30px 13px 30px",
    marginTop: "20px",
    width: "247px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    outline: "none",
    background: "#d3d3d3"
  },
  formContainer: {
    width: "100%",
    JsDisplay: "flex",
    display: "-webkit-box" /* OLD - iOS 6-, Safari 3.1-6 */,
    display: "-moz-box" /* OLD - Firefox 19- (buggy but mostly works) */,
    display: "-ms-flexbox" /* TWEENER - IE 10 */,
    display: "-webkit-flex" /* NEW - Chrome */,
    display: "flex" /* NEW, Spec - Opera 12.1, Firefox 20+ */,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    textAlign: "center",
    borderBottom: "2px solid rgba(225,232,238,1)",
    padding: "15px 20px",
    color: "#364563",
    width: "95%",
    display: "flex",
    flexDirection: "column"
  },
  form: {
    width: "100%"
  },
  formError: {
    display: "none",
    marginLeft: "25px",
    marginTop: "5px",
    marginBottom: "10px",
    color: "#EA3131",
    transition: "color 0.3s ease-in-out"
  },
  formErrorActive: {
    display: "block"
  },
  formSuccess: {
    display: "none",
    marginLeft: "25px",
    marginTop: "5px",
    color: "rgb(3, 210, 127)",
    transition: "color 0.3s ease-in-out"
  },
  formSuccessActive: {
    display: "block"
  },
  formDropdown: {
    padding: "13px 30px 13px 30px",
    marginTop: "5px",
    width: "310px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    outline: "none"
  },
  formInput: {
    padding: "13px 30px 13px 30px",
    width: "100%",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    outline: "none"
  },
  formInputError: {
    border: "2px solid #EA3131",
    transition: "color 0.3s ease-in-out"
  },
  optionsContainer: {
    display: "flex",
    flexFlow: "row wrap",
    justifyContent: "center",
    alignItems: "center"
  },
  inputContainer: {
    display: "flex",
    flexFlow: "column wrap",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "10px"
  },
  label: {
    color: "#364563",
    marginBottom: "5px",
    marginTop: "10px",
    width: "100%",
    textAlign: "center"
  },
  inputLabel: {
    color: "#364563",
    marginBottom: "10px",
    marginTop: "10px",
    width: "100%",
    textAlign: "center"
  },
  selectOption: {
    padding: "10px 20px",
    backgroundColor: "white",
    border: "2px solid #77E9AF",
    color: "#77E9AF",
    borderRadius: "5px",
    margin: "5px"
  },
  options: {
    backgroundColor: "#fff"
  },
  optionsDefault: {
    color: "rgba(169, 169, 169, 1)"
  },
  optionsError: {
    border: "2px solid #EA3131",
    transition: "color 0.3s ease-in-out"
  },
  selectArrowZone: {
    cursor: "pointer",
    display: "table-cell",
    position: "relative",
    textAlign: "center",
    verticalAlign: "middle",
    width: "25px",
    paddingRight: "5px"
  },
  selectArrow: {
    borderColor: "#999 transparent transparent",
    borderStyle: "solid",
    borderWidth: "5px 5px 2.5px",
    display: "inline-block",
    height: "0",
    width: "0",
    position: "absolute",
    right: "-17em",
    top: "-2.75em"
  },
  validationText: {
    display: "none",
    marginLeft: "25px",
    marginTop: "5px",
    color: "#EA3131",
    transition: "color 0.3s ease-in-out"
  },
  validationTextVisible: {
    display: "block"
  },
  btn: {
    backgroundColor: "#36c4f1",
    width: "250px",
    padding: "11px 60px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    cursor: "pointer",
    display: "block",
    margin: "auto",
    marginTop: "0px",
    marginBottom: "20px"
  },
  lockedButton: {
    backgroundColor: "gray",
    width: "250px",
    padding: "11px 60px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    cursor: "pointer",
    display: "block",
    margin: "auto",
    marginTop: "0px",
    marginBottom: "20px"
  },

  disabledButton: {
    backgroundColor: "gray",
    width: "250px",
    padding: "11px 60px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    display: "block",
    margin: "auto",
    marginTop: "0px",
    marginBottom: "20px"
  }
};

const textArea = css({
  display: "block",
  boxSizing: "padding-box",
  overflow: "hidden",
  padding: "10px",
  width: " 90%",
  maxWidth: "90%",
  minHeight: "50px",
  borderRadius: "6px"
});

let checkBoxDiv = css({
  padding: 10,
  width: "100%",
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "flex-start"
});

let checkboxStyle = css({
  margin: "5px 15px 5px 5px",
  border: "1px dotted gray",
  zIndex: 0
});

let checkBoxLabelStyle = css({
  flex: "1 0 100%",
  textAlign: "left"
});
