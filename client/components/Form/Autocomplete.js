import React from "react";
import { css } from "glamor";
// eslint-disable-next-line import/no-extraneous-dependencies
import PropTypes from "prop-types";

import ConcernService from "../../utils/concernService";
import { inputClass } from "../NewSkin/Styles";

const concern = new ConcernService();

export default class extends React.Component {
  static propTypes = {
    suggestions: PropTypes.instanceOf(Array)
  };

  static defaultProps = {
    suggestions: []
  };

  constructor(props) {
    super(props);
    this.input = this.props.input;
    this.state = {
      sgst: [],
      // The active selection's index
      activeSuggestion: 0,
      // The suggestions that match the user's input
      filteredSuggestions: [],
      // Whether or not the suggestion list is shown
      showSuggestions: false,
      // What the user has entered
      userInput: this.props.input.value ? this.props.input.value : "",
      message: {
        goodMessage: "",
        badMessage: ""
      }
    };

    this.onLoadCheckSuggest = this.onLoadCheckSuggest.bind(this);
  }

  componentDidMount() {
    this._getConcernList();
  }

  componentWillUnmount() {
    // use intervalId from the state to clear the interval
  }

  onLoadCheckSuggest(str) {
    const userInput = str;
    // Filter our suggestions that don't contain the user's input
    const filteredSuggestions = this.state.sgst.filter(
      suggestion =>
        suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );

    this.setState({
      activeSuggestion: 0,
      filteredSuggestions,
      showSuggestions: filteredSuggestions === "",
      userInput: str
    });
    this.props._event(str);
  }

  onChange = e => {
    const userInput = e.currentTarget.value.trim();
    // Filter our suggestions that don't contain the user's input
    const filteredSuggestions = this.state.sgst.filter(
      suggestion =>
        suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions,
      showSuggestions: true,
      userInput: e.currentTarget.value
    });
    this.props._event(e.currentTarget.value);
  };

  onClick = e => {
    this.setState({
      activeSuggestion: 0,
      filteredSuggestions: [],
      showSuggestions: false,
      userInput: e.currentTarget.innerText
    });
    this.props._event(e.currentTarget.innerText);
  };

  onFocus = e => {
    this.setState({
      showSuggestions: true
    });
    this.onLoadCheckSuggest(this.props.input.value);
  };

  onKeyDown = e => {
    const { activeSuggestion, filteredSuggestions } = this.state;

    // User pressed the enter key
    if (e.keyCode === 13) {
      this.setState({
        activeSuggestion: 0,
        showSuggestions: false,
        userInput: filteredSuggestions[activeSuggestion]
      });
      this.props._event(filteredSuggestions[activeSuggestion]);
      // eslint-disable-next-line brace-style
    }
    // User pressed the up arrow
    else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion - 1 });
      // eslint-disable-next-line brace-style
    }
    // User pressed the down arrow
    else if (e.keyCode === 40) {
      if (activeSuggestion - 1 === filteredSuggestions.length) {
        return;
      }

      this.setState({ activeSuggestion: activeSuggestion + 1 });
    }
  };

  _getConcernList() {
    concern
      .get()
      .then(result => {
        const conscernList = [];
        result.forEach(item => {
          conscernList.push(item.name);
        });
        let sgst = this.state.sgst;
        sgst = conscernList;
        this.setState({ sgst }, () => {
          // if (this.props.input.value !== "") {
            this.onLoadCheckSuggest(this.props.input.value);
          // }
        });
      })
      .catch(e => {
        console.log("_getConcernList : Error ");
        console.log(e);
      });
  }

  _addToList(str) {
    if (str !== "") {
      this.props
        .addToList(str)
        .then(res => {
          if (res) {
            this._showMessage("Added to list", "goodMessage");
            this._getConcernList();
          } else {
            this._showMessage("Error Adding to list", "badMessage");
            this._getConcernList();
          }
        })
        .catch(e => {
          this._showMessage("Error Adding to list!", "badMessage");
          console.log(e);
        });
    }
  }

  _showMessage(str, statename) {
    const message = this.state.message;
    message[statename] = str;
    this.setState({ message });
    setTimeout(() => {
      message.goodMessage = "";
      message.badMessage = "";
      this.setState({ message });
    }, 10000);
  }

  render() {
    const {
      onChange,
      onClick,
      onKeyDown,
      onFocus,
      state: {
        activeSuggestion,
        filteredSuggestions,
        showSuggestions,
        userInput
      }
    } = this;

    let suggestionsListComponent;

    if (showSuggestions) {
      if (filteredSuggestions.length) {
        suggestionsListComponent = (
          <ul className={suggestionsStyle}>
            {filteredSuggestions.map((suggestion, index) => {
              // eslint-disable-next-line no-unused-vars
              let className;

              // Flag the active suggestion with a class
              if (index === activeSuggestion) {
                className = "suggestion-active";
              }

              return (
                <li
                  className={suggestionsItemStyle}
                  key={suggestion}
                  onClick={onClick}
                >
                  {suggestion}
                </li>
              );
            })}
          </ul>
        );
      } else {
        suggestionsListComponent = (
          <div className="no-suggestions">
            {this.props.user.type === "Doctor" ? (
              <span
                className={addTolistStyle}
                onClick={() => this._addToList(userInput)}
              >
                Add to list [+]
              </span>
            ) : null}
          </div>
        );
      }
    }

    return (
      <div style={{ width: "100%", position: "relative" }}>
        {/**
          <input placeholder={input.question}
            type={input.type}
            style={input.disabled? styles.disabled: input.error.active ? {...styles.formInput,...styles.formInputError} : styles.formInput}
            onChange={this._handleChange.bind(this, input)} value={input.value} disabled={input.disabled}
            hidden={input.hidden}>
            </input>
            **/}
        <input
          data-id="appointment"
          style={
            // eslint-disable-next-line no-nested-ternary
            this.props.appointment
              ? {}
              : this.input.disabled
              ? styles.disabled
              : this.input.error.active
              ? { ...styles.formInput, ...styles.formInputError }
              : styles.formInput
          }
          type="text"
          onChange={onChange}
          onKeyDown={onKeyDown}
          value={userInput}
          className={this.props.appointment ? inputClass : ""}
          onFocus={onFocus}
        />
        {this.state.message.goodMessage !== "" ||
        this.state.message.badMessage !== "" ? (
          <span
            className={
              this.state.message.goodMessage
                ? goodMessageStyle
                : badMessageStyle
            }
          >
            {this.state.message.goodMessage
              ? this.state.message.goodMessage
              : this.state.message.badMessage}
          </span>
        ) : null}

        <section className={suggestionsWrapper}>
          {suggestionsListComponent}
        </section>
      </div>
    );
  }
}

const suggestionsWrapper = css({
  position: "absolute",
  bottom: 0,
  left: 0,
  width: "100%",
  transform: "translateY(100%)",
  background: "white",
  boxShadow: "0 5px 10px rgba(0,0,0,.1)",
  zIndex: 100
});

const styles = {
  disabled: {
    padding: "13px 30px 13px 30px",
    marginTop: "20px",
    width: "247px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    // fontSize: "14px",
    outline: "none",
    background: "#d3d3d3"
  },
  formContainer: {
    // border: '2px solid rgba(151,151,151,0.2)',
    // borderRadius: '6px',
    width: "100%",
    JsDisplay: "flex",
    display: "-webkit-box" /* OLD - iOS 6-, Safari 3.1-6 */,
    display: "-moz-box" /* OLD - Firefox 19- (buggy but mostly works) */,
    display: "-ms-flexbox" /* TWEENER - IE 10 */,
    display: "-webkit-flex" /* NEW - Chrome */,
    display: "flex" /* NEW, Spec - Opera 12.1, Firefox 20+ */,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    textAlign: "center",
    borderBottom: "2px solid rgba(225,232,238,1)",
    padding: "15px 20px",
    fontSize: "18px",
    color: "#364563",
    width: "95%",
    display: "flex",
    flexDirection: "column"
  },
  form: {
    // margin: "10px 30px 20px 20px"
  },
  formError: {
    display: "none",
    fontSize: "0.8em",
    marginLeft: "25px",
    marginTop: "5px",
    marginBottom: "10px",
    color: "#EA3131",
    transition: "color 0.3s ease-in-out"
  },
  formErrorActive: {
    display: "block"
  },
  formSuccess: {
    display: "none",
    fontSize: "0.8em",
    marginLeft: "25px",
    marginTop: "5px",
    color: "rgb(3, 210, 127)",
    transition: "color 0.3s ease-in-out"
  },
  formSuccessActive: {
    display: "block"
  },
  formDropdown: {
    padding: "13px 30px 13px 30px",
    marginTop: "5px",
    width: "310px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    // fontSize: "14px",
    outline: "none"
  },
  formInput: {
    padding: "13px 30px 13px 30px",
    //marginTop: '20px',
    width: "100%",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    // fontSize: "14px",
    outline: "none"
  },
  formInputError: {
    border: "2px solid #EA3131",
    transition: "color 0.3s ease-in-out"
  },
  optionsContainer: {
    display: "flex",
    flexFlow: "row wrap",
    justifyContent: "center",
    alignItems: "center"
  },
  inputContainer: {
    display: "flex",
    flexFlow: "column wrap",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "10px"
  },
  label: {
    color: "#364563",
    marginBottom: "5px",
    marginTop: "10px",
    width: "100%",
    textAlign: "center"
  },
  inputLabel: {
    color: "#364563",
    marginBottom: "10px",
    marginTop: "10px",
    width: "100%",
    textAlign: "center"
  },
  selectOption: {
    padding: "10px 20px",
    backgroundColor: "white",
    border: "2px solid #77E9AF",
    color: "#77E9AF",
    borderRadius: "5px",
    margin: "5px",
    // fontSize: "12px"
  },
  options: {
    backgroundColor: "#fff"
  },
  optionsDefault: {
    color: "rgba(169, 169, 169, 1)"
  },
  optionsError: {
    border: "2px solid #EA3131",
    transition: "color 0.3s ease-in-out"
  },
  selectArrowZone: {
    cursor: "pointer",
    display: "table-cell",
    position: "relative",
    textAlign: "center",
    verticalAlign: "middle",
    width: "25px",
    paddingRight: "5px"
  },
  selectArrow: {
    borderColor: "#999 transparent transparent",
    borderStyle: "solid",
    borderWidth: "5px 5px 2.5px",
    display: "inline-block",
    height: "0",
    width: "0",
    position: "absolute",
    right: "-17em",
    top: "-2.75em"
  },
  validationText: {
    display: "none",
    // fontSize: "0.8em",
    marginLeft: "25px",
    marginTop: "5px",
    color: "#EA3131",
    transition: "color 0.3s ease-in-out"
  },
  validationTextVisible: {
    display: "block"
  },
  button: {
    backgroundColor: "#36c4f1",
    width: "250px",
    padding: "11px 60px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    cursor: "pointer",
    display: "block",
    margin: "auto",
    marginTop: "0px",
    marginBottom: "20px"
  },
  lockedButton: {
    backgroundColor: "gray",
    width: "250px",
    padding: "11px 60px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    cursor: "pointer",
    display: "block",
    margin: "auto",
    marginTop: "0px",
    marginBottom: "20px"
  },

  disabledButton: {
    backgroundColor: "gray",
    width: "250px",
    padding: "11px 60px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    display: "block",
    margin: "auto",
    marginTop: "0px",
    marginBottom: "20px"
  }
};

let suggestionsStyle = css({
  padding: 10,
  // background: "#eee",
  listStyle: "none"
});

let suggestionsItemStyle = css({
  padding: 5,
  transition: "250ms ease",
  ":hover": {
    background: "#ccc2"
  },
  cursor: "pointer"
});

let addTolistStyle = css({
  position: "relative",
  borderRadius: 5,
  border: "1px dotted",
  margin: "5px, 0",
  padding: 5,
  fontSize: "x-small",
  fontStyle: "italic",
  background: "#bbe0ef",
  ":hover": {
    background: "#87cde9"
  },
  cursor: "pointer"
});

let goodMessageStyle = css({
  position: "relative",
  borderRadius: 5,
  border: "1px dotted",
  margin: "5px, 0",
  padding: 5,
  fontSize: "x-small",
  fontStyle: "italic",
  background: "#bbe0ef",
  color: "white"
});

let badMessageStyle = css({
  position: "relative",
  borderRadius: 5,
  border: "1px dotted",
  margin: "5px, 0",
  padding: 5,
  fontSize: "x-small",
  fontStyle: "italic",
  background: "red",
  color: "white"
});
