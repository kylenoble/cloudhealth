import React, { useEffect, useState } from 'react';
import GridContainer from '../../NewSkin/Wrappers/GridContainer';
import Colors from '../../NewSkin/Colors';
import { css } from 'glamor';
import { HealthStatusColor } from '../../NewSkin/Styles';
import { GetStatus } from '../../Commons';

const CompanyInfo = ({ logo = '', activeReport = {} }) => {

  const [score, setScore] = useState(null);
  const [status, setHealtStatus] = useState();
  const [rank, setRank] = useState(null);

  useEffect(() => {
    setScore(activeReport.score);
    setHealtStatus(activeReport.health_status);
    setRank(activeReport.percentile_rank);
  }, []);

  const alignCenter = { textAlign: 'left', justifyContent: 'left' };
  const title = { color: Colors.blueDarkAccent, fontWeight: 500 };

  return (
    <GridContainer columnSize="200px 1fr" childStyles={alignCenter}>
      <article style={{ width: 200, overflowX: 'hidden', textAlign: 'center' }}>
        {logo ? (
          <img
            alt="Company Logo"
            src={`data:image/jpeg;base64,${logo}`}
            style={{ height: 100 }}
          />
        ) : null}
      </article>

      <GridContainer>

        <GridContainer columnSize="1fr .5fr" childStyles={alignCenter}>
          <p style={title}>Company Overall Health Risk Score:</p>

          <strong className={riskScore} style={{ background: HealthStatusColor(status) }}>
            {score ? score : 0}{status && ', '} {GetStatus(score)}
          </strong>
        </GridContainer>

        <GridContainer columnSize="1fr 1fr" childStyles={alignCenter}>
          <p style={title}>Percentile Rank:</p>

          <strong style={{ color: !rank && '#ccc' }}>{rank ? rank : 0}%</strong>
        </GridContainer>

      </GridContainer>

    </GridContainer>
  );
}

const riskScore = css({
  padding: '0 10px',
  borderRadius: 5,
  color: '#fff',
  width: 'fit-content',
  display: 'grid',
  placeContent: 'center'
})

export default CompanyInfo;