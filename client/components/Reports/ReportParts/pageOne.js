import React from 'react';
import GridContainer from '../../NewSkin/Wrappers/GridContainer';
import { css } from 'glamor';
import Colors from '../../NewSkin/Colors';
import moment from 'moment-timezone';
import { blue__dark_accent_color } from '../../NewSkin/Styles';
import CHAHeader from '../../NewSkin/Wrappers/CHAHeader';

const PageOne = ({ companyName = 'Company Name', report = {} }) => {
  return (
    <GridContainer classes={[wrapper]} childStyles={{ textAlign: 'left', justifyContent: 'left' }}>
      <CHAHeader />

      <section className={chirTitle}>

        <h1 className={corp}>CORPORATE<br />HEALTH<br />INDEX<br />REPORT</h1>
        <h2 className={blue__dark_accent_color}><strong>{companyName}</strong></h2>

        <div className={toBottom}>
          <h4 className={`${border} ${padding}`}>{moment(report.date_created).format('MMMM DD YYYY')}</h4>

          <GridContainer columnSize="150px 1fr" childStyles={{ textAlign: 'left', justifyContent: 'left' }}>
            <h4 className={blue__dark_accent_color}>Report Name: </h4>
            <h4><strong>{report.report_name}</strong></h4>
          </GridContainer>

          <GridContainer columnSize="150px 1fr" childStyles={{ textAlign: 'left', justifyContent: 'left' }}>
            <h4 className={blue__dark_accent_color}>Prepared By: </h4>
            <h4><strong>{report.md_incharge}</strong></h4>
          </GridContainer>
        </div>

      </section>
    </GridContainer>
  );
}

const padding = css({
  padding: '10px 0'
})

const toBottom = css({
  position: 'absolute',
  bottom: 0,
  left: 0,
  padding: 20,
  boxSizing: 'border-box',
  width: '100%',
});

const border = css({
  borderBottom: `5px solid ${Colors.blueDarkAccent}`
})

const corp = css({
  lineHeight: 1.5,
  fontSize: 50,
  fontWeight: 200,
  marginBottom: 70,
})

const wrapper = css({
  textAlign: 'left !important',
  padding: 20,
  '@media print': {
    padding: 0,
    margin: 0
  }
})

const chirTitle = css({
  position: 'relative',
  width: '65%',
  background: Colors.skyblue,
  color: '#fff',
  textAlign: 'left !important',
  padding: 20,
  height: 1000
})

export default PageOne;