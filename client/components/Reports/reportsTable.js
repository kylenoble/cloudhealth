import React, { useEffect, useState } from "react";
import ReactTable from "react-table";
import { ReactTableStyle, combined } from "../NewSkin/Styles";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Button from "../NewSkin/Components/Button";
import NoData from "../NewSkin/Components/NoData";
import { css } from "glamor";
import moment from "moment-timezone";
import Colors from "../NewSkin/Colors";
import Dropzone from "react-dropzone";
import { tooltipWrapper } from "../Commons/tooltip";
import ReactTooltip from "react-tooltip";
import { confirmAlert } from "react-confirm-alert"; // Import

const ReportsTable = ({
  reports = [],
  activeReport = {},
  getCurrentReport = () => { },
  sendToHR = () => { },
  removeReport = () => { },
  finalizeReport = () => { },
  showReport = () => { }
}) => {
  const [updatedReports, setUpdatedReports] = useState({});
  const [currentID, setCurrentID] = useState(null);

  useEffect(() => {

    const updated = reports.map(r => {
      if (r.id === activeReport.id) {
        r.active = true;
        return r;
      }
      r.active = false;
      return r;
    });

    setUpdatedReports(updated);
  }, [activeReport]);

  const currentReport = (id, isPrint = false) => {
    setCurrentID(id);
    getCurrentReport(id, isPrint);
    showReport();
    // setTimeout(() => {
    //   showReport();
    // }, 100);
  };

  const actionAlert = (id, report_name, action) => {
    let label = action === "Remove" ? "Yes" : "Report is final";
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: `${action} Report`, // Title dialog
      message: `Are you sure you want to ${action.toLowerCase()} the report ${report_name}?`, // Message dialog
      buttons: [
        {
          label: label,
          onClick: () => {
            action === "Remove" ? removeReport(id) : finalizeReport(true);
          }
        },
        {
          label: "Cancel"
        }
      ]
    });
  };

  const finalizeButton = (status, id, i, report_name) => {
    if (status !== null) {
      return (
        <Dropzone
          onDrop={files => {
            getCurrentReport(id);
            setTimeout(() => {
              sendToHR(files, "PDFReport", i);
            }, 100);
          }}
          multiple={false}
          accept="application/pdf"
        >
          {({ getRootProps, getInputProps }) => (
            <button
              data-type="light"
              data-html
              data-tip={tooltipWrapper({
                description:
                  status === 2
                    ? "Report already sent to HR"
                    : "Upload the final report and send to HR by saving the report as PDF using the <strong>PRINT</strong> button."
              })}
              {...getRootProps()}
              style={{ fontSize: 12.8 }}
            >
              {
                status !== 2 &&
                <input {...getInputProps()} />
              }
              SEND TO HR
            </button>
          )}
        </Dropzone>
      );
    }

    return (
      <button
        onClick={() => {
          getCurrentReport(id);
          actionAlert(id, report_name, "Finalize");
        }}
      >
        FINALIZE
      </button>
    );
  };

  const renderColumns = () => {
    let columns = [];
    if (updatedReports.length > 0) {
      updatedReports.filter((report, i) => {
        columns = [
          {
            Header: "Subscription",
            accessor: "subscription_name",
            style: { textAlign: "center" },
            width: 270
          },
          {
            Header: "Report Name",
            accessor: "report_name",
            Cell: report => (
              <span
                data-tip={tooltipWrapper({
                  title: report.value,
                  description: "Click to view report"
                })}
                data-type="light"
                data-html
                style={{
                  cursor: "pointer",
                  color: Colors.skyblue,
                  fontWeight: 500
                }}
                onClick={() => currentReport(report.original.id)}
                title={report.value}
              >
                {report.value}
              </span>
            ),
            style: { textAlign: "center" },
            width: 270
          },
          {
            Header: "Last Update",
            accessor: "date_created",
            Cell: report => (
              <label>
                {moment(
                  report.original.date_modified
                    ? report.original.date_modified
                    : report.value
                ).format("MMM DD, 'YY")}
              </label>
            ),
            style: { textAlign: "center" },
            width: 105
          },
          {
            Header: "Lead MD",
            accessor: "md_incharge",
            style: { textAlign: "center" },
            width: 200
          },
          {
            Header: "Status",
            accessor: "status",
            Cell: report => (
              <label className={status(report.value)}>
                {report.value ? "Final" : "Draft"}
              </label>
            ),
            style: { textAlign: "center" },
            width: 70
          },
          {
            Header: "Action",
            accessor: "id",
            Cell: report => (
              <GridContainer columnSize="1fr 1fr 1fr" gap={10}>
                <ReactTooltip />

                <Button
                  disabled={report.original.status === 2}
                  styles={{
                    minWidth: 124,
                    width: "fit-content",
                    margin: "0 auto"
                  }}
                  type={
                    report.original.status !== null
                      ? report.original.status === 2
                        ? "disabled"
                        : "blue"
                      : "warning"
                  }
                >
                  {finalizeButton(
                    report.original.status,
                    report.value,
                    i,
                    report.original.report_name
                  )}
                </Button>

                <Button
                  styles={{
                    minWidth: "null !important",
                    width: "fit-content",
                    margin: "0 auto"
                  }}
                  type="pink"
                >
                  <button
                    onClick={() =>
                      actionAlert(
                        report.value,
                        report.original.report_name,
                        "Remove"
                      )
                    }
                  >
                    DELETE
                  </button>
                </Button>

                <Button
                  type={
                    report.original.status !== null ? "default" : "disabled"
                  }
                  styles={{
                    minWidth: "null !important",
                    width: "fit-content",
                    lineHeight: "14px",
                    margin: "0 auto"
                  }}
                >
                  {report.original.status !== null ? (
                    <button onClick={() => currentReport(report.value, true)}>
                      PRINT
                    </button>
                  ) : (
                      <label
                        data-type="light"
                        data-html
                        data-tip={tooltipWrapper({
                          description: "You can only print the <strong>FINAL</strong> report."
                        })}
                      >
                        PRINT
                    </label>
                    )}
                </Button>
              </GridContainer>
            ),
            style: { textAlign: "center", width: "250px !important" }
          }
        ];
      });
      return (
        <ReactTable
          data={reports}
          columns={columns}
          defaultPageSize={5}
          minRows={1}
          className="-highlight"
          resizable={false}
        />
      );
    }
    return <NoData />;
  };

  return (
    <section className={combined([ReactTableStyle, wrapper])}>
      {renderColumns()}
    </section>
  );
};

const status = status =>
  css({
    color: status ? Colors.skyblue : Colors.nutritionColor,
    fontWeight: 500
  });

const wrapper = css({
  width: "100%",
  maxWidth: "100%",

  "& .ReactTable .rt-tbody .rt-td:last-child": {
    width: "280px !important"
  },
  "& .ReactTable .rt-thead .rt-resizable-header:last-child": {
    width: "280px !important"
  }
});

export default ReportsTable;
