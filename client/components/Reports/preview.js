import Reacr from "react";
import { css } from "glamor";
import Modal from "react-modal";

import TableToExcel from "react-html-table-to-excel";
import { scrollClass } from "../NewSkin/Styles";
import Button from "../NewSkin/Components/Button";
import Label from "../NewSkin/Components/Label";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Colors from "../NewSkin/Colors";

export default class extends Reacr.Component {
  constructor(props) {
    super(props);
    this.headerNames = {
      email_address: "Email Address",
      name: "Name",
      branch: "Branch",
      department: "Department",
      concern: "Concern",
      concern_details: "Concern Details",
      concern2: "2nd Concern",
      concern2_details: "2nd Concern Details",
      concern3: "3rd Concern",
      concern3_details: "3rd Concern Details"
    };

    this.OthersHeaders = {
      email_address: "Email Address",
      name: "Name",
      branch: "Branch",
      department: "Department",
      issue: "Other Health Illnesses"
    }
    this.state = {
      data: this.props.data,
      report_name: this.props.report_name,
      company_name: this.props.company_name,
      loading: true,
      exelData: [],
      modal: {
        preview: false
      }
    };

    this._prepareDataToPrint = this._prepareDataToPrint.bind(this);
  }

  componentDidMount() {
    this.props.issues ? this._prepareOthersToPrint() : this._prepareDataToPrint()
  }

  modalSwitch(name) {
    const modal = this.state.modal;
    modal[name] = !modal[name];
    this.setState({
      modal
    });
  }

  _getOtherIssues = (user_id, user_issues) => {
    const user_issues_array = []
    const mapped_issues = user_issues.map(issue => {
      const index = user_issues_array.findIndex(uia => uia['user_id'] == user_id)
      if (index === -1) {
        const h_issue = issue.health_issues.trim().split(',')

        const filtered_issue = h_issue.filter(i => {
          if (i != "" && i != 'chronic' && i != 'hypertension' && i != 'stroke' && i != 'cancer' && i != 'weight' && i != 'diabetes' && i != 'heart' && i != 'none') {
            return i
          }
        })

        const user_issue = {
          user_id: issue.user_id,
          issues: filtered_issue
        }

        user_issues_array.push(user_issue)

        return filtered_issue
      }
    })
    return mapped_issues
  }

  _prepareOthersToPrint = () => {
    const { data, issues } = this.props;
    const { exelData } = this.state;

    data.forEach(item => {
      const email = item.email;
      const user_issue = issues.filter(issue => issue.user_id == item.user_id)
      const index = exelData.findIndex(item => item["email_address"] == email);

      if (index === -1) {
        const issues = this._getOtherIssues(item.user_id, user_issue)

        let new_issues = ''

        if (issues.length > 0 && issues[0].length > 0) {
          for (const issue of issues[0]) {
            if (issues[0].indexOf(issue) + 1 == issues[0].length) {
              new_issues += ` ${issue}`
            } else {
              new_issues += ` ${issue},`
            }
          }
        } else {
          new_issues = '-'
        }

        exelData.push({
          email_address: item.email,
          name: item.name,
          branch: item.branch,
          department: item.department,
          issue: new_issues
        })
      }
      this.setState(exelData);
    });
  }

  _prepareDataToPrint = () => {
    const { data } = this.props;
    // PREPARE DATA TO EXPORT

    const { exelData } = this.state;

    data.forEach((item, i) => {
      const email = item.email;

      const index = exelData.findIndex(item => item["email_address"] == email);
      if (index === -1) {
        exelData.push({
          email_address: item.email,
          name: item.name,
          branch: item.branch,
          department: item.department,
          concern: item.doh,
          concern_details: item.dod
        });
      } else {
        if (exelData[index].hasOwnProperty("concern2")) {
          exelData[index]["concern3"] = item.doh;
          exelData[index]["concern3_details"] = item.dod;
        } else {
          exelData[index]["concern2"] = item.doh;
          exelData[index]["concern2_details"] = item.dod;
        }
      }
      this.setState(exelData);
    });
  };

  _thead() {
    const header = this.state.exelData[0];
    let th;
    if (header) {
      th = Object.keys(header).map((name, i) => {
        // let title = name.replace(/[^a-z0-9]|\s+|\r?\n|\r/gim, " ");
        // const title = this.headerNames[name]
        const title = this.props.issues ? this.OthersHeaders[name] : this.headerNames[name]
        return (
          <th key={i} style={{ fontSize: 'smaller', textTransform: 'uppercase', color: '#fff', padding: 5, width: '100px !important' }}>
            {title}
          </th>
        );
      });
    }

    return (
      <thead style={{
        boxShadow: '0 2px 15px 0 rgba(0,0,0,0.15)', backgroundColor: `${Colors.skyblue}`
      }}>
        <tr>{th}</tr>
      </thead>
    );
  }

  _tbody() {
    const body = this.state.exelData;
    let tr;
    // let td;
    if (body) {
      tr = body.map((name, i) => {
        let td = Object.keys(name).map((item, j) => {
          return (
            <td key={j} style={{ fontSize: 'smaller' }}>
              {name[item]}
            </td>
          );
        });
        //let title = name.replace(/[^a-z0-9]|\s+|\r?\n|\r/gim, " ");
        return <tr key={i} style={{ borderBottom: '1px dotted #ccc' }}>{td}</tr>;
      });
    }

    return <tbody>{tr}</tbody>;
  }

  render() {
    return (
      <>
        <Button type="darkpink" fitContent right styles={{ marginBottom: 0, borderRadius: 8, fontSize: 'small' }}>
          <button
            onClick={() => this.modalSwitch("preview")}>View graph details</button>
        </Button>

        <Modal
          isOpen={this.state.modal.preview}
          onRequestClose={() => this.modalSwitch("preview", false)}
          shouldCloseOnOverlayClick={false}
          style={customStyle}
        >
          <GridContainer columnSize={"1fr .5fr .5fr"} gap={20} styles={{ marginBottom: 20 }}>
            <Label
              text="Concern details"
              styles={{ padding: '3px 10px' }}
            />
            <Button right styles={{ marginBottom: 0 }}>
              <button
                onClick={() => this.modalSwitch("preview", false)}>Close</button>
            </Button>

            <Button left type="pink" styles={{ marginBottom: 0 }}>
              <TableToExcel
                table="table-to-xls"
                filename={`${this.state.company_name}-${this.state.report_name}`}
                sheet={this.state.report_name}
                buttonText="Download Graph Details"
              />
            </Button>

          </GridContainer>
          <section style={{
            maxHeight: '69vh',
            width: 'fit-content !important',
            overflow: 'auto',
            position: 'relative',
            maxWidth: '90vw'
          }} className={scrollClass}>

            <table id="table-to-xls" style={{ borderCollapse: 'collapse', width: '100%' }}>
              {this._thead()}
              {this._tbody()}
            </table>
          </section>

        </Modal>
      </>
    );
  }
}

const styles = {
  tableStyle: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
    width: "100%"
  },
  titleStyle: {
    width: 200,
    padding: "3px 10px",
    margin: 0,
    background: `${Colors.skyblue}`,
    color: "#fff"
  },
  tdStyle: {
    width: 200,
    padding: "3px 10px",
    cellpadding: 10,
    margin: 0,
    border: "1px dotted gray"
  }
};

const customStyle = {
  content: {
    textAlign: "center",
    zIndex: 6,
    width: "80%",
    height: "auto",
    top: "55%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    transform: "translate(-50%, -50%)",
    padding: 20,
    boxShadow: `0 8px 15px rgba(0,0,0,.1)`,
    border: `1px solid rgba(0,0,0,0)`,
    borderRadius: 10,
    maxWidth: '90%',
    maxHeight: '85vh',
    minHeight: '50vh'
  }
};