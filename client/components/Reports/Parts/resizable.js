import React from "react";
import { css } from "glamor";
import Resizable from "re-resizable";

class ResizableDiv extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      //filters: this.props.filters,
      resizeLock: this.props.resizeLock || false,
      isGraph: this.props.isGraph,
      isResizable: this.props.isResizable,
      defaultSize: this.props.defaultSize,
      maxWidth: "100%",
      name: this.props.name,
      boxProps: {
        width: 500,
        height: 100,
        alignment: "center"
      },
      noOption: this.props.noOption || false
    };
  }

  componentDidMount() {
    if (this.props.defaultSize) {
      this._setDefaultSize();
    }

    // let width;
    // let height;
    // let align;
    // let filters = this.props.filters;
    // filters[this.state.name] = {};

    const { boxProps, name } = this.state;
    if (this.props.filters && this.props.filters[name]) {
      const f = this.props.filters[name];
      boxProps.width = f.width;
      boxProps.height = f.height;
      boxProps.alignment = f.alignment;
      this.setState({ boxProps });
    }

    if (this.props.status === 1) {
      const recos = name.split("_");
      if (recos && recos[1] === "recommendation") {
        boxProps.height -= 75;
        this.setState({
          boxProps
        });
      }
    }
  }

  _setDefaultSize() {
    const { boxProps } = this.state;

    const d = this.state.defaultSize;
    boxProps.width = d.width;
    boxProps.height = d.height;
    this.setState({ boxProps });
  }

  _setDivAlignment(alignment) {
    const { boxProps } = this.state;
    boxProps.alignment = alignment;
    this.setState({
      boxProps
    });

    // let filters = this.state.filters;
    // filters[this.state.name].alignment = boxProps.alignment;

    // this.setState({ filters });
    this._saveToFilters();
  }

  _saveToFilters() {
    const { boxProps } = this.state;
    this.props.setDivAlignment(boxProps, this.state.name);
  }

  _setProperties(d) {
    const { boxProps } = this.state;
    // eslint-disable-next-line no-unused-expressions
    boxProps.width += d.width;
    boxProps.height += d.height;
    this.setState({
      boxProps
    });

    this.setState({ boxProps });
    this._saveToFilters();
  }

  render() {
    return (
      <React.Fragment>
        {this.state.noOption ? null : (
          <div className={alignmentHoverDiv}>
            <div> Position :</div>
            <div
              className={alignmentButtonStyle}
              onClick={() => this._setDivAlignment("initial")}
            >
              Left
            </div>
            {/* <div
            className={alignmentButtonStyle}
            onClick={() => this._setDivAlignment("center")}
          >
            Center
          </div> */}
            <div
              className={alignmentButtonStyle}
              onClick={() => this._setDivAlignment("row-reverse")}
            >
              Right
            </div>
          </div>
        )}
        {this.state.isGraph ? (
          <Resizable
            enable={{
              top: !this.state.resizeLock,
              right: !this.state.resizeLock,
              bottom: !this.state.resizeLock,
              left: !this.state.resizeLock,
              topRight: !this.state.resizeLock,
              bottomRight: !this.state.resizeLock,
              bottomLeft: !this.state.resizeLock,
              topLeft: !this.state.resizeLock
            }}
            className={this.state.resizeLock ? notResizable(this.props.fromReport) : resizable(this.props.fromReport)}
            maxWidth={this.state.maxWidth ? this.state.maxWidth : "100%"}
            size={{
              width: this.state.boxProps.width,
              height: this.state.boxProps.height
            }}
            defaultSize={{
              width: "100%",
              height: "100%"
            }}
            onResizeStop={(e, direction, ref, d) => {
              this._setProperties(d);
            }}
          >
            {/* {this.state.boxProps.width} {this.state.boxProps.height} */}
            {this.props.children}
          </Resizable>
        ) : (
            <Resizable
              enable={{
                top: !this.state.resizeLock,
                right: !this.state.resizeLock,
                bottom: !this.state.resizeLock,
                left: !this.state.resizeLock,
                topRight: !this.state.resizeLock,
                bottomRight: !this.state.resizeLock,
                bottomLeft: !this.state.resizeLock,
                topLeft: !this.state.resizeLock
              }}
              className={resizable}
              maxWidth={this.state.maxWidth ? this.state.maxWidth : "100%"}
              onResizeStop={(e, direction, ref, d) => {
                this._setProperties(d);
              }}
            >
              {/* {this.state.boxProps.width} {this.state.boxProps.height} */}
              {this.props.children}
            </Resizable>
          )}
      </React.Fragment>
    );
  }
}

const alignmentHoverDiv = css({
  background: "tranparent",
  margin: 0,
  padding: 0,
  display: "flex",
  width: "100%",
  justifyContent: "center",
  alignItems: "center",
  fontSize: "x-small",
  opacity: 0,
  ":hover": {
    opacity: 1
  },
  "@media print": {
    display: "none"
  }
});

const alignmentButtonStyle = css({
  //alignSelf: 'center',
  border: "1px solid gray",
  borderRadius: 5,
  padding: "0 5px",
  margin: "0px 5px",
  cursor: "pointer",
  ":hover": {
    background: "pink"
  }
});

let notResizable = fromReport => css({
  position: "relative",
  display: "flex",
  flexWrap: "wrap",
  alignItems: "center",
  justifyContent: "center",
  //border: "dotted 2px green",
  //background: "#f0f0f0",
  textAlign: "justify",
  margin: fromReport ? null : "5px 5px 5px 10px",
  padding: fromReport ? null : 10,
  "@media print": {
    border: "none"
  }
});

let resizable = fromReport => css({
  position: "relative",
  display: "flex",
  flexWrap: "wrap",
  alignSelf: "center",
  alignItems: "center",
  justifyContent: "center",
  border: "dotted 2px green",
  //background: "#f0f0f0",
  textAlign: "justify",
  margin: fromReport ? '0 auto' : "5px 5px 5px 10px",
  padding: fromReport ? 0 : 10,
  "@media print": {
    border: "none"
  }
});

export { ResizableDiv };
