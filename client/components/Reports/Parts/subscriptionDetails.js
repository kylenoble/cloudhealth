{
    "description": {
        "health_talk": {
            "list":[
                {
                    "name":"Detox Workshop by Functional Medicine Physician",
                    "description":"Detox Workshop by Functional Medicine Physician (2 sessions, 1.5 hours each)"
                }
            ],
            "name":"Health Talk"},
        
     "addons":{
                "list":[
                    {"name":"Myers(Detox)","description":"Two (2) IV Therapy: Myers(Detox) with Acetylcysteine"},
                    {"name":"Hyperbaric Oxygen Therapy (HBOT)","description":"Hyperbaric Oxygen Therapy (HBOT)"},
                    {"name":"GPL Heavy Metals Blood Test","description":"GPL Heavy Metals Blood Test (RBC or Whole Blood)"},
                    {"name":"Heilen Low Laser Light Therapy","description":"Heilen Low Laser Light Therapy"}],
                    "name":"Add ons (please consult with your account manager for details)"}
    }
}


{
    "description": {
        "health_talk": {
            "list": [
                {
                    "name":"Optimal Weight Management Workshop",
                    "description":"Optimal Weight Management Workshop"
                }
            ],
            "name":"Health Talk"},
        
        "addons":{
            "list":[
                {
                    "name":"Food Detective",
                    "description":"Food Detective"
                },
                {
                    "name":"Nutrition Sensor",
                    "description":"Nutrition Sensor"
                },
                {
                    "name":"Toxo Sensor",
                    "description":"Toxo Sensor"
                },
                {
                    "name":"Nutrition Sensor, Weight Sensor and Toxo sensor",
                    "description":"Nutrition Sensor, Weight Sensor and Toxo sensor"},
                {
                    "name":"Comprehensive Stool Analysis and Parasitology",
                    "description":"Comprehensive Stool Analysis and Parasitology"
                }
            ],
            "name":"Add ons (please consult with your account manager for details)"},
            
        "activity":{
                "list":[
                    {"name":"Food diary analysis","description":"Food diary analysis"},
                    {"name":"Fitness workshop","description":"Fitness workshop"},
                    {"name":"Body Composition Analysis","description":"Body Composition Analysis"}
                ],
                "name":"Activity"}
    }
}