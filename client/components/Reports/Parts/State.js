const State = {
  healthGoalloading: true,
  loading: true,
  showReportList: false,
  subscriptions: [],
  avatar: "",
  uploadError: "",
  earlyMarkers: {
    weight: [],
    nutrition: [],
    detox: [],
    sleep: [],
    movement: [],
    stress: []
  },
  complianceData: [],
  recievedDemographicsFiltedData: [],
  users: [],
  activeReport: "",
  reports: [],
  createReportForm: {
    hide: true
  },
  addChiForm: {
    weight: { hide: true },
    nutrition: { hide: true },
    detox: { hide: true },
    upstream: { hide: true },
    downstream: { hide: true },
    movement: { hide: true },
    sleep: { hide: true },
    stress: { hide: true }
  },
  formError: {
    active: false,
    message: ""
  },
  formSuccess: {
    active: false,
    message: ""
  },
  chiGraph: {
    weight: [],
    nutrition: [],
    detox: [],
    upstream: [],
    movement: [],
    stress: [],
    sleep: [],
    downstream: []
  },
  inputs: {
    subscriptions: {
      id: "",
      name: "subscriptions",
      question: "Subscription",
      type: "dropdown",
      validation: "required",
      disabled: false,
      options: [
        {
          label: "CHIT",
          value: 1
        },
        {
          label: "CHIR",
          value: 2
        }
      ],
      error: {
        active: false,
        message: "Please select subscription"
      },
      value: "default"
    },
    report_name: {
      id: "",
      name: "report_name",
      question: "Report Name",
      type: "required",
      disabled: false,
      validation: "required",
      options: [],
      error: {
        active: false,
        message: "Please Enter Report Name"
      },
      value: ""
    },
    weight: {
      id: 2,
      name: "weight",
      question: "Select CHI",
      type: "select",
      validation: "required",
      disabled: false,
      options: [
        {
          label: "Demographics",
          value: "demographics"
        },
        {
          label: "Readiness",
          value: "readiness"
        },
        {
          label: "Health Score and Status",
          value: "healthScoreStatus"
        },
        {
          label: "Downstream",
          value: "downstream"
        },
        {
          label: "Symptoms Review",
          value: "msq"
        },
        {
          label: "Upstream Manifestations",
          value: "upstream"
        }
      ],
      error: {
        active: false,
        message: "Please select item"
      },
      value: "default"
    },
    nutrition: {
      id: 2,
      name: "nutrition",
      question: "Select CHI",
      type: "select",
      validation: "required",
      disabled: false,
      options: [
        {
          label: "Demographics",
          value: "demographics"
        },
        {
          label: "Readiness",
          value: "readiness"
        },
        {
          label: "Health Score and Status",
          value: "healthScoreStatus"
        },
        {
          label: "Downstream",
          value: "downstream"
        },
        {
          label: "Symptoms Review",
          value: "msq"
        },
        {
          label: "Upstream Manifestations",
          value: "upstream"
        }
      ],
      error: {
        active: false,
        message: "Please select item"
      },
      value: "default"
    },
    detox: {
      id: 2,
      name: "detox",
      question: "Select CHI",
      type: "select",
      validation: "required",
      disabled: false,
      options: [
        {
          label: "Demographics",
          value: "demographics"
        },
        {
          label: "Readiness",
          value: "readiness"
        },
        {
          label: "Health Score and Status",
          value: "healthScoreStatus"
        },
        {
          label: "Downstream",
          value: "downstream"
        },
        {
          label: "Symptoms Review",
          value: "msq"
        },
        {
          label: "Upstream Manifestations",
          value: "upstream"
        }
      ],
      error: {
        active: false,
        message: "Please select item"
      },
      value: "default"
    },
    sleep: {
      id: 2,
      name: "sleep",
      question: "Select CHI",
      type: "select",
      validation: "required",
      disabled: false,
      options: [
        {
          label: "Demographics",
          value: "demographics"
        },
        {
          label: "Readiness",
          value: "readiness"
        },
        {
          label: "Health Score and Status",
          value: "healthScoreStatus"
        },
        {
          label: "Downstream",
          value: "downstream"
        },
        {
          label: "Symptoms Review",
          value: "msq"
        },
        {
          label: "Upstream Manifestations",
          value: "upstream"
        }
      ],
      error: {
        active: false,
        message: "Please select item"
      },
      value: "default"
    },
    movement: {
      id: 2,
      name: "movement",
      question: "Select CHI",
      type: "select",
      validation: "required",
      disabled: false,
      options: [
        {
          label: "Demographics",
          value: "demographics"
        },
        {
          label: "Readiness",
          value: "readiness"
        },
        {
          label: "Health Score and Status",
          value: "healthScoreStatus"
        },
        {
          label: "Downstream",
          value: "downstream"
        },
        {
          label: "Symptoms Review",
          value: "msq"
        },
        {
          label: "Upstream Manifestations",
          value: "upstream"
        }
      ],
      error: {
        active: false,
        message: "Please select item"
      },
      value: "default"
    },
    stress: {
      id: 2,
      name: "stress",
      question: "Select CHI",
      type: "select",
      validation: "required",
      disabled: false,
      options: [
        {
          label: "Demographics",
          value: "demographics"
        },
        {
          label: "Readiness",
          value: "readiness"
        },
        {
          label: "Health Score and Status",
          value: "healthScoreStatus"
        },
        {
          label: "Downstream",
          value: "downstream"
        },
        {
          label: "Symptoms Review",
          value: "msq"
        },
        {
          label: "Upstream Manifestations",
          value: "upstream"
        }
      ],
      error: {
        active: false,
        message: "Please select item"
      },
      value: "default"
    }
  },
  determinants: {},
  activeFilter: {
    filter: "branch",
    value: "",
    init: true
  },
  prevFilter: "",
  defaultFltr: {
    goals: {
      filter: "",
      value: "",
      init: true
    },
    demographicsTop: {
      filter: "",
      value: "",
      init: true
    },
    weight_demographics: {
      filter: "",
      value: "",
      init: true
    },
    weight_readiness: {
      filter: "",
      value: "",
      init: true
    },
    weight_healthScoreStatus: {
      filter: "",
      value: "",
      init: true,
      selections: {
        score: true,
        status: true,
        determinants: true
      }
    },
    weight_downstream: {
      filter: "",
      value: "",
      init: true
    },
    weight_msq: {
      filter: "",
      value: "",
      init: true
    },
    weight_upstream: {
      filter: "",
      value: "",
      init: true,
      selections: {
        upstreamTable: false,
        foodchoices: false,
        msqTopAffectedSystems: false,
        prevalentSymptoms: false,
        weight: false,
        sleep: false,
        movement: false,
        stress: false,
        detox: false
      }
    },

    nutrition_demographics: {
      filter: "",
      value: "",
      init: true
    },
    nutrition_readiness: {
      filter: "",
      value: "",
      init: true
    },
    nutrition_healthScoreStatus: {
      filter: "",
      value: "",
      init: true,
      selections: {
        score: true,
        status: true,
        determinants: true
      }
    },
    nutrition_downstream: {
      filter: "",
      value: "",
      init: true
    },
    nutrition_msq: {
      filter: "",
      value: "",
      init: true
    },
    nutrition_upstream: {
      filter: "",
      value: "",
      init: true,
      selections: {
        upstreamTable: true,
        foodchoices: true,
        msqTopAffectedSystems: true,
        prevalentSymptoms: true,
        weight: true,
        sleep: true,
        movement: true,
        stress: true,
        detox: true
      }
    },

    detox_demographics: {
      filter: "",
      value: "",
      init: true
    },
    detox_readiness: {
      filter: "",
      value: "",
      init: true
    },
    detox_healthScoreStatus: {
      filter: "",
      value: "",
      init: true,
      selections: {
        score: true,
        status: true,
        determinants: true
      }
    },
    detox_downstream: {
      filter: "",
      value: "",
      init: true
    },
    detox_msq: {
      filter: "",
      value: "",
      init: true
    },
    detox_upstream: {
      filter: "",
      value: "",
      init: true,
      selections: {
        upstreamTable: true,
        foodchoices: true,
        msqTopAffectedSystems: true,
        prevalentSymptoms: true,
        weight: true,
        sleep: true,
        movement: true,
        stress: true,
        detox: true
      }
    },

    sleep_demographics: {
      filter: "",
      value: "",
      init: true
    },
    sleep_readiness: {
      filter: "",
      value: "",
      init: true
    },
    sleep_healthScoreStatus: {
      filter: "",
      value: "",
      init: true,
      selections: {
        score: true,
        status: true,
        determinants: true
      }
    },
    sleep_downstream: {
      filter: "",
      value: "",
      init: true
    },
    sleep_msq: {
      filter: "",
      value: "",
      init: true
    },
    sleep_upstream: {
      filter: "",
      value: "",
      init: true,
      selections: {
        upstreamTable: true,
        foodchoices: true,
        msqTopAffectedSystems: true,
        prevalentSymptoms: true,
        weight: true,
        sleep: true,
        movement: true,
        stress: true,
        detox: true
      }
    },

    movement_demographics: {
      filter: "",
      value: "",
      init: true
    },
    movement_readiness: {
      filter: "",
      value: "",
      init: true
    },
    movement_healthScoreStatus: {
      filter: "",
      value: "",
      init: true,
      selections: {
        score: true,
        status: true,
        determinants: true
      }
    },
    movement_downstream: {
      filter: "",
      value: "",
      init: true
    },
    movement_msq: {
      filter: "",
      value: "",
      init: true
    },
    movement_upstream: {
      filter: "",
      value: "",
      init: true,
      selections: {
        upstreamTable: true,
        foodchoices: true,
        msqTopAffectedSystems: true,
        prevalentSymptoms: true,
        weight: true,
        sleep: true,
        movement: true,
        stress: true,
        detox: true
      }
    },

    stress_demographics: {
      filter: "",
      value: "",
      init: true
    },
    stress_readiness: {
      filter: "",
      value: "",
      init: true
    },
    stress_healthScoreStatus: {
      filter: "",
      value: "",
      init: true,
      selections: {
        score: true,
        status: true,
        determinants: true
      }
    },
    stress_downstream: {
      filter: "",
      value: "",
      init: true
    },
    stress_msq: {
      filter: "",
      value: "",
      init: true
    },
    stress_upstream: {
      filter: "",
      value: "",
      init: true,
      selections: {
        upstreamTable: true,
        foodchoices: true,
        msqTopAffectedSystems: true,
        prevalentSymptoms: true,
        weight: true,
        sleep: true,
        movement: true,
        stress: true,
        detox: true
      }
    },
    healthScoreStatus: {
      filter: "",
      value: "",
      init: true
    },
    downstream: {
      filter: "",
      value: "",
      init: true
    },
    msq: {
      filter: "",
      value: "",
      init: true
    },
    upstream: {
      filter: "",
      value: "",
      init: true
    },
    weight: {
      filter: "",
      value: "",
      init: true
    },
    nutrition: {
      filter: "",
      value: "",
      init: true
    },
    detox: {
      filter: "",
      value: "",
      init: true
    },
    sleep: {
      filter: "",
      value: "",
      init: true
    },
    movement: {
      filter: "",
      value: "",
      init: true
    },
    stress: {
      filter: "",
      value: "",
      init: true
    },
    all: {
      filter: "",
      value: "",
      init: true
    }
  },

  filters: {
    goals: {
      filter: "",
      value: "",
      init: true
    },
    demographicsTop: {
      filter: "",
      value: "",
      init: true,
      alignment: "center"
    },
    weight_demographics: {
      filter: "",
      value: "",
      init: true
    },
    weight_readiness: {
      filter: "",
      value: "",
      init: true
    },
    weight_healthScoreStatus: {
      filter: "",
      value: "",
      init: true,
      selections: {
        score: true,
        status: true,
        determinants: true
      }
    },
    weight_downstream: {
      filter: "",
      value: "",
      init: true
    },
    weight_msq: {
      filter: "",
      value: "",
      init: true
    },
    weight_upstream: {
      filter: "",
      value: "",
      init: true,
      selections: {
        upstreamTable: false,
        foodchoices: false,
        msqTopAffectedSystems: false,
        prevalentSymptoms: false,
        weight: false,
        sleep: false,
        movement: false,
        stress: false,
        detox: false
      }
    },

    nutrition_demographics: {
      filter: "",
      value: "",
      init: true
    },
    nutrition_readiness: {
      filter: "",
      value: "",
      init: true
    },
    nutrition_healthScoreStatus: {
      filter: "",
      value: "",
      init: true,
      selections: {
        score: true,
        status: true,
        determinants: true
      }
    },
    nutrition_downstream: {
      filter: "",
      value: "",
      init: true
    },
    nutrition_msq: {
      filter: "",
      value: "",
      init: true
    },
    nutrition_upstream: {
      filter: "",
      value: "",
      init: true,
      selections: {
        upstreamTable: true,
        foodchoices: true,
        msqTopAffectedSystems: true,
        prevalentSymptoms: true,
        weight: true,
        sleep: true,
        movement: true,
        stress: true,
        detox: true
      }
    },

    detox_demographics: {
      filter: "",
      value: "",
      init: true
    },
    detox_readiness: {
      filter: "",
      value: "",
      init: true
    },
    detox_healthScoreStatus: {
      filter: "",
      value: "",
      init: true,
      selections: {
        score: true,
        status: true,
        determinants: true
      }
    },
    detox_downstream: {
      filter: "",
      value: "",
      init: true
    },
    detox_msq: {
      filter: "",
      value: "",
      init: true
    },
    detox_upstream: {
      filter: "",
      value: "",
      init: true,
      selections: {
        upstreamTable: true,
        foodchoices: true,
        msqTopAffectedSystems: true,
        prevalentSymptoms: true,
        weight: true,
        sleep: true,
        movement: true,
        stress: true,
        detox: true
      }
    },

    sleep_demographics: {
      filter: "",
      value: "",
      init: true
    },
    sleep_readiness: {
      filter: "",
      value: "",
      init: true
    },
    sleep_healthScoreStatus: {
      filter: "",
      value: "",
      init: true,
      selections: {
        score: true,
        status: true,
        determinants: true
      }
    },
    sleep_downstream: {
      filter: "",
      value: "",
      init: true
    },
    sleep_msq: {
      filter: "",
      value: "",
      init: true
    },
    sleep_upstream: {
      filter: "",
      value: "",
      init: true,
      selections: {
        upstreamTable: true,
        foodchoices: true,
        msqTopAffectedSystems: true,
        prevalentSymptoms: true,
        weight: true,
        sleep: true,
        movement: true,
        stress: true,
        detox: true
      }
    },

    movement_demographics: {
      filter: "",
      value: "",
      init: true
    },
    movement_readiness: {
      filter: "",
      value: "",
      init: true
    },
    movement_healthScoreStatus: {
      filter: "",
      value: "",
      init: true,
      selections: {
        score: true,
        status: true,
        determinants: true
      }
    },
    movement_downstream: {
      filter: "",
      value: "",
      init: true
    },
    movement_msq: {
      filter: "",
      value: "",
      init: true
    },
    movement_upstream: {
      filter: "",
      value: "",
      init: true,
      selections: {
        upstreamTable: true,
        foodchoices: true,
        msqTopAffectedSystems: true,
        prevalentSymptoms: true,
        weight: true,
        sleep: true,
        movement: true,
        stress: true,
        detox: true
      }
    },

    stress_demographics: {
      filter: "",
      value: "",
      init: true
    },
    stress_readiness: {
      filter: "",
      value: "",
      init: true
    },
    stress_healthScoreStatus: {
      filter: "",
      value: "",
      init: true,
      selections: {
        score: true,
        status: true,
        determinants: true
      }
    },
    stress_downstream: {
      filter: "",
      value: "",
      init: true
    },
    stress_msq: {
      filter: "",
      value: "",
      init: true
    },
    stress_upstream: {
      filter: "",
      value: "",
      init: true,
      selections: {
        upstreamTable: true,
        foodchoices: true,
        msqTopAffectedSystems: true,
        prevalentSymptoms: true,
        weight: true,
        sleep: true,
        movement: true,
        stress: true,
        detox: true
      }
    },
    healthScoreStatus: {
      filter: "",
      value: "",
      init: true
    },
    downstream: {
      filter: "",
      value: "",
      init: true
    },
    msq: {
      filter: "",
      value: "",
      init: true
    },
    upstream: {
      filter: "",
      value: "",
      init: true
    },
    weight: {
      filter: "",
      value: "",
      init: true
    },
    nutrition: {
      filter: "",
      value: "",
      init: true
    },
    detox: {
      filter: "",
      value: "",
      init: true
    },
    sleep: {
      filter: "",
      value: "",
      init: true
    },
    movement: {
      filter: "",
      value: "",
      init: true
    },
    stress: {
      filter: "",
      value: "",
      init: true
    },
    all: {
      filter: "",
      value: "",
      init: true
    }
  },
  demographics: {},
  mainHealhGoals: {},
  mainHealthConcern: {
    weight: 0,
    nutrition: 0,
    sleep: 0,
    detox: 0,
    stress: 0,
    movement: 0,
    other: 0
  },
  companyInfo: {},
  scoreAndPercentile: {},
  sixDeterminants: {},
  reportInfo: {
    id: "",
    company_subscription_id: "",
    company_id: "",
    company_name: {},
    location: {},
    insights: "",
    md_incharge: "",
    md_id: "",
    date_created: "",
    date_modified: "",
    score: "",
    percentile_rank: "",
    registered_employee: "",
    health_status: "",
    compliance_rate: "",
    details: {
      filters: {},
      chiGraph: {},
      activeFilter: {},
      analysis: {},
      earlyMarkers: {}
    },
    mean_scores: {},
    scoreandpercentile: {}
  },
  defaultAnalysis: {
    overall: "",
    goals: "",
    weight: "",
    nutrition: "",
    detox: "",
    sleep: "",
    movement: "",
    stress: "",

    weight_demographics_analysis: "",
    weight_readiness_analysis: "",
    weight_healthScoreStatus_analysis: "",
    weight_downstream_analysis: "",
    weight_msq_analysis: "",
    weight_upstream_analysis: "",

    nutrition_demographics_analysis: "",
    nutrition_readiness_analysis: "",
    nutrition_healthScoreStatus_analysis: "",
    nutrition_downstream_analysis: "",
    nutrition_msq_analysis: "",
    nutrition_upstream_analysis: "",

    detox_demographics_analysis: "",
    detox_readiness_analysis: "",
    detox_healthScoreStatus_analysis: "",
    detox_downstream_analysis: "",
    detox_msq_analysis: "",
    detox_upstream_analysis: "",

    sleep_demographics_analysis: "",
    sleep_readiness_analysis: "",
    sleep_healthScoreStatus_analysis: "",
    sleep_downstream_analysis: "",
    sleep_msq_analysis: "",
    sleep_upstream_analysis: "",

    movement_demographics_analysis: "",
    movement_readiness_analysis: "",
    movement_healthScoreStatus_analysis: "",
    movement_downstream_analysis: "",
    movement_msq_analysis: "",
    movement_upstream_analysis: "",

    stress_demographics_analysis: "",
    stress_readiness_analysis: "",
    stress_healthScoreStatus_analysis: "",
    stress_downstream_analysis: "",
    stress_msq_analysis: "",
    stress_upstream_analysis: "",
    others: {}
  },
  analysis: {
    overall: "",
    goals: "",
    weight: "",
    nutrition: "",
    detox: "",
    sleep: "",
    movement: "",
    stress: "",

    weight_demographics_analysis: "",
    weight_readiness_analysis: "",
    weight_healthScoreStatus_analysis: "",
    weight_downstream_analysis: "",
    weight_msq_analysis: "",
    weight_upstream_analysis: "",

    nutrition_demographics_analysis: "",
    nutrition_readiness_analysis: "",
    nutrition_healthScoreStatus_analysis: "",
    nutrition_downstream_analysis: "",
    nutrition_msq_analysis: "",
    nutrition_upstream_analysis: "",

    detox_demographics_analysis: "",
    detox_readiness_analysis: "",
    detox_healthScoreStatus_analysis: "",
    detox_downstream_analysis: "",
    detox_msq_analysis: "",
    detox_upstream_analysis: "",

    sleep_demographics_analysis: "",
    sleep_readiness_analysis: "",
    sleep_healthScoreStatus_analysis: "",
    sleep_downstream_analysis: "",
    sleep_msq_analysis: "",
    sleep_upstream_analysis: "",

    movement_demographics_analysis: "",
    movement_readiness_analysis: "",
    movement_healthScoreStatus_analysis: "",
    movement_downstream_analysis: "",
    movement_msq_analysis: "",
    movement_upstream_analysis: "",

    stress_demographics_analysis: "",
    stress_readiness_analysis: "",
    stress_healthScoreStatus_analysis: "",
    stress_downstream_analysis: "",
    stress_msq_analysis: "",
    stress_upstream_analysis: "",
    others: {}
  },
  defaultPrograms: {
    weight: {
      // totalHV: 1,
      // totalRegular: 2,
      selected: false,
      analysis: "",
      premium: {
        title: "Premium weight management Program  ",
        selected: false,
        description: {},
        newDescription: {}
      },
      basic: {
        title: "Basic weight management Program   ",
        selected: false,
        description: {},
        newDescription: {}
      }
    },
    sleep: {
      selected: false,
      analysis: "",
      premium: {
        title: "Premium Sleep Program  ",
        selected: false,
        description: {},
        newDescription: {}
      },
      basic: {
        title: "Basic Sleep Program ",
        selected: false,
        description: {},
        newDescription: {}
      }
    },
    movement: {
      selected: false,
      analysis: "",
      premium: {
        title: "Premium Kinesis Program ",
        selected: false,
        description: {},
        newDescription: {}
      },

      basic: {
        title: "Basic Kinesis Program ",
        selected: false,
        description: {},
        newDescription: {}
      }
    },
    detox: {
      selected: false,
      analysis: "",
      premium: {
        title: "Premium Detoxification Program  ",
        selected: false,
        description: {},
        newDescription: {}
      },

      basic: {
        title: "Basic Detoxification Program  ",
        selected: false,
        description: {},
        newDescription: {}
      }
    },
    nutrition: {
      selected: false,
      analysis: "",
      premium: {
        title: "Premium Nutrition Program ",
        selected: false,
        description: {},
        newDescription: {}
      },

      basic: {
        title: "Basic Nutrition Program  ",
        selected: false,
        description: {},
        newDescription: {}
      }
    },
    stress: {
      selected: false,
      analysis: "",
      premium: {
        title: "premium Stress Management Program ",
        selected: false,
        description: {},
        newDescription: {}
      },

      basic: {
        title: "Basic Stress Management Program ",
        selected: false,
        description: {},
        newDescription: {}
      }
    }
  },
  programs: {
    weight: {
      // totalHV: 1,
      // totalRegular: 2,
      selected: false,
      analysis: "",
      premium: {
        title: "Premium weight management Program  ",
        selected: false,
        description: {},
        newDescription: {}
      },
      basic: {
        title: "Basic weight management Program   ",
        selected: false,
        description: {},
        newDescription: {}
      }
    },
    sleep: {
      selected: false,
      analysis: "",
      premium: {
        title: "Premium Sleep Program  ",
        selected: false,
        description: {},
        newDescription: {}
      },
      basic: {
        title: "Basic Sleep Program ",
        selected: false,
        description: {},
        newDescription: {}
      }
    },
    movement: {
      selected: false,
      analysis: "",
      premium: {
        title: "Premium Kinesis Program ",
        selected: false,
        description: {},
        newDescription: {}
      },

      basic: {
        title: "Basic Kinesis Program ",
        selected: false,
        description: {},
        newDescription: {}
      }
    },
    detox: {
      selected: false,
      analysis: "",
      premium: {
        title: "Premium Detoxification Program  ",
        selected: false,
        description: {},
        newDescription: {}
      },

      basic: {
        title: "Basic Detoxification Program  ",
        selected: false,
        description: {},
        newDescription: {}
      }
    },
    nutrition: {
      selected: false,
      analysis: "",
      premium: {
        title: "Premium Nutrition Program ",
        selected: false,
        description: {},
        newDescription: {}
      },

      basic: {
        title: "Basic Nutrition Program  ",
        selected: false,
        description: {},
        newDescription: {}
      }
    },
    stress: {
      selected: false,
      analysis: "",
      premium: {
        title: "premium Stress Management Program ",
        selected: false,
        description: {},
        newDescription: {}
      },

      basic: {
        title: "Basic Stress Management Program ",
        selected: false,
        description: {},
        newDescription: {}
      }
    }
  },
  barGrapdata: {},
  branch: [],
  department: [],
  meanScore: {},
  responseMessage: "",
  errorMessage: "",
  buttons: {
    disabled: false,
    finalButton: {
      label: "Finalize",
      disabled: false
    },
    draftButton: {
      label: "Save as Draft",
      disabled: false
    }
  },
  leadDoctor: {},
  companyHR: [],
  downstream_issue: "",
  reportIsOpen: false
};

export { State };
