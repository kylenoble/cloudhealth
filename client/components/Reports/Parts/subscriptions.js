/* eslint-disable radix */
import React, { Component } from "react";
import { css } from "glamor";

class Subscriptions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // scoreAndPercentile: this.props.scoreAndPercentile,
      activeReport: this.props.activeReport,
      subscriptions: this.props.subscriptions
    };
  }

  componentWillReceiveProps(nextProps) {
    this._setUpdatedState(nextProps);
  }

  _setUpdatedState(nextProps) {
    const { subscriptions, activeReport } = nextProps;
    this.setState({ subscriptions, activeReport });
  }

  _handleChange(e) {
    const activeReport = this.state.activeReport;
    activeReport.company_subscription_id = e.target.value;
    this.setState({
      activeReport
    });
  }

  _selectOption(subscriptions) {
    return subscriptions.map((option, i) => {
      return <option value={option.id}>{option.subscription_name}</option>;
    });
  }

  render() {
    const activeReport = this.state.activeReport;
    const subscription_id = parseInt(activeReport.company_subscription_id);
    const subscriptionsRaw = this.state.subscriptions;

    // Remove Duplicates
    const subscriptions = subscriptionsRaw.filter(
      (raw, index, self) => index === self.findIndex(t => t.id === raw.id)
    );

    //let items;
    let select;
    if (subscriptions.length > 0) {
      select = (
        <select name="subscription" onChange={this._handleChange.bind(this)}>
          <option value="">Select Subscription</option>
          {this._selectOption(subscriptions)}
        </select>
      );
      // items = subscriptions.map((item, i) => (
      //   <span className={radioStyle} key={i}>
      //     <input
      //       disabled={this.state.activeReport.status > 0}
      //       checked={subscription_id === item.id}
      //       onClick={this._handleChange.bind(this)}
      //       type="radio"
      //       name="subscription"
      //       value={item.id}
      //     />
      //     {item.program_code}
      //   </span>
      // ));
    }

    return (
      <div className={subscriptionsListDiv}>
        <div className={subtitle}>
          {subscriptions.length > 1
            ? "Subscriptions"
            : subscriptions.length === 1
            ? "Subscription"
            : null}
        </div>
        {select}
      </div>
    );
  }
}

let radioStyle = css({
  display: "inline-block",
  border: "1px dotted orange",
  borderRadius: 5,
  margin: 5,
  padding: 5,
  textTransform: "uppercase"
});

let subtitle = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "center",
  fontWeight: "bold",
  padding: 10,
  fontSize: 14,
  textTransform: "uppercase"
});

let subscriptionsListDiv = css({
  "@media print": {
    display: "none"
  }
});

export { Subscriptions };
