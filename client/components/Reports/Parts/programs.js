import moment from "moment-timezone";

const titleTemplate = (content, i = "") => {
  const ts = moment().format("MMDDYYhhmmss");
  return {
    key: `${i}${ts}`,
    text: content,
    type: "unordered-list-item",
    depth: 0,
    inlineStyleRanges: [
      { offset: 0, length: 15, style: "color-rgb(0,0,0)" },
      {
        offset: 0,
        length: 15,
        style: "bgcolor-rgb(255,255,255)"
      },
      { offset: 0, length: 15, style: "fontsize-15" },
      {
        offset: 0,
        length: 15,
        style: "fontfamily-Roboto-Medium, sans-serif"
      },
      { offset: 0, length: 16, style: "BOLD" }
    ],
    entityRanges: [],
    data: {}
  };
};

const listTemplate = (content, i = "") => {
  const ts = moment().format("MMDDYYhhmmss");
  return {
    key: `b${i}${ts}`,
    text: content,
    type: "unordered-list-item",
    depth: 1,
    inlineStyleRanges: [
      { offset: 0, length: 94, style: "color-rgb(0,0,0)" },
      {
        offset: 0,
        length: 94,
        style: "bgcolor-rgb(255,255,255)"
      },
      { offset: 0, length: 94, style: "fontsize-15" },
      {
        offset: 0,
        length: 94,
        style: "fontfamily-Roboto-Medium, sans-serif"
      }
    ],
    entityRanges: [],
    data: {}
  };
};

const convertToEditorFormat = obj => {
  let healthTalk = undefined;
  let activity = undefined;
  healthTalk = obj.inclusions.description.health_talk;
  activity = obj.inclusions.description.activity;

  let blocks = [];
  if (healthTalk) {
    // get the healthTalk.name as bold title
    blocks.push(titleTemplate(healthTalk.name, "ht"));

    // iterite to healhTalk.list as list of description
  }
  if (healthTalk && healthTalk.list && healthTalk.list.length > 0) {
    healthTalk.list.forEach((item, i) => {
      blocks.push(listTemplate(item.description, i));
    });
  }

  if (activity) {
    blocks.push(titleTemplate(activity.name, "act"));
  }

  if (activity && activity.list && activity.list.length > 0) {
    activity.list.forEach((item, i) => {
      blocks.push(listTemplate(item.description, `act${i}`));
    });
  }

  return { blocks, entityMap: {} };

  // {
  //   blocks: [
  //     {
  //       key: "54ak",
  //       text: "H",
  //       type: "unordered-list-item",
  //       depth: 0,
  //       inlineStyleRanges: [
  //         { offset: 0, length: 11, style: "color-rgb(0,0,0)" },
  //         {
  //           offset: 0,
  //           length: 11,
  //           style: "bgcolor-rgb(255,255,255)"
  //         },
  //         { offset: 0, length: 11, style: "fontsize-15" },
  //         {
  //           offset: 0,
  //           length: 11,
  //           style: "fontfamily-Roboto-Medium, sans-serif"
  //         }
  //       ],
  //       entityRanges: [],
  //       data: {}
  //     },
  //     {
  //       key: "er1nu",
  //       text:
  //         "Optimal Weight Management Workshop (1-2 hours), Food diary analysis, Fitness workshop (1 hour) ",
  //       type: "unordered-list-item",
  //       depth: 1,
  //       inlineStyleRanges: [
  //         { offset: 0, length: 94, style: "color-rgb(0,0,0)" },
  //         {
  //           offset: 0,
  //           length: 94,
  //           style: "bgcolor-rgb(255,255,255)"
  //         },
  //         { offset: 0, length: 94, style: "fontsize-15" },
  //         {
  //           offset: 0,
  //           length: 94,
  //           style: "fontfamily-Roboto-Medium, sans-serif"
  //         }
  //       ],
  //       entityRanges: [],
  //       data: {}
  //     },
  //     {
  //       key: "2k8h9",
  //       text: "Functional Test ",
  //       type: "unordered-list-item",
  //       depth: 0,
  //       inlineStyleRanges: [
  //         { offset: 0, length: 15, style: "color-rgb(0,0,0)" },
  //         {
  //           offset: 0,
  //           length: 15,
  //           style: "bgcolor-rgb(255,255,255)"
  //         },
  //         { offset: 0, length: 15, style: "fontsize-15" },
  //         {
  //           offset: 0,
  //           length: 15,
  //           style: "fontfamily-Roboto-Medium, sans-serif"
  //         },
  //         { offset: 0, length: 16, style: "BOLD" }
  //       ],
  //       entityRanges: [],
  //       data: {}
  //     },
  //     {
  //       key: "6dgq0",
  //       text: "Body Composition Analysis ",
  //       type: "unordered-list-item",
  //       depth: 1,
  //       inlineStyleRanges: [
  //         { offset: 0, length: 25, style: "color-rgb(0,0,0)" },
  //         {
  //           offset: 0,
  //           length: 25,
  //           style: "bgcolor-rgb(255,255,255)"
  //         },
  //         { offset: 0, length: 25, style: "fontsize-15" },
  //         {
  //           offset: 0,
  //           length: 25,
  //           style: "fontfamily-Roboto-Medium, sans-serif"
  //         }
  //       ],
  //       entityRanges: [],
  //       data: {}
  //     }
  //   ],
  //   entityMap: {}
  // }
};

const ProgramsToEditor = data => {
  return convertToEditorFormat(data);
};

export { ProgramsToEditor };
