import React, { Component } from "react";
import moment from "moment-timezone";
import Datetime from "react-datetime";
import { css } from "glamor";
import Button from "../../NewSkin/Components/Button";
import { labelClass, combined, sqrSelectedOption, sqrDefaultOption, changeButton, centered, disabledInputStyle, disabledChangeButton } from "../../NewSkin/Styles";
import GridContainer from "../../NewSkin/Wrappers/GridContainer";
import Colors from "../../NewSkin/Colors";
import Label from "../../NewSkin/Components/Label";

class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      btnTxt: this.props.buttonText,
      companies: this.props.companies,
      departments: this.props.departments,
      btnLocked: false,
      submitted: false,
      formError: {
        active: false,
        message: ''
      }
    };

    this._isMounted = this.props.formError || false;
    this._submitForm = this._submitForm.bind(this);
  }

  render() {
    return (
      <React.Fragment>

        <div id="form-container" style={styles.formContainer}>

          <div style={styles.form}>
            <div className={labelClass} style={{borderBottom: `1px solid ${Colors.disabled}`, paddingBottom: 10, marginBottom: 20}}>{this.props.header}</div>

            <div>
              {this._displayInputs()}
              {this.props.table ? this.props.table : ""}
            </div>

            <span
              className={errorMsg}
              style={
                this.props.formError.active ? { ...styles.validationText, ...styles.validationTextVisible }
                  : styles.validationText
              }
            >
            </span>

            <span
              id="success"
              style={
                this.props.formSuccess.active
                  ? { ...styles.formSuccess, ...styles.formSuccessActive }
                  : styles.formSuccess
              }
            >
              {this.props.formSuccess.message}
            </span>

            <React.Fragment>
                <GridContainer columnSize={this.props.back ? '1fr 1fr' : '1fr'} gap={20} styles={{ margin: '20px auto', maxWidth: 500 }}>
                {
                    this.props.back &&
                    <Button
                    styles={{
                        margin: "20px auto 0",
                        minWidth: "auto",
                        width: 179
                    }}
                    ><button onClick={() => this.props.back()}>Back</button></Button>
                }
                <Button
                    type={this.props.btnLocked ? "disabled" : "pink"}
                    styles={{
                    margin: "20px auto 0",
                    minWidth: "unset",
                    width: "auto"
                    }}
                >
                    <button
                    id="update-btn"
                    disabled={this.props.btnLocked}
                    style={
                        this.props.btnLocked ? styles.disabledButton : styles.button
                    }
                    onClick={this._submitForm}
                    >
                    {this.props.buttonText}
                    </button>
                </Button>

                {
                    this.props.secondButton &&
                    <Button
                    type={this.props.secondButton.type || null}
                    styles={this.props.secondButton.style}
                    >
                    <button onClick={() => this.props.secondButton.action()}>{this.props.secondButton.name}</button>
                    </Button>
                }
                </GridContainer>
            </React.Fragment>
          </div>
        </div>
      </React.Fragment>
    );
  }

  supportsFlexBox() {
    const test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  componentDidMount() {
    this._isMounted = true;

    if (this.props.btnLocked) {
      this.state.btnLocked = this.props.btnLocked;
    }
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      console.log("flexbox is not supported");
      flexibility(document.getElementById("__next"));
    }
  }

  _handleKeyPress = (event) => {
    if (this.props.from === 'login' && event.key === 'Enter') {
      this._submitForm(event);
    }
  }

  _displayInputs() {
    const html = [];
    for (let i = 0; i < this.props.inputs.length; i++) {
      const input = this.props.inputs[i];
      if (input.type === "dropdown") {
        html.push(this._displayDropdown(input, i));
      } else if (input.type === "select") {
        html.push(this._displaySelect(input, i));
      } else if (input.type === "date") {
        html.push(this._displayDateInput(input, i));
      } else {
        html.push(this._displayInput(input, i));
      }
    }
    return html;
  }

  _checkInputName = (name, isBtn = false) => {
    switch (name) {
      case 'fullName':
        return isBtn ? true : Colors.blueDarkAccent
      case 'branch':
        return isBtn ? true : Colors.blueDarkAccent
      case 'department':
        return isBtn ? true : Colors.blueDarkAccent
      case 'birthday':
        return isBtn ? true : Colors.blueDarkAccent
      case 'gender':
        return isBtn ? true : Colors.blueDarkAccent
      default:
        return isBtn ? false : '#777'
    }
  }


  _displayInput(input, index) {
    let columnSize;
    switch(this.props.from){
        case "newReport":
        columnSize = '.5fr 1fr'
        break;
    }
    return (
      <div key={index} style={styles.inputContainer} className="input-wrapper">

        <GridContainer
          columnSize={columnSize}
          gap={0} fullWidth
        >
            <Label text={input.question} with_error={input.error.active} />

            <input
                placeholder={input.question}
                type={input.type}
                style={
                    input.disabled
                    ? styles.disabled
                    : input.error.active
                    ? { ...styles.formInput, ...styles.formInputError }
                    : styles.formInput
                }
                onChange={this._handleChange.bind(this, input)}
                value={input.value}
                disabled={input.disabled}
                hidden={input.hidden}
            />
            <span
                className={errorMsg}
                style={
                    input.error.active
                    ? {...styles.validationText, ...styles.validationTextVisible}
                    : styles.validationText
                }
          >
            {input.error.message}
          </span>
        </GridContainer>
      </div>
    );
  }

  _displayDateInput(input, index) {
    return (
      <div key={index} style={styles.inputContainer}>
        <Datetime
          inputProps={{ placeholder: input.question }}
          value={input.value}
          onChange={this._handleChange.bind(this, input)}
        />
        <span
          style={
            input.error.active
              ? { ...styles.validationText, ...styles.validationTextVisible }
              : styles.validationText
          }
        >
          {input.error.message}
        </span>
      </div>
    );
  }

  _checkSelectedInput = (input) => {
    const options = input.options
    let val = ''

    const style = {
      padding: '0 15px',
      color: Colors.blueDarkAccent,
      display: 'block',
      fontWeight: 500,
      textAlign: 'left'
    }

    options.forEach(option => {
      if (option.label === input.value) {
        val = <span style={style}>{option.label}</span>
      } else if (input.value.split(",").indexOf(option.value) > -1) {
        val = <span style={style}>{option.label}</span>
      }
    })

    return val;
  }

  _displaySelect(input, index) {
    return (
      <div key={index} style={styles.inputContainer}>
        <span style={styles.label}>{input.question}</span>
        <div style={styles.optionsContainer}>{this._displayOptions(input)}</div>
      </div>
    );
  }

  _displayDropdown(input, index) {
    let columnSize;
    switch(this.props.from){
        case "newReport":
            columnSize = '.5fr 1fr'
        break;
    }
    return (
      <div key={index} >
        <GridContainer
          columnSize={columnSize}
          gap={0} fullWidth
        >
            <Label text={input.question} with_error={input.error.active} />
            <select
                placeholder={input.question}
                disabled={input.disabled}
                id={input.name}
                style={
                input.error.active
                    ? { ...styles.formDropdown, ...styles.formInputError }
                    : styles.formDropdown
                }
                onChange={this._handleSelect.bind(this)}
            >
                <option key="a" value="">{input.question}</option>
                {this._displayListOptions(input)}
            </select>

          <span
            className={errorMsg}
            style={
              input.error.active
                ? {...styles.validationText, ...styles.validationTextVisible}
                : styles.validationText
            }
          >
            {input.error.message}
          </span>
        </GridContainer>
      </div>
    );
  }

  _styleSelect(input) {
    if (input.error.active === true) {
      return {
        ...styles.options,
        ...styles.optionsDefault,
        ...styles.optionsError
      };
    } else if (input.value === "default") {
      return { ...styles.options, ...styles.optionsDefault };
    } else {
      return { ...styles.options };
    }
  }

  _displayListOptions(input, col = 'label') {

    const html = [];
    const options = input.options;

    options.forEach((option, i) => {
      html.push(
        <option key={i} value={option.value}>
          {option[`${col}`]}
        </option>
      );
    });
    return html;
  }

  _displayOptions(input) {
    const html = [];
    const options = input.options;
    for (let i = 0; i < options.length; i++) {
      if (options[i].label === input.value) {
        html.push(
        <button
            data-name="selected__personal-info-option"
            key={i}
            id={input.name}
            className={selectOptionFilledStyle}
            name={options[i].value}
            onClick={this._handleClick()}
        >
            {options[i].label}
        </button>
        );
      } else if (input.value.split(",").indexOf(options[i].value) > -1) {
        html.push(
            <button
              data-name="submitted_personal-info-option"
              key={i}
              id={input.name}
              className={selectOptionFilledStyle}
              name={options[i].value}
              onClick={this._handleClick()}
            >
              {options[i].label}
            </button>
        );
      } else {
        html.push(
              <button
                data-name="survey-button"
                key={i}
                id={input.name}
                className={selectOptionStyle}
                name={options[i].value}
                onClick={this._handleClick()}
              >
                {options[i].label}
              </button>
        );
      }
    }

    return html;
  }

  _handleSelect = (e) => {
    const value = e.target.value;
    const name = e.target.id;

    const validated = this._validateInput(value, "dropdown");

    this.props.handleChange(value, name, !validated);
  }

  _handleClick = param => e => {
    const value = e.target.name;
    const name = e.target.id;

    const validated = this._validateInput(value, "select");

    this.props.handleChange(value, name, !validated);
  }

  _handleChange(input, event) {
    this.setState({
      btnTxt: this.props.buttonText
    });
    let value;
    if (input.type === "date") {
      value = event;
      input.question = "";
    } else {
      value = event.target.value;
    }
    const validated = this._validateInput(value, input.validation, input);

    this.props.handleChange(value, input.name, !validated);
  }

  _validateInput(input, type) {
      console.log(input, type)
    if (type === "email") {
      const ck_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      if (!ck_email.test(input)) {
        return false;
      }
      return true;
    } else if (type === "company") {
      let test = false;
      for (let i = 0; i < this.props.companies.length; i++) {
        if (this.props.companies[i].code == input) {
          test = true;
        }
      }
      return test;
    } else if (type === "name") {
      const ck_name = /^(.*)\s(.*)$/i;
      if (!ck_name.test(input) || !input) {
        return false;
      }
      return true;
    } else if (type === "password") {
      //  if (this.props.location !== 'profile') {
      const ck_password = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/;
      if (!ck_password.test(input)) {
        return false;
      }
      //  }
      return true;
    } else if (type === "birthday") {
      const eighteenYearsAgo = moment().subtract(1, "month");
      if (input) {
        const birthday = moment(input);

        if (!birthday.isValid()) {
          return false;
        } else if (eighteenYearsAgo.isAfter(birthday)) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    } else if (type === "number") {
      var ck_required = /^[0-9]+/;
      if (!ck_required.test(input)) {
        return false;
      }
      return true;
    } else if (type === "date") {
      const date = moment(input);

      if (!date.isValid()) {
        return false;
      }
      return true;
    } else if (type === "select") {
      if (input === "default" || input === "") {
        return false;
      }
      return true;
    } else if (type === "dropdown") {
      if (input === "default" || input === "") {
        return false;
      }
      return true;
    } else if (type === "text") {
      if (input === "default" || input === "") {
        return false;
      }
      return true;
    } if (type === "required") {
      var ck_required = /^[a-zA-Z\d]/;
      if (input === "default" || !ck_required.test(input)) {
        return false;
      }
      return true;
    } else if (type === "withRange") {
      let isValid = false;
      const arrVal = input.split('.');
      const val1 = parseInt(arrVal[0]);
      const val2 = parseInt(arrVal[1]);
      if (val1 && val1 > 0 && val1 < 8) {
        isValid = true;
        if (val2) {
          if (val2 < 0 || val2 > 11) {
            isValid = false;
          }

        }
      } else {
        isValid = false
      }
      var ck_required = /^[0-9]+/;
      if (!isValid || input === "default" || !ck_required.test(input)) {
        return false;
      }
      return true;
    }
  }

  _allInputsValid() {
    const results = [];
    for (let i = 0; i < this.props.inputs.length; i++) {

      results.push(
        this._validateInput(
          this.props.inputs[i].value,
          this.props.inputs[i].validation
        )
      );
    }

    if (results.indexOf(false) !== -1) {
      return false;
    }
    return true;
  }

  _submitForm(e) {
    if (!this._allInputsValid()) {
      console.log("could not validate");
      for (let i = 0; i < this.props.inputs.length; i++) {
        const validated = this._validateInput(
          this.props.inputs[i].value,
          this.props.inputs[i].type
        );
        this.props.handleChange(
          this.props.inputs[i].value,
          this.props.inputs[i].name,
          !validated
        );
      }
      this.setState({
        formError: {
          active: true,
          message: 'Please complete all fields'
        }
      });
      return;
    } else {
      this.setState({
        formError: {
          active: false
        }
      });
      this.props.submitForm();
    }
  }
}

const errorMsg = css({
  background: Colors.movementColor,
  padding: '5px',
  color: 'white',
  textAlign: 'center',
  gridColumn: '2/12',
  margin: '0',
  borderRadius: 5,
  gridColumnEnd: 3
});

let selectOptionStyle = css({
  padding: "10px 20px",
  backgroundColor: "#fff",
  border: "2px solid #eee",
  color: "#000",
  borderRadius: "5px",
  margin: "5px",
  fontSize: ".8rem",
  cursor: "pointer",
  transition: '250ms ease',
  ":hover": {
    // backgroundColor: "#ED81B9",
    borderColor: Colors.skyblue,
    color: Colors.skyblue
  },
  ':focus': {
    outline: 'none'
  }
});

let selectOptionFilledStyle = css({
  padding: "10px 20px",
  backgroundColor: Colors.skyblue,
  border: `2px solid ${Colors.skyblue}`,
  color: "#fff",
  borderRadius: "5px",
  margin: "5px",
  fontSize: ".8rem",
  cursor: "pointer",
  fontWeight: 600,
  boxShadow: `0 5px 15px ${Colors.skyblue}70`,
  ':focus': {
    outline: 'none'
  }
});

const styles = {
  disabled: {
    padding: 10,
    width: "247px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    fontSize: "14px",
    outline: "none",
    background: "#d3d3d3"
  },
  formContainer: {
    width: '100%',
    position: 'relative',
    // border: '2px solid rgba(151,151,151,0.2)',
    // borderRadius: '6px',
    maxWidth: "100%",
    // JsDisplay: "flex",
    // display: "-webkit-box" /* OLD - iOS 6-, Safari 3.1-6 */,
    // display: "-moz-box" /* OLD - Firefox 19- (buggy but mostly works) */,
    // display: "-ms-flexbox" /* TWEENER - IE 10 */,
    // display: "-webkit-flex" /* NEW - Chrome */,
    // display: "flex" /* NEW, Spec - Opera 12.1, Firefox 20+ */,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    textAlign: "center",
    // borderBottom: "2px solid rgba(225,232,238,1)",
    // padding: "15px 20px",
    fontSize: "18px",
    color: "#364563",
    fontFamily: '"Roboto-Medium",sans-serif',
    // width: "95%",
    display: "flex",
    flexDirection: "column"
  },
  form: {
    margin: "10px 30px 20px 20px"
  },
  formError: {
    // display: 'none',
    fontSize: '1rem',
    color: '#EA3131',
    transition: 'color 0.3s ease-in-out',
    fontWeight: 500
  },
  formErrorActive: {
    display: "block"
  },
  formSuccess: {
    display: "none",
    fontSize: "0.8em",
    margin: '10px auto 0',
    color: "white",
    padding: '10px 30px',
    borderRadius: 5,
    transition: "color 0.3s ease-in-out",
    background: 'rgb(3, 210, 127)'
  },
  formSuccessActive: {
    display: "block",
    maxWidth: 'fit-content',
  },
  formDropdown: {
    padding: 10,
    width: "100%",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    fontSize: "14px",
    outline: "none",
    minWidth: 300
  },
  formInput: {
    padding: 10,
    width: "100%",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    fontSize: "14px",
    outline: "none",
    textAlign: "left"
  },
  formInputError: {
    border: "2px solid #EA3131",
    transition: "color 0.3s ease-in-out"
  },
  optionsContainer: {
    display: "flex",
    flexFlow: "row wrap",
    justifyContent: "left",
    alignItems: "center"
  },
  inputContainer: {
    display: "flex",
    flexFlow: "column wrap",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "10px",
    position: "relative"
  },
  label: {
    color: "#364563",
    fontSize: "15px",
    fontFamily: '"Roboto-Medium",sans-serif',
    marginBottom: "5px",
    marginTop: "10px",
    width: "100%",
    textAlign: "center"
  },
  inputLabel: {
    color: "#364563",
    fontSize: "15px",
    fontFamily: '"Roboto-Medium",sans-serif',
    marginBottom: "-10px",
    width: "100%",
    textAlign: "center"
  },
  selectOption: {
    padding: "10px 20px",
    backgroundColor: "white",
    border: "2px solid #77E9AF",
    color: "#77E9AF",
    borderRadius: "5px",
    margin: "5px",
    fontSize: "12px"
  },
  options: {
    backgroundColor: "#fff"
  },
  optionsDefault: {
    color: "rgba(169, 169, 169, 1)"
  },
  optionsError: {
    border: "2px solid #EA3131",
    transition: "color 0.3s ease-in-out"
  },
  selectArrowZone: {
    cursor: "pointer",
    display: "table-cell",
    position: "relative",
    textAlign: "center",
    verticalAlign: "middle",
    width: "25px",
    paddingRight: "5px"
  },
  selectArrow: {
    borderColor: "#999 transparent transparent",
    borderStyle: "solid",
    borderWidth: "5px 5px 2.5px",
    display: "inline-block",
    height: "0",
    width: "0",
    position: "absolute",
    right: "-17em",
    top: "-2.75em"
  },
  validationText: {
    display: "none",
    fontSize: "0.8em",
    marginTop: "5px",
    transition: "color 0.3s ease-in-out"
  },
  validationTextVisible: {
    display: "block"
  },
  button: {
    backgroundColor: "#36c4f1",
    width: "auto",
    padding: "11px 50px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    // fontFamily: '"Roboto",sans-serif',
    // fontSize: "1rem",
    cursor: "pointer",
    display: "block",
    margin: "auto",
    marginTop: "0px",
    marginBottom: "20px"
  },
  disabledButton: {
    backgroundColor: "gray",
    width: "250px",
    padding: "11px 50px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontSize: "1rem",
    display: "block",
    margin: "auto",
    marginTop: "0px",
    marginBottom: "20px"
  },
  longGridError: {
    gridColumn: '1/7'
  },
  shortGridError: {
    gridColumn: '2/3',
    marginTop: '0 !important'
  }
};

export { Form };