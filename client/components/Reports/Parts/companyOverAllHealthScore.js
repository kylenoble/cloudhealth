import React, { Component } from "react";
import { css } from "glamor";

import { GetStatus, GetColor } from "../../../components/Commons";

class CompanyOverAllHealthScore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scoreAndPercentile: this.props.scoreAndPercentile,
      activeReport: this.props.activeReport
    };
  }

  componentWillReceiveProps(nextProps) {
    this._setUpdatedState(nextProps);
  }

  _setUpdatedState(nextProps) {
    const { scoreAndPercentile, activeReport } = nextProps;
    this.setState({ scoreAndPercentile, activeReport });
  }

  render() {
    return (
      <div className={wrapper}>
        <span className={wrapper}>
          {/* <div className={asflexTitle}>
            {this.state.activeReport.subscription_name}
          </div> */}
          <div className={asflexTitle}>
            {this.state.activeReport.report_name}
          </div>
          <hr />
          <strong>
            Company Overall Health Risk Score : &nbsp;
            <em>
              {this.state.scoreAndPercentile.average},{" "}
              <span
                style={{
                  color: `${GetColor(
                    GetStatus(this.state.scoreAndPercentile.average)
                  )}`
                }}
              >
                {GetStatus(this.state.scoreAndPercentile.average)}
              </span>
            </em>
          </strong>
        </span>
        <strong>
          Percentile Rank: {this.state.scoreAndPercentile.percentile}%
        </strong>
      </div>
    );
  }
}

let wrapper = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "center",
  //padding: '10px',
  margin: "0 0 0 0",
  "@media print": {
    //display: 'block !important',
    background: "#fff"
  }
});

let asflexTitle = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "space-around",
  padding: "5px",
  fontWeight: "bold",
  fontSize: 16,
  textTransform: "uppercase"
});

export { CompanyOverAllHealthScore };
