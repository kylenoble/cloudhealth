import React, { Component } from "react";
import { css } from "glamor";
import { EditorState, convertToRaw, convertFromRaw } from "draft-js";
import { Editor } from "react-draft-wysiwyg";

class Wyswyg extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: EditorState.createEmpty(),
      updatedRawContent: {}
    };
  }

  componentDidMount() {
    let htmldata;

    const { description, newDescription } = this.props.programs.basic;
    if (this.props.programs && newDescription) {
      if (Object.keys(newDescription).length > 0) {
        //htmldata = convertFromRaw(newDescription);
        this.setState({
          editorState: EditorState.createWithContent(
            convertFromRaw(newDescription)
          )
        });
      } else {
        //htmldata = convertFromRaw(description);
        this.setState({
          editorState: EditorState.createWithContent(
            convertFromRaw(description)
          )
        });
      }
    }
  }

  // onEditorStateChange : conflict in version
  onEditorStateChange = editorState => {
    let { updatedRawContent } = this.state;

    let rawContentState = convertToRaw(editorState.getCurrentContent());
    updatedRawContent = rawContentState;
    this.setState({
      editorState,
      updatedRawContent
    });
    this._handleChange(rawContentState);
  };

  _handleChange(rawData) {
    const data = { programName: this.props.programName, rawData };
    this.props.handleChange(data);
  }

  render() {
    const status = this.props.status;
    const { editorState } = this.state;
    //const styling = this.props.status ? editorStylelocked : editorStyle;
    return (
      <div className={editorStyle}>
        <Editor
          toolbar={{
            options: [
              "inline",
              "blockType",
              "fontSize",
              "fontFamily",
              "list",
              "textAlign"
            ],
            inline: { inDropdown: true },
            list: { inDropdown: true },
            textAlign: { inDropdown: true },
            link: { inDropdown: true },
            history: { inDropdown: true }
          }}
          toolbarHidden={status}
          readOnly={status}
          editorState={editorState}
          //wrapperClassName="demo-wrapper"
          // editorClassName={editorStyle}
          onEditorStateChange={this.onEditorStateChange}
        />
        {/* <textarea disabled value={JSON.stringify(this.state.updatedRawContent)} /> */}
      </div>
    );
  }
}

const editorStylelocked = css({
  display: "block",
  width: "100%",
  marginTop: -125
  //background: "#eee"
});

const editorStyle = css({
  display: "block",
  width: "100%",
  border: "none !important",
  background: "#fff"
  //marginTop: -50
});

export { Wyswyg };
