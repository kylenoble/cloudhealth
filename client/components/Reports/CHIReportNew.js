/* eslint-disable no-param-reassign */
/* eslint-disable no-mixed-operators */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-dupe-keys */
/* eslint-disable radix */
/* eslint-disable no-undef */
import React, { createRef } from "react";

import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  PieChart,
  Pie,
  Cell,
  LabelList,
  ResponsiveContainer
} from "recharts";

import { css } from "glamor";
import { confirmAlert } from "react-confirm-alert"; // Import
import TextareaAutosize from "react-autosize-textarea";
import Modal from 'react-modal'
import ReactTooltip from "react-tooltip";
import moment from "moment-timezone"; // localizede
import dateFormat from "dateformat"; // not localize
import Dropzone from "react-dropzone";
import request from "axios";
import ReportsService from "../../utils/reportsService";
import subscriptionService from "../../utils/subscriptionService";
import currentDomain from "../../utils/domain.js";
import DefaultFilters from "../../utils/defaultFilters";
import Demographics from "../DoctorDashboard/taskManager/demographics";
import Readiness from "../DoctorDashboard/taskManager/readiness";
import HealthScoreStatus from "../DoctorDashboard/taskManager/healthScoreStatus";
import Downstream from "../DoctorDashboard/taskManager/downstream";
import MSQ from "../DoctorDashboard/taskManager/msq";
import Upstream from "../DoctorDashboard/taskManager/upstream";
import User from "../../utils/userService";
import Email from "../../utils/emailService";
import Notifications from "../../utils/notificationService";
import Preview from "./preview";
import ReactToPrint from 'react-to-print';
import { Preparing } from "../NewSkin/Components/Loading";

import { GetStatus, GetColor, MultiFilter } from "../../components/Commons";

import { ProgramContext } from "../../context";

import {
  ProgramsToEditor,
  CompanyOverAllHealthScore,
  ResizableDiv,
  Wyswyg,
  State,
  Form
} from "./Parts";
import ReportsTable from "./reportsTable";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Button from "../NewSkin/Components/Button";
import { inputClass, scrollClass, sqrDefaultOption, sqrSelectedOption, avoidPageBreakInside } from "../NewSkin/Styles";
import PageOne from "./ReportParts/pageOne";
import PageWrapper from "../NewSkin/Wrappers/PageWrapper";
import CompanyInfo from "./ReportParts/companyInfo";
import Label from "../NewSkin/Components/Label";
import Colors from "../NewSkin/Colors";
import Dropdown from "../NewSkin/Components/Dropdown";
import Checkbox from "../NewSkin/Components/Checkbox";
import CHAHeader from "../NewSkin/Wrappers/CHAHeader";

const HIGH_VALUE = 1;
const HIGH_RISK = 2;
const DRAFT_REPORT = null;
const FINAL_REPORT = 1;
const REPORT_SENT_TO_HR = 2;
const CLOUDINARY_UPLOAD_URL = `${currentDomain}/api/upload/notmultiple`;
const CLOUDINARY_REPORT_UPLOAD_URL = `${currentDomain}/api/upload/report`;

const Report = new ReportsService();
const subscription = new subscriptionService();
const email = new Email();
const user = new User();
const notification = new Notifications();
const noAnalysis = (
  <p style={{fontStyle: 'italic', fontSize: 'smaller' }}>
    No data available
  </p>
)

const initialState = {
  ...State,
      chiRow: {
        weight: false,
        nutrition: false,
        detox: false,
        sleep: false,
        movement: false,
        stress: false
      }
};


class CHIReportNew extends React.Component {
  static contextType = ProgramContext;
  constructor(props) {

    // Chrome 1 - 71
    super(props);
    this.analysis = createRef()
    this.isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

    this._isMounted = false;
    this.state = initialState

    this._getCompliance = this._getCompliance.bind(this);
    this._formSubmit = this._formSubmit.bind(this);
    this._getAllusersByCompany = this._getAllusersByCompany.bind(this);
    this._getQuery = this._getQuery.bind(this);
    this._getUserFilters = this._getUserFilters.bind(this);
    this._getFilteredData = this._getFilteredData.bind(this);
    //this._getReadinessGraphData = this._getReadinessGraphData.bind(this);
    this._setFilter = this._setFilter.bind(this);
    this._healthScoreStatus = this._healthScoreStatus.bind(this);
    this._goToNext = this._goToNext.bind(this);
    this._getUsers = this._getUsers.bind(this);
    this._recievedFiltedData = this._recievedFiltedData.bind(this);
    this.onImageDrop = this.onImageDrop.bind(this);
    this._setDivAlignment = this._setDivAlignment.bind(this);
    this._handleWyswygChange = this._handleWyswygChange.bind(this);
    this._secondButtonAction = this._secondButtonAction.bind(this);

    this.scrollToCHI = React.createRef();
  } // constructor end


  componentWillMount() {
    this._clearState()
    this._isMounted = true;
    this._updateState();    
  }

  _clearState(){
    
    this.setState(initialState)
    this._updateState()
  }

  _setRow = () => {
    const CHIs = ['weight', 'nutrition', 'sleep', 'detox', 'stress', 'movement'];
      const analysisWrapper = document.getElementById(`weight-analysis-wrapper`);
      if (analysisWrapper) {
        CHIs.forEach(chi => {
          const analysisWrapper = document.getElementById(`${chi}-analysis-wrapper`);
          const awh = analysisWrapper.offsetHeight;
          console.log(chi, awh)
          if (awh > 350) {
            this.setState({
              chiRow: { ...this.state.chiRow, [chi]: true }
            });
          } else {
            this.setState({
              chiRow: { ...this.state.chiRow, [chi]: false }
            });
          }
        });
      }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentWillReceiveProps() {

    this.setState({
      activeFilter: this.props.activeFilter
    })
  }

  onImageDrop = (files, doh, i = null) => {
    if (files.length > 0) {
      if (this._isMounted) {
        this.setState({
          uploadError: ""
        });
      }
      this.handleImageUpload(files[0], doh, i);
    } else {
      let msg = 'Only image with extention of .jpg or .png are allowed and must not  exceed to 300 KB';

      if (doh === "PDFReport") {
        msg = "Only files with extention of .pdf are allowed";
      }
      if (this._isMounted) {
        this.setState({
          uploadError: msg
        });
      }
    }
  }

  _updateState() {
    let { companyInfo, scoreAndPercentile } = this.state;
    const { reportInfo, createReportForm } = this.state;
    companyInfo = this.props.activeCompanyDetails;
    scoreAndPercentile = this.props.scoreAndPercentile();
    reportInfo.company_id = this.props.id;
    reportInfo.company_name = this.props.activeCompanyDetails.company_name;
    reportInfo.location = this.props.activeCompanyDetails.location;
    createReportForm.hide = true;

    if (this._isMounted) {
      this.setState({
        companyInfo,
        scoreAndPercentile,
        reportInfo,
        createReportForm
      }, () => {
        this._updateProgramsState();
        this._getSubscriptions();
        this._getReportFromDatabase();
        this._getLeadDoctor();
        this._getAllusersByCompany();
        this._getCompliance();
        this._getHealthDeterminants();
      });
    }
    
    
  }

  _updateProgramsState() {
    const Programs = this.context.programs;
    const { programs, defaultPrograms } = this.state;

    Programs.forEach(prog => {
      const strArr = prog.subscription_name.split(" ");
      let name = strArr[0].toLowerCase();
      const type = strArr[strArr.length - 1].toLowerCase();
      if (name === "detoxification") {
        name = "detox";
      }

      programs[name][type].title = prog.subscription_name;
      programs[name][type].description = ProgramsToEditor(prog);
      defaultPrograms[name][type].description = ProgramsToEditor(prog);
    });
    //programs.weight.WMPB.description = ProgramsToEditor(this.context.programs[0])
    if (this._isMounted) {
      this.setState({ programs });
    }
  }

  _getReportFromDatabase() {
    if (this._isMounted) this.setState({ loading: true });
    const criteria = {
      field: "company_id",
      value: this.state.reportInfo.company_id
    };

    this._getReport(criteria)
      .then(res => {

        if (res) this._setData(res);
      })
      .catch(e => {
        console.log(e);
      });
  }


  _getAdmin = companyID => {
    user
      .getAdmin(companyID)
      .then(res => {
        if (this._isMounted) {
          this.setState(prevState => ({
            // eslint-disable-next-line no-param-reassign
            companyHR: (prevState.companyHR = res)
          }));
        }
      })
      .catch(e => {
        console.log(e);
      });
  };

  _getSubscriptions() {
    this._getAdmin(this.props.activeCompanyDetails.id);
    const data = {
      field: "company_id",
      id: this.props.activeCompanyDetails.id
    };
    subscription
      .get(data)
      .then(res => {
        if (res.length > 0) {
          // Remove Duplicates
          const noduplicate = res.filter(
            (raw, index, self) => index === self.findIndex(t => t.id === raw.id)
          );
          if (this._isMounted) {
            this.setState({
              subscriptions: noduplicate
            });
          }
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _clear() {
    if (this._isMounted) {
      this.setState({
        responseMessage: "",
        errorMessage: ""
      });
    }
  }

  _updateReportList() {
    
    //const reports = this.state.reports;
    
    const activeReport = this.state.activeReport;
    //const reportInfo = this.state.reportInfo;
    let chiGraph = this.state.chiGraph;
    let filters = this.state.filters;
    let activeFilter = this.state.activeFilter;
    let earlyMarkers = this.state.earlyMarkers;
    let programs = this.state.programs;
    let analysis = this.state.analysis;
    let showReportList = this.state.showReportList;
    //reset data
    analysis = {};

    programs.weight.basic.newDescription = {};
    programs.detox.basic.newDescription = {};
    programs.movement.basic.newDescription = {};
    programs.nutrition.basic.newDescription = {};
    programs.sleep.basic.newDescription = {};
    programs.stress.basic.newDescription = {};

    earlyMarkers = {};
    activeFilter = {};
    chiGraph = {};
    filters = {};

    if (activeReport) {
      if (Object.keys(activeReport.details.analysis).length > 0) {
        analysis = activeReport.details.analysis;
      } else {
        //analysis = this.state.defaultAnalysis;
      }
      programs = activeReport.programs;
      if (activeReport.details.earlyMarkers) {
        earlyMarkers = activeReport.details.earlyMarkers;
      }
      if (activeReport.details.activeFilter) {
        activeFilter = activeReport.details.activeFilter;
      }
      if (activeReport.details && activeReport.details.chiGraph) {
        chiGraph = activeReport.details.chiGraph;
      } else {
        chiGraph.weight = [];
        chiGraph.nutrition = [];
        chiGraph.detox = [];
        chiGraph.upstream = [];
        chiGraph.msq = [];
        chiGraph.downstream = [];
      }
      if (activeReport.details.filters && Object.keys(activeReport.details.filters).length > 0) {
        filters = activeReport.details.filters;
      }
    }
    showReportList = true
    if (this._isMounted) {
      this.setState(
        {
          //reports,
          activeReport,
          //reportInfo,
          filters,
          chiGraph,
          activeFilter,
          earlyMarkers,
          programs,
          analysis,
          showReportList
        },
        function () {
          this._getQuery();
          setTimeout(() => {
            this._setRow();
          }, 5000)
        }
      );
    }
  }

  _onCloseHandler = () => {
    if (this._isMounted) {
      this.setState({ loading: false });
    }
  }

  _showReport = () => {
    if (this._isMounted) {
      this.setState({
        reportIsOpen: !this.state.reportIsOpen
      })
    }
  }

  _displayReport = () => {
    const status = this.state.activeReport.status;
    const isPrint = this.state.isPrint;
    const loading = this.state.healthGoalloading;

    const styles = {
      maxHeight: '75vh',
      width: 'fit-content !important',
      overflow: loading ? 'hidden' : 'hidden auto',
      position: 'relative'
    }

    const trigger = (
      <a id="trigger" href="#" style={{ textDecoration: 'none' }}>
        <Button type={loading ? 'disabled' : 'blue'} left>
          <button disabled={loading}>{isPrint ? 'Print Now' : 'Print Report'}</button>
        </Button>
      </a>
    )

    const saveChanges = (
      <Button left type={loading ? 'disabled' : "blue"} styles={{ marginBottom: 0 }}>
        <button
          disabled={loading}
          onClick={() => {
            this._formSubmit(false);
            this._showReport();
            this._onCloseHandler();
          }}>SAVE CHANGES</button>
      </Button>
    )

    return (
      <>
        <Modal isOpen={this.state.reportIsOpen} style={customStyle} ariaHideApp={false}>
          <section style={styles} className={loading ? null : scrollClass}>
            {
              loading &&
              <div className={printingOverlay}>
                <img style={{ width: 100, margin: '0 auto' }} src="/static/images/cha_logo_only.png" alt="CHA Logo" />
                <div>Please wait...</div>
              </div>
            }

            <article id="renderview-wrapper" style={{ filter: 'blur(3px)', transition: '1.5s ease' }} className={loading ? null : notBlurred}>
              {this._renderView()}
            </article>
          </section>

          <GridContainer
            columnSize={status === DRAFT_REPORT ? '1fr .5fr .5fr' : this.state.isPrint ? '1fr .5fr .5fr' : "1fr .5fr"}
            gap={20}
            styles={{ marginTop: 20 }}
          >
            <Label
              text={`Report Name: ${this.state.activeReport.report_name}`}
              styles={{ textTransform: 'none', fontStyle: 'italic', fontSize: 'smaller', background: 'whitesmoke', padding: '3px 10px', border: '1px solid whitesmoke', borderRadius: 3, width: '100%' }}
            />
            <Button styles={{ marginLeft: 'auto', marginBottom: 0 }}>
              <button
                id="close-trigger"
                onClick={() => {
                  this._clearState()                  
                  this._showReport();
                  this._onCloseHandler();
                }}>Close</button>
            </Button>

            {status === DRAFT_REPORT && saveChanges}
            {
              status !== DRAFT_REPORT && this.state.isPrint &&
              <ReactToPrint
                trigger={() => trigger}
                content={() => this.printable}
              />
            }
          </GridContainer>
        </Modal>
      </>
    )
  }



  _reportList() {
    const reports = this.state.reports;
    const activeReport = this.state.activeReport;

    return (
      <ReportsTable
        finalizeReport={this._formSubmit}
        removeReport={this._deleteReport}
        sendToHR={this.onImageDrop}
        getCurrentReport={this._getCurrentReport}
        reports={reports}
        activeReport={activeReport}
        showReport={this._showReport}
      />
    )
  }

  _openReport(id) {
    const reports = this.state.reports;
    let filters = this.state.filters;
    let reportInfo = this.state.reportInfo;
    let chiGraph = this.state.chiGraph;
    let activeFilter = this.state.activeFilter;
    let programs = this.state.programs;
    let analysis = this.state.analysis;
    const activeReport = reports.filter(item => item.id === id);

    if (activeReport[0].details.activeFilter) {
      activeFilter = activeReport[0].details.activeFilter;
    } else {
      activeFilter = {
        filter: "branch",
        value: "",
        init: true
      };
    }

    if (activeReport[0].details.filters) {
      filters = activeReport[0].details.filters;
    } else {
      filters = this.state.defaultFltr;
    }

    if (activeReport[0].programs) {
      programs = activeReport[0].programs;
    }

    if (activeReport[0].details.analysis) {
      analysis = activeReport[0].details.analysis;
    } else {
      analysis = {
        overall: "",
        goals: "",
        weight: "",
        nutrition: "",
        detox: "",
        sleep: "",
        movement: "",
        stress: "",
        isPrint: false,

        weight_demographics_analysis: "",
        weight_readiness_analysis: "",
        weight_healthScoreStatus_analysis: "",
        weight_downstream_analysis: "",
        weight_msq_analysis: "",
        weight_upstream_analysis: "",

        nutrition_demographics_analysis: "",
        nutrition_readiness_analysis: "",
        nutrition_healthScoreStatus_analysis: "",
        nutrition_downstream_analysis: "",
        nutrition_msq_analysis: "",
        nutrition_upstream_analysis: "",

        detox_demographics_analysis: "",
        detox_readiness_analysis: "",
        detox_healthScoreStatus_analysis: "",
        detox_downstream_analysis: "",
        detox_msq_analysis: "",
        detox_upstream_analysis: "",

        sleep_demographics_analysis: "",
        sleep_readiness_analysis: "",
        sleep_healthScoreStatus_analysis: "",
        sleep_downstream_analysis: "",
        sleep_msq_analysis: "",
        sleep_upstream_analysis: "",

        movement_demographics_analysis: "",
        movement_readiness_analysis: "",
        movement_healthScoreStatus_analysis: "",
        movement_downstream_analysis: "",
        movement_msq_analysis: "",
        movement_upstream_analysis: "",

        stress_demographics_analysis: "",
        stress_readiness_analysis: "",
        stress_healthScoreStatus_analysis: "",
        stress_downstream_analysis: "",
        stress_msq_analysis: "",
        stress_upstream_analysis: "",
        others: {}
      };
    }

    reportInfo = activeReport[0];
    if (reportInfo.details.chiGraph) {
      chiGraph = reportInfo.details.chiGraph;
    } else {
      chiGraph.weight = [];
      chiGraph.nutrition = [];
      chiGraph.detox = [];
      chiGraph.upstream = [];
      chiGraph.msq = [];
      chiGraph.downstream = [];
    }
    if (activeReport[0].details) {
      filters.demographicsTop.value =
        activeReport[0].details.filters.demographicsTop.value;
      filters.demographicsTop.filter =
        activeReport[0].details.filters.demographicsTop.filter;
      filters.goals.value = activeReport[0].details.filters.goals.value;
      filters.goals.filter = activeReport[0].details.filters.goals.filter;
    } else {
      filters.demographicsTop.value = "";
      filters.demographicsTop.filter = "";
      filters.goals.value = "";
      filters.goals.filter = "";
    }
    if (this._isMounted) {
      this.setState({
        activeReport: activeReport[0],
        reportInfo: activeReport[0],
        filters,
        chiGraph,
        activeFilter,
        programs,
        analysis
      });
    }
  }

  _handleFormChange = (value, input, validated) => {
    const inputName = input;
    const newInput = this.state.inputs[inputName];
    newInput.value = value;
    newInput.error.active = validated;
    if (this._isMounted) {
      this.setState({
        inputName: newInput
      });
    }
  }

  _secondButtonAction() {
    if (this._isMounted) {
      const { createReportForm } = this.state;
      createReportForm.hide = true;
      this.setState({ createReportForm });
    }
  }

  _createReportModal = () => {
    const { report_name, subscriptions } = this.state.inputs;
    const fields = [report_name, subscriptions];

    const getInputs = () => {
      const formContainer = document.getElementById('form-container');
      const input = formContainer && formContainer.querySelectorAll('input')[0];
      const select = formContainer && formContainer.querySelectorAll('select')[0];

      if (formContainer) {
        input.style = null;
        select.style = null;
        input.classList.add(inputClassName);
        select.classList.add(inputClassName);
      }
    }

    if (this.state.createReportForm.hide === false) {
      confirmAlert({
        title: `You are creating a report for ${this.state.companyInfo.company_name}`, // Title dialog
        buttons: [
          {
            label: "Create Now",
            onClick: (onClose) => {
              this.setState({ showReportList: false })
              this._goToNext("createReport");
              onClose();

            }
          },
          {
            label: "Cancel"
          }
        ],
        customUI: ({ title, onClose }) => {
          return (
            <div className={wrapper}>
              <Form
                loaded={getInputs}
                from={"newReport"}
                header={title}
                submitForm={() => {
                  this._goToNext("createReport");
                  onClose();
                }}
                buttonText={"Create Now"}
                handleChange={this._handleFormChange.bind(this)}
                inputs={fields}
                formError={this.state.formError}
                formSuccess={this.state.formSuccess}
                back={onClose}
              />
            </div>
          )
        }
      });
    }
  }

  _createReport() {
    const { subscriptions } = this.state;
    let subscriptionOption;
    if (subscriptions.length > 0) {
      subscriptionOption = subscriptions.map(item => ({ label: item.subscription_name, value: item.id }));
    }

    const { createReportForm, inputs } = this.state;
    inputs.subscriptions.options = subscriptionOption;
    createReportForm.hide = false;

    if (this._isMounted) this.setState({ createReportForm, inputs });

    this._createReportModal();
  }

  _goToNext(formName, doh) {
    if (formName === "createReport") {
      this._formSubmit(false, true);
    } else if (formName === "addChi") {
      this._addChiSet(doh);
    }
  }

  _addChiSet(doh) {
    const input = this.state.inputs;
    const chiGraph = this.state.chiGraph;
    const addChiForm = this.state.addChiForm;
    addChiForm[doh].hide = true;
    chiGraph[doh].push(input[doh].value);
    if (this._isMounted) {
      this.setState({
        chiGraph,
        addChiForm
      }, () => {
        if (this.scrollToCHI.current) {
          setTimeout(() => {
            this.scrollToCHI.current.scrollIntoView({
              behavior: "smooth",
              block: "nearest"
            });
          }, 1000);
        }
      });
    }
  }

  _getQuery() {
    if (this._isMounted) this.setState({ healthGoalloading: true });
    const goalFilter = { ...this.state.filters.goals };

    let mainHealhGoals = this.state.mainHealhGoals;
    let arg = null;

    if (goalFilter && goalFilter.filters) {
      arg = {
        age_group: goalFilter.filters.age_group
          ? goalFilter.filters.age_group
          : null,
        branch: goalFilter.filters.branch ? goalFilter.filters.branch : null,
        department: goalFilter.filters.department
          ? goalFilter.filters.department
          : null,
        high_risk: goalFilter.filters.high_risk
          ? goalFilter.filters.high_risk
          : null,
        job_grade: goalFilter.filters.jobgrade
          ? goalFilter.filters.jobgrade
          : null
      };
    }

    if (goalFilter && goalFilter.filters) {
      this.props
        .getQuery(arg)
        .then(res => {
          if (res && res.length > 0) {
            mainHealhGoals = res;
            if (this._isMounted) this.setState({ mainHealhGoals });
            this._getGoalConcern(res);
          } else if (this._isMounted) {
            this.setState({
              mainHealhGoals: {},
              mainHealthConcern: {
                weight: 0,
                nutrition: 0,
                sleep: 0,
                detox: 0,
                stress: 0,
                movement: 0,
                other: 0
              }
            });
          }
          if (this._isMounted) {
            this.setState({ loading: false, healthGoalloading: false });
          }
        })
        .catch(e => {
          console.log(e);
        });
    } else {
      this.props
        .getQuery()
        .then(res => {
          if (res.length > 0) {
            mainHealhGoals = res;
            if (this._isMounted) this.setState({ mainHealhGoals });
            this._getGoalConcern(res);
          } else if (this._isMounted) {
            this.setState({
              mainHealhGoals: {},
              mainHealthConcern: {
                weight: 0,
                nutrition: 0,
                sleep: 0,
                detox: 0,
                stress: 0,
                movement: 0,
                other: 0
              }
            });
          }
          if (this._isMounted) {
            this.setState({ loading: false, healthGoalloading: false });
          }
        })
        .catch(e => {
          console.log(e);
        });
    }
  }

  filterMainGoal(data) {
    let tmp_userid = []
    let new_data = {}

    return new_data = data.filter(item => {
      if (tmp_userid.indexOf(item.user_id) < 0) {
        tmp_userid.push(item.user_id)
        return item;
      }
    })
  }

  _getGoalConcern(res) {
    const filteredData = this.filterMainGoal(res);
    const mainHealthConcern = this.state.mainHealthConcern;
    mainHealthConcern.weight = 0;
    mainHealthConcern.nutrition = 0;
    mainHealthConcern.sleep = 0;
    mainHealthConcern.detox = 0;
    mainHealthConcern.movement = 0;
    mainHealthConcern.stress = 0;
    mainHealthConcern.other = 0;
    const tot = filteredData.length;

    filteredData.forEach(item => {
      if (item.doh === "detox") {
        mainHealthConcern.detox += parseInt(item.count);
      } else if (item.doh === "weight") {
        mainHealthConcern.weight += parseInt(item.count);
      } else if (item.doh === "nutrition") {
        mainHealthConcern.nutrition += parseInt(item.count);
      } else if (item.doh === "movement") {
        mainHealthConcern.movement += parseInt(item.count);
      } else if (item.doh === "sleep") {
        mainHealthConcern.sleep += parseInt(item.count);
      } else if (item.doh === "stress") {
        mainHealthConcern.stress += parseInt(item.count);
      } else if (item.doh === "other") {
        mainHealthConcern.other += parseInt(item.count);
      }
    });

    mainHealthConcern.weight = parseFloat(
      (mainHealthConcern.weight / tot) * 100
    ).toFixed();
    mainHealthConcern.nutrition = parseFloat(
      (mainHealthConcern.nutrition / tot) * 100
    ).toFixed();
    mainHealthConcern.sleep = parseFloat(
      (mainHealthConcern.sleep / tot) * 100
    ).toFixed();
    mainHealthConcern.detox = parseFloat(
      (mainHealthConcern.detox / tot) * 100
    ).toFixed();
    mainHealthConcern.movement = parseFloat(
      (mainHealthConcern.movement / tot) * 100
    ).toFixed();
    mainHealthConcern.stress = parseFloat(
      (mainHealthConcern.stress / tot) * 100
    ).toFixed();
    mainHealthConcern.other = parseFloat(
      (mainHealthConcern.other / tot) * 100
    ).toFixed();
    if (this._isMounted) {
      this.setState({
        mainHealthConcern
      });
    }
  }

  _getAllusersByCompany() {
    this.props.data().then(res => {
      if (res.length > 0) {
        if (this._isMounted) {
          this.setState({
            users: res,
            registered_employee: res.length
          });
        }
      }
    });
  }

  _getCurrentReport = (reportId = null, isPrint) => {
    
    if (this._isMounted) this.setState({ loading: true, isPrint });

    const { reports } = this.state;

    this._setData(reports, reportId);
  }

  _setData(res, reportId) {
    
    let { reports, activeReport, reportInfo } = this.state;
    const btn = this.state.buttons;

    if (reportId) {
      const reslt = res;
      activeReport = reslt.filter(item => item.id === reportId);
      activeReport = activeReport[0];
    } else {
      activeReport = res[0];
    }

    if (activeReport) {
      reportInfo = activeReport;
    }
    reports = res;
    if (activeReport && activeReport.programs) {
      if (activeReport.status > 0) {
        btn.disabled = true;
      } else {
        btn.disabled = false;
      }
    }

    // get subscription_name from subrscription array
    if (this._isMounted) {
      this.setState(
        {
          reportInfo,
          activeReport,
          reports,
          buttons: btn
        },
        () => {
          this._updateReportList();
        }
      );
    }
  }

  _getHealthDeterminants() {
    this.props
      .healthDeterminants()
      .then(res => {
        if (this._isMounted) {
          this.setState(
            {
              determinants: res
            },
            () => this._prepareDeterminants()
          );
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _getFilteredData1(fltr) {
    const filters = this.state.filters[fltr];
    const determinants = this.state.determinants;
    let filtered = [];

    if (filters.value) {
      if (filters.filter === "branch") {
        filtered = determinants.filter(str => str.branch === filters.value);
      } else if (filters.filter === "department") {
        filtered = determinants.filter(str => str.department === filters.value);
      } else if (filters.filter === "job_grade") {
        filtered = determinants.filter(str => str.job_grade === filters.value);
      } else if (filters.filter === "high_risk") {
        filtered = determinants.filter(str => str.high_risk === filters.value);
      }
    } else {
      filtered = determinants;
    }
    if (this._isMounted) this.setState({ determinants: filtered });
  }

  _prepareDeterminants() {
    const { determinants = [] } = this.state;

    const filteredDeterminants = determinants.filter(
      d => d.healthsurvey_status === "Locked"
    ); // FILTER, ONLY PATIENT SUBMITTED HEALTH SURVEY

    let obj = {};

    if (
      Object.keys(this.state.filters).length > 0 &&
      this.state.filters.all.filter
    ) {
      this._getFilteredData1("all");
      obj = filteredDeterminants;
    } else {
      obj = filteredDeterminants;
    }

    const sleep = [];
    const detox = [];
    const nutrition = [];
    const weight = [];
    const movement = [];
    const stress = [];
    obj.forEach(item => {
      if (item.name === "sleep") {
        sleep.push({
          age_group: item.age_group,
          branch: item.branch,
          department: item.department,
          high_risk: item.high_risk,
          job_grade: item.job_grade,
          score: item.value,
          high_risk: item.high_risk
        });
      } else if (item.name === "detox") {
        detox.push({
          age_group: item.age_group,
          branch: item.branch,
          department: item.department,
          high_risk: item.high_risk,
          job_grade: item.job_grade,
          score: item.value,
          high_risk: item.high_risk
        });
      } else if (item.name === "nutrition") {
        nutrition.push({
          age_group: item.age_group,
          branch: item.branch,
          department: item.department,
          high_risk: item.high_risk,
          job_grade: item.job_grade,
          score: item.value,
          high_risk: item.high_risk
        });
      } else if (item.name === "weightScore") {
        weight.push({
          age_group: item.age_group,
          branch: item.branch,
          department: item.department,
          high_risk: item.high_risk,
          job_grade: item.job_grade,
          score: item.value,
          high_risk: item.high_risk
        });
      } else if (item.name === "exercise") {
        movement.push({
          age_group: item.age_group,
          branch: item.branch,
          department: item.department,
          high_risk: item.high_risk,
          job_grade: item.job_grade,
          score: item.value,
          high_risk: item.high_risk
        });
      } else if (item.name === "stress") {
        stress.push({
          age_group: item.age_group,
          branch: item.branch,
          department: item.department,
          high_risk: item.high_risk,
          job_grade: item.job_grade,
          score: item.value,
          high_risk: item.high_risk
        });
      }
    });
    if (this._isMounted) {
      this.setState({
        meanScore: { sleep, detox, nutrition, weight, movement, stress }
      });
    }
  }

  _getCompliance() {
    this.props
      .getCompliance()
      .then(res => {
        if (this._isMounted) {
          this.setState(
            {
              complianceData: res
            },
            () => {
              this._processCompliance();
            }
          );
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _processCompliance() {
    const complianceData = this.state.complianceData;
    let narr = [];
    if (
      Object.keys(this.state.filters).length > 0 &&
      this.state.filters.demographicsTop.filter !== ""
    ) {
      narr = complianceData.filter(
        // eslint-disable-next-line no-shadow
        complianceData =>
          complianceData[this.state.filters.demographicsTop.filter] ===
          this.state.filters.demographicsTop.value
      );
    }
    const arr = narr.length > 0 ? narr : complianceData;
    const branch = [];
    const department = [];

    arr.forEach(item => {
      if (branch.indexOf(item.branch) === -1) {
        branch.push(item.branch);
      }

      if (department.indexOf(item.department) === -1) {
        department.push(item.department);
      }
    });
    if (this._isMounted) {
      this.setState({
        branch,
        department
      });
    }
    const gdata = [];
    branch.forEach((item, i) => {
      let incomplete = 0;
      let complete = 0;
      gdata.push({ branch: item, incomplete: 0, complete: 0 });
      arr.forEach(subitem => {
        if (subitem.branch === item) {
          if (
            subitem.activity === "Complete" &&
            subitem.detox === "Complete" &&
            subitem.eyes_ears_nose === "Complete" &&
            subitem.head_and_mind === "Complete" &&
            subitem.health_goals === "Complete" &&
            subitem.health_issues === "Complete" &&
            subitem.mouth_throat_lungs === "Complete" &&
            subitem.movement === "Complete" &&
            subitem.nutrition === "Complete" &&
            subitem.other === "Complete" &&
            subitem.readiness === "Complete" &&
            subitem.sleep === "Complete" &&
            subitem.stress === "Complete" &&
            subitem.weight === "Complete" &&
            subitem.weight_and_digestion === "Complete"
          ) {
            complete++;
          } else {
            incomplete++;
          }
        }
      });
      gdata[i].incomplete = incomplete;
      gdata[i].complete = complete;
    });
    if (this._isMounted) {
      this.setState({
        barGrapdata: gdata
      });
    }
  }

  _getData() {
    this.props
      .data()
      .then(() => { })
      .catch(err => {
        console.log(err);
      });
  }

  _companyInfo() {
    const bc = this.state.companyInfo.logo;
    const imageSource = `data:image/jpeg;base64,${bc}`;

    return (
      <div className={wrapper}>
        {bc ? (
          <img alt="Company Logo.." src={imageSource} style={{ height: 80 }} />
        ) : null}
        <div className={title}>{this.state.companyInfo.company_name}</div>
        <div className={locationStyle}> {this.state.companyInfo.location} </div>
      </div>
    );
  }

  // _color(status) {
  //   //let status = 'alarming'
  //   const colors = {
  //     Optimal: "#83b85f",
  //     Suboptimal: "#83cde7",
  //     Neutral: "#ffc82f",
  //     Compromised: "#f6921e",
  //     Alarming: "#ec1c24"
  //   };
  //   return colors[status];
  // }

  // _companyOverAllHealthScore() {
  //   return (
  //     <div className={wrapper}>
  //       <span className={wrapper}>
  //         <strong>
  //           Company Overall Health Risk Score:{" "}
  //           <em>
  //             {this.state.scoreAndPercentile.average},{" "}
  //             <span
  //               style={{
  //                 color: `${GetColor(
  //                   GetStatus(this.state.scoreAndPercentile.average)
  //                 )}`
  //               }}
  //             >
  //               {GetStatus(this.state.scoreAndPercentile.average)}
  //             </span>
  //           </em>
  //         </strong>
  //       </span>
  //       <strong>
  //         Percentile Rank: {this.state.scoreAndPercentile.percentile}
  //       </strong>
  //       <div className={asflexTitle}>{this.state.activeReport.report_name}</div>
  //     </div>
  //   );
  // }

  // _subscriptions() {
  //   const activeReport = this.state.activeReport;
  //   const subscription_id = parseInt(activeReport.company_subscription_id);
  //   const subscriptionsRaw = this.state.subscriptions;

  //   // Remove Duplicates
  //   const subscriptions = subscriptionsRaw.filter(
  //     (raw, index, self) => index === self.findIndex(t => t.id === raw.id)
  //   );

  //   let items;
  //   if (subscriptions.length > 0) {
  //     items = subscriptions.map((item, i) => (
  //       <span className={radioStyle} key={i}>
  //         <input
  //           disabled={this.state.activeReport.status > 0}
  //           checked={subscription_id === item.id}
  //           onClick={this._radioChange.bind(this)}
  //           type="radio"
  //           name="subscription"
  //           value={item.id}
  //         />

  //         {item.program_code}
  //       </span>
  //     ));
  //   }

  //   return (
  //     <div className={subscriptionsListDiv}>
  //       <div className={subtitle}>
  //         {subscriptions.length > 1
  //           ? "Subscriptions"
  //           : subscriptions.length === 1
  //           ? "Subscription"
  //           : null}
  //       </div>
  //       {items}
  //     </div>
  //   );
  // }

  _radioChange(e) {
    const activeReport = this.state.activeReport;
    activeReport.company_subscription_id = e.target.value;
    if (this._isMounted) {
      this.setState({
        activeReport
      });
    }
  }

  // _getStatus(score) {
  //   const wtdScore = score;

  //   let subOptimalLabel = "";
  //   if (wtdScore >= 1.0 && wtdScore <= 2.9) {
  //     subOptimalLabel = "Optimal";
  //   } else if (wtdScore >= 3.0 && wtdScore <= 4.9) {
  //     subOptimalLabel = "Suboptimal";
  //   } else if (wtdScore >= 5.0 && wtdScore <= 5.9) {
  //     subOptimalLabel = "Neutral";
  //   } else if (wtdScore >= 6.0 && wtdScore <= 8.5) {
  //     subOptimalLabel = "Compromised";
  //   } else if (wtdScore >= 8.6) {
  //     subOptimalLabel = "Alarming";
  //   }

  //   return subOptimalLabel;
  // }

  _complialancePercentage() {
    const x = this.state.barGrapdata;

    let incomplete = 0;
    let complete = 0;

    x.forEach(item => {
      incomplete += item.incomplete;
      complete += item.complete;
    });

    const filtered_data = this.state.recievedDemographicsFiltedData;
    const percent =
      (filtered_data.total_submitted / filtered_data.total_dashboard_logins) *
      100;
    const toFloat = filtered_data.total_submitted > 0 ? parseInt(percent) : 0;

    // const percent =
    //   (complete / this.state.recievedDemographicsFiltedData.length) * 100;
    // const toFloat = parseFloat(percent).toFixed();
    return { percentile: toFloat, complete, incomplete };
  }

  _getUserFilters() {
    return this.props
      .filters()
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
      });
  }

  // GET FILTERED DATA FOR ALL GRAPH
  _getFilteredData(filter_column, filter_value, filterFor, selected_filters) {
    const filters = this.state.filters;
    filters[filterFor].filter = filter_column;
    filters[filterFor].value = filter_value;
    filters[filterFor].init = false;
    filters[filterFor].filters = selected_filters;

    if (this._isMounted) this.setState({ filters });
    if (filterFor === "all") {
      this._getHealthDeterminants();
    } else if (filterFor === "goals") {
      this._getQuery();
    }
  }

  _filterHG(fltr, obj) {
    const filters = fltr;
    let determinants = obj;
    if (filters.filters) {
      if (filters.filters.branch) {
        determinants = determinants.filter(
          str => str.branch === filters.filters.branch
        );
      }

      if (filters.filters.department && filters.filters.department.length > 0) {
        determinants = determinants.filter(
          str => str.department === filters.filters.department
        );
      }

      if (filters.filters.jobgrade) {
        determinants = determinants.filter(
          str => str.job_grade === filters.filters.jobgrade
        );
      }

      if (filters.filters.high_risk) {
        determinants = determinants.filter(str => {
          if (filters.filters.high_risk === "High Risk") {
            return str.high_risk === HIGH_RISK;
          } else if (filters.filters.high_risk === "High Value") {
            return str.high_risk === HIGH_VALUE;
          }
          return str.high_risk === 0 || str.high_risk == null;
        });
      }
      if (filters.filters.age_group) {
        determinants = determinants.filter(
          str => str.age_group === filters.filters.age_group
        );
      }

      return determinants;
    }
    return determinants;
  }

  _healthGoals() {
    let complianceData = [];

    // NEW LOGIC FOR FOR THIS
    const obj = this.state.complianceData;

    const filter = this.state.filters.demographicsTop;

    if (filter && filter.filters) {
      complianceData = this._filterHG(filter, obj);
    } else {
      complianceData = obj;
    }

    // eslint-disable-next-line no-unused-vars
    let incomplete = 0;
    let complete = 0;

    complianceData.forEach(subitem => {
      if (
        subitem.activity === "Complete" &&
        subitem.detox === "Complete" &&
        subitem.eyes_ears_nose === "Complete" &&
        subitem.head_and_mind === "Complete" &&
        subitem.health_goals === "Complete" &&
        subitem.health_issues === "Complete" &&
        subitem.mouth_throat_lungs === "Complete" &&
        subitem.movement === "Complete" &&
        subitem.nutrition === "Complete" &&
        subitem.other === "Complete" &&
        subitem.readiness === "Complete" &&
        subitem.sleep === "Complete" &&
        subitem.stress === "Complete" &&
        subitem.weight === "Complete" &&
        subitem.weight_and_digestion === "Complete"
      ) {
        if (subitem.healthsurvey_status === "Active") {
          complete++;
        }
      } else {
        incomplete++;
      }
    });

    const filtered_data = this.state.recievedDemographicsFiltedData;
    const percent =
      (filtered_data.total_submitted / filtered_data.total_dashboard_logins) *
      100;
    const toFloat = filtered_data.total_submitted > 0 ? parseInt(percent) : 0;

    const styles = {
      style: { textAlign: 'left' }
    }

    const goalStyles = {
      borderRadius: 5,
      boxShadow: '0 5px 8px rgba(0,0,0,.1)',
      overflow: 'hidden'
    }

    const rowStyle = (compliance) => ({
      columnSize: "1fr 80px",
      childStyles: {
        padding: '4px 20px',
        width: '100%',
        color: compliance ? '#fff' : null,
        background: compliance ? Colors.skyblue : '#fff'
      }
    })

    const value = {
      style: {
        borderLeft: `1px solid ${Colors.skyblue}`,
      }
    }

    return (
      <div className={wrapper} style={{ paddingBottom: 20 }}>
        <GridContainer columnSize="1fr 1fr" gap={20} styles={{ marginTop: 20, fontSize: 'smaller', rowGap: 0 }}>

          <GridContainer {...rowStyle(true)} styles={{ gridColumn: '1/span 6' }} >
            <span {...styles}>Compliance Rate: <strong {...value}>{`${toFloat}%`}</strong></span>
          </GridContainer>

          <GridContainer styles={{ ...goalStyles, height: 'fit-content' }}>
            <GridContainer {...rowStyle()}  >
              <span {...styles}>Total registered:</span>
              <strong {...value}>{filtered_data.total_registered}</strong>
            </GridContainer>

            <GridContainer {...rowStyle()} >
              <span {...styles}>Dashboard Logins:</span>
              <strong {...value}>{filtered_data.total_dashboard_logins}</strong>
            </GridContainer>

            <GridContainer {...rowStyle()} >
              <span {...styles}>Total respondents:</span>
              <strong {...value}>{filtered_data.total_respondents}</strong>
            </GridContainer>
          </GridContainer>

          <GridContainer styles={{ ...goalStyles }}>
            <GridContainer {...rowStyle()} >
              <span {...styles}>Incomplete:</span>
              <strong {...value}>{incomplete}</strong>
            </GridContainer>

            <GridContainer {...rowStyle()} >
              <span {...styles}>Complete (not yet submitted):</span>
              <strong {...value}>{complete}</strong>
            </GridContainer>

            <GridContainer {...rowStyle()} >
              <span {...styles}>Submitted:</span>
              <strong {...value}>{filtered_data.total_submitted}</strong>
            </GridContainer>

          </GridContainer>

        </GridContainer>



      </div>
    );
  }

  _healthGoalsChart(dflt) {
    const { report_name, company_name } = this.state.activeReport;

    const dta = this.state.mainHealthConcern;

    const data = [
      { name: "Weight", rlbl: dta.weight, labelCount: `${dta.weight}%` },
      {
        name: "Nutrition",
        rlbl: dta.nutrition,
        labelCount: `${dta.nutrition}%`
      },
      { name: "Detox", rlbl: dta.detox, labelCount: `${dta.detox}%` },
      { name: "Sleep", rlbl: dta.sleep, labelCount: `${dta.sleep}%` },
      { name: "Stress", rlbl: dta.stress, labelCount: `${dta.stress}%` },
      { name: "Movement", rlbl: dta.movement, labelCount: `${dta.movement}%` },
      { name: "Others", rlbl: dta.other, labelCount: `${dta.other}%` }
    ];

    return (
      <div className={pageBreakBefore}>
        <GridContainer columnSize="1fr 1fr" fullWidth>
          <Label fullWidth text="II. Main Health Goals/Concerns " />

          {this.state.activeReport.status == null ?
            <DefaultFilters
              withBackground
              fromReport
              defaultValue={dflt}
              filters={this._getUserFilters}
              filterData={this._getFilteredData}
              filterfor={"goals"}
              label={"Filter Health Goals by"}
            />
            : null}

        </GridContainer>

        <ResizableDiv
          isGraph
          name="goalChart"
          setDivAlignment={this._setDivAlignment}
          maxWidth={800}
          defaultSize={{ width: 800, height: 400 }}
          filters={this.state.filters.boxProps}
        >
          <ResponsiveContainer>
            <BarChart
              layout="vertical"
              data={data}
              margin={{ top: 0, right: 50, left: 0, bottom: 0 }}
            >
              <XAxis
                type="number"
                unit="%"
                tick={{ transform: "translate(5, 3)" }}
                fontSize={'smaller'}
              />
              <YAxis
                type="category"
                dataKey="name"
                width={120}
                tick={{ transform: "translate(-10, 0)" }}
                fontSize={'smaller'}
              />
              <CartesianGrid strokeDasharray="3 3" />
              <Bar dataKey="rlbl" stackId="a" fill="#80cde9">
                <LabelList dataKey="labelCount" position="inside" style={{ fontSize: 'smaller' }} />
              </Bar>
            </BarChart>
          </ResponsiveContainer>
        </ResizableDiv>

        {this.state.activeReport.status == null && !this.state.loading && this.state.mainHealhGoals.length > 0 ? (
          <Preview
            company_name={company_name}
            report_name={report_name}
            data={this.state.mainHealhGoals}
          />
        ) : null}

        {this.chiAnalysis(this.state.analysis.goals, 'goals')}
      </div>
    );
  }

  _getAverage(elmt) {
    let sum = 0;
    for (let i = 0; i < elmt.length; i++) {
      sum += parseFloat(elmt, 10); //don't forget to add the base
    }

    const avg = sum / elmt.length;
    return avg;
  }

  _displayMeanScore(displayfor) {
    const branch = this.state.branch;
    const department = this.state.department;
    const allScore = this.state.scoreAndPercentile.scores;
    const avaragePerBranch = [];

    const averagePerDepartment = [];
    branch.forEach((item, i) => {
      const avaragePerBranchScore = [];
      avaragePerBranch.push({ branch: item, average: 0 });
      allScore.forEach(subitem => {
        if (subitem.branch === item) {
          avaragePerBranchScore.push(subitem.healthScore);
        }
      });
      const ave = parseFloat(this._getAverage(avaragePerBranchScore)).toFixed();
      avaragePerBranch[i].average = ave;
    });

    department.forEach((arr, y) => {
      const avaragePerDepartmentScore = [];
      averagePerDepartment.push({ department: arr, average: 0 });
      allScore.forEach(depitem => {
        if (depitem.department === arr) {
          avaragePerDepartmentScore.push(depitem.healthScore);
        }
      });

      const ave = parseFloat(
        this._getAverage(avaragePerDepartmentScore)
      ).toFixed();
      averagePerDepartment[y].average = ave;
    });

    if (displayfor === "department") {
      return averagePerDepartment.map((dept, y) => (
        <div key={y} className={meanDiv}>
          <div className={meanItemsLeft}>{dept.department}</div>
          <div className={meanItems}>{dept.average}</div>
          <div className={meanItems}>
            <span
              style={{
                color: `${GetColor(GetStatus(dept.average))}`
              }}
            >
              {GetStatus(dept.average)}
            </span>
          </div>
        </div>
      ));
    }
    return avaragePerBranch.map((brnch, i) => (
      <div key={i} className={meanDiv}>
        <div className={meanItemsLeft}>{brnch.branch}</div>
        <div className={meanItems}>{brnch.average}</div>
        <div className={meanItems}>{GetStatus(brnch.average)}</div>
      </div>
    ));
  }

  _meanScore(branch) {
    return (
      <div className={wrapper}>
        <span className={title}>Mean Health Risk Score Per {branch}</span>

        <div className={meanDiv}>
          <div className={meanLabel}>{branch}</div>
          <div className={meanLabel}>Score</div>
          <div className={meanLabel}>Health Status</div>
        </div>
        {this._displayMeanScore(branch)}
      </div>
    );
  }

  _drawCircle(color) {
    return (
      <svg height="14" width="14">
        <circle cx="7" cy="7" r="6" stroke="black" stroke="0" fill={color} />
      </svg>
    );
  }

  // REPLACE this.multifilter with Multifilter

  // multiFilter(array, filters) {
  //   const filterKeys = Object.keys(filters);
  //   return array.filter(item =>
  //     filterKeys.every(key => !!~filters[key].indexOf(item[key]))
  //   );
  // }

  _filterDoh(fltr, obj) {
    const filters = this.state.filters[fltr];
    let determinants = obj;

    if (
      filters.filters &&
      filters.filters.branch &&
      filters.filters.branch.length > 0
    ) {
      const fltrs = { branch: filters.filters.branch };
      determinants = MultiFilter(determinants, fltrs);
    }

    if (
      filters.filters &&
      filters.filters.department &&
      filters.filters.department.length > 0
    ) {
      const fltrs = { department: filters.filters.department };
      determinants = MultiFilter(determinants, fltrs);
    }

    if (
      filters.filters &&
      filters.filters.jobgrade &&
      filters.filters.jobgrade.length > 0
    ) {
      const fltrs = { job_grade: filters.filters.jobgrade };
      determinants = MultiFilter(determinants, fltrs);
    }

    if (
      filters.filters &&
      filters.filters.high_risk &&
      filters.filters.high_risk.length > 0
    ) {
      const fltrs = { high_risk: filters.filters.high_risk };
      determinants = MultiFilter(determinants, fltrs);
    }

    if (
      filters.filters &&
      filters.filters.age_group &&
      filters.filters.age_group.length > 0
    ) {
      const fltrs = { age_group: filters.filters.age_group };
      determinants = MultiFilter(determinants, fltrs);
    }

    return determinants;
  }

  _pieValues(string) {
    const filters = this.state.filters;
    let obj = {};

    if (filters[string] && filters[string].filter) {
      obj = this._filterDoh(string, this.state.meanScore[string]);
    } else {
      obj = this.state.meanScore[string];
    }

    let alarmingCtr = 0;
    let alarmingHRCtr = 0;
    let alarmingHVCtr = 0;
    let nonealarmingHRCtr = 0;

    let compromisedCtr = 0;
    let compromisedHRCtr = 0;
    let compromisedHVCtr = 0;
    let noneCompromisedHRCtr = 0;

    let neutralCtr = 0;
    let neutralHRCtr = 0;
    let neutralHVCtr = 0;
    let noneNeutralHRCtr = 0;

    let subOptimalCtr = 0;
    let subOptimalHRCtr = 0;
    let subOptimalHVCtr = 0;
    let noneSubOptimalHRCtr = 0;

    let optimalCtr = 0;
    let optimalHRCtr = 0;
    let optimalHVCtr = 0;
    let noneOptimalHRCtr = 0;

    let count = 0;

    // eslint-disable-next-line no-unused-vars
    let data = [];

    obj.forEach(item => {
      if (item.score >= 1.0 && item.score <= 2.9) {
        optimalCtr++;
        if (item.high_risk === 1) {
          optimalHVCtr++;
        } else if (item.high_risk === 2) {
          optimalHRCtr++;
        }
        if (item.high_risk !== 1) {
          noneOptimalHRCtr++;
        }
      } else if (item.score >= 3.0 && item.score <= 4.9) {
        subOptimalCtr++;
        if (item.high_risk === 1) {
          subOptimalHVCtr++;
        } else if (item.high_risk === 2) {
          subOptimalHRCtr++;
        }
        if (item.high_risk !== 1) {
          noneSubOptimalHRCtr++;
        }
      } else if (item.score >= 5.0 && item.score <= 5.9) {
        neutralCtr++;
        if (item.high_risk === 1) {
          neutralHVCtr++;
        } else if (item.high_risk === 2) {
          neutralHRCtr++;
        }
        if (item.high_risk !== 1) {
          noneNeutralHRCtr++;
        }
      } else if (item.score >= 6.0 && item.score <= 8.5) {
        compromisedCtr++;
        if (item.high_risk === 1) {
          compromisedHVCtr++;
        } else if (item.high_risk === 2) {
          compromisedHRCtr++;
        }
        if (item.high_risk !== 1) {
          noneCompromisedHRCtr++;
        }
      } else if (item.score >= 8.6) {
        alarmingCtr++;
        if (item.high_risk === 1) {
          alarmingHVCtr++;
        } else if (item.high_risk === 2) {
          alarmingHRCtr++;
        }
        if (item.high_risk !== 1) {
          nonealarmingHRCtr++;
        }
      }
      count++;
    });

    const hrCtr = {
      alarmingCtr,
      alarmingHRCtr,
      alarmingHVCtr,
      nonealarmingHRCtr,

      compromisedCtr,
      compromisedHRCtr,
      compromisedHVCtr,
      noneCompromisedHRCtr,

      neutralHRCtr,
      neutralHRCtr,
      neutralHVCtr,
      noneNeutralHRCtr,

      subOptimalCtr,
      subOptimalHRCtr,
      subOptimalHVCtr,
      noneSubOptimalHRCtr,

      optimalCtr,
      optimalHRCtr,
      optimalHVCtr,
      noneOptimalHRCtr,

      count
    };

    const pieValue = [
      { name: "Alarming", value: alarmingCtr },
      { name: "Compromised", value: compromisedCtr },
      { name: "Neutral", value: neutralCtr },
      { name: "SubOptimal", value: subOptimalCtr },
      { name: "Optimal", value: optimalCtr }
    ];

    return (data = [hrCtr, pieValue]);
  }

  _textareaResizeHandler = (e, chi) => {
    const isCHIrowFalse = this.state.chiRow[chi] === false;
    const textareaHeight = e ? e.target.offsetHeight : null;

    if (textareaHeight) {
      if (textareaHeight > 350) {
        if (isCHIrowFalse) {
          setTimeout(() => {
            this.setState({
              chiRow: { ...this.state.chiRow, [chi]: true }
            });
          }, 600);
        }
      } else {
        this.setState({
          chiRow: { ...this.state.chiRow, [chi]: false }
        });
      }
    }
  }

  _sixDeterminants(programName) {
    let flexDirection = 'initial';
    if (this.state.filters.boxProps && this.state.filters.boxProps[`${programName}_pie`]) {
      flexDirection = this.state.filters.boxProps[`${programName}_pie`].alignment;
    }

    const piedata = this._pieValues(programName);
    const program = this.state.programs[programName];
    program.totalHV =
      parseInt(piedata[0].compromisedHVCtr) +
      parseInt(piedata[0].alarmingHVCtr);
    program.totalRegular =
      parseInt(piedata[0].noneCompromisedHRCtr) +
      parseInt(piedata[0].nonealarmingHRCtr);

    let dflt = { defName: "branch", defValue: "" };
    if (this.state.filters[programName] && this.state.filters[programName].filter !== "") {
      dflt = this.state.filters[programName].filters;
    }

    const setProgram = (name, bg = false) => {
      switch (name) {
        case 'weight':
          if (bg) {
            return Colors.weightColor
          }
          return 'A. Weight'

        case 'nutrition':
          if (bg) {
            return Colors.nutritionColor
          }
          return 'B. Nutrition'

        case 'detox':
          if (bg) {
            return Colors.detoxColor
          }
          return 'C. Detox'

        case 'sleep':
          if (bg) {
            return Colors.sleepColor
          }
          return 'D. Sleep'

        case 'movement':
          if (bg) {
            return Colors.movementColor
          }
          return 'E. Movement'

        case 'stress':
          if (bg) {
            return Colors.stressColor
          }
          return 'F. Stress'

        default:
          return 'Title Here'
      }
    }

    const isRow = this.state.chiRow[programName];

    return (
      <div id={`${programName}-wrapper`} className={!isRow && avoidPageBreakInside}>
        <GridContainer
          dataID="determinants-title-wrapper"
          columnSize="1fr 1fr"
          styles={{ background: setProgram(programName, true) }}
          fullWidth
        >
          <Label text={setProgram(programName)} fullWidth styles={{ paddingLeft: 10, color: 'white' }} />

          {this.state.activeReport.status == null ?
            <DefaultFilters
              fromReport
              defaultValue={dflt}
              filters={this._getUserFilters}
              filterData={this._getFilteredData}
              filterfor={programName}
              label={`Filter ${programName} by`}
            />
            : null}
        </GridContainer>

        <div className={earlyMarkersDivStyle} style={isRow ? { display: 'grid', gridTemplateColumns: '1fr', width: '100%' } : { flexDirection }}>

          <section style={{ margin: isRow ? '0 auto 10px' : 0 }}>
            {this._pieChart(piedata, programName)}
          </section>

          <div style={isRow ? { width: '100%' } : { flex: '1 0 40%', textAlign: "justify", padding: '0 20px' }}>
            <section id={`${programName}-analysis-wrapper`} style={{ textAlign: 'justify' }}>
              <Label fullWidth subheader text="Analysis and recommendations" styles={{ margin: '0 auto 0 10px' }} />
              {this.state.activeReport.status > 0 ?
                (
                  <div className={analysisDiv}>{this.state.analysis[programName] || noAnalysis}</div>
                )
             :                   
                <TextareaAutosize
                  placeholder={
                    program.analysis
                      ? null
                      : "Write your analysis and recommendations here"
                  }
                  disabled={this.state.activeReport.status > 0}
                  className={insightsTextArea(true)}
                  id={programName}
                  name={programName}
                  defaultValue={this.state.analysis[programName]}
                  onBlur={this._handleChange.bind(this)}
                />
              }
            </section>
          </div>
        </div>
      </div>
    );
  }

  _handleDescChange(e) {
    e.preventDefault();
    const name = e.target.id.split("_");
    const earlyMarkers = this.state.earlyMarkers;
    earlyMarkers[name[0]][name[1]].description = e.target.value;
    if (this._isMounted) {
      this.setState({
        earlyMarkers
      });
    }
  }

  _handleChange(e) {
    e.preventDefault();
    const analysis = this.state.analysis;

    analysis[e.target.name] = e.target.value;
    if (this._isMounted) {
      this.setState({
        analysis
      });
    }
  }

  _pieChart(pieData, programName) {
    const data = pieData[1];
    const piedata = pieData;
    const COLORS = ["#ff0000", "#d77f1a", "#dfaf29", "#71b2ca", "#72a153"];
    const RADIAN = Math.PI / 180;
    const renderCustomizedLabel = ({
      cx,
      cy,
      midAngle,
      innerRadius,
      outerRadius,
      percent
    }) => {
      const radius = innerRadius + (outerRadius - innerRadius) * 0.1;
      const x = cx + radius * Math.cos(-midAngle * RADIAN);
      const y = cy + radius * Math.sin(-midAngle * RADIAN);

      return (
        <text
          className={`${pieTextStyle}`}
          x={x}
          y={y}
          textAnchor={x > cx ? "start" : "end"}
          dominantBaseline="central"
        >
          {percent > 0 ? `${(percent * 100).toFixed()}%` : null}
        </text>
      );
    };

    return (
      <ResizableDiv
        fromReport
        isGraph
        name={`${programName}_pie`}
        setDivAlignment={this._setDivAlignment}
        maxWidth={700}
        defaultSize={{ width: 350, height: 350 }}
        filters={this.state.filters.boxProps}
      >
        <ResponsiveContainer width="100%">
          <PieChart onMouseEnter={this.onPieEnter}>
            <Pie
              dataKey="value"
              data={data}
              labelLine={false}
              label={renderCustomizedLabel}
              isAnimationActive={false}
              outerRadius={"100%"}
              innerRadius={"60%"}
              fill="#8884d8"
            >
              {data != null ? (
                data.map((entry, index) => (
                  <Cell key={index} fill={COLORS[index % COLORS.length]} />
                ))
              ) : (
                  <p>wait</p>
                )}
            </Pie>
            <Pie
              dataKey="value"
              data={data}
              isAnimationActive={false}
              outerRadius={"95%"}
              innerRadius={"94.5%"}
              fill="#fff"
            />
          </PieChart>
        </ResponsiveContainer>

        <div className={pieCornerTextStyle}>
          <div
            style={{
              display: "flex",
              flexWrap: "wrap",
              flex: 1,
              width: "0 auto"
            }}
          >
            {piedata[0].noneCompromisedHRCtr || piedata[0].compromisedHVCtr ? (
              <div
                className={legnd}
                style={{ color: `${GetColor("Compromised")}`, height: 20, textAlign: 'center' }}
              >
                {((piedata[1][1].value / piedata[0].count) * 100).toFixed()}%
                Compromised (R={piedata[0].noneCompromisedHRCtr}, HV=
                {piedata[0].compromisedHVCtr})
              </div>
            ) : null}

            {piedata[0].nonealarmingHRCtr || piedata[0].alarmingHVCtr ? (
              <div
                className={legnd}
                style={{ color: `${GetColor("Alarming")}`, height: 20, textAlign: 'center' }}
              >
                {((piedata[1][0].value / piedata[0].count) * 100).toFixed()}%
                Alarming (R={piedata[0].nonealarmingHRCtr}, HV=
                {piedata[0].alarmingHVCtr})
              </div>
            ) : null}

            {piedata[0].noneNeutralHRCtr || piedata[0].neutralHVCtr ? (
              <div
                className={legnd}
                style={{ flex: "1 0 100%", color: `${GetColor("Neutral")}`, height: 20, textAlign: 'center' }}
              >
                {((piedata[1][2].value / piedata[0].count) * 100).toFixed()}%
                Neutral (R={piedata[0].noneNeutralHRCtr}, HV=
                {piedata[0].neutralHVCtr})
              </div>
            ) : null}

            {piedata[0].noneSubOptimalHRCtr || piedata[0].subOptimalHVCtr ? (
              <div
                className={legnd}
                style={{ flex: "1 0 100%", color: `${GetColor("Suboptimal")}`, height: 20, textAlign: 'center' }}
              >
                {((piedata[1][3].value / piedata[0].count) * 100).toFixed()}%
                Suboptimal (R={piedata[0].noneSubOptimalHRCtr}, HV=
                {piedata[0].subOptimalHVCtr})
              </div>
            ) : null}

            {piedata[0].noneOptimalHRCtr || piedata[0].optimalHVCtr ? (
              <div
                className={legnd}
                style={{ color: `${GetColor("Optimal")}`, height: 20, textAlign: 'center' }}
              >
                {((piedata[1][4].value / piedata[0].count) * 100).toFixed()}%
                Optimal (R={piedata[0].noneOptimalHRCtr}, HV=
                {piedata[0].optimalHVCtr})
              </div>
            ) : null}
          </div>

          <div style={{ flex: "1 0 100%", textAlign: 'center', height: 25, fontWeight: 500, lineHeight: 3, fontSize: 16 }} className={legnd}>
            N={piedata[0].count}
          </div>

        </div>
      </ResizableDiv>
    );
  }

  _displayPacakge(obj) {
    const html = [];
    let ctr = 0;
    Object.keys(obj).forEach((item, x) => {
      ctr++;
      html.push(
        <div key={item + x} className={listTitle}>
          {obj[item].name}
        </div>
      );
      obj[item].list.forEach((list, i) => {
        html.push(
          <span key={`${ctr}_${i}`} className={listStyle}>
            {list}
          </span>
        );
      });
    });
    return <div>{html}</div>;
  }

  _updateBox = (e) => {
    const id = document.getElementById(e.target.name);
    const names = e.target.name.split("_");

    const programs = this.state.programs;

    if (id.checked) {
      programs[names[0]][names[1]].selected = true;
    } else {
      programs[names[0]][names[1]].selected = false;
    }
    if (this._isMounted) {
      this.setState({
        programs
      });
    }
  }

  _addEarlyMarker(doh) {
    const earlyMarkers = this.state.earlyMarkers;
    earlyMarkers[doh].push({
      imageLink: "",
      description: ""
    });
    if (this._isMounted) {
      this.setState({
        earlyMarkers
      });
    }
  }

  _addEarlymarkers(doh) {
    return (
      <Button styles={{ maxWidth: 'fit-content', borderRadius: 8, minWidth: 'unset', maxWidth: 'unset', fontSize: 'small', margin: '0 0 0 10px' }} right type="darkpink">
        <button onClick={() => this._addEarlyMarker(doh)}>
          + Add Early Markers of Health Imbalances
      </button>
      </Button>
    );
  }

  _removeEM(ind) {
    const index = ind.split("_");
    const earlyMarkers = this.state.earlyMarkers;

    earlyMarkers[index[0]].splice(index[1], 1);
    if (this._isMounted) {
      this.setState({
        earlyMarkers
      });
    }
  }

  _getLeadDoctor() {
    Report.getLeadDoctor(this.state.companyInfo.id)
      .then(res => {
        if (this._isMounted) this.setState({ leadDoctor: res });
      })
      .catch(e => console.log(e));
  }

  // send email notification to HRs
  sendEmailNotificationToHR = () => {
    const { companyHR } = this.state;

    let recipient = "";

    companyHR.forEach(hr => {
      recipient += `${hr.email},`;
    });

    const to = recipient;
    const subject = "Corporate Health Index Report";
    const text = "Corporate Health Index Report";
    const content = `<p style="font-size: 16px; color: #000; text-align: left !important;">Our medical team has sent in their analysis and recommendations for your company. The report is available under the <strong>Summary and Analysis</strong> tab. For questions and clarifications, please reach out to your account officer.</p>`;
    const emailContent = { to, content, subject, text };

    email
      .sendEmailToHR(emailContent)
      .then(res => {
        console.log(res);
      })
      .catch(e => {
        console.log(e);
      });

    this._sendReportNotificationToHR();
  };

  // send system notification to HRs
  _sendReportNotificationToHR = () => {
    const sender = {
      type: "Doctor",
      name: this.props.user.name,
      id: this.props.user.id
    };

    const date_created = moment(Date.now()).format();

    this.state.companyHR.forEach(hr => {
      const data = {
        type: "admin",
        name: hr.name.trim(),
        id: hr.id,
        appointmentDate: moment(Date.now()).format(),
        patientName: this.state.companyInfo.company_name
      };

      const message =
        "Our medical team has sent in their analysis and recommendations for your company. The report is available under the Summary and Analysis tab. For questions and clarifications, please reach out to your account officer.";

      const subject = "Corporate Health Index Report";

      const notificationDetails = {
        sender,
        data,
        message,
        subject,
        date_created
      };
      this._sendReportSystemNotification(notificationDetails);
    });
  };

  _sendReportSystemNotification = notificationDetails => {
    notification
      .sendReportNotification(notificationDetails)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
  };

  handleImageUpload(file, doh, i) {
    const date = new Date();
    const prefix = dateFormat(date, "yymmddhmmss");
    const filename = prefix + file.name;
    const fd = new FormData();
    let upload = "";

    if (this.state.avatar) {
      fd.append("UserId", this.state.userId);
      fd.append("oldAvatar", this.state.avatar);
      fd.append("prefix", prefix);
      fd.append("file", file);

      upload = request.post(CLOUDINARY_UPLOAD_URL, fd, {
        onUploadProgress: ProgressEvent => {
          console.log(
            `Upload Progress ${Math.round((ProgressEvent.loaded / ProgressEvent.total) * 100)}`
          );
        }
      });
    } else if (doh === "PDFReport") {
      const company_id = this.state.reportInfo.company_id;
      const md_id = this.state.reportInfo.md_id;
      const prefix1 = `${company_id}-${md_id}-${prefix}_`;

      fd.append("prefix", prefix1);
      fd.append("is_report", true);
      fd.append("file", file);
      console.log('ffffffffffff', fd)
      upload = request.post(CLOUDINARY_REPORT_UPLOAD_URL, fd, {
        onUploadProgress: ProgressEvent => {
          console.log(
            `Upload Progress ${Math.round((ProgressEvent.loaded / ProgressEvent.total) * 100)}`
          );
        }
      });
    } else {
      fd.append("prefix", prefix);
      fd.append("file", file);

      upload = request.post(CLOUDINARY_UPLOAD_URL, fd, {
        onUploadProgress: ProgressEvent => {
          console.log(
            `Upload Progress ${
            Math.round((ProgressEvent.loaded / ProgressEvent.total) * 100)}`
          );
        }
      });
    }

    upload.then(response => {
      if (response.status === 200) {
        if (doh === "PDFReport") {
          this.state.activeReport.status = 2;
          this.state.activeReport.file_name = response.data.filename;
          delete this.state.activeReport.subscription_name;
          delete this.state.activeReport.active;
          this._updateReportStatus();
        } else {
          this._saveImage(doh, i, filename);
        }
      } else {
        console.log(err);
      }
    });
  }

  _updateReportStatus() {
    Report.updateReport(this.state.activeReport)
      .then(res => {
        if (res) {
          this.sendEmailNotificationToHR();
          if (this._isMounted) {
            this.setState(
              {
                responseMessage: "Report was successfully sent to HR"
              },
              function () {
                setTimeout(() => {
                  this._clear();
                }, 5000);
              }
            );
          }
          this._getReportFromDatabase();
        }
      })
      .catch(() => {
        if (this._isMounted) {
          this.setState(
            {
              errorMessage: "Error on sending report"
            },
            function () {
              setTimeout(() => {
                this._clear();
              }, 5000);
            }
          );
        }
      });
  }

  _saveImage(doh, index, filename) {
    const earlyMarkers = this.state.earlyMarkers;
    earlyMarkers[doh][index].imageLink = filename;
    if (this._isMounted) {
      this.setState({
        earlyMarkers
      });
    }
  }

  _earlyMarkers(doh) {
    let flexDirection = 'initial';

    const em = this.state.earlyMarkers[doh];

    const emDiv = em.map((item, i) => {
      if (this.state.filters.boxProps && this.state.filters.boxProps[`${doh}_earlyMarker_${i}`]) {
        flexDirection = this.state.filters.boxProps[`${doh}_earlyMarker_${i}`].alignment;
      }

      return (
        <section key={i} className={avoidPageBreakInside}>
          {this.chiHeader(`Early Markers of Health Imbalances for ${doh}`, doh, null, false, i)}
          <div className={earlyMarkersDiv} style={{ flexDirection }}>
            <ResizableDiv
              isGraph
              name={`${doh}_earlyMarker_${i}`}
              setDivAlignment={this._setDivAlignment}
              defaultSize={{ width: 250, height: 250 }}
              filters={this.state.filters.boxProps}
              fromReport
            >
              <div id="dropzone-wrapper" className={dropzoneWrapper}>
                <Dropzone
                  disabled={this.state.activeReport.status > 0}
                  data-tip="Click or Drag Image here to change."
                  onDrop={files => {
                    this.onImageDrop(files, doh, i);
                  }}
                  multiple={false}
                  maxSize={300000} // limit to 300 KB
                  accept="image/jpeg, image/png"
                >
                  {({ getRootProps, getInputProps }) => (
                    <div className={`${dropStyle}`} {...getRootProps()}>
                      <input {...getInputProps()} />
                      {
                        !this.state.uploadError &&
                        <img
                          style={
                            !this.state.earlyMarkers[doh][i].imageLink ?
                              { width: '50%', margin: '0 auto 50px', opacity: .3 } :
                              { width: '100%' }}

                          src={this.state.earlyMarkers[doh][i].imageLink ? `/static/avatar/${
                            this.state.earlyMarkers[doh][i].imageLink
                            }` : "/static/images/upload.png"}

                          onError={e => {
                            e.target.onerror = null;
                          }}
                        />
                      }

                      {!this.state.activeReport.status &&
                        <label style={{ opacity: !this.state.earlyMarkers[doh][i].imageLink ? .5 : 1 }}>
                        Click here to select
                        file or drop files here
                        to upload.
                      </label>}
                    </div>
                  )}
                </Dropzone>
              </div>
              {
                this.state.uploadError &&
                <span className={uploadError}>{this.state.uploadError}</span>
              }

            </ResizableDiv>

            {
              this.chiAnalysis(this.state.earlyMarkers[doh][i].description, `${doh}_${i}`, true)
            }

          </div>
        </section>
      );
    }
    );
    return (
      <section className={avoidPageBreakInside} style={{ width: '100%', margin: '20px auto' }}>
        {emDiv}
      </section>
    );
  }

  _recommendations(str) {
    const programs = this.state.programs[str];
    const basicId = `${str}_basic`;
    //const premiumId = `${str}_premium`;

    const displayValue =
      !this.state.activeReport.status ||
        programs.premium.selected ||
        programs.basic.selected
        ? "block"
        : "none";

    const recoWrapper = css({
      display: "flex",
      flex: "1 0 100%",
      flexWrap: "wrap",
      justifyContent: "center",
      padding: 15,
      display: `${displayValue}`,
      background: '#eee3',
      marginTop: 20,
      borderRadius: 10,
      "@media print": {
        marginTop: '10px !important'
      }
    });

    return (
      <div className={`${recoWrapper} ${avoidPageBreakInside}`}>
        <Label text="Recommendations" />

        {!this.state.activeReport.status ? (
          <div className={subwrapper}>
            <Checkbox
              id={basicId}
              name={basicId}
              value={basicId}
              checked={programs.basic.selected}
              onChange={this._updateBox.bind(this)}
              label={`
                ${programs.basic.title}
                ${programs.totalRegular > 0 ? `for (${programs.totalRegular}) pax` : ''}
              `}
            />

            {programs.basic.selected ?
              <Wyswyg
                programs={programs}
                status={this.state.activeReport.status}
                handleChange={this._handleWyswygChange}
                programName={str}
              />
              : null}
          </div>
        ) : programs.basic.selected ? <Wyswyg
          programs={programs}
          status={this.state.activeReport.status}
          handleChange={this._handleWyswygChange}
          programName={str}
        /> : null}
      </div>
    );
  }

  _handleWyswygChange(data) {
    const { programName, rawData } = data;
    const programs = this.state.programs;

    programs[programName].basic.newDescription = rawData;
  }

  _getUsers() {
    return this.state.users;
  }

  _recievedFiltedData(data) {
    let total_registered = 0;
    let total_dashboard_logins = 0;
    let total_respondents = 0;
    let total_submitted = 0;
    const filtered_data = {};
    data.forEach(item => {
      if (item.status !== 3) {
        total_registered++;

        if (item.status != null) {
          total_dashboard_logins++;

          if (item.healthsurvey_status !== null) {
            total_respondents++;

            if (item.healthsurvey_status === "Locked") {
              total_submitted++;
            }
          }
        }
      }
    });
    filtered_data.total_registered = total_registered;
    filtered_data.total_dashboard_logins = total_dashboard_logins;
    filtered_data.total_respondents = total_respondents;
    filtered_data.total_submitted = total_submitted;
    if (this._isMounted) {
      this.setState(
        {
          recievedDemographicsFiltedData: filtered_data
        },
        () => {
          this._processCompliance();
        }
      );
    }
  }

  _demographicsTop(fltr) {
    let dflt = { defName: "branch", defValue: "" };
    if (this.state.filters[fltr] && this.state.filters[fltr].filter !== "") {
      dflt = this.state.filters[fltr].filters;
    }

    return (
      <div className={wrapper}>
        {Object.keys(this.state.users).length > 0 ?
          <Demographics
            fromReport
            resizeLock
            graphSize={{ graphWidth: 700, graphHeight: 150 }}
            data={this._getUsers}
            activeFilter={this.state.filters[fltr]}
            filteredData={this._recievedFiltedData}
            activeCompany={this.state.activeReport}
            boxProps={this.state.filters.boxProps}
            setDivAlignment={this._setDivAlignment}
            resizeDivName='top_demogChart'
          />
          :
          <h2> Loading... </h2>
        }
      </div>
    );
  }

  _setDivAlignment(boxProps, name) {
    console.log('SET ALIGNMENT', boxProps, name);
    const { filters } = this.state;

    // eslint-disable-next-line no-prototype-builtins
    if (!filters.hasOwnProperty("boxProps")) {
      filters.boxProps = {};
    }

    filters.boxProps[name] = boxProps;
    if (this._isMounted) {
      this.setState({
        filters
      });
    }
  }

  _demographics(fltr, i) {
    fltr.toLowerCase();
    const nme = fltr.split("_");
    let dflt = { defName: "branch", defValue: "" };
    if (this.state.filters[fltr].filter !== "") {
      dflt = dflt = this.state.filters[fltr].filters;
    }

    return (
      <GridContainer key={`dem1${i}`} styles={{ margin: '0 auto', border: '1px solid #eee' }}>
        {this.chiHeader("Demographics", nme[0], nme[1])}

        {this.state.activeReport.status == null ? (
          <div className={wrapper}>
            <DefaultFilters
              withBackground
              fromReport
              defaultValue={dflt}
              filters={this._getUserFilters}
              filterData={this._getFilteredData}
              filterfor={fltr}
              label={"Filter Demographics by"}
            />
          </div>
        ) : null}

        {Object.keys(this.state.users).length > 0 ? (
          <Demographics
            fromReport
            graphSize={{ graphWidth: 700, graphHeight: 250 }}
            data={this._getUsers}
            activeFilter={this.state.filters[fltr]}
            activeCompany={this.state.activeReport}
            // filteredData={this._recievedFiltedData}
            boxProps={this.state.filters.boxProps}
            setDivAlignment={this._setDivAlignment}
            resizeDivName={`${nme[0]}_demogChart_${i}`}
          />
        ) : (
            <h2> Loading... </h2>
          )}

        {this.chiAnalysis(this.state.analysis[`${nme[0]}_${nme[1]}_analysis`], `${nme[0]}_${nme[1]}_analysis`)}
      </GridContainer>
    );
  }

  chiAnalysis = (value, id, isHalfCol=false) => {
    return (
      <section key={`d_dem3${id}`} className={addedChi_text}>
        <Label fullWidth subheader styles={{ fontStyle: 'italic', fontSize: 'smaller' }} text="Summary analysis and description" />
        {
          this.state.activeReport.status > 0 ? (
            <div className={analysisDiv}>
              {value || noAnalysis}
            </div>
          ) : (
              <TextareaAutosize
                className={insightsTextArea(isHalfCol)}
                placeholder="Write your Summary Analysis and Description here"
                disabled={this.state.activeReport.status > 0}
                name={id}
                id={id}
                defaultValue={value}
                onBlur={this._handleChange.bind(this)}
              />
            )
        }
      </section>
    )
  }

  chiHeader = (title, DOH, chi, is_chi = true, i) => {
    return (
      <GridContainer columnSize={'1fr .2fr'} styles={{ background: '#eee', padding: 5, border: '1px solid #eee' }}>
        <Label text={title} styles={{ fontWeight: 600, background: '#eee', paddingLeft: '20px', fontSize: 'smaller' }} />
        {this.state.activeReport.status == null ? (
          <Button styles={{ minWidth: '100px !important', width: 100, margin: '1px auto' }}>
            <button
              style={{ fontSize: 'smaller', fontWeight: 500 }}
              onClick={() => { is_chi ? this._removeCHI(DOH, chi) : this._removeEM(`${DOH}_${i}`) }}
            >
              REMOVE
                </button>
          </Button>
        ) : null}
      </GridContainer>
    )
  }

  _readiness(fltr, i) {
    const nme = fltr.split("_");

    const btnStyles = { minWidth: 'fit-content' }
    const childStyle = {
      textAlign: 'left',
      margin: 'auto 0'
    }

    return (
      <GridContainer key={`rea1${i}`} styles={{ border: '1px solid #eee' }} >
        {this.chiHeader("Readiness to Functional Medicine", nme[0], nme[1])}
        <GridContainer dataID="legend-filter-wrapper" columnSize=".4fr 1fr" styles={{ alignContent: 'start', padding: 15 }}>
          <GridContainer dataID="readiness-legends-wrapper" columnSize=".5fr 1fr" styles={{ fontSize: 'smaller' }}>
            <span>LEGEND:</span>

            <section style={{ textAlign: 'left' }}>
              <GridContainer childStyles={childStyle} columnSize="14px 1fr" gap={5}>
                {this._drawCircle("#80cde9")}<span>Ready</span>
              </GridContainer>
              <GridContainer childStyles={childStyle} columnSize="14px 1fr" gap={5}>
                {this._drawCircle("#eec5e2")}<span>Not Ready</span>
              </GridContainer>
            </section>
          </GridContainer>

          {this.state.activeReport.status == null ? (
            <div className={wrapper} style={{ textAlign: 'left' }}>
              <small>Filter By:</small>

              <GridContainer gap={10} columnSize="repeat(4, 1fr)">
                <Button styles={btnStyles} type={this.state.activeFilter.filter === "branch" ? 'pink' : 'default'}>
                  <button
                    onClick={() => this._setFilter("branch")}
                  >
                    Business Area
                </button>
                </Button>

                <Button styles={btnStyles} type={this.state.activeFilter.filter === "dept" ? 'pink' : 'default'}>
                  <button
                    onClick={() => this._setFilter("dept")}
                  >
                    Department
                </button>
                </Button>


                <Button styles={btnStyles} type={this.state.activeFilter.filter === "job_grade" ? 'pink' : 'default'}>
                  <button
                    onClick={() => this._setFilter("job_grade")}
                  >
                    Job Grade
                </button>
                </Button>

                <Button styles={btnStyles} type={this.state.activeFilter.filter === "high_risk" ? 'pink' : 'default'}>
                  <button
                    onClick={() => this._setFilter("high_risk")}
                  >
                    Status
                </button>
                </Button>
              </GridContainer>
            </div>
          ) : null}
        </GridContainer>

        <Readiness
          fromReport
          //  graphSize={{graphWidth: 700, graphHeight: 200}}
          defaultValue={{ defName: this.state.activeFilter.filter }}
          getReadinessGraphData={this.props.getReadinessGraphData}
          filter={this._setFilter}
          activeFilter={this.props.activeFilter}
          fromReport
        />

        {
          this.chiAnalysis(this.state.analysis[`${nme[0]}_${nme[1]}_analysis`], `${nme[0]}_${nme[1]}_analysis`)
        }
      </GridContainer>
    );
  }

  _handleCheckboxChange(e) {
    //e.preventDefault()
    const filters = this.state.filters;
    const id = document.getElementById(e.target.name);
    const names = e.target.name.split("_");

    const filterName = `${names[0]}_${names[1]}`;
    if (id.checked) {
      filters[filterName].selections[names[2]] = true;
    } else {
      filters[filterName].selections[names[2]] = false;
    }
    if (this._isMounted) {
      this.setState({
        filters
      });
    }
  }

  _healthScoreStatusDisplayOption(dflt, fltr) {
    const triggerOptions = () => {
      this.setState({
        showOptions: !this.state.showOptions
      })
    }

    const showOptions = this.state.showOptions ? this.state.showOptions : false

    return (
      <section className={optionsWrapper}>
        {/* className={triggerBtn} */}
        <Button type="darkpink" styles={{ borderRadius: 8, minWidth: 'fit-content', fontSize: 'small' }}>
          <button onClick={triggerOptions} >Select Table to Display</button>
        </Button>
        {
          showOptions &&
          <GridContainer classes={[opts]} styles={{ width: 340 }}>
            <div className={header}>Select Table to Display <button onClick={triggerOptions} className={closeBtn} /></div>
            <section style={{ padding: 15 }}>
              <GridContainer columnSize="20px 1fr" gap={10} styles={{ width: 'fit-content', marginRight: 10 }}>
                <div onClick={() => this._optionClickHandler(`${fltr}_score`)} className={!!this.state.filters[fltr].selections.score ? sqrSelectedOption : sqrDefaultOption}>
                  <input
                    style={{ opacity: 0, position: 'relative', zIndex: 0, width: 1, height: 1 }}
                    disabled={this.state.activeReport.status > 0}
                    type="checkbox"
                    id={`${fltr}_score`}
                    name={`${fltr}_score`}
                    value="score"
                    checked={!!this.state.filters[fltr].selections.score}
                    onChange={this._handleCheckboxChange.bind(this)}
                  />
                </div>
                <label htmlFor={`${fltr}_score`} style={{ color: !!this.state.filters[fltr].selections.score ? Colors.skyblue : '#ccc' }}>Distribution of Health Risk Scores</label>
              </GridContainer>

              <GridContainer columnSize="20px 1fr" gap={10} styles={{ width: 'fit-content', marginRight: 10 }}>
                <div onClick={() => this._optionClickHandler(`${fltr}_status`)} className={!!this.state.filters[fltr].selections.status ? sqrSelectedOption : sqrDefaultOption}>
                  <input
                    style={{ opacity: 0, position: 'relative', zIndex: 0, width: 1, height: 1 }}
                    disabled={this.state.activeReport.status > 0}
                    type="checkbox"
                    id={`${fltr}_status`}
                    name={`${fltr}_status`}
                    value="status"
                    checked={!!this.state.filters[fltr].selections.status}
                    onChange={this._handleCheckboxChange.bind(this)}
                  />
                </div>
                <label htmlFor={`${fltr}_status`} style={{ color: !!this.state.filters[fltr].selections.status ? Colors.skyblue : '#ccc' }}>Distribution of Health Status</label>
              </GridContainer>

              <GridContainer columnSize="20px 1fr" gap={10} styles={{ width: 'fit-content', marginRight: 10 }}>
                <div onClick={() => this._optionClickHandler(`${fltr}_determinants`)} className={!!this.state.filters[fltr].selections.determinants ? sqrSelectedOption : sqrDefaultOption}>
                  <input
                    style={{ opacity: 0, position: 'relative', zIndex: 0, width: 1, height: 1 }}
                    disabled={this.state.activeReport.status > 0}
                    type="checkbox"
                    id={`${fltr}_determinants`}
                    name={`${fltr}_determinants`}
                    value="determinants"
                    checked={!!this.state.filters[fltr].selections.determinants}
                    onChange={this._handleCheckboxChange.bind(this)}
                  />
                </div>
                <label htmlFor={`${fltr}_determinants`} style={{ color: !!this.state.filters[fltr].selections.determinants ? Colors.skyblue : '#ccc' }}>Status of Determinants of Health</label>
              </GridContainer>
            </section>
          </GridContainer>
        }
      </section>
    )

    // return (      
    //   <GridContainer dataID="legends-filter-wrapper" columnSize="1fr 1fr .3fr">
    //     {this._legends()}

    //     <GridContainer dataID="filter-wrapper">
    //       <span className={smallTitle}>Select Table to Display</span>

    //       <GridContainer columnSize="20px 1fr" gap={10} styles={{ width: 'fit-content', marginRight: 10 }}>
    //         <div onClick={() => this._optionClickHandler(`${fltr}_score`)} className={!!this.state.filters[fltr].selections.score ? sqrSelectedOption : sqrDefaultOption}>
    //           <input
    //             style={{ opacity: 0, position: 'relative', zIndex: 0, width: 1, height: 1 }}
    //             disabled={this.state.activeReport.status > 0}
    //             type="checkbox"
    //             id={`${fltr}_score`}
    //             name={`${fltr}_score`}
    //             value="score"
    //             checked={!!this.state.filters[fltr].selections.score}
    //             onChange={this._handleCheckboxChange.bind(this)}
    //           />
    //         </div>
    //         <label htmlFor={`${fltr}_score`} style={{ color: !!this.state.filters[fltr].selections.score ? Colors.skyblue : '#ccc' }}>Distribution of Health Risk Scores</label>
    //       </GridContainer>

    //       <GridContainer columnSize="20px 1fr" gap={10} styles={{ width: 'fit-content', marginRight: 10 }}>
    //         <div onClick={() => this._optionClickHandler(`${fltr}_score`)} className={!!this.state.filters[fltr].selections.status ? sqrSelectedOption : sqrDefaultOption}>
    //           <input
    //             style={{ opacity: 0, position: 'relative', zIndex: 0, width: 1, height: 1 }}
    //             disabled={this.state.activeReport.status > 0}
    //             type="checkbox"
    //             id={`${fltr}_status`}
    //             name={`${fltr}_status`}
    //             value="status"
    //             checked={!!this.state.filters[fltr].selections.status}
    //             onChange={this._handleCheckboxChange.bind(this)}
    //           />
    //         </div>
    //         <label htmlFor={`${fltr}_status`} style={{ color: !!this.state.filters[fltr].selections.status ? Colors.skyblue : '#ccc' }}>Distribution of Health Status</label>
    //       </GridContainer>

    //       <GridContainer columnSize="20px 1fr" gap={10} styles={{ width: 'fit-content', marginRight: 10 }}>
    //         <div onClick={() => this._optionClickHandler(`${fltr}_score`)} className={!!this.state.filters[fltr].selections.determinants ? sqrSelectedOption : sqrDefaultOption}>
    //           <input
    //             style={{ opacity: 0, position: 'relative', zIndex: 0, width: 1, height: 1 }}
    //             disabled={this.state.activeReport.status > 0}
    //             type="checkbox"
    //             id={`${fltr}_determinants`}
    //             name={`${fltr}_determinants`}
    //             value="determinants"
    //             checked={!!this.state.filters[fltr].selections.determinants}
    //             onChange={this._handleCheckboxChange.bind(this)}
    //           />
    //         </div>
    //         <label htmlFor={`${fltr}_determinants`} style={{ color: !!this.state.filters[fltr].selections.determinants ? Colors.skyblue : '#ccc' }}>Status of Determinants of Health</label>
    //       </GridContainer>
    //     </GridContainer>

    // );
  }

  _healthScoreStatus(fltr, i) {
    const nme = fltr.split("_");

    let dflt = { defName: "branch", defValue: "" };
    if (this.state.filters[fltr].filter !== "") {
      dflt = this.state.filters[fltr].filters;
    }

    return (
      <GridContainer styles={{ border: '1px solid #eee' }}>
        {this.chiHeader("Health Risk Score and Status", nme[0], nme[1])}

        {this.state.activeReport.status == null &&
          <GridContainer columnSize={'1fr 1fr .3fr'}>
            {this._legends()}
            {this._healthScoreStatusDisplayOption(dflt, fltr)}
            <DefaultFilters
              withBackground
              fromReport
              defaultValue={dflt}
              filters={this._getUserFilters}
              filterData={this._getFilteredData}
              filterfor={fltr}
              label={"Filter Health Risk Score Status by"}
            />
          </GridContainer>
        }
        <HealthScoreStatus
          fromReport
          getScoresByCompany={this.props.getScoresByCompany}
          healthDeterminants={this.props.healthDeterminants}
          activeFilter={this.state.filters[fltr]}
          boxProps={this.state.filters.boxProps}
          setDivAlignment={this._setDivAlignment}
        />

        {
          this.chiAnalysis(this.state.analysis[`${nme[0]}_${nme[1]}_analysis`], `${nme[0]}_${nme[1]}_analysis`)
        }
      </GridContainer>
    );
  }

  _getDownstreamHealthIssue = () => {
    const downstream = [];
    const downstreamData = this.props.getDownstreamGraphData();
    downstreamData.then(res => {
      const result = { ...res };
      downstream.push(result);
      if (!this.state.downstream_issue) {
        if (this._isMounted) {
          this.setState({
            downstream_issue: res
          });
        }
      }
    });
  };

  _downstream(fltr, i) {
    // ___downstream_download_edit___
    this._getDownstreamHealthIssue();

    // get report name and company name from this.state.activeReport
    const { report_name, company_name } = this.state.activeReport;

    const nme = fltr.split("_");
    let dflt = { defName: "branch", defValue: "" };
    if (this.state.filters[fltr].filter !== "") {
      dflt = {
        defName: this.state.filters[fltr].filter,
        defValue: this.state.filters[fltr].value
      };
    }
    return (
      <GridContainer key={`ds1${i}`} styles={{ border: '1px solid #eee' }}>
        {this.chiHeader("DOWNSTREAM MANIFESTATIONS", nme[0], nme[1])}
        {this.state.activeReport.status == null ? (
          <div className={wrapper}>
            <DefaultFilters
              withBackground
              fromReport
              defaultValue={dflt}
              filters={this._getUserFilters}
              filterData={this._getFilteredData}
              filterfor={fltr}
              label={"Filter Downstream by"}
            />
          </div>
        ) : null}

        <Downstream
          fromReport
          getDownstreamGraphData={this.props.getDownstreamGraphData}
          activeFilter={this.state.filters[fltr]}
        />

        {this.state.activeReport.status == null && !this.state.loading && this.state.downstream_issue ? (
          <Preview
            company_name={company_name}
            report_name={report_name}
            issues={this.state.downstream_issue}
            data={this.state.mainHealhGoals}
          />
        ) : null}

        {
          this.chiAnalysis(this.state.analysis[`${nme[0]}_${nme[1]}_analysis`], `${nme[0]}_${nme[1]}_analysis`)
        }
      </GridContainer>
    );
  }

  _msq(fltr, i) {
    const nme = fltr.split("_");
    let dflt = { defName: "branch", defValue: "" };
    if (this.state.filters[fltr].filter !== "") {
      dflt = this.state.filters[fltr].filters;
    }
    return (
      <GridContainer styles={{ border: '1px solid #eee' }}>
          {this.chiHeader("Symptoms Review", nme[0], nme[1])}
          {this.state.activeReport.status == null ? (
            <DefaultFilters
              withBackground
              fromReport
              defaultValue={dflt}
              filters={this._getUserFilters}
              filterData={this._getFilteredData}
              filterfor={fltr}
              label={"Filter Symptoms Review by"}
            />
          ) : null}

          <MSQ
            fromReport
            data={this.props.msq_data}
            msqScoresPerSystem={this.props.msqScoresPerSystem}
            activeFilter={this.state.filters[fltr]}
            showButton={this.state.activeReport.status == null}
          />

          {
            this.chiAnalysis(this.state.analysis[`${nme[0]}_${nme[1]}_analysis`], `${nme[0]}_${nme[1]}_analysis`)
          }
      </GridContainer>
    );
  }

  _optionClickHandler = (id) => {
    const child = document.querySelector(`#${id}`);
    child.click();
  }

  _upstreamTableToDisplayOptions(fltr) {
 
    
    const options = {
      weight: "Weight",
      stress: "Stress",
      sleep: "Sleep",
      detox: "Detox",
      movement: "Movement",
      foodchoices: "Food Choices",
      msqTopAffectedSystems: "Top Affected Systems",
      prevalentSymptoms: "Prevalent Symptoms"
    };

    return Object.keys(options).map((item, k) => (
      <GridContainer key={k} columnSize="20px 1fr" gap={10} styles={{ width: 'fit-content', marginRight: 10 }}>
        <div onClick={() => this._optionClickHandler(`${fltr}_${item}`)} className={this.state.filters[fltr].selections[item] ? sqrSelectedOption : sqrDefaultOption}>
          <input
            style={{ opacity: 0, position: 'relative', zIndex: 0, width: 1, height: 1 }}
            disabled={this.state.activeReport.status > 0}
            type="checkbox"
            id={`${fltr}_${item}`}
            name={`${fltr}_${item}`}
            value={item}
            checked={this.state.filters[fltr].selections[item]}
            onChange={this._handleCheckboxChange.bind(this)}
          />
        </div>
        <label style={{ color: this.state.filters[fltr].selections[item] ? Colors.skyblue : '#ccc' }}>{options[item]}</label>
      </GridContainer>
    ));
  }

  _upstreamOptions = fltr => {
    const triggerOptions = () => {
      this.setState({
        showOptions: !this.state.showOptions
      })
    }

    const showOptions = this.state.showOptions ? this.state.showOptions : false

    return (
      <section className={optionsWrapper}>
        {/* className={triggerBtn} */}
        <Button type="darkpink" styles={{ borderRadius: 8, minWidth: 'fit-content', fontSize: 'small' }}>
          <button onClick={triggerOptions} >Select Table to Display</button>
        </Button>
        {
          showOptions &&
          <GridContainer classes={[opts]}>
            <div className={header}>Select Table to Display <button onClick={triggerOptions} className={closeBtn} /></div>
            <section style={{ padding: 15 }}>
              {this._upstreamTableToDisplayOptions(fltr)}
            </section>
          </GridContainer>
        }
      </section>
    )
  }

  _upstream(fltr, i) {
    const nme = fltr.split("_");
    let dflt = { defName: "branch", defValue: "" };
    if (this.state.filters[fltr].filter !== "") {
      dflt = this.state.filters[fltr].filters;
    }

    return (
      <GridContainer key={`upst${i}`} styles={{ margin: '0 auto', border: '1px solid #eee'}}>
        {this.chiHeader("Upstream Manifestations", nme[0], nme[1])}
        {this.state.activeReport.status == null ? (
          <GridContainer columnSize="1fr 1fr" >
            {this._upstreamOptions(fltr)}

            <DefaultFilters
              withBackground
              fromReport
              defaultValue={dflt}
              filters={this._getUserFilters}
              filterData={this._getFilteredData}
              filterfor={fltr}
              label={"Filter Upstream by"}
            />
          </GridContainer>
        ) : null
        }
        <Upstream
          fromReport
          data={this.props.upstream_data}
          msqSystemRank={this.props.msqSystemRank}
          stressSourceRank={this.props.stressSourceRank}
          prevalentSymptoms={this.props.prevalentSymptoms}
          foodchoices={this.props.foodchoices}
          activeFilter={this.state.filters[fltr]}
        />
        {
          this.chiAnalysis(this.state.analysis[`${nme[0]}_${nme[1]}_analysis`], `${nme[0]}_${nme[1]}_analysis`)
        }
      </GridContainer>
    );
  }

  _showChiOptions = (chiname) => {
    const addChiForm = { ...this.state.addChiForm };
    addChiForm[chiname].hide = false;

    if (this._isMounted) {
      this.setState({
        addChiForm
      });
    }
  }

  _removeCHI(name, chi) {
    const chiGraph = this.state.chiGraph;
    const index = chiGraph[name].indexOf(chi);
    if (index >= 0) {
      chiGraph[name].splice(index, 1);
    }
    if (this._isMounted) {
      this.setState({
        chiGraph
      });
    }
  }

  _displayChi(name, chi, i) {
    let html;
    if (chi === "demographics") {
      html = this._demographics(`${name}_${chi}`, i);
    } else if (chi === "upstream") {
      html = this._upstream(`${name}_${chi}`, i);
    } else if (chi === "readiness") {
      html = this._readiness(`${name}_${chi}`, i);
    } else if (chi === "healthScoreStatus") {
      html = this._healthScoreStatus(`${name}_${chi}`, i);
    } else if (chi === "downstream") {
      html = this._downstream(`${name}_${chi}`, i);
    } else if (chi === "msq") {
      html = this._msq(`${name}_${chi}`, i);
    }
    return <div id="withRef" key={`${name}_${chi}_${i}`} className={[avoidPageBreakInside]} ref={this.scrollToCHI}>{html}</div>;
  }

  _additionalChi(chiName) {
    const chilist = this.state.chiGraph;
    return chilist[chiName].map((item, i) => this._displayChi(chiName, item, i));
  }

  _deleteReport = (id) => {
    this.setState({ showReportList: false })
    Report.deleteReport({ id })
      .then(() => {
        this._getReportFromDatabase();
      })
      .catch(e => {
        console.log(e);
      });
  }

  _spacer(px) {
    return <div id="spacer" style={{ width: "100%", paddingBottom: px }}>&nbsp;</div>;
  }

  _legends = () => {
    const childStyle = {
      textAlign: 'left',
      margin: 'auto 0'
    }

    return (
      <GridContainer columnSize=".5fr repeat(2, 1fr) 1.5fr" gap={'0 10px'} styles={{ width: '80%', fontSize: 'smaller', margin: '10px auto auto 30px' }}>
        <div>LEGEND:</div>
        <GridContainer childStyles={childStyle} columnSize="14px 1fr" gap={5}>
          {this._drawCircle("#72a153")}<span>Optimal</span>
        </GridContainer>
        <GridContainer childStyles={childStyle} columnSize="14px 1fr" gap={5} styles={{ gridColumn: 2 }}>
          {this._drawCircle("#71b2ca")}<span>Suboptimal</span>
        </GridContainer>
        <GridContainer childStyles={childStyle} columnSize="14px 1fr" gap={5} styles={{ gridColumn: 2 }}>
          {this._drawCircle("#dfaf29")}<span>Neutral</span>
        </GridContainer>
        <GridContainer childStyles={childStyle} columnSize="14px 1fr" gap={5} styles={{ gridColumn: 3, gridRow: 1 }}>
          {this._drawCircle("#d77f1a")}<span>Compromised</span>
        </GridContainer>
        <GridContainer childStyles={childStyle} columnSize="14px 1fr" gap={5} styles={{ gridColumn: 3, gridRow: 2 }}>
          {this._drawCircle("red")}<span>Alarming</span>
        </GridContainer>
      </GridContainer>
    )
  }

  _sixDeterminantsCompanyStatus() {
    const childStyle = {
      textAlign: 'left',
      margin: 'auto 0'
    }

    const legend = (
      <GridContainer columnSize=".5fr repeat(2, 1fr) 1.5fr" styles={{ width: 'fit-content', fontSize: 'smaller', margin: '10px auto auto 30px' }}>
        <div>LEGEND:</div>
        <GridContainer childStyles={childStyle} columnSize="14px 1fr" gap={5}>
          {this._drawCircle("#72a153")}<span>Optimal</span>
        </GridContainer>
        <GridContainer childStyles={childStyle} columnSize="14px 1fr" gap={5} styles={{ gridColumn: 2 }}>
          {this._drawCircle("#71b2ca")}<span>Suboptimal</span>
        </GridContainer>
        <GridContainer childStyles={childStyle} columnSize="14px 1fr" gap={5} styles={{ gridColumn: 2 }}>
          {this._drawCircle("#dfaf29")}<span>Neutral</span>
        </GridContainer>
        <GridContainer childStyles={childStyle} columnSize="14px 1fr" gap={5} styles={{ gridColumn: 3, gridRow: 1 }}>
          {this._drawCircle("#d77f1a")}<span>Compromised</span>
        </GridContainer>
        <GridContainer childStyles={childStyle} columnSize="14px 1fr" gap={5} styles={{ gridColumn: 3, gridRow: 2 }}>
          {this._drawCircle("red")}<span>Alarming</span>
        </GridContainer>
        <span style={{ gridColumn: 4, gridRow: 1, textAlign: 'left' }}><label style={{ fontWeight: 500 }}>N</label> = Number of Respondents</span>
        <span style={{ gridColumn: 4, gridRow: 2, textAlign: 'left' }}><label style={{ fontWeight: 500 }}>HV</label> = High Value</span>
        <span style={{ gridColumn: 4, gridRow: 3, textAlign: 'left' }}><label style={{ fontWeight: 500 }}>R</label> = Regular</span>
      </GridContainer>
    )

    return (
      <div className={dohContainer}>
        <section className={pageBreakBefore}>
          <Label text="III. Status of the Company in the Six Determinants of Health" fullWidth />
          {legend}
        </section>
        {this._displayDeterminantsGroup()}
      </div>
    );
  }

  _finalAnalysis() {
    return (
      <div className={pageBreakBefore}>
        <div className={title}>Final Analysis</div>
        <div className={descWrapper}>
          {this.state.activeReport.status > 0 ? (
            <div className={analysisDiv}>{this.state.analysis.overall}</div>
          ) : (
              <>
                <Label fullWidth subheader text="Summary analysis and description" />
                <TextareaAutosize
                  className={insightsTextArea()}
                  placeholder="Summary Analysis and Description"
                  disabled={this.state.activeReport.status > 0}
                  name={"overall"}
                  id={`overall`}
                  defaultValue={this.state.analysis.overall}
                  onBlur={this._handleChange.bind(this)}
                />
              </>
            )}
        </div>
      </div>
    );
  }

  _sixDeterminantsFinal(programName) {
    let flexDirection = 'initial';
    if (this.state.filters.boxProps && this.state.filters.boxProps[`${programName}_pie`]) {
      flexDirection = this.state.filters.boxProps[`${programName}_pie`].alignment;
    }

    const piedata = this._pieValues(programName);
    const program = this.state.programs[programName];
    program.totalHV =
      parseInt(piedata[0].compromisedHVCtr) +
      parseInt(piedata[0].alarmingHVCtr);
    program.totalRegular =
      parseInt(piedata[0].noneCompromisedHRCtr) +
      parseInt(piedata[0].nonealarmingHRCtr);

    let dflt = { defName: "branch", defValue: "" };
    if (this.state.filters[programName] && this.state.filters[programName].filter !== "") {
      dflt = this.state.filters[programName].filters;
    }

    const setProgram = (name, bg = false) => {
      switch (name) {
        case 'weight':
          if (bg) {
            return Colors.weightColor
          }
          return 'A. Weight'

        case 'nutrition':
          if (bg) {
            return Colors.nutritionColor
          }
          return 'B. Nutrition'

        case 'detox':
          if (bg) {
            return Colors.detoxColor
          }
          return 'C. Detox'

        case 'sleep':
          if (bg) {
            return Colors.sleepColor
          }
          return 'D. Sleep'

        case 'movement':
          if (bg) {
            return Colors.movementColor
          }
          return 'E. Movement'

        case 'stress':
          if (bg) {
            return Colors.stressColor
          }
          return 'F. Stress'

        default:
          return 'Title Here'
      }
    }

    const isRow = this.state.chiRow[programName];

    if (isRow) {
      return (
        <div id={`${programName}-wrapper`}>
          <div className={avoidPageBreakInside}>
            <div style={{ background: setProgram(programName, true), width: '100%' }} >
              <Label text={setProgram(programName)} fullWidth styles={{ paddingLeft: 10, color: 'white' }} />
            </div>
            <section style={{ margin: '0 auto 10px' }}>
              {this._pieChart(piedata, programName)}
            </section>
          </div>

          <div style={{ width: '100%' }}>
            <section id={`${programName}-analysis-wrapper`} style={{ textAlign: 'justify' }}>
              <Label fullWidth subheader text="Analysis and recommendations" />
              <div className={analysisDiv}>{this.state.analysis[programName] || noAnalysis}</div>
            </section>
          </div>
        </div>
      );
    }

    return (
      <div id={`${programName}-wrapper`} className={avoidPageBreakInside}>
        <div style={{ background: setProgram(programName, true), width: '100%' }} >
          <Label text={setProgram(programName)} fullWidth styles={{ paddingLeft: 10, color: 'white' }} />
        </div>

        <div className={earlyMarkersDivStyle} style={{ flexDirection }}>

          <section style={{ margin: 0 }}>
            {this._pieChart(piedata, programName)}
          </section>

          <div style={{ flex: '1 0 40%', textAlign: "justify", padding: '0 20px' }}>
            <section id={`${programName}-analysis-wrapper`} style={{ textAlign: 'justify' }}>
              <Label fullWidth subheader text="Analysis and recommendations" styles={{ margin: '0 auto 0 10px' }} />
              <div className={analysisDiv}>{this.state.analysis[programName] || noAnalysis}</div>
            </section>
          </div>
        </div>
      </div>
    );
  }

  _displayDeterminantsGroup() {
    const doh = {
      weight: "",
      nutrition: "",
      detox: "",
      sleep: "",
      movement: "",
      stress: ""
    };

    // eslint-disable-next-line no-shadow
    return Object.keys(doh).map((doh, i) => (
      <section
        key={`a${i}`}
        id="determinants-group-wrapper"
        style={{ width: '100%', padding: '30px 30px 0' }}
      >
        {this.state.meanScore.weight != null &&
          <React.Fragment>
          {!this.state.activeReport.status ? this._sixDeterminants(doh) : this._sixDeterminantsFinal(doh)}

            {
              this.state.chiGraph && Object.keys(this.state.chiGraph).length > 0 ? this._additionalChi(doh) : null
            }

            {
              this.state.earlyMarkers[doh] && this.state.earlyMarkers[doh].length > 0 ? this._earlyMarkers(doh) : null
            }

            {!this.state.activeReport.status &&
              <section style={{
                width: '100%',
                display: "flex",
                flex: "1 0 100%",
                flexWrap: "wrap",
                marginTop: 20
              }}>
                <Dropdown
                  doh={doh}
                  options={this.state.inputs[doh]}
                  onChange={this._handleFormChange}
                  onSubmit={this._goToNext}
                  activeCHIforms={this.state.chiGraph}
                />
                {this._addEarlymarkers(doh)}
              </section>
            }

            {this._recommendations(doh)}

          </React.Fragment>
        }
      </section>
    ));
  }

  _scoreAndPercentFromDB = () => {
    let { activeReport } = this.state;
    const data = { average: activeReport.score, percentile: activeReport.percentile_rank }
    return data;
  }

  _renderView = () => {
    const { subscriptions, activeReport } = this.state;
    let dflt = { defName: "branch", defValue: "" };
    if (this.state.filters.goals && this.state.filters.goals.filter !== "") {
      dflt = this.state.filters.goals.filters;
    }

    return (
      <div id="printable-container" className={reportDiv} ref={el => (this.printable = el)}>
        <ReactTooltip className="tooltipStyle" />
        {
          this.state.isPrint &&
          <PageOne companyName={this.state.companyInfo.company_name} report={this.state.activeReport} />
        }

        <PageWrapper>
          <CompanyInfo
            logo={this.state.companyInfo.logo}
            activeReport={this.state.activeReport}
          />

          <section style={{ padding: '30px 30px 0' }}>
            <GridContainer columnSize="1fr 1fr" fullWidth>
              <Label text="I. Demographics " />

              {this.state.activeReport.status == null ?
                <DefaultFilters
                  withBackground
                  fromReport
                  defaultValue={dflt}
                  filters={this._getUserFilters}
                  filterData={this._getFilteredData}
                  filterfor={"demographicsTop"}
                  label={"Filter Demographics by"}
                />
                : null}
            </GridContainer>

            {
              this.state.barGrapdata.length > 0 ?
                this._healthGoals() : <div className={wrapper}>No Data for Graph.</div>
            }

            {this._demographicsTop("demographicsTop")}

          </section>
        </PageWrapper>

        <PageWrapper>
          {this._healthGoalsChart(dflt)}
        </PageWrapper>

        {this._sixDeterminantsCompanyStatus()}

        <PageWrapper>
          {this._finalAnalysis()}
        </PageWrapper>
      </div>
    );
  }


  _back() {
    this.props.back();
  }

  _reportSwitch() {
    this.props.reportSwitch("wr");
  }

  // _getReadinessGraphData() {
  //   return this.props
  //     .readinessData(this.state.activeFilter)
  //     .then(res => Promise.resolve(res))
  //     .catch(e => {
  //       console.log(e);
  //     });
  // }

  _setFilter(filter, value, init) {
    this.props.filter(filter, value, init);

  }

  _formSubmit = (final, newreport = false) => {
    const { defaultPrograms } = this.state;
    const programs = this.state.programs;
    const reportInfo = this.state.reportInfo;
    const inputs = this.state.inputs;
    const analysis = this.state.analysis;
    const earlyMarkers = this.state.earlyMarkers;
    const filters = this.state.filters;
    const chiGraph = this.state.chiGraph;

    if (newreport) {
      programs.weight.premium.selected = false;
      programs.weight.basic.selected = false;
      programs.weight.basic.description = defaultPrograms.weight.basic.description;
      programs.weight.basic.newDescription = {};
      programs.weight.selected = false;

      programs.nutrition.premium.selected = false;
      programs.nutrition.basic.selected = false;
      programs.nutrition.basic.description = defaultPrograms.nutrition.basic.description;
      programs.nutrition.basic.newDescription = {};
      programs.nutrition.selected = false;

      programs.sleep.premium.selected = false;
      programs.sleep.basic.selected = false;
      programs.sleep.basic.description = defaultPrograms.sleep.basic.description;
      programs.sleep.basic.newDescription = {};
      programs.sleep.selected = false;

      programs.detox.premium.selected = false;
      programs.detox.basic.selected = false;
      programs.detox.basic.description = defaultPrograms.detox.basic.description;
      programs.detox.basic.newDescription = {};
      programs.detox.selected = false;

      programs.movement.premium.selected = false;
      programs.movement.basic.selected = false;
      programs.movement.basic.description = defaultPrograms.movement.basic.description;
      programs.movement.basic.newDescription = {};
      programs.movement.selected = false;

      programs.stress.premium.selected = false;
      programs.stress.basic.selected = false;
      programs.stress.basic.description = defaultPrograms.stress.basic.description;
      programs.stress.basic.newDescription = {};
      programs.stress.selected = false;
    }

    //  reportInfo.insights = this.state.reportInfo.insights
    reportInfo.md_incharge = this.props.user.name;
    reportInfo.md_id = this.props.user.id;
    reportInfo.programs = programs;
    reportInfo.details.filters = filters;
    reportInfo.details.chiGraph = chiGraph;
    reportInfo.details.activeFilter = this.state.activeFilter;
    reportInfo.details.analysis = analysis;
    reportInfo.details.earlyMarkers = earlyMarkers;

    reportInfo.mean_scores = JSON.stringify(this.state.meanScore);
    reportInfo.scoreandpercentile = JSON.stringify(
      this.state.scoreAndPercentile
    );
    reportInfo.compliance_rate = this._complialancePercentage();
    reportInfo.registered_employee = this.state.scoreAndPercentile.scores
      ? this.state.scoreAndPercentile.scores.length
      : 0;
    reportInfo.score = this.state.scoreAndPercentile.average;
    reportInfo.percentile_rank = this.state.scoreAndPercentile.percentile;
    reportInfo.health_status = GetStatus(this.state.scoreAndPercentile.average);

    reportInfo.status = null;

    if (final) {
      reportInfo.status = 1;
      // reportInfo.mean_scores = JSON.stringify(this.state.meanScore)
      // reportInfo.scoreandpercentile = JSON.stringify(this.state.scoreAndPercentile)
      // reportInfo.compliance_rate = this._complialancePercentage()
      // reportInfo.registered_employee = this.state.scoreAndPercentile.scores.length
      // reportInfo.score = this.state.scoreAndPercentile.average
      // reportInfo.percentile_rank = this.state.scoreAndPercentile.percentile
      // reportInfo.health_status = GetStatus(this.state.scoreAndPercentile.average)
    }
    if (this._isMounted) {
      this.setState({
        programs,
        reportInfo
      });
    }

    if (newreport) {
      if (inputs.report_name.value !== undefined) {
        reportInfo.report_name = inputs.report_name.value;
        reportInfo.company_subscription_id = parseInt(
          inputs.subscriptions.value
        );
        reportInfo.details.filters = this.state.defaultFltr;
        reportInfo.details.earlyMarkers = {
          weight: [],
          nutrition: [],
          detox: [],
          sleep: [],
          movement: [],
          stress: []
        };
        reportInfo.details.chiGraph = {
          weight: [],
          nutrition: [],
          detox: [],
          upstream: [],
          movement: [],
          stress: [],
          sleep: [],
          downstream: []
        };
        reportInfo.details.analysis = {};

        if (this._isMounted) {
          this.setState(
            {
              reportInfo,
              inputs
            },
            () => this._toReport(final, newreport)
          );
        }
      }
    } else {
      this._toReport(final);
    }
  }

  _toReport(final, newreport) {
    const reportInfo = this.state.reportInfo;
    const ndate = moment();

    if (newreport) {
      reportInfo.date_created = ndate;
      reportInfo.date_modified = null;
      if (this._isMounted) {
        this.setState(
          {
            reportInfo,
            showReportList: false
          },
          () => this._saveReport(newreport)
        );
      }
    } else {
      let criteria;
      if (this.state.reportInfo.id) {
        criteria = { field: "r.id", value: this.state.reportInfo.id };
      } else {
        criteria = {
          field: "company_id",
          value: this.state.reportInfo.company_id
        };
      }
      this._getReport(criteria)
        .then(res => {
          if (res.length > 0) {
            delete reportInfo.date_created;
            delete reportInfo.subscription_name;
            delete reportInfo.active;
            reportInfo.id = res[0].id;
            reportInfo.date_modified = ndate;
            if (this._isMounted) {
              this.setState(
                {
                  reportInfo
                },
                () => this._updateReport()
              );
            }
          } else {
            reportInfo.date_created = ndate;
            if (this._isMounted) {
              this.setState(
                {
                  reportInfo
                },
                () => this._saveReport(newreport)
              );
            }
          }
          if (final) {
            const btn = this.state.buttons;
            btn.disabled = true;
            if (this._isMounted) {
              this.setState({
                buttons: btn
              });
            }
          }
        })
        .catch(e => {
          console.log(e);
          if (this._isMounted) {
            this.setState({
              errorMessage: "Error on _toReport method"
            });
          }
        });
    }
  }

  _saveReport(newreport = false) {
    if (this.state.inputs.report_name.value === "" && newreport) {
      alert("Oppps!  Your trying to save with No Report name");
      return;
    }

    // let message;
    const createReportForm = this.state.createReportForm;
    const inputs = { ...this.state.inputs };

    createReportForm.hide = true;
    inputs.report_name.value = "";
    // if (this.state.final) {
    //   message = "Creating Report file..";
    // } else {
    //   message = "Report saved";
    // }
    console.log('SAVE REPORT DATA', this.state.reportInfo)
    Report.saveReport(this.state.reportInfo)
      .then(() => {
        if (this._isMounted) {
          this.setState(
            {
              // responseMessage: message,
              createReportForm,
              inputs,
              showReportList: true
            },
            function () {
              setTimeout(() => {
                this._clear();
              }, 5000);
            }
          );
        }

        this._getReportFromDatabase();

        //this._updateReportList()
      })
      .catch(() => {
        if (this._isMounted) {
          this.setState({
            errorMessage: "Error Saving Report"
          });
        }
      });
  }

  _getReport(criteria) {
    return Report.getReport(criteria)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _updateReport() {
    let message;
    if (this.state.final) {
      message = "Creating Report file..";
    } else {
      message = "Report Updated";
    }
    console.log('UPDATE REPORT DATA ', this.state.reportInfo)
    Report.updateReport(this.state.activeReport)
      .then((r) => {
        if (this._isMounted) {
          this.setState({
            responseMessage: message
          });
        }
        this._getReportFromDatabase();
      })
      .catch(() => {
        if (this._isMounted) {
          this.setState({
            errorMessage: "Error on updating Report"
          });
        }
      });

    setTimeout(() => {
      this._clear();
    }, 3000);


    //return;
  }

  render() {
    
    if (!this.isChrome) {
      return <h2>Sorry! this page requires Google Chrome Browser.</h2>
    }
    
    const that = this;
    window.onbeforeprint = function () {
      if (that.state.activeReport && that.state.activeReport.status) {
        return true;
      }
      alert("This Report is not yet finalized, Printing is not advisable.");
    };
    return (
      <div className={wrapper}>
        {this.state.activeReport ? this._displayReport() : null}
        <GridContainer gap={20} fullWidth>
          <GridContainer columnSize="1fr 1fr" fullWidth>
            <Button>
              <button onClick={() => this._back()}>Back</button>
            </Button>

            <Button type="blue" right>
              <button onClick={() => this._createReport()}>Create</button>
            </Button>
          </GridContainer>

          {!this.state.showReportList ? <Preparing /> : this._reportList()}

        </GridContainer>


        <div className={menuMessageDiv}>
          {this.state.errorMessage ? (
            <div className={[wrapper, errorMessage].join(" ")}>
              {this.state.errorMessage}
            </div>
          ) : null}
          {this.state.responseMessage ? (
            <div className={[wrapper, goodMessage].join(" ")}>
              {this.state.responseMessage}
            </div>
          ) : null}
        </div>

      </div>
    );
  } // render end
} // class end

// styling

const notBlurred = css({
  filter: 'blur(0px) !important',
})

const uploadError = css({
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: '100%',
  padding: 20,
  background: '#a13232',
  lineHeight: '20px',
  fontSize: 16,
  height: '100%',
  display: 'grid',
  placeContent: 'center',
  color: 'white',
  zIndex: 0,
})

const optionsWrapper = css({
  position: 'relative',
  padding: 10,
  fontSize: 'smaller'
})

const opts = css({
  background: 'white',
  boxShadow: '0 5px 10px rgba(0,0,0,.1)',
  position: 'absolute',
  top: 10,
  left: 10,
  width: 300,
  borderRadius: 8,
  zIndex: 1001
})

const closeBtn = css({
  width: 15,
  height: 15,
  position: 'absolute',
  borderStyle: 'none',
  background: 'none',
  top: '50%',
  right: 10,
  transform: 'translateY(-50%) rotate(45deg)',
  opacity: .5,
  transition: '200ms ease',
  cursor: 'pointer',
  '::before': {
    content: '""',
    position: 'absolute',
    top: 0,
    left: '50%',
    width: 3,
    height: '100%',
    background: '#fff',
    transform: 'translateX(-50%)'
  },
  '::after': {
    content: '""',
    position: 'absolute',
    top: 0,
    left: '50%',
    width: 3,
    height: '100%',
    background: '#fff',
    transform: 'translateX(-50%) rotate(90deg)'
  },
  ':hover': {
    opacity: 1
  },
  ':focus': {
    outline: 'none'
  }
})

const header = css({
  background: Colors.pink,
  color: '#fff',
  position: 'relative',
  width: '100%',
  padding: '5px 10px',
  textAlign: 'left !important',
  borderRadius: '5px 5px 0 0'
})

const printingOverlay = css({
  position: 'absolute',
  top: 0,
  left: 0,
  width: '100%',
  height: '100%',
  background: 'rgba(255,255,255,.9)',
  display: 'grid',
  placeContent: 'center',
  color: Colors.skyblue,
  zIndex: 1009,
  fontWeight: 700
});

css.global('.react-confirm-alert', {
  width: 'fit-content !important'
})

const inputClassName = css(inputClass, {
  minWidth: 300
});

let addedChi_text = css({
  display: "block",
  width: "100%",
  justifyContent: "center",
  alignItems: "center",
  marginTop: 10,
  padding: 10,
  "@media print": {
    pageBreakInside: "avoid",
    pageBreakAfter: "auto"
  }
});

let reportDiv = css({
  position: "relative",
  margin: "0 0px 10px 0",
  padding: 10,
  maxWidth: "27cm",
  "@media print": {
    display: "block !important",
    background: "#fff",
    boxShadow: "none",
    margin: 0,
    padding: 0
  }
});

let wrapper = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "center",
  width: '100%',
  margin: "0 0 0 0"
});

let insightsTextArea = (isHalfCol = false) => css(inputClass, {
  resize: "none",
  display: "block",
  boxSizing: "padding-box",
  overflow: "visible",
  padding: 10,
  margin: isHalfCol ? '10px auto 0 10px' : '10px 0 0',
  width: isHalfCol ? "97%" : "100%",
  fontSize: "16px",
  lineHeight: "1.5em",
  textAlign: "justify",
  alignSelf: "center",
  minHeight: 50,
  "@media print": {
    pageBreakInside: "avoid",
    pageBreakAfter: "auto",
    border: "none"
  }
});

let earlyMarkersDivStyle = css({
  // border: "1px dotted gray",
  padding: 10,
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  alignItems: "flex-start",
  width: "100%",
  "@media print": {
    border: "none"
  }
});

let subwrapper = css({
  position: "relative",
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "flex-start",
  padding: "5px",
  margin: 0
});

let descWrapper = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "flex-start",
  padding: 20
});

let analysisDiv = css({
  margin: "0 auto",
  padding: 10,
  width: "100%",
  display: "block !important",
  whiteSpace: "pre-line" /* Preserve line breaks */,
  textAlign: "justify",
  "@media print": {
    pageBreakInside: "auto !important"
  }
});

let listStyle = css({
  marginLeft: 35,
  display:
    "list-item" /* This has to be "list-item"                                               */,
  listStyleType:
    "circle" /* See https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-type     */,
  listStylePosition:
    "inside" /* See https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-position */
});

let title = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "center",
  fontWeight: "bold",
  // padding: 10,
  fontSize: "large",
  textTransform: "uppercase"
});

let listTitle = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "flex-start",
  fontWeight: "bold",
  padding: 10,
  //fontSize: 'large',
  textTransform: "capitalize",
  display:
    "list-item" /* This has to be "list-item"                                               */,
  listStyleType:
    "square" /* See https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-type     */,
  listStylePosition:
    "inside" /* See https://developer.mozilla.org/en-US/docs/Web/CSS/list-style-position */
});

let meanDiv = css({
  display: "flex",
  //flex: '1',
  width: "80%",

  justifyContent: "center"
});

let meanLabel = css({
  display: "flex",
  flexWrap: "1",
  justifyContent: "center",
  textTransform: "uppercase",
  fontWeight: "bold",
  //fontSize: 'large',
  padding: "10px",
  width: 200
});

let meanItems = css({
  display: "flex",
  flexWrap: "1",
  justifyContent: "center",
  width: 200,
  padding: 5,
  border: "1px dotted gray"
});

let meanItemsLeft = css({
  display: "flex",
  flexWrap: "1",
  justifyContent: "flex-start",
  width: 200,
  padding: 5,
  border: "1px dotted gray"
});

let menuMessageDiv = css({
  position: "fixed",
  display: "flex",
  flexWrap: "wrap",
  background: "gray",
  justifyContent: "center",
  alignItems: "center",
  bottom: 210,
  right: 0,
  margin: "10px",
  padding: "10px 0 10px 0",
  width: "auto",
  borderRadius: 10,
  boxShadow: "-3px 3px 10px #424242",
  background: "white",
  opacity: "0.5",
  ":hover": {
    opacity: "1"
  },
  "@media print": {
    display: "none"
  }
});

let goodMessage = css({
  color: "#80cde9",
  padding: "5px"
});

let errorMessage = css({
  color: "red",
  padding: "5px"
});

let dropStyle = css({
  height: "100%",
  width: "100%"
});

let legnd = css({
  textAlign: "left",
  flex: "1",
  width: 185,
  margin: 0,
  padding: "0 0 0 5px"
});

let earlyMarkersDiv = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "center",
  border: "1px solid #eee",
  borderRadius: 5,
  padding: 10
});

let pageBreakBefore = css({
  width: "100%",
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  padding: '0 30px',
  '@media print': {
    padding: '0 !important',
    margin: '0 !important',
    pageBreakBefore: 'always'
  }
});

let pieTextStyle = css({
  fontSize: 14,
  fill: "#000"
});

const pieCornerTextStyle = css({
  display: "flex",
  flexWrap: "wrap",
  zIndex: "1",
  background: "none",
  position: "absolute",
  fontSize: ".7em"
});

const locationStyle = css({
  fontSize: "small",
  margin: "-10px 0 0 0"
});

const customStyle = {
  content: {
    textAlign: "center",
    zIndex: 6,
    width: "auto",
    height: "auto",
    top: "55%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: 20,
    boxShadow: `0 8px 15px rgba(0,0,0,.1)`,
    border: `1px solid rgba(0,0,0,0)`,
    borderRadius: 10,
  }
};

const dropzoneWrapper = css({
  width: "100%",
  height: "100%",
  position: 'relative',
  zIndex: 1001,
  "& > div": {
    cursor: "pointer",
    height: "100%",
    boxSizing: "border-box",
    outline: "none",
    display: "grid",
    placeContent: "center",
    transition: "250ms ease",
    position: 'relative',
    overflow: 'hidden',
    "& img": {
      width: '100%',
      display: "block",
      margin: "0 auto",
    },
    "& label": {
      fontWeight: 600,
      opacity: 1,
      textAlign: 'center',
      fontSize: 12,
      lineHeight: '16px',
      display: 'grid',
      placeContent: 'center',
      height: 75,
      position: 'absolute',
      bottom: 0,
      left: 0,
      background: 'linear-gradient(to top, black, transparent)',
      color: 'white',
      padding: 10,
      cursor: 'pointer',
      width: '100%'
    }
  }
});

const dohContainer = css({
  padding: '0 20px',
  width: '100%',
  margin: '0 auto',
  '@media print': {
    '& > section#determinants-group-wrapper': {
      paddingLeft: '0 !important',
      marginLeft: '0 !important',
      paddingRight: '0 !important',
      marginRight: '0 !important'
    }
  }
})

export { CHIReportNew };
