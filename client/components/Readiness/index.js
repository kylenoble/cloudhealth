import React from "react";
import Link from "next/link";

import Form from "../Form";

import AnswerService from "../../utils/answerService.js";
import GridContainer from "../NewSkin/Wrappers/GridContainer";

const answer = new AnswerService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modifyDiet: {
        id: 11,
        name: "modifyDiet",
        question: "Significantly modify your diet",
        type: "select",
        validation: "select",
        options: [
          { label: "5", value: 5 },
          { label: "4", value: 4 },
          { label: "3", value: 3 },
          { label: "2", value: 2 },
          { label: "1", value: 1 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      nutritionalSupplements: {
        id: 12,
        name: "nutritionalSupplements",
        question: "Take several nutritional supplements each day",
        type: "select",
        validation: "select",
        options: [
          { label: "5", value: 5 },
          { label: "4", value: 4 },
          { label: "3", value: 3 },
          { label: "2", value: 2 },
          { label: "1", value: 1 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      recordEating: {
        id: 13,
        name: "recordEating",
        question: "Keep a record of everything you eat each day",
        type: "select",
        validation: "select",
        options: [
          { label: "5", value: 5 },
          { label: "4", value: 4 },
          { label: "3", value: 3 },
          { label: "2", value: 2 },
          { label: "1", value: 1 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      modifyLifestyle: {
        id: 14,
        name: "modifyLifestyle",
        question: "Modify your lifestyle (e.g., work demands, sleep habits)",
        type: "select",
        validation: "select",
        options: [
          { label: "5", value: 5 },
          { label: "4", value: 4 },
          { label: "3", value: 3 },
          { label: "2", value: 2 },
          { label: "1", value: 1 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      practiceRelaxation: {
        id: 15,
        name: "practiceRelaxation",
        question: "Practice a relaxation technique",
        type: "select",
        validation: "select",
        options: [
          { label: "5", value: 5 },
          { label: "4", value: 4 },
          { label: "3", value: 3 },
          { label: "2", value: 2 },
          { label: "1", value: 1 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      regularExercise: {
        id: 16,
        name: "regularExercise",
        question: "Engage in regular exercise",
        type: "select",
        validation: "select",
        options: [
          { label: "5", value: 5 },
          { label: "4", value: 4 },
          { label: "3", value: 3 },
          { label: "2", value: 2 },
          { label: "1", value: 1 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      labTests: {
        id: 17,
        name: "labTests",
        question: "Have periodic lab tests to assess your progress",
        type: "select",
        validation: "select",
        options: [
          { label: "5", value: 5 },
          { label: "4", value: 4 },
          { label: "3", value: 3 },
          { label: "2", value: 2 },
          { label: "1", value: 1 }
        ],
        error: {
          active: false,
          message: "Please select a valid answer"
        },
        value: "default"
      },
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      }
    };

    this._handleChange = this._handleChange.bind(this);
    this._goToNext = this._goToNext.bind(this);
  }

  supportsFlexBox() {
    var test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  componentDidMount() {
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      flexibility(document.body);
    }
    //if(this.props.logged){
    this._setDefaultValues();
  }

  _setDefaultValues() {
    const {
      modifyDiet,
      nutritionalSupplements,
      recordEating,
      modifyLifestyle,
      practiceRelaxation,
      regularExercise,
      labTests
    } = this.state;

    modifyDiet.value = this.props.survey.modifyDiet || "";
    nutritionalSupplements.value = this.props.survey.nutritionalSupplements || "";
    recordEating.value = this.props.survey.recordEating || "";
    modifyLifestyle.value = this.props.survey.modifyLifestyle || "";
    practiceRelaxation.value = this.props.survey.practiceRelaxation || "";
    regularExercise.value = this.props.survey.regularExercise || "";
    labTests.value = this.props.survey.labTests || "";

    this.setState({
      modifyDiet,
      nutritionalSupplements,
      recordEating,
      modifyLifestyle,
      practiceRelaxation,
      regularExercise,
      labTests
    });
    console.log(this.state)
  }

  render() {
    return (
      <div style={styles.container}>
        <div style={styles.introInfo}>
          <h1>Readiness Questions</h1>
          <span>
            The answers below will help us assess your commitment and
            willingness to improve your health.
          </span>
        </div>

        <Form
          from="health-survey"
          readiness
          header="In order to improve your health, how willing are you to:"
          submitForm={this._goToNext}
          buttonText={
            this.props.location == "profile"
              ? "Update My Profile"
              : "Continue My Profile"
          }
          handleChange={this._handleChange}
          inputs={[
            this.state.modifyDiet,
            this.state.nutritionalSupplements,
            this.state.recordEating,
            this.state.modifyLifestyle,
            this.state.practiceRelaxation,
            this.state.regularExercise,
            this.state.labTests
          ]}
          secondaryOption=""
          formError={this.state.formError}
          formSuccess={this.state.formSuccess}
          back={this.props.back}
        />
      </div>
    );
  }

  _goToNext() {
    this.setState({
      formError: {
        active: false,
        message: ""
      }
    });

    var inputs = [
      this.state.modifyDiet,
      this.state.nutritionalSupplements,
      this.state.recordEating,
      this.state.modifyLifestyle,
      this.state.practiceRelaxation,
      this.state.regularExercise,
      this.state.labTests
    ];

    answer
      .create(inputs)
      .then(res => {
        if (res) {
          console.log("updated user profile");
          this.setState({
            formSuccess: {
              active: true,
              message: "Your Profile Was Updated"
            }
          });
          // go back
          this.props.back();
          //    return this.props.onClick('goals')
        } else {
          this.setState({
            formError: {
              active: true,
              message: "There was an error updating your profile"
            }
          });
        }
      })
      .catch(e => {
        console.log(e);
        this.setState({
          formError: {
            active: true,
            message: e.error
          }
        });
      }); // you would show/hide error messages with component state here
  }

  _handleChange(value, input, validated) {
    const inputName = input;
    var newInput = this.state[inputName];
    newInput.value = value;
    newInput.error.active = validated;

    this.setState({
      inputName: newInput
    });
  }
}

const styles = {
  container: {
    width: "100%",
    fontFamily: '"Roboto",sans-serif',
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden"
  },
  introInfo: {
    flex: "1 0 100%",
    fontFamily: '"Roboto",sans-serif',
    color: "#364563",
    fontSize: "14px",
    // padding: "10px",
    textAlign: "center"
  },
  formContainer: {
    border: "2px solid rgba(151,151,151,0.2)",
    borderRadius: "6px",
    maxWidth: "400px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.20)"
  },
  header: {
    borderBottom: "2px solid rgba(225,232,238,1)",
    padding: "15px 20px",
    fontSize: "18px",
    color: "#364563",
    fontFamily: '"Roboto-Medium",sans-serif',
    width: "360px"
  },
  form: {
    margin: "10px 30px 20px 20px"
  },
  formInput: {
    padding: "13px 30px 13px 30px",
    marginTop: "20px",
    width: "287px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    fontSize: "14px"
  },
  options: {
    marginTop: "15px",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif',
    padding: "13px 30px 13px 30px",
    WebkitAppearance: "none",
    MozAppearance: "none",
    appearance: "none",
    width: "350px",
    backgroundColor: "#fff",
    borderColor: "#d9d9d9 #ccc #b3b3b3",
    borderradius: "4px",
    border: "2px solid #E1E8EE",
    cursor: "default",
    borderSpacing: "0",
    borderCollapse: "separate",
    modifyDiet: "50px",
    outline: "none",
    overflow: "hidden",
    position: "relative",
    cursor: "pointer"
  },
  optionsDefault: {
    color: "rgba(169, 169, 169, 1)"
  },
  selectArrowZone: {
    cursor: "pointer",
    display: "table-cell",
    position: "relative",
    textAlign: "center",
    verticalAlign: "middle",
    width: "25px",
    paddingRight: "5px"
  },
  selectArrow: {
    borderColor: "#999 transparent transparent",
    borderStyle: "solid",
    borderWidth: "5px 5px 2.5px",
    display: "inline-block",
    modifyDiet: "0",
    width: "0",
    position: "absolute",
    right: "-18em",
    top: "-1.75em"
  },
  button: {
    backgroundColor: "#ED81B9",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "0px 0px 20px 0px",
    cursor: "pointer"
  },
  register: {
    textDecoration: "none",
    margin: "20px 10px",
    color: "#43484D",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif'
  }
};
