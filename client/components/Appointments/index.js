import React from "react";
import "./appointmentStyle.scss";
import AddAppointment from "./addAppointment.js";
import AppointmentsList from "./appointmentsList.js";
import AppointmentService from "../../utils/appointmentService.js";

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      doctorAppointments: [],
      loadingAppointments: true,
      showAddAppointment: false,
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      }
    };

    this.appointment = new AppointmentService();

    this._renderView = this._renderView.bind(this);
    this._showAddAppointment = this._showAddAppointment.bind(this);
    this._hideAddAppointment = this._hideAddAppointment.bind(this);
    this._addAppointmentSubmit = this._addAppointmentSubmit.bind(this);
    this._updateDoctorAppointments = this._updateDoctorAppointments.bind(this);
  }

  componentDidMount() {
    const comp = this;
    this.appointment
      .getAppointmentsList({ completed: false })
      .then(res => {
        if (res.name === "Error" || res.message === "Unauthorized") {
          comp.setState({
            loadingAppointments: false
          });
        } else {
          comp.setState({
            doctorAppointments: res,
            loadingAppointments: false
          });
        }
      })
      .catch(() => {
        comp.setState({
          loadingAppointments: false
        });
      }); // you would show/hide error messages with component state here
  }

  _renderView() {
    if (this.state.showAddAppointment) {
      return (
        <AddAppointment
          submitButtonPressed={this._addAppointmentSubmit}
          formError={this.state.formError}
          formSuccess={this.state.formSuccess}
        />
      );
    } else if (this.state.loadingAppointments) {
      return <div>Loading...</div>;
    }
    return (
      <AppointmentsList
        addAppointment={this._showAddAppointment}
        doctorAppointments={this.state.doctorAppointments}
        beginAppointment={this.props.startAppointment}
      />
    );
  }

  _showAddAppointment() {
    this._updateAddApointment(true);
  }

  _hideAddAppointment() {
    this._updateAddApointment(false);
  }

  _updateAddApointment(input) {
    this.setState({
      showAddAppointment: input
    });
  }

  _updateDoctorAppointments(appointments) {
    this.setState({
      doctorAppointments: appointments
    });
  }

  _addAppointmentSubmit(name, email, date) {
    this.appointment
      .create(name, email, date)
      .then(res => {
        this._updateDoctorAppointments(res);
        this._hideAddAppointment();
      })
      .catch(e => {
        this.setState({
          formError: {
            active: true,
            message: e.error
          }
        });
      }); // you would show/hide error messages with component state here
  }

  render() {
    return <div className="container">{this._renderView()}</div>;
  }
}
