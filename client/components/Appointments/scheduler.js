/* eslint-disable no-nested-ternary */
/* eslint-disable no-param-reassign */
import React from "react";
import { css } from "glamor";
import moment from "moment-timezone";
import Calendar from "react-big-calendar";
import Modal from "react-modal";
import { confirmAlert } from "react-confirm-alert"; // Import
import Form from "../../components/Form";
import ScheduleService from "../../utils/schedulesService";
import Reschedule from "./rescheduler";
import Constants from "../../constants";
import AppointmentService from "../../utils/appointmentService";
import { saveAppointmentHistory } from "../../helpers";
import Colors from "../NewSkin/Colors";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Button from "../NewSkin/Components/Button";
import {
  inputStyleObj,
  labelClass,
  combined,
  titleWbackground
} from "../NewSkin/Styles";

const hoursPerSlot = Constants.HOURSPERSLOT;
const schedulesService = new ScheduleService();
const Appointment = new AppointmentService();

const localizer = Calendar.momentLocalizer(moment); // or globalizeLocalizer

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      slotInfo: {},
      calenderCurrentTab: "month",
      loading: false,
      rescheduleTab: false,
      modal: {
        addSched: false,
        detail: false,
        rescheduler: false
      },
      currentSlotInfo: {},
      tempSchedStorage: [],
      schedule: [],
      fields: [],
      startTime: {
        id: "",
        name: "startTime",
        label: (
          <span className={combined([labelClass], { fontSize: 16 })}>
            Start Time
          </span>
        ),
        question: "Select Start Time",
        type: "dropdown",
        disabled: false,
        //label: 'Start Date',
        validation: "required",
        options: [],
        error: {
          active: false,
          message: "Please select Start Time"
        },
        value: "x"
      },
      endTime: {
        id: "",
        name: "endTime",
        label: (
          <span
            className={combined([labelClass], {
              fontSize: 16,
              margin: "auto 0",
              textIndent: 20
            })}
          >
            End Time
          </span>
        ),
        question: "Select End Time",
        type: "dropdown",
        disabled: false,
        //label: 'End Date',
        validation: "required",
        options: [],
        error: {
          active: false,
          message: "Please select End Time"
        },
        value: "y"
      },
      schedTitle: {
        id: "",
        name: "schedTitle",
        question: "Enter Schedule title",
        label: (
          <span className={combined([labelClass], { fontSize: 16 })}>
            Title
          </span>
        ),
        type: "text",
        //label: 'Schedule Title',
        validation: "required",
        options: [],
        error: {
          active: false,
          message: "Please enter Schedule Title"
        },
        value: "Consultation"
      },
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      }
    };

    this._datetimeSelection = this._datetimeSelection.bind(this);
    this._addSchedule = this._addSchedule.bind(this);
    this._handleChange = this._handleChange.bind(this);
    //this.closeModal = this.closeModal.bind(this);
    this.modalSwitch = this.modalSwitch.bind(this);
    this._editSched = this._editSched.bind(this);
    this._acceptAppointment = this._acceptAppointment.bind(this);
    this._onView = this._onView.bind(this);
  }

  componentWillMount() {
    // this._getSchedules()
    document.body.style.overflowY = "hidden";
  }

  componentWillUnmount() {
    document.body.style.overflowY = "auto";
  }

  componentDidMount() {
    this._getSchedules();
    this._updateFields();
  }

  _getSchedules() {
    this.setState({ loading: true });
    schedulesService
      .get({ id: this.props.user.id })
      .then(res => {
        this._saveDataToState(res);
      })
      .catch(e => console.log(e));
  }

  _getSelectedAppointments() {
    //let data = []
    this.props.queuedPatients().then(res => {
      const currentSlotInfo = this.state.currentSlotInfo;
      const data = res;
      let schedule = this.state.schedule;
      const tempSchedStorage = this.state.tempSchedStorage;
      const appointments = [];
      if (data && data.length > 0) {
        data.forEach(item => {
          if (currentSlotInfo.id === item.id) {
            currentSlotInfo.start = item.datetime;
          }
          const sdate = item.datetime;
          const edate = moment(sdate).add(hoursPerSlot, "hours");
          appointments.push({
            type: "appointment",
            active: item.active,
            name: item.name,
            title: item.name,
            id: item.id,
            start: moment(sdate)._d,
            end: moment(edate)._d,
            status: item.status,
            concern: item.concern,
            doctor_id: item.doctor_id,
            patient_id: item.patient_id,
            datetime: item.datetime,
            patient_company_id: item.patient_company_id,
            appointment_code: item.appointment_code,
            email: item.email
          });
        });
      }

      schedule = tempSchedStorage.concat(appointments);
      this.setState(
        {
          schedule
        },
        () => this.setState({ loading: false })
      );
    });
  }

  _saveDataToState(res) {
    const data = res;
    //let schedule = this.state.schedule
    let tempSchedStorage = this.state.tempSchedStorage;
    tempSchedStorage = [];
    //schedule = []
    data.forEach(item => {
      tempSchedStorage.push({
        type: "schedule",
        id: item.id,
        start: moment(item.start_datetime)._d,
        end: moment(item.end_datetime)._d,
        title: item.title,
        status: item.status
      });
    });
    this.setState(
      {
        tempSchedStorage
      },
      () => {
        this._getSelectedAppointments();
      }
    );
  }

  modalSwitch(name, bol) {
    this._setDefaultValue();
    const modal = this.state.modal;
    if (!bol) {
      this.setState({ currentSlotInfo: {} });
    }

    modal[name] = bol;
    this.setState({
      modal
    });
  }

  _updateFields() {
    let fields = this.state.fields;
    const currentFields = [
      this.state.schedTitle,
      this.state.startTime,
      this.state.endTime
    ];
    // const arrfields = [];

    // for (let i = 0; i < currentFields.length; i++) {
    //   arrfields.push(currentFields[i]);
    // }

    fields = currentFields;

    this.setState({
      fields
    });
  }

  _onSelectSlot(slotInfo) {
    this._datetimeSelection(slotInfo);
    this.modalSwitch("addSched", true);
  }

  _datetimeSelection(slotInfo) {
    const sst = moment(slotInfo.start);
    const set = moment(slotInfo.end);
    const nt = moment(slotInfo.start).format("MMMM DD YYYY");
    const timeCount = 48;
    const opt1 = [];
    const opt2 = [];
    const fields = this.state.fields;

    for (let i = 0; i < timeCount; i++) {
      const ch = moment(nt).add(i / 2, "hour");
      opt1.push({
        label: ch.format("hh:mm a"),
        value: ch._d.toString() || undefined
      });
    }

    for (let i = 0; i < timeCount; i++) {
      const ch = moment(set).add(i / 2, "hour");
      opt2.push({
        label: ch.format("hh:mm a"),
        value: ch._d.toString() || undefined
      });
    }

    fields.forEach(item => {
      item.error.active = false;

      if (item.name === "startTime") {
        item.label = (
          <span className={combined([labelClass], { fontSize: 16 })}>
            Start Time
            {/* for {sst.format("ll")} */}
          </span>
        );
        item.options = opt1;
        item.value = this.state.currentSlotInfo.start
          ? this.state.currentSlotInfo.start
          : this.state.calenderCurrentTab === "month"
            ? ""
            : moment(sst)._d.toString();
      } else if (item.name === "endTime") {
        item.label = (
          <span className={combined([labelClass], { fontSize: 16 })}>
            End Time
            {/* for {set.format("ll")} */}
          </span>
        );
        if (this.state.calenderCurrentTab === "month") {
          item.options = opt1;
        } else {
          item.options = opt1;
        }

        const end_date = moment(set);
        if (this.state.calenderCurrentTab === "month") {
          end_date.add(23, "hour");
        }
        item.value = this.state.currentSlotInfo.end
          ? this.state.currentSlotInfo.end
          : this.state.calenderCurrentTab === "month"
            ? ""
            : slotInfo.end;
      } else if (item.name === "schedTitle") {
        item.value = slotInfo.title
          ? slotInfo.title
          : this.state.schedTitle.value;
      }
    });

    this.setState({
      fields,
      slotInfo
    });
  }

  _toggle() {
    this.setState({
      rescheduleTab: !this.state.rescheduleTab
    });
  }

  _removeSched() {
    this.setState({
      formError: {
        active: false,
        message: ""
      }
    });

    const dta = {
      id: this.state.currentSlotInfo.id
    };

    const obj = {
      option: "delete",
      data: dta
    };

    schedulesService
      .update(obj)
      .then(() => {
        this._getSchedules();
      })
      .catch(e => {
        console.log("errror");
        console.log(e);
      });
    this.modalSwitch("detail", false);
  }

  _editSched() {
    this.setState({
      formError: {
        active: false,
        message: ""
      }
    });

    const currentSlotInfo = this.state.currentSlotInfo;

    const dta = {
      id: currentSlotInfo.id,
      start_datetime: moment(this.state.startTime.value)._d,
      end_datetime: moment(this.state.endTime.value)._d,
      title: this.state.schedTitle.value,
      modified_date: moment()._d
    };

    const obj = {
      option: "update",
      data: dta
    };

    const schedules = this.state.tempSchedStorage;
    let valid = true;
    schedules.forEach(item => {
      if (item.id !== dta.id) {
        if (!this._validateTime(dta, item)) valid = false;
      } else {
        console.log(`${item.id} ${dta.id}`);
      }
    });

    if (!valid) {
      // eslint-disable-next-line no-undef
      alert("Not allowed");
      return false;
    }

    schedulesService
      .update(obj)
      .then(() => {
        this._getSchedules();
      })
      .catch(e => {
        console.log(e);
      });
    this.modalSwitch("detail", false);
  }

  _validateTime(data, item) {
    let valid = true;
    const startTimeInvalid =
      moment(data.start_datetime).toISOString() <=
      moment(item.start).toISOString() &&
      moment(data.start_datetime).toISOString() >=
      moment(item.end).toISOString();
    const endTimeInvalid =
      moment(data.end_datetime).toISOString() <=
      moment(item.start).toISOString() &&
      moment(data.end_datetime).toISOString() >= moment(item.end).toISOString();
    const invalidTimeRange =
      moment(data.start_datetime).toISOString() <=
      moment(item.start).toISOString() &&
      moment(data.end_datetime).toISOString() >= moment(item.end).toISOString();
    const invalidStartEnd =
      moment(data.start_datetime).toISOString() >=
      moment(data.end_datetime).toISOString();
    if (
      startTimeInvalid ||
      endTimeInvalid ||
      invalidStartEnd ||
      invalidTimeRange
    ) {
      valid = false;
    }

    return valid;
  }

  _addSchedule() {
    this.setState({
      formError: {
        active: false,
        message: ""
      }
    });
    //  let schedule = this.state.schedule

    const data = {
      user_id: this.props.user.id,
      start_datetime: moment(this.state.startTime.value)._d,
      end_datetime: moment(this.state.endTime.value)._d,
      title: this.state.schedTitle.value,
      created_date: moment()._d
    };

    const schedules = this.state.tempSchedStorage;
    let valid = true;
    schedules.forEach(item => {
      if (!this._validateTime(data, item)) valid = false;
    });

    if (!valid) {
      // eslint-disable-next-line no-undef
      alert("Please Check input Dates");
      return false;
    }

    if (this.state.calenderCurrentTab === "month") {
      const { slots } = this.state.slotInfo;

      slots.forEach(slot => {
        const schedDate = moment(slot)
          .format("ll")
          .toString();
        const startTime = moment(this.state.startTime.value)
          .format("h:mm:ss a")
          .toString();
        const endTime = moment(this.state.endTime.value)
          .format("h:mm:ss a")
          .toString();
        const start = moment(`${schedDate} ${startTime}`).format("lll");
        const end = moment(`${schedDate} ${endTime}`).format("lll");

        const data = {
          user_id: this.props.user.id,
          start_datetime: moment(start).toISOString(),
          end_datetime: moment(end).toISOString(),
          title: this.state.schedTitle.value,
          created_date: moment()._d
        };

        this._saveScheduleToDB(data);
      });
    } else {
      this._saveScheduleToDB(data);
    }
  }

  _setDefaultValue() {
    const { schedTitle } = this.state;
    schedTitle.value = "Consultation";
    this.setState({ schedTitle });
  }

  _saveScheduleToDB(data) {
    schedulesService
      .create(data)
      .then(() => {
        this._getSchedules();
        this.modalSwitch("addSched", false);
        this._setDefaultValue();
      })
      .catch(e => console.log(e));
  }

  _handleChange(value, input, validated) {
    const inputName = input;
    const newInput = this.state[inputName];
    newInput.value = value;
    newInput.error.active = validated;
    this.setState({
      newInput
    });
  }

  _showScheduleDetails(sched) {
    this.setState(
      {
        currentSlotInfo: sched
      },
      () => {
        this.modalSwitch("detail", true);
        this._datetimeSelection(sched);
      }
    );
  }

  _acceptAppointment() {
    this.props.acceptAppointment(this.state.currentSlotInfo).then(res => {
      if (res) {
        this._getSchedules();
        this.modalSwitch("detail", false);
      }
    });
  }

  _rescheduleAppointment() {
    this.modalSwitch("detail", false);
    this.modalSwitch("rescheduler", true);
  }

  _confirmCancelAppointment(item) {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: "Please confirm appoinment cancellation", // Message dialog
      buttons: [
        {
          label: "Yes, cancel this appointment.",
          onClick: this._cancelAppointment(item)
        },
        {
          label: "Im not sure."
        }
      ]
    });
  }

  _cancelAppointment(item) {
    this.props.sendCancelNotification(item);

    const {
      appointment_code,
      patient_id,
      concern,
      datetime,
      status,
      doctor_id
    } = item;

    // data for Appointment History
    const appointmentHistoryData = {
      appointment_code,
      patient_id,
      history_created_date: moment(),
      concern,
      appointment_date: datetime,
      appointment_status: status,
      action: "Cancelled",
      md_id: doctor_id
    };

    const data = {
      date_rescheduled: moment()._d,
      //datetime :  moment(time)._d,
      status: 3,
      active: true,
      is_modified: true
    };

    Appointment.update(item.id, data)
      .then(res => {
        if (res) {
          saveAppointmentHistory(appointmentHistoryData);
          this._getSelectedAppointments();
          this.modalSwitch("detail", false);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _onView(tab) {
    this.setState({
      calenderCurrentTab: tab
    });
  }

  eventStyleGetter(event) {
    let color = "#87cde9";
    if (event.type === "appointment") {
      if (event.status === 2) {
        color = "orange";
      } else if (event.status === 3) {
        color = "red";
      } else if (event.active) {
        color = "#f4c5e3";
      } else {
        color = "yellow";
      }
    }
    const backgroundColor = color;
    const style = {
      backgroundColor,
      borderRadius: "8px",
      opacity: 0.8,
      color: "black",
      border: "1px dotted",
      display: "block"
    };
    return {
      style
    };
  }

  _displayMessage() {
    const { start, end, slots } = this.state.slotInfo;
    let message = ``;
    if (this.state.calenderCurrentTab === "month") {
      if (slots && slots.length > 1) {
        message += `${moment(start).format("ll")} - ${moment(end).format(
          "ll"
        )}`;
      } else {
        message += moment(start).format("ll");
      }
    } else {
      message += moment(start).format("ll");
    }
    return <div className={titleWbackground}>{message}</div>;
  }

  render() {
    return (
      <div className="wrapper">
        <Modal
          ariaHideApp={false}
          isOpen={this.state.loading}
          //onAfterOpen={this.afterOpenModal}
          onRequestClose={() => this.modalSwitch("addSched", false)}
          shouldCloseOnOverlayClick={false}
          style={customStyles}
        >
          <div className={loadingDiv}>Processing....</div>
        </Modal>

        <Modal
          ariaHideApp={false}
          isOpen={this.state.modal.rescheduler}
          onRequestClose={() => this.modalSwitch("rescheduler", false)}
          shouldCloseOnOverlayClick={false}
          style={customStyles}
        >
          <div className={modalContentWrapper}>
            <Reschedule
              doctorId={this.props.user.id}
              appointmentData={this.state.currentSlotInfo}
              getSelectedAppointments={() => this._getSelectedAppointments()}
              user={this.props.user}
            />
          </div>

          <Button styles={{ margin: "20px auto" }}>
            <button onClick={() => this.modalSwitch("rescheduler", false)}>
              Close
            </button>
          </Button>
        </Modal>

        <Modal
          ariaHideApp={false}
          isOpen={this.state.modal.addSched}
          //onAfterOpen={this.afterOpenModal}
          onRequestClose={() => this.modalSwitch("addSched", false)}
          shouldCloseOnOverlayClick={false}
          style={customStyles}
        >
          <div className={modalContentWrapper}>
            <GridContainer
              columnSize="50px 1fr"
              styles={{
                borderBottom: "1px solid #eee",
                padding: 15,
                paddingTop: 0
              }}
            >
              <img width="30" src="/static/icons/add_sched.png" alt="" />
              <span className={titleStyle}>Create New Schedule</span>
            </GridContainer>
            {this._displayMessage()}
            <section className={formWrapper} data-name="form-wrapper">
              <Form
                submitForm={this._addSchedule}
                buttonText={"Set Schedule"}
                handleChange={this._handleChange}
                inputs={this.state.fields}
                secondaryOption=""
                formError={this.state.formError}
                formSuccess={this.state.formSuccess}
              />
            </section>
            <Button styles={{ margin: 'auto' }}>
              <button onClick={() => this.modalSwitch("addSched", false)}>
                Cancel
                </button>
            </Button>
          </div>
        </Modal>

        <Modal
          ariaHideApp={false}
          isOpen={this.state.modal.detail}
          onRequestClose={() => this.modalSwitch("detail", false)}
          shouldCloseOnOverlayClick={false}
          style={customStyles}
        >
          <div className={modalContentWrapper}>
            {this.state.currentSlotInfo.type === "schedule" ? (
              <GridContainer
                columnSize="50px 1fr"
                styles={{
                  borderBottom: "1px solid #eee",
                  padding: 15,
                  paddingTop: 0
                }}
              >
                <img width="30" src="/static/icons/update_sched.png" alt="" />
                <span className={titleStyle}>Update Schedule</span>
              </GridContainer>
            ) : null}

            {this.state.currentSlotInfo.type === "schedule" ? (
              <section className={formWrapper} data-name="form-wrapper">
                <Form
                  submitForm={this._editSched}
                  buttonText={"Save Changes"}
                  handleChange={this._handleChange}
                  inputs={this.state.fields}
                  secondaryOption=""
                  formError={this.state.formError}
                  formSuccess={this.state.formSuccess}
                />
              </section>
            ) : (
                <div className={infoStyle}>
                  <div className={infoTitleContentStyle}>
                    <GridContainer
                      columnSize="50px 1fr"
                      styles={{ marginBottom: 30 }}
                    >
                      <img width={30} src="/static/icons/appointment_icon.svg" />
                      <span className={titleStyle}>Consultation Appointment</span>
                    </GridContainer>

                    <GridContainer columnSize="100px 1fr" classes={[content]}>
                      <span>Name:</span>
                      <strong>{this.state.currentSlotInfo.name}</strong>
                      <span>Concern:</span>
                      <strong>{this.state.currentSlotInfo.concern}</strong>
                      <span>Start:</span>
                      <strong>
                        {moment(this.state.currentSlotInfo.start).format("LLL")}
                      </strong>
                      <span>End:</span>
                      <strong>
                        {moment(this.state.currentSlotInfo.end).format("LLL")}
                      </strong>
                    </GridContainer>
                  </div>
                </div>
              )}
          </div>

          <div className={detailOptionStyle}>
            {this.state.currentSlotInfo.type === "schedule" ? (
              <Button styles={{ margin: "5px auto" }}>
                <button onClick={() => this._removeSched()}>Remove</button>
              </Button>
            ) : !this.state.currentSlotInfo.active ? (
              <div>
                <span className={btn} onClick={() => this._acceptAppointment()}>
                  Approve
                </span>
                <span
                  className={btn}
                  onClick={() => this._rescheduleAppointment()}
                >
                  Reschedule
                </span>
                <span
                  className={btn}
                  onClick={() =>
                    this._confirmCancelAppointment(this.state.currentSlotInfo)
                  }
                >
                  Cancel
                </span>
              </div>
            ) : null}
          </div>

          <Button styles={{ margin: "10px auto" }}>
            <button onClick={() => this.modalSwitch("detail", false)}>
              Close
            </button>
          </Button>
        </Modal>

        <GridContainer columnSize="200px 1fr">
          <GridContainer
            styles={{ justifyContent: "left", textAlign: "left !important" }}
          >
            <strong style={{ textAlign: "left" }}>Legend</strong>
            <div className={legendItemStyle}>
              <span className={legendIcon} style={{ background: "#9bd3e9" }} />
              Schedule Range
            </div>

            <div className={legendItemStyle}>
              <span className={legendIcon} style={{ background: "#f6d1e9" }} />
              Accepted
            </div>

            <div className={legendItemStyle}>
              <span className={legendIcon} style={{ background: "yellow" }} />
              Pending
            </div>

            <div className={legendItemStyle}>
              <span className={legendIcon} style={{ background: "orange" }} />
              Rescheduled
            </div>

            <div className={legendItemStyle}>
              <span className={legendIcon} style={{ background: "red" }} />
              Cancelled
            </div>
          </GridContainer>

          <section className={calendarWrapper(this.state.calenderCurrentTab)}>
            <Calendar
              localizer={localizer}
              selectable
              defaultDate={new Date()}
              defaultView={"month"}
              views={["month", "week", "day", "work_week"]}
              events={this.state.schedule}
              style={{
                position: "relative",
                height: 380,
                width: 800,
                zIndex: 0,
                margin: "0 auto"
              }}
              onSelectEvent={schedule => this._showScheduleDetails(schedule)}
              eventPropGetter={this.eventStyleGetter}
              onSelectSlot={slotInfo => this._onSelectSlot(slotInfo)}
              onView={tab => this._onView(tab)}
            />
          </section>
        </GridContainer>
      </div>
    );
  }
} // end class

const formWrapper = css({
  padding: 15,
  "& input, & select": {
    ...inputStyleObj,
    margin: "0 !important"
  },
  "& [data-name]": {
    gridGap: "0 !important",
    gridTemplateColumns: "1fr 1fr !important"
  },
  "& #form-container div": {
    margin: "0 !important"
  },
  '& [data-name="button-wrapper"]': {
    transform: "translateX(50%)",
    "> *": {
      width: "230px !important"
    }
  }
});

const content = css({
  "& > span, & > strong": {
    textAlign: "left"
  }
});

const calendarWrapper = tab =>
  css({
    position: "relative",
    marginBottom: tab === "month" ? 50 : 0,
    width: "100%",
    zIndex: 0,
    minHeight: tab === "month" ? "fit-content" : 470,
    "& .rbc-event": {
      padding: "1px 3px !important",
      borderRadius: "2px !important",
      color: "#fff !important",
      cursor: "pointer",
      border: "none !important",
      margin: "1px 2px",
      fontSize: 12,
      ":focus": {
        outline: "none"
      }
    },
    "& .rbc-day-bg + .rbc-day-bg": {
      borderLeft: "1px solid #eee8 !important",
      cursor: "pointer"
    },
    "& .rbc-month-row + .rbc-month-row": {
      cursor: "pointer",
      borderTop: "1px solid #eee8 !important"
    },
    "& .rbc-month-view, & .rbc-time-view": {
      minHeight: 380,
      border: "1px solid #eee8 !important"
    },
    "& .rbc-header": {
      borderBottom: "1px solid #eee8 !important",
      background: Colors.skyblue,
      color: "white"
    },
    "& .rbc-time-header-content": {
      "& .rbc-header": {
        paddingBottom: 10
      }
    },
    "& .rbc-header + .rbc-header": {
      borderLeft: `1px solid ${Colors.skyblue} !important`
    },
    "& body, html": {
      overflowY: "hidden"
    }
  });

const legendIcon = css({
  width: 12,
  height: 12,
  borderRadius: "100%",
  display: "inline-block",
  marginRight: 10,
  verticalAlign: "middle",
  margin: "auto"
});

const customStyles = {
  content: {
    textAlign: "center",
    zIndex: 6,
    width: "auto",
    height: "auto",
    top: "55%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    // marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: 20,
    boxShadow: `0 8px 15px rgba(0,0,0,.1)`,
    border: `1px solid rgba(0,0,0,0)`,
    borderRadius: 10
  }
};

// styling
let btn = css({
  //float: 'right',
  padding: "10px 20px",
  backgroundColor: "white",
  border: "1px solid white",
  color: Colors.pink,
  borderRadius: 50,
  // margin: 5,
  fontSize: 14,
  cursor: "pointer",
  background: "white",
  transition: "250ms ease",
  ":hover": {
    background: Colors.pink,
    color: "white"
  }
});

let detailOptionStyle = css({
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center"
});

let modalContentWrapper = css({
  // display: "flex",
  // flexWrap: "wrap",
  // justifyContent: "center",
  // margin: '10px 20px 20px',
  padding: 10,
  // border: "1px dotted",
  borderRadius: 10
  //width: '80%'
});

let loadingDiv = css({
  padding: 50,
  textTransform: "uppercase",
  color: "#87cde9",
  fontSize: "large",
  fontWeight: "bold"
});

let titleStyle = css({
  fontWeight: "bold",
  color: Colors.blueDarkAccent,
  textAlign: "left !important",
  display: "block"
});

let infoStyle = css({
  flex: 1,
  display: "flex",
  padding: 30,
  flexWrap: "wrap",
  justifyContent: "flex-start",
  alignItems: "flex-start"
});

let infoTitleContentStyle = css({
  flex: "1 0 45%",
  textAlign: "left"
});

let legendItemStyle = css({
  padding: 5,
  fontSize: 14,
  display: "grid",
  gridTemplateColumns: "12px 1fr",
  gridGap: 10,
  alignContent: "left !important",
  textAlign: "left !important"
});
