/* eslint-disable no-unused-vars */
import React from "react";
import { css } from "glamor";
import moment from "moment-timezone";
import AppointmentService from "../../utils/appointmentService";
import BookAppNotifService from "../../utils/book_Appointment_Notification_Service";
import Slots from "./slots";
import Constants from "../../constants";
import { saveAppointmentHistory } from "../../helpers";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Colors from "../NewSkin/Colors";
import { scrollClass } from "../NewSkin/Styles"

const bookAppNotifService = new BookAppNotifService();
const hoursPerSlot = Constants.HOURSPERSLOT;
const Appointment = new AppointmentService();

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appointmentData: this.props.appointmentData,
      message: {},
      id: "",
      datetime: ""
    };

    this._setDatetime = this._setDatetime.bind(this);
    this._setMessage = this._setMessage.bind(this);
    this._resetMessage = this._resetMessage.bind(this);
    console.log("PROPS", this.props);
  }

  _reschedule(appointmentData, data, time) {
    const { id, appointment_code } = appointmentData;

    return Appointment.update(id, data)
      .then(res => {
        if (res) {
          this._sendRescheduledAppNotification(appointmentData);
          this._sendRescheduledAppointmentEmail(appointmentData, time);
          return res;
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _sendRescheduledAppNotification = item => {
    const sender = {
      type: "Doctor",
      name: "System",
      id: this.props.user.id
    };

    const doctor = {
      type: "Doctor",
      name: this.props.user.name,
      id: this.props.user.id,
      appointmentDate: item.datetime,
      patientName: item.name
    };

    const patient = {
      type: "Patient",
      name: item.name,
      id: item.patient_id,
      appointmentDate: item.datetime,
      patientName: item.name
    };

    const message = item.concern;

    const subject = "Rescheduled Appointment";

    const notificationDetails = { sender, doctor, patient, message, subject };

    this._sendSystemNotification(notificationDetails);
  };

  _sendSystemNotification = notificationDetails => {
    bookAppNotifService
      .create(notificationDetails)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
  };

  _sendRescheduledAppointmentEmail = (appData, time) => {
    const emailBodyForPatient = `
            <p style="font-size: 16px; margin: 0">Your booked appointment has been rescheduled. Please see below details:</p>

            <p style="font-size: 16px; margin: 0px; color: #9e9e9e">Old Appointment date and time: <span style="color: #000; font-weight:600">${moment(
      appData.datetime
    ).format("lll")}<span></p>
            <p style="font-size: 16px; margin: 0px; color: #9e9e9e">New Appointment date and time: <span style="color: #000; font-weight:600">${moment(
      time
    ).format("lll")}<span></p>

            <p style="font-size: 16px; color: #9e9e9e; margin-bottom: 0px; margin-top: 0px">Patient Name:
                <span style="color: #000; font-weight: 600">${
      appData.name
      }</span>
            </p>`;

    const emailBodyForDoctor = `
            <p style="font-size: 16px; margin: 0">You have rescheduled an appointment. Please see below details:</p>
            <p style="font-size: 16px; margin: 0px; color: #9e9e9e">Appointment date and time: <span style="color: #000; font-weight:600">${moment(
      appData.datetime
    ).format("lll")}<span></p>
            <p style="font-size: 16px; margin: 0px; color: #9e9e9e">New Appointment date and time: <span style="color: #000; font-weight:600">${moment(
      time
    ).format("lll")}<span></p>

            <p style="font-size: 16px; color: #9e9e9e;margin-bottom: 0px; margin-top: 0px">Patient Name:
                <span style="color: #000; font-weight: 600">${
      appData.name
      }</span>
            </p>`;

    let emailDetails = "";

    const emailDetailsForPatient = {
      recipient: appData.email,
      date: moment(new Date()).format("lll"),
      title: "Rescheduled Appointment",
      body: emailBodyForPatient
    };

    const emailDetailsForDoctor = {
      recipient: this.props.user.email,
      date: moment(new Date()).format("lll"),
      title: "Rescheduled Appointment",
      body: emailBodyForDoctor
    };

    emailDetails = emailDetailsForPatient;
    this._emailSetup(emailDetails);

    setTimeout(() => {
      emailDetails = emailDetailsForDoctor;
      this._emailSetup(emailDetails);
    }, 2000);
  };

  _emailSetup = email => {
    const subject = "New Notification from CloudHealthAsia";
    const text = "This is the notification of your Appointment";
    const to = email.recipient; // recipient
    const date = email.date; // notification date
    const notificationTitle = email.title; // notification title ex. Cancelled Appointment
    const notificationBody = email.body; // notification body ex. <span style="">Message here</span>

    const content = `
            <p style="font-size: 16px;">Please see below notification for your account:</p>

            <div style="border: 1px solid #ccc; padding: 20px; border-radius: 5px; background: #eee1; box-shadow: 0 8px 32px rgba(0,0,0,.050)">
                <p style="font-size: 16px; color: #9e9e9e; margin: 0">${date}</p>

                <h5 style="font-size: 1.64rem; font-weight: 400; margin: 0.82rem 0 0.656rem 0">${notificationTitle}</h5>

                ${notificationBody}
            </div>

            <p style="font-size: 16px">Please log-in to your account for further details.</p>
            <a title="Login to CloudHealthAsia" href="https://cloudhealthasia.com/login" target="_blank" style="text-decoration: none; padding: 10px 20px; background: #81cce6; color: #fff; border-radius: 5px; margin-top: 20px; display: block; width: 200px; font-weight: 500;">Login to CloudHealthAsia</a>
        `;

    const emailDetails = { to, content, subject, text };

    this._sendEmailNotification(emailDetails);
  };

  _sendEmailNotification = emailDetails => {
    bookAppNotifService
      .sendBookAppEmailNotification(emailDetails)
      .then(() => {
        console.log("Sending Email Notification Successfull!");
      })
      .catch(err => {
        console.log(err);
      });
  };

  _create(time, parentAppointmentCode = null) {
    const appointment = this.state.appointmentData;

    const data = {
      NoPassword: true,
      name: appointment.name,
      email: appointment.email,
      date: moment(time)._d,
      doctorId: appointment.doctor_id,
      concern: appointment.concern,
      date_rescheduled: moment()._d,
      patient_company_id: appointment.patient_company_id,
      appointment_code: new Date().getTime(),
      parent_appointment_code: parentAppointmentCode || null,
      active: true
    };

    this.createNewAppoitnment(data);
  }

  createNewAppoitnment(data) {
    return Appointment.create(data)
      .then(() => Promise.resolve(true))
      .catch(e => {
        console.log("ERROR : ");
        console.log(e);
      });
  }

  _checkIfStillAvailable(dte) {
    const qry = { active: false, apmt_date: dte };

    return Appointment.getAppointmentsList(qry)
      .then(res => {
        if (res && res.length > 0) {
          return Promise.resolve(false);
        }
        return Promise.resolve(true);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _setDatetime(time) {
    const { appointmentData } = this.state;
    const {
      appointment_code,
      patient_id,
      concern,
      datetime,
      status,
      doctor_id
    } = appointmentData;

    const data = {
      date_rescheduled: moment()._d,
      //datetime :  moment(time)._d,
      status: 2,
      active: true,
      is_modified: true
    };

    // update scripts
    // eslint-disable-next-line no-undef
    const conf = confirm("Reschedule now?");
    if (conf) {
      this._checkIfStillAvailable(moment(time).toISOString())
        .then(res => {
          if (res) {
            this._reschedule(appointmentData, data, time).then(response => {
              if (response) {
                // data for Appointment History
                const appointmentHistoryData = {
                  appointment_code,
                  patient_id,
                  history_created_date: moment(),
                  concern,
                  appointment_date: datetime,
                  appointment_status: status,
                  action: "Rescheduled",
                  md_id: doctor_id
                };

                appointmentData.start = time;
                this.props.getSelectedAppointments();

                // insert record to appointment history table
                saveAppointmentHistory(appointmentHistoryData);

                this._create(time, appointmentData.appointment_code);

                this._setMessage("Successfully Rescheduled.", true);
                this.setState({ appointmentData });
              } else {
                this._setMessage("Error Rescheduling.", true);
              }
            });
          } else {
            this._setMessage("Sorry that slot not available.", false);
          }
        })
        .catch(e => {
          console.log("Error : ");
          console.log(e);
        });
    } else {
      console.log("Reschedule Cancel");
    }
  }

  _setMessage(StringMessage, good) {
    const message = this.state.message;
    if (good) {
      message.goodMessage = StringMessage;
    } else {
      message.badMessage = StringMessage;
    }
    this.setState({ message });
    this._resetMessage();
  }

  _resetMessage() {
    // clear messages 10 seconds after showing
    const that = this;
    setTimeout(() => {
      that.setState({ message: {} });
    }, 10000);
  }

  render() {
    return (
      <GridContainer columnSize={'1fr 1.3fr'} styles={{ padding: '20px 20px 0 20px', minWidth: 1000, maxWidth: 1000, maxHeight: 500 }}>

        <section className={infoTitleContentStyle}>
          <GridContainer columnSize="50px 1fr" styles={{ marginBottom: 30 }}>
            <img width={30} src="/static/icons/appointment_icon.svg" />
            <span className={titleStyle}>Consultation Appointment</span>
          </GridContainer>

          <GridContainer columnSize="1fr" gap={10} classes={[content]}>
            <span>Name:</span><strong>{this.state.appointmentData.name}</strong>
            <span>Concern:</span><strong>{this.state.appointmentData.concern}</strong>
            <span>Start:</span><strong>{moment(this.state.appointmentData.start).format("LLL")}</strong>{" "}
            <span>End:</span>
            <strong>{moment(this.state.appointmentData.start).add(hoursPerSlot, "hours").format("LLL")}</strong>
          </GridContainer>
        </section>

        <section style={{ maxHeight: '100%', overflowY: 'auto' }} className={scrollClass}>
          <Slots
            doctorId={this.props.doctorId}
            setDatetime={this._setDatetime}
          />
        </section>

        <section
          className={
            this.state.message.badMessage ? badMessageStyle : goodMessageStyle
          }
        >
          {this.state.message.goodMessage
            ? this.state.message.goodMessage
            : null}{" "}
          {this.state.message.badMessage ? this.state.message.badMessage : null}
        </section>
      </GridContainer>
    );
  }
}

const content = css({
  '& > span, & > strong': {
    textAlign: 'left',
  }
})

// styling
let goodMessageStyle = css({
  marginTop: 20,
  textAlign: "center",
  color: Colors.skyblue
});

let badMessageStyle = css(goodMessageStyle, {
  color: "red"
});

let infoStyle = css({
  flex: 1,
  display: "flex",
  padding: 50,
  flexWrap: "wrap",
  justifyContent: "flex-start",
  alignItems: "flex-start",
  background: "#e8f8ff"
});

let infoTitleContentStyle = css({
  flex: "1 0 45%",
  textAlign: "left"
});

let titleStyle = css({
  fontWeight: "bold",
  color: Colors.blueDarkAccent,
  textAlign: 'left !important',
  display: 'block'
});