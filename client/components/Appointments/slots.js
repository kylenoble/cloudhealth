import React from "react";
import { css } from "glamor";
import moment from "moment-timezone";
import AppointmentService from "../../utils/appointmentService";
import ScheduleService from "../../utils/schedulesService";
import Constants from "../../constants";
import Colors from "../NewSkin/Colors";
import { Preparing } from "../NewSkin/Components/Loading";
import { scrollClass, combined } from "../NewSkin/Styles";

const appointment = new AppointmentService();
const schedulesService = new ScheduleService();
const hoursPerSlot = Constants.HOURSPERSLOT;
const daysNotice = Constants.DAYSNOTICE;

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDate: "",
      availableSched: [],
      takenSlots: [],
      doctorId: "",
      appointmentRequest: [],
      m: moment(),
      loadingQueue: true
    };

    this._setDatetime = this._setDatetime.bind(this);
  }

  componentDidMount() {
    this._getSechedules();
    // const intervalId = setInterval(() => {
    //   console.log('get schedules')
    //   this._getSechedules();
    // }, 1000);

    // this.setState({ intervalId });
  }

  componentWillUnmount() {
    //clearInterval(this.state.intervalId);
  }

  // eslint-disable-next-line no-unused-vars
  componentWillReceiveProps(nextprops) {
    this._getSechedules();
  }

  _getSechedules() {
    const filter = {
      field: "start_datetime",
      condition: this.props.specificDate ? "equal" : "gte",
      value: this.props.specificDate ? moment(this.props.specificDate.toISOString()).format("YYYY-MM-D") : moment(
        moment()
          .add(daysNotice, "days")
          .toISOString()
      ).format("YYYY-MM-D")
    };
    const obj = { id: this.props.doctorId, filter };
    schedulesService
      .get(obj)
      .then(res => {
        this._setAvailableSched(res);
      })
      .catch(e => console.log(e));
  }

  _setAvailableSched(res) {
    // availableSched = []
    // if(res.length > 0){
    //     res.forEach((item, i)=>{
    //       availableSched.push(item)
    //     })
    // }

    this.setState(
      {
        availableSched: res
      },
      () => this._getAppointmentRequest()
    );
  }

  _getTakenSlots() {
    const appointmentRequest = this.state.appointmentRequest;
    let takenSlots = this.state.takenSlots;
    takenSlots = [];
    if (appointmentRequest.length > 0) {
      appointmentRequest.forEach(item => {
        if (
          item.doctor_id == this.props.doctorId &&
          (item.status === null || item.status === 1 || item.status === 2)
        ) {
          takenSlots.push(moment(item.datetime).format("LLL"));
        }
      });
    }
    this.setState({
      takenSlots,
      loadingQueue: false
    });
  }

  _getAppointmentRequest() {
    appointment
      .getAppointmentsList({ taken: true })
      .then(res => {
        if (res.name === "Error" || res.message === "Unauthorized") {
          this.setState({
            loadingQueue: false
          });
        } else {
          this.setState(
            {
              appointmentRequest: res
            },
            function () {
              this._getTakenSlots();
            }
          );
        }
      })
      .catch(e => {
        console.log("ERROR : ");
        console.log(e);
        this.setState({
          loadingQueue: false
        });
      });
  }

  _displayAvailableSchedule() {
    const availableSched = this.state.availableSched;
    let html = <div className={waiting}>No Available Slot</div>;

    if (availableSched.length > 0) {
      html = availableSched.map((item, i) => (
        <div className={`${slotDateStyle} slotDateStyle`} key={i}>
          <div className={slotDateTitle}>
            {moment(item.start_datetime).format("LL, dddd")}
          </div>
          {this._devide(item.start_datetime, item.end_datetime)}
        </div>
      ));
    }

    return html;
  }

  _devide(startTime, endTime) {
    const now = moment(startTime); //todays date
    const end = moment(endTime); // another date
    const duration = moment.duration(end.diff(now));
    const hours = duration.asHours();
    // eslint-disable-next-line radix
    const slots = parseInt(hours / hoursPerSlot);
    const html = [];
    const takenSlots = this.state.takenSlots;

    for (let i = 0; i < slots; i++) {
      const st = moment(startTime).add(i * hoursPerSlot, "hours");
      const selectedStyle =
        moment(this.state.selectedDate).format("MMDDYY hh:mm a") ===
        st.format("MMDDYY hh:mm a");
      const en = moment(st).add(hoursPerSlot, "hours");
      const locate = takenSlots.indexOf(st.format("LLL"));
      if (locate === -1) {
        html.push(
          <div
            key={i}
            className={selectedStyle ? SelecteddateSlotsStyle : dateSlotsStyle}
            onClick={() => {
              this._setDatetime(moment(st)._d);
            }}
          >
            {st.format("hh:mm a")} <br /> to <br />
            {en.format("hh:mm a")}
          </div>
        );
      } else {
        html.push(
          <div
            key={i}
            className={lockedDateSlotsStyle}
          >
            {st.format("hh:mm a")} <br /> to <br />
            {en.format("hh:mm a")}
          </div>
        );
      }
    }
    return <div className={slotDiv}>{html}</div>;
  }

  _setDatetime(dateTime) {
    //validate if slots still available

    this.setState({ selectedDate: dateTime });
    this._checkIfStillAvailable(moment(dateTime).toISOString())
      .then(res => {
        if (res) {
          this.props.setDatetime(dateTime);
          this._getSechedules();
        } else {
          // eslint-disable-next-line no-undef
          alert("Slot not Available");
        }
      })
      .catch(e => {
        console.log(e);
      });
    //this.props.setDatetime(dateTime)
  }

  _checkIfStillAvailable(dte) {
    const qry = { active: false, apmt_date: dte };

    return appointment
      .getAppointmentsList(qry)
      .then(res => {
        if (res && res.length > 0) {
          return Promise.resolve(false);
        }
        return Promise.resolve(true);
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    if (this.state.loadingQueue) {
      return <Preparing />;
    }
    return <div className={combined([slotsDiv, scrollClass])}>{this._displayAvailableSchedule()}</div>;
  }
}

let waiting = css({
  textAlign: "center",
  color: Colors.movementColor,
  marginTop: 10
});

let slotDiv = css({
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  marginTop: 10
  // padding: 10
});

let dateSlotsStyle = css({
  border: "1px solid #eee",
  borderRadius: 5,
  width: "auto",
  height: "auto",
  margin: 5,
  padding: 8,
  fontSize: ".8rem",
  letterSpacing: 0.3,
  lineHeight: 1,
  textAlign: "center",
  background: "white",
  color: "#777",
  cursor: "pointer",
  transition: "200ms ease",
  fontWeight: 500,
  boxShadow: "0 2px 2px rgba(0,0,0,.050)",
  ":hover": {
    boxShadow: "0 5px 10px rgba(0,0,0,.1)",
    borderColor: Colors.skyblue,
    color: Colors.skyblue
  }
});

let SelecteddateSlotsStyle = css(dateSlotsStyle, {
  background: Colors.skyblue,
  borderColor: Colors.skyblue,
  boxShadow: `0 5px 10px ${Colors.skyblue}60`,
  color: "white",
  ":hover": {
    boxShadow: `0 5px 10px ${Colors.skyblue}60`,
    borderColor: Colors.skyblue,
    color: "white"
  }
});

let lockedDateSlotsStyle = css(dateSlotsStyle, {
  background: "#eee",
  borderColor: "#eee",
  color: "#ccc",
  boxShadow: "none",
  cursor: 'not-allowed',
  ":hover": {
    borderColor: "#eee",
    color: "#ccc",
    boxShadow: "none"
  }
});

let slotDateStyle = css({
  // border: "1px solid #36c4f1",
  // margin: 5,
  borderRadius: 5,
  textAlign: "center"
  // boxShadow: '0 2px 7px rgba(0,0,0,.1)',
  // transition: '300ms ease',
  // ':hover': {
  //   boxShadow: '0 8px 15px rgba(0,0,0,.1)',
  // }
});

let slotDateTitle = css({
  background: Colors.skyblue,
  color: '#000',
  fontSize: "1rem",
  fontWeight: "600",
  width: "100%",
  padding: '3px 3px 3px 15px',
  borderRadius: 10,
  textAlign: 'left',
  textTransform: 'uppercase'
});

let slotsDiv = css({
  // background: "#e8f8ff",
  // border: "1px dotted",
  // padding: 5,
  // margin: 5,
  // borderRadius: 10,
  maxHeight: 380
  // width: 400
  // overflow: "auto"
});
