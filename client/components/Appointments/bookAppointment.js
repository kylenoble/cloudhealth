/* eslint-disable react/sort-comp */
/* eslint-disable eqeqeq */
/* eslint-disable no-undef */
/* eslint-disable no-param-reassign */
/* eslint-disable no-nested-ternary */
import React from "react";
import ReactTable from "react-table";
import { css } from "glamor";
import { confirmAlert } from "react-confirm-alert"; // Import
import Router from "next/router";
import moment from "moment-timezone";
import Modal from "react-modal";
import { tooltipWrapper } from '../Commons/tooltip';
import ReactTooltip from "react-tooltip";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import Form from "../Form/specialForm";
import InputMoment from "../calender/input-moment";
import currentDomain from "../../utils/domain";
import AppointmentService from "../../utils/appointmentService";
import ScheduleService from "../../utils/schedulesService";
import Slots from "./slots";
import Constants from "../../constants";
import VideoCall from "../VideoCall/index.js";
import BookAppNotifService from "../../utils/book_Appointment_Notification_Service";
import TimelineService from "../../utils/timelineService";
import ConcernService from "../../utils/concernService";
import Restricted from "../../components/restricted";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Card from "../NewSkin/Wrappers/Card";
import Colors from "../NewSkin/Colors";
import { inputClass, condensedTitle, scrollClass, font, ReactTableStyle, closeBtn, appointmentStatus } from "../NewSkin/Styles";
import { Preparing } from "../NewSkin/Components/Loading";
import Button from "../NewSkin/Components/Button";
import NoData from "../NewSkin/Components/NoData";

const bookAppNotifService = new BookAppNotifService();
const Appointment = new AppointmentService();
const schedulesService = new ScheduleService();
const hoursPerSlot = Constants.HOURSPERSLOT;
const daysNotice = Constants.DAYSNOTICE;
const timelineService = new TimelineService();
const Concern = new ConcernService();

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.domain = `${currentDomain}/images/`;
    this.state = {
      telemedicine_enabled: this.props.profile.telemedicine_enabled,
      modal: {
        videoConference: false
      },
      buttonText: "Add Appointment",
      appointmentRequest: "",
      appointment_detail: {},
      doctors: this.props.doctors,
      fileredDoctors: [],
      selectedDoctorIdByDate: [],
      profile: this.props.profile,
      selectedDoctor: "",
      doctorId: "",
      searchMode: false,
      m: moment() /*.add(daysNotice, 'days')*/,
      slotSelected: false,
      selectedDate: this.props.selectedDate,
      queuedPatients: [],
      loadingQueue: true,
      formError: "",
      formSuccess: "",
      searchString: "",
      concern: {
        id: "",
        name: "concern",
        label: "Concern",
        question: "Enter Concern",
        type: "autocomplete",
        validation: "required",
        data: [],
        options: [],
        error: {
          active: true,
          message: "Please enter concern"
        },
        value: ""
      },
      password: {
        id: "",
        name: "password",
        question: "Password",
        type: "password",
        validation: "password",
        options: [],
        error: {
          active: false,
          message:
            "Passwords must have at least 1 number, 1 Uppercase letter, 1 Lowercase letter, and a minimum of 8 characters"
        },
        value: ""
      },
      availableSched: [],
      takenSlots: [],
      cancelledAppDoctorEmail: "",
      selectedDoctorEmail: "",
      emptyTimeline: null,
      showTimeSlot: null,
      showContent: false
    };
    this._updateFields = this._updateFields.bind(this);
    this._goToNext = this._goToNext.bind(this);
    this._getAppointmentRequest = this._getAppointmentRequest.bind(this);
    this._selectDoctor = this._selectDoctor.bind(this);
    this._cancelAppointment = this._cancelAppointment.bind(this);
    this._checkIfStillAvailable = this._checkIfStillAvailable.bind(this);
    this._setDatetime = this._setDatetime.bind(this);
    this._dayToSearch = this._dayToSearch.bind(this);
    this._openRoom = this._openRoom.bind(this);
    this.modalSwitch = this.modalSwitch.bind(this);
    this._closeModal = this._closeModal.bind(this);
    this._checkTimelineData = this._checkTimelineData.bind(this);
    this._getDuration = this._getDuration.bind(this);

    this.focusAfterSlotSelect = React.createRef();
    this.appointmentsEnd = React.createRef();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      telemedicine_enabled: nextProps.profile.telemedicine_enabled
    });
  }

  componentWillMount() {
    const intervalId = setInterval(() => {
      this._getAppointmentRequest();
    }, 2000);

    this.setState({ intervalId });
  }

  componentDidMount() {
    this._checkTimelineData().then(r => {
      if (r.length > 0) {
        this.setState({ emptyTimeline: false }, () => {
          if (this.state.telemedicine_enabled === "Approved") {
            // show all available doctors
            this._setSelectedDoctorIdByDate(this.state.m);
            this._getAppointmentRequest(true);
            this._getConcernList();
            this._updateFields();
          }
        });
      } else {
        this.setState({ emptyTimeline: true });
      }
    });
  }

  componentWillUnmount() {
    // use intervalId from the state to clear the interval
    clearInterval(this.state.intervalId);
  }

  _getConcernList() {
    Concern.get()
      .then(result => {
        const conscernList = [];
        result.forEach(item => {
          conscernList.push(item.name);
        });
        const { concern } = this.state;
        concern.data = conscernList;
        this.setState({ concern });
      })
      .catch(e => {
        console.log(e);
      });
  }

  _updateView() {
    //this.props.updateView(view);
    Router.push("/healthprofile");
  }

  modalSwitch(name, bol) {
    const modal = this.state.modal;
    modal[name] = bol;
    this.setState({
      modal
    });
  }

  _checkTimelineData() {
    return timelineService
      .get(this.props.profile.id)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _cancelConfirmation(item) {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: "Please confirm appointment cancellation", // Message dialog
      buttons: [
        {
          label: "Yes, cancel my appointment",
          onClick: () => this._cancelAppointment(item)
        },
        {
          label: "I'm not sure"
        }
      ]
    });
  }

  // get selected doctor's email
  _getDoctorEmail = item => {
    this.state.doctors.forEach(doc => {
      if (item.doctor_id === doc.id) {
        this.setState({
          cancelledAppDoctorEmail: doc.email
        });
      }
    });

    this._sendCancelledAppointmentEmail(item);
  };

  // Appointment Cancelled by PX
  _sendCancelledAppointmentEmail = item => {
    const emailBodyForPatient = `
          <p style="font-size: 16px; margin: 0">Your appointment with the following details was cancelled. with the following details:</p>

          <p style="font-size: 16px; margin: 0px; color: #9e9e9e">Appointment date and time: <span style="color: #000; font-weight:600">${this.state.m.format(
      "lll"
    )}<span></p>`;

    const emailBodyForDoctor = `
          <p style="font-size: 16px; margin: 0">Appointment details:</p>
          <p style="font-size: 16px; margin: 0px; color: #9e9e9e">Appointment date and time: <span style="color: #000; font-weight:600">${this.state.m.format(
      "lll"
    )}<span></p>

          <p style="font-size: 16px; color: #9e9e9e; margin-top: 0px">Patient Name:
              <span style="color: #000; font-weight: 600">${
      this.state.profile.name
      }</span>
          </p>`;

    let emailDetails = "";
    const title = "Cancelled Appointment";

    const emailDetailsForPatient = {
      recipient: this.state.profile.email,
      date: moment(new Date()).format("lll"),
      title,
      body: emailBodyForPatient
    };

    const emailDetailsForDoctor = {
      recipient: this.state.cancelledAppDoctorEmail,
      date: moment(new Date()).format("lll"),
      title,
      body: emailBodyForDoctor
    };

    this._sendCancelledAppNotification(item);

    emailDetails = emailDetailsForPatient;
    this._emailSetup(emailDetails);

    setTimeout(() => {
      emailDetails = emailDetailsForDoctor;
      this._emailSetup(emailDetails);
    }, 2000);
  };

  // send System Notification for new Booked Appointment
  _sendCancelledAppNotification = item => {
    console.log("cancelleed appointments");
    const sender = {
      type: "Patient",
      // name: this.state.profile.name,
      name: "System",
      id: this.state.profile.id
    };

    const doctor = {
      type: "Doctor",
      id: item.doctor_id,
      name: item.name,
      appointmentDate: item.datetime,
      patientName: this.state.profile.name
    };

    const patient = {
      type: "Patient",
      name: this.state.profile.name,
      id: this.state.profile.id,
      appointmentDate: item.datetime,
      patientName: this.state.profile.name
    };

    const message = item.concern;

    const subject = "Cancelled Appointment";

    const notificationDetails = { sender, doctor, patient, message, subject };

    this._sendSystemNotification(notificationDetails);
  };

  _cancelAppointment(item) {
    const data = {
      //date_rescheduled: moment()._d,
      //datetime :  moment(time)._d,
      status: 3,
      active: true
    };

    Appointment.update(item.id, data)
      .then(() => {
        this._getDoctorEmail(item);
        this._getAppointmentRequest();
      })
      .catch(e => {
        console.log(e);
      });
  }

  _getAppointmentRequest(initial = false) {
    Appointment.getAppointmentsList()
      .then(res => {
        if (res.name === "Error" || res.message === "Unauthorized") {
          this.setState({
            loadingQueue: false
          });
        } else {
          this.setState({
            appointmentRequest: res,
            loadingQueue: false,
            formError: {
              active: false,
              message: ""
            }
          }, () => {
            if (initial) {
              if (this.props.appointmentRequestsFocus && this.appointmentsEnd.current) {
                this.appointmentsEnd.current.scrollIntoView({ behavior: "smooth" });
              }
            }
          });
        }
      })
      .catch(e => {
        console.log("ERROR : ");
        console.log(e);
        this.setState({
          loadingQueue: false
        });
      });
  }

  _selectDoctor(e, doctorId, selectedDoctor, email) {
    // Styling avatar wrapper
    const fig = document.querySelectorAll(`.${avatarWrapper}`);
    fig.forEach(f => {
      f.style.border = "1px solid #fafafa";
      f.style.boxShadow = "0 1px 5px rgba(0,0,0,0)";
    });
    if (e.target.tagName == "IMG") {
      const img = e.target;
      img.parentNode.style.border = "1px solid #1b75bb";
      img.parentNode.style.boxShadow = "0 8px 15px rgba(0,0,0,.1)";
    } else {
      e.target.style.border = "1px solid #1b75bb";
      e.target.style.boxShadow = "0 8px 15px rgba(0,0,0,.1)";
    }

    this.setState({
      slotSelected: false,
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      },
      selectedDoctorEmail: email,
      buttonText: "Add Appointment"
    });

    this.setState(
      {
        selectedDoctor,
        doctorId,
        showTimeSlot: true
      },
      () => this._getAppointmentRequest()
    );
  }

  _handleSearchString(e) {
    let { searchString } = this.state;
    searchString = e.target.value;
    this.setState({ searchString }, () => {
      this._searchDoctorByName();
    });
  }

  _searchDoctorByName() {
    const { searchString } = this.state;
    const doctors = this.state.doctors;
    let fileredDoctors = this.state.fileredDoctors;
    let searchMode = this.state.searchMode;
    fileredDoctors = [];

    fileredDoctors = doctors.filter(
      d => d.name.toLowerCase().indexOf(searchString.toLowerCase()) > -1
    );

    if (searchString) {
      searchMode = true;
    } else {
      searchMode = false;
    }

    this.setState({ searchMode, fileredDoctors, loadingQueue: false });
  }

  _renderDoctorImg = src => {
    const img = document.createElement("img");
    img.src = `/static/avatar/${src}`;
    if (img.complete) {
      console.log(
        "%c changing src!",
        "padding: 5px; background: green; color: white;"
      );
      return `/static/avatar/${src}`;
    }
    return "/static/icons/user.svg";
  };

  _viewDoctors() {
    const selectedDoctorIdByDate = this.state.selectedDoctorIdByDate;
    const fileredDoctors = this.state.fileredDoctors;
    let html = [];

    if (this.state.searchMode) {
      if (fileredDoctors.length > 0) {
        html = fileredDoctors.map((item, i) => (
          <div className={`${doctorListStyle}`} key={i}>
            <GridContainer
              columnSize="2fr 1fr"
              gap={20}
              styles={{ width: "100% !important" }}
            >
              <GridContainer columnSize="110px 1fr" gap={10}>
                <figure
                  className={`${avatarWrapper}`}
                // onClick={e => this._selectDoctor(e, item.id, item.name)}
                >
                  <img
                    alt={"user"}
                    className={avatarStyle}
                    src={
                      item.avatar
                        ? this._renderDoctorImg(item.avatar)
                        : "/static/icons/user.svg"
                    }
                  />
                </figure>

                <section className={mdDetailsWrapper}>
                  <span className={font(1)}>
                    <strong>{item.name}</strong>
                  </span>
                  <span style={{ lineHeight: 1.4 }}>{item.company}</span>
                  <span style={{ lineHeight: 1.4 }}>{item.expertise}</span>
                </section>
              </GridContainer>

              <Button
                type="blue"
                styles={{
                  maxWidth: 150,
                  minWidth: "fit-content",
                  margin: "auto auto"
                }}
              >
                <span
                  style={{ cursor: "pointer" }}
                  onClick={e => this._selectDoctor(e, item.id, item.name)}
                >
                  View Schedule
                </span>
              </Button>
            </GridContainer>
          </div>
        ));
      } else {
        html = (
          <div className={queueItemSubDivStyle}>
            <div className={noDoctorStyle}>No Doctors Available</div>
          </div>
        );
      }
    } else if (this.state.doctors.length > 0) {
      let ctr = 0;
      html = this.state.doctors.map((item, i) => {
        let htmlDiv = "";
        if (selectedDoctorIdByDate.indexOf(item.id) !== -1) {
          ctr++;
          htmlDiv = (
            <div className={`${doctorListStyle}`} key={i}>
              <GridContainer
                columnSize="2fr 1fr"
                gap={20}
                styles={{ width: "100% !important" }}
              >
                <GridContainer columnSize="110px 1fr" gap={10}>
                  <figure
                    className={`${avatarWrapper}`}
                  // onClick={e => this._selectDoctor(e, item.id, item.name)}
                  >
                    <img
                      alt={"avatar"}
                      className={avatarStyle}
                      src={
                        item.avatar
                          ? this._renderDoctorImg(item.avatar)
                          : "/static/icons/user.svg"
                      }
                    />
                  </figure>
                  <section className={mdDetailsWrapper}>
                    <span className={font(1)}>
                      <strong>{item.name}</strong>
                    </span>
                    <span style={{ lineHeight: 1.4 }}>{item.company}</span>
                    <span style={{ lineHeight: 1.4 }}>{item.expertise}</span>
                  </section>
                </GridContainer>

                <Button
                  type="blue"
                  styles={{
                    maxWidth: 150,
                    minWidth: "fit-content",
                    margin: "auto auto"
                  }}
                >
                  <span
                    style={{ cursor: "pointer" }}
                    onClick={e => this._selectDoctor(e, item.id, item.name)}
                  >
                    View Schedule
                  </span>
                </Button>
              </GridContainer>
            </div>
          );
        }

        return htmlDiv;
      });
      if (ctr == 0) {
        html = (
          <div className={`${queueItemSubDivStyle}`}>
            <div className={noDoctorStyle}>No Doctors Available</div>
          </div>
        );
      }
    } else {
      html = (
        <div className={`${queueItemSubDivStyle}`}>
          <div className={noDoctorStyle}>No Doctors Available</div>
        </div>
      );
    }
    return (
      <div className={doctorsMainDiv}>
        <div className={searchDiv}>
          <input
            className={inputClass}
            type="text"
            id="searchField"
            placeholder="Name of Doctor"
            value={this.state.searchString}
            onChange={this._handleSearchString.bind(this)}
          />
        </div>
        <GridContainer
          gap={10}
          styles={{
            width: "100%",
            minHeight: 380,
            maxHeight: 400,
            overflow: "auto",
            alignContent: "start"
          }}
          columnSize={"1fr"}
          classes={[scrollClass]}
        >
          {html}
        </GridContainer>
      </div>
    );
  }

  _daysLeft(ad) {
    const a = moment(ad);
    const b = moment();
    return a.diff(b, "days");
  }

  _openRoom(item) {
    this.setState({ consultMode: true, appointment_detail: item }, () => {
      this.modalSwitch("videoConference", true);
    });
    // this.setState({consultId: item.id}, ()=>{
    //   this.modalSwitch('conference', true)
    // })
  }

  _viewRequestTable() {
    let columns = [];
    const filtered_data = [];
    const data = this.state.appointmentRequest;
    const tooltipCancel = { description: "Cancellation of appointments can only be done more than 3 days from the chosen date. Otherwise, you will be charged accordingly" }
    const tooltipCantCall = { description: "Available only during Consultation date. (Note: Temporary enabled for testing)" }

    if (data.length > 0) {
      data.forEach(item => {
        const edate = moment(item.datetime).add(hoursPerSlot, "hours");

        if (item.time_extension && item.time_extension > 0) {
          edate.add(item.time_extension, "minutes");
        }

        item.canCall =
          moment().toISOString() >= moment(item.datetime).toISOString() &&
          moment().toISOString() < edate.toISOString();
        item.lapsed = edate.toISOString() < moment().toISOString();

        //filtered data
        filtered_data.push(item);
      }, this);
    }

    if (filtered_data.length > 0) {
      columns = [
        {
          Header: "DATE AND TIME",
          accessor: "datetime",
          Cell: row => <span>{moment(row.value).format("MMM D hh:mma")}</span>
        },
        {
          Header: "DOCTOR",
          accessor: "name",
          style: { textAlign: "center" }
        },
        {
          Header: "CONCERN",
          accessor: "concern",
          style: { whiteSpace: "unset" }
        },
        {
          Header: "STATUS",
          accessor: "status",
          Cell: row => (
            <span
              style={!row.original.active ? appointmentStatus.pending :
                row.value === null ? appointmentStatus.accepted :
                  row.value === 1 ? appointmentStatus.completed :
                    row.value === 2 ? appointmentStatus.rescheduled :
                      appointmentStatus.cancelled
              }
            >
              {row.original.active
                ? row.value === null
                  ? "Accepted"
                  : row.value === 1
                    ? "Completed"
                    : row.value === 2
                      ? "Rescheduled"
                      : "Cancelled"
                : "Pending for MD Approval"}
            </span>
          ),
          style: { textAlign: "center" }
        },
        {
          Header: "ACTION",
          accessor: "active",
          Cell: row => (
            <div>
              <ReactTooltip />
              {row.value ? (
                <div>
                  {this._daysLeft(row.original.datetime) >= 3 ? (
                    <div>
                      {row.original.status === 3 ||
                        row.original.status === 2 ||
                        row.original.status === 1 ? null : (
                          <span
                            data-type="light"
                            data-html
                            data-tip={tooltipWrapper(tooltipCancel)}
                            className={cancelBtn}
                            onClick={() => this._cancelConfirmation(row.original)}
                          >
                            Cancel
                        </span>
                        )}
                    </div>
                  ) : row.original.status == null ? (
                    row.original.lapsed ? (
                      <span className={warning}>LAPSED</span>
                    ) : (
                        <span
                          data-type="light"
                          data-html
                          data-tip={
                            !row.original.canCall ? tooltipWrapper(tooltipCantCall) : null
                          }
                          className={row.original.canCall ? accptbtn : lockedbtn}
                          onClick={
                            row.original.canCall
                              ? () => this._openRoom(row.original)
                              : null
                          }
                        >
                          Open Conference
                      </span>
                      )
                  ) : null}
                </div>
              ) : row.original.status === 3 ||
                row.original.status === 1 ? null : (
                    <span
                      data-type="light"
                      data-html
                      data-tip={tooltipWrapper(tooltipCancel)}
                      className={cancelBtn}
                      onClick={() => this._cancelConfirmation(row.original)}
                    >
                      Cancel
                </span>
                  )}
            </div>
          ),
          style: { textAlign: "center", whiteSpace: "unset" },
          width: 150
        }
      ];

      return (
        <div className={`${queueItemDivStyle} ${ReactTableStyle}`}>
          <ReactTable
            data={filtered_data}
            columns={columns}
            defaultPageSize={10}
            minRows={1}
            noDataText="No booked appointment"
            className={`-striped -highlight ${reactTable}`}
            width={100}
            getTdProps={() => ({
              onClick: (e, handleOriginal) => {
                if (handleOriginal) {
                  handleOriginal();
                }
              }
            })}
          />
        </div>
      );
    }
    return <NoData text="No booked appointment(s)." />
  }

  _clearFields() {
    const currentFields = [this.state.concern, this.state.password];
    const fields = [];
    for (let i = 0; i <= currentFields.length - 1; i++) {
      const input = currentFields[i];
      input.value = "";
      fields.push(input);
    }
    this.setState({
      fields,
      showTimeSlot: false
    });
  }

  _updateFields() {
    const currentFields = [this.state.concern, this.state.password];
    const fields = [];
    for (let i = 0; i <= currentFields.length - 1; i++) {
      const input = currentFields[i];
      fields.push(input);
    }
    this.setState({
      fields
    });
  }

  _updateRequestView() {
    this.setState({
      appointmentRequest: this.props.appointmentRequest
    });
  }

  _goToNext() {
    this.setState({
      formError: {
        active: false,
        message: ""
      },
      buttonText: "Please Wait..."
    });

    let inputs;
    if (this.state.password.value !== "" && this.state.concern.value !== "") {
      inputs = {
        NoPassword: false,
        password: this.state.password.value,
        name: this.state.profile.name,
        email: this.state.profile.email,
        date: this.state.m._d,
        doctorId: this.state.doctorId,
        concern: this.state.concern.value,
        date_requested: moment()._d,
        patient_company_id: this.props.profile.company_id,
        appointment_code: new Date().getTime(),
        parent_appointment_code: null,
        active: false
      };
    } else {
      console.log("ERROR: PASWORD and CONCERN MUST NOT EMPTY");
      this.setState({
        formError: {
          active: true,
          message: "PASWORD and CONCERN MUST NOT EMPTY"
        },
        buttonText: "Try Again"
      });
      return;
    }

    // check if slot is still availableSched
    this._checkIfStillAvailable(moment(inputs.date).toISOString())
      .then(res => {
        if (res) {
          this._faildCreate();
        } else {
          this._create(inputs);
        }
      })
      .catch(e => console.log(e));
  }

  _faildCreate() {
    this._clearFields();
    this._updateFields();
    this.setState({
      formError: {
        active: true,
        message: "Slot not Available!"
      },
      buttonText: "Try Again"
    });
  }

  // send System Notification for new Booked Appointment
  _sendBookAppNotification = input => {

    const sender = {
      type: "Patient",
      // name: input.name,
      name: "System",
      id: this.state.profile.id
    };

    const doctor = {
      type: "Doctor",
      id: this.state.doctorId,
      name: this.state.selectedDoctor,
      appointmentDate: input.date,
      patientName: input.name
    };

    const patient = {
      type: "Patient",
      id: this.state.profile.id,
      name: input.name,
      appointmentDate: input.date,
      patientName: input.name
    };

    const message = input.concern;

    const subject = "New Booked Appointment";

    const notificationDetails = { sender, doctor, patient, message, subject };

    this._sendSystemNotification(notificationDetails);
    this._sendBookAppEmail();
  };

  _sendSystemNotification = notificationDetails => {

    bookAppNotifService
      .create(notificationDetails)
      .then(res => {
        console.log("sent", res);
      })
      .catch(err => {
        console.log(err);
      });
  };

  // send Email Notification for new booked Appointment
  _sendBookAppEmail = () => {
    const date = moment(new Date()).format("lll");

    const recipients = {
      patient: this.state.profile.email,
      doctor: this.state.selectedDoctorEmail
    };

    const title = "New Booked Appointment";

    const notificationBodyForDoctor = `
        <p style="font-size: 16px; margin: 0px; color: #9e9e9e">Appointment date and time: <span style="color: #000; font-weight:600">${this.state.m.format(
      "lll"
    )}<span></p>
        <p style="font-size: 16px; color: #9e9e9e; margin-top: 0px">Patient Name:
            <span style="color: #000; font-weight: 600">${
      this.state.profile.name
      }</span>
        </p>
        <p style="font-size: 16px; margin: 0">An appointment has been booked. To view the appointment details, go to
            <span style="color: #36c4f1; font-weight: 600">My Schedule > Appointment Request</span>
        </p>`;

    const notificationBodyForPatient = `
        <p style="font-size: 16px; margin: 0">You have successfully booked an appointment. To view the appointment details, go to
            <span style="color: #36c4f1; font-weight:600">My Health Activities > Book Appointment > Appointment Request</span>
        </p>`;

    let emailDetails = "";

    const emailDetailsForDoctor = {
      recipient: recipients.doctor,
      date,
      title,
      body: notificationBodyForDoctor
    };

    const emailDetailsForPatient = {
      recipient: recipients.patient,
      date,
      title,
      body: notificationBodyForPatient
    };

    // sending email notification to MD
    emailDetails = emailDetailsForDoctor;
    this._emailSetup(emailDetails);

    // sending email notification to PX
    setTimeout(() => {
      emailDetails = emailDetailsForPatient;
      this._emailSetup(emailDetails);
    }, 2000);
  };

  _emailSetup = email => {
    const subject = "New Notification from CloudHealthAsia";
    const text = "This is the notification of your Appointment";
    const to = email.recipient; // recipient
    const date = email.date; // notification date
    const notificationTitle = email.title; // notification title ex. Cancelled Appointment
    const notificationBody = email.body; // notification body ex. <span style="">Message here</span>

    const content = `
            <p style="font-size: 16px;">Please see below notification for your account:</p>

            <div style="border: 1px solid #ccc; padding: 20px; border-radius: 5px; background: #eee1; box-shadow: 0 8px 32px rgba(0,0,0,.050)">
                <p style="font-size: 16px; color: #9e9e9e; margin: 0">${date}</p>

                <h5 style="font-size: 1.64rem; font-weight: 400; margin: 0.82rem 0 0.656rem 0">${notificationTitle}</h5>

                ${notificationBody}
            </div>

            <p style="font-size: 16px">Please log-in to your account for further details.</p>
            <a title="Login to CloudHealthAsia" href="https://cloudhealthasia.com/login" target="_blank" style="text-decoration: none; padding: 10px 20px; background: #81cce6; color: #fff; border-radius: 5px; margin-top: 20px; display: block; width: 200px; font-weight: 500;">Login to CloudHealthAsia</a>
        `;

    const emailDetails = { to, content, subject, text };

    this._sendEmailNotification(emailDetails);
  };

  _sendEmailNotification = emailDetails => {
    bookAppNotifService
      .sendBookAppEmailNotification(emailDetails)
      .then(() => {
        console.log("sending email notification success!");
      })
      .catch(err => {
        console.log(err);
      });
  };

  _create(inputs) {
    Appointment.create(inputs)
      .then(res => {
        if (res) {
          this._sendBookAppNotification(inputs);
          this.setState({
            formSuccess: {
              active: true,
              message: "Appointment Added"
            },
            slotSelected: false
            // buttonText: "Add Another Appointment"
          });
          //this._setAppointment(inputs)
          this._clearFields();
          this._getAppointmentRequest();
          this._updateFields();
        }
      })
      .catch(e => {
        this.setState({
          formError: {
            active: true,
            message: e.error || "Invalid Inputs!"
          },
          buttonText: "Try Again"
        });
      });
  }

  _checkIfStillAvailable(dte) {
    const qry = { active: false, apmt_date: dte };

    return Appointment.getAppointmentsList(qry)
      .then(res => {
        if (res && res.length > 0) {
          return Promise.resolve(true);
        }
        return Promise.resolve(false);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _handleChange(value, input, validated) {
    this.setState({
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      }
    });
    const inputName = input;
    const newInput = this.state[inputName];
    newInput.value = value;
    newInput.error.active = validated;
    this.setState({
      inputName: newInput
    });
    this._updateFields();
  }

  _getDuration(durtn) {
    let duration = this.state.duration;
    duration = durtn;
    this.setState({ duration });
  }

  _booking() {
    if (
      this.state.m.format("Y-MM-DD") &&
      this.state.selectedDoctor &&
      this.state.slotSelected
    ) {
      return (
        <div>
          <div className={`${details}`}>
            <p>
              You are booking an appointment on{" "}
              <strong style={{ color: Colors.skyblue }}>
                {this.state.m.format("Y-MM-DD hh:mma")}
              </strong>{" "}
              with Dr.{" "}
              <strong style={{ color: Colors.skyblue }}>
                {this.state.selectedDoctor}
              </strong>
            </p>
            <p style={{ margin: "10px 0 20px" }}>
              Please select your health concern/reason for consult and enter
              your password to confirm your appointment. For health concerns not listed, please select
              <span style={{ fontStyle: 'italic', fontWeight: 600 }}> Others</span> and input your specific concern. (E.g. Heartburn)
            </p>
          </div>
          <Form
            appointment
            submitForm={this._goToNext}
            buttonText={this.state.buttonText}
            handleChange={this._handleChange.bind(this)}
            inputs={this.state.fields}
            formError={this.state.formError}
            formSuccess={this.state.formSuccess}
            location={this.props.location}
            user={this.props.profile}
          />
        </div>
      );
    }
    return (
      <div style={{ color: Colors.movementColor }}>
        Please choose your doctor and your preferred date and time
      </div>
    );
  }

  _dayToSearch(dte) {
    const fromNow = moment().add(daysNotice, "days");
    if (dte.diff(fromNow, "days") >= 0) {
      return dte._d;
    }
    return false;
  }

  _setSelectedDoctorIdByDate = m => {
    this._clearFields();
    let doctorId = this.state.doctorId;
    doctorId = "";
    const dayToSeach = this._dayToSearch(m);
    const obj = {
      strDate: moment(dayToSeach).format("YYYY-MM-D"),
      endDate: moment(dayToSeach).format("YYYY-MM-D")
    };

    schedulesService
      .get(obj)
      .then(res => {
        this._saveSelectedDoctorsIdByDate(res);
      })
      .catch(e => console.log(e));

    this.setState(
      {
        m,
        doctorId,
        searchMode: false
      },
      () => {
        // clear serach field
        // eslint-disable-next-line no-undef
        if (document.getElementById("searchField")) {
          document.getElementById("searchField").value = "";
        }
      }
    );
  };

  _saveSelectedDoctorsIdByDate(res) {
    let selectedDoctorIdByDate = this.state.selectedDoctorIdByDate;
    selectedDoctorIdByDate = [];

    if (res && res.length > 0) {
      res.forEach(item => {
        selectedDoctorIdByDate.push(item.user_id);
      });
    }

    this.setState({ selectedDoctorIdByDate, showContent: true });
  }

  _setDatetime(time) {
    this._clearFields();
    this.setState({
      m: moment(time),
      slotSelected: true
    });
    // scrolls user into appointment details section
    //.current is verification that your element has rendered
    if (this.focusAfterSlotSelect.current) {
      this.focusAfterSlotSelect.current.scrollIntoView({
        behavior: "smooth",
        block: "nearest"
      });
    }
  }

  findObjectByKey(array, key, value) {
    for (let i = 0; i < array.length; i++) {
      if (array[i][key] === value) {
        return array[i];
      }
    }
    return null;
  }

  // _displayAvailableSchedule(){
  //   let availableSched = this.state.availableSched
  // //  let takenSlots = this.state.takenSlots
  //   let html = availableSched.map((item, i)=>{
  //     return (<div className={slotDateStyle} key={i}>
  //         <div className={slotDateTitle}>{moment(item.start_datetime).format('LL')} - {moment(item.end_datetime).format('LL')}</div>
  //         {this._devide(item.start_datetime, item.end_datetime)}
  //       </div>)
  //   })
  //   return (<div>{html}</div>)
  // }

  // _devide(startTime, endTime, locate){
  //     var now = moment(startTime); //todays date
  //     var end = moment(endTime); // another date
  //     var duration = moment.duration(end.diff(now));
  //     var hours = duration.asHours();
  //     let slots = parseInt(hours/hoursPerSlot)
  //     let html = []
  //     let takenSlots =  this.state.takenSlots
  //
  //     for(let i = 0; i < slots; i++){
  //       let st = moment(startTime).add(i*hoursPerSlot, 'hours')
  //       let en = moment(st).add(hoursPerSlot, 'hours')
  //       var locate = takenSlots.indexOf(st.format('LLL'))
  //       if(locate === -1 ){
  //         html.push(<div key={i} className={dateSlotsStyle} onClick={()=>{this._setDatetime(st)}}>{st.format('hh:mm a')} <br/> to <br/> {en.format('hh:mm a')}</div>)
  //       }else{
  //         html.push(<div key={i} className={lockedDateSlotsStyle}>{st.format('hh:mm a')} <br/> to <br/> {en.format('hh:mm a')}</div>)
  //       }
  //
  //     }
  //     return(
  //       <div className={slotDiv}>{html}</div>
  //     )
  // }

  _closeModal() {
    this.modalSwitch("videoConference", false);
  }

  render() {
    if (this.state.loadingQueue || emptyTimeline === null) {
      return <Preparing />;
    }
    const { telemedicine_enabled, emptyTimeline } = this.state;

    if (telemedicine_enabled !== "Approved") {
      const restrict_msg = `This feature is not included in your current subscription. Please contact the officer in-charge of your account if you wish to subscribe`;
      return (
        <Card>
          <Restricted message={restrict_msg} />
        </Card>
      );
    }

    if (emptyTimeline === true) {
      const restrict_msg = `Before you can book an appointment, you need to accomplish your health timeline. <br /><span style="display: inline">Go to </span><span class="breadcrumbs">My Health Profile &#8594; Summary and Analysis &#8594; Health Timeline</span>`;
      return (
        <Card>
          <Restricted message={restrict_msg} html />;
        </Card>
      );
    }

    return (
      <div>
        <Modal
          isOpen={this.state.modal.videoConference}
          onRequestClose={() => this.modalSwitch("videoConference", false)}
          shouldCloseOnOverlayClick={false}
          style={customStyles}
        >
          <div className={colapseableDiv} >
            <MuiThemeProvider>

              <VideoCall
                user={this.props.profile}
                getDuration={this._getDuration}
                queuedPatients={this.state.appointmentRequest}
                appointment_detail={this.state.appointment_detail}
              />

            </MuiThemeProvider>
            <Button
              styles={{ minWidth: "unset", width: 180, margin: '5px auto' }}
            >
              <button title="Close" onClick={this._closeModal}>
                Close
              </button>
            </Button>
          </div>
        </Modal>

        <GridContainer styles={{ width: "100%", marginBottom: 20 }}>
          <Card>
            <div
              style={{
                fontSize: 15,
                textAlign: "left",
                padding: "0 20px",
                borderLeft: `2px solid ${Colors.movementColor}`
              }}
            >
              <strong style={{ fontSize: 18, color: Colors.movementColor }}>
                Booking an Appointment
              </strong>

              <ol
                style={{
                  lineHeight: "20px",
                  textAlign: "justify",
                  color: "#777",
                  marginLeft: 20
                }}
              >
                <li>Select your preferred date from the calendar. </li>
                <li>Select a doctor from the available list.</li>
                <li>
                  Then, choose your desired slot from the doctor's schedule.
                </li>
                <li>You can also search for specific doctor.</li>
              </ol>
            </div>
          </Card>
        </GridContainer>

        <GridContainer columnSize={"1fr 1fr"} gap={20}>
          <GridContainer
            dataID="calendar-wrapper"
            styles={{ alignContent: "start" }}
          >
            <Card>
              <div className={condensedTitle}>CHOOSE PREFERRED DATE</div>
              <div className={calenderStyle}>
                <InputMoment
                  moment={this.state.m}
                  onChange={this._setSelectedDoctorIdByDate}
                  minStep={5}
                  //onSave={this.handleSave}
                  dateTimeBtn={false}
                />
              </div>
            </Card>
          </GridContainer>

          {this.state.showTimeSlot === null ||
            this.state.showTimeSlot === false ? (
              <GridContainer styles={{ alignContent: "start" }}>
                <Card styles={{ minHeight: "-webkit-fill-available" }}>
                  <div style={{ padding: 0 }} className={contentDivStyle}>
                    <div className={condensedTitle}>CHOOSE YOUR DOCTOR</div>
                    {!this.state.showContent ? (
                      <Preparing />
                    ) : (
                        this._viewDoctors()
                      )}
                  </div>
                </Card>
              </GridContainer>
            ) : (
              <GridContainer styles={{ alignContent: "start" }}>
                <Card styles={{ minHeight: "-webkit-fill-available" }}>
                  <div className={condensedTitle}>
                    SELECT YOUR PREFERRED TIME
                  <Button
                      // type="pink"
                      classes={[closeBtn]}
                      styles={{ minWidth: "unset" }}
                    >
                      <button
                        title="Close"
                        onClick={() => this.setState({ showTimeSlot: false })}
                      >
                        X
                    </button>
                    </Button>
                  </div>
                  <GridContainer
                    gap={10}
                    styles={{
                      width: "100%",
                      maxHeight: 400,
                      overflow: "auto",
                      alignContent: "start"
                    }}
                  >
                    <Slots
                      doctorId={this.state.doctorId}
                      setDatetime={this._setDatetime}
                      specificDate={this.state.m}
                    />
                  </GridContainer>
                </Card>
              </GridContainer>
            )}
        </GridContainer>

        <GridContainer styles={{ width: "100%", marginTop: 20 }}>
          <Card>
            <div
              className={contentDivStyle}
              style={{ borderBottom: "none", paddingTop: 0 }}
            >
              <div className={condensedTitle}>APPOINTMENT DETAILS</div>
              {this._booking()}
            </div>
          </Card>
        </GridContainer>

        <GridContainer styles={{ marginTop: 20, width: "100%" }}>
          <Card>
            <div className={`${queueItemDivStyle} queueItemDivStyle`}>
              <div className={labelDivStyle}>
                <div className={condensedTitle} ref={this.focusAfterSlotSelect}>
                  {" "}
                  APPOINTMENT REQUEST(S)
                </div>
              </div>
              {this._viewRequestTable()}
            </div>
          </Card>
        </GridContainer>
        <div ref={this.appointmentsEnd}></div>
      </div>
    );
  }
}

// styling

const mdDetailsWrapper = css({
  fontSize: 14,
  display: "grid",
  alignContent: "center",
  justifyContent: "left",
  textAlign: "left !important"
});

const noDoctorStyle = css({
  color: Colors.movementColor,
  marginTop: 20
});

let details = css({
  marginTop: 10
});

let avatarWrapper = css({
  margin: "0 auto",
  background: "#fcfcfc",
  width: 100,
  height: 100,
  overflow: "hidden",
  transition: "250ms ease",
  border: "1px solid #fafafa",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  borderRadius: "100%",
  boxShadow: "0 2px 5px rgba(0,0,0,.1)",
  "> img": {
    width: "100%"
  }
});

let reactTable = css({
  "> div": {
    // rt-table
    "> div:nth-child(2)": {
      // rt-body
      "> div:nth-child(2n+1)": {
        background: "#fafafa !important"
      },
      "> div": {
        // rt tr-group
        borderBottom: "none !important",
        "> div": {
          // rt tr
          display: "flex",
          alignItems: "center",
          background: "none"
        }
      }
    }
  }
});

let actionBtn = css({
  borderRadius: 5,
  border: `2px solid ${Colors.compromised}`,
  fontSize: 12,
  fontWeight: 500,
  display: 'block',
  width: '100% !important'
})

let cancelBtn = css(actionBtn, {
  backgroundColor: "white",
  color: Colors.compromised,
  cursor: "pointer",
  ":hover": {
    backgroundColor: Colors.compromised,
    color: "white"
  }
})

let accptbtn = css(actionBtn, {
  backgroundColor: "white",
  borderColor: Colors.skyblue,
  color: Colors.skyblue,
  cursor: "pointer",
  ":hover": {
    backgroundColor: Colors.skyblue,
    color: "white"
  }
});

let warning = css(actionBtn, {
  backgroundColor: "white",
  color: Colors.nutritionColor,
  border: `1px dotted ${Colors.nutritionColor}`,
  borderRadius: 5
});

let lockedbtn = css(actionBtn, {
  backgroundColor: Colors.disabled,
  color: "#fff",
  borderColor: Colors.disabled
});

let avatarStyle = css({
  marginRight: "auto",
  marginLeft: "auto",
  align: "center",
  verticalAlign: "center",
  "@media(max-width: 767px)": {
    height: "75px",
    width: "75px"
  }
});

let calenderStyle = css({
  // flex: "1 0 100%",
  // display: "flex",
  // alignItems: "flex-start"
  display: "grid",
  width: "100%",
  placeContent: "center",
  "> div.m-input-moment": {
    border: "none",
    "> div.tabs": {
      "> div": {
        height: "fit-content",
        "> table": {
          "> thead": {
            "> tr": {
              "> td": {
                border: "none"
              }
            }
          },
          "> tbody": {
            "> tr": {
              "> td": {
                transition: "250ms ease",
                height: 54,
                width: 54,
                borderRadius: "100%",
                border: "none",
                ":hover": {
                  background: "#ccc3",
                  color: Colors.skyblue
                }
              }
            }
          }
        }
      }
    }
  }
});

let queueItemDivStyle = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "space-around",
  maxWidth: '100%'
});

let contentDivStyle = css({
  height: "fit-content",
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  padding: "20px 0",
  ":hover": {
    // boxShadow: "0 8px 15px rgba(0,0,0,.1)"
  }
});

let labelDivStyle = css({
  //display: 'flex',
  flexWrap: "wrap",
  flex: "1 0 100%",
  flexContent: "flex-start"
  //margin: '20px 0 30px 5px',
  // padding: "5px"
});

let ItemDivStyle = css({
  display: "flex",
  margin: "5px",
  marginTop: 15,
  flex: "1 0 100%",
  justifyContent: "space-around"
});

let doctorListStyle = css({
  display: "flex",
  margin: "5px",
  marginTop: 15,
  justifyContent: "space-around"
});

let queueItemSubDivStyle = css({
  display: "flex",
  flex: "1 0 98%",
  flexWrap: "wrap",
  margin: "0",
  width: "100%",
  justifyContent: "space-around"
});

let doctorsMainDiv = css({
  display: "flex",
  flexWrap: "wrap",
  width: "100%"
});

let searchDiv = css({
  width: "100%",
  // padding: 5
  marginTop: 10
});

const customStyles = {
  content: {
    textAlign: "center",
    zIndex: 6,
    width: "30%",
    height: "auto",
    top: "55%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    // marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: 10,
    boxShadow: "-3px 3px 30px #424242",
    fontSize: '.8rem',
    maxHeight: "85%"
  }
};

const colapseableDiv = css({
  overflow: 'hidden',
  transition: "width 300ms ease",
  zIndex: 100,
  background: "#fff"
})