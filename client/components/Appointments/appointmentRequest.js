/* eslint-disable no-nested-ternary */
import React from "react";
import { css } from "glamor";
import moment from "moment-timezone";
import Modal from "react-modal";
import ReactTooltip from "react-tooltip";
import { tooltipWrapper } from '../Commons/tooltip';
import { confirmAlert } from "react-confirm-alert"; // Import
import AppointmentService from "../../utils/appointmentService.js";
import InputMoment from "../../components/calender/input-moment";
import Scheduler from "./scheduler";
import Reschedule from "./rescheduler";
import Constants from "../../constants";
import BookAppNotifService from "../../utils/book_Appointment_Notification_Service";
import { saveAppointmentHistory } from "../../helpers";
import ReactTable from "react-table";
import GridContainer from "../NewSkin/Wrappers/GridContainer.js";
import Button from "../NewSkin/Components/Button.js";
import Card from '../NewSkin/Wrappers/Card';
import { ReactTableStyle, labelClass, calendarClass, font, fontWeight, appointmentStatus, link } from "../NewSkin/Styles.js";
import NoData from "../NewSkin/Components/NoData.js";
import Colors from "../NewSkin/Colors.js";

const Appointment = new AppointmentService();
const hoursPerSlot = Constants.HOURSPERSLOT;

const bookAppNotifService = new BookAppNotifService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this._isMounted = false;
    this.state = {
      displayAll: true,
      currentView: "",
      m: moment(),
      selectedDate: this.props.selectedDate,
      queuedPatients: [],
      loadingQueue: true,
      currentSlotInfo: {},
      modal: {
        rescheduler: false
      },
      loading: false,
      showCalendar: false
    };

    // this._showQueuedPatients = this._showQueuedPatients.bind(this);
    //this._getpatientQueue = this._getpatientQueue.bind(this)
    this._acceptAppointment = this._acceptAppointment.bind(this);
    this._displayAll = this._displayAll.bind(this);
    this._getAnswers = this._getAnswers.bind(this)
    this._setSchedule = this._setSchedule.bind(this);
    this._setNewState = this._setNewState.bind(this);
    this._reschedule = this._reschedule.bind(this);
  }

  componentWillMount() {
    this._isMounted = true;
    this._fetchQueuedPatients();
  }

  componentDidMount() {
    this._isMounted = true;
    const intervalId = setInterval(() => {
      if (this.state.selectedDate) {
        this._fetchQueuedPatients(this.state.selectedDate);
      } else {
        this._fetchQueuedPatients();
      }
    }, 2000);

    this._fetchQueuedPatients();

    this.setState({ intervalId });
  }

  componentWillUnmount() {
    this._isMounted = false;
    // use intervalId from the state to clear the interval
    clearInterval(this.state.intervalId);
  }

  modalSwitch(name, bol) {
    const modal = this.state.modal;
    modal[name] = bol;
    if (this._isMounted) {
      this.setState({
        modal
      });
    }
  }

  _fetchQueuedPatients(date = null) {
    let qry;
    if (date) {
      qry = { active: false, apmt_datetime: moment(date).format("YYYY-MM-DD") };
    } else {
      qry = { active: false };
    }
    return Appointment.getAppointmentsList(qry)
      .then(res => {
        this._setNewState(date, res);
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _setNewState(date, res) {
    // eslint-disable-next-line no-unused-vars
    let queuedPatients = this.state.queuedPatients;
    queuedPatients = [];
    if (this._isMounted) {
      this.setState({
        selectedDate: date,
        queuedPatients: res,
        loading: false
      });
    }
  }

  _acceptAppointment(item) {
    const id = item.id ? item.id : item.id;
    return Appointment.update(id, { active: true })
      .then(res => {
        this._sendAcceptedAppSystemNotification(item);
        this._sendAcceptedAppointmentEmail(item);

        this._fetchQueuedPatients();
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _sendAcceptedAppSystemNotification = item => {
    const sender = {
      type: "Doctor",
      // name: this.props.user.name,
      name: "System",
      id: this.props.user.id
    };

    const doctor = {
      type: "Doctor",
      name: this.props.user.name,
      id: this.props.user.id,
      appointmentDate: item.datetime,
      patientName: item.name
    };

    const patient = {
      type: "Patient",
      name: item.name,
      id: item.patient_id,
      appointmentDate: item.datetime,
      patientName: item.name
    };

    const message = item.concern;

    const subject = "Accepted Appointment";

    const notificationDetails = { sender, doctor, patient, message, subject };

    this._sendSystemNotification(notificationDetails);
  };

  // Accepted Appointment by MD
  _sendAcceptedAppointmentEmail = item => {
    const emailBodyForPatient = `
            <p style="font-size: 16px; margin: 0">Your appointment was accepted with the following details:</p>

            <p style="font-size: 16px; margin: 0px; color: #9e9e9e">Appointment date and time: <span style="color: #000; font-weight:600">
            ${
      item.datetime
        ? moment(item.datetime).format("lll")
        : moment(item.start).format("lll")
      }<span></p>

            <p style="font-size: 16px; color: #9e9e9e; margin-bottom: 0px; margin-top: 0px">Patient Name:
                <span style="color: #000; font-weight: 600">${item.name}</span>
            </p>`;

    const emailBodyForDoctor = `
            <p style="font-size: 16px; margin: 0">You have accepted an appointment with the following details:</p>
            <p style="font-size: 16px; margin: 0px; color: #9e9e9e">Appointment date and time: <span style="color: #000; font-weight:600">
            ${
      item.datetime
        ? moment(item.datetime).format("lll")
        : moment(item.start).format("lll")
      }
            <span></p>

            <p style="font-size: 16px; color: #9e9e9e;margin-bottom: 0px; margin-top: 0px">Patient Name:
                <span style="color: #000; font-weight: 600">${item.name}</span>
            </p>`;

    let emailDetails = "";
    const titles = {
      patient: "Accepted Appointment",
      doctor: "Accepted Appointment."
    };

    const emailDetailsForPatient = {
      recipient: item.email,
      date: moment(new Date()).format("lll"),
      title: titles.patient,
      body: emailBodyForPatient
    };

    const emailDetailsForDoctor = {
      recipient: this.props.user.email,
      date: moment(new Date()).format("lll"),
      title: titles.doctor,
      body: emailBodyForDoctor
    };

    emailDetails = emailDetailsForPatient;
    this._emailSetup(emailDetails);

    setTimeout(() => {
      emailDetails = emailDetailsForDoctor;
      this._emailSetup(emailDetails);
    }, 2000);
  };

  _emailSetup = email => {
    const subject = "New Notification from CloudHealthAsia";
    const text = "This is the notification of your Appointment";
    const to = email.recipient; // recipient
    const date = email.date; // notification date
    const notificationTitle = email.title; // notification title ex. Cancelled Appointment
    const notificationBody = email.body; // notification body ex. <span style="">Message here</span>

    const content = `
            <p style="font-size: 16px;">Please see below notification for your account:</p>

            <div style="border: 1px solid #ccc; padding: 20px; border-radius: 5px; background: #eee1; box-shadow: 0 8px 32px rgba(0,0,0,.050)">
                <p style="font-size: 16px; color: #9e9e9e; margin: 0">${date}</p>

                <h5 style="font-size: 1.64rem; font-weight: 400; margin: 0.82rem 0 0.656rem 0">${notificationTitle}</h5>

                ${notificationBody}
            </div>

            <p style="font-size: 16px">Please log-in to your account for further details.</p>
            <a title="Login to CloudHealthAsia" href="https://cloudhealthasia.com/login" target="_blank" style="text-decoration: none; padding: 10px 20px; background: #81cce6; color: #fff; border-radius: 5px; margin-top: 20px; display: block; width: 200px; font-weight: 500;">Login to CloudHealthAsia</a>
        `;

    const emailDetails = { to, content, subject, text };

    this._sendEmailNotification(emailDetails);
  };

  _reschedule(item) {
    let currentSlotInfo = this.state.currentSlotInfo;
    currentSlotInfo = item;
    currentSlotInfo.start = item.datetime;
    currentSlotInfo.id = item.id;
    if (this._isMounted) {
      this.setState({ currentSlotInfo });
    }
    this.modalSwitch("rescheduler", true);
  }

  _getAnswers(userid) {
    this.props.userid(userid);
    this.props.linkView("patientProfile");
    this.props.restrict(true);
  }

  _cancel(item) {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: "Please confirm appointment cancellation", // Message dialog
      buttons: [
        {
          label: "Yes, cancel this appointment",
          onClick: () => this._cancelAppointment(item)
        },
        {
          label: "No"
        }
      ]
    });
  }

  _cancelAppointment(item) {
    const {
      appointment_code,
      patient_id,
      concern,
      datetime,
      status,
      doctor_id
    } = item;

    const data = {
      //date_rescheduled: moment()._d,
      //datetime :  moment(time)._d,
      status: 3,
      active: true,
      is_modified: true
    };

    // data for Appointment History
    const appointmentHistoryData = {
      appointment_code,
      patient_id,
      history_created_date: moment(),
      concern,
      appointment_date: datetime,
      appointment_status: status,
      action: "Cancelled",
      md_id: doctor_id
    };

    const id = item.id ? item.id : item.id;

    Appointment.update(id, data)
      .then(res => {
        if (res) {
          // insert record to appointment history table
          saveAppointmentHistory(appointmentHistoryData);

          this._fetchQueuedPatients();
          this._sendCancelledAppNotification(item);
          this.modalSwitch("detail", false);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  // send system notification for cancelled appointment by doctor
  _sendCancelledAppNotification = item => {
    this._sendCancelledAppointmentEmail(item);
    const sender = {
      type: "Doctor",
      // name: this.props.user.name,
      name: "System",
      id: this.props.user.id
    };

    const doctor = {
      type: "Doctor",
      id: this.props.user.id,
      name: this.props.user.name,
      appointmentDate: item.datetime,
      patientName: item.name
    };

    const patient = {
      type: "Patient",
      name: item.name,
      id: item.patient_id,
      appointmentDate: item.datetime,
      patientName: item.name
    };

    const message = item.concern;

    const subject = "Cancelled Appointment";

    const notificationDetails = { sender, doctor, patient, message, subject };

    // console.log('%c notification details', 'padding: 5px; background: red; color:white', notificationDetails);

    this._sendSystemNotification(notificationDetails);
  };

  _sendSystemNotification = notificationDetails => {
    bookAppNotifService
      .create(notificationDetails)
      .then(res => {
        console.log(res);
      })
      .catch(err => {
        console.log(err);
      });
  };

  // send cancelled appointment email notification
  // Appointment Cancelled by Doctor
  _sendCancelledAppointmentEmail = item => {
    const emailBodyForPatient = `
            <p style="font-size: 16px; margin: 0">Your appointment with the following details was cancelled.:</p>

            <p style="font-size: 16px; margin: 0px; color: #9e9e9e">Appointment date and time: <span style="color: #000; font-weight:600">
            ${
      item.datetime
        ? moment(item.datetime).format("lll")
        : moment(item.start).format("lll")
      }
            <span></p>

            <p style="font-size: 16px; color: #9e9e9e; margin-bottom: 0px; margin-top: 0px">Patient Name:
                <span style="color: #000; font-weight: 600">${item.name}</span>
            </p>`;

    const emailBodyForDoctor = `
            <p style="font-size: 16px; margin: 0">You have cancelled an appointment with the following details:</p>
            <p style="font-size: 16px; margin: 0px; color: #9e9e9e">Appointment date and time: <span style="color: #000; font-weight:600">
            ${
      item.datetime
        ? moment(item.datetime).format("lll")
        : moment(item.start).format("lll")
      }
            <span></p>

            <p style="font-size: 16px; color: #9e9e9e;margin-bottom: 0px; margin-top: 0px">Patient Name:
                <span style="color: #000; font-weight: 600">${item.name}</span>
            </p>`;

    let emailDetails = "";
    const title = "Cancelled Appointment";

    const emailDetailsForPatient = {
      recipient: item.email, // patient's email
      date: moment(new Date()).format("lll"),
      title,
      body: emailBodyForPatient
    };

    const emailDetailsForDoctor = {
      recipient: this.props.user.email, // doctor's email
      date: moment(new Date()).format("lll"),
      title,
      body: emailBodyForDoctor
    };

    emailDetails = emailDetailsForPatient;
    this._emailSetup(emailDetails);

    setTimeout(() => {
      emailDetails = emailDetailsForDoctor;
      this._emailSetup(emailDetails);
    }, 2000);
  };

  _emailSetup = email => {
    const subject = "New Notification from CloudHealthAsia";
    const text = "This is the notification of your Appointment";
    const to = email.recipient; // recipient
    const date = email.date; // notification date
    const notificationTitle = email.title; // notification title ex. Cancelled Appointment
    const notificationBody = email.body; // notification body ex. <span style="">Message here</span>

    const content = `
            <p style="font-size: 16px;">Please see below notification for your account:</p>

            <div style="border: 1px solid #ccc; padding: 20px; border-radius: 5px; background: #eee1; box-shadow: 0 8px 32px rgba(0,0,0,.050)">
                <p style="font-size: 16px; color: #9e9e9e; margin: 0">${date}</p>

                <h5 style="font-size: 1.64rem; font-weight: 400; margin: 0.82rem 0 0.656rem 0">${notificationTitle}</h5>

                ${notificationBody}
            </div>

            <p style="font-size: 16px">Please log-in to your account for further details.</p>
            <a title="Login to CloudHealthAsia" href="https://cloudhealthasia.com/login" target="_blank" style="text-decoration: none; padding: 10px 20px; background: #81cce6; color: #fff; border-radius: 5px; margin-top: 20px; display: block; width: 200px; font-weight: 500;">Login to CloudHealthAsia</a>
        `;

    const emailDetails = { to, content, subject, text };

    this._sendEmailNotification(emailDetails);
  };

  _sendEmailNotification = emailDetails => {
    bookAppNotifService
      .sendBookAppEmailNotification(emailDetails)
      .then(res => {
        console.log("Sending email notification success!", res);
      })
      .catch(err => {
        console.log(err);
      });
  };

  _showReminder(currentItem) {
    const html = [];

    const edate = moment(currentItem.datetime).add(hoursPerSlot, "hours");

    if (currentItem.time_extension && currentItem.time_extension > 0) {
      edate.add(currentItem.time_extension, "minutes");
    }

    const lapsed = edate < moment();
    const pending_for_approval = currentItem.status === null
    const complete = currentItem.status === 1
    const rescheduled = currentItem.status === 2
    const cancelled = currentItem.status === 3

    if (pending_for_approval) {
      if (
        moment(currentItem.datetime).format('Y-MM-D') === moment().format('Y-MM-D')
      ) {
        if (
          moment(currentItem.datetime)
            .add(1, "hours") > moment()
        ) {
          html.push(
            <div key="k1">
              <h1 className={[messageStyle, "blink_me"].join(" ")}>Today</h1>
            </div>
          );
        } else {
          html.push(
            <div key="k1a">
              <h1 className={[messageStyle, warning].join(" ")}>Lapsed</h1>
            </div>
          );
        }
      } else if (
        moment(currentItem.datetime).format('Y-MM-D') ===
        moment()
          .add(1, "days").format('Y-MM-D')
      ) {
        html.push(
          <div key="k2">
            <h1 className={messageStyle}>Tomorrow</h1>
          </div>
        );
      } else if (lapsed) {
        html.push(
          <div key="k3">
            <div className={[messageStyle, warning].join(" ")}>Lapsed</div>
          </div>
        );
      } else {
        html.push(
          <div key="k3">
            <div className={messageStyle}>
              {moment(currentItem.datetime).diff(moment(), "days") < 1
                ? moment(currentItem.datetime).diff(moment(), "hours") < 2
                  ? moment(currentItem.datetime).diff(moment(), "minutes") > 0
                    ? `${moment(currentItem.datetime).diff(
                      moment(),
                      "minutes"
                    )} minutes before Consultation`
                    : "Ongoing"
                  : `${moment(currentItem.datetime).diff(
                    moment(),
                    "hours"
                  )} hours before Consultation`
                : `${moment(currentItem.datetime).diff(
                  moment(),
                  "days"
                )} days before Consultation`}
            </div>
          </div>
        );
      }
    } else if (complete) {
      html.push(
        <div key="k4">
          <div style={appointmentStatus.completed}>Completed</div>
        </div>
      );
    } else if (rescheduled) {
      html.push(
        <div key="k5">
          <div style={appointmentStatus.rescheduled}>Rescheduled</div>
        </div>
      );
    } else if (cancelled) {
      html.push(
        <div key="k6">
          <div style={appointmentStatus.cancelled}>Cancelled</div>
        </div>
      );
    }

    return html;
  }

  _showQueuedPatientsTable() {
    let columns = [];
    let data = this.state.queuedPatients;
    let datetime = "";
    let date_requested = "";
    let tooltip_name =
      { description: "See Patient Profile,<br />Write Analysis and<br />Recommendation" }

    data.map(function (item) {
      item.datetime = moment(item.datetime).format("lll");
      datetime = moment(item.datetime);
      date_requested = moment(item.date_requested);

      const edate = moment(item.datetime).add(hoursPerSlot, "hours");

      if (item.time_extension && item.time_extension > 0) {
        edate.add(item.time_extension, "minutes");
      }
      item.edate = edate
      columns = [
        {
          Header: "Date and time requested",
          accessor: "datetime",
          style: { whiteSpace: "unset", textAlign: "center" },
          width: 120
        },
        {
          Header: "Employee ID",
          accessor: "employee_id",
          style: { whiteSpace: "unset", textAlign: "center" },
          width: 150
        },
        {
          Header: "Name",
          accessor: "name",
          Cell: row => (
            <>
              <ReactTooltip html={true} />
              <span
                data-tip={tooltipWrapper(tooltip_name)}
                data-type="light"
                className={link()}
                onClick={() => this._getAnswers(row.original.userid)}
              >
                {row.value}
              </span>
            </>
          ),
          style: { whiteSpace: "unset", textAlign: "center" },
          width: 250
        },
        {
          Header: "Concern",
          accessor: "concern",
          style: { whiteSpace: "unset", textAlign: "center" },
          width: 200
        },
        {
          Header: "Status",
          accessor: "status",
          Cell: row =>
            (!row.original.active && moment(row.original.edate).isAfter(moment())) ? (
              <div>
                <span data-id="accept_appointment"
                  onClick={() => {
                    this._acceptAppointment(row.original);
                  }}
                  className={accptbtn}
                >
                  ACCEPT
              </span>
                <span
                  className={reschedbtn}
                  onClick={() => this._reschedule(row.original)}
                >
                  RESCHEDULE
              </span>
                <span
                  className={cancelBtn}
                  style={{ width: '100%' }}
                  onClick={() => this._cancel(row.original)}
                >
                  CANCEL
              </span>
              </div>
            ) :
              this._showReminder(row.original)
          ,
          width: 120,
          lineHeight: 1.5,
          style: { whiteSpace: 'unset', zIndex: 0 }
        }
        // {
        //   Header: "Reminder",
        //   accessor: "reminder",
        //   Cell: row => (
        //     <span className="queueItemStyle">
        //       {this._showReminder(row.original)}
        //     </span>
        //   ),
        //   style: { whiteSpace: "unset", textAlign: "center" },
        //   width: 100
        // }
      ];
    }, this);

    return (
      <ReactTable
        data={data}
        columns={columns}
        defaultPageSize={10}
        minRows={0}
        noDataText="No Patient in Queue"
        className="-striped -highlight"
        width={100}
        onFetchData={(state, instance) => {
          // show the loading overlay
          if (this._isMounted) {
            this.setState({ loading: true });
          }
        }}
      />
    );
  }

  _showCalendar = () => {
    if (this._isMounted) {
      this.setState({
        showCalendar: !this.state.showCalendar
      });
    }
  };

  _renderView() {
    const currentView = this.state.currentView;
    if (currentView === "setSchedule") {
      return (
        <div style={{ width: "100%", textAlign: "center" }}>
          <Card styles={{ padding: '20px 30px' }}>
            <Scheduler
              queuedPatients={() => this._fetchQueuedPatients()}
              user={this.props.user}
              acceptAppointment={this._acceptAppointment}
              sendCancelNotification={this._sendCancelledAppNotification}
            />
          </Card>
        </div>
      );
    }

    return (
      <GridContainer columnSize={'3fr 1fr'} gap={20}>
        <Card>
          <div>
            <span className={labelClass}>
              {this.state.displayAll
                ? "Appointment Request(s)"
                : `Appointment Request(s):  ${this.state.m.format(
                  "MMMM D, Y"
                ).toUpperCase()}`}{" "}

              {
                !this.state.displayAll ? (
                  <Button
                    styles={{ float: 'right', marginBottom: 20 }}
                  >
                    <button onClick={() => this._displayAll()}>VIEW ALL</button>
                  </Button>
                ) : null
              }
            </span>
          </div>

          <div className={ReactTableStyle} style={{ maxWidth: '64vw' }}>
            {this.state.queuedPatients.length > 0
              ? this._showQueuedPatientsTable()
              : <NoData text="No Appointment Request." />
            }
          </div>
        </Card>

        <Card>
          <p style={{ fontSize: 16, textAlign: 'justify', lineHeight: '18px', marginBottom: 20 }}>
            By default, all appointment requests are listed. You can filter
            appointments by selecting desired date.<br /><br />To view all appointments,
              click the <strong>View all</strong> button in the Appointment Request Title.
          </p>
          <section className={calendarClass}>
            <InputMoment
              moment={this.state.m}
              onChange={this.handleChange}
              minStep={5}
              dateTimeBtn={false}
            />
          </section>
        </Card>
      </GridContainer>
    );
  }

  // _bulletins() {
  //     let bulletins = [
  //         {
  //             id: '',
  //             name: 'Notify1',
  //             desc: 'Notification Description here'
  //         },
  //         {
  //             id: '',
  //             name: 'Notify2',
  //             desc: 'Notification Description here'
  //         }
  //     ]
  //
  //     let bulletin = [
  //         {
  //             id: '',
  //             name: 'bullet1',
  //             desc: 'Bulletin Description here'
  //         },
  //         {
  //             id: '',
  //             name: 'bullet2',
  //             desc: 'Bulletin Description here'
  //         }
  //     ]
  // }

  // _getpatientQueue() {
  //     let patientQueue = [
  //         {
  //             id: 'asfasdf',
  //             title: 'Date and Time',
  //             date: 'Aug 7 10 AM',
  //             name: 'Rachel Adam',
  //             chefComplaint: 'Gain Weight',
  //             action: 'Accept / Reschedule',
  //             calendar: 'Open calendar',
  //             value: 'Aug 7 10 AM',
  //             tooltip: 'Not Yet Set'
  //         },
  //         {
  //             id: 'asfsad',
  //             title: 'Date and Time',
  //             date: 'Aug 7 10 AM',
  //             name: 'Rachel Adam',
  //             chefComplaint: 'Gain Weight',
  //             action: 'Accept / Reschedule',
  //             calendar: 'Open calendar',
  //             value: 'Aug 7 10 AM',
  //             tooltip: 'Not Yet Set'
  //         },
  //         {
  //             id: '123124',
  //             title: 'Date and Time',
  //             date: 'Aug 7 10 AM',
  //             name: 'Rachel Adam',
  //             chefComplaint: 'Gain Weight',
  //             action: 'Accept / Reschedule',
  //             calendar: 'Open calendar',
  //             value: 'Aug 7 10 AM',
  //             tooltip: 'Not Yet Set'
  //         }
  //     ]
  //
  //     let html
  //     if (patientQueue.length > 0) {
  //         html = patientQueue.map((item, i) => (
  //             <div key={i} className={queueItemSubDivStyle}>
  //                 <div data-tip={item.tooltip} className={ItemDivStyle}>
  //                     <span className="queueItemStyle">{item.date}</span>
  //                     <span className="queueItemStyle">{item.id}</span>
  //                     <span className="queueItemStyle">{item.name}</span>
  //                     <span className="queueItemStyle">{item.chefComplaint}</span>
  //                     <span className="queueItemStyle">{item.action}</span>
  //                     <span className="queueItemStyle">{item.calendar}</span>
  //                 </div>
  //             </div>
  //         ))
  //     } else {
  //         html = (
  //             <div className={queueItemSubDivStyle}>
  //                 <div className="warningStyle">No Patients in Queue</div>
  //             </div>
  //         )
  //     }
  //
  //     return html
  // }

  _displayAll() {
    if (this._isMounted) {
      this.setState({
        displayAll: true
      });
    }
    this._fetchQueuedPatients();
  }

  handleChange = m => {
    if (this._isMounted) {
      this.setState({
        m,
        displayAll: false
      });
    }
    this._fetchQueuedPatients(m.toISOString());
  };

  _setSchedule(view) {
    let currentView = this.state.currentView;
    currentView = view;
    if (this._isMounted) {
      this.setState({
        currentView
      });
    }
  }

  render() {
    const currentView = this.state.currentView;
    return (
      <div>

        <GridContainer gap={20}>
          <GridContainer columnSize={'1fr 1fr'} gap={20} styles={{ maxWidth: 500, margin: '0 auto' }}>
            <Button type={currentView === "" ? 'blue' : 'default'}
              classes={[font(0.75), fontWeight(700)]}
              styles={{ textTransform: "uppercase" }}
            >
              <div onClick={() => this._setSchedule("")}>Appointments</div>
            </Button>

            <Button type={currentView === "setSchedule" ? 'blue' : 'default'}
              classes={[font(0.75), fontWeight(700)]}
              styles={{ textTransform: "uppercase" }}
            >
              <div onClick={() => this._setSchedule("setSchedule")} >Scheduler</div>
            </Button>
          </GridContainer>

          {this._renderView()}
        </GridContainer>
        <Modal
          isOpen={this.state.modal.rescheduler}
          onRequestClose={() => this.modalSwitch("rescheduler", false)}
          shouldCloseOnOverlayClick={false}
          style={customStyles}
        >
          <div className={modalContentWrapper}>
            <Reschedule
              doctorId={this.props.user.id}
              user={this.props.user}
              appointmentData={this.state.currentSlotInfo}
              getSelectedAppointments={() => this._fetchQueuedPatients()}
            />
          </div>
          <Button styles={{ margin: '10px auto' }}>
            <button onClick={() => this.modalSwitch("rescheduler", false)}>Close</button>
          </Button>
        </Modal>
      </div>
    );
  }
}

let messageStyle = css({
  fontSize: 14,
  textTransform: 'uppercase',
  fontWeight: 600
});

let actionBtn = css({
  borderRadius: 5,
  border: `2px solid ${Colors.compromised}`,
  fontSize: 12,
  fontWeight: 500,
  display: 'block',
  width: '100% !important'
})

let reschedbtn = css(actionBtn, {
  backgroundColor: "white",
  border: `2px solid ${Colors.pink}`,
  color: Colors.pink,
  margin: '2px auto',
  lineHeight: 1.75,
  cursor: "pointer",
  ":hover": {
    backgroundColor: Colors.pink,
    color: "white"
  }
});

let cancelBtn = css(actionBtn, {
  backgroundColor: "white",
  color: Colors.compromised,
  cursor: "pointer",
  lineHeight: 1.75,
  ":hover": {
    backgroundColor: Colors.compromised,
    color: "white"
  }
})

let accptbtn = css(actionBtn, {
  backgroundColor: "white",
  borderColor: Colors.skyblue,
  color: Colors.skyblue,
  cursor: "pointer",
  lineHeight: 1.75,
  ":hover": {
    backgroundColor: Colors.skyblue,
    color: "white"
  }
});

let warning = css(actionBtn, {
  backgroundColor: "white",
  color: Colors.nutritionColor,
  border: `1px dotted ${Colors.nutritionColor}`,
  borderRadius: 5
});

const customStyles = {
  content: {
    textAlign: "center",
    zIndex: 6,
    width: "auto",
    height: 530,
    top: "55%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: 20,
    boxShadow: `0 8px 15px rgba(0,0,0,.1)`,
    border: `1px solid rgba(0,0,0,0)`,
    borderRadius: 10,
  }
};

let modalContentWrapper = css({
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  position: "relative",
  zIndex: 100
});