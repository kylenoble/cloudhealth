import React from "react";
import Form from "../Form";
import { css } from "glamor";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: {
        id: "",
        name: "name",
        question: "Enter Patient Name",
        type: "name",
        validation: "required",
        options: [],
        error: {
          active: false,
          message: "Please enter a valid name"
        },
        value: ""
      },
      email: {
        id: "",
        name: "email",
        question: "Enter Email",
        type: "email",
        validation: "required",
        options: [],
        error: {
          active: false,
          message: "Please enter a valid email address"
        },
        value: ""
      },
      date: {
        id: "",
        name: "date",
        question: "Appointment Time Ex. 5/25/2016 10:00am",
        type: "date",
        validation: "date",
        options: [],
        error: {
          active: false,
          message: "Please enter a valid date i.e. mm/dd/yyyy hh:mm am/pm"
        },
        value: ""
      },
      formError: this.props.formError,
      formSuccess: this.props.formSuccess
    };

    this._handleChange = this._handleChange.bind(this);
    this._handleSubmit = this._handleSubmit.bind(this);
  }

  render() {
    return (
      <div className={container}>
        <Form
          header="Add Appointment Details"
          submitForm={this._handleSubmit}
          buttonText="Add Appointment"
          handleChange={this._handleChange.bind(this)}
          inputs={[this.state.name, this.state.email, this.state.date]}
          formError={this.props.formError}
          formSuccess={this.state.formSuccess}
        />
      </div>
    );
  }

  _handleChange(value, input, validated) {
    const inputName = input;
    var newInput = this.state[inputName];
    newInput.value = value;
    newInput.error.active = validated;
    this.setState({
      inputName: newInput
    });
  }

  _handleSubmit() {
    this.props.submitButtonPressed(
      this.state.name.value,
      this.state.email.value,
      this.state.date.value
    );
  }
}

const container = css({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "flex-start",
  width: "98%",
  marginBottom: "250px",
  marginTop: "80px",
  fontFamily: '"Roboto-Medium",sans-serif',
  paddingLeft: "20px",
  "@media(max-width: 700px)": {
    marginTop: "100px"
  }
});
