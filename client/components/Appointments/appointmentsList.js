import React from 'react';
import moment from 'moment-timezone';
import jstz from 'jstz';
import {css} from 'glamor';

import AuthService from '../../utils/authService.js'

const auth = new AuthService()

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.user = auth.getProfile()

        this.state = {

        }

        this._showAppointments = this._showAppointments.bind(this)
        this._startAppointment = this._startAppointment.bind(this)
    }

    render () {
      let btn = ''
      if (this.user.type === 'Doctor') {
         this.btn = <button key={0} className="addAppointment" onClick={this.props.addAppointment}>Add Appointment</button>
      }
        return (
            <div className="container">
            <div className = "tableHeader">
                <h1 className="headerItem">Patient</h1>
                <h1 className="headerItem">Date</h1>
                <h1 className="headerItem">{this.btn}</h1>
            </div>
                {this._showAppointments(this.props.doctorAppointments)}
            </div>
        )
    }

    _showAppointments(appointments) {
        let html = []

        if (this.user.type === 'Doctor' && appointments.length < 1) {
           html.push(
                <button key={0} className="addAppointment" onClick={this.props.addAppointment}>Add</button>
            )
        }

        if (appointments.length < 1) {
            return(
                <div className="emptyContainer">
                    <span key={0} className="emptyState">No Appointments Scheduled</span>
                    {this.user.type === 'Doctor' ? html : ''}
                </div>
            )
        }

        let dates = []
        // added for key with multiple returns for each currentItem
        let i = 1
        appointments.forEach((currentItem) => {
            if (moment(currentItem.datetime).format('MM/DD/YYYY') !== dates[dates.length - 1]) {
                dates.push(moment(currentItem.datetime).format('MM/DD/YYYY'))
                if (moment(currentItem.datetime).format('MM/DD/YYYY') === moment().format('MM/DD/YYYY')) {
                    html.push(
                        <div key={i} className = "tableHeader">
                            <h1 className="appointmentDayHeader">Today</h1>
                        </div>
                    )
                    i++;
                } else if (moment(currentItem.datetime).format('MM/DD/YYYY') === moment().add(1, 'days').format('MM/DD/YYYY')) {
                    html.push(
                        <div key={i} className = "tableHeader">
                            <h1 className="appointmentDayHeader">Tomorrow</h1>
                        </div>
                    )
                    i++;
                } else {
                    html.push(
                        <div key={i} className = "tableHeader">
                            <h1 className="appointmentDayHeader">{moment(currentItem.datetime).format('MMMM Do YYYY')}</h1>
                        </div>
                    )
                    i++;
                }
            }

            var btn1 = currentItem.active ? <span className="note">On Queue</span> : <button id={"btn1"+currentItem.id} className='startAppointment patient-button' onClick={() => this._startAppointment(currentItem.room_name, currentItem.id)}>Start</button>
            var btn2 = currentItem.active ? '' : <button id={"btn"+currentItem.id} className='startAppointment patient-button' onClick={() => this._startAppointment(currentItem.room_name, currentItem.id)}>Cancel</button>

            html.push(
                <div key={i} className="itemContainer">
                    <h1 className="patientItemName">{currentItem.name}</h1>
                    <h1 className="patientItem">{moment(currentItem.datetime).format('hh:mma')}</h1>
                    <span className="patientItem">
                    {btn1}
                    {btn2}

                    </span>
                </div>
            )
            i++;
        });

        return html
    }


    _startAppointment(roomName, id) {
   document.getElementById("btn1"+id).innerText="On Que"
   document.getElementById("btn"+id).style.display ='none';
   document.getElementById("btn1"+id).classList.add('disabledButton')
   this.props.beginAppointment(roomName, id)
    }
}

const disab = css({
    background: 'green'
})
