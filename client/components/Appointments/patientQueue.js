/* eslint-disable no-nested-ternary */
import React from "react";
import { css } from "glamor";
import moment from "moment-timezone";
import Modal from "react-modal";
import ReactTooltip from "react-tooltip";
import { tooltipWrapper } from "../Commons/tooltip";
import InputMoment from "../../components/calender/input-moment";
import ConsultsService from "../../utils/consultsService";
import Consult from "../../components/Consultations";
import Constants from "../../constants";
import ConsultHistory from "../PatientTracker/consultHistory";
import ReactTable from "react-table";
import AppointmentService from "../../utils/appointmentService.js";
import Card from "../NewSkin/Wrappers/Card";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import {
  calendarClass,
  link,
  combined,
  ReactTableStyle,
  statusButton,
  font
} from "../NewSkin/Styles";
import NoData from "../NewSkin/Components/NoData";
import Button from "../NewSkin/Components/Button";
import Colors from "../NewSkin/Colors";
import { Preparing } from "../NewSkin/Components/Loading";

const Appointment = new AppointmentService();

const hoursPerSlot = Constants.HOURSPERSLOT;
const Consults = new ConsultsService();

export default class extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = {
      //modal: { history: false, concern_history: false },
      consultMode: false,
      displayAll: true,
      appointment_detail: {},
      m: moment(),
      selectedDate: this.props.selectedDate,
      queuedPatients: [],
      loading: false,
      modal: {
        conference: false
      },
      selectedAppointment: {},
      allConsult: [],
      showCalendar: false
    };

    //this._showQueuedPatients = this._showQueuedPatients.bind(this);
    //this._getpatientQueue = this._getpatientQueue.bind(this)
    this._getAnswers = this._getAnswers.bind(this);
    this._fetchQueuedPatients = this._fetchQueuedPatients.bind(this);
    this._displayAll = this._displayAll.bind(this);
    this.modalSwitch = this.modalSwitch.bind(this);
    this._back = this._back.bind(this);
    this._showQueuedPatientsTable = this._showQueuedPatientsTable.bind(this);
    this._consultCount = this._consultCount.bind(this);
  }

  componentWillMount() {
    this._fetchQueuedPatients();
  }

  componentDidMount() {
    this._fetchQueuedPatients(this.state.selectedDate);
    this._getConsults();
    this._isMounted = true;
  }

  componentWillUnmount() {
    // use intervalId from the state to clear the interval
    // clearInterval(this.state.intervalId);
    this._isMounted = false;
  }

  _getConsults() {
    const param = {
      doctor_id: 9,
      status: 1
    };

    Consults.get(param)
      .then(res => {
        this.setState({ allConsult: res });
      })
      .catch(e => {
        console.log(e);
      });
  }

  _fetchQueuedPatients(date) {
    let qry;
    if (date) {
      qry = { active: true, apmt_datetime: date };
    } else {
      qry = { active: true };
    }
    return Appointment.getAppointmentsList(qry)
      .then(result => {
        this.setState({
          selectedDate: date,
          queuedPatients: result,
          loading: false
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  _openRoom(item) {
    this.setState({ consultMode: true, appointment_detail: item });
  }

  _getAnswers(userid) {
    this.props.userid(userid);
    this.props.linkView("patientProfile");
    this.props.restrict(false);
  }

  _patientTracker(item, modalname) {
    this.setState({ selectedAppointment: item }, () => {
      this.modalSwitch(modalname, true);
    });
  }

  _consultCount(item, field) {
    const allConsult = this.state.allConsult;
    let ctr = 0;
    allConsult.forEach(c => {
      if (field === "concern") {
        //field2 = 'concern'
        if (item.concern === c.concern && item.userid === c.patient_id) {
          ctr++;
        }
      } else if (field === "userid") {
        //field2 = 'patient_id'
        if (item.userid === c.patient_id) {
          ctr++;
        }
      }

      //  field2 = 'patient_id'
    });

    return ctr;
  }

  _lastConsult(item) {
    let lastDateConsult = "-";
    const allConsult = this.state.allConsult;
    const index = allConsult.findIndex(
      x => x.appointment_code === item.appointment_code
    );

    if (index !== -1) {
      lastDateConsult = moment(allConsult[index].consult_date).format(
        "MMM D, YYYY hh:mma"
      );
    }
    return <div>{lastDateConsult}</div>;
  }

  // _showQueuedPatients() {
  //   let html = [];
  //   let canCall = false;
  //   if (this.state.queuedPatients.length > 0) {
  //     html = this.state.queuedPatients.map((item, i) => {
  //       const edate = moment(item.datetime).add(hoursPerSlot, "hours");
  //       canCall =
  //         moment().toISOString() >= moment(item.datetime).toISOString() &&
  //         moment().toISOString() < edate.toISOString();
  //       const lapsed =
  //         moment(item.datetime)
  //           .add(1, "hours")
  //           .toISOString() < moment().toISOString();

  //       if (item.active && item.status == null) {
  //         html = (
  //           <div key={i} className={queueItemSubDivStyle}>
  //             <div data-tip={item.tooltip} className={`${ItemDivStyle} ${itemWrapper}`}>
  //               <ReactTooltip className="tooltipStyle" />
  //               <span style={{ flex: 2 }} className={`queueItemStyle ${queueItemStyle}`}>
  //                 {moment(item.datetime).format("MMM D, YYYY.  hh:mma")}{" "}
  //                 {lapsed ? (
  //                   <span className={`${queueItemStyle} ${warning}`}>(Lapsed)</span>
  //                 ) : null}
  //               </span>

  //               <span
  //                 className={`queueItemStyle ${queueItemStyle}`}
  //                 style={{ justifyContent: "center" }}
  //               >
  //                 {item.userid}
  //               </span>

  //               <span
  //                 style={{ flex: 2, ...styles.itemLinkStyle }}
  //                 data-tip="See Patient Profile, Write Analysis and Recommendation"
  //                 className={`queueItemStyle ${queueItemStyle}`}
  //                 onClick={() => this._getAnswers(item.userid)}
  //               >
  //                 {item.name}{" "}
  //               </span>

  //               <span className={`queueItemStyle ${queueItemStyle}`}>
  //                 {item.concern}
  //                 {this._consultCount(item, "concern") > 0 ? (
  //                   <div
  //                     style={{
  //                       ...styles.itemLinkStyle,
  //                       justifyContent: "center"
  //                     }}
  //                     data-tip="See Consult History."
  //                     className={`queueItemStyle ${queueItemStyle}`}
  //                     onClick={() =>
  //                       this._patientTracker(item, "concern_history")
  //                     }
  //                   >( {this._consultCount(item, "concern")} )</div>
  //                 ) : null}
  //               </span>

  //               {this._consultCount(item, "userid") > 0 ? (
  //                 <div
  //                   style={{
  //                     ...styles.itemLinkStyle,
  //                     justifyContent: "center"
  //                   }}
  //                   data-tip="See Consult History."
  //                   className={`queueItemStyle ${queueItemStyle}`}
  //                   onClick={() => this._patientTracker(item, "history")}
  //                 >
  //                   {this._consultCount(item, "userid")}
  //                 </div>
  //               ) : (
  //                   <div className={`queueItemStyle ${queueItemStyle}`}>No Prev Consult</div>
  //                 )}
  //               <div className={`queueItemStyle ${queueItemStyle}`}>{this._lastConsult(item)}</div>

  //               {canCall ? (
  //                 <span className={`queueItemStyle ${queueItemStyle}`}>
  //                   <span
  //                     className={callBtn}
  //                     onClick={() => this._openRoom(item)}
  //                   >Video Conference</span>
  //                 </span>
  //               ) : lapsed ? (
  //                 <span className={`queueItemStyle ${queueItemStyle}`}>
  //                   <span
  //                     className={callBtn}
  //                     onClick={() => this._openRoom(item)}
  //                   >Consult Form</span>
  //                 </span>
  //               ) : (
  //                     <span
  //                       data-tip="Available only during Consultation date."
  //                       className={`queueItemStyle ${queueItemStyle}`}
  //                     >
  //                       <span className={lockCallBtn}>Video Conferences</span>
  //                     </span>
  //                   )}
  //             </div>
  //           </div>
  //         );
  //       }
  //       return html;
  //     });
  //   } else {
  //     html = (
  //       <div className={`${queueItemSubDivStyle}`}>
  //         <div className="warningStyle">No Patients in Queue</div>
  //       </div>
  //     );
  //   }

  //   return html;
  // }

  modalSwitch(name, bol) {
    const modal = this.state.modal;
    modal[name] = bol;
    if (this._isMounted) {
      this.setState({
        modal
      });
    }
  }

  _showQueuedPatientsTable() {
    let tooltip_name = {
      description:
        "See Patient Profile,<br />Write Analysis and<br />Recommendation"
    };
    let tooltip_consult = { description: "See Consult History" };
    let consult_cnt_per_concern = 0;
    let consult_cnt_per_user = 0;
    let columns = [];
    let filtered_data = [];
    let data = this.state.queuedPatients;

    if (data.length > 0) {
      data.filter(function(item) {
        if (item.active && (item.status === null || item.status === 1)) {
          consult_cnt_per_concern = this._consultCount(item, "concern");
          consult_cnt_per_user = this._consultCount(item, "userid");
          item.last_consult = this._lastConsult(item);

          //store consult_cnt to be used in the onClick event
          let consult_cnt = {};
          consult_cnt.consult_cnt_per_concern = consult_cnt_per_concern;
          consult_cnt.consult_cnt_per_user = consult_cnt_per_user;
          item.consult_cnt = consult_cnt;

          // item.edate = moment(item.datetime).add(hoursPerSlot, "hours"); CHANGED BY JM
          // const edate = moment(item.datetime).add(hoursPerSlot, "hours");
          // item.canCall =
          //   moment().toISOString() >= moment(item.datetime).toISOString() &&
          //   moment().toISOString() < edate.toISOString();
          // item.lapsed =
          //   moment(item.datetime)
          //     .add(1, "hours")
          //     .toISOString() < moment().toISOString();

          const edate = moment(item.datetime).add(hoursPerSlot, "hours");

          if (item.time_extension && item.time_extension > 0) {
            edate.add(item.time_extension, "minutes");
          }

          item.canCall =
            moment().toISOString() >= moment(item.datetime).toISOString() &&
            moment().toISOString() < edate.toISOString();
          item.lapsed = edate.toISOString() < moment().toISOString();

          //filtered data
          filtered_data.push(item);
        }
      }, this);
    }

    if (filtered_data.length > 0) {
      columns = [
        {
          Header: "User ID",
          accessor: "userid",
          show: false
        },
        {
          Header: "ID",
          accessor: "id",
          show: false
        },
        {
          Header: "Appointment Date",
          accessor: "datetime",
          Cell: row => (
            <span>
              {moment(row.value).format("MMM D, YYYY")}
              <br />
              {moment(row.value).format("hh:mmA")}
            </span>
          ),
          style: { justifyContent: "center" },
          width: 130
        },
        // {
        //   Header: "Employee ID",
        //   accessor: "employee_id",
        //   style: { textAlign: "center" },
        //   width: 100
        // },
        {
          Header: "Name",
          accessor: "name",
          Cell: row => (
            <div>
              <ReactTooltip html={true} />
              <span
                data-tip={tooltipWrapper(tooltip_name)}
                data-type="light"
                className={combined([link()])}
                onClick={() => this._getAnswers(row.original.userid)}
              >
                {row.value}
              </span>
            </div>
          ),
          style: { justifyContent: "center", whiteSpace: "unset" },
          width: 200
        },
        {
          Header: "Concern / # of Consults",
          accessor: "concern",
          Cell: row => (
            <span>
              {row.value}{" "}
              {row.original.consult_cnt.consult_cnt_per_concern > 0 ? (
                <span
                  style={{
                    ...styles.itemLinkStyle
                  }}
                  data-tip={tooltipWrapper(tooltip_consult)}
                  data-type="light"
                  onClick={() =>
                    this._patientTracker(row.original, "concern_history")
                  }
                >
                  ( {row.original.consult_cnt.consult_cnt_per_concern} )
                </span>
              ) : null}
            </span>
          ),
          style: { textAlign: "center", whiteSpace: "unset" },
          width: 150
        },
        {
          Header: "# of Consults",
          accessor: "consults",
          Cell: row => (
            <div>
              {row.original.consult_cnt.consult_cnt_per_user > 0 ? (
                <span
                  style={{
                    ...styles.itemLinkStyle,
                    justifyContent: "center"
                  }}
                  data-tip={tooltipWrapper(tooltip_consult)}
                  data-type="light"
                  className={`queueItemStyle ${queueItemStyle}`}
                  onClick={() => this._patientTracker(row.original, "history")}
                >
                  {row.original.consult_cnt.consult_cnt_per_user}
                </span>
              ) : (
                `-`
              )}
            </div>
          ),
          style: { textAlign: "center", whiteSpace: "unset" },
          width: 90
        },
        {
          Header: "Last Consult",
          accessor: "last_consult",
          style: { textAlign: "center", whiteSpace: "unset" },
          width: 105
        },
        {
          Header: "Status",
          accessor: "status",
          Cell: row => (
            <span>
              {row.original.lapsed ? (
                <div
                  className={combined([statusButton, font(0.8), link("#fff")], {
                    padding: 0
                  })}
                  data-tip={tooltipWrapper({
                    description: "View consultation form",
                    title: "Status: Complete"
                  })}
                  data-type="light"
                  style={{
                    background: Colors.detoxColor
                  }}
                  onClick={() => this._openRoom(row.original)}
                >
                  View Consult Form
                </div>
              ) : row.original.canCall ? (
                <div
                  className={combined([statusButton, font(0.8), link("#fff")], {
                    padding: 0
                  })}
                  style={{
                    background: Colors.weightColor
                  }}
                  onClick={() => this._openRoom(row.original)}
                >
                  Video Conference
                </div>
              ) : (
                <div
                  className={combined([statusButton, font(0.8), link("#fff")], {
                    padding: 0
                  })}
                  data-tip={tooltipWrapper({
                    description: "Available only during consultation date."
                  })}
                  data-type="light"
                  style={{
                    background: Colors.disabled
                  }}
                >
                  Video Conference
                </div>
              )}
            </span>
          ),
          style: { whiteSpace: "unset", margin: "0 auto" },
          width: 130
        }
      ];

      return (
        <div className={`${queueItemDivStyle} ${ReactTableStyle}`}>
          <ReactTable
            data={filtered_data}
            columns={columns}
            defaultPageSize={10}
            minRows={1}
            noDataText="No Patient in Queue"
            className={`-highlight`}
            width={100}
          />
        </div>
      );
    } else {
      return <NoData text="No Patient in Queue" />;
    }
  }

  _showCalendar = () => {
    this.setState({
      showCalendar: !this.state.showCalendar
    });
  };

  _renderView() {
    if (this.state.consultMode) {
      return (
        <Consult
          back={this._back}
          user={this.props.user}
          queuedPatients={this.state.queuedPatients}
          appointment_detail={this.state.appointment_detail}
          updateRoot={this.props.updateRoot}
        />
      );
    }
    if (this.state.loading) {
      return <Preparing />;
    }
    return (
      <div>
        <GridContainer columnSize={"2fr 1fr"} gap={20}>
          <Card styles={{ maxWidth: 915, width: 915 }}>
            <section
              style={{ minHeight: 585, width: "100%", padding: "30px 5px" }}
            >
              {this._showQueuedPatientsTable()}
            </section>
          </Card>

          <Card styles={{ padding: "20px 30px", width: 366 }}>
            <Button
              type={this.state.displayAll && "disabled"}
              styles={{
                margin: "0 auto",
                fontWeight: 500,
                maxWidth: "fit-content"
              }}
            >
              <span
                style={{ cursor: "pointer" }}
                onClick={() => this._displayAll()}
              >
                VIEW ALL APPOINTMENTS
              </span>
            </Button>

            <div
              style={{ margin: "20px auto 0", maxWidth: 300 }}
              className={`${calendarClass} calenderStyle`}
            >
              <InputMoment
                moment={this.state.m}
                onChange={this.handleChange}
                minStep={5}
                dateTimeBtn={false}
              />
            </div>
            {/* <div
              className={`${calendarDiv} ${calendarWrapper} ${col} ${
                this.state.showCalendar ? showCalendar : ""
                } calendarWrapper`}
            >

              <button
                className={`${showCalendarBtn} `}
                onClick={this._showCalendar}
              >
                {this.state.showCalendar ? "Hide" : "Show"} Calendar{" "}
                <i
                  className={`${arrow} ${this.state.showCalendar ? rotate : ""}`}
                />
              </button>
            </div> */}
          </Card>
        </GridContainer>
      </div>
    );
  }

  _bulletins() {}

  _displayAll() {
    this.setState({
      displayAll: true
    });
    this._fetchQueuedPatients();
  }

  handleChange = m => {
    this.setState({
      m,
      displayAll: false
    });
    this._fetchQueuedPatients(m.format("Y-MM-DD"));
    //  this._getQueuedPatients();
  };

  _back(final) {
    this.setState({ consultMode: false, loading: true }, () => {
      if (final) {
        setTimeout(() => {
          this._fetchQueuedPatients(this.state.selectedDate);
        }, 1000);
      } else {
        this._fetchQueuedPatients(this.state.selectedDate);
      }
    });
  }

  render() {
    return (
      <div>
        {this._renderView()}

        <Modal
          isOpen={this.state.modal.history}
          onRequestClose={() => this.modalSwitch("history", false)}
          shouldCloseOnOverlayClick={false}
          style={customStyles}
        >
          <ConsultHistory
            groupBy="id"
            patient_id={this.state.selectedAppointment.patient_id}
            modalSwitch={() => this.modalSwitch("history", false)}
          />
        </Modal>

        <Modal
          isOpen={this.state.modal.concern_history}
          onRequestClose={() => this.modalSwitch("concern_history", false)}
          shouldCloseOnOverlayClick={false}
          style={customStyles}
        >
          <ConsultHistory
            groupBy="id"
            patient_id={this.state.selectedAppointment.patient_id}
            concern={this.state.selectedAppointment.concern}
            modalSwitch={() => this.modalSwitch("concern_history", false)}
          />
        </Modal>
      </div>
    );
  }
}

// styling
let queueItemStyle = css({
  padding: "5px !important",
  "> span": {
    "> span": {
      padding: "5px 10px !important"
    }
  }
});

let btn = css({
  //float: 'right',
  padding: "0 5px",
  backgroundColor: "white",
  border: "2px solid #80cde9",
  color: "#80cde9",
  borderRadius: "5px",
  margin: "0px",
  fontSize: "12px",
  cursor: "pointer",
  ":hover": {
    backgroundColor: "#80cde9",
    border: "2px solid #80cde9",
    color: "white"
  }
});

const styles = {
  itemLinkStyle: {
    cursor: "pointer",
    color: "#80cde9"
  },
  accptbtn: {
    color: "blue",
    ":hover": {
      boxShadow: "0 0 20px 0 rgba(249,213,232,1)"
    },
    cursor: "pointer"
  }
};

let queueItemDivStyle = css({
  display: "flex",
  flex: "1",
  flexWrap: "wrap",
  justifyContent: "space-around",
  "> div": {
    border: "none !important"
  }
});

const customStyles = {
  content: {
    textAlign: "center",
    zIndex: 6,
    width: "auto",
    height: "auto",
    top: "55%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: 10,
    boxShadow: "-3px 3px 30px #424242"
  }
};
