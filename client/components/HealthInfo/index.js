import React from "react";
import Link from "next/link";
import AnswerService from "../../utils/answerService.js";
import SummaryService from "../../utils/summaryService.js";
import GenerateSummary from "../../utils/generateSummary.js";
import { css } from "glamor";
import ReactTooltip from "react-tooltip";
import GridContainer from "../NewSkin/Wrappers/GridContainer.js";
import Button from "../NewSkin/Components/Button.js";
import { sqrDefaultOption, sqrSelectedOption } from "../NewSkin/Styles.js";
import Colors from "../NewSkin/Colors.js";

const answer = new AnswerService();
const summary = new SummaryService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      healthIssues: [],
      options: {
        hypertension: {
          label: "Hypertension",
          selected: false,
          value: "hypertension",
          subtext: "",
          error: {
            active: false
          }
        },
        diabetes: {
          label: "Diabetes",
          selected: false,
          value: "diabetes",
          subtext: "",
          error: {
            active: false
          }
        },
        stroke: {
          label: "Stroke/TIA",
          selected: false,
          value: "stroke",
          subtext: "",
          error: {
            active: false
          }
        },
        heart: {
          label: "Heart Disease",
          selected: false,
          value: "heart",
          subtext:
            "(Cardiac accidents, myocardial infarction, coronary disease)",
          error: {
            active: false
          }
        },
        cancer: {
          label: "Cancer or Chemotherapy History",
          selected: false,
          value: "cancer",
          subtext: "",
          error: {
            active: false
          }
        },
        chronic: {
          label: "Chronic Non-Resolving Pathology",
          selected: false,
          value: "chronic",
          subtext:
            "(persistently elevated cholesterol levels, uric acid, persistent allergies, chronic asthma etc.)",
          error: {
            active: false
          }
        },
        weight: {
          label: "Weight Problems",
          selected: false,
          value: "weight",
          subtext: "(persistent underweight or overweight/obesity)",
          error: {
            active: false
          }
        },
        others: {
          label: "Others",
          selected: false,
          value: "others",
          subtext: "",
          error: {
            active: false
          }
        },
        none: {
          label: "None of the above",
          selected: false,
          value: "none",
          subtext: "",
          error: {
            active: false
          }
        }
      },
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      },
      othersTextboxValue: ""
    };
  }

  supportsFlexBox() {
    const test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  componentDidMount() {
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      flexibility(document.body);
    }
    this._setDefaultValues();
  }

  _setDefaultValues() {
    const latestAnswer = this.props.survey;
    const newState = {
      options: this.state.options,
      healthIssues: []
    };
    const latestAnswerValue = latestAnswer
      ? latestAnswer.value.split(",").filter(Boolean)
      : "";
    const others_values = [];
    for (const answer of latestAnswerValue) {
      const name = answer;
      const item = this.state.options[name];

      if (
        name == "hypertension" ||
        name == "diabetes" ||
        name == "stroke" ||
        name == "heart" ||
        name == "cancer" ||
        name == "chronic" ||
        name == "weight" ||
        name == "none"
      ) {
        item.selected = true;
        newState.options[name] = item;
      } else {
        if (!this.state.options["others"].selected) {
          this.state.options["others"].selected = true;
        }
        others_values.push(name.trim());
      }
      newState.healthIssues.push(name.trim());
    }
    this._addOthersTextbox("create-answers", others_values);
    this.setState(newState);
  }

  render() {
    return (
      <div>
        <div style={styles.container}>
          <div style={styles.introInfo}>
            <h1 style={{ marginTop: 0 }}>Health Issues</h1>
            <span style={{ color: Colors.blueDarkAccent }}>
              The answers selected below will give us a picture of your current
              health condition and will help us recommend
              <br />
              health plans that will not just manage these conditions but also
              potentially reverse them.
            </span>
          </div>
          <h1 style={styles.h1}>
            Do you have any of the following? (Check all that apply)
          </h1>
          <div
            style={{ width: "70%", marginBottom: "20px", textAlign: "left" }}
          >
            {this._healthIssues()}
          </div>
          <span
            style={
              this.state.formError.active
                ? { ...styles.formError, ...styles.formErrorActive }
                : styles.formError
            }
          >
            {this.state.formError.message}&nbsp;
          </span>
          <span
            style={
              this.state.formSuccess.active
                ? { ...styles.formSuccess, ...styles.formSuccessActive }
                : styles.formSuccess
            }
          >
            {this.state.formSuccess.message}
          </span>

          <GridContainer
            columnSize="1fr 1fr"
            gap={20}
            styles={{ margin: "20px auto", maxWidth: 550 }}
          >
            <Button>
              <button onClick={() => this.props.back()}>Back</button>
            </Button>
            {/* <Link href="#">
            <a> */}
            <Button type="pink">
              <button
                id="update-btn"
                // style={styles.button}
                className={
                  this.state.options.others.selected
                    ? this.state.othersTextboxValue.trim()
                      ? ""
                      : updateDisabled
                    : ""
                }
                disabled={
                  this.state.options.others.selected
                    ? this.state.othersTextboxValue.trim()
                      ? false
                      : true
                    : false
                }
                onClick={this._goToNext.bind(this)}
              >
                {this.props.location == "profile"
                  ? "Update My Profile"
                  : "Continue My Profile"}
              </button>
            </Button>
            {/* </a>
          </Link> */}
          </GridContainer>
        </div>
        <ReactTooltip />
      </div>
    );
  }

  tooltipWrapper = label => {
    return `
      <div style="max-width: 400px; border-top-left-radius: 3px; border-top-right-radius: 3px; overflow: hidden">
        <small style="padding: 5px; background: #364563; display: block; text-align:center; color: white">${label}</small>
        <p style="padding: 15px;">Stroke that went away in 24 hours with full recovery; diagnosis by a doctor.</p>
      </div>`;
  };

  _healthIssues() {
    var html = [];
    let i = 0;
    for (var key of Object.keys(this.state.options)) {
      let currentVal = this.state.options[key];
      const isStroke = currentVal.label === "Stroke/TIA";
      const isOthers = currentVal.label === "Others";
      html.push(
        <div key={i} style={{ marginBottom: "5px" }}>
          <label
            style={{
              display: "grid",
              gridTemplateColumns: isOthers ? "40px 80px 1fr" : "40px 9fr"
            }}
          >
            <div
              className={
                currentVal.selected ? sqrSelectedOption : sqrDefaultOption
              }
            />
            <input
              onChange={this._handleChange.bind(this)}
              type="checkbox"
              checked={currentVal.selected ? true : false}
              id={currentVal.value}
              name={currentVal.value}
              value={currentVal.value}
              style={{ position: "absolute", width: 0, height: 0, opacity: 0 }}
            />
            <span
              data-id="health-issue-label"
              className={isStroke ? infoWrapper : ""}
              data-tip={isStroke ? this.tooltipWrapper(currentVal.label) : null}
              data-type={isStroke ? "light" : null}
              data-html={isStroke ? true : false}
              style={{ color: currentVal.selected ? Colors.skyblue : "#777" }}
            >
              {currentVal.label} {currentVal.subtext}
              {isStroke && <small className={info}>i</small>}
            </span>
          </label>
        </div>
      );
      i++;
    }

    return html;
  }

  _filterOutOthers = () => {
    const filtered = this.state.healthIssues.filter(
      issue =>
        issue == "hypertension" ||
        issue == "diabetes" ||
        issue == "stroke" ||
        issue == "heart" ||
        issue == "cancer" ||
        issue == "chronic" ||
        issue == "weight" ||
        issue == "none"
    );
    const filtered_issues = filtered.filter(f => f !== "others");
    return filtered_issues;
  };

  _addOthersTextbox = (input, issues) => {
    const others = document.querySelector("input#others");

    // creating textbox elements
    const textbox = document.createElement("input");
    const textboxWrapper = document.createElement("div");

    const div = others.parentNode;

    // adding textbox attributes
    textbox.placeholder = "Please enter your health condition...";
    textbox.id = "others-input";
    // adding textbox wrapper atrributes
    textboxWrapper.className = othersInputWrapper;
    textboxWrapper.appendChild(textbox);

    // animate textbox focus
    const animate = event => {
      if (event === "focus") {
        document
          .querySelector(`.${othersInputWrapper}`)
          .classList.add(animateBorder);
      } else {
        document
          .querySelector(`.${othersInputWrapper}`)
          .classList.remove(animateBorder);
      }
    };

    // adding textbox event listeners
    textbox.addEventListener("focus", () => animate("focus"));
    textbox.addEventListener("blur", () => animate("blur"));
    textbox.addEventListener("keyup", e => {
      this.setState({
        othersTextboxValue: e.target.value.trim()
      });
    });

    if (
      this.state.options.others.selected &&
      document.querySelector("#others-input")
    ) {
      this.setState({
        othersTextboxValue: document.querySelector("#others-input").value.trim()
      });
    }

    const othersWrapper = document.querySelector(`.${othersInputWrapper}`);

    if (input === "create-answers") {
      let new_issues = "";
      for (const issue of issues) {
        if (issues.indexOf(issue) + 1 === issues.length) {
          new_issues += ` ${issue}`;
        } else {
          new_issues += ` ${issue},`;
        }
      }
      textbox.value = new_issues;
      div.appendChild(textboxWrapper);
    }

    // appending textbox
    if (this.state.options.others.selected) {
      if (!document.querySelector("#others-input")) {
        div.appendChild(textboxWrapper);
      }
    } else {
      // removing textbox and textbox's event listeners
      if (othersWrapper) {
        textbox.value = "";
        textbox.removeEventListener("focus", animate);
        textbox.removeEventListener("blur", animate);
        div.removeChild(othersWrapper);
        this.setState({
          othersTextboxValue: ""
        });
      }
    }
  };

  _handleChange(e) {
    const inputName = e.target.id;
    const newInput = this.state.options[inputName];

    if (!newInput) {
      return;
    }
    newInput.selected = !newInput.selected;
    let healthIssues = this.state.healthIssues;

    if (inputName == "none" && newInput.selected) {
      healthIssues = [];
      healthIssues.push(inputName);
      //uncheck others
      for (const key of Object.keys(this.state.options)) {
        const currentVal = this.state.options[key];
        if (currentVal.value != "none") {
          currentVal.selected = false;
        }
      }
    } else {
      if (healthIssues.indexOf(inputName) < 0 && newInput.selected) {
        healthIssues.push(inputName);
      } else {
        const location = healthIssues.indexOf(inputName);
        healthIssues.splice(location, 1);
      }
      const none_exists = healthIssues.indexOf("none");
      if (none_exists > -1) {
        healthIssues.splice(none_exists, 1);
        //uncheck none
        this.state.options.none.selected = false;
      }
    }
    this.setState({
      inputName: newInput,
      healthIssues: healthIssues
    });
    this._addOthersTextbox();
  }

  _validateInput(key) {
    if (!this.state.options[key].selected) {
      this.state.options[key].error.active = true;
      return false;
    }
    this.state.options[key].error.active = false;
    return true;
  }

  _allInputsValid() {
    var results = [];
    for (let key of Object.keys(this.state.options)) {
      if (this._validateInput(key)) {
        results.push(this.state.options[key]);
      }
    }

    return results.length > 0;
  }

  _goToNext() {
    if (!this._allInputsValid()) {
      // add form error validation
      this.setState({
        formError: {
          active: true,
          message:
            "Please select applicable health issue(s), otherwise, select None of the above"
        }
      });
      return;
    } else {
      this.setState({
        formError: {
          active: false,
          message: ""
        }
      });

      let newHealthIssues = [];
      let issues = "";
      let textboxValue = "";
      let filtered = [];

      // get textbox value and add to current health issues array
      const textbox = document.querySelector("#others-input");
      if (textbox) {
        textboxValue = document.querySelector("#others-input").value;
      }

      if (this.state.options.others.selected) {
        // filter out others value/health issue
        const filtered_issues = this._filterOutOthers();
        filtered.push(...filtered_issues);
      } else {
        filtered.push(...this.state.healthIssues);
      }

      // adding textbox value to health issues array
      const alteredIssues = textboxValue
        ? [...filtered, ...textboxValue.split(",")]
        : [...filtered];

      for (const issue of alteredIssues) {
        // check if the current issue is the last issue in the array
        if (alteredIssues.indexOf(issue) + 1 === alteredIssues.length) {
          issues += `${issue}`;
        } else {
          issues += `${issue},`;
        }
      }

      if (this.state.healthIssues.length < 1) {
        issues = "none";
      }

      newHealthIssues.push({ value: issues, id: 53 });

      let downstreamSummary_value = issues;
      if (issues !== "none") {
        const summaryGenerator = new GenerateSummary(
          issues,
          "downstreamHealth"
        );
        downstreamSummary_value = summaryGenerator.generateSummary();
      }

      let downstreamSummary = [
        {
          name: "downstreamHealth",
          group: "health",
          label: "Downstream Health",
          value: downstreamSummary_value
        }
      ];
      if (this.props.location === "profile") {
        answer
          .create(newHealthIssues)
          .then(res => {
            if (res) {
              summary
                .create(downstreamSummary)
                .then(result => {
                  console.log("update user profile");
                  this.setState({
                    formSuccess: {
                      active: true,
                      message: "Your Profile Was Updated"
                    }
                  });
                  this.props.back();
                  //  return this.props.onClick('Dashboard')
                })
                .catch(error => {
                  console.log(error);
                  this.setState({
                    formError: {
                      active: true,
                      message: "There was an error updating your profile"
                    }
                  });
                });
            } else {
              this.setState({
                formError: {
                  active: true,
                  message: "There was an error updating your profile"
                }
              });
            }
          })
          .catch(e => {
            if (e.error === "This email is already in use") {
              var newInput = this.state["email"];
              newInput.error.active = true;
              newInput.error.message = e.error;
              this.setState({
                inputName: newInput
              });
            } else {
              this.setState({
                formError: {
                  active: true,
                  message: e.error
                }
              });
            }
          }); // you would show/hide error messages with component state here
      } else {
        answer
          .create(newHealthIssues)
          .then(res => {
            if (res && !res.error) {
              console.log("should move on");
              //  this.props.onClick('intake')
            } else {
              this.setState({
                formError: {
                  active: true,
                  message: "There was an error submitting your answers"
                }
              });
              return;
            }
          })
          .catch(e => {
            this.setState({
              formError: {
                active: true,
                message: e.error
              }
            });
            return;
          }); // you would show/hide error messages with component state here
      }
    }
  }
}

let infoWrapper = css({
  display: "grid",
  gridTemplateColumns: "1fr 30px",
  maxWidth: 120,
  ":hover > small": {
    borderColor: "#000",
    color: "#000"
  },
  cursor: "help"
});

let info = css({
  width: 20,
  height: 20,
  borderRadius: "100%",
  border: "2px solid #ccc",
  display: "grid",
  justifyContent: "center",
  alignContent: "center",
  fontWeight: 600,
  transition: "250ms ease",
  color: "#ccc",
  margin: "auto auto"
});

const updateDisabled = css({
  backgroundColor: "#ccc !important",
  cursor: "not-allowed !important"
});

const animateBorder = css({
  "::before": {
    width: "100% !important"
  }
});

const othersInputWrapper = css({
  width: 300,
  position: "relative",
  border: "none",
  display: "inline-block",
  width: 350,
  borderBottom: "3px solid #ccc",
  "::before": {
    content: '""',
    position: "absolute",
    bottom: "-3px",
    left: 0,
    height: 3,
    width: 0,
    backgroundColor: "#80cde9",
    transition: "300ms ease",
    zIndex: "100"
  },
  "> input": {
    width: "100%",
    boxSizing: "border-box",
    padding: "10px 15px",
    border: "none",
    ":focus": {
      outline: "none",
      border: "none",
      boxShadow: "none"
    }
  }
});

const styles = {
  container: {
    fontFamily: '"Roboto",sans-serif',
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  h1: {
    color: Colors.blueDarkAccent,
    fontSize: "24px",
    fontFamily: '"Roboto-Medium",sans-serif'
  },
  introInfo: {
    fontFamily: '"Roboto",sans-serif',
    color: "#364563",
    fontSize: "14px",
    padding: "10px 20px",
    textAlign: "justified"
  },
  bodies: {
    marginBottom: "20px",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    flexWrap: "wrap",
    flex: "1 0"
  },
  bodyContainer: {
    height: "240px",
    width: "216px",
    boxShadow: "1px 2px 3px 0px rgba(0,0,0,0.10)",
    borderRadius: "6px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    padding: "20px",
    margin: "25px",
    cursor: "pointer",
    color: "#364563",
    boxShadow: "0 2px 2px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.08)",
    transition: "box-shadow 200ms cubic-bezier(0.4, 0.0, 0.2, 1)"
  },
  active: {
    backgroundColor: "#ED81B9",
    color: "white"
  },
  header: {
    borderBottom: "2px solid rgba(225,232,238,1)",
    padding: "15px 20px",
    fontSize: "20px",
    color: "#364563",
    fontFamily: '"Roboto-Medium",sans-serif',
    width: "360px"
  },
  title: {
    fontSize: "18px",
    fontFamily: '"Roboto-Medium",sans-serif',
    marginBottom: "5px"
  },
  sub: {
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif'
  },
  formError: {
    fontSize: "1rem",
    color: "#EA3131",
    transition: "color 0.3s ease-in-out",
    fontWeight: 500
  },
  formErrorActive: {
    display: "block"
  },
  formSuccess: {
    display: "none",
    fontSize: "0.8em",
    marginLeft: "25px",
    marginTop: "5px",
    marginBottom: "10px",
    color: "rgb(3, 210, 127)",
    transition: "color 0.3s ease-in-out"
  },
  formSuccessActive: {
    display: "block"
  },
  button: {
    backgroundColor: "#ED81B9",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "0px 0px 20px 0px",
    cursor: "pointer"
  }
};
