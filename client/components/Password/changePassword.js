import React from "react";
import Link from "next/link";
import UserService from "../../utils/userService.js";
import Form from "../Form";
import currentDomain from "../../utils/domain.js";
import { css } from "glamor";

const user = new UserService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this._isMounted = false;
    this.domain = `${currentDomain}/images/`;
    this.state = {
      userId: "",
      user_type: "",
      companies: [],
      departments: [],
      branches: [],
      fields: [],
      fullName: {
        id: "",
        name: "fullName",
        question: "Enter Name",
        type: "text",
        validation: "name",
        options: [],
        error: {
          active: false,
          message: "Please enter a valid full name"
        },
        value: ""
      },
      email: {
        id: "",
        name: "email",
        question: "Enter valid Email",
        type: "email",
        validation: "email",
        options: [],
        error: {
          active: false,
          message: "Please enter a valid email address"
        },
        value: ""
      },
      currentPassword: {
        id: "",
        name: "currentPassword",
        question: "Current Password",
        type: "password",
        validation: "password",
        options: [],
        error: {
          active: false,
          message:
            "Passwords must have at least 1 number, 1 Uppercase letter, 1 Lowercase letter, and a minimum of 8 characters"
        },
        value: ""
      },
      password: {
        id: "",
        name: "password",
        question: "New Password",
        type: "password",
        validation: "password",
        options: [],
        error: {
          active: false,
          message:
            "Passwords must have at least 1 number, 1 Uppercase letter, 1 Lowercase letter, and a minimum of 8 characters"
        },
        value: ""
      },
      confirmPassword: {
        id: "",
        name: "confirmPassword",
        question: "Confirm Password",
        type: "password",
        validation: "password",
        options: [],
        error: {
          active: false,
          message:
            "Passwords must have at least 1 number, 1 Uppercase letter, 1 Lowercase letter, and a minimum of 8 characters"
        },
        value: ""
      },
      company: {
        id: "",
        name: "company",
        question: "Enter Company Code",
        type: "company",
        validation: "company",
        options: [],
        error: {
          active: false,
          message: "Please enter a valid Company code!"
        },
        value: ""
      },
      department: {
        id: "",
        name: "department",
        question: "Select your Department",
        type: "dropdown",
        disabled: false,
        validation: "required",
        options: [],
        error: {
          active: false,
          message: "Please select a department"
        },
        value: ""
      },
      branch: {
        id: "",
        name: "branch",
        question: "Select branch",
        type: "dropdown",
        disabled: false,
        validation: "required",
        options: [],
        error: {
          active: false,
          message: "Please select a branch"
        },
        value: ""
      },
      employeeId: {
        id: "",
        name: "employeeId",
        question: "Enter Employee Id",
        type: "required",
        validation: "required",
        options: [],
        error: {
          active: false,
          message: "Please enter a valid employee id address"
        },
        value: ""
      },
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      }
    };

    this._goToNext = this._goToNext.bind(this);
    this._updateFields = this._updateFields.bind(this);
  }

  supportsFlexBox() {
    var test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  componentDidMount() {
    this._isMounted = true;
    let userProfile = user.getProfile();
    this.setState({
      userId: userProfile.id,
      user_type: userProfile.type
    });
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      flexibility(document.body);
    }
    this._updateFields();
  }

  componentWillUnmount() {
    // use intervalId from the state to clear the interval
    this._isMounted = false;
  }

  render() {
    return (
      <div>
        <div
          style={{ marginBottom: 20 }}
        >
          <section className={noteWrapper}>
            { this.state.user_type === 'Patient' &&
            <span>
              Please note that only your registered name, email address and
              employee ID will be used as identifiers in setting and reviewing
              appointments.
            </span>
            }
            {this.props.message ? (
              <span className={warning}>{this.props.message}</span>
            ) : null}
            <span className={warning}>
              Once you click "Update Password" you will be redirected to the
              log-in page. Login with your username and new password
            </span>
          </section>
        </div>
        <Form
          account
          cpassword
          companies={this.state.companies}
          departments={this.state.departments}
          submitForm={this._goToNext}
          buttonText={
            this.props.location == "profile"
              ? "Update Password"
              : "Continue My Profile"
          }
          handleChange={this._handleChange.bind(this)}
          inputs={this.state.fields}
          secondaryOption={
            this.props.location == "profile" ? (
              ""
            ) : (
                <Link href="/login">
                  <a style={styles.register}>I Already Have an Account</a>
                </Link>
              )
          }
          formError={this.state.formError}
          formSuccess={this.state.formSuccess}
          location={this.props.location}
        />
      </div>
    );
  }

  _handleChange(value, input, validated) {
    const inputName = input;
    var newInput = this.state[inputName];
    newInput.value = value;
    newInput.error.active = validated;
    this._updateFields();
    if(this._isMounted){
      this.setState({
        inputName: newInput
      });
    }
  }

  _updateFields() {
    let currentFields = [
      this.state.currentPassword,
      this.state.password,
      this.state.confirmPassword
    ];
    let fields = [];
    for (let i = 0; i <= currentFields.length - 1; i++) {
      let input = currentFields[i];
      fields.push(input);
    }
    if(this._isMounted){
      this.setState({
        fields: fields
      });
    }
  }

  _updateAdminPassword = data => {
    const user_data = [
      {
        email: data.email,
        password: data.password
      }
    ];

    user
      .updateAdminPassword(user_data)
      .then(res => {
        console.log(res);
      })
      .catch(err => console.log(err));
  };

  _goToNext() {
    if(this._isMounted){
      this.setState({
        formError: {
          active: false,
          message: ""
        }
      });
    }
    var inputs = [];
    if (this.state.password.value !== "") {
      inputs = [
        {
          userId: this.state.userId,
          currentPassword: this.state.currentPassword.value,
          password: this.state.password.value,
          confirmPassword: this.state.confirmPassword.value
        }
      ];

      if (this.state.password.value === this.state.confirmPassword.value) {
        user
          .changePassword(inputs)
          .then(res => {
            //this._updateAdminPassword(res);
            if (res.status != 401) {
              if(this._isMounted){
                this.setState({
                  formSuccess: {
                    active: true,
                    message: "Your password has been successfully updated"
                  }
                }, function(){
                  this.props.logout();
                });
              }
            } else {
              if(this._isMounted){
                this.setState({
                  formError: {
                    active: true,
                    message: res.message
                  }
                });
              }
            }
          })
          .catch(e => {
            if (e.error === "This email is already in use") {
              var newInput = this.state["email"];
              newInput.error.active = true;
              newInput.error.message = e.error;
              if(this._isMounted){
                this.setState({
                  inputName: newInput
                });
              }
            } else {
              if(this._isMounted){
                this.setState({
                  formError: {
                    active: true,
                    message: e.error
                  }
                });
              }
            }
          });
      } else {
        if(this._isMounted){
          this.setState({
            formError: {
              active: true,
              message: "New Password and Confirm Password do not match"
            }
          });
        }
      }
    } else {
      console.log("ERROR PASWORD EMPTY");
    }
  }
}

const noteWrapper = css({
  boxShadow: '0 5px 10px rgba(0,0,0,.050)',
  padding: '10px',
  fontSize: 15,
  borderLeft: `2px solid orange`,
  '> span': {
    display: 'block'
  }
})

let errorStyle = css({
  color: "red"
});

let imageStyle = css({
  borderRadius: "25%",
  border: "1px solid rgba(204, 204, 204, 0.3)",
  height: "100px",
  width: "100px",
  align: "center",
  //  color: 'rgb(54, 69, 99)',
  "@media(max-width: 767px)": {
    height: "75px",
    width: "75px"
  }
});

let dropStyle = css({
  marginRight: "auto",
  marginLeft: "auto",
  display: "block",
  width: "100px",
  height: "100px",
  padding: "5px",
  display: "flex",
  justifyContent: "flex-end",
  //  backgroundColor: '#000',
  border: "2px solid #36c4f1",
  color: "#000",
  borderRadius: "25%",
  //margin: '5px',
  fontSize: "12px",
  cursor: "pointer",
  ":hover": {
    backgroundColor: "#ED81B9",
    border: "2px solid #ED81B9",
    color: "white"
  }
});

const styles = {
  container: {
    fontFamily: '"Roboto",sans-serif',
    display: "-webkit-flex",
    display: "flex",
    flex: "1 0 100%",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  introInfo: {
    maxWidth: "360px",
    fontFamily: '"Roboto",sans-serif',
    color: "#364563",
    fontSize: "14px",
    margin: "20px",
    textAlign: "justified"
  },
  register: {
    textDecoration: "none",
    margin: "auto",
    marginBottom: "5px",
    color: "#364563",
    fontSize: "14px",
    fontFamily: '"Roboto",sans-serif'
  }
};
const btn = css({
  textAlign: "center",
  padding: "5px",
  fontSize: "small",
  background: "#36c4f1",
  color: "#fff",
  width: "100px",
  borderRadius: "6px",
  //border: '1px solid',
  cursor: "pointer"
});

let warning = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  // fontSize: "large",
  color: "orange",
  textAlign: "center",
  margin: "10px 0 5px 0px"
});
