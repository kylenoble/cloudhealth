import React from "react";
import EmailService from "../../utils/emailService.js";
import Form from "../Form";
import Colors from "../NewSkin/Colors.js";
import {
  headerAndFooterHeight,
  checkScreenResolution
} from "../NewSkin/Methods";
import CenteredWrapper from "../NewSkin/Wrappers/CenteredWrapper";
import ContainedWrapper from "../NewSkin/Wrappers/ContainedWrapper";
import TwoColumnWrapper from "../NewSkin/Wrappers/TwoColumnWrapper";
import Text from "../NewSkin/Components/Text";
import FormWrapper from "../NewSkin/Wrappers/FormWrapper";
import GridContainer from "../NewSkin/Wrappers/GridContainer.js";
import Button from "../NewSkin/Components/Button.js";

const email = new EmailService();
export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      companies: [],
      departments: [],
      branches: [],
      fields: [],
      email: {
        id: "",
        name: "email",
        question: "Enter valid Email",
        type: "email",
        validation: "email",
        options: [],
        error: {
          active: false,
          message: "Please enter a valid email address"
        },
        value: ""
      },
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      },
      buttonName: "Reset Password",
      buttonLocked: false
    };

    this._isMounted = false;

    this._goToNext = this._goToNext.bind(this);
    // this._updateFields = this._updateFields.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;

    if (this._isMounted) {
      const body = document.body;
      body.addEventListener("resize", this.resizing());
      body.onresize = this.resizing;
      body.style = `background: ${Colors.skyblue} !important`;

      const lowReso = checkScreenResolution();
      setTimeout(() => {
        const headerAndFooter = headerAndFooterHeight();
        this.setState({ pageLoaded: true, headerAndFooter });
      }, 200);

      this.setState({ lowReso });

      // this._updateFields();
    }

    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // eslint-disable-next-line no-undef
      flexibility(document.getElementById("ch-reset"));
    }
  }

  resizing = () => {
    const screensize = checkScreenResolution();
    this.setState({ screenSize: screensize });
  };

  componentWillUnmount() {
    this._isMounted = false;
    document.body.removeEventListener("resize", this.resizing);
  }

  supportsFlexBox() {
    var test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  getHeaderAndFooterHeight = () => {
    const headerAndFooter = headerAndFooterHeight();
    this.setState({ headerAndFooter });
  };

  componentWillUnmount() {
    this._isMounted = false;
  }

  randomsort(a, b) {
    return Math.random() > 0.5 ? -1 : 1;
  }

  _generatePassword() {
    // GENERATE PASSWORD, MUST HAVE CAPITAL LETTER, SMALL LETTER AND NUMBER
    var length = 3,
      flength = 9,
      charset1 = "abcdefghijklmnopqrstuvwxyz",
      charset2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
      charset3 = "0123456789",
      capitals = "",
      small = "",
      numbers = "",
      holder = "",
      genPassword = "";
    for (var i = 0, n = charset1.length; i < length; ++i) {
      capitals += charset1.charAt(Math.floor(Math.random() * n));
      small += charset2.charAt(Math.floor(Math.random() * n));
    }
    for (var i = 0, n = charset3.length; i < length; ++i) {
      numbers += charset3.charAt(Math.floor(Math.random() * n));
    }

    holder = capitals + small + numbers;
    // shuffle it
    genPassword = holder.split("").sort(this.randomsort);
    return (genPassword = genPassword.join(""));
  }

  render() {
    this._generatePassword();

    const header = (<>
      <Text
        size={1}
        weight={600}
        color={Colors.blueDarkAccent}
        styles={{ marginBottom: 30, textTransform: 'uppercase', fontSize: 25 }}
      >
        Password Reset
      </Text>
      <Text
        size={1}
        weight={500}
        color={Colors.blueDarkAccent}
        styles={{ marginBottom: 20 }}
      >
        Please input your email address associated to your CloudHealthAsia account.
      </Text>
      </>
    );

    return (
      <div id="ch-reset" style={styles.maindiv}>
        <ContainedWrapper
          styles={{ marginTop: 0 }}
          headerAndFooterHeight={this.state.headerAndFooter}
        >
          <TwoColumnWrapper
            lowReso={this.state.lowReso}
            withImage={true}
            template="0.6fr 0.4fr"
            styles={{ lineHeight: 1, gridColumnGap: 100 }}
          >
            <CenteredWrapper to="left">
              <section >{header}</section>
              <FormWrapper
                deviceSize={this.props.screenSize}
                textAlign="left"
                onMobileTextAlign="center"
              >
                <React.Fragment>
                  <div id="form-container" style={styles.formContainer}>
                    <GridContainer
                      columnSize={'1fr'}
                    >
                      <input
                        type="email"
                        style={
                          this.state.formError.active
                          ? { ...styles.formInput, ...styles.formInputError }
                          : styles.formInput
                        }
                        onChange={this._handleChange.bind(this)}
                        value={this.state.email.value}
                      />
                      <span
                        style={
                          this.state.formError.active
                            ? { ...styles.validationText, ...styles.validationTextVisible }
                            : styles.validationText
                        }
                      >
                        Email address is invalid
                      </span>
                    </GridContainer>
                    <Button type={this.state.buttonLocked ? "disabled" : "pink"}
                      styles={styles.button}
                    >
                      <button
                        onClick={this._goToNext}
                      >
                        {this.state.buttonName}
                      </button>
                    </Button>
                  </div>
                </React.Fragment>
              </FormWrapper>
            </CenteredWrapper>
          </TwoColumnWrapper>
        </ContainedWrapper>
      </div>
    );
  }

  _handleChange(event) {
    let value = event.target.value;
    const validated = this._validateInput();
    var newInput = this.state.email;
    newInput.value = value;
    newInput.error.active = validated;

    // this._updateFields();
    this.setState({
      inputName: newInput
    });
  }

  _updateFields() {
    let currentFields = [this.state.email];
    let fields = [];
    for (let i = 0; i <= currentFields.length - 1; i++) {
      let input = currentFields[i];
      fields.push(input);
    }

    this.setState({
      fields: fields
    });
    console.log(this.state.fields)
  }

  _sendNewPassword(detail) {
    // send email to users
    email
      .sendEmail(detail[0].email, detail[0].password, detail[0].name)
      .then(res => {
        console.log("sending email ");
        this.setState({
          formSuccess: {
            active: true,
            message: "New Password was sent to your email address"
          }
        });
        return this.props.onClick("success");
      })
      .catch(err => {
        console.log("Sending email error");
        console.log(err);
        this.setState({
          formError: {
            active: true,
            message:
              "Error Sending email : please check your internet connection"
          },
          buttonName: "Try Again",
          buttonLocked: false
        });
      });
  }

  _resetPassword(id, name) {
    // generate Random Password
    this.setState({
      buttonLocked: true
    });
    let inputs = [
      {
        name: name,
        email: this.state.email.value,
        password: this._generatePassword()
      }
    ];
    email
      .reset(id, inputs)
      .then(res => {
        if (res) {
          this.setState({
            buttonName: "Sending e-mail, please wait...."
          });
          this._sendNewPassword(inputs);
        } else {
          this.setState({
            formError: {
              active: true,
              message: "There was an error resetting your password"
            }
          });
        }
      })
      .catch(e => {
        console.log("Problem locating your email!");
        console.log(e);
      }); // you would show/hide error messages with component state here
  }

  _validateInput() {
    const ck_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if (!ck_email.test(this.state.email.value)) {
      return false;
    }
    return true;
  }

  _goToNext() {
    if(!this._validateInput()){
      this.setState({
        formError: {
          active: true,
          message: ""
        }
      });
      return;
    }
    this.setState({
      formError: {
        active: false,
        message: ""
      }
    });

    // get record with that email
    email
      .get(this.state.email.value)
      .then(res => {
        if (res.length > 0) {
          this.setState({
            buttonName: "Please wait...."
          });
          this._resetPassword(res[0].id, res[0].name);
        } else {
          return this.props.onClick("success");
        }
      })
      .catch(e => {
        console.log(e);
      });
  }
}

const styles = {
  formContainer: {
    width: '100%',
    position: 'relative',
    maxWidth: "100%",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  formInput: {
    padding: "13px 30px 13px 30px",
    marginTop: "20px",
    width: "247px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px",
    fontSize: "14px",
    outline: "none"
  },
  formInputError: {
    border: "2px solid #EA3131 !important",
    transition: "color 0.3s ease-in-out"
  },
  button: {
    marginTop: "20px"
  },
  validationText: {
    display: "none",
    fontSize: "0.8em",
    transition: "color 0.3s ease-in-out",
    background: Colors.movementColor,
    padding: '5px',
    color: 'white',
    textAlign: 'center',
    margin: '5px 0 0 !important',
    borderRadius: 5,
    padding: '10px'
  },
  validationTextVisible: {
    display: "block",
    margin: '5px 0 0 !important'
  }
};
