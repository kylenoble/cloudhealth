import React from "react";
import moment from "moment-timezone";
import jstz from "jstz";
import { css } from "glamor";

import AppointmentService from "../../utils/appointmentService.js";

const appointment = new AppointmentService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      queuedPatients: [],
      loadingQueue: true
    };

    this._showQueuedPatients = this._showQueuedPatients.bind(this);
    this._getQueuedPatients = this._getQueuedPatients.bind(this);
    this._startAppointment = this._startAppointment.bind(this);
  }

  componentWillMount() {
    this._getQueuedPatients();
  }

  componentDidMount() {
    this._getQueuedPatients();
    // let intervalId = setInterval(() => {
    //     this._getQueuedPatients();
    // }, 10000);

    // store intervalId in the state so it can be accessed later:
    // this.setState({intervalId: intervalId});
  }

  componentWillUnmount() {
    // use intervalId from the state to clear the interval
    // clearInterval(this.state.intervalId);
  }

  render() {
    return (
      <div className={container}>
        <div style={styles.tableHeader}>
          <h1 style={styles.headerItem}>Patient</h1>
          <h1 style={styles.headerItem}>Date</h1>
          <h1 style={styles.headerItem}>Action</h1>
        </div>
        {this._renderView()}
      </div>
    );
  }

  _getQueuedPatients() {
    appointment
      .getAppointmentsList({ active: true })
      .then(res => {
        if (res.name === "Error" || res.message === "Unauthorized") {
          this.setState({
            loadingQueue: false
          });
        } else {
          this.setState({
            queuedPatients: res,
            loadingQueue: false
          });
        }
      })
      .catch(e => {
        console.log(e);
        this.setState({
          loadingQueue: false
        });
      }); // you would show/hide error messages with component state here
  }

  _renderView() {
    if (this.state.loadingQueue) {
      return <div>Loading...</div>;
    } else {
      return this._showQueuedPatients();
    }
  }

  _showQueuedPatients() {
    if (this.state.queuedPatients.length < 1) {
      return (
        <span key={1} style={styles.emptyState}>
          No Patients Waiting
        </span>
      );
    }
    let html = [];
    this.state.queuedPatients.forEach((currentItem, index) => {
      html.push(
        <div key={index} style={styles.itemContainer}>
          <h1
            style={{ ...styles.patientItem, ...styles.patientItemLarge }}
            className="patient-item"
          >
            {currentItem.name}
          </h1>
          <h1
            style={{ ...styles.patientItem, ...styles.patientItemSmall }}
            className="patient-item"
          >
            {moment(currentItem.datetime).format("hh:mma")}
          </h1>
          <button
            style={styles.startAppointment}
            className="patient-button"
            onClick={() =>
              this._startAppointment(currentItem.room_name, currentItem.id)
            }
          >
            Start
          </button>
          <button
            style={styles.startAppointment}
            className="patient-button"
            onClick={() => this.props.patientHealthInfo(currentItem.patient_id)}
          >
            Info
          </button>
          <style jsx>{`
            @media (max-width: 500px) {
              .patient-item {
                flex: 1 0 100% !important;
                text-align: center;
                margin-right: 0 !important;
                margin-left: 0 !important;
              }
              .patient-button {
                flex: 1 0 50% !important;
                margin-left: 0 !important;
              }
              .table-container: {
                justifycontent: center !important;
                align-items: center !important;
              }
            }
          `}</style>
        </div>
      );
    });

    return html;
  }

  _startAppointment(roomName, id) {
    this.props.startAppointment(roomName, id);
  }
}

const container = css({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "flex-start",
  width: "80%",
  marginTop: "80px",
  fontFamily: '"Roboto-Medium",sans-serif',
  paddingLeft: "20px",
  "@media(max-width: 700px)": {
    marginTop: "100px"
  }
});

const styles = {
  tableHeader: {
    display: "flex",
    flexDirection: "row",
    alignItems: "left",
    justifyContent: "left",
    borderBottom: "1px solid #E1E8EE",
    flexWrap: "wrap",
    width: "100%",
    marginBottom: "10px"
  },
  headerItem: {
    textAlign: "center",
    fontFamily: "Roboto-Medium",
    fontSize: "14px",
    color: "#BDC7CF",
    letterSpacing: 0,
    padding: "10px",
    cursor: "pointer",
    flex: "1 0 auto"
  },
  itemContainer: {
    display: "flex",
    flexDirection: "row",
    alignItems: "left",
    justifyContent: "left",
    flexWrap: "wrap",
    width: "100%"
  },
  patientItem: {
    fontFamily: "Roboto-Medium",
    fontSize: "16px",
    color: "#364563",
    letterSpacing: 0,
    lineHeight: "14px",
    width: "20%",
    padding: "10px",
    cursor: "pointer",
    textAlign: "left",
    flex: "1 0 auto"
  },
  emptyState: {
    marginTop: "30px"
  },
  startAppointment: {
    fontFamily: '"Roboto-Medium",sans-serif',
    backgroundColor: "#03D27F",
    width: "auto",
    height: "30px",
    margin: "5px",
    border: "none",
    borderRadius: "6px",
    fontSize: "14px",
    color: "white",
    cursor: "pointer",
    float: "right",
    flex: "1 0 auto"
  }
};
