import moment from "moment-timezone";
import { css } from "glamor";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      time: 0,
      isOn: false,
      start: 0
    };
    this.startTimer = this.startTimer.bind(this);
    this.stopTimer = this.stopTimer.bind(this);
    this.resetTimer = this.resetTimer.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.timer.start) {
      if (this.state.isOn === false) {
        this.startTimer();
      }
    } else {
      if (this.state.isOn === true) {
        this.stopTimer();
      }
    }

    // if(nextProps.timer.stop){
    //     console.log('stop')
    //     this.stopTimer()
    //   }
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId);
  }

  startTimer() {
    this.setState({
      isOn: true,
      time: this.state.time,
      start: Date.now() - this.state.time
    });

    let intervalId = setInterval(() => {
      this.setState({
        time: Date.now() - this.state.start
      });
    }, 1000);
    this.setState({ intervalId: intervalId });
  }
  stopTimer() {
    this.setState({ isOn: false });
    clearInterval(this.state.intervalId);

    var elapsed = Math.round(this.state.time / 100);
    var seconds = (elapsed / 10).toFixed(1);
    let duration = moment.utc(seconds * 1000).format("HH:mm:ss");
    //alert(duration)
    this.props.duration(duration);
  }
  resetTimer() {
    this.setState({ time: 0, isOn: false });
  }
  render() {
    var elapsed = Math.round(this.state.time / 100);
    var seconds = (elapsed / 10).toFixed(1);
    let duration = moment.utc(seconds * 1000).format("HH:mm:ss");
    return (
      <div>
        <div className={durationStyle}>Duration: {duration}</div>
      </div>
    );
  }
}

let durationStyle = css({
  fontSize: "x-small",
  fontStyle: "italic"
});
