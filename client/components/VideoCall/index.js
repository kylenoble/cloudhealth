/* eslint-disable no-nested-ternary */
/* eslint-disable radix */
import React, { Component } from "react";
import moment from "moment-timezone";
import { css } from "glamor";
import Video from "twilio-video";
import { confirmAlert } from "react-confirm-alert";
import ReactTooltip from "react-tooltip";
import { RaisedButton } from "material-ui";
import { tooltipWrapper } from "../Commons/tooltip";
import VideoCallService from "../../utils/videoCallService";
import Timer from "./timer";
import Constants from "../../constants";
import AppointmentService from "../../utils/appointmentService";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Button from "../NewSkin/Components/Button";
import {
  grid_container,
  combined,
  scrollClass,
  inputClass
} from "../NewSkin/Styles";

const videocall = new VideoCallService();
const hoursPerSlot = Constants.HOURSPERSLOT;
const appointment = new AppointmentService();

const warnTime = 60 * 60;
const lockTime = 0;

export default class VideoComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      videoCall_log: [],
      extensionTime: "",
      totalExtension: 0,
      addingTime: false,
      timeRemaining: 0,
      timer: { start: false },
      minimizeLocalTrack: false,
      identity: null,
      roomName: this.props.appointment_detail.room_name,
      roomNameErr: false,
      previewTracks: null,
      localMediaAvailable: false,
      hasJoinedRoom: false,
      activeRoom: "",
      appointment: this.props.appointment_detail,
      detailsOn: true,
      disabledJoinBtn: false
    };
    this.joinRoom = this.joinRoom.bind(this);
    this.handleRoomNameChange = this.handleRoomNameChange.bind(this);
    this.roomJoined = this.roomJoined.bind(this);
    this.leaveRoom = this.leaveRoom.bind(this);
    this.detachTracks = this.detachTracks.bind(this);
    this.detachParticipantTracks = this.detachParticipantTracks.bind(this);
    this.message = this.message.bind(this);
    this._duration = this._duration.bind(this);
    this._addTime = this._addTime.bind(this);
  }

  handleWindowClose = e => {
    e.preventDefault();
    return (e.returnValue =
      "You have unsaved changes - are you sure you wish to close?");
  };

  componentDidMount() {
    const data = {
      id: this.state.appointment.id,
      user_id: this.props.user.id,
      user_name: this.props.user.name
    };
    videocall.get(data).then(results => {
      this.setState({ identity: results.identity, token: results.token });
    });

    const intervalId = setInterval(() => {
      this.setState({
        curTime: new Date().toLocaleString(),
        timeRemaining: this._getTimeRaimaining()
      });

      if (
        this._getTimeRaimaining() <= lockTime &&
        !this.state.addingTime &&
        this.state.hasJoinedRoom
      ) {
        this._leaveRoom();
      }

      if (this._getTimeRaimaining() <= lockTime && !this.state.addingTime) {
        this.props.toggleConference(false);
      }
    }, 1000);
    this.setState({ intervalId });
  }

  componentWillUnmount() {
    window.removeEventListener("beforeunload", this.handleWindowClose);
    this._stopTrack();
    this._leaveRoom();
    clearInterval(this.state.intervalId);
  }

  // Get the Participant's Tracks.
  getTracks(participant) {
    return Array.from(participant.tracks.values())
      .filter(publication => publication.track)
      .map(publication => publication.track);
  }

  _preview() {
    this.setState({ localMediaAvailable: true });
    const localTracksPromise = this.previewTracks
      ? Promise.resolve(this.previewTracks)
      : Video.createLocalTracks();

    localTracksPromise.then(
      tracks => {
        // eslint-disable-next-line no-undef
        window.previewTracks = this.previewTracks = tracks;
        // eslint-disable-next-line no-undef
        const previewContainer = document.getElementById("local-media");
        if (!previewContainer.querySelector("video")) {
          this.attachTracks(tracks, previewContainer);
        }
      },
      () => {
        this.message("Unable to access Camera and Microphone");
      }
    );
  }

  _leaveRoom() {
    window.removeEventListener("beforeunload", this.handleWindowClose);

    if (this.state.activeRoom) this.state.activeRoom.disconnect();

    this.setState({
      hasJoinedRoom: false,
      localMediaAvailable: false,
      disabledJoinBtn: false
    });
    this.message("Disconnected");
  }

  leaveRoom() {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: "Are you sure you want to leave this Room?", // Message dialog
      buttons: [
        {
          label: "Yes, I'm sure.",
          onClick: () => this._leaveRoom()
        },
        {
          label: "Do not leave the Room."
        }
      ]
    });
  }

  handleRoomNameChange(e) {
    const roomName = e.target.value;
    this.setState({ roomName });
  }

  // A new RemoteTrack was published to the Room.
  trackPublished(publication, container) {
    if (publication.isSubscribed) {
      this.attachTrack(publication.track, container);
    }
    publication.on("subscribed", track => {
      //console.log(`Subscribed to ${publication.kind} track`);
      this.attachTrack(track, container);
    });
    publication.on("unsubscribed", this.detachTrack);
  }

  // A RemoteTrack was unpublished from the Room.
  trackUnpublished(publication) {
    //console.log(`${publication.kind} track was unpublished.`);
  }

  // A new RemoteParticipant joined the Room
  participantConnected(participant, container) {
    //console.log("NEW PARTICIPANT", participant);
    const timer = this.state.timer;
    if (timer.start === false) timer.start = true;
    this.setState({ minimizeLocalTrack: true, timer });

    this.setState({ minimizeLocalTrack: true });
    participant.tracks.forEach(publication => {
      this.trackPublished(publication, container);
    });
    participant.on("trackPublished", publication => {
      this.trackPublished(publication, container);
    });
    participant.on("trackUnpublished", this.trackUnpublished);
  }

  joinRoom() {
    this.setState({ disabledJoinBtn: true });
    if (!this.state.roomName.trim()) {
      this.setState({ roomNameErr: true });
      return;
    }

    this.message("Connecting...");
    const connectOptions = {
      name: this.state.roomName
    };

    if (this.state.previewTracks) {
      connectOptions.tracks = this.state.previewTracks;
    }

    // Join the Room with the token from the server and the
    // LocalParticipant's Tracks.
    Video.connect(this.state.token, connectOptions).then(
      this.roomJoined,
      error => {
        // eslint-disable-next-line no-undef
        alert(
          `An unexpected error has occurred. Please refresh the page and try again.`
        );
        console.log(
          `An unexpected error has occurred. Please refresh the page and try again: ${error.message}`
        );
      }
    );
  }

  // Attach the Track to the DOM.
  attachTrack(track, container) {
    container.appendChild(track.attach());
  }

  attachTracks(tracks, container) {
    // const timer = this.state.timer;
    // if (tracks.length > 0) {
    //   if (timer.start === false) timer.start = true;
    //   this.setState({ minimizeLocalTrack: true, timer });
    // } else {
    //   if (timer.start === true) timer.start = false;
    //   this.setState({ minimizeLocalTrack: false, timer });
    // }
    tracks.forEach(track => {
      this.attachTrack(track, container);
    });

    //	this.setState({timer})
  }

  // Attaches a track to a specified DOM container
  attachParticipantTracks(participant, container) {
    const tracks = Array.from(participant.tracks.values());
    this.attachTracks(tracks, container);
  }

  // Detach given track from the DOM
  detachTrack(track) {
    track.detach().forEach(element => {
      element.remove();
    });
  }

  detachTracks(tracks) {
    // const timer = this.state.timer;
    // if (tracks.length === 1) {
    //   if (timer.start === false) timer.start = true;

    //   this.setState({ minimizeLocalTrack: true, timer });
    // } else {
    //   if (timer.start === true) timer.start = false;

    //   this.setState({ minimizeLocalTrack: false, timer });
    // }
    tracks.forEach(track => {
      this.detachTrack(track);
    });

    //	this.setState({timer})
  }

  // Detach the Participant's Tracks from the DOM.
  detachParticipantTracks(participant) {
    const tracks = this.getTracks(participant);
    tracks.forEach(this.detachTrack);
  }

  message(message) {
    const logtime = moment().format("LTS");
    const { videoCall_log } = this.state;

    videoCall_log.push({ logtime, message });
    //console.log("call log", videoCall_log);
    // this.setState({
    //   videoCall_log
    // });
    const logItem = `
      <span>
        <section className={${grid_container(
      "1fr 1fr"
    )}} style="font-size: .9em">
          <strong>${logtime}: </strong>
          <span>${message}</span>
        </section>
      </span>`;
    // const logDiv = this.refs.log;
    // if (!this.props.minimized) {
    //   // logDiv.innerHTML += `<p>&nbsp; ${logtime} &gt;&nbsp;${message}</p>`;
    //   logDiv.innerHTML += logItem;
    //   logDiv.scrollTop = logDiv.scrollHeight;
    // }
  }

  _showLog() {
    const { videoCall_log } = this.state;
    if (videoCall_log && videoCall_log.length > 0) {
      return videoCall_log.map((log, k) => {
        return (
          <span key={k}>
            <section>
              <strong>{log.logtime}: </strong>
              <span>{log.message}</span>
            </section>
          </span>
        );
      });
    }
    return null;
  }

  _updateRoot() {
    this.props.updateroot(this.state);
  }

  _getTimeRaimaining() {
    const index = this.props.queuedPatients.findIndex(
      x => x.id === this.props.appointment_detail.id
    );
    const b = moment(); //now
    const a = moment(this.props.queuedPatients[index].datetime).add(
      hoursPerSlot,
      "hours"
    );
    if (
      this.props.queuedPatients[index].time_extension &&
      this.props.queuedPatients[index].time_extension > 0
    ) {
      a.add(this.props.queuedPatients[index].time_extension, "minutes");
    }

    const extension = this.state.totalExtension * 60;
    const secondsRemaining = a.diff(b, "seconds");

    return secondsRemaining + extension;
  }

  roomJoined(room) {
    window.addEventListener("beforeunload", this.handleWindowClose);
    const identity = this.state.identity.split("_");
    this.message(`Connected as ${identity[1]}`);
    this.setState({
      activeRoom: room,
      localMediaAvailable: true,
      hasJoinedRoom: true
    });

    let previewContainer = this.refs.localMedia;

    // Attach LocalParticipant's Tracks, if not already attached.
    if (!previewContainer.querySelector("video")) {
      this.attachTracks(
        this.getTracks(room.localParticipant),
        previewContainer
      );
    }

    // Attach the Tracks of the Room's Participants.
    room.participants.forEach(participant => {
      const participant_identity = participant.identity.split("_");
      this.message(`Already in Room: '${participant_identity[1]}'`);
      previewContainer = this.refs.remoteMedia;
      this.participantConnected(participant, previewContainer);
    });

    // When a Participant joins the Room, log the event.
    room.on("participantConnected", participant => {
      previewContainer = this.refs.remoteMedia;
      this.message("Participant Connected");
      this.participantConnected(participant, previewContainer);
    });

    // When a Participant adds a Track, attach it to the DOM.
    room.on("trackAdded", track => {
      //console.log("TRACK ADDED");
      previewContainer = this.refs.remoteMedia;
      this.attachTracks([track], previewContainer);
    });

    // When a Participant removes a Track, detach it from the DOM.
    room.on("trackRemoved", track => {
      this.detachTracks([track]);
    });

    // When a Participant leaves the Room, detach its Tracks.
    room.on("participantDisconnected", participant => {
      const identity = participant.identity.split("_")[1];
      this.message(`Participant : ${identity} left the room`);
      this.detachParticipantTracks(participant);
      const { timer } = this.state;
      timer.start = false;
      this.setState({ minimizeLocalTrack: false, timer });
    });

    // Once the LocalParticipant leaves the room, detach the Tracks
    // of all Participants, including that of the LocalParticipant.
    room.on("disconnected", () => {
      this._stopTrack();
      const { timer } = this.state;
      let { minimizeLocalTrack } = this.state;
      minimizeLocalTrack = false;

      timer.start = false;

      this.detachParticipantTracks(room.localParticipant);
      room.participants.forEach(this.detachParticipantTracks);
      this.state.activeRoom = null;
      this.setState({
        hasJoinedRoom: false,
        localMediaAvailable: false,
        timer,
        minimizeLocalTrack,
        disabledJoinBtn: false
      });
    });

    //this._updateRoot();
  }

  _stopTrack() {
    if (this.previewTracks) {
      this.previewTracks.forEach(track => {
        track.stop();
      });
    }
  }

  _duration(duration) {
    this.props.getDuration(duration);
  }

  _addTime() {
    // eslint-disable-next-line no-undef
    this.setState({ addingTime: true });
    const timeValue = this.state.extensionTime;
    let withNextHrAppt = "";

    if (timeValue <= 0) {
      // eslint-disable-next-line no-undef
      return;
    }

    //check if with another appointment for the next hour
    const nextHrAppt = this.props.queuedPatients.filter(x => {
      return (
        moment(x.datetime).format("Y-MM-DD hh:mm") ==
        moment(this.props.appointment_detail.datetime)
          .add(1, "hour")
          .format("Y-MM-DD hh:mm")
      );
    });
    if (nextHrAppt.length > 0) {
      withNextHrAppt = "You have another appointment for the next hour. ";
    }

    // eslint-disable-next-line no-undef
    const confirmed = confirm(
      `${withNextHrAppt}Are you sure you want to extend the ongoing appointment by ${timeValue} minutes?`
    );

    if (timeValue > 0 && confirmed) {
      const index = this.props.queuedPatients.findIndex(
        x => x.id === this.props.appointment_detail.id
      );
      const time_extension =
        parseInt(this.props.queuedPatients[index].time_extension) || 0;
      const data = {
        time_extension: time_extension + parseInt(timeValue)
      };

      return appointment
        .update(this.props.appointment_detail.id, data)
        .then(res => {
          if (res) {
            this.message(
              `Appointment has been extended by ${timeValue} minutes`
            );
            this.setState({
              totalExtension: timeValue,
              extensionTime: 0,
              addingTime: false
            });
          }
        })
        .catch(e => {
          console.log(`Error Extending time ${e}`);
        });
    }
  }

  render() {
    const showLocalTrack = this.state.localMediaAvailable ? (
      <div>
        <div
          className={this.state.minimizeLocalTrack ? smallDiv : localTrackDiv}
          ref="localMedia"
          id="local-media"
        //style={{ position: "relative", width: "100%" }}
        />
      </div>
    ) : (
        ""
      );

    const minimized = this.props.minimized;

    const timeInput = (
      <React.Fragment>
        <input
          className={inputClass}
          style={{ flex: 1, margin: 'auto', height: 'fit-content !important' }}
          // id="timeToExtend"
          // name="timeToExtend"
          value={this.state.extensionTime}
          type="number"
          min={0}
          placeholder={"Minutes to add"}
          onChange={e => this.setState({ extensionTime: e.target.value })}
        />
        <Button
          styles={{
            margin: "10px auto",
            minWidth: this.props.miniOn ? 90 : 230
          }}
        >
          <button
            onClick={this._addTime}
            style={{
              textTransform: "uppercase",
              fontWeight: 500,
              padding: this.props.miniOn ? 10 : "10px 20px"
            }}
          >
            Extend Time
          </button>
        </Button>
      </React.Fragment>
    );

    // Hide 'Join Room' button if user has already joined a room.
    const joinOrLeaveRoomButton = this.state.hasJoinedRoom ? (
      <Button
        type="pink"
        styles={{
          margin: minimized
            ? "auto auto"
            : this.props.user.type === "Doctor"
              ? "20px auto"
              : "10px auto",
          maxWidth: minimized ? "120px !important" : null,
          minWidth: minimized ? "120px !important" : null
        }}
      >
        <button
          onClick={this.leaveRoom}
          style={{ fontWeight: 500, textTransform: "uppercase" }}
        >
          Leave{minimized ? "" : " Room"}
        </button>
      </Button>
    ) : (
        <Button
          type={
            this.state.disabledJoinBtn || this._getTimeRaimaining() <= lockTime
              ? `disabled`
              : `blue`
          }
          styles={{
            margin: minimized ? "auto auto" : "20px auto",
            maxWidth: this.props.miniOn ? "120px !important" : null,
            minWidth: this.props.miniOn ? "120px !important" : "unset"
          }}
        >
          <button
            disabled={
              this.state.disabledJoinBtn || this._getTimeRaimaining() <= lockTime
            }
            onClick={this.joinRoom}
            style={{ textTransform: "uppercase", fontWeight: 600 }}
          >
            Join Room
        </button>
        </Button>
      );

    //var elapsed = Math.round((this._getTimeRaimaining()*60) / 100);
    const seconds = this._getTimeRaimaining();
    let hourMinute = moment.utc(0 * 1000).format("HH:mm:ss");
    if (seconds > 0) {
      hourMinute = moment.utc(seconds * 1000).format("HH:mm:ss");
    }

    const tooltipMessage = {
      description: "Show / Hide Below Details"
    };

    return (
      <section
        className={minimized ? minimize : ""}
        style={{ width: minimized ? "calc(100% - 50px)" : "100%" }}
      >
        <div
          className={locaCamDiv(minimized, this.state.localMediaAvailable)}
          id="cam-div"
        >
          {showLocalTrack}
          <div className="flex-item" ref="remoteMedia" id="remote-media">
            {this.state.minimizeLocalTrack ? (
              <div className={timerStyle}>{this.state.curTime}</div>
            ) : null}
          </div>
        </div>

        {!minimized ? (
          <>
            <GridContainer columnSize="1fr 1fr" classes={[timeWrapper]}>
              <div
                style={{ flex: "1 0 40%" }}
                className={
                  this._getTimeRaimaining() <= warnTime
                    ? warning
                    : timeLeftStyle
                }
              >
                Time left : {hourMinute}{" "}
              </div>
              <div style={{ flex: "1 0 40%" }}>
                <Timer timer={this.state.timer} duration={this._duration} />
              </div>
              <div style={{ flex: "1 0 20%" }}>
                <span
                  className={toggleStyle}
                  data-type="light"
                  data-html
                  id={`tooltip`}
                  data-tip={tooltipWrapper(tooltipMessage)}
                  onClick={() =>
                    this.setState({ detailsOn: !this.state.detailsOn })
                  }
                >
                  {!this.state.detailsOn ? (
                    <span>&nbsp;&darr;&nbsp;</span>
                  ) : (
                      <span>&nbsp;&uarr;&nbsp;</span>
                    )}
                </span>
              </div>
            </GridContainer>

            <GridContainer
              columnSize={!this.state.localMediaAvailable ? "1fr 140px" : "1fr"}
              gap={10}
              styles={{ maxWidth: 370, margin: "0 auto" }}
            >
              {!this.state.localMediaAvailable ? (
                <Button styles={{ margin: "20px auto", minWidth: "140px" }}>
                  <button
                    disabled={this.state.disabledJoinBtn}
                    style={{
                      textTransform: "uppercase",
                      fontWeight: 500,
                      padding: this.props.miniOn ? 10 : "10px 20px"
                    }}
                    id="button-preview"
                    onClick={this._preview.bind(this)}
                  >
                    {" "}
                    Preview My Camera
                  </button>
                </Button>
              ) : null}

              <section>
                {this.state.roomNameErr ? "Room Name is required" : undefined}
                {joinOrLeaveRoomButton}
              </section>
            </GridContainer>
            {this._getTimeRaimaining() <= warnTime &&
              this.props.user.type === "Doctor" ? (
                <GridContainer
                  styles={{ margin: "0px 20px 20px" }}
                  columnSize={"1fr 0.5fr"}
                  gap={10}
                >
                  {timeInput}
                </GridContainer>
              ) : null}

            {/* <div
                className={
                  this.state.detailsOn ? detailsOnStyle : detailsOffStyle
                }
              > */}
            {this.state.detailsOn && (
              <div
                ref="log"
                id="log"
                className={combined([logWrapper, scrollClass])}
                style={{
                  marginBottom: this.props.user.type === "Doctor" ? 20 : 0
                }}
              >
                {this._showLog()}
              </div>
            )}
            {/* </div> */}
          </>
        ) : (
            <>
              <span
                style={{ flex: 1, width: 80 }}
                className={
                  this._getTimeRaimaining() <= warnTime ? warning : timeLeftStyle
                }
              >
                Time left : {hourMinute}{" "}
              </span>
              {joinOrLeaveRoomButton}
            </>
          )}
        {/* <ReactTooltip /> */}
      </section>
    );
  }
}

const minimize = css({
  display: "grid",
  gridTemplateColumns: "1fr 1fr"
});

const timeWrapper = css({
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  background: "#eee6",
  "& div > div": {
    // padding: 5
  }
});

const logWrapper = css({
  background: "#eee6",
  padding: 10,
  margin: "0 10px 0",
  width: "auto",
  borderRadius: 2,
  maxHeight: 100,
  overflow: "auto",
  textAlign: "left"
});

let locaCamDiv = (minimized, available) =>
  css({
    position: "relative",
    display: minimized ? "none" : "inherit",
    width: minimized ? 70 : "100%",
    height: "auto",
    minHeight: minimized ? 50 : 100,
    // minWidth: 400,
    maxHeight: minimized ? 50 : available ? 400 : 100,
    backgroundImage: `url(static/images/camera_icon.png)`,
    backgroundPosition: "center",
    backgroundBlendMode: "overlay",
    backgroundRepeat: "no-repeat",
    backgroundSize: minimized ? "50%" : "20%",
    backgroundColor: "#eee",
    "& div#local-media video": {
      border: "none",
      display: "block"
      //maxWidth: 400
    }
  });

let timerStyle = css({
  position: "absolute",
  bottom: "5px",
  right: "5px",
  background: "gray",
  color: "white",
  opacity: 0.5,
  padding: 5,
  margin: 0,
  borderRadius: 5,
  fontStyle: "italic",
  fontSize: "x-small"
});

let smallDiv = css({
  position: "absolute",
  bottom: "5px",
  left: "5px",
  border: "1px solid #80cde9",
  transition: "300ms ease",
  width: "30%",
  height: "30%"
});

const localTrackDiv = css({
  transition: "300ms ease",
  display: "flex",
  justifyContent: "center"
});

let timeLeftStyle = css({
  background: "#555",
  color: "white",
  opacity: 0.5,
  padding: 5,
  margin: 0,
  fontStyle: "italic",
  fontSize: "x-small"
});

let warning = css(timeLeftStyle, {
  background: "red"
});

let inputDivStyle = css({
  display: "grid",
  width: "100%"
});

let inputStyle = css({
  border: "1px dotted gray",
  padding: 3,
  margin: "0 5px",
  borderRadius: 5,
  textAlign: "center"
});

const detailsOnStyle = css({
  transition: "300ms ease",
  height: "auto"
});

const detailsOffStyle = css({
  transition: "300ms ease",
  height: 0
});

const toggleStyle = css({
  width: 30,
  cursor: "pointer",
  borderRadius: "50%",
  background: "#000",
  color: "#fff",
  marginRight: 10,
  padding: 3,
  fontWeight: "bold",
  transition: "300ms ease",
  ":hover": {
    background: "gray"
  }
});
