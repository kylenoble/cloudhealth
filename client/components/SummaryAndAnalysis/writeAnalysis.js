import { css } from "glamor";
import TextareaAutosize from "react-autosize-textarea";
import AnalysisService from "../../utils/analysisService.js";
import { scrollClass, goodMessage, errorMsg, formMsgActive, flex_wrapper } from "../NewSkin/Styles";
import Colors from "../NewSkin/Colors.js";
import Button from "../NewSkin/Components/Button.js";

const analyis = new AnalysisService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.user = this.props.user;
    this.patientInfo = this.props.patientInfo;

    this.state = {
      formMessage: {
        success: "",
        error: ""
      },
      disabled: this.user.type === "Patient" ? true : false,
      fields: {
        assimilation: {
          disabled: false,
          label: "Assimilation",
          name: "assimilation",
          value: ""
        },
        communication: {
          disabled: false,
          label: "Communication",
          name: "communication",
          value: ""
        },
        defenseRepair: {
          label: "Defense and Repair",
          name: "defenseRepair",
          value: ""
        },
        biotransformationElimination: {
          label: "Biotransformation and Elimination",
          name: "biotransformationElimination",
          value: ""
        },
        energy: {
          disabled: false,
          label: "Energy",
          name: "energy",
          value: ""
        },
        transport: {
          disabled: false,
          label: "Transport",
          name: "transport",
          value: ""
        },
        structuralIntegrity: {
          disabled: false,
          label: "Structural Integrity",
          name: "structuralIntegrity",
          value: ""
        }
      },
      buttons: {
        disabled: false,
        finalButton: {
          label: "Finalize",
          disabled: true
        },
        draftButton: {
          label: "Save",
          disabled: true
        }
      },
      mode: {
        update: false
      }
    };
  }

  componentDidMount() {
    this._getAnalysis();
  }

  render() {
    let errmess = this.state.formMessage.error;
    let succmess = this.state.formMessage.success;

    return (
      <div
        style={{ height: "100%", overflow: "auto" }}
        className={`${flex_wrapper} ${scrollClass}`}
      >
        {this.user.type !== "Patient" && (
          <h2 className={summaryTitle}>WRITE ANALYSIS AND RECOMMENDATIONS</h2>
        )}

        {this.user.type === "Patient" && (
          <h2 className={summaryTitle}>ANALYSIS AND RECOMMENDATIONS</h2>
        )}

        <section style={{ width: "100%" }}>{this._displayFields()}</section>

        {this.user.type === "Doctor" ? (
          <Button type={this.state.buttons.draftButton.disabled ? "disabled" : "blue"} styles={{ marginTop: 20 }}>
            <button
              disabled={this.state.buttons.draftButton.disabled}
              className={
                this.state.buttons.draftButton.disabled ? disabledbtn : btn
              }
              onClick={() => {
                this._formSubmit(false);
              }}
            >
              {this.state.buttons.draftButton.label}
            </button>
          </Button>
        ) : null}
        {/**<button disabled={this.state.buttons.finalButton.disabled} className={this.state.buttons.finalButton.disabled? disabledbtn: btn} onClick={()=>{this._formSubmit(true)}}>{this.state.buttons.finalButton.label}</button>**/}
        <div
          className={[
            flex_wrapper,
            this.state.formMessage.error ? errorMsg : goodMessage,
            this.state.formMessage.error || this.state.formMessage.success ? formMsgActive : undefined
          ].join(" ")}
        >
          {succmess}
          {errmess}
        </div>
      </div>
    );
  }

  _setMode(bol) {
    let mode = this.state.mode;
    mode.update = bol;
    this.setState({
      mode
    });
  }

  _getAnalysis() {
    let buttons = this.state.buttons;
    analyis
      .get(this.patientInfo.id)
      .then(res => {
        if (res) {
          this._setMode(true);
          buttons.draftButton.label = "Update";
          this._setInitialFieldValue(res);
        } else {
          this._setMode(false);
          buttons.draftButton.label = "Save";
        }
      })
      .catch(err => {
        console.log(err);
      });

    this.setState({
      buttons
    });
  }

  _setInitialFieldValue(dta) {
    let data = dta;
    let fields = this.state.fields;
    let buttons = this.state.buttons;

    fields.assimilation.value = data.assimilation;
    fields.communication.value = data.communication;
    fields.defenseRepair.value = data.defense_and_repair;
    fields.biotransformationElimination.value =
      data.biotransformation_and_elimination;
    fields.energy.value = data.energy;
    fields.transport.value = data.transport;
    fields.structuralIntegrity.value = data.structural_integrity;

    if (data.length > 0) {
      buttons.finalButton.disabled = false;
    }

    if (data.status == 1) {
      this._lockFields(true);
      buttons.finalButton.disabled = true;
    }
    this.setState({
      buttons,
      fields
    });
  }

  _lockFields(bol) {
    let fields = this.state.fields;

    fields.assimilation.disabled = bol;
    fields.communication.disabled = bol;
    fields.defenseRepair.disabled = bol;
    fields.biotransformationElimination.disabled = bol;
    fields.energy.disabled = bol;
    fields.transport.disabled = bol;
    fields.structuralIntegrity.disabled = bol;
    this.setState({
      fields
    });
  }

  toggleWAForm(form) {
    this.props.toggleWAForm(form);
  }

  _formSubmit(bol) {
    let fields = this.state.fields;
    let mode = this.state.mode;
    let buttons = this.state.buttons;

    let data = {
      user_id: this.patientInfo.id,
      assimilation: fields.assimilation.value,
      communication: fields.communication.value,
      defense_and_repair: fields.defenseRepair.value,
      biotransformation_and_elimination:
        fields.biotransformationElimination.value,
      energy: fields.energy.value,
      transport: fields.transport.value,
      structural_integrity: fields.structuralIntegrity.value,
      status: 0
    };
    if (bol) {
      data.status = 1;
    }

    if (this.state.mode.update) {
      data.modified_date = new Date();
      analyis
        .update(data)
        .then(res => {
          if (res.message) {
            this._setMessage(res.message, false);
          } else if (res.length > 0) {
            if (bol) {
              this._setMessage("Successfully Finalized", true);
            } else {
              this._setMessage("Successfully Updated", true);
            }
          }
          this._getAnalysis();
        })
        .catch(err => {
          console.log(err);
        });
    } else {
      data.created_date = new Date();
      analyis
        .save(data)
        .then(res => {
          if (res.message) {
            this._setMessage(res.message, false);
          } else {
            this._setMode(true);
            buttons.draftButton.label = "Update";
            if (bol) {
              this._setMessage("Successfully Finalized", true);
            } else {
              this._setMessage("Successfully Saved", true);
            }
            this._getAnalysis();
          }
        })
        .catch(err => {
          console.log(err);
        });
    }
    buttons.draftButton.disabled = true;

    this.setState({
      buttons
    });
  }

  _setMessage(mess, good) {
    let formMessage = this.state.formMessage;
    if (good) {
      formMessage.success = mess;
    } else {
      formMessage.error = mess;
    }

    this.setState({
      formMessage
    });
  }

  _displayFields() {
    let fields = this.state.fields;

    return Object.keys(fields).map(item => {
      let fld = fields[item];

      return (
        <div key={item} className={flex_wrapper}>
          <div className={itemWrapper}>
            <div className={fieldLableStyle}>{fld.label}</div>
          </div>
          <div className={itemWrapper}>
            {this.user.type === "Patient" ? (
              <div className={analyisDivStyle}>
                {fld.value !== "" ? fld.value : "No data yet"}
              </div>
            ) : (
                <TextareaAutosize
                  disabled={this.state.disabled}
                  className={textAreaContent}
                  name={item}
                  onChange={this._handleChange.bind(this)}
                  value={fld.value || ""}
                />
              )}
          </div>
        </div>
      );
    });
  }

  _handleChange(e) {
    let message = this.state.formMessage;
    message.success = "";
    message.error = "";
    let fields = this.state.fields;
    fields[e.target.name].value = e.target.value;
    this.setState({
      fields,
      formMessage: message
    });

    this._canSumbit();
  }

  _canSumbit() {
    let fields = this.state.fields;
    let buttons = this.state.buttons;
    let nullCount = 0;

    Object.keys(fields).forEach((item, i) => {
      if (fields[item].value === "") {
        nullCount++;
      }
    });

    if (nullCount === Object.keys(fields).length) {
      buttons.draftButton.disabled = true;
      buttons.finalButton.disabled = true;
    } else {
      buttons.draftButton.disabled = false;
      buttons.finalButton.disabled = false;
    }

    this.setState({
      buttons
    });
  }
} // class end

let summaryTitle = css({
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 100%",
  justifyContent: "center",
  fontSize: 18,
  fontWeight: 500,
  color: "#36c4f1",
  textAlign: "center",
  textTransform: "uppercase"
});

let itemWrapper = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap"
  // padding: '5px 5px 3px 20px'
});

let textAreaContent = css({
  display: "block",
  boxSizing: "padding-box",
  overflow: "hidden",
  padding: "5px",
  width: " 100%",
  height: 50,
  fontSize: "14px",
  margin: "5px",
  borderRadius: "6px",
  boxShadow: "2px 2px 8px rgba(black, .3)"
});

const disabledbtn = css({
  textAlign: "center",
  padding: "5px",
  fontSize: "small",
  background: "gray",
  color: "#fff",
  width: "100px",
  border: "1px solid",
  marginRight: "10px"
  //cursor: 'pointer',
  //borderRadius: '15px 0 15px 0'
});
const btn = css({
  textAlign: "center",
  padding: "5px",
  fontSize: "small",
  background: "#80cde9",
  color: "#fff",
  width: "100px",
  border: "1px solid",
  cursor: "pointer",
  marginRight: "10px"
  //  borderRadius: '6px'
});

let fieldLableStyle = css({
  fontWeight: 600,
  color: Colors.blueDarkAccent,
  marginBottom: 10
});

let analyisDivStyle = css({
  fontStyle: "italic",
  color: "#777",
  fontWeight: 500,
  textAlign: "justify",
  whiteSpace: "pre-wrap",
  fontSize: 16,
  lineHeight: "18px",
  paddingRight: 20,
  marginBottom: 20
});
