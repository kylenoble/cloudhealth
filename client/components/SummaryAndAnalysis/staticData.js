export const static_AnalysisData =
    [
        [
            { msq_system: 'Digestive', msq_analysis: '' },
            { msq_system: 'Ears', msq_analysis: '' },
            { msq_system: 'Emotions', msq_analysis: '' },
            { msq_system: 'Energy', msq_analysis: '' },
            { msq_system: 'Eyes', msq_analysis: '' },
            { msq_system: 'Head', msq_analysis: '' },
            { msq_system: 'Heart', msq_analysis: '' },
            { msq_system: 'Joints and Muscles', msq_analysis: '' },
            { msq_system: 'Lungs', msq_analysis: '' },
            { msq_system: 'Mind', msq_analysis: '' },
            { msq_system: 'Mouth and Throat', msq_analysis: '' },
            { msq_system: 'Nose', msq_analysis: '' },
            { msq_system: 'Other', msq_analysis: '' },
            { msq_system: 'Skin', msq_analysis: '' },
            { msq_system: 'Weight', msq_analysis: '' }
        ],
        { recommendation: null }
    ]

export const static_MSQSystemRank = [
    { msq_system: 'Digestive', total_score: 0 },
    { msq_system: 'Ears', total_score: 0 },
    { msq_system: 'Emotions', total_score: 0 },
    { msq_system: 'Energy', total_score: 0 },
    { msq_system: 'Eyes', total_score: 0 },
    { msq_system: 'Head', total_score: 0 },
    { msq_system: 'Heart', total_score: 0 },
    { msq_system: 'Joints and Muscles', total_score: 0 },
    { msq_system: 'Lungs', total_score: 0 },
    { msq_system: 'Mind', total_score: 0 },
    { msq_system: 'Mouth and Throat', total_score: 0 },
    { msq_system: 'Nose', total_score: 0 },
    { msq_system: 'Other', total_score: 0 },
    { msq_system: 'Skin', total_score: 0 },
    { msq_system: 'Weight', total_score: 0 }
]