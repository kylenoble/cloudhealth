import GridContainer from '../../NewSkin/Wrappers/GridContainer';
import { labelClass } from '../../NewSkin/Styles';
import { css } from 'glamor';
import Colors from '../../NewSkin/Colors';

export default class GrandTotal extends React.Component {
  render() {
    const { grandTotal, msqStatus } = this.props;

    return (
      <section className={wrapper}>
        <GridContainer styles={{ width: 'fit-content' }} columnSize="1fr 1fr" gap={20} dataID="grand-total">
          <label className={labelStyle}>Grand Total:</label>
          <span className={total(grandTotal)}>{grandTotal}</span>
        </GridContainer>

        <GridContainer styles={{ width: 'fit-content' }} columnSize="1fr 2fr" gap={20} dataID="msq-status">
          <label className={labelStyle}>MSQ Status:</label>
          <span className={msqStatusStyle(msqStatus)}>
            {msqStatus}
          </span>
        </GridContainer>
      </section>
    )
  }
}

const wrapper = css({
  display: 'flex',
  width: 'fit-content',
  margin: '0 auto'
})

const msqStatusColor = (status) => {
  switch (status) {
    case 'Optimal':
      return '#00a838';
    case 'Mild Toxicity':
      return '#467dc7';
    case 'Moderate toxicity':
      return '#fffb00';
    case 'Severe Toxicity':
      return '#ff0000';
    default:
      return '#999';
  }
}

const msqStatusStyle = (status) => {
  return css({
    color: 'white',
    padding: '5px 15px',
    fontSize: '1rem',
    borderRadius: 50,
    background: msqStatusColor(status)
  })
}

const total = (gt) => css({
  fontWeight: 600,
  color: gt > 0 ? Colors.skyblue : '#999',
  fontSize: 22,
  textAlign: 'left !important'
});

const labelStyle = css(labelClass, {
  marginBottom: 0,
  display: 'grid',
  alignContent: 'center'
});


