import SystemTable from "./systemTable";
import MSQAnalysis from "./msqAnalysis";
import GrandTotal from "./grandTotal";
import axios from "axios";
import currentDomain from "../../../utils/domain.js";
const UPDATE_ANALYSIS_URL = `${currentDomain}/api/symptoms/update`;
const ADD_ANALYSIS_URL = `${currentDomain}/api/symptoms/add`;
const CREATE_ANALYSIS_URL = `${currentDomain}/api/symptoms/create`;
import { css } from "glamor";
import { static_AnalysisData } from "../staticData";
import GridContainer from "../../NewSkin/Wrappers/GridContainer";
import { labelClass } from "../../NewSkin/Styles";

export default class SymptomsReview extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      newMsqSystemRanks: "",
      firstColumn: "",
      secondColumn: "",
      tbHeight: "",
      grandTotal: 0,
      analysisData: "",
      newAnalysisData: "",
      staticAnalysisData: static_AnalysisData[0],
      msqStatus: "",
      savingState: "Save Changes",
      savingStatus: false,
      recommendation: "",
      editing: false,
      symptomsReviewStatus: false
    };
  }

  componentDidMount() {
    this.handleMsqSystemRanks();
  }

  componentDidUpdate(prevProps, prevState) {
    // console.log('@symptoms', this.state.staticAnalysisData);
  }

  handleMsqSystemRanks = () => {
    setTimeout(() => {
      this.setState({
        analysisData: this.props.analysisAndRec,
        recommendation: this.props.analysisAndRec[1],
        symptomsReviewStatus: this.props.symptomsReviewStatus
      });
    }, 100);

    let newMsqSystemRanks = [];

    this.props.msqSystemRanks.map(msqSystemRank =>
      newMsqSystemRanks.push({
        msq_system: msqSystemRank.msq_system,
        total_score: msqSystemRank.total_score
      })
    );

    setTimeout(() => {
      this.setState({ newMsqSystemRanks });
    }, 1);

    setTimeout(() => {
      this.state.newMsqSystemRanks.length > 0
        ? this.handleColumnDivision()
        : null;
    }, 100);
  };

  handleColumnDivision = () => {
    let firstColumn = [];
    let secondColumn = [];
    let analysisData = this.state.analysisData[0];

    for (let i = 0; i < this.state.newMsqSystemRanks.length; i++) {
      let msqSystemRanks = this.state.newMsqSystemRanks[i];

      if (i > 7) {
        analysisData
          ? (msqSystemRanks["analysis"] = analysisData[i].msq_analysis)
          : null;
        secondColumn.push(msqSystemRanks);
      } else if (i < 8) {
        analysisData
          ? (msqSystemRanks["analysis"] = analysisData[i].msq_analysis)
          : null;
        firstColumn.push(msqSystemRanks);
      }
    }

    setTimeout(() => {
      this.setState({ firstColumn, secondColumn });
      this.getGrandTotal();
    }, 1);
  };

  tbHeight = tbHeight => {
    this.setState({
      tbHeight
    });
  };

  getGrandTotal = () => {
    let grandTotal = 0;

    for (let i = 0; i < this.state.newMsqSystemRanks.length; i++) {
      let msqSystemRank = this.state.newMsqSystemRanks[i].total_score;
      grandTotal += parseInt(msqSystemRank);
    }

    grandTotal = parseInt(grandTotal);
    this.getMsqStatus(grandTotal);

    this.setState({
      grandTotal: grandTotal
    });
  };

  getMsqStatus = gt => {
    let gTotal = gt;

    if (gTotal >= 100) {
      this.setState({
        msqStatus: "Severe Toxicity"
      });
    } else if (gTotal >= 50 && gTotal <= 99) {
      this.setState({
        msqStatus: "Moderate toxicity"
      });
    } else if (gTotal >= 11 && gTotal <= 49) {
      this.setState({
        msqStatus: "Mild Toxicity"
      });
    } else if (gTotal >= 1 && gTotal <= 10) {
      this.setState({
        msqStatus: "Optimal"
      });
    } else if (gTotal == 0) {
      this.setState({
        msqStatus: "None"
      });
    }
  };

  saveMSQAnalysis = () => {
    this.state.symptomsReviewStatus
      ? this.addAnalysis()
      : this.updateAnalysis();
  };

  updateAnalysis = () => {
    let date = new Date();
    let newData = this.state.editing
      ? this.state.newAnalysisData[0]
      : this.state.analysisData[0];
    let newAnalysisData = [newData, this.state.recommendation];

    const data = {
      data: JSON.stringify(newAnalysisData),
      id: this.props.patientInfo.id,
      dateModified: date
    }
    axios
      .put(UPDATE_ANALYSIS_URL, data)
      .then(res => {
        this.handleSavingStatus();

        setTimeout(() => {
          this.setState({
            savingStatus: true
          });
          console.log(
            `%c Symptoms Review Updated. ✔`,
            "color: white; background: #1da362; padding: 5px 10px"
          );
          this.handleSavingStatus();
        }, 1000);
      })
      .catch(err => {
        console.log("msq analysis save error: ", err);
      });
  };

  addAnalysis = () => {
    this.setState((prevState, props) => ({
      symptomsReviewStatus: false
    }));

    let date = new Date();
    let newData = this.state.editing
      ? this.state.newAnalysisData[0]
      : this.state.analysisData[0];
    let newAnalysisData = [newData, this.state.recommendation];

    const data = {
      data: JSON.stringify(newAnalysisData),
      id: this.props.patientInfo.id,
      dateCreated: date
    }

    axios
      .post(CREATE_ANALYSIS_URL, data)
      .then(res => {
        this.handleSavingStatus();

        setTimeout(() => {
          this.setState({
            savingStatus: true
          });

          console.log(
            "%c Symptoms Review Created. ✔",
            "color: white; background: #1da362; padding: 5px 10px",
            res.body
          );
          this.handleSavingStatus();
        }, 1000);
      })
      .catch(err => {
        console.log("msq analysis create error: ", err);
      });
  };

  handleSavingStatus = () => {
    this.state.savingStatus
      ? this.setState({ savingState: "saved" })
      : this.setState({ savingState: "saving..." });
  };

  handleAnalysis = anData => {
    let newAData = "";

    this.state.analysisData[0]
      ? (newAData = this.state.analysisData[0])
      : (newAData = this.state.staticAnalysisData[0]);
    newAData[anData.index].msq_analysis = anData.data;

    let newAnalysisData = [
      newAData,
      { recommendation: this.state.recommendation }
    ];

    setTimeout(() => {
      this.setState({
        editing: true,
        newAnalysisData
      });
    }, 100);
  };

  handleAnalysisChange = () => {
    setTimeout(() => {
      this.setState({
        savingStatus: false,
        savingState: "Save Changes"
      });
    }, 100);
  };

  writeRecommendation = e => {
    this.setState((prevState, props) => ({
      symptomsReviewStatus: false
    }));

    let recommendation = e.target.value;
    setTimeout(() => {
      this.setState((prevState, props) => ({
        recommendation: { recommendation: recommendation },
        newAnalysisData: this.state.newAnalysisData,
        savingStatus: false,
        savingState: "Save Changes"
      }));
    }, 100);
  };

  render() {
    return (
      <GridContainer gap={20} fullWidth dataID="symptoms-review-wrapper">
        <GridContainer columnSize="1fr 1fr" gap={20}>
          {this.state.firstColumn.length > 0 ? (
            <>
              <SystemTable
                tbHeight={this.tbHeight}
                height={this.state.tbHeight}
                columnData={this.state.firstColumn}
                analysisData={this.props.analysisAndRec[0]}
                onAnalysis={this.handleAnalysis}
                staticAnalysisData={this.state.staticAnalysisData[0]}
                onHandleAnalysisChange={this.handleAnalysisChange}
                userType={this.props.userType}
              />
              <SystemTable
                height={this.state.tbHeight}
                columnData={this.state.secondColumn}
                analysisData={this.state.analysisData[0]}
                onAnalysis={this.handleAnalysis}
                staticAnalysisData={this.state.staticAnalysisData[0]}
                onHandleAnalysisChange={this.handleAnalysisChange}
                userType={this.props.userType}
              />
            </>
          ) :
            <label htmlFor="" className={labelClass}>Loading...</label>
          }
        </GridContainer>

        <GrandTotal
          grandTotal={this.state.grandTotal}
          msqStatus={this.state.msqStatus}
        />

        {this.props.userType === "Patient" ? (
          <section className={recommendationWrapper}>
            <label className={labelClass}>MSQ Analysis and Recommendation(s):</label>
            <p className={recommendationStyle(this.state.recommendation["recommendation"])}>
              {
                this.state.recommendation["recommendation"] == null ?
                  'No MSQ Analysis and Recommendation yet.' :
                  this.state.recommendation["recommendation"]
              }
            </p>
          </section>

        ) : (
            <MSQAnalysis
              writeRecommendations={this.writeRecommendation}
              height={this.state.tbHeight}
              onSaveAnalysis={this.saveMSQAnalysis}
              savingState={this.state.savingState}
              recommendation={this.state.recommendation["recommendation"]}
            />
          )}
      </GridContainer>
    );
  }
}

const recommendationStyle = (recom) => css({
  color: recom ? '#000' : '#ccc',
  textAlign: 'justify'
})

let recommendationWrapper = css({
  float: "left",
  width: "100%",
  padding: "20px",
  border: "1px solid #eee",
  borderRadius: "5px",
  marginTop: "20px",
  borderRadius: 10
});

let recommendationHeader = css({
  fontWeight: 600,
  color: "#36c4f1"
});

let recommendation = css({
  marginTop: 10,
  fontSize: "1rem"
});
