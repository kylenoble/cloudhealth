import GridContainer from '../../NewSkin/Wrappers/GridContainer';
import { labelClass, inputClass } from '../../NewSkin/Styles';
import { css } from 'glamor';
import Colors from '../../NewSkin/Colors';

export default class SystemTable extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      height: this.props.height,
      timer: null,
      analysis: this.props.analysisData,
      staticAnalysisData: this.props.staticAnalysisData,
      newAnalysis: ''
    }
  }

  getTableHeight() {
    let table = document.getElementById('table')
    this.props.tbHeight(table.offsetHeight)
  }

  handleAnalysis = e => {
    setTimeout(() => {
      this.props.onHandleAnalysisChange()
    }, 1)

    let elID = e.target.id
    let elVal = e.target.value
    let analysisData
    this.state.analysis ? (analysisData = this.state.analysis) : (analysisData = this.state.staticAnalysisData)

    let ansData = analysisData.findIndex(analysis => analysis.msq_system === elID)
    let newAnalysisData = {
      index: ansData,
      data: elVal
    }

    clearTimeout(this.state.timer)

    let timer = setTimeout(() => {
      this.props.onAnalysis(newAnalysisData)
    }, 500)

    this.setState({
      timer
    })
  }

  render() {
    return (
      <section>
        <GridContainer columnSize="1.2fr 1fr 1.3fr" gap={10} classes={[header]} dataID="table-header">
          <span className={labelClass} style={{ textAlign: 'center !important', fontSize: 15 }}>SYSTEMS</span>
          <span className={labelClass} style={{ textAlign: 'center !important', fontSize: 15 }}>TOTAL SCORE</span>
          <span className={note} style={{ textAlign: 'center !important', fontSize: 15 }}>NOTES</span>
        </GridContainer>

        <section className={tableWrapper}>
          {
            this.props.columnData.map((fc, i) => {
              return (
                <GridContainer key={i} columnSize="1.2fr 1fr 1.3fr" gap={10} styles={{ padding: '5px 10px' }} dataID="table-content">
                  <span className={msqStyle} style={{ textAlign: 'left' }}>{fc.msq_system}</span>
                  <span className={msqStyle} style={{ color: fc.total_score > 0 ? Colors.blueDarkAccent : '#9995' }}>{fc.total_score}</span>
                  <span className={msqStyle} style={{ textAlign: 'justify' }}>
                    {this.props.userType === 'Doctor' ?
                      <input
                        onChange={this.handleAnalysis}
                        type="text"
                        id={fc.msq_system}
                        className={inputClass}
                        defaultValue={fc.analysis}
                        style={{ borderColor: '#777' }}
                      /> :

                      fc.analysis ? <small>{fc.analysis}</small> :
                        <small style={{ color: '#eee', textAlign: 'center', display: 'block' }}>No review yet.</small>
                    }
                  </span>
                </GridContainer>
              )
            })
          }
        </section>
      </section>
    )
  }
}

const msqStyle = css({
  display: 'grid',
  alignContent: 'center'
})

const note = css(labelClass, {
  textAlign: 'center !important'
})

const header = css({
  borderBottom: `1px solid ${Colors.pink}`,
  padding: '0px 10px',
  fontSize: 15
});

const tableWrapper = css({
  '& > section:nth-child(odd)': {
    background: '#eee4'
  }
})