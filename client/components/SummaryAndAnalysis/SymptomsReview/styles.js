import { css } from 'glamor'
let borderColor = 'rgba(0,0,0,.3)'
let blue = '#36c4f1'

export let tableWrapper = css({
  // display: 'table',
  // clear: 'both',
  // width: '100%',
  // padding: '50px',
  // background: 'white'
})

export let caps = css({
  display: 'inline-block',
  textTransform: 'uppercase'
})

export let centered = css({
  textAlign: 'center'
})

export let table = css({
  float: 'left',
  width: '100%',
  maxWidth: '43%',
  borderLeft: '1.5px solid ' + borderColor,
  position: 'relative'
})

export let fromLeftSeparator = css({
  position: 'absolute',
  top: '0',
  left: '0',
  // transform: 'translateX(-50%)',
  width: '1px',
  height: '100%',
  background: borderColor
})

export let fromRightSeparator = css({
  position: 'absolute',
  top: '0',
  right: '0',
  // transform: 'translateX(-50%)',
  width: '1px',
  height: '100%',
  background: borderColor
})

export let pad = css({
  padding: '5px 20px'
})

export let margin = css({
  margin: '5px 20px'
})

export let half = css({
  // width: '33.333333333%',
  padddingBottom: '10px !important',
  position: 'relative'
})

export let fLeft = css({
  float: 'left'
})

export let fRight = css({
  float: 'right'
})

export let block = css({
  display: 'block'
})

export let colBlue = css({
  color: blue
})

export let bgBlue = css({
  background: blue,
  color: 'white'
})

export let dbgBlue = css({
  background: '#364563 !important',
  color: 'white',
  ':hover': {
    color: blue
  }
})

export let btn = css({
  border: 'none',
  background: 'none',
  cursor: 'pointer',
  padding: '10px 20px',
  boxSizing: 'border-box',
  margin: '5px 0px',
  transition: '300ms ease'
})

export let fullWidth = css({
  width: '100%'
})

export let toBottom = css({
  position: 'absolute',
  bottom: '0',
  left: '0',
  paddingLeft: '20px'
})

export let grandTotalWrapper = css({
  float: 'left',
  marginTop: '30px',
  width: '100%',
  textAlign: 'center'
  // marginLeft: '47%'
})

export let bold = css({
  fontWeight: '600'
})

export let formInput = css({
  padding: '5px 10px',
  border: 'none',
  borderBottom: '1px solid #ccc',
  transition: '250ms ease',
  width: '100%',
  boxSizing: 'border-box',
  '::placeholder': {
    color: '#ccc',
    fontStyle: 'italic'
  },
  ':hover': {
    borderColor: '#333'
  },
  ':focus': {
    borderColor: blue
  }
})

export let msqAnalysisWrapper = css({
  width: '100%',
  float: 'left',
  position: 'relative',
  marginTop: 20
  // padding: 20
  // borderLeft: '1.5px solid ' + borderColor
})

export let alignRight = css({
  textAlign: 'right'
})

export let alignLeft = css({
  textAlign: 'left'
})

export let formInputWrapper = css({
  position: 'relative'
})


export let textarea = css({
  width: '100%',
  resize: 'none',
  padding: '20px',
  boxSizing: 'border-box',
  border: '1px solid #eee',
  boxShadow: '0 0px 0px rgba(0,0,0,0)',
  transition: '300ms ease',
  ':focus': {
    boxShadow: '0 8px 20px rgba(0,0,0,.1)'
  },
  marginBottom: 10
})

export let recommendationWrapper = css({
  // padding: '20px',
  // paddingRight: '0px',
  // paddingTop: '0px'
})

export let recomTitle = css({
  // padding: '10px 0px',
  fontWeight: 600,
  margin: '10px 0'
})