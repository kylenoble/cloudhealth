import { msqAnalysisWrapper, recommendationWrapper } from "./styles";
import { inputClass, labelClass, goodMessage, errorMsg, formMsgActive, flex_wrapper } from "../../NewSkin/Styles";
import Button from "../../NewSkin/Components/Button";
import GridContainer from "../../NewSkin/Wrappers/GridContainer";

export default class MsqAnalysis extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      saving: "Save Changes",
      recommendation: ""
    };
  }

  componentDidMount() {
    this.setState({
      recommendation: this.props.recommendation
    });
  }

  componentWillReceiveProps() {
    this.setState({
      recommendation: this.props.recommendation
    });
  }

  render() {
    return (
      <section className={msqAnalysisWrapper}>
        <article className={recommendationWrapper}>
          <label className={labelClass}>Write MSQ Analysis and Recommendation(s)</label>

          <textarea
            value={this.props.recommendation}
            onChange={this.props.writeRecommendations}
            className={inputClass}
            cols="30"
            rows="5"
            placeholder="Write MSQ Analysis and Recommendation(s)"
          />
        </article>

        <GridContainer center columnSize={'1fr'} dataID="msq-buttons">
          {this.props.savingState === "saving..." ? (
            <Button type="disabled" styles={{ margin: '0 auto' }}>
              <button
                disabled
                onClick={this.props.onSaveAnalysis}
              >
                Updating...
              </button>
            </Button>
          ) : this.props.savingState === "saved" ? (
            <React.Fragment>
              <Button styles={{ margin: '0 auto' }} type="disabled" >
                <button
                  disabled
                  onClick={this.props.onSaveAnalysis}
                >
                  {this.state.saving}
                </button>
              </Button>
              <div
                className={[
                  flex_wrapper,
                  goodMessage,
                  formMsgActive
                ].join(" ")}
                style={{ width: '40%' }}
              >
                Successfully Updated
                </div>
            </React.Fragment>
          ) : (
                <Button type="blue" styles={{ margin: '0 auto' }}>
                  <button onClick={this.props.onSaveAnalysis}>
                    {this.props.savingState}
                  </button>
                </Button>
              )}
        </GridContainer>
      </section>
    );
  }
}

