import { css } from "glamor";
import {
  Radar,
  RadarChart,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis,
  Tooltip
} from "recharts";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chart_data: []
    };
    this._getCoreImbalancesData = this._getCoreImbalancesData.bind(this);
    this._showTooltip = this._showTooltip.bind(this);
    this._getDescriptionOfGroup = this._getDescriptionOfGroup.bind(this);
    const fontSize = this.props.scale.fontSize;
  }

  componentWillMount() {
    this._getCoreImbalancesData();
  }

  componentWillReceiveProps() {
    this._getCoreImbalancesData();
  }

  _getCoreImbalancesData() {
    this.props.coreImbalances(true).then(res => {
      let obj = {};
      res.map(value => {
        obj = [
          {
            group: "Biotransformation And Elimination",
            value: parseInt(value.biotransformation_and_elimination)
          },
          { group: "Communication", value: parseInt(value.communication) },
          { group: "Assimilation", value: parseInt(value.assimilation) },
          {
            group: "Defense And Repair",
            value: parseInt(value.defense_and_repair)
          },
          { group: "Energy", value: parseInt(value.energy) },
          {
            group: "Structural Integrity",
            value: parseInt(value.structural_integrity)
          },
          { group: "Transport", value: parseInt(value.transport) }
        ];
      });
      this.setState({
        chart_data: obj
      });
    });
  }

  render() {
    return this._renderChart()
  }

  _getDescriptionOfGroup(label) {
    switch (label) {
      case "Assimilation":
        return "Digestion, Absorption, Microbiota/GI, Respiration";
      case "Defense And Repair":
        return "Immune system, inflammatory processes, infection and bacteria";
      case "Energy":
        return "Energy regulation, mitochondrial function";
      case "Biotransformation And Elimination":
        return "Toxicity, Detoxification";
      case "Communication":
        return "Endocrine, Neurotransmitters, Immune messengers, Cognition";
      case "Transport":
        return "Cardiovascular, Lymphatic systems";
      case "Structural Integrity":
        return "From the subcellular membranes to the musculoskeletal system";
    }
  }

  _showTooltip = data => {
    if (data.active) {
      let label = data.label;
      let value = data.payload[0].value;
      return (
        <div style={{ maxWidth: 400, borderTopLeftRadius: 3, borderTopRightRadius: 3, overflow: 'hidden', boxShadow: '0 2px 8px rgba(0, 0, 0, 0.2)' }}>
          <small style={{ padding: 5, background: '#364563', display: 'block', textAlign: 'center', color: 'white' }}>{`${label} : ${value}`}</small>
          <p style={{ padding: 15, backgroundColor: '#FFF', fontSize: '.8rem' }}> {this._getDescriptionOfGroup(label)}</p>
        </div>
      );
    }
  };

  test() {
    this.props.updateView("defenseRepair");
  }

  _renderChart() {
    const styles = {
      nameStyle: {
        fontSize: `${this.props.scale.fontSize} !important`,
        stroke: "transparent",
        fill: "black"
      },
      nameStyleA: {
        fontSize: 15,
        stroke: "transparent"
      }
    };
    const chart_data = this.state.chart_data;
    return (
      <div className={chartDiv}>
        <RadarChart
          outerRadius={this.props.scale.radius - 30}
          width={this.props.scale.width - 30}
          height={this.props.scale.height - 30}
          data={chart_data}
        >
          <PolarGrid />
          <PolarAngleAxis
            style={styles.nameStyleA}
            width={80}
            dataKey={"group"}
          />
          <PolarRadiusAxis
            style={styles.nameStyle}
            angle={90}
            domain={[5, 60]}
            type="number"
          />
          <Tooltip content={this._showTooltip} />
          <Radar
            name="Core Imbalances"
            fontSize={this.props.scale.fontSize}
            dataKey="value"
            stroke="#36c4f1"
            fill="#d383b6"
            fillOpacity={0.6}
          />
        </RadarChart>
      </div>
    );
  }
}

let summaryTitle = css({
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 100%",
  justifyContent: "center",
  color: "#36c4f1",
  margin: "15px 0 0 0"
});

let summaryDivStyle = css({
  position: "relative",
  left: "-260px",
  textAlign: "center"
});

let chartDiv = css({
  display: "flex",
  flex: 1,
  justifyContent: "center"
});
