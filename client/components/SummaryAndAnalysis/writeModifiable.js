import { css } from 'glamor'
import TextareaAutosize from 'react-autosize-textarea';
import AnalysisService from '../../utils/analysisService.js'
import GridContainer from '../NewSkin/Wrappers/GridContainer'
import Colors from '../NewSkin/Colors.js';
import { scrollClass, font, link, combined, centered, statusButton, mainFont, goodMessage, errorMsg, formMsgActive, flex_wrapper } from '../NewSkin/Styles.js';
import { DOH_Interpretation } from '../Commons';
import Link from 'next/link'
import Button from '../NewSkin/Components/Button.js';

const analyis = new AnalysisService()

export default class extends React.Component {

  constructor(props) {
    super(props)

    this.user = this.props.user
    this.patientInfo = this.props.patientInfo
    this.state = {
      scores: [],
      disabled: this.props.user.type === 'Patient' ? true : false,
      fields: {
        detox: {
          label: 'Detox',
          name: 'detox',
          value: '',
          disabled: false,
        },
        sleep: {
          label: 'Sleep',
          name: 'sleep',
          value: '',
          disabled: false,
        },
        stress: {
          label: 'Stress',
          name: 'stress',
          value: '',
          disabled: false,
        },
        exercise: {
          label: 'Exercise',
          name: 'exercise',
          value: '',
          disabled: false,
        },
        nutrition: {
          label: 'Nutrition',
          name: 'nutrition',
          value: '',
          disabled: false,
        },
        weightScore: {
          label: 'Weight',
          name: 'weightScore',
          value: '',
          disabled: false,
        }
      },
      buttons: {
        disabled: false,
        finalButton: {
          label: 'Finalize',
          disabled: false
        },
        draftButton: {
          label: 'Save',
          disabled: true
        }
      },
      formMessage: {
        success: '',
        error: ''
      },
      mode: {
        update: false
      }
    }

    this._getHealthScores = this._getHealthScores.bind(this)
  }

  componentWillMount() {
    this._getHealthScores(this.patientInfo.id)
    this._getAnalysis()
  }

  componentDidMount() {

  }

  _getAnalysis() {
    const buttons = this.state.buttons
    analyis.get(this.patientInfo.id)
      .then((res) => {
        if (res) {
          this._setMode(true)
          buttons.draftButton.label = 'Update'
          this._setInitialFieldValue(res)
        } else {
          this._setMode(false)
          buttons.draftButton.label = 'Save'
        }

      })
      .catch((err) => {
        console.log(err);
      })

    this.setState({
      buttons
    })
  }

  _setInitialFieldValue(dta) {
    const data = dta
    let fields = this.state.fields
    const buttons = this.state.buttons


    if (data.modifiables) {
      fields = data.modifiables
      buttons.finalButton.disabled = false
    }

    if (data.status == 1) {
      this._lockFields(true)
      buttons.finalButton.disabled = true
    }
    this.setState({
      buttons,
      fields
    })


  }

  _setMode(bol) {
    const mode = this.state.mode
    mode.update = bol
    this.setState({
      mode
    })
  }

  _getHealthScores(userid) {
    const obj = {}
    this.props.scores(true, userid)
      .then(res => {
        res.map(summary => {
          obj[summary.name] = summary.value
        });
        this.setState({ scores: obj })
      })
      .catch((err) => {
        console.log(err);
      })
  }

  _displayScore(value) {
    const score = (!NaN ? parseFloat(value).toFixed(1) : 0)
    return score * 2

  }



  _getHealthStatus(value) {
    const score = (!NaN ? parseFloat(value).toFixed(1) : 0) * 2

    if (score >= 1.0 && score <= 2.9) {
      return 'Optimal'
    } else if (score >= 3.0 && score <= 4.9) {
      return 'Suboptimal'
    } else if (score >= 5.0 && score <= 5.9) {
      return 'Neutral'
    } else if (score >= 6.0 && score <= 8.5) {
      return 'Compromised'
    } else if (score >= 8.6) {
      return 'Alarming'
    }
  }

  _renderIcon(str, icon = false) {
    switch (str) {
      case 'weightScore':
        return icon ? '/static/icons/cha_icons.svg#Weight' : Colors.weightColor;
      case 'nutrition':
        return icon ? '/static/icons/cha_icons.svg#Nutrition' : Colors.nutritionColor;
      case 'sleep':
        return icon ? '/static/icons/cha_icons.svg#Sleep' : Colors.sleepColor;
      case 'detox':
        return icon ? '/static/icons/cha_icons.svg#Detox' : Colors.detoxColor;
      case 'stress':
        return icon ? '/static/icons/cha_icons.svg#Stress' : Colors.stressColor;
      case 'exercise':
        return icon ? '/static/icons/cha_icons.svg#Exercise' : Colors.movementColor;
    }

  }


  toggleWAForm(form) {
    this.props.toggleWAForm(form)
  }

  _hadleInputChange(e) {
    const fields = this.state.fields
    fields[e.target.name].value = e.target.value
    this.setState({
      fields
    })

    this._canSumbit()
  }

  _canSumbit() {
    const fields = this.state.fields
    const buttons = this.state.buttons
    let nullCount = 0

    Object.keys(fields).forEach((item, i) => {
      if (fields[item].value === '') {
        nullCount++
      }
    })

    if (nullCount === Object.keys(fields).length) {
      buttons.draftButton.disabled = true
      buttons.finalButton.disabled = true
    } else {
      buttons.draftButton.disabled = false
      buttons.finalButton.disabled = false
    }

    this.setState({
      buttons
    })
  }

  _formSubmit(bol, final) {

    const fields = this.state.fields
    const mode = this.state.mode
    const buttons = this.state.buttons

    // let data = {
    //   user_id : this.props.patientInfo.id,
    //   assimilation: fields.assimilation.value,
    //   communication: fields.communication.value,
    //   defense_and_repair: fields.defenseRepair.value,
    //   biotransformation_and_elimination: fields.biotransformationElimination.value,
    //   energy: fields.energy.value,
    //   transport: fields.transport.value,
    //   structural_integrity: fields.structuralIntegrity.value,
    //   status: 0
    // }


    const data = {
      user_id: this.patientInfo.id,
      modifiables: JSON.stringify(fields),
      modifiables_status: final ? 1 : 0
    }

    if (bol) {
      data.status = 1
    }

    if (this.state.mode.update) {

      data.modifiables_date_modified = new Date()
      analyis.update(data)
        .then((res) => {
          if (res.message) {
            this._setMessage(res.message, false)
          } else if (res.length > 0) {
            if (bol) {
              this._setMessage("Successfully Finalized", true)
            } else {
              this._setMessage("Successfully Updated", true)
            }

          }
          this._getAnalysis()
        })
        .catch(err => {
          console.log(err);
        })
    } else {
      data.modifiables_date_created = new Date()
      analyis.save(data)
        .then(res => {
          if (res.message) {
            this._setMessage(res.message, false)
          } else {
            this._setMode(true)
            buttons.draftButton.label = 'Update'
            if (bol) {
              this._setMessage("Successfully Finalized", true)
            } else {
              this._setMessage("Successfully Saved", true)
            }
            this._getAnalysis()
          }
        })
        .catch(err => {
          console.log(err)
        })
    }
    buttons.draftButton.disabled = true

    this.setState({
      buttons
    })
  }

  _setMessage(mess, good) {
    const formMessage = this.state.formMessage
    if (good) {
      formMessage.success = mess
    } else {
      formMessage.error = mess
    }

    this.setState({
      formMessage
    })
  }

  _displayFields() {
    const fields = this.state.fields

    const modifiables = Object.keys(fields).map((item, i) => {

      const healthStatus = this._getHealthStatus(this.state.scores[item])

      let textAreaDefaulValue = "";

      if (healthStatus && item) {

        let doh = "";
        if (item.toLowerCase() === 'weightscore') {
          doh = 'weight';
        } else {
          doh = item.toLowerCase()
        }
        textAreaDefaulValue = DOH_Interpretation[`${doh}`][`${healthStatus.toLowerCase()}`];
      }

      const selectDoctor = label => {
        switch (label) {
          case 'Nutrition':
            return 'Nutritionist';
          case 'Exercise':
            return 'Kinesiologist'

          default:
            return 'Doctor'
        }
      }

      const label = this.state.fields[item].label;

      return (
        <GridContainer
          dataID={`${item}-doh-modifiables`}
          key={item}
          onHoverStyles={{ boxShadow: '0 8px 15px rgba(0,0,0,.1)', borderRadius: 5 }}
          styles={{
            width: '100%',
            borderLeft: label === 'Weight' ? '' : '1px solid #ccc3',
            padding: 10,
            alignContent: 'start'
          }}>
          <div>
            <section>
              <svg style={{ width: 35, height: 35, fill: this._renderIcon(item), verticalAlign: 'middle' }}>
                <use xlinkHref={this._renderIcon(item, true)} />
              </svg>
              <div className={titleStyle}>
                {label}
              </div>
            </section>

            <div className={scoreStyle}>Score: {this._displayScore(this.state.scores[item])}</div>

            <div className={statusButton}
              style={{
                background: healthStatus ? Colors[`${healthStatus.toLowerCase()}`] : ''
              }}>{healthStatus}
            </div>
          </div>

          <div className={textDiv}>
            {this.user.type === 'Patient' ?
              <div>
                <small style={{ margin: 0, textTransform: 'uppercase', fontWeight: 500 }}>Interpretation</small>

                <div className={combined([interpretationWrapper, font(.8), centered])}>
                  {this.state.fields[item].value !== "" ? this.state.fields[item].value : textAreaDefaulValue}
                </div>
              </div>

              : <textarea disabled={this.state.disabled} className={insightsTextArea} name={this.state.fields[item].name} onChange={this._hadleInputChange.bind(this)} value={this.state.fields[item].value ? this.state.fields[item].value : textAreaDefaulValue} placeholder="RECOMMENDATIONS"></textarea>
            }
          </div>

          {this.user.type === 'Patient' &&
            <Link href="/activities?tab=appointment">
              <p className={combined([link(Colors.skyblue), font(.8)])}>Book an Appointment with a {selectDoctor(label)}</p>
            </Link>
          }
        </GridContainer>
      )
    })
    return modifiables
  }

  render() {
    return (
      <div className={left_column}>

        <div style={{ width: '100%' }} className={scrollClass}>
          <GridContainer columnSize={'repeat(6, 1fr)'} styles={{ width: '100%' }} >
            {this._displayFields()}
          </GridContainer>
        </div>

        {
          this.user.type === 'Doctor' &&
          <GridContainer styles={{ width: '50%' }} center columnSize={'1fr'} dataID="modifiable-buttons">
            <Button type={this.state.buttons.draftButton.disabled ? "disabled" : "blue"} center>
              <button disabled={this.state.buttons.draftButton.disabled} onClick={() => { this._formSubmit(false) }}>{this.state.buttons.draftButton.label}</button>
            </Button>
            <div
              className={[
                flex_wrapper,
                this.state.formMessage.error ? errorMsg : goodMessage,
                this.state.formMessage.error || this.state.formMessage.success ? formMsgActive : undefined
              ].join(" ")}
              style={{ width: '60%' }}
            >
              {this.state.formMessage.success}
              {this.state.formMessage.error}
            </div>
          </GridContainer>
        }

        {/** <button disabled={this.state.buttons.finalButton.disabled} className={this.state.buttons.finalButton.disabled? disabledbtn: btn} onClick={()=>{this._formSubmit(true)}}>{this.state.buttons.finalButton.label}</button>**/}
      </div>
    )
  }

} // class end

const interpretationWrapper = css({
  width: 165,
  overflow: 'hidden',
})

let insightsTextArea = css(mainFont, {
  display: 'block',
  boxSizing: 'padding-box',
  overflow: 'hidden',
  padding: '5px',
  width: ' 100%',
  height: 150,
  fontSize: '14px',
  margin: '5px',
  borderRadius: '6px',
  boxShadow: '2px 2px 8px rgba(black, .3)'
})

let left_column = css({
  display: 'flex',
  flex: '4',
  flexWrap: 'wrap'
})

let titleStyle = css({
  textAlign: 'left',
  fontWeight: 600,
  color: Colors.blueDarkAccent,
  display: 'inline-block',
  verticalAlign: 'middle'
})

let scoreStyle = css({
  display: 'flex',
  flex: '1 0 100%',
  justifyContent: 'center',
  color: '#999',
  fontSize: 'small',
  fontStyle: 'italic',
  fontWeight: 500
})

let textDiv = css({
  flex: 1,
  margin: '20px 0'
})
