import { css } from "glamor";
import TextareaAutosize from "react-autosize-textarea";
import AnalysisService from "../../utils/analysisService";
import AnswerService from "../../utils/answerService";
import SymptomsReview from "./SymptomsReview/symptomsReview";
import Button from '../NewSkin/Components/Button';
import { Preparing } from "../NewSkin/Components/Loading";

const analyis = new AnalysisService();
const answer = new AnswerService();
import { static_AnalysisData, static_MSQSystemRank } from "./staticData";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      msqSystemRanks: "",
      patientInfo: this.props.patientInfo,
      analysisAndRecommendation: "",
      noSymptomsReview: false,
      userType: "",
      readyToView: false,
      staticAnalysisData: static_AnalysisData,
      staticMSQSystemRank: static_MSQSystemRank
    };
  }

  componentDidMount() {
    this.checkCurrentUser();
  }

  checkCurrentUser = () => {
    this.props.user
      ? (this._getMSQSystemRank(this.props.user.id),
        this.setState({ userType: "Patient" }))
      : (this._getMSQSystemRank(this.state.patientInfo.id),
        this.setState({ userType: "Doctor" }));
  };

  _getSymptomsAnalysis = userID => {
    let qry = `SELECT * FROM public.analysis_and_recomendation WHERE user_id = ${userID}`;
    answer
      .getQuery(qry)
      .then(res => {
        console.log(
          "%c Static Data Loaded.",
          "color: white; background: orange; padding: 5px 10px"
        );
        this.setState({
          analysisAndRecommendation: this.state.staticAnalysisData
        });

        if (res[0].symptoms_review != null) {
          this.setState({
            noSymptomsReview: false,
            analysisAndRecommendation: res[0].symptoms_review
          });
          console.log(
            "%c Dynamic Data Loaded.",
            "color: white; background: skyblue; padding: 5px 10px"
          );
        }

        this.checkAllValues();
      })
      .catch(err => {
        this.checkAllValues();
        console.log(
          "%c NO DATA | analysis_and_recommendations",
          "color: red; paddin: 5px 10px",
          err
        );
        this.setState({
          noSymptomsReview: true
        });
      });
  };

  _getMSQSystemRank = userID => {
    let qry = `SELECT * FROM public.msqsystem_ranks WHERE user_id = ${userID}`;

    answer
      .getQuery(qry)
      .then(res => {
        if (res.length >= 1) {
          this._getSymptomsAnalysis(userID);
          this.setState({
            msqSystemRanks: res,
            noSymptomsReview: false
          });
        } else {
          this.setState({
            msqSystemRanks: this.state.staticMSQSystemRank,
            analysisAndRecommendation: this.state.staticAnalysisData,
            noSymptomsReview: true
          });

          this.checkAllValues();
        }
      })
      .catch(err => {
        this.setState({
          msqSystemRanks: this.state.staticMSQSystemRank,
          analysisAndRecommendation: this.state.staticAnalysisData
        });
        // console.log('%c NO MSQ RANKS | msqsystem_ranks', 'color: red; paddin: 5px 10px', err)
      });
  };

  checkAllValues = () => {
    if (this.state.msqSystemRanks) {
      setTimeout(() => {
        this.setState((prevState, props) => ({
          readyToView: !prevState.readyToView
        }));
      }, 1300);
    }
  };

  render() {
    return (
      <div className={wrapper} style={{ padding: "20px" }}>
        {this.state.readyToView ? (
          <SymptomsReview
            patientInfo={this.state.patientInfo}
            msqSystemRanks={this.state.msqSystemRanks}
            analysisAndRec={this.state.analysisAndRecommendation}
            symptomsReviewStatus={this.state.noSymptomsReview}
            userType={this.state.userType}
          />
        ) : (
          <Preparing styles={{margin: 'auto'}}/>
          )}
      </div>
    );
  }

  toggleWAForm(form) {
    this.props.toggleWAForm(form);
  }
} // class end

let wrapper = css({
  position: "relative",
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap"
});