import {css} from 'glamor'
import TextareaAutosize from 'react-autosize-textarea';
import AnalysisService from '../../utils/analysisService.js'

const analyis = new AnalysisService()

export default class extends React.Component{

  constructor(props){
    super(props)
    this.state = {
    }
  }

  componentDidMount(){

  }

render(){
  return(
      <div className={wrapper}>
      <h2 className={summaryTitle}>RECOMMENDATIONS</h2>
      <span className={closeSpan} onClick={()=>this.toggleWAForm('writeRecomendation')}>X</span>
      <div className={note}>COMING SOON!</div>
      </div>
    )
  }



  toggleWAForm(form){
    this.props.toggleWAForm(form)
  }

} // class end

let summaryTitle = css({
  display: 'flex',
  flexWrap: 'wrap',
  flex: '1 0 100%',
  justifyContent: 'center',

  color: '#36c4f1',
  margin: '25px 5px 25px 5px'
})

let centered = css({
  display: 'flex',
  flex: '1 0 100%',
  flexWrap: 'wrap',
  justifyContent: 'center'
})

let wrapper = css({
  position: 'relative',
  display: 'flex',
  flex: '1 0 100%',
  flexWrap: 'wrap'
})


let closeSpan = css({
    position: 'absolute',
    top: 0,
    right: 0,
    background: '#36c4f1',
    borderRadius: 20,
    padding: '3px 8px 3px 8px',
    cursor: 'pointer'
})

let itemWrapper = css ({
  display: 'flex',
  flex: '1 0 100%',
  flexWrap: 'wrap',
  padding: '5px 5px 3px 20px'
})

let insightsTextArea = css({
  display: 'block',
  boxSizing: 'padding-box',
  overflow: 'hidden',
  padding: '5px',
  width:' 100%',
  height: 50,
  fontSize: '14px',
  margin: '5px',
  borderRadius: '6px',
  boxShadow: '2px 2px 8px rgba(black, .3)'
})

const disabledbtn = css({
  textAlign: 'center',
  padding: '5px',
  fontSize: 'small',
  background: 'gray',
  color: '#fff',
  width: '100px',
  border: '1px solid',
  //cursor: 'pointer',
  borderRadius: '6px'
})
const btn = css({
  textAlign: 'center',
  padding: '5px',
  fontSize: 'small',
  background: '#80cde9',
  color: '#fff',
  width: '100px',
  border: '1px solid',
  cursor: 'pointer',
  borderRadius: '6px'
})

let fieldLableStyle = css({
  fontWeight: 'bold'
})

let fieldValueStyle = css({
  fontStyle: 'italic'
})

let goodMessage = css({
  color: '#80cde9',
  padding: '5px',
})

let errorMessage = css({
  color: 'red',
  padding: '5px',
})

let note = css({
  display: 'flex',
  flex: '1 0 100%',
  flexWrap: 'wrap',
  justifyContent: 'center',
  textAlign: 'center',
  textTransform: 'uppercase',
  color: 'Orange'
})
