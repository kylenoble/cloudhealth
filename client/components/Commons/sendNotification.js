import moment from "moment-timezone";
import BookAppNotifService from "../../utils/book_Appointment_Notification_Service";

const bookAppNotifService = new BookAppNotifService();

const sendNotification = data => {
  _sendEmailDetail(data);
};

const _sendEmailDetail = data => {
  const emailBodyForPatient = `
            <p style="font-size: 16px; margin: 0">Your Health Survey forms have been unlocked.</p>

            <p style="font-size: 16px; color: #9e9e9e; margin-bottom: 0px; margin-top: 0px">
                ${data.notifMessage}
            </p>`;

  //   const emailBodyForDoctor = `
  //             <p style="font-size: 16px; margin: 0">You have Unlock Patient health Profile:</p>
  //             <p style="font-size: 16px; margin: 0px; color: #9e9e9e">Appointment date and time: <span style="color: #000; font-weight:600">

  //             <span></p>

  //             <p style="font-size: 16px; color: #9e9e9e;margin-bottom: 0px; margin-top: 0px">Patient Name:
  //                 <span style="color: #000; font-weight: 600"></span>
  //             </p>`;

  let emailDetails = "";
  const titles = {
    patient: "Unlocked Health Survey.",
    doctor: "Unlocked Health Survey."
  };

  const emailDetailsForPatient = {
    recipient: data.patientEmail,
    date: moment(new Date()).format("lll"),
    title: titles.patient,
    body: emailBodyForPatient
  };

  //   const emailDetailsForDoctor = {
  //     recipient: data.doctorEmail,
  //     date: moment(new Date()).format("lll"),
  //     title: titles.doctor,
  //     body: emailBodyForDoctor
  //   };

  emailDetails = emailDetailsForPatient;
  _emailSetup(emailDetails);

  //   setTimeout(() => {
  //     emailDetails = emailDetailsForDoctor;
  //     _emailSetup(emailDetails);
  //   }, 2000);
};

const _emailSetup = email => {
  const subject = "New Notification from CloudHealthAsia";
  const text = "Health Survey Unlocked";
  const to = email.recipient; // recipient
  const date = email.date; // notification date
  const notificationTitle = email.title; // notification title ex. Cancelled Appointment
  const notificationBody = email.body; // notification body ex. <span style="">Message here</span>

  const content = `
            <p style="font-size: 16px;">Please see below notification for your account:</p>

            <div style="border: 1px solid #ccc; padding: 20px; border-radius: 5px; background: #eee1; box-shadow: 0 8px 32px rgba(0,0,0,.050)">
                <p style="font-size: 16px; color: #9e9e9e; margin: 0">${date}</p>

                <h5 style="font-size: 1.64rem; font-weight: 400; margin: 0.82rem 0 0.656rem 0">${notificationTitle}</h5>

                ${notificationBody}
            </div>

            <p style="font-size: 16px">Please log-in to your account for further details.</p>
            <a title="Login to CloudHealthAsia" href="https://cloudhealthasia.com/login" target="_blank" style="text-decoration: none; padding: 10px 20px; background: #81cce6; color: #fff; border-radius: 5px; margin-top: 20px; display: block; width: 200px; font-weight: 500;">Login to CloudHealthAsia</a>
        `;

  const emailDetails = { to, content, subject, text };

  _sendEmailNotification(emailDetails);
};

const _sendEmailNotification = emailDetails => {
  bookAppNotifService
    .sendBookAppEmailNotification(emailDetails)
    .then(res => {
      console.log("Sending email notification success!", res);
    })
    .catch(err => {
      console.log(err);
    });
};

export { sendNotification };
