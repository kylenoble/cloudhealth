const DOH_Interpretation = {
  weight: {
    optimal: `Your weight is normal for your height but there are still other factors to consider such as body composition, visceral fat, and your metabolism. Know how to optimize your weight. Book an appointment now to talk to our Doctor and Nutritionist-Dietitian to learn more. `,
    suboptimal: `Your weight is normal for your height but there are still other factors to consider such as body composition, visceral fat, and your metabolism. Know how to optimize your weight. Book an appointment now to talk to our Doctor and Nutritionist-Dietitian to learn more. `,
    neutral: `Your weight is normal for your height but there are still other factors to consider such as body composition, visceral fat, and your metabolism. Know how to optimize your weight. Book an appointment now to talk to our Doctor and Nutritionist-Dietitian to learn more. `,
    compromised: `Your weight is higher than the expected range for your height. There are a lot of factors to consider such as body composition, visceral fat, and your metabolism. Know how to optimize your weight. Book an appointment now to talk to our Doctor and Nutritionist-Dietitian to learn more.`,
    alarming: `Your weight is way higher than the expected range for your height. There are a lot of factors to consider such as body composition, visceral fat, and your metabolism. Know how to manage your weight. Book an appointment now to talk to our Doctor and Nutritionist-Dietitian to learn more. `
  },
  nutrition: {
    optimal: `Food is medicine and is one of the predictors of your overall health.  Learn more how you can optimize your food choices.  Book an appointment now to talk to our Doctor and Nutritionist-Dietitian to learn more. `,
    suboptimal: `Food is medicine and is one of the predictors of your overall health.  Learn more how you can optimize your food choices.  Book an appointment now to talk to our Doctor and Nutritionist-Dietitian to learn more. `,
    neutral: `Food is medicine and is one of the predictors of your overall health.  Learn more how you can optimize your food choices.  Book an appointment now to talk to our Doctor and Nutritionist-Dietitian to learn more. `,
    compromised: `You have poor food choices which can greatly affect your overall health. Learn how food becomes your medicine by letting us analyze your food choices. Book an appointment now to talk to our Doctor and Nutritionist-Dietitian to learn more.`,
    alarming: `You have poor food choices which can greatly affect your overall health. This predisposes you to a lot of health problems.  Learn how food becomes your medicine by letting us analyze your food choices. Book an appointment now to talk to our Doctor and Nutritionist-Dietitian to learn more. `
  },
  sleep: {
    optimal: `Sleep is important to help your body recover and recuperate. You have good sleep hygiene. To further optimize your sleeping habits, book an appointment now to talk to our Doctor.`,
    suboptimal: `Sleep is important to help your body recover and recuperate. You have good sleep hygiene. To further optimize your sleeping habits, book an appointment now to talk to our Doctor.`,
    neutral: `Sleep is important to help your body recover and recuperate. You have good sleep hygiene. To further optimize your sleeping habits, book an appointment now to talk to our Doctor.`,
    compromised: `Your sleep hygiene can impair your energy and concentration. Sleep is important to help your body recover and recuperate. To adujst and improve your sleeping habits, book an appointment now to talk to our Doctor.`,
    alarming: `Your sleep hygiene can impair your energy and concentration. Sleep is important to help your body recover and recuperate. This predisposes you to a lot of health problems.  To adujst and improve your sleeping habits, book an appointment now to talk to our Doctor.`
  },
  detox: {
    optimal: `Our body has intricate processes to deal with all the substances to which we are exposed .  We need to be mindful how our ability eliminates these toxins. 
    Your initial assessment reveals that your body’s metabolism in relation to toxin is working.  To fully assess and to optimize your detoxification ability, book an appointment with your doctor today.
    `,
    suboptimal: `Our body has intricate processes to deal with all the substances to which we are exposed .  We need to be mindful how our ability eliminates these toxins. 
    Your initial assessment reveals that your body’s metabolism in relation to toxin is working.  To fully assess and to optimize your detoxification ability, book an appointment with your doctor today.
    `,
    neutral: `Our body has intricate processes to deal with all the substances to which we are exposed .  We need to be mindful how our ability eliminates these toxins. 
    Your initial assessment reveals that your body’s metabolism in relation to toxin is working.  To fully assess and to optimize your detoxification ability, book an appointment with your doctor today.
    `,
    compromised: `Our body has intricate processes to deal with all the substances to which we are exposed .  We need to be mindful how our ability eliminates these toxins. 
    Your initial assessment reveals that your body’s metabolism in relation to toxin is needs further monitoring.  To fully assess and to optimize your detoxification ability, book an appointment with your doctor today.`,
    alarming: `Our body has intricate processes to deal with all the substances to which we are exposed .  We need to be mindful how our ability eliminates these toxins. 
    Your initial assessment reveals that your body’s metabolism in relation to toxin is needs further monitoring.  To fully assess and to optimize your detoxification ability, book an appointment with your doctor today.`
  },
  stress: {
    optimal: `Stress manifests differently depending on the individual. Consistent stress over long periods of time can become a serious threat to one's wellness.  Your initial results showed you are handling your stressors well. To further improve your stress tolerance, book an appointment with your doctor today.`,
    suboptimal: `Stress manifests differently depending on the individual. Consistent stress over long periods of time can become a serious threat to one's wellness.  Your initial results showed you are handling your stressors well. To further improve your stress tolerance, book an appointment with your doctor today.`,
    neutral: `Stress manifests differently depending on the individual. Consistent stress over long periods of time can become a serious threat to one's wellness.  Your initial results showed you are handling your stressors well. To further improve your stress tolerance, book an appointment with your doctor today.`,
    compromised: `Stress manifests differently depending on the individual. Your initial results showed that your response to stressors needs to optimize because the stressors may be causing changes in your mood, energy and concentration, and could affect your weight and nutrition. Book an appointment now to know how to best handle your stress levels. To further improve your stress tolerance, book an appointment with your doctor today.`,
    alarming: `Stress manifests differently depending on the individual. Consistent stress over long periods of time can become a serious threat to one's wellness.  Your initial results showed that your response to stressors needs to improve. Book an appointment now to know how to best handle your stress levels and improve your tolerance to stress.`
  },
  exercise: {
    optimal: `Movement is medicine.  Your results reveal you that your movement is not compromised.  However, to effectively move well to fully optimize your health, book an appointment with your Doctor and Kinesiologiist for your movement activities. `,
    suboptimal: `Movement is medicine.  Your results reveal you that your movement is not compromised.  However, to effectively move well to fully optimize your health, book an appointment with your Doctor and Kinesiologiist for your movement activities. `,
    neutral: `Movement is medicine.  Your results reveal you that your movement is not compromised.  However, to effectively move well to fully optimize your health, book an appointment with your Doctor and Kinesiologiist for your movement activities. `,
    compromised: `Movement is medicine.  There are different mechanisms by which movement can reverse the major underlying causes of different diseases. The lack of movement in your body can impair your energy and metabolism. Learn how to effectively move well for the improvement of your health.  Book an appointment with your Doctor and Kinesiologiist to assess and improve your movement activities. `,
    alarming: `Movement is medicine.  There are different mechanisms by which movement can reverse the major underlying causes of different diseases. The lack of movement in your body impairs your energy and metabolism. This causes a lot of health concerns. Learn how to effectively move well for the improvement of your health.  Book an appointment with your Doctor and Kinesiologiist to assess and improve your movement activities. `
  }
};

export { DOH_Interpretation };
