import React from "react";
import { css } from "glamor";
import ReactTooltip from "react-tooltip";

const Tooltip = input => {
  return (
    <React.Fragment>
      <ReactTooltip />
      <span
        className={info}
        data-tip={tooltipWrapper(input.hint)}
        data-type="light"
        data-html
      >
        i
      </span>
    </React.Fragment>
  );
};

const tooltipWrapper = hint => {
  let title = "";
  if (hint.title) {
    title = `<span style="padding: 5px; background: #364563; display: block; text-align:center; color: white; font-size: 15px">${
      hint.title
      }</span>`
  }
  return `
      <div style="max-width: 400px; border-top-left-radius: 3px; border-top-right-radius: 3px; overflow: hidden">
        ${title}
        <p style="padding: 15px; white-space: normal;"> ${hint.description}</p>
      </div>`;
};

export { Tooltip, tooltipWrapper };

let info = css({
  borderRadius: "50px/50px",
  width: 50,
  height: 50,
  border: "2px solid #ccc",
  padding: "0 7px",
  marginLeft: 10,
  fontWeight: 700,
  transition: "250ms ease",
  color: "#ccc"
});
