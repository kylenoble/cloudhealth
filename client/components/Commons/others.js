import Colors from "../NewSkin/Colors";

const GetStatus = score => {
  const wtdScore = score;

  let subOptimalLabel = "";
  if (wtdScore >= 1.0 && wtdScore <= 2.9) {
    subOptimalLabel = "Optimal";
  } else if (wtdScore >= 3.0 && wtdScore <= 4.9) {
    subOptimalLabel = "Suboptimal";
  } else if (wtdScore >= 5.0 && wtdScore <= 5.9) {
    subOptimalLabel = "Neutral";
  } else if (wtdScore >= 6.0 && wtdScore <= 8.5) {
    subOptimalLabel = "Compromised";
  } else if (wtdScore >= 8.6) {
    subOptimalLabel = "Alarming";
  }

  return subOptimalLabel;
};

const GetColor = status => {
  //let status = 'alarming'
  const colors = {
    Optimal: Colors.optimal,
    Suboptimal: Colors.suboptimal,
    Neutral: Colors.neutral,
    Compromised: Colors.compromised,
    Alarming: Colors.alarming
  };
  return colors[status];
};

const MultiFilter = (array, filters) => {
  const filterKeys = Object.keys(filters);
  return array.filter(item =>
    filterKeys.every(key => !!~filters[key].indexOf(item[key]))
  );
};

const GetPercentileRank = (position, usersCount) => {
  return Math.round((100 * (position - 0.05)) / usersCount);
};

const converInchesToFeet = value => {
  const source = value;
  const ft = source / 12;
  const inches = source % 12;
  return { feet: Math.floor(ft), inches: Math.floor(inches) };
};

export {
  GetStatus,
  GetColor,
  MultiFilter,
  GetPercentileRank,
  converInchesToFeet
};
