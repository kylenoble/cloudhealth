import AnswersService from "../../utils/answerService.js";

const Answer = new AnswersService();

const getAnswersByDistinctQuestion = userid =>
  Answer.getAnswerList(
    {
      column: "question_id",
      operator: "gt",
      value: 0,
      distinct_question: true
    },
    userid
  )
    .then(res => Promise.resolve(res))
    .catch(err => {
      console.log(err);
    });

export { getAnswersByDistinctQuestion };
