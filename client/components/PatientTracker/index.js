import React from "react";
import { css } from "glamor";
import PrimeInfo from "../DoctorDashboard/patientProfile/primeInfo";
import ConsultHistory from "./consultHistory";
import ConsultsService from "../../utils/consultsService";
import UserService from "../../utils/userService.js";
import AppointmentService from "../../utils/appointmentService.js";
import PatientProfile from "../../components/DoctorDashboard/patientProfile";
import PatientDetails from "../DoctorDashboard/patientProfile/patientDetails";
import Colors from "../NewSkin/Colors";
import Button from "../NewSkin/Components/Button";
import Card from "../NewSkin/Wrappers/Card";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import { labelClass, scrollClass, inputClass } from "../NewSkin/Styles";

const Consults = new ConsultsService();
const User = new UserService();
const Appointment = new AppointmentService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.user = this.props.user;

    this.state = {
      selectedUserId: "",
      appointmentIDs: [],
      appoinments: [],
      toggle: {},
      patientInfo: {
        id: "",
        name: "",
        age: "",
        sex: "",
        avatar: ""
      },
      consultHistory: [],
      filteredConsultHistory: [],
      filtered: false,
      fields: [],
      fullname: {
        id: "",
        name: "fullname",
        label: "Search Patient",
        question: "Enter Employee ID or Full Name",
        type: "text",
        validation: "",
        options: [],
        error: {
          active: false,
          message: "Enter Employee ID or Full Name"
        },
        value: ""
      },
      outcome: {
        id: "",
        name: "outcome",
        label: "Filter Outcome by",
        question: "Ex. Not Improving",
        type: "select",
        validation: "",
        options: [
          {
            name: "Improving",
            value: "Improving"
          },
          {
            name: "Not Improving",
            value: "Not Improving"
          }
        ],
        error: {
          active: false,
          message: "Ex. No Improving"
        },
        value: ""
      },
      resultList: [],
      selectedResult: []
    };

    this._renderView = this._renderView.bind(this);
    this._getPatientInfo = this._getPatientInfo.bind(this);
    this._toggle = this._toggle.bind(this);

    this.scrollToDetails = React.createRef();
  }

  componentDidMount() {
    this._fetchQueuedPatients().then(res => {
      if (res.length > 0) {
        this.setState({ appoinments: res }, () => {
          this._getAppointmentIDs();
        });
      }
    });
  }

  _fetchQueuedPatients(date = null) {
    let qry;

    if (date) {
      qry = { active: this.props.active, apmt_datetime: date };
    } else {
      qry = { active: this.props.active };
    }
    return Appointment.getAppointmentsList(qry)
      .then(res => {
        if (res.name === "Error" || res.message === "Unauthorized") {
          console.log(res);
        } else {
          return Promise.resolve(res);
        }
      })
      .catch(e => {
        console.log(e);
      }); // you would show/hide error messages with component state here
  }

  _getAppointmentIDs() {
    const appoinments = this.state.appoinments;

    const appointment_ids = appoinments.filter(
      (obj, pos, arr) =>
        arr.map(mapObj => mapObj.patient_id).indexOf(obj.patient_id) === pos
    );

    const appointmentIDArray = [];

    appointment_ids.forEach(item => {
      appointmentIDArray.push(item.patient_id);
    });

    this.setState({ appointmentIDs: appointmentIDArray });
  }

  _toggle(divname) {
    const toggle = this.state.toggle;
    toggle[divname] = !toggle[divname];

    this.setState({
      toggle
    });
  }

  _resolveResult(item) {
    const resultList = this.state.resultList;
    const selectedResult = this.state.selectedResult;
    const filtered = resultList.filter(result => result.id === item.id);
    const res = filtered[0];

    this.setState(
      {
        selectedResult,
        patientInfo: res,
        selectedUserId: ""
      },
      function () {
        this.scrollToBottom();
        this._getConsults();
      }
    );
  }

  _getPatientInfo(str) {
    User.getPatientInfo(str)
      .then(res => {
        if (res.status !== 404) {
          this.setState(
            {
              resultList: res
            },
            () => {
              //  this._resolveResult()
            }
          );
        } else {
          this.setState({
            patientInfo: {
              id: "",
              name: "",
              age: "",
              sex: "",
              avatar: ""
            },
            consultHistory: []
          });
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _getConsults() {
    const param = {
      patient_id: this.state.patientInfo.id,
      status: 1
    };
    Consults.get(param)
      .then(res => {
        if (res.status !== "404") {
          this.setState({
            consultHistory: res
          });
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _onChange(e) {
    this._checkInputValue(e);
    const outcome = this.state.outcome;
    const fullname = this.state.fullname;
    if (e.target.name === "fullname") {
      const string = `${e.target.value}_type_eq_Patient`;
      fullname.value = e.target.value;
      this._getPatientInfo(string);
      outcome.value = "";
      this.setState({
        filtered: false,
        fullname,
        filteredConsultHistory: []
      });
    } else if (e.target.name === "outcome") {
      if (e.target.value) {
        outcome.value = e.target.value;
        this.setState({
          outcome,
          filtered: true
        });
      } else {
        this.setState({
          filtered: false,
          filteredConsultHistory: []
        });
      }
      const fillterd = this.state.consultHistory.filter(
        str => str.health_outcome === e.target.value
      );
      this.setState({
        filteredConsultHistory: fillterd
      });
    }
  }

  _renderfields(fld) {
    let currentFields;
    if (fld === "search") {
      currentFields = [this.state.fullname];
    }
    if (fld === "filter") {
      currentFields = [this.state.outcome];
    }

    const flds = currentFields.map((item, i) => (
      <div key={i} className={searchDiv}>
        {item.type === "text"
          ? this._renderInput(item)
          : this._displayDropdown(item)}
      </div>
    ));
    return flds;
  }

  _displayDropdown(input) {
    return (
      <div className={fldsDiv}>
        <div className={labelStyle}>{input.label}</div>
        <select
          id={input.name}
          className={inputStyle}
          name={input.name}
          value={this.state.outcome.value}
          onChange={this._onChange.bind(this)}
        >
          <option key="a" value="">
            All
          </option>
          {this._displayListOptions(input)}
        </select>
      </div>
    );
  }

  _displayListOptions(input) {
    const html = [];
    const options = input.options;
    for (let i = 0; i < options.length; i++) {
      if (options[i].label === input.value) {
        html.push(
          <option key={i} value={input.value}>
            {options[i].name}
          </option>
        );
      } else {
        html.push(
          <option key={i} value={options[i].label}>
            {options[i].name}
          </option>
        );
      }
    }

    return html;
  }

  _checkInputValue = e => {
    const elem = e.target;
    const elemVal = elem.value;
    const elemParent = elem.parentNode;

    if (elemVal.length >= 1) {
      elemParent.classList.add(inputFocused);
    } else {
      elemParent.classList.remove(inputFocused);
    }
  };

  _onFocus = e => {
    const elem = e.target;
    const elemParent = elem.parentNode;
    elem.style.borderColor = "#36c4f1";
    elemParent.classList.add(inputFocused);
  };

  _onBlur = e => {
    const elem = e.target;
    const elemParent = elem.parentNode;
    elem.style.borderColor = "#ccc";

    if (elem.value <= 0) {
      elemParent.classList.remove(inputFocused);
    }
  };

  handleSearchTitleClick = () => {
    const searchInput = document.getElementById("search-input");
    searchInput.focus();
  };

  _renderInput(item) {
    return (
      <Card fullWidth classes={[cardStyles]} styles={{ marginTop: 20 }}>
        <label htmlFor="" className={labelClass}>
          <img style={{ width: 15, marginRight: 10 }} src="/static/icons/search.svg" alt="" />
          Search Patient
          </label>
        <input
          id="search-input"
          name={item.name}
          className={inputClass}
          placeholder={item.question}
          type="text"
          value={this.state.fullname.value}
          onChange={this._onChange.bind(this)}
          onFocus={this._onFocus}
          onBlur={this._onBlur}
        />
      </Card>
    );
  }

  _renderPatienProfile() {
    return (
      <PatientProfile
        active={this.props.active}
        userid={this.state.selectedUserId}
        back={this._back}
        activeTab={this.props.activeTab}
        setActiveView={this.props.setActiveView}
        user={this.props.user}
        restricted={this.state.restricted}
      />
    );
  }

  _renderInfo() {
    const patientInfo = this.state.patientInfo;

    if (patientInfo.id) {
      return (
        <GridContainer fullWidth>
          <Card classes={[cardStyles]} fullWidth>
            <div
              className={`wrap`}
              id="item-preview"
            >
              <PatientDetails info={this.state.patientInfo} />
              <div className={labelStyle} style={{ textAlign: 'left' }}>Consult History</div>
              {this.state.appointmentIDs.indexOf(patientInfo.id) === -1 ? (
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    justifyContent: "center"
                  }}
                >
                  <div
                    className={[warningStyle, fullWidthDiv].join(" ")}
                    style={{ fontSize: "large" }}
                  >
                    You do not have an existing appointment with this client hence,
                    only the Health Tracker will be available for viewing/editing.
              </div>
                </div>
              ) : this.state.filteredConsultHistory.length > 0 ||
                this.state.consultHistory.length > 0 ? (
                    <ConsultHistory groupBy="id" patient_id={patientInfo.id} />
                  ) : (
                    <div
                      className={[fullWidthDiv, warningStyle].join(" ")}
                      style={{ fontSize: "large", width: "100%" }}
                    >
                      No Consult History.
            </div>
                  )}
            </div>
          </Card>
        </GridContainer>
      );
    }
  }

  groupBy(collection, property) {
    let val;
    let index;
    const values = [];
    const result = [];
    for (let i = 0; i < collection.length; i++) {
      val = collection[i][property];
      index = values.indexOf(val);
      if (index > -1) result[index].push(collection[i]);
      else {
        values.push(val);
        result.push([collection[i]]);
      }
    }
    return result;
  }

  _formatDisplay(str) {
    let html = [];
    if (str) {
      const strArr = str.split(",");
      html = strArr.map(item => <div className={listStyle}>{item}</div>);
    }
    return html;
  }

  _handleChange() { }

  _checkRender() {
    return (
      <>
        {this.state.resultList.length > 0 ?
          <GridContainer gap={20} fullWidth center styles={{ alignContent: 'start', marginBottom: 20 }}>
            {this.state.resultList.length > 0 ? this._resultList() : null}
            {this._renderInfo()}
            {this.state.selectedUserId ? this._renderPatienProfile() : null}
            <div ref={this.scrollToDetails}></div>
          </GridContainer> : null
        }
      </>
    );
  }

  _viewDetails(item) {
    this._resolveResult(item);
  }

  scrollToBottom = () => {
    if (this.scrollToDetails.current) {
      this.scrollToDetails.current.scrollIntoView({ behavior: "smooth" });
    }
    // const resListLength = this.state.resultList.length;
    // if (resListLength < 5) {
    //   this.toBottom();
    // } else {
      // window.scrollTo(0, 100);
    //   this.updateStyle();
    // }
    this.updateStyle();
  };

  toBottom = () => {
    const c = document.documentElement.scrollTop || document.body.scrollTop;
    const bodyHeight = document.body.offsetHeight;

    if (c <= 300) {
      window.requestAnimationFrame(this.toBottom);
      const y = bodyHeight / 35;
      const scroll = c + y;
      window.scrollTo(0, scroll);
    }
    this.updateStyle();
  };

  updateStyle = () => {
    const time = setTimeout(() => {
      const itemPreview = document.querySelector(".resultItemPreview");

      if (itemPreview) {
        itemPreview.classList.add(bordered);
        setTimeout(() => {
          itemPreview.classList.remove(bordered);
        }, 2000);
        clearTimeout(time);
      }
    }, 150);
  };

  _viewHealthTracker(item) {
    this.setState({ selectedUserId: item.id, patientInfo: {} }, () => {this.scrollToBottom()});
  }

  _resultList() {
    const resultList = this.state.resultList;
    const items = resultList.map((item, i) => (
      <GridContainer columnSize="1fr 1fr 1fr" fullWidth styles={{ padding: '10px 15px' }}>
        <div className={resultName} onClick={() => this._viewDetails(item)}>
          {item.name}
        </div>

        <div className={resultCompany}>{item.company}</div>
        {/** <div onClick={this.state.appointmentIDs.indexOf(item.id) != -1 ? ()=>this._viewHealthTracker(item) : null} className={itemContentLinked}>View Health Tracker {this.state.appointmentIDs.indexOf(item.id) != -1 ? null : <span className={warningStyle}>(Restricted)</span>}</div> **/}

        <div className={resultButton}>
          <Button styles={{ maxWidth: '100%' }}>
            <button onClick={() => this._viewHealthTracker(item)}>
              View Health Tracker
              {this.state.appointmentIDs.indexOf(item.id) !== -1 ? null : (
                <span className={warningStyle}> (Restricted)</span>
              )}
            </button>
          </Button>
        </div>
      </GridContainer>
    ));

    return (
      <Card classes={[cardStyles]}>
        <div className={`${itemsDiv}`}>
          <label className={labelClass}>
            <img style={{ width: 20, marginRight: 10 }} src="/static/icons/search_result.png" alt="" />
            Search {resultList.length > 1 ? "Results" : "Result"}
          </label>
          <div className={itemStyleLable}>
            <GridContainer columnSize="1fr 1fr 1fr" fullWidth>
              <div>Name</div>
              <div>Company</div>
              <div />
            </GridContainer>
          </div>
          <div
            className={`${results} ${resultList.length > 1 ? limitHeight : ""} ${scrollClass}`}
          >
            <GridContainer gap={10} classes={[resultsWrapper]}>
              {items}
            </GridContainer>
          </div>
        </div>
      </Card>
    );
  }
  _renderView() {
    return (
      <React.Fragment>
        <GridContainer fullWidth gap={20} styles={{ minHeight: '90vh', padding: '0 20px' }}>
          {this._renderfields("search")}
          {this._checkRender()}
        </GridContainer>
      </React.Fragment>
    );
  }

  _setView() {
    this.props.setActiveView("DoctorDashboard");
    this.props.linkView("patientOutcomes");
  }

  render() {
    return (
      <div className={`${container}`}>
        <GridContainer gap={20} center fullWidth>
          {this._renderView()}
        </GridContainer>

      </div>
    );
  }
}

const resultsWrapper = css({
  '& > section': {
    transition: '250ms ease',
    '&:hover': {
      background: '#eee8 !important',
    },
    '&:nth-child(odd)': {
      background: '#eee5',
      borderRadius: 5,
    }
  }
})

const resultButton = css({
  display: 'grid',
  alignContent: 'center',
})

const resultCompany = css(resultButton, {
  textAlign: 'left !important',
})

const resultName = css(resultButton, {
  cursor: 'pointer',
  color: Colors.blueDarkAccent,
  textAlign: 'left !important',
  fontWeight: 500,
  ':hover': {
    color: Colors.skyblue
  }
})

const cardStyles = css({
  maxWidth: 1100,
  margin: '0 auto'
})

let bordered = css({
  border: "1px solid #36c4f1 !important",
  boxShadow: "0 8px 20px rgba(0,0,0,.2)"
});

let backBtn = css({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  borderRadius: 5,
  padding: "10px 20px !important",
  marginRight: 10,
  transition: "250ms ease",
  ":hover": {
    background: "#2fa8ce"
  }
});

let searchContainer = css({
  position: "relative",
  zIndex: 1
});

let resultTitle = css({
  padding: 10
});

let itemCompany = css({
  // textAlign: 'center'
});

let results = css({
  width: "100%",
  boxShadow: "inset 0 0px 10px rgba(0,0,0,0)",
  transition: "300ms ease",
  borderRadius: 5,
  padding: 15
});

let limitHeight = css({
  maxHeight: 400,
  overflowY: "auto",
  position: "relative"
  // boxShadow: 'inset 0 1px 20px rgba(0,0,0,.050)',
});

let searchTitle = css({
  display: "flex",
  justifyContent: "center",
  width: "100% !important",
  background: "none !important",
  marginBottom: 20,
  "> p": {
    color: "#364563",
    padding: "10px 20px",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    border: "1px solid #364563",
    borderRadius: 5,
    cursor: "pointer",
    "> img": {
      width: 15
    },
    "> span": {
      marginLeft: 10,
      fontSize: 20
    }
  }
});

let resultItemPreview = css({
  padding: 30,
  // boxShadow: '0 20px 32px -5px rgba(0,0,0,.1), 0 1px 16px rgba(0,0,0,.070)',
  boxShadow: "0 2px 8px rgba(0,0,0,.1)",
  borderRadius: 5,
  maxWidth: "70% !important",
  margin: '20px 0',
  position: "relative",
  //zIndex: 101,
  border: "1px solid rgba(0,0,0,0)",
  transition: "300ms ease",
  transition: "250ms ease",
  ":hover": {
    boxShadow: "0 8px 20px rgba(0,0,0,.2) !important"
  }
});

let resultItem = css({
  padding: 10,
  marginBottom: 5,
  transition: "300ms ease",
  // background: 'white',
  position: "relative",
  zIndex: "100",
  // cursor: 'pointer',
  ":hover": {
    background: "#fbfbfb"
  }
});

let resultWrapper = css({
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  position: "relative"
});

let searchResultWrapper = css({
  padding: 30,
  // boxShadow: '0 20px 32px -5px rgba(0,0,0,.1), 0 1px 16px rgba(0,0,0,.070)',
  boxShadow: "0 2px 8px rgba(0,0,0,.1)",
  borderRadius: 5,
  width: "70% !important",
  margin: 0,
  marginTop: 20,
  position: "relative",
  zIndex: 100,
  transition: "250ms ease",
  ":hover": {
    boxShadow: "0 8px 20px rgba(0,0,0,.2)"
  }
});

let inputFocused = css({
  "::before": {
    transform: "translateY(-10px) scale(.9) !important",
    color: "#36c4f1 !important"
  }
});

let searchbarInputWrapper = css({
  width: "100%",
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  position: "relative",
  marginTop: 20,
  zIndex: "100",
  display: 'block',
  "::before": {
    content: "Enter Employee ID or Fullname",
    position: "absolute",
    top: 0,
    left: 16,
    transformOrigin: "left",
    transform: "translateY(15px) scale(1)",
    color: "#ccc",
    zIndex: 100,
    transition: "transform 250ms ease"
  }
});

let searchbarInput = css({
  width: "100% !important",
  // borderRadius: 5,
  borderStyle: "none",
  borderBottom: "1px solid #ccc",
  margin: 0,
  position: "relative",
  zIndex: 101,
  background: "rgba(0,0,0,0)",
  fontSize: "1.2rem",
  ":focus": {
    boxShadow: "none",
    outline: "none"
  }
});

let searchbarWrapper = css({
  display: "flex",
  alignItems: "center",
  justifyContent: "center"
  // border: '1px solid red',
});

let fldsDiv = css({
  display: "flex",
  flexWrap: "wrap",
  width: "100%"
});

let container = css({
  width: "100%",
  display: "flex",
  justifyContent: "center",
  margin: "50px 0px 0px"
});

let topDiv = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "flexStart",
  flexWrap: "wrap"
});

let searchDiv = css({
  flex: "1 0 100%",
  display: "flex",
  justifyContent: "center"
});

let inputStyle = css({
  width: "300px",
  padding: 10,
  margin: "5px"
});

let labelStyle = css({
  width: '100%',
  color: Colors.blueDarkAccent,
  padding: '10px 0',
  fontWeight: 600,
  textTransform: "uppercase",
  borderTop: '1px solid #eee'
});

let itemsDiv = css({
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "flex-start",
  "@media screen and (orientation:portrait)": {
    width: "100%"
  }
});

let itemStyleLable = css({
  display: "flex",
  flex: "1 0 100%",
  color: "#000",
  textTransform: "capitalize",
  padding: "5px 0 10px 0"
});

let itemStyle = css({
  display: "flex",
  flex: "1 0 100%",
  textTransform: "capitalize"
});

let itemContent = css({
  width: "40%",
  paddingLeft: 10
});

let itemContentLinked = css({
  width: "40%",
  color: "#80cde9",
  textTransform: "capitalize",
  cursor: "pointer",
  transition: "250ms ease",
  ":hover": {
    color: "orange"
  }
});

let listStyle = css({
  //border: '1px solid',
  padding: 5
});

let warningStyle = css({
  fontSize: "x-small",
  // textTransform: "capitalize",
  fontStyle: "italic",
  color: "orange"
});

const fullWidthDiv = css({
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  alignItems: "center",
  minHeight: 50
});
