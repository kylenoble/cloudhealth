import {css} from 'glamor'
import moment from 'moment-timezone';
import ReactTooltip from 'react-tooltip'
import DIAGNOSTICS from '../../Consultations/diagnostics';
import PrimeInfo from './primeInfo'
import ConsultHistory from './consultHistory'
import PatientProfile from '../patientProfile'

export default class extends React.Component{

  constructor(props){
    super(props)
    this.state = {
      appointmentIDs : [],
      appoinments: [],
      toggle: {},
      patientInfo: {
        id: '',
        name: '',
        age: '',
        sex: '',
        avatar: ''
      },
      consultHistory: [],
      filteredConsultHistory: [],
      filtered: false,
      fields: [],
        fullname: {
          id: '',
          name: 'fullname',
          label: 'Search Patient',
          question: 'Enter Employee ID or Full Name',
          type: 'text',
          validation: '',
          options: [],
          error: {
            active: false,
            message: 'Enter Employee ID or Full Name'
          },
          value: ''
        },
        outcome: {
          id: '',
          name: 'outcome',
          label: 'Filter Outcome by',
          question: 'Ex. Not Improving',
          type: 'select',
          validation: '',
          options: [
            {
              name: "Improving",
              value: "Improving"
            },
            {
              name: "Not Improving",
              value: "Not Improving"
            },
          ],
          error: {
            active: false,
            message: 'Ex. No Improving'
          },
          value: ''
        },
        resultList: [],
        selectedResult: []
      }

    this._renderView = this._renderView.bind(this)
    this._getPatientInfo = this._getPatientInfo.bind(this)
    this._toggle = this._toggle.bind(this)

    }

  componentDidMount(){
    this.props.getAppoinments()
    .then(res=>{
      if(res.length > 0){
        this.setState({appoinments : res}, ()=>{
          this._getAppointmentIDs()
        })
      }
    })
  }

_getAppointmentIDs(){
    let appoinments = this.state.appoinments
    let appointmentIDs = this.state.appointmentIDs

    let appointment_ids = appoinments.filter((obj, pos, arr)=>{
      return arr.map(mapObj => mapObj['patient_id']).indexOf(obj['patient_id']) === pos
    })

    let appointmentIDArray = []

    appointment_ids.forEach(item=>{
      appointmentIDArray.push(item.patient_id)
    })

    this.setState({appointmentIDs : appointmentIDArray})
}

_toggle(divname){
    let toggle = this.state.toggle
    toggle[divname] = !toggle[divname]

    this.setState({
      toggle
    })

}


  render(){
    return(
      <div className={container}>
      {this._renderView()}
      </div>
    )
  }

  _resolveResult(item){
      let res
      let resultList = this.state.resultList
      let selectedResult = this.state.selectedResult
      let filtered = resultList.filter((result)=>{return result.id == item.id})
      res = filtered[0]
      let birthDate = new Date(res.birthday);
      let otherDate = new Date();
      var age = null
      if(res.birthday){
        age = (otherDate.getFullYear() - birthDate.getFullYear());
      }

      this.setState({
        selectedResult,
        patientInfo: {
          employee_id:res.employee_id,
          id: res.id,
          name: res.name,
          age: age,
          sex: res.gender,
          avatar: res.avatar
        }
      }, function(){
        this._getConsults()
      })
    }

  _getPatientInfo(str){
    this.props.getPatientInfo(str)
    .then(res=>{
      if(res.status != 404){
        this.setState({
          resultList : res
        }, function(){
        //  this._resolveResult()
        })

      }else{
        this.setState({
          patientInfo: {
            id: '',
            name: '',
            age: '',
            sex: '',
            avatar: ''
          },
          consultHistory:[]
        })
      }

    })
    .catch(e=>{console.log(e);})
  }

  _getConsults(){
    let param = {
      patient_id : this.state.patientInfo.id,
      status : 1
    }
    this.props.getConsults(param)
    .then(res =>{
      if(res.status != '404'){
        this.setState({
          consultHistory: res
        })
      }
      })
  }

  _onChange(e){
    let outcome = this.state.outcome
    let fullname = this.state.fullname
    if(e.target.name == 'fullname'){
      let string = `${e.target.value}_type_eq_Patient`
        fullname.value = e.target.value
        this._getPatientInfo(string)
        outcome.value = ''
        this.setState({
          filtered: false,
          fullname:fullname,
          filteredConsultHistory: []
        })
    }else if(e.target.name == 'outcome'){
      if(e.target.value){
        outcome.value = e.target.value
        this.setState({
          outcome:outcome,
          filtered: true,
        })
        var fillterd = this.state.consultHistory.filter((str)=> {
            return str.health_outcome == e.target.value
            });
      }else{
        this.setState({
          filtered: false,
          filteredConsultHistory: []
        })
      }
      this.setState({
        filteredConsultHistory: fillterd
      })
    }
  }

  _renderfields(fld){
      let currentFields
      if(fld === 'search'){
          currentFields = [this.state.fullname]
      }
      if(fld === 'filter'){
          currentFields = [this.state.outcome]
      }

      let flds = currentFields.map((item, i)=>
        <div key={i} className={searchDiv}>
          {item.type == 'text' ? this._renderInput(item) : this._displayDropdown(item)}
        </div>
      )
      return flds
  }

  _displayDropdown(input) {
      return (
        <div className={fldsDiv}>
        <div className={labelStyle}>{input.label}</div>
        <select id={input.name} className={inputStyle} name={input.name} value={this.state.outcome.value} onChange={this._onChange.bind(this)}>
        <option key='a' value=''>All</option>
        {this._displayListOptions(input)}
        </select>
      </div>
    )
  }

  _displayListOptions(input) {
    var html = [];
    let options = input.options
    for (var i = 0; i < options.length; i++) {
      if (options[i].label === input.value) {
        html.push (
          <option key={i} value={input.value}>{options[i].name}</option>
        )
      } else {
        html.push (
          <option key={i} value={options[i].label}>{options[i].name}</option>
        )
      }
    }

    return html
  }

  _renderInput(item){
    return(
      <div className={fldsDiv}>
      <div className={labelStyle}>{item.label}</div>
      <span><input name={item.name} className={inputStyle} placeholder={item.question} type='text' value={this.state.fullname.value} onChange={this._onChange.bind(this)}/></span>
      </div>

    )
  }

  _renderInfo(){

      let patientInfo = this.state.patientInfo

      if(patientInfo.id){
        return (
          <div className = 'wrap'>
              <PrimeInfo patientInfo = {this.state.patientInfo}/>
              {(this.state.filteredConsultHistory.length > 0 || this.state.consultHistory.length > 0) && this.state.appointmentIDs.indexOf(patientInfo.id) != -1 ? <ConsultHistory groupBy="id" patient_id={patientInfo.id}/> : <div className={warningStyle} style={{fontSize: 'large'}}>You do not have an existing appointment with this client hence, only the Health Tracker will be available for viewing/editing.</div>}
          </div>
        )
      }
    }

  groupBy(collection, property) {
    var i = 0, val, index,
        values = [], result = [];
    for (; i < collection.length; i++) {
        val = collection[i][property];
        index = values.indexOf(val);
        if (index > -1)
            result[index].push(collection[i]);
        else {
            values.push(val);
            result.push([collection[i]]);
        }
    }
    return result;
}



  // _renderDetails(){
  //   let consultData;
  //   if(this.state.filtered){
  //     consultData = this.state.filteredConsultHistory
  //   }else {
  //     consultData = this.state.consultHistory
  //   }
  //
  //   let html = []
  //
  //   var group = this.groupBy(consultData, "appointment_code");
  //   group.forEach((groupArray, x)=>{
  //     let index = 0
  //     let outcome = ''
  //     let Imbalances = []
  //     if(groupArray[groupArray.length-1].details.healthoutcome_details){
  //       index = DIAGNOSTICS.ITEMS.healthoutcome.findIndex(x => x.value==groupArray[groupArray.length-1].details.healthoutcome_details);
  //       outcome = DIAGNOSTICS.ITEMS.healthoutcome[index].label
  //     }
  //
  //     if(groupArray[groupArray.length-1].details.assimilation ){
  //       Imbalances.push(groupArray[groupArray.length-1].details.assimilation)
  //     }
  //     if(groupArray[groupArray.length-1].details.communication ){
  //       Imbalances.push(groupArray[groupArray.length-1].details.communication)
  //     }
  //     if(groupArray[groupArray.length-1].details.defenseAndRepair ){
  //       Imbalances.push(groupArray[groupArray.length-1].details.defenseAndRepair)
  //     }
  //     if(groupArray[groupArray.length-1].details.energy ){
  //       Imbalances.push(groupArray[groupArray.length-1].details.energy)
  //     }
  //     if(groupArray[groupArray.length-1].details.structuralIntegrity ){
  //       Imbalances.push(groupArray[groupArray.length-1].details.structuralIntegrity)
  //     }
  //     if(groupArray[groupArray.length-1].details.transport ){
  //       Imbalances.push(groupArray[groupArray.length-1].details.transport)
  //     }
  //
  //
  //     html[x] =
  //       <div key={x} className={detailsDiv}>
  //         {x == 0 ?
  //           <div key={"t"+x} className={detailsWrapper} style={{background: '#fff', textAlign: 'center', textTransform: 'uppercase', border: '1px dotted gray', padding: '10px 0 10px 0'}}>
  //               <div className={'vertline '+labelsStyle} style={{maxWidth: '10%', margin: 0}}>Last Consult</div>
  //               <div className={'vertline '+labelsStyle} style={{maxWidth: '10%', margin: 0}}>First Consult</div>
  //               <div className={'vertline '+labelsStyle} style={{maxWidth: '5%', margin: 0}}>Times</div>
  //               <div className={'vertline '+labelsStyle} style={{maxWidth: '10%', margin: 0}}>MD in-charge</div>
  //               <div className={'vertline '+labelsStyle} style={{maxWidth: '10%', margin: 0}}>Health Concern</div>
  //               <div className={'vertline '+labelsStyle} style={{maxWidth: '20%', margin: 0}}>Imbalances ID</div>
  //               <div className={'vertline '+labelsStyle} style={{maxWidth: '20%', margin: 0}}>Healt Plan and Summary</div>
  //               <div className={'vertline '+labelsStyle} style={{maxWidth: '10%', margin: 0}}>Health Outcome</div>
  //               <div className={'vertline '+labelsStyle} style={{maxWidth: '10%', margin: 0}}>Next Step</div>
  //           </div>
  //       : null}
  //             <div key={x} className={detailsWrapper}>
  //               <ReactTooltip className='tooltipStyle'/>
  //               <div className={'vertline '+detailsStyle} style={{maxWidth: '10%', margin: 0}}>{moment(groupArray[groupArray.length-1].consult_date).format('LL')}</div>
  //               <div className={'vertline '+detailsStyle} style={{maxWidth: '10%', margin: 0}}>{moment(groupArray[0].consult_date).format('LL')}</div>
  //               <div className={'vertline '+detailsStyle} style={{maxWidth: '5%', margin: 0, textAlign: 'center'}}>{groupArray.length}</div>
  //               <div className={'vertline '+detailsStyle} style={{maxWidth: '10%', margin: 0}}>{groupArray[groupArray.length-1].md_incharge}</div>
  //               <div className={'vertline '+detailsStyle} style={{maxWidth: '10%', margin: 0}}>{groupArray[groupArray.length-1].concern}</div>
  //               <div className={'vertline '+detailsStyle} style={{maxWidth: '20%', margin: 0}} onClick={Imbalances.toString() ? ()=>this._toggle(x+'a') : null}>
  //                   {Imbalances.toString()
  //                     ? <div  data-tip='Click to show more.' className={this.state.toggle[x+'a'] ? [transis, show].join(' ') : [transis].join(' ')}>
  //                       {this._formatDisplay(Imbalances.toString())}</div>
  //                     : null}
  //               </div>
  //
  //               <div className={'vertline '+detailsStyle} style={{maxWidth: '20%', margin: 0}} onClick={groupArray[groupArray.length-1].details.functionalDiagnosticsRecommendations_details ? ()=>this._toggle(x) : null}>
  //                   {groupArray[groupArray.length-1].details.functionalDiagnosticsRecommendations_details
  //                     ? <div  data-tip='Click to show more.' className={this.state.toggle[x] ? [transis, show].join(' ') : [transis].join(' ')}>
  //                       {this._formatDisplay(groupArray[groupArray.length-1].details.functionalDiagnosticsRecommendations_details)}</div>
  //                     : null}
  //               </div>
  //               <div className={'vertline '+detailsStyle} style={{maxWidth: '10%', margin: 0}}>{outcome}</div>
  //               <div className={'vertline '+detailsStyle} style={{maxWidth: '10%', margin: 0}}>{groupArray[groupArray.length-1].details.nextsteps_details} {groupArray[groupArray.length-1].details.nextsteps_reschedule}</div>
  //             </div>
  //       </div>
  //     }
  //   )
  //
  //   return (<div>
  //     {/**this._renderfields('filter')**/}
  //     {html}
  //     </div>)
  // }

  _formatDisplay(str){
    let html = []
    if(str){
      let strArr  = str.split(",")
      html = strArr.map(item=>{
        return(
          <div className={listStyle}>{item}</div>
        )
      })
    }
    return html
  }

  _handleChange() {

  }

_checkRender(){
  let selectedResult = this.state.selectedResult
  return(
      <div className='wrap'>
      { this.state.resultList.length > 0 ? this._resultList() : null}
      {this._renderInfo()}


      </div>
    )
}

_viewDetails(item){
  this._resolveResult(item)
}

_viewHealthTracker(item){

  this.props.userid(item.id);
  this.props.linkView('patientProfile');
  this.props.setActiveView('DoctorDashboard')
  this.props.setActiveTab('healthTracker')
  this.props.restrict(this.state.appointmentIDs.indexOf(item.id) === -1)
  // if(this.state.appointmentIDs.indexOf(item.id) != -1){
  //   this.props.userid(item.id);
  //   this.props.linkView('patientProfile');
  //   this.props.setActiveView('DoctorDashboard')
  //   this.props.setActiveTab('healthTracker')
  // }else{
  //   alert('Restricted')
  //   return
  // }

}



_resultList(){
  let resultList =  this.state.resultList
  let items = resultList.map((item, i)=>{

  return  <div key={i} className={itemStyle}>
    <div onClick={()=>this._viewDetails(item)} className={itemContentLinked}>{item.name}</div>
    <div className={itemContent}>{item.company}</div>
    {/** <div onClick={this.state.appointmentIDs.indexOf(item.id) != -1 ? ()=>this._viewHealthTracker(item) : null} className={itemContentLinked}>View Health Tracker {this.state.appointmentIDs.indexOf(item.id) != -1 ? null : <span className={warningStyle}>(Restricted)</span>}</div> **/}
    <div onClick={()=>this._viewHealthTracker(item)} className={itemContentLinked}>View Health Tracker {this.state.appointmentIDs.indexOf(item.id) != -1 ? null : <span className={warningStyle}>(Restricted)</span>}</div>
    </div>
  })

  return (
    <div className={itemsDiv}>
    Search {resultList.length > 1 ? 'Results' : 'Result'}
    <div className={itemStyleLable}><div className={itemContent}>Name</div><div className={itemContent}>Company</div></div>
    {items}
    </div>
  )
}
  _renderView(){
    return(
      <div className={topDiv}>
          <div className='backBtn' onClick={()=>this._setView()}>Back</div>
          {this._renderfields('search')}
          {this._checkRender()}
      </div>
    )
  }

  _setView(){
    this.props.setActiveView('DoctorDashboard')
    this.props.setLinkView('confirmedPatients')
  }
}

// styling

let detailsDiv = css({
  display: 'flex',
  flexWrap: 'wrap',
  width: '100%',
  ':nth-child(odd)': {background: '#a7daf5'},
  ':nth-child(even)': {background: '#d4edf9'}
})

let fldsDiv = css({
  display: 'flex',
  flexWrap: 'wrap',
  width: '100%'
})

let avatarStyle = css({
  marginRight: 'auto',
  marginLeft: 'auto',
    borderRadius: '25%',
    border: '1px solid rgba(204, 204, 204, 0.3)',
    height: '100px',
    width: '100px',
    align: 'center',
    verticalAlign: 'center',
    border: '2px solid #36c4f1',
    ':hover': {
      backgroundColor: '#ED81B9',
      border: '2px solid #ED81B9',
      color: 'white',
    },
  //  color: 'rgb(54, 69, 99)',
    '@media(max-width: 767px)': {
        height: '75px',
        width: '75px',
    }
})

let info = css({
  flex: '1 0 100%',
  padding: '5px',

})

let infoLabelStyle = css({
  fontWeight: 'bold',
  color: '#80cde9'
})

let infoDiv = css({
  width: '100%',
  display: 'flex',
  margin: '50px 5px 50px 5px'
})

let imageStyle = css({
  width: '100px',
  border: '2px solid #36c4f1',
  border: '1px solid rgba(204, 204, 204, 0.3)',
})

let box = css({
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'flexStart',
  margin: '10px',
  height:'100'
})


let detailsWrapper = css({
  width: '100%',
  display: 'flex',
  flexWrap:'wrap',
  justifyContent: 'center'
})


let labelsStyle = css({
  flex: '1',
  // minWidth: '10%',
  // maxWidth: '10%',
  padding: '5px',
  margin: '5px',
  fontWeight: 'bold',
  margin : '0 5px 0 5px'
})

let detailsStyle = css({
  flex: '1',
  // minWidth: '10%',
  // maxWidth: '10%',
  padding: '5px',
  margin: '5px',
  margin : '0 5px 0 5px',
  textTransform: 'capitalize'
})

let container = css({
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  margin: '50px 5px 5px 5px'
})

let topDiv = css({
  display: 'flex',
  flex: '1 0 100%',
  justifyContent:'flexStart',
  flexWrap: 'wrap'
})

let searchDiv = css({
  flex: '1 0 100%',
  display: 'flex',
  justifyContent: 'center'
})

let inputStyle = css({
  width: '300px',
  padding: '5px 5px 5px 10px',
  margin: '5px',

})

let labelStyle = css({
  width: '300px',
  background: '#1b75bb',
  color: '#fff',
  padding: '5px 5px 5px 100px',
  margin: '5px',
  textTransform: 'uppercase'
})

let itemsDiv = css({
  display: 'flex',
  flexWrap: 'wrap',
  width: '50%',
  margin: '20px 5px 5px 10px',
  justifyContent: 'flex-start',
  '@media screen and (orientation:portrait)': {
    width: '100%',
}
})

let itemStyleLable = css({
    display: 'flex',
    flex: '1 0 100%',
    color: '#000',
    textTransform: 'capitalize',
    padding: '5px 0 10px 0'
})

let itemStyle = css({
    display: 'flex',
    flex: '1 0 100%',
    textTransform: 'capitalize',
})

let itemContent = css({
  width: '40%'
})

let itemContentLinked = css({
  width: '40%',
  color: '#80cde9',
  textTransform: 'capitalize',
  cursor: 'pointer',
  'hover': {
    color: 'orange'
  }
})

let dropdownDiv = css({

  display: 'flex',
  flexWrap: 'wrap',
  border: '1px solid',
  width: '100%',
  height: 'auto'
})

let transis = css({
  display: 'block',
  padding: 5,
  //width: '100px',
  maxHeight: '30px',
  lineHeight: '50%',
  border: '1px dotted gray',
  WebkitTransition: '0.5s',
  MozTransition: '0.5s',
  OTransition: '0.5s',
  transition: '0.5s',
  overflow: 'hidden',
  cursor: 'pointer'
//  opacity: 0,
  //visibility: 'hidden',
})

let show = css({
  //opacity: 1,
  maxHeight: '100%',
  lineHeight: '100%',
  overflow: 'visible',
  //visibility: 'visible',
})

let listStyle = css({
  //border: '1px solid',
  padding: 5,

})

let warningStyle = css({
  fontSize: 'x-small',
  textTransform: 'capitalize',
  fontStyle: 'italic',
  color: 'orange',
})
