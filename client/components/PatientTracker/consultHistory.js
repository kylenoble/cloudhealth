import React from "react";
import { css } from "glamor";
import moment from "moment-timezone";
import Modal from "react-modal";
import DIAGNOSTICS from "../Consultations/diagnostics";
import ConsultsService from "../../utils/consultsService";
import ReactTable from "react-table";
import {
  ReactTableStyle,
  statusButton,
  inputClass,
  labelClass,
  combined,
  font,
  link,
  scrollClass,
} from "../NewSkin/Styles";
import Button from "../NewSkin/Components/Button";
import Colors from "../NewSkin/Colors";
import NoData from "../NewSkin/Components/NoData";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Card from "../NewSkin/Wrappers/Card";


const Consults = new ConsultsService();

//const diagnostics = Diagnostics
export default class extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = false
    this.state = {
      modal: { consult: false },
      consultDetails: {},
      appoinments: [],
      consultHistory: [],
      patientInfo: [],
      toggle: {},
      diagnostics: [],
      filter: {
        concern: "",
        outcome: ""
      },
      healthConcernOptions: [],
      healthoutcomeOptions: []
    };

    this._viewConsultDetails = this._viewConsultDetails.bind(this);
    this._consultDetails = this._consultDetails.bind(this);
    this.modalSwitch = this.modalSwitch.bind(this);
    this._renderFilter = this._renderFilter.bind(this);
  }

  componentWillMount() {
    let appointment_code = null;
    let concern = null;
    if (this.props.appointment_code && this.props.appointment_code !== "") {
      appointment_code = this.props.appointment_code;
    }

    if (this.props.concern && this.props.concern !== "") {
      concern = this.props.concern;
    }
    this._getConsults(this.props.patient_id, concern, appointment_code);
    const all = DIAGNOSTICS.ITEMS.healthoutcome.concat(
      DIAGNOSTICS.ITEMS.imbalances,
      DIAGNOSTICS.ITEMS.cns,
      DIAGNOSTICS.ITEMS.rgcc,
      DIAGNOSTICS.ITEMS.genosense,
      DIAGNOSTICS.ITEMS.gpl,
      DIAGNOSTICS.ITEMS.caam,
      DIAGNOSTICS.ITEMS.cellmaxLife
    );
    this.setState({ diagnostics: all });
  }

  componentDidMount() {
    this._isMounted = true
  }

  componentWillUnmount() {
    this._isMounted = false
  }

  componentWillReceiveProps(nextprops) {
    let appointment_code = null;
    let concern = null;
    if (nextprops.appointment_code && nextprops.appointment_code !== "") {
      appointment_code = nextprops.appointment_code;
    }

    if (nextprops.concern && nextprops.concern !== "") {
      concern = nextprops.concern;
    }

    this._getConsults(nextprops.patient_id, concern, appointment_code);
  }

  _getAppointments() {
    this.appointment
      .getAppointmentsList({ completed: false })
      .then(res => Promise.resolve(res));
  }

  _getConsults(id, concern = null, appointment_code = null) {
    let param = {};

    if (appointment_code) {
      param = {
        patient_id: id,
        status: 1,
        appointment_code
      };
    } else if (concern) {
      param = {
        patient_id: id,
        status: 1,
        concern
      };
    } else {
      param = {
        patient_id: id,
        status: 1
      };
    }

    Consults.get(param)
    .then(res => {
      this.setState({
        consultHistory: res
      });
    })
    .catch(e => {
      console.log(e);
    });
  }

  modalSwitch(name, bol) {
    const modal = this.state.modal;
    modal[name] = bol;
    if(this._isMounted){
      this.setState({
        modal
      });
    }
  }

  _consultDetails() {
    const consultDetails = this.state.consultDetails;
    if (consultDetails.details) {
      let outcome = "";

      if (consultDetails.details.healthoutcome_details) {
        const index = DIAGNOSTICS.ITEMS.healthoutcome.findIndex(
          x => x.value === consultDetails.details.healthoutcome_details
        );
        outcome = DIAGNOSTICS.ITEMS.healthoutcome[index].label;
      }
      return (<>
        <div className={labelClass}>
          Health Concern: {consultDetails.concern}{" "}
        </div>
        <div className={scrollClass} style={{maxHeight: 400, overflow: 'auto', paddingRight: 20}}>
          {consultDetails.details.history ? (
            <Card classes={[cardClass]}>
              <div className={detailsTitle}>
              History, Current Symptoms, Conditions and Concerns{" "}
              </div>
              <p className={detailsContent}>{consultDetails.details.history}</p>
            </Card>
            
            // <div className={consultItemStyle}>
            //   <div style={{ width: "100%", textAlign: "left" }}>
            //     <div className={itemLabelStyle} style={{ width: "100%" }}>
            //       History, Current Symptoms, Conditions and Concerns{" "}
            //     </div>
            //   </div>
            //   <div
            //     style={{
            //       flex: 1,
            //       padding: "3px 30px",
            //       textAlign: "left",
            //       background: "#b7cfe3",
            //       borderRight: 5
            //     }}
            //   >
            //     {consultDetails.details.history}
            //   </div>
            // </div>
          ) : null}
          {consultDetails.details.assimilation_details ? (
            <Card classes={[cardClass]}>
              <div className={detailsTitle}>
              Assimilation{" "}
              </div>
              <p className={detailsContent}>{consultDetails.details.assimilation_details}</p>
            </Card>
            // <div className={consultItemStyle}>
            //   <div className={itemLabelStyle}>Assimilation </div>{" "}
            //   <div className={consItemStyle}>
            //     {consultDetails.details.assimilation_details}
            //   </div>
            // </div>
          ) : null}
          {consultDetails.details.communication_details ? (
            <Card classes={[cardClass]}>
              <div className={detailsTitle}>
              Communication{" "}
              </div>
              <p className={detailsContent}>{consultDetails.details.communication_details}</p>
            </Card>
            // <div className={consultItemStyle}>
            //   <div className={itemLabelStyle}>Communication </div>
            //   <div className={consItemStyle}>
            //     {consultDetails.details.communication_details}
            //   </div>
            // </div>
          ) : null}
          {consultDetails.details.defenseAndRepair_details ? (
            <Card classes={[cardClass]}>
              <div className={detailsTitle}>
              Defense and Repair{" "}
              </div>
              <p className={detailsContent}>{consultDetails.details.defenseAndRepair_details}</p>
            </Card>
            // <div className={consultItemStyle}>
            //   <div className={itemLabelStyle}>Defense and Repair </div>{" "}
            //   <div className={consItemStyle}>
            //     {consultDetails.details.defenseAndRepair_details}
            //   </div>
            // </div>
          ) : null}
          {consultDetails.details.energy_details ? (
            <Card classes={[cardClass]}>
              <div className={detailsTitle}>
              Energy{" "}
              </div>
              <p className={detailsContent}>{consultDetails.details.energy_details}</p>
            </Card>
            // <div className={consultItemStyle}>
            //   <div className={itemLabelStyle}>Energy </div>{" "}
            //   <div className={consItemStyle}>
            //     {consultDetails.details.energy_details}
            //   </div>
            // </div>
          ) : null}
          {consultDetails.details.structuralIntegrity_details ? (
            <Card classes={[cardClass]}>
              <div className={detailsTitle}>
              Structural Integrity{" "}
              </div>
              <p className={detailsContent}>{consultDetails.details.structuralIntegrity_details}</p>
            </Card>
            // <div className={consultItemStyle}>
            //   <div className={itemLabelStyle}>Structural Integrity </div>{" "}
            //   <div className={consItemStyle}>
            //     {consultDetails.details.structuralIntegrity_details}
            //   </div>
            // </div>
          ) : null}
          {consultDetails.details.transport_details ? (
            <Card classes={[cardClass]}>
              <div className={detailsTitle}>
              Transport{" "}
              </div>
              <p className={detailsContent}>{consultDetails.details.transport_details}</p>
            </Card>
            // <div className={consultItemStyle}>
            //   <div className={itemLabelStyle}>Transport </div>{" "}
            //   <div className={consItemStyle}>
            //     {consultDetails.details.transport_details}
            //   </div>
            // </div>
          ) : null}
          {consultDetails.details.medications_supplements_details ? (
            <Card classes={[cardClass]}>
              <div className={detailsTitle}>
              Medications / Supplements{" "}
              </div>
              <p className={detailsContent}>{consultDetails.details.medications_supplements_details}</p>
            </Card>
            // <div className={consultItemStyle}>
            //   <div className={itemLabelStyle}>Medications / Supplements </div>{" "}
            //   <div className={consItemStyle}>
            //     {consultDetails.details.medications_supplements_details}
            //   </div>
            // </div>
          ) : null}
          {consultDetails.details
            .functionalDiagnosticsRecommendations_details ? (
              <Card classes={[cardClass]}>
              <div className={detailsTitle}>
                Functional Diagnostics Recommendations{" "}
              </div>
              <p className={detailsContent} dangerouslySetInnerHTML={{ __html: this._formatDisplay(
                  consultDetails.details
                    .functionalDiagnosticsRecommendations_details
                ) }} />
            </Card>
          ) : null}
          {consultDetails.details.otherRecommendations_details ? (
            <Card classes={[cardClass]}>
              <div className={detailsTitle}>
              Other Recommendations{" "}
              </div>
              <p className={detailsContent}>{consultDetails.details.otherRecommendations_details}</p>
            </Card>
            // <div className={consultItemStyle}>
            //   <div className={itemLabelStyle}>Other Recommendations </div>{" "}
            //   <div className={consItemStyle}>
            //     {consultDetails.details.otherRecommendations_details}
            //   </div>
            // </div>
          ) : null}
          {consultDetails.details.healthoutcome_details ? (
            <Card classes={[cardClass]}>
              <div className={detailsTitle}>
              Health Outcome{" "}
              </div>
              <p className={detailsContent}>{outcome}</p>
            </Card>
            // <div className={consultItemStyle}>
            //   <div className={itemLabelStyle}>Health Outcome </div>{" "}
            //   <div className={consItemStyle}>{outcome}</div>
            // </div>
          ) : null}
          {consultDetails.details.targethealthoutomes_details ? (
            <Card classes={[cardClass]}>
              <div className={detailsTitle}>
              Target Health Outcomes{" "}
              </div>
              <p className={detailsContent}>{consultDetails.details.targethealthoutomes_details}</p>
            </Card>
            // <div className={consultItemStyle}>
            //   <div className={itemLabelStyle}>Target Health Outcomes </div>{" "}
            //   <div className={consItemStyle}>
            //     {consultDetails.details.targethealthoutomes_details}
            //   </div>
            // </div>
          ) : null}

          {consultDetails.details.nextsteps_details ? (
            <Card classes={[cardClass]}>
              <div className={detailsTitle}>
              Next Steps{" "}
              </div>
              <p className={detailsContent}>{consultDetails.details.nextsteps_details}</p>
            </Card>
            // <div className={consultItemStyle}>
            //   <div className={itemLabelStyle}>Next Steps </div>{" "}
            //   <div className={consItemStyle}>
            //     {consultDetails.details.nextsteps_details}
            //   </div>
            // </div>
          ) : null}
          {consultDetails.details.nextsteps_reschedule ? (
            <Card classes={[cardClass]}>
              <div className={detailsTitle}>
              Next Schedule{" "}
              </div>
              <p className={detailsContent}>{moment(consultDetails.details.nextsteps_reschedule).format("LLL")}</p>
            </Card>
            // <div className={consultItemStyle}>
            //   <div className={itemLabelStyle}>Next Schedule </div>{" "}
            //   <div className={consItemStyle}>
            //     {moment(consultDetails.details.nextsteps_reschedule).format(
            //       "LLL"
            //     )}
            //   </div>
            // </div>
          ) : null}
        </div>
      </>);
    }
  }

  _formatDisplay(str) {
    let html = "";
    if (str) {
      const strArr = str.split(",");
      strArr.map((item, i) => (
      html += `<span key={${i}} style="display: block; text-align: left; padding-bottom: 2px">
          ${this._getLabel(item)}
        </span>`
      ));
    }
    return html;
  }

  groupBy(collection, property) {
    let val;
    let index;
    const values = [];
    const result = [];

    for (let i = 0; i < collection.length; i++) {
      val = collection[i][property];
      index = values.indexOf(val);
      if (index > -1) result[index].push(collection[i]);
      else {
        values.push(val);
        result.push([collection[i]]);
      }
    }
    return result;
  }

  _getLabel(str) {
    let label = "";
    const diagnostics = this.state.diagnostics;
    const index = diagnostics.findIndex(x => x.value === str);
    if (index !== -1) {
      label = diagnostics[index].label;
    }
    return label;
  }

  _viewConsultDetails(id) {
    const index = this.state.consultHistory.findIndex(x => x.id === id);
    const consultDetails = this.state.consultHistory[index];
    this.setState(
      {
        consultDetails
      },
      () => this.modalSwitch("consult", true)
    );
    // reference to _consultDetails
  }

  _handleChange(e) {
    const filter = this.state.filter;
    if (e.target.name === "concern") {
      filter.concern = e.target.value;
    }
    if (e.target.name === "outcome") {
      filter.outcome = e.target.value;
    }

    this.setState({ filter });
  }

  _renderFilter() {
    const consults = this.state.consultHistory;
    const healthoutcomeOptions = [];
    const healthConcernOptions = [];

    consults.forEach(ite => {
      if (healthConcernOptions.indexOf(ite.concern) === -1) {
        healthConcernOptions.push(ite.concern);
      }

      if (
        healthoutcomeOptions.indexOf(ite.details.healthoutcome_details) === -1
      ) {
        healthoutcomeOptions.push(ite.details.healthoutcome_details);
      }
    });

    const concernOptions = healthConcernOptions.map((item, i) => (
      <option key={i} value={item}>{item}</option>
    ));

    const outcomeOptions = healthoutcomeOptions.map((item, i) => {
      let outcome = "";
      if (item) {
        const index = DIAGNOSTICS.ITEMS.healthoutcome.findIndex(
          x => x.value === item
        );
        outcome = DIAGNOSTICS.ITEMS.healthoutcome[index].label;
      }
      return <option key={i} value={item}>{outcome}</option>;
    });
    return (
      <GridContainer columnSize={'20% 30% 20% 30%'} styles={{margin: '0 auto 10px'}}>
        <span className={labelClass} style={{margin: 'auto'}}>Health Concerns:</span>
        <select
          name="concern"
          className={inputClass} style={{width: '90%', padding: 5}}
          onChange={this._handleChange.bind(this)}
        >
          <option value="">All</option>
          {concernOptions}
        </select>
        <span className={labelClass} style={{margin: 'auto'}}>Health Outcome:</span>
        <select
          name="outcome"
          className={inputClass} style={{width: '90%', padding: 5}}
          onChange={this._handleChange.bind(this)}
        >
          <option value="">All</option>
          {outcomeOptions}
        </select>
        </GridContainer>
    );
  }

  _renderDetails() {
    const consultData = this.state.consultHistory;
    let html = [];

    if(consultData.length < 0){
      return <NoData text="No Consultation Found" />
    }

    const group = this.groupBy(consultData, this.props.groupBy);
    const cd = [];
    group.forEach(ite => {
      cd.push(ite[0]);
    });

    const healthConcernString = this.state.filter.concern;
    const healthoutcomeString = this.state.filter.outcome;
    let filteredD = [];

    if (healthConcernString !== "" && healthoutcomeString !== "") {
      filteredD = cd.filter(
        d =>
          d.concern.toLowerCase().indexOf(healthConcernString.toLowerCase()) >
          -1
      );
      filteredD = filteredD.filter(
        d =>
          d.details.healthoutcome_details
            .toLowerCase()
            .indexOf(healthoutcomeString.toLowerCase()) > -1
      );
    } else if (healthConcernString !== "") {
      filteredD = cd.filter(
        d =>
          d.concern.toLowerCase().indexOf(healthConcernString.toLowerCase()) >
          -1
      );
    } else if (healthoutcomeString !== "") {
      filteredD = cd.filter(
        d =>
          d.details.healthoutcome_details
            .toLowerCase()
            .indexOf(healthoutcomeString.toLowerCase()) > -1
      );
    } else {
      filteredD = cd;
    }

    if (filteredD.length < 0) {
      return <NoData text="No Result found" />;
    }

    filteredD.map((groupArray, x) => {
      const data = groupArray;
      let index = 0;
      const Imbalances = [];
      if (data.details.healthoutcome_details) {
        index = DIAGNOSTICS.ITEMS.healthoutcome.findIndex(
          // eslint-disable-next-line no-shadow
          x => x.value === data.details.healthoutcome_details
        );
        data.outcome = DIAGNOSTICS.ITEMS.healthoutcome[index].label;
      }

      if (data.details.assimilation) {
        Imbalances.push(data.details.assimilation);
      }
      if (data.details.communication) {
        Imbalances.push(data.details.communication);
      }
      if (data.details.defenseAndRepair) {
        Imbalances.push(data.details.defenseAndRepair);
      }
      if (data.details.energy) {
        Imbalances.push(data.details.energy);
      }
      if (data.details.structuralIntegrity) {
        Imbalances.push(data.details.structuralIntegrity);
      }
      if (data.details.transport) {
        Imbalances.push(data.details.transport);
      }
      data.imbalances = Imbalances
    })

    let columns = [
      {
        Header: "Consult Dates",
        accessor: "consult_date",
        Cell: row => (
          <span>
            {moment(row.value).format("MMM D, YYYY hh:mm A")}
          </span>
        ),
        style: { justifyContent: "center" },
        width: 200
      },
      {
        Header: "MD in-charge",
        accessor: "md_incharge", 
        style: { justifyContent: "center", whiteSpace: "unset" },
        width: 250
      },
      {
        Header: "Health Concern",
        accessor: "concern",
        style: { textAlign: "center", whiteSpace: "unset" },
        width: 150
      },
      // {
      //   Header: "Imbalances ID",
      //   accessor: "imbalances",
      //   Cell: row => (
      //     row.value.toString() ? <>
      //     <ReactTooltip />
      //       <div
      //         className={combined([statusButton, font(0.8), link("#fff")])}
      //         data-tip={tooltipWrapper({title: "Imbalances ID",
      //           description: this._formatDisplay(row.value.toString())}
      //         )}
      //         data-type="light"
      //         data-html
      //         style={{
      //           background: Colors.detoxColor, padding: 0, width: 120, margin: 'auto'
      //         }}
      //       >
      //         View details
      //       </div>
      //       </>
      //     : null
      //   ),
      //   style: { textAlign: "center", whiteSpace: "unset" },
      //   width: 200
      // },
      // {
      //   Header: "Health Plan and Summary",
      //   accessor: "details",
      //   Cell: row => (
      //     row.value.functionalDiagnosticsRecommendations_details ? <>
      //     <div
      //         className={combined([statusButton, font(0.8), link("#fff")])}
      //         data-tip={tooltipWrapper({title: "Health Plan and Summary",
      //           description: this._formatDisplay(row.value.functionalDiagnosticsRecommendations_details)}
      //         )}
      //         data-type="light"
      //         data-html
      //         style={{
      //           background: Colors.detoxColor, padding: 0, width: 120, margin: 'auto'
      //         }}
      //       >
      //         View details
      //       </div>
      //       </>
      //     : null
      //   ),
      //   style: { textAlign: "center", whiteSpace: "unset" },
      //   width: 200
      // },
      {
        Header: "Health Outcome",
        accessor: "outcome",
        style: { whiteSpace: "unset"},
        width: 200
      },
      // {
      //   Header: "Next Step",
      //   accessor: "details",
      //   Cell: row => (
      //     <div
      //       style={{margin: 0 }}
      //     >
      //       {row.value.nextsteps_details
      //         ? `${row.value.nextsteps_details}. `
      //         : null}{" "}
      //       {row.value.nextsteps_reschedule
      //         ? `Next Appointment date : ${moment(
      //           row.value.nextsteps_reschedule
      //           ).format("MMM D, YYYY.  hh:mma")}`
      //         : null}
      //     </div>
      //   ),
      //   style: { whiteSpace: "unset", margin: "0 auto" },
      //   width: 150
      // },
      {
        Header: "Details",
        accessor: "id",
        Cell: row => (
          <div
            className={combined([statusButton, font(0.8), link("#fff")])}
            style={{
              background: Colors.skyblue, padding: 0, width: 140, margin: 'auto'
            }}
            onClick={() => this._viewConsultDetails(row.value)}
          >
            Consult details
          </div>
        ),
        width: 150
      }
    ]

    html = <div className={`${ReactTableStyle} ${scrollClass}`} style={{maxWidth: 'unset', maxHeight: 400}}>
      <ReactTable
        data={filteredD}
        columns={columns}
        defaultPageSize={5}
        minRows={1}
        className={`-highlight`}
        width={100}
        getTdProps={(state, rowInfo, column, instance) => {
          return {
            onClick: (e, handleOriginal) => {
              if (handleOriginal) {
                handleOriginal()
              }
            }
          }
        }}
        style={{maxHeight: 390}}
      />
    </div>
    
    return (
      <div style={{ width: "100%" }}>
        {this._renderFilter()}
        {html}
        <Button styles={{margin: '10px auto 0'}}>
          <button onClick={this.props.modalSwitch}>Close</button>
        </Button>
      </div>
    );
  }


  render() {
    return (
      <div className={historyDiv}>
        {this._renderDetails()}
        <Modal
          isOpen={this.state.modal.consult}
          onRequestClose={() => this.modalSwitch("consult", false)}
          shouldCloseOnOverlayClick={false}
          style={customStyles}
        >
          {this._consultDetails()}
          <Button styles={{margin: '10px auto 0'}}>
            <button onClick={() => this.modalSwitch("consult", false)}>Close</button>
          </Button>
        </Modal>
      </div>
    );
  }
}

// styling
let historyDiv = css({
  width: "100%",
  display: "flex",
  padding: 20
});

const customStyles = {
  content: {
    textAlign: "center",
    zIndex: 6,
    width: "70%",
    height: "auto",
    top: "55%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    padding: '20px 30px',
    boxShadow: "-3px 3px 30px #424242"
  }
};

let detailsTitle = css({
  margin: 0,
  display: 'block',
  padding: '5px 15px',
  background: '#80cde9',
  textTransform: 'uppercase',
  color: '#fff',
  borderRadius: 5,
  borderBottomRightRadius: 0,
  borderBottomLeftRadius: 0,
  textAlign: 'left'
})

let cardClass = css({
  padding: '0', 
  width: '100%', 
  borderRadius: 5, 
  marginBottom: 15
})

let detailsContent = css({
  padding: '10px 20px',
  textAlign: 'left'
})