import { css } from "glamor";
export default class extends React.Component {
  render() {
    if(this.props.html !== undefined){
      return <div className={notAllowedStyle} dangerouslySetInnerHTML={{ __html: this.props.message }} />
    }
    return <div className={notAllowedStyle}>{this.props.message}</div>;
  }
}

let notAllowedStyle = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  fontWeight: "500",
  fontStyle: "italic",
  justifyContent: "center",
  alignItems: "center",
  minHeight: "40vh",
  alignContent: "flex-start"
});
