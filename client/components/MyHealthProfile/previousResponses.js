import React, { useState, useContext } from "react";
import {
  Sleep,
  PersonalInfo,
  Summary,
  Nutrition,
  Readiness,
  Weight,
  Stress,
  Detox,
  Movement,
  Symptoms,
  Health
} from "./PreviousResponses/";
import { UserContext } from "../../context";

import { btnStyle, selectedBtn } from "./PreviousResponses/style.js";

const PreviousResponses = (props) => {
  const [state, setState] = useState({ view: "" });
  const context = useContext(UserContext);

  const switchView = string => {
    setState({ ...state, view: string });
  };

  const renderView = (survey) => {
    const view = state.view;
    
    if(context.user.type === 'Patient'){
      survey = context.healthSummaryData;
    }
    if (view === "sleep") {
      return <Sleep data={survey} />;
    } else if (view === "personalInfo") {
      return <PersonalInfo data={survey} />;
    } else if (view === "nutrition") {
      return <Nutrition data={survey} />;
    } else if (view === "readiness" && context.user.type === 'Patient') {
      return <Readiness />;
    } else if (view === "weight") {
      return <Weight data={survey} />;
    } else if (view === "stress") {
      return <Stress data={survey} />;
    } else if (view === "detox") {
      return <Detox data={survey} />;
    } else if (view === "movement") {
      return <Movement data={survey} />;
    } else if (view === "symptoms") {
      return <Symptoms userid={props.userid} />;
    } else if (view === "health") {
      return <Health data={survey} />;
    }
    return <Summary data={survey} />;
  };

  return (
    <div style={{ display: "flex", flexWrap: "wrap", flex: 1 }}>
      <div
        style={{
          flex: "1 0 100%",
          margin: "0 0 5px 0",
          padding: 5,
          borderBottom: "2px solid #364563"
        }}
      >
        <span
          className={
            state.view === "" ? [selectedBtn, btnStyle].join(" ") : btnStyle
          }
          onClick={() => switchView("")}
        >
          Summary
        </span>

        <span
          className={
            state.view === "personalInfo"
              ? [selectedBtn, btnStyle].join(" ")
              : btnStyle
          }
          onClick={() => switchView("personalInfo")}
        >
          Personal Info
        </span>
        {context.user.type === 'Patient' ?
        <span
          className={
            state.view === "readiness"
              ? [selectedBtn, btnStyle].join(" ")
              : btnStyle
          }
          onClick={() => switchView("readiness")}
        >
          Readiness
        </span> : null
        }
        <span
          className={
            state.view === "weight"
              ? [selectedBtn, btnStyle].join(" ")
              : btnStyle
          }
          onClick={() => switchView("weight")}
        >
          Weight
        </span>

        <span
          className={
            state.view === "nutrition"
              ? [selectedBtn, btnStyle].join(" ")
              : btnStyle
          }
          onClick={() => switchView("nutrition")}
        >
          Nutrition
        </span>
        <span
          className={
            state.view === "sleep"
              ? [selectedBtn, btnStyle].join(" ")
              : btnStyle
          }
          onClick={() => switchView("sleep")}
        >
          Sleep
        </span>
        <span
          className={
            state.view === "stress"
              ? [selectedBtn, btnStyle].join(" ")
              : btnStyle
          }
          onClick={() => switchView("stress")}
        >
          Stress
        </span>

        <span
          className={
            state.view === "detox"
              ? [selectedBtn, btnStyle].join(" ")
              : btnStyle
          }
          onClick={() => switchView("detox")}
        >
          Detox
        </span>
        <span
          className={
            state.view === "movement"
              ? [selectedBtn, btnStyle].join(" ")
              : btnStyle
          }
          onClick={() => switchView("movement")}
        >
          Movement
        </span>
        <span
          className={
            state.view === "symptoms"
              ? [selectedBtn, btnStyle].join(" ")
              : btnStyle
          }
          onClick={() => switchView("symptoms")}
        >
          Symptoms Review
        </span>
        <span
          className={
            state.view === "health"
              ? [selectedBtn, btnStyle].join(" ")
              : btnStyle
          }
          onClick={() => switchView("health")}
        >
          Health Goal / Issues
        </span>
      </div>
      <div style={{ flex: "1 0 100%" }}>{renderView(props.survey)}</div>
    </div>
  );
};

export default PreviousResponses;
