import React, { useContext } from "react";

import { UserContext } from "../../../context";
import ReactTable from "react-table";
import moment from "moment-timezone";
import { mainDiv, itemDiv, label, text } from "./style.js";

const Summary = props => {
  const context = useContext(UserContext);

  // const survey = context.healthSummaryData;
  const survey = props.data;
  const columns = [
    {
      Header: "Summary",
      accessor: "weight",
      Cell: item => {
        console.log("ITEM", item);
        return (
          <div className={mainDiv}>
            <div className={itemDiv}>
              <span className={label}>
                Submitted: {moment(item.original.date_created).format("lll")}
              </span>
              <span className={text}>&nbsp;</span>
            </div>
            {/* <div className={itemDiv}>
              <span className={label}>
                Your Percentile Rank in your company :
              </span>
              <span className={text}>
                {item.value.data ? item.value.data.rank : 0}
              </span>
            </div>
            <div className={itemDiv}>
              <span className={label}>Overall Percentile Rank :</span>
              <span className={text}>
                {item.value.data ? item.value.data.overAllRank : 0}
              </span>
            </div> */}
            <div className={itemDiv}>
              <span className={label}>Overall Health Risk Score :</span>
              <span className={text}>
                {item.value.data
                  ? item.value.data.overAllScore.value.toFixed(1)
                  : 0}
                , {item.value.data ? item.value.data.overAllStatus : ""}
              </span>
            </div>
            <div className={itemDiv}>
              <span className={label}>Weight Score :</span>
              <span className={text}>
                {item.value.data
                  ? (item.value.data.body.value * 2).toFixed(1)
                  : 0}
                , {item.value.data ? item.value.data.body.HStatus : ""}
              </span>
            </div>
            <div className={itemDiv}>
              <span
                className={label}
                style={{ textIndent: 20, fontStyle: "italic" }}
              >
                BMI :
              </span>
              <span className={text}>
                {item.value.data ? item.value.data.body.bmi : 0}
              </span>
            </div>
            <div className={itemDiv}>
              <span
                className={label}
                style={{ textIndent: 20, fontStyle: "italic" }}
              >
                WHR :
              </span>
              <span className={text}>
                {item.value.data ? item.value.data.body.whr : 0}
              </span>
            </div>
            <div className={itemDiv}>
              <span className={label}>Nutrition :</span>
              <span className={text}>
                {item.value.data
                  ? (item.value.data.nutrition.value * 2).toFixed(1)
                  : 0}
                , {item.value.data ? item.value.data.nutrition.HStatus : ""}
              </span>
            </div>
            <div className={itemDiv}>
              <span className={label}>Sleep :</span>
              <span className={text}>
                {item.value.data
                  ? (item.value.data.sleep.value * 2).toFixed(1)
                  : 0}
                , {item.value.data ? item.value.data.sleep.HStatus : ""}
              </span>
            </div>
            <div className={itemDiv}>
              <span className={label}>Stress :</span>
              <span className={text}>
                {item.value.data
                  ? (item.value.data.stress.value * 2).toFixed(1)
                  : 0}
                , {item.value.data ? item.value.data.stress.HStatus : ""}
              </span>
            </div>
            <div className={itemDiv}>
              <span className={label}>Detoxification :</span>
              <span className={text}>
                {item.value.data
                  ? (item.value.data.health.value * 2).toFixed(1)
                  : 0}
                , {item.value.data ? item.value.data.health.HStatus : ""}
              </span>
            </div>
            <div className={itemDiv}>
              <span className={label}>Movement :</span>
              <span className={text}>
                {item.value.data
                  ? (item.value.data.exercise.value * 2).toFixed(1)
                  : 0}
                , {item.value.data ? item.value.data.exercise.HStatus : ""}
              </span>
            </div>
          </div>
        );
      },
      style: { whiteSpace: "unset", cursor: "pointer" }
    }
  ];
  return context.fetching ? (
    <h4>Please Wait... {context.fetching}</h4>
  ) : (
    <ReactTable
      columns={columns}
      data={survey}
      showPageSizeOptions={false}
      defaultPageSize={1}
      sortable={false}
      filterable={false}
      resizable={false}
    />
  );
};

export { Summary };
