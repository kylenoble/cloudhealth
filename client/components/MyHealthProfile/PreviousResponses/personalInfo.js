import React, { useContext } from "react";
import { UserContext } from "../../../context";
import ReactTable from "react-table";
import moment from "moment-timezone";
import { mainDiv, itemDiv, label, text } from "./style.js";

const PersonalInfo = (props) => {
  const context = useContext(UserContext);

  // const survey = context.healthSummaryData;
  const survey = props.data;
  const columns = [
    {
      Header: "Personal Info",
      accessor: "personal_info",
      Cell: item => {
        return (
          <div className={mainDiv}>
            <div className={itemDiv}>
              <span className={label}>
                Submitted: {moment(item.original.date_created).format("lll")}
              </span>
              <span className={text}>&nbsp;</span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Gender :</span>
              <span className={text}>
                {item.value ? item.value.gender : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Birth Date :</span>
              <span className={text}>
                {item.value ? moment(item.value.birthday).format("ll") : ""}
              </span>
            </div>

          {item.value && item.value.gender !== 'Female' ?
          <>
            <div className={itemDiv}>
              <span className={label}>PSA Done?</span>
              <span className={text}>{item.value ? item.value.psa : ""}</span>
            </div>
            <div className={itemDiv}>
              <span className={label}>PSA Level :</span>
              <span className={text}>
                {item.value ? item.value.psaLevel : ""}
              </span>
            </div>
            <div className={itemDiv}>
              <span className={label}>Problem List :</span>
              <span className={text}>
                {item.value ? item.value.maleIssues : ""}
              </span>
            </div></>
            : null
          }
          {item.value && item.value.gender === 'Female' ?
            <><div className={itemDiv}>
              <span className={label}>Menstrual Status :</span>
              <span className={text}>
                {item.value ? item.value.menstrual : ""}
              </span>
            </div>
            <div className={itemDiv}>
              <span className={label}>Pregnant? :</span>
              <span className={text}>
                {item.value ? item.value.pregnant : ""}
              </span>
            </div></>
            : null
          }
            <div className={itemDiv}>
              <span className={label}>Relationship Status :</span>
              <span className={text}>
                {item.value ? item.value.relationship : ""}
              </span>
            </div>
          { item.value && item.value.relationship !== "Single" ?
            <div className={itemDiv}>
              <span className={label}>Number of Children :</span>
              <span className={text}>
                {item.value ? item.value.numberChildren : ""}
              </span>
            </div>
            : null
          }
            <div className={itemDiv}>
              <span className={label}>Race :</span>
              <span className={text}>{item.value ? item.value.race : ""}</span>
            </div>
          {item.value && item.value.raceOther !== item.value.race ?
            <div className={itemDiv}>
              <span className={label}>Other Race :</span>
              <span className={text}>
                {item.value ? item.value.raceOther : ""}
              </span>
            </div>
            : null
          }
          </div>
        );
      },
      style: { whiteSpace: "unset", cursor: "pointer" }
    }
  ];
  return (
    <ReactTable
      columns={columns}
      data={survey}
      showPageSizeOptions={false}
      defaultPageSize={1}
      sortable={false}
      filterable={false}
      resizable={false}
    />
  );
};

export { PersonalInfo };
