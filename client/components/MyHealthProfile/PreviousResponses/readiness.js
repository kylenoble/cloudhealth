import React, { useContext } from "react";
import { UserContext } from "../../../context";
import ReactTable from "react-table";
import moment from "moment-timezone";
import { mainDiv, itemDiv, label, text } from "./style.js";

const Readiness = () => {
  const context = useContext(UserContext);

  const survey = context.healthSummaryData;
  const columns = [
    {
      Header: "Readiness",
      accessor: "readiness",
      Cell: item => {
        return (
          <div className={mainDiv}>
            <div className={itemDiv}>
              <span className={label}>
                Submitted: {moment(item.original.date_created).format("lll")}
              </span>
              <span className={text}>&nbsp;</span>
            </div>
            <div className={itemDiv}>
              <span className={label}>Significantly modify your diet :</span>
              <span className={text}>
                {item.value ? item.value.modifyDiet : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
                Take several nutritional supplements each day :
              </span>
              <span className={text}>
                {item.value ? item.value.nutritionalSupplements : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
                Keep a record of everything you eat each day :
              </span>
              <span className={text}>
                {item.value ? item.value.recordEating : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
                Modify your lifestyle (e.g., work demands, sleep habits) :
              </span>
              <span className={text}>
                {item.value ? item.value.modifyLifestyle : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Practice a relaxation technique :</span>
              <span className={text}>
                {item.value ? item.value.practiceRelaxation : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Engage in regular exercise :</span>
              <span className={text}>
                {item.value ? item.value.regularExercise : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
                Have periodic lab tests to assess your progress :
              </span>
              <span className={text}>
                {item.value ? item.value.labTests : ""}
              </span>
            </div>
          </div>
        );
      },
      style: { whiteSpace: "unset", cursor: "pointer" }
    }
  ];
  return (
    <ReactTable
      columns={columns}
      data={survey}
      showPageSizeOptions={false}
      defaultPageSize={1}
      sortable={false}
      filterable={false}
      resizable={false}
    />
  );
};

export { Readiness };
