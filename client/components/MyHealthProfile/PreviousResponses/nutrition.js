import React, { useContext } from "react";
import { UserContext } from "../../../context";
import ReactTable from "react-table";
import moment from "moment-timezone";
import { mainDiv, itemDiv, label, text } from "./style.js";

const Nutrition = (props) => {
  const context = useContext(UserContext);

  // const survey = context.healthSummaryData;
  const survey = props.data;
  const columns = [
    {
      Header: "Nutrition",
      accessor: "nutrition",
      Cell: item => {
        return (
          <div className={mainDiv}>
            <div className={itemDiv}>
              <span className={label}>
                Submitted: {moment(item.original.date_created).format("lll")}
              </span>
              <span className={text}>&nbsp;</span>
            </div>
            <div className={itemDiv}>
              <span className={label}>Special Diet :</span>
              <span className={text}>
                {item.value
                  ? item.value.specialDiet === "default"
                    ? "< 10%"
                    : item.value.specialDiet
                  : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Food Allergies :</span>
              <span className={text}>
                {item.value
                  ? item.value.foodAllergies === "default"
                    ? "< 10%"
                    : item.value.foodAllergies
                  : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Usual Meal :</span>
              <span className={text}>
                {item.value
                  ? item.value.usualMeal === "default"
                    ? "< 10%"
                    : item.value.usualMeal[0] === "," ? item.value.usualMeal.substr(1) : item.value.usualMeal
                  : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Seafood Consumption (Percent) :</span>
              <span className={text}>
                {item.value
                  ? item.value.seafoodConsumedPercent === "default"
                    ? "< 10%"
                    : item.value.seafoodConsumedPercent
                  : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Meat Consumption (Percent) :</span>
              <span className={text}>
                {item.value
                  ? item.value.meatConsumedPercent === "default"
                    ? "< 10%"
                    : item.value.meatConsumedPercent
                  : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Vegetables Consumption (Percent) :</span>
              <span className={text}>
                {item.value
                  ? item.value.vegetableConsumedPercent === "default"
                    ? "< 10%"
                    : item.value.vegetableConsumedPercent
                  : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Fruit Consumption (Percent) :</span>
              <span className={text}>
                {item.value
                  ? item.value.fruitConsumedPercent === "default"
                    ? "< 10%"
                    : item.value.fruitConsumedPercent
                  : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Starch Consumption (Percent) :</span>
              <span className={text}>
                {item.value
                  ? item.value.starchConsumedPercent === "default"
                    ? "< 10%"
                    : item.value.starchConsumedPercent
                  : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Poultry Consumption (Percent) :</span>
              <span className={text}>
                {item.value
                  ? item.value.poultryConsumedPercent === "default"
                    ? "< 10%"
                    : item.value.poultryConsumedPercent
                  : ""}
              </span>
            </div>
          </div>
        );
      },
      style: { whiteSpace: "unset", cursor: "pointer" }
    }
  ];
  return (
    <ReactTable
      columns={columns}
      data={survey}
      showPageSizeOptions={false}
      defaultPageSize={1}
      sortable={false}
      filterable={false}
      resizable={false}
    />
  );
};

export { Nutrition };
