import React, { useContext } from "react";
import { UserContext } from "../../../context";
import ReactTable from "react-table";
import moment from "moment-timezone";
import { mainDiv, itemDiv, label, text } from "./style.js";

const Health = (props) => {
  const context = useContext(UserContext);

  // const survey = context.healthSummaryData;
  const survey = props.data;

  const columns = [
    {
      Header: "Health Goals / Issues",
      accessor: "health_goals",
      Cell: item => {
        console.log("ITEM", item);
        return (
          <div className={mainDiv}>
            <div className={itemDiv}>
              <span className={label}>
                Submitted: {moment(item.original.date_created).format("lll")}
              </span>
              <span className={text}>&nbsp;</span>
            </div>
            <div className={itemDiv}>
              <span className={label} style={{maxWidth: '40%', width: '40%', minWidth: '40%'}}>Health Issues :</span>
              <span className={text}>{item.original.health_issues.value}</span>
            </div>

            <div className={itemDiv}>
              <span className={label} style={{maxWidth: '40%', width: '40%', minWidth: '40%'}}>Health Goal 1 :</span>
              <span className={text}>
                {item.value
                  ? `(${item.value.goalOne_Cat}) ${item.value.goalOne}`
                  : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label} style={{maxWidth: '40%', width: '40%', minWidth: '40%'}}>Health Goal 2 :</span>
              <span className={text}>
                {item.value
                  ? `(${item.value.goalTwo_Cat}) ${item.value.goalTwo}`
                  : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label} style={{maxWidth: '40%', width: '40%', minWidth: '40%'}}>Health Goal 3 :</span>
              <span className={text}>
                {item.value
                  ? `(${item.value.goalThree_Cat}) ${item.value.goalThree}`
                  : ""}
              </span>
            </div>
          </div>
        );
      },
      style: { whiteSpace: "unset", cursor: "pointer" }
    }
  ];
  return (
    <ReactTable
      columns={columns}
      data={survey}
      showPageSizeOptions={false}
      defaultPageSize={1}
      sortable={false}
      filterable={false}
      resizable={false}
    />
  );
};

export { Health };
