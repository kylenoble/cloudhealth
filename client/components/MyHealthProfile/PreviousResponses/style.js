import { css } from "glamor";

const mainDiv = css({
  display: "flex",
  flexWrap: "wrap",
  flex: 1,
  width: '90%',
  margin: '5px auto'
});

const itemDiv = css({
  display: "flex",
  flex: "1 0 100%",
  borderBottom: '1px dotted #ddd',
  padding: '5px 20px'
});

const label = css({
  fontWeight: 500,
  maxWidth: "70%",
  minWidth: '50%',
  textAlign: "left",
  margin: "0 10px 0 10px"
});

const text = css({
  margin: "0 10px 0 10px",
  textTransform: "capitalize",
  width: '100%',
  textAlign: 'left'
});

const tdLabel = css({
  fontWeight: 500,
  width: "30%",
  textAlign: "left",
  borderBottom: "1px dotted",
  margin: "0 10px 0 10px"
});

const tableHeader = css({
  fontWeight: 500,
  textAlign: "center",
  //background: "#96dcf4",
  textTransform: "uppercase",
  margin: "0 10px 0 10px"
});

const tdText = css({
  margin: "0 10px 0 10px",
  borderBottom: "1px dotted",
  textTransform: "capitalize"
});

const btnStyle = css({
  flex: 1,
  fontWeight: 500,
  fontSize: 14,
  margin: "3px 3px 1px 3px",
  padding: "5px 5px 5px 5px",
  borderBottom: "5px solid transparent",
  //borderRadius: 10,
  cursor: "pointer",

  transition: "300ms ease",
  ":hover": {
    borderTop: "1px solid #364563",
    borderBottom: "5px solid #364563"
  }
});

const selectedBtn = css({
  borderBottom: "5px solid #364563",
  //background: "#81cce6",
  color: "#000"
});

export {
  mainDiv,
  itemDiv,
  label,
  text,
  tdText,
  tdLabel,
  tableHeader,
  btnStyle,
  selectedBtn
};
