import React, { useContext } from "react";
import { UserContext } from "../../../context";
import ReactTable from "react-table";
import moment from "moment-timezone";
import { mainDiv, itemDiv, label, text } from "./style.js";

const Stress = (props) => {
  const context = useContext(UserContext);

  // const survey = context.healthSummaryData;
  const survey = props.data;
  const columns = [
    {
      Header: "Stress",
      accessor: "stress",
      Cell: item => {
        return (
          <div className={mainDiv}>
            <div className={itemDiv}>
              <span className={label}>
                Submitted: {moment(item.original.date_created).format("lll")}
              </span>
              <span className={text}>&nbsp;</span>
            </div>
            <div className={itemDiv}>
              <span className={label}>Have you ever sought counseling? :</span>
              <span className={text}>
                {item.value ? item.value.counseling : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Are you currently in therapy? :</span>
              <span className={text}>
                {item.value ? item.value.therapy : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
              Daily Stressors (1-10; 1 lowest, 10 highest)
              </span>
              <span className={text}>&nbsp;</span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
                Work Stress :
              </span>
              <span className={text}>
                {item.value ? item.value.workStress : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
              Family Stress :
              </span>
              <span className={text}>
                {item.value ? item.value.familyStress : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
                Social Stress (relationships, networks, except family) :
              </span>
              <span className={text}>
                {item.value ? item.value.socialStress : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
                Financial Stress :
              </span>
              <span className={text}>
                {item.value ? item.value.financialStress : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
                Health Stress :
              </span>
              <span className={text}>
                {item.value ? item.value.healthStress : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
              Other Stress :
              </span>
              <span className={text}>
                {item.value ? item.value.otherStress : ""}
              </span>
            </div>
          </div>
        );
      },
      style: { whiteSpace: "unset", cursor: "pointer" }
    }
  ];
  return (
    <ReactTable
      columns={columns}
      data={survey}
      showPageSizeOptions={false}
      defaultPageSize={1}
      sortable={false}
      filterable={false}
      resizable={false}
    />
  );
};

export { Stress };
