import React, { useContext } from "react";
import { UserContext } from "../../../context";
import ReactTable from "react-table";
import moment from "moment-timezone";
import { mainDiv, itemDiv, label, text } from "./style.js";

const Movement = (props) => {
  const context = useContext(UserContext);

  // const survey = context.healthSummaryData;
  const survey = props.data;
  const columns = [
    {
      Header: "Movement",
      accessor: "movement",
      Cell: item => {
        return (
          <div className={mainDiv}>
            <div className={itemDiv}>
              <span className={label}>
                Submitted: {moment(item.original.date_created).format("lll")}
              </span>
              <span className={text}>&nbsp;</span>
            </div>
            <div className={itemDiv}>
              <span className={label} style={{width: "80%"}}>
                Do you exercise at least 150 minutes a week?
              </span>
              <span className={text}>
                {item.value ? item.value.weeklyExercise : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label} style={{width: "80%"}}>
                Does anything limit you from being physically active?
              </span>
              <span className={text}>
                {item.value ? item.value.limitActivity : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label} style={{width: "80%"}}>
                Do you feel unusually fatigued or sore after exercise?
              </span>
              <span className={text}>
                {item.value ? item.value.postActivitySoreness : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
                When you are at work, which of the following describes what you
                do? :
              </span>
              <span className={text}>
                {item.value ? item.value.workActivity : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
                How many hours per day do you spend sitting?
              </span>
              <span className={text}>
                {item.value ? item.value.sittingHours : ""}
              </span>
            </div>
          </div>
        );
      },
      style: { whiteSpace: "unset", cursor: "pointer" }
    }
  ];
  return (
    <ReactTable
      columns={columns}
      data={survey}
      showPageSizeOptions={false}
      defaultPageSize={1}
      sortable={false}
      filterable={false}
      resizable={false}
    />
  );
};

export { Movement };
