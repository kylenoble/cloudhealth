import React, { useContext } from "react";
import { UserContext } from "../../../context";
import ReactTable from "react-table";
import moment from "moment-timezone";
import { mainDiv, itemDiv, label, text } from "./style.js";

const Weight = (props) => {
  const context = useContext(UserContext);

  // const survey = context.healthSummaryData;
  const survey = props.data;
  const columns = [
    {
      Header: "Weight",
      accessor: "weight",
      Cell: item => {
        return (
          <div className={mainDiv}>
            <div className={itemDiv}>
              <span className={label}>
                Submitted: {moment(item.original.date_created).format("lll")}
              </span>
              <span className={text}>&nbsp;</span>
            </div>
            <div className={itemDiv}>
              <span className={label}>Height (in inches) :</span>
              <span className={text}>
                {item.value ? item.value.height : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Weight (in lbs) :</span>
              <span className={text}>
                {item.value ? item.value.weight : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
                Estimate Waist Measurement (in inches) :
              </span>
              <span className={text}>{item.value ? item.value.waist : ""}</span>
            </div>

            <div className={itemDiv}>
              <span className={label}>
                Estimate Hip Measurement (in inches) :
              </span>
              <span className={text}>{item.value ? item.value.hip : ""}</span>
            </div>
          </div>
        );
      },
      style: { whiteSpace: "unset", cursor: "pointer" }
    }
  ];
  return (
    <ReactTable
      columns={columns}
      data={survey}
      showPageSizeOptions={false}
      defaultPageSize={1}
      sortable={false}
      filterable={false}
      resizable={false}
    />
  );
};

export { Weight };
