import React, { useContext, useState, useEffect } from "react";
import { UserContext } from "../../../context";
import ReactTable from "react-table";
import moment from "moment-timezone";
import {
  mainDiv,
  itemDiv,
  label,
  tdLabel,
  tdText,
  tableHeader,
  btnStyle,
  selectedBtn
} from "./style.js";
import SummaryService from "../../../utils/summaryService";

const summaryService = new SummaryService();
/*

Head and Mind
Other
Eyes, Ears, Nose
Activity
Mouth, Throat, Lungs
Weight and Digestion


*/

class Symptoms extends React.Component {
  static contextType = UserContext;

  constructor(props) {
    super(props);
    this.state = {
      survey: [],
      filteredData: [],
      group: "Activity"
    };
  }

  componentWillMount() {
    this._getData();
  }

  switchView(string) {
    this.setState({ group: string });
    this._getData();
  }

  _getData() {
    let userid = this.context.user.type === 'Patient' ? this.context.user.id : this.props.userid
    summaryService
      .getHealthSurveySummary(userid)
      .then(r => {
        if (r) {
          const fd = r.filter(srvy => {
            let nsr = [];
            const nr = Object.keys(srvy.symptoms_review).forEach(sr => {
              if (srvy.symptoms_review[sr].group === this.state.group) {
                nsr.push(srvy.symptoms_review[sr]);
              }
            });
            srvy.symptoms_review = nsr;
            return true;
          });

          this.setState({ filteredData: fd });
        }
      });
  }

  render() {
    const columns = [
      {
        Header: "Muscle, Energy, Heart",
        accessor: "symptoms_review",
        Cell: items => {
          const divs = items.value.map(item => {
            return (
              <tr>
                <td className={tdLabel}>{item.label ? item.label : ""}</td>
                <td className={tdText} style={{width: '20%', textAlign: 'center'}}>{item.value ? item.value : 0}</td>
                <td className={tdText} style={{width: '20%', textAlign: 'center'}}>{item.count ? item.count : 0}</td>
              </tr>
            );
          });

          return (
            <div className={mainDiv}>
              <div className={itemDiv}>
                <div className={label}>
                  Date Created :{" "}
                  {moment(items.original.date_created).format("lll")}
                </div>
              </div>
              <table width={"100%"}>
                <tr className={tableHeader}>
                  <td style={{width: '30%'}}>&nbsp;</td>
                  <td style={{width: '20%', textAlign: 'center'}}>Intensity</td>
                  <td style={{width: '20%', textAlign: 'center'}}>Frequency</td>
                </tr>

                {divs}
              </table>
            </div>
          );
        },
        style: { whiteSpace: "unset", cursor: "pointer" }
      }
    ];
    const { group } = this.state;
    return (
      <div style={{ display: "flex", flexWrap: "wrap", flex: 1 }}>
        <div
          style={{
            flex: "1 0 100%",
            margin: "0 0 5px 0",
            padding: 5,
            borderBottom: "1px solid #364563"
          }}
        >
          <span
            className={
              group === "Activity"
                ? [selectedBtn, btnStyle].join(" ")
                : btnStyle
            }
            onClick={() => this.switchView("Activity")}
          >
            Muscle, Energy, Heart
          </span>
          <span
            className={
              group === "Eyes, Ears, Nose"
                ? [selectedBtn, btnStyle].join(" ")
                : btnStyle
            }
            onClick={() => this.switchView("Eyes, Ears, Nose")}
          >
            Eyes, Ears, Nose
          </span>
          <span
            className={
              group === "Head and Mind"
                ? [selectedBtn, btnStyle].join(" ")
                : btnStyle
            }
            onClick={() => this.switchView("Head and Mind")}
          >
            Head and Mind
          </span>
          <span
            className={
              group === "Mouth, Throat, Lungs"
                ? [selectedBtn, btnStyle].join(" ")
                : btnStyle
            }
            onClick={() => this.switchView("Mouth, Throat, Lungs")}
          >
            Mouth, Throat, Lungs
          </span>
          <span
            className={
              group === "Weight and Digestion"
                ? [selectedBtn, btnStyle].join(" ")
                : btnStyle
            }
            onClick={() => this.switchView("Weight and Digestion")}
          >
            Weight and Digestion
          </span>
          <span
            className={
              group === "Other" ? [selectedBtn, btnStyle].join(" ") : btnStyle
            }
            onClick={() => this.switchView("Other")}
          >
            Others
          </span>
        </div>
        <ReactTable
          columns={columns}
          data={this.state.filteredData}
          showPageSizeOptions={false}
          defaultPageSize={1}
          sortable={false}
          filterable={false}
          resizable={false}
        />
      </div>
    );
  }
}

export { Symptoms };
