import React, { useContext } from "react";
import { UserContext } from "../../../context";
import ReactTable from "react-table";
import moment from "moment-timezone";
import { mainDiv, itemDiv, label, text } from "./style.js";

const Detox = (props) => {
  const context = useContext(UserContext);

  // const survey = context.healthSummaryData;
  const survey = props.data;
  const columns = [
    {
      Header: "Detox",
      accessor: "detoxification",
      Cell: item => {
        return (
          <div className={mainDiv}>
            <div className={itemDiv}>
              <span className={label}>
                Submitted: {moment(item.original.date_created).format("lll")}
              </span>
              <span className={text}>&nbsp;</span>
            </div>
            <div className={itemDiv}>
              <span className={label} style={{maxWidth: "80%"}}>
                Number of prescription drugs presently taking :
              </span>
              <span className={text}>
                {item.value ? item.value.prescriptionDrugs : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label} style={{maxWidth: "80%"}}>
                Response to prescription medications :
              </span>
              <span className={text}>
                {item.value ? item.value.prescriptionScenarios : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label} style={{maxWidth: "80%"}}>
                Negative reactions to caffeine or caffeine containing products :
              </span>
              <span className={text}>
                {item.value ? item.value.caffeine : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label} style={{maxWidth: "80%"}}>
                Ill-feeling after consumption even small amounts of alcohol :
              </span>
              <span className={text}>
                {item.value ? item.value.illAlcohol : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label} style={{maxWidth: "80%"}}>
                Are you taking one or more of the following over-the-counter
                medications? Acetaminophen/OCP:
              </span>
              <span className={text}>
                {item.value ? item.value.overTheCounter : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label} style={{maxWidth: "80%"}}>
                Do you currently use or within the last 6 months had you
                regularly used tobacco products (cigars, cigarettes)? :
              </span>
              <span className={text}>
                {item.value ? item.value.tobacco : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label} style={{maxWidth: "80%"}}>
                Do you develop symptoms on exposure to fragrances, exhaust fumes
                or strong odors? :
              </span>
              <span className={text}>{item.value ? item.value.odors : ""}</span>
            </div>

            <div className={itemDiv}>
              <span className={label} style={{maxWidth: "80%"}}>
                Are you alcohol or chemical dependent? :
              </span>
              <span className={text}>
                {item.value ? item.value.dependent : ""}
              </span>
            </div>
          </div>
        );
      },
      style: { whiteSpace: "unset", cursor: "pointer" }
    }
  ];
  return (
    <ReactTable
      columns={columns}
      data={survey}
      showPageSizeOptions={false}
      defaultPageSize={1}
      sortable={false}
      filterable={false}
      resizable={false}
    />
  );
};

export { Detox };
