import React, { useContext } from "react";
import { UserContext } from "../../../context";
import ReactTable from "react-table";
import moment from "moment-timezone";
import { mainDiv, itemDiv, label, text } from "./style.js";

const Sleep = (props) => {
  const context = useContext(UserContext);

  // const survey = context.healthSummaryData;
  const survey = props.data;
  const columns = [
    {
      Header: "Sleep",
      accessor: "sleep",
      Cell: item => {
        return (
          <div className={mainDiv}>
            <div className={itemDiv}>
              <span className={label}>
                Submitted: {moment(item.original.date_created).format("lll")}
              </span>
              <span className={text}>&nbsp;</span>
            </div>
            <div className={itemDiv}>
              <span className={label}>Regular Sleeping Time :</span>
              <span className={text}>
                {item.value ? item.value.regularSleep : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Feel rested upon awakening? :</span>
              <span className={text}>
                {item.value ? item.value.restedFeeling : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Sleeping Aids :</span>
              <span className={text}>
                {item.value ? item.value.sleepingAids : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Trouble Falling Asleep? :</span>
              <span className={text}>
                {item.value ? item.value.troubleFallingAsleep : ""}
              </span>
            </div>

            <div className={itemDiv}>
              <span className={label}>Average Number of Sleep Hours :</span>
              <span className={text}>
                {item.value ? item.value.averageSleep : ""}
              </span>
            </div>
          </div>
        );
      },
      style: { whiteSpace: "unset", cursor: "pointer" }
    }
  ];
  return (
    <ReactTable
      columns={columns}
      data={survey}
      showPageSizeOptions={false}
      defaultPageSize={1}
      sortable={false}
      filterable={false}
      resizable={false}
    />
  );
};

export { Sleep };
