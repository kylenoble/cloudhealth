import React from "react";
import { css } from "glamor";
import Router from "next/router";
import Button from '../NewSkin/Components/Button'
import { font, fontWeight } from "../NewSkin/Styles";
import GridContainer from '../NewSkin/Wrappers/GridContainer'
import Colors from "../NewSkin/Colors";

const baseLink = "/healthprofile";

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};

    this._showTopMenu = this._showTopMenu.bind(this);
    this._updateCurrentView = this._updateCurrentView.bind(this);
  }

  render() {
    return <div className="wrap">{this._showTopMenu()}</div>;
  }

  _showTopMenu() {
    const titles = [
      { name: "health_survey", value: "HEALTH SURVEY" },
      { name: "summary_and_analysis", value: "SUMMARY AND ANALYSIS" },
      { name: "health_tracker", value: "HEALTH TRACKER" },
      { name: "other_health_info", value: "OTHER HEALTH INFO" },
      { name: "my_health_documents", value: "MY HEALTH DOCUMENTS" }
    ];

    let html = [];
    titles.forEach((currentItem, index) => {
      html.push(

        <div
          key={index}
          name={currentItem.name}
          onClick={() => this._updateCurrentView(currentItem.name)}
          className={
            this.props.activeView === currentItem.name ? Activeitem : item
          }
        >
          <Button
            classes={[font(0.75), fontWeight(700)]}
            styles={{ padding: '10px 20px' }}
            type={this.props.activeView === currentItem.name ? 'blue' : ''}>
            {currentItem.value}
          </Button>
        </div>
      );
    });

    return (
      <GridContainer contain={true} columnSize={'repeat(5, 1fr)'} gap={10}>
        {html}
      </GridContainer>
    )
  }

  _updateCurrentView(name) {
    //e.preventDefault();
    const url = `${baseLink}?tab=${name}`;
    Router.push(url);
    //this.props.activePressed(e.target.innerText);
  }
}

const container = css({
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "space-around",
  flexWrap: "wrap",
  margin: "0",
  marginTop: "10px"
});

const dashItem = css({
  fontFamily: '"Roboto-Black",sans-serif',
  fontSize: "14px",
  color: "black",
  letterSpacing: 0,
  lineHeight: "14px",
  padding: "10px 10px",
  margin: "-1px 15px",
  cursor: "pointer",
  textAlign: "center"
});

const dashItemActive = css({
  color: "#d383b6",
  cursor: "none"
});

const item = css({
  textAlign: "center",
  // margin: "3px 0",
  // padding: "3px 15px",
  textTransform: "uppercase",
  cursor: "pointer",
  color: Colors.blueDarkAccent,
  transition: '300ms ease',
  // fontWeight: "bold",
  // letterSpacing: "1.5px",
  // border: "1px solid transparent",
  // borderRight: "1.5px dotted gray",
  // ":last-child": { border: "none" },
  ":hover": {
    color: Colors.skyblue
  }
});

const Activeitem = css({
  textAlign: "center",
  // margin: "3px 0",
  // padding: "3px 15px",
  textTransform: "uppercase",
  // fontWeight: "bold",
  // letterSpacing: "1.5px",
  // borderRight: "1.5px dotted gray",
  // ":last-child": { border: "none" }
});
