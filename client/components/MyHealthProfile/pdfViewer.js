import React, { useState, useEffect, useRef } from 'react';
import { usePdf } from 'react-pdf-js';
import { css } from 'glamor';

const PdfViewer = ({ file = '' }) => {
  const canvasEl = useRef(null);

  const [loading, numPages] = usePdf({
    file: file,
    page: 1,
    canvasEl
  });

  return (
    <React.Fragment>
      {loading && <span>Loading...</span>}
      <canvas className={pdfwrapper} ref={canvasEl} />
    </React.Fragment>
  );
}

const pdfwrapper = css({
  maxWidth: '100%',
  height: 'auto !important'
})

export default PdfViewer;