import React from "react";
import { css } from "glamor";
import Router, { withRouter } from "next/router";

// import ProfileMenu from "./profileMenu";
import HealthSurvey from "./healthSurvey";
//import CoreImbalances from '../../components/SummaryAndAnalysis/coreImbalances'
import SummaryAndAnalysis from "./summaryAndAnalysis";
import OtherHealthInfo from "./otherHealthInfo";
import MyhealthDocuments from "./myHealthDocuments";
import Restricted from "../../components/restricted";
import AnswersService from "../../utils/answerService.js";
import UserService from "../../utils/userService";
import DefenseRapair from "../../components/DefenseRapair";
import HealthTracker from "../../components/HealthTracker";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import PromptCompleteModal from "../Modals/surveyComplete.js";
import PromptSuccessModal from "../Modals/surveySuccess.js";
import Card from "../NewSkin/Wrappers/Card";
import { on_mobile } from "../NewSkin/Styles";
import OptimumHealth from "../NewSkin/Components/OptimumHealth";
import MenuButtons from "../NewSkin/Components/MenuButtons";

const AnswerService = new AnswersService();
const userService = new UserService();

class Index extends React.Component {
  //static contextType = UserContext;

  constructor(props) {
    super(props);
    this._isMounted = false;
    this.user = this.props.user; // will remove someday, implementing not props dependent component
    this.state = {
      surveyCompleteModalOpen: false,
      surveySuccessModalOpen: false,
      activeView: this.props.router.query.tab || "health_survey",
      userid: this.props.user.id,
      scale: {
        radius: 150,
        width: 700,
        height: 500,
        fontSize: 12
      }
    };

    this._updateView = this._updateView.bind(this);
    this._updateCurrentView = this._updateCurrentView.bind(this);
    this._returnProfileSwitcher = this._returnProfileSwitcher.bind(this);
    //this._getCoreImbalancesData = this._getCoreImbalancesData.bind(this)
    this.defenseRepair = this.defenseRepair.bind(this);
    this._openModal = this._openModal.bind(this);
  }

  componentWillMount() {
    this._isMounted = true;
    this._getProfile();
  }

  componentDidMount() {
    this._isMounted = true;
    this.defenseRepair();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  componentWillReceiveProps(nextProps) {
    this._getProfile();
    this.setState({
      activeView: nextProps.router.query.tab || "health_survey"
    });
  }

  // GET UPDATED profile info.
  // Solution for "Not Props dependent" Component
  _getProfile() {
    userService
      .getLatest()
      .then(res => {
        this.user = res;
        // this.setState({
        //   profile: res
        // });
      })
      .catch(e => {
        console.log(e);
      });
  }

  _closeModal(modalName) {
    if (this._isMounted) {
      this.setState({
        [modalName]: false
      });
    }
  }

  _openModal(modalName) {
    if (this._isMounted) {
      this.setState({
        [modalName]: true
      });
    }
  }

  render() {
    return (
      <div className="wrap">
        <OptimumHealth />
        <GridContainer
          dataID="Health-Profile-Wrapper"
          contain
          columnSize={"1fr"}
          gap={20}
          styles={{
            margin: "0 auto",
            padding: "0 20px 50px",
            minHeight: "69vh",
            alignContent: "start"
          }}
        >
          {this._returnProfileSwitcher()}
          <Card
            classes={[on_mobile([], { width: "95%", margin: "0 auto 20px" })]}
          >
            {this._renderView()}
          </Card>
        </GridContainer>
        <PromptSuccessModal
          modalIsOpen={this.state.surveySuccessModalOpen}
          closeModal={() => this._closeModal("surveySuccessModalOpen")}
        />
        <PromptCompleteModal
          modalIsOpen={this.state.surveyCompleteModalOpen}
          closeModal={() => this._closeModal("surveyCompleteModalOpen")}
        />
      </div>
    );
  }

  _returnProfileSwitcher() {
    if (this.user.type !== "Doctor") {
      const baseLink = "/healthprofile";
      const titles = [
        { name: "health_survey", value: "HEALTH SURVEY" },
        { name: "summary_and_analysis", value: "SUMMARY AND ANALYSIS" },
        { name: "health_tracker", value: "HEALTH TRACKER" },
        { name: "other_health_info", value: "OTHER HEALTH INFO" },
        { name: "my_health_documents", value: "MY HEALTH DOCUMENTS" }
      ];
      return (
        <MenuButtons
          activeView={this.state.activeView}
          user={this.user}
          titles={titles}
          baseLink={baseLink}
          contain={true}
        />
      );
    }
  }

  _updateView(lnk) {
    Router.push(`/dashboard?menu=healthprofile&tab=${lnk}`);
  }

  _updateCurrentView(tab) {
    this.setState({
      activeView: tab
    });
  }

  _renderView() {
    if (this.state.activeView === "health_survey") {
      return (
        <HealthSurvey
          openModal={this._openModal}
          closeModal={this._closeModal}
          updateView={this._updateView}
          user={this.user}
          setView={this.props.setView}
          setNextView={this.props.setNextView}
          setPreviousView={this.props.setPreviousView}
        />
      );
    } else if (this.state.activeView === "summary_and_analysis") {
      return (
        // <Restricted message="Sorry this part is still under maintenance"/>
        <SummaryAndAnalysis user={this.user} />
      );
    } else if (this.state.activeView === "PRESCRIPTION") {
      return (
        <Restricted message="Sorry this part is still under maintenance" />
        //<Prescriptions/>
      );
    } else if (this.state.activeView === "other_health_info") {
      return (
        //<Restricted message="Sorry this part is still under maintenance" />
        <OtherHealthInfo user={this.user} />
      );
    } else if (this.state.activeView === "my_health_documents") {
      return (
        //<Restricted message="Sorry this part is still under maintenance" />
        <MyhealthDocuments user={this.user} />
      );
    } else if (this.state.activeView === "defenseRepair") {
      return <DefenseRapair getData={this.defenseRepair()} />;
    } else if (this.state.activeView === "health_tracker") {
      return (
        <HealthTracker
          userType={this.user.type}
          userid={this.state.userid}
          telemedicine_enabled={this.user.telemedicine_enabled}
        />
      );
    }
  }

  defenseRepair() {
    const qry = `select DISTINCT ON (a.question_id) * from new_answers a left join new_questions q on q.id = a.question_id where a.user_id = ${
      this.state.userid
    } and q.flag_assimilation = true or q.flag_defense_and_repair = true or q.flag_energy = true or q.flag_biotransformation_and_elimination = true
or q.flag_communication = true or q.flag_transport = true or q.flag_structural_integrity = true order by a.question_id, q.group`;
    return AnswerService.getQuery(qry)
      .then(res => {
        Promise.resolve(res);
      })
      .catch(err => {
        console.log(err);
      });
  }
}

export default withRouter(Index);
