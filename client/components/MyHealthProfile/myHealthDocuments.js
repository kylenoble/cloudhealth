import React, { Component } from "react";
import { css } from "glamor";
import Uploader from "./uploader";
import UserService from "../../utils/userService";
import moment from "moment-timezone";
import { setTimeout } from "timers";
import Restricted from "../../components/restricted";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import { labelClass, scrollClass } from "../NewSkin/Styles";
import Colors from "../NewSkin/Colors";
import NoData from "../NewSkin/Components/NoData.js";
import ReactTooltip from "react-tooltip";
import {tooltipWrapper} from '../Commons/tooltip';
import PdfViewer from "./pdfViewer";

const User = new UserService();

export default class extends Component {
  constructor(props) {
    super(props);
    this.user = this.props.user;
    this.userType = this.props.userType || "Patient";
    this.state = {
      files: [],
      message: ""
    };

    //this._getFiles = this._getFiles.bind(this);
  }

  componentDidMount() {
    this._getFiles();
  }

  _getFiles = () => {
    User.getHealthDocs(this.user.id)
      .then(res => {
        this.setState({ files: res });
      })
      .catch(e => console.log(e));
  };

  _deleteFile = (fileId, fileName) => {
    const toDelete = confirm("Are you sure you want to delete this file?");

    if (!toDelete) {
      return;
    }
    const params = {
      user_id: this.user.id,
      id: fileId,
      file_name: fileName
    };

    User.deleteFile(params).then(r => {
      this.setState({
        message: "File Deleted."
      });
      this._getFiles();
      setTimeout(() => {
        this.setState({ message: "" });
      }, 3000);
    });
  };

  _header() {
    return (
      <div
        className={contentStyle}
        style={{
          background: "#ccdee2"
        }}
      >
        <div className={columnStyle}>
          <div className={header}>Filename</div>
        </div>

        <div className={columnStyle}>
          <div className={header}>Date Upload</div>
        </div>
        <div className={columnStyle}>
          <div className={header}>Description</div>
        </div>
        {this.userType === "Patient" ? (
          <div className={columnStyle}>
            <div className={header}>&nbsp;</div>
          </div>
        ) : null}
      </div>
    );
  }

  _fileList() {
    const { files } = this.state;

    if (files.length > 0) {
      return files.map((file, i) => {
        const link = `/static/healthdocs/${file.filename}`;
        const file_name = file.filename.split("~")[1];
        const fileNoExt = file_name.split(".")[0];
        const fileNameExtName = file_name.split(".")[1];
        // fix file text format
        const date = moment(file.uploaded_date).format("ll");
        const lowerd = fileNoExt.toLowerCase();
        date.toLowerCase();

        return (
          <GridContainer classes={[pdf]} key={i}>
            {this.state[lowerd] && (
              <div
                style={{
                  width: 202,
                  height: 189,
                  background: "#eee",
                  borderRadius: 5
                }}
              />
            )}

            <article
              style={{ display: this.state[lowerd] ? "none" : "block" }}
            >
              {this.userType === "Patient" && (
                <button
                  className={closeBtn}
                  onClick={() => this._deleteFile(file.id, file.filename)}
                >
                  x
                </button>
              )}
              <a target="_blank" href={link} style={{ textDecoration: "none" }}>
                <article
                  style={{
                    height: 130,
                    overflow: "hidden",
                    borderBottom: `1px solid ${Colors.pink}`,
                    borderRadius: "5px 5px 0 0"
                  }}
                >
                  {fileNameExtName === "pdf" ? (
                    <PdfViewer file={link} />
                  ) : (
                    <img src={link} className={thumbnail} />
                  )}
                </article>

                <GridContainer>
                  <article className={pdfDetails}>
                    <label>
                      <strong>
                        <small>{lowerd}</small>
                      </strong>
                    </label>
                    <small>{date} {file.description !== "" && <span> | 
                      <span className={descriptionTooltip} data-tip={
                        tooltipWrapper({description: file.description, title: lowerd})
                      }> Show description</span></span>}
                    </small>
                  </article>
                </GridContainer>
              </a>
            </article>
            <ReactTooltip
              type="light"
              html
            />
          </GridContainer>
        );
      });
    }
  }

  render() {
    if (this.user.telemedicine_enabled !== "Approved") {
      let restrict_msg = `This feature is not included in your current subscription. Please contact the officer in-charge of your account if you wish to subscribe`;
      return <Restricted message={restrict_msg} />;
    }

    const isPatient = this.userType === "Patient";
    return (
      <section>
        <GridContainer columnSize={isPatient ? "1fr 1.5fr" : "1fr"} gap={20}>
          {this.userType === "Patient" ? (
            <Uploader
              user={this.user}
              files={this.state.files}
              update={() => this._getFiles()}
            />
          ) : null}

          <section className={fileMainDiv}>
            <GridContainer
              columnSize="1fr 1fr"
              styles={{
                borderBottom: `1px solid ${Colors.pink}`,
                alignContent: 'start'
              }}
              fullWidth
            >
              <label className={labelClass}>Uploaded Documents</label>
              {this.state.message !== "" ? (
                <div style={{ color: Colors.movementColor }}>
                  {this.state.message}
                </div>
              ) : null}
            </GridContainer>

            <article
              style={{
                maxHeight: 410,
                overflow: "hidden auto",
                padding: "15px 15px 15px 0",
                width: "100%"
              }}
              className={scrollClass}
            >
              <GridContainer
                columnSize={this.state.files.length == 0 ? "1fr" : isPatient ? "repeat(3, 1fr)" : "repeat(5, 1fr)"}
                gap={10}
                styles={{minHeight: 200}}
              >
                {this.state.files.length > 0 ? this._fileList() : 
                <NoData text="No uploaded document" />
                }
              </GridContainer>
            </article>
          </section>
        </GridContainer>
      </section>
    );
  }
}

const closeBtn = css({
  width: 30,
  height: 30,
  position: "absolute",
  top: 0,
  right: 0,
  background: "white",
  borderRadius: "100%",
  transform: "translate(50%, -50%)",
  zIndex: 1,
  borderStyle: "none",
  border: `1px solid #ccc`,
  opacity: 0,
  transition: "250ms ease",
  display: "grid",
  placeContent: "center",
  cursor: "pointer",
  boxShadow: "0 3px 3px rgba(0,0,0,.1)",
  color: "#ccc",
  ":focus": {
    outline: "none"
  },
  ":hover": {
    background: Colors.skyblue,
    color: "#fff",
    borderColor: Colors.skyblue,
    boxShadow: `0 5px 15px ${Colors.skyblue}90`
  }
});

const pdf = css({
  border: `1px solid #eee`,
  borderRadius: 5,
  alignContent: "start",
  transition: "250ms ease",
  position: "relative",
  ":hover": {
    borderColor: Colors.skyblue,
    "& button": {
      opacity: 1
    }
  }
});

const pdfDetails = css({
  fontSize: 12,
  textAlign: "right !important",
  padding: 10,
  boxSizing: "border-box",
  "& > label": {
    color: Colors.blueDarkAccent,
    fontSize: 14,
    wordBreak: "break-word",
    lineHeight: "14px",
    "& small": {
      display: "block",
      lineHeight: "11px"
    }
  },
  "& > small": {
    display: "block",
    marginTop: -5,
    marginBottom: -5
  }
});

const fileMainDiv = css({
  flex: 1,
  display: "flex",
  flexWrap: "wrap",
  padding: 5,
  justifyContent: "space-between"
  //border: "1px solid blue"
});

const contentStyle = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "space-between",
  alignItems: "flex-start",
  padding: "3px 3px",
  ":nth-child(even)": {
    background: "#e8edee"
  }
});

const columnStyle = css({
  display: "flex",
  flex: "1 303px 600px",
  //border: "1px solid blue",
  alingSelf: "center",
  alignItems: "center",
  width: "23%",
  //margin: 10,
  padding: "3px 10px",
  textTransform: "capitalize"
});

const header = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "flex-start",
  alignItems: "center",
  fontWeight: "bold",
  margin: "5px 0",
  textTransform: "uppercase"
});

const thumbnail = css({
  border: "1px solid #ddd",
  borderRadius: "4px",
  padding: "5px",
  width: "100%"
});

const descriptionTooltip = css({
  cursor: 'pointer', 
  color: Colors.skyblue, 
  fontWeight: 500
})