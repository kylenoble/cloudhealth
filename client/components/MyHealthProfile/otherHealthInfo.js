import React, { Component } from "react";
import { css } from "glamor";
import moment from "moment-timezone";
import UserService from "../../utils/userService";
import Restricted from "../../components/restricted";
import GridContainer from '../NewSkin/Wrappers/GridContainer';
import { labelClass, inputClass, inputClassError } from "../NewSkin/Styles";
import Button from '../NewSkin/Components/Button';
import Colors from "../NewSkin/Colors";
import TabWrapper from '../NewSkin/Wrappers/TabWrapper';
import TabItem from '../NewSkin/Wrappers/TabItem';
import NoData from "../NewSkin/Components/NoData.js";

const User = new UserService();

export default class extends Component {
  constructor(props) {
    super(props);
    this.user = this.props.user;
    this.userType = this.props.userType || "Patient";

    this.state = {
      message: {
        doctors: "",
        activities: ""
      },
      formOn: { doctors: false, activities: false },
      updatingForm: false,
      idToEdit: {
        doctors: null,
        activities: null
      },
      info: [],
      data: {
        doctors: {
          title: "My Other Doctors",
          details: []
        },
        activities: {
          title: "My Other Health Activities",
          details: []
        }
      },
      inputs: {
        doctor_name: {
          id: "",
          name: "doctor_name",
          question: "Enter Name",
          type: "text",
          validation: "name",
          disabled: true,
          options: [],
          error: {
            active: false,
            message: "Please enter a valid full name"
          },
          value: ""
        },
        specialization: {
          id: "",
          name: "specialization",
          question: "Enter Specialization",
          type: "text",
          validation: "name",
          disabled: true,
          options: [],
          error: {
            active: false,
            message: "Please Specialization"
          },
          value: ""
        },
        hospital_clinic: {
          id: "",
          name: "hospital_clinic",
          question: "Enter hospital / Clinic",
          type: "text",
          validation: "name",
          disabled: true,
          options: [],
          error: {
            active: false,
            message: "Please Enter hospital / Clinic"
          },
          value: ""
        },
        activity_name: {
          id: "",
          name: "activity_name",
          question: "Enter Activity",
          type: "text",
          validation: "name",
          disabled: true,
          options: [],
          error: {
            active: false,
            message: "Activity"
          },
          value: ""
        },
        place: {
          id: "",
          name: "place",
          question: "Enter Place",
          type: "text",
          validation: "name",
          disabled: true,
          options: [],
          error: {
            active: false,
            message: "Please Enter Activity"
          },
          value: ""
        },
        frequency: {
          id: "",
          name: "frequency",
          question: "Enter Frequency",
          type: "select",
          validation: "name",
          disabled: true,
          options: [],
          error: {
            active: false,
            message: "Please Enter Frequency"
          },
          value: "",
        }
      },
      activeTab: 'doctors'
    };
  }

  _header(formfor) {
    let titles = [];
    if (formfor === "doctors") {
      titles = ["Full Name", "Specialization", "Hospital / Clinic"];
    } else {
      titles = ["Activities", "Place", "Frequency"];
    }

    const style = {
      color: '#989898',
      fontWeight: 600,
      textAlign: 'left'
    }

    return (
      <GridContainer columnSize="1fr 2fr 1.5fr .5fr" gap={10} fullWidth styles={{ borderBottom: `3px solid ${Colors.pinkDarkAccent}`, marginTop: 20 }}>
        <span style={style}>{titles[0]}</span>
        <span style={style}>{titles[1]}</span>
        <span style={style}>{titles[2]}</span>
        {this.userType === "Patient" ? <span style={style}>&nbsp;</span> : null}
      </GridContainer>
    );
  }

  componentDidMount() {
    this._getData();

    this.setState({ activeTab: 'doctors' })
  }

  _getData() {
    User.getOtherInfo(this.user.id)
      .then(res => {
        if (res) {
          this._prepareData(res);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _prepareData(res) {
    const { data } = this.state;
    if (res.length > 0) {
      data.doctors.details = res.filter(r => r.tag === 1);
      data.activities.details = res.filter(r => r.tag === 2);
    }

    this.setState({ data });
  }

  _displayMessage(formfor, string) {
    const { message } = this.state;
    message[formfor] = string;
    this.setState({ message });

    setTimeout(() => {
      message[formfor] = "";
      this.setState({ message });
    }, 3000);
  }

  _update(string, id) {
    const { data, inputs } = this.state;

    if (string === "doctors") {
      if (
        inputs.doctor_name.value !== "" &&
        inputs.specialization.value !== "" &&
        inputs.hospital_clinic.value !== ""
      ) {
        this.setState({ updatingForm: true });
        //save data

        const dataToSave = {
          id: id,
          doctor_name: inputs.doctor_name.value,
          specialization: inputs.specialization.value,
          hospital_clinic: inputs.hospital_clinic.value,
          activity_name: "",
          place: "",
          frequency: "",
          tag: 1,
          modified_date: moment()
        };

        this._updateDB(dataToSave, string);

        inputs.doctor_name.value = "";
        inputs.specialization.value = "";
        inputs.hospital_clinic.value = "";

        this.setState({ data, inputs }, () => {
          this._onClickAdd(string);
          this.setState({ updatingForm: false });
        });
      } else {
        // display error
        if (inputs.doctor_name.value === "") {
          inputs.doctor_name.error.active = true;
          inputs.doctor_name.question = "*Name is required";
          inputs.doctor_name.error.message = "Name is required";
        }

        if (inputs.specialization.value === "") {
          inputs.specialization.error.active = true;
          inputs.specialization.question = "*Specialization is required";
          inputs.specialization.error.message =
            "Specialization is required";
        }

        if (inputs.hospital_clinic.value === "") {
          inputs.hospital_clinic.error.active = true;
          inputs.hospital_clinic.question = "*Hospital/Clinic is required";
          inputs.hospital_clinic.error.message =
            "Hospital/Clinic is required";
        }

        this.setState({ inputs });
      }
    } else if (string === "activities") {
      if (
        inputs.activity_name.value !== "" &&
        inputs.place.value !== "" &&
        inputs.frequency.value !== ""
      ) {
        this.setState({ updatingForm: true });
        //save data
        const dataToSave = {
          id: id,
          doctor_name: "",
          specialization: "",
          hospital_clinic: "",
          activity_name: inputs.activity_name.value,
          place: inputs.place.value,
          frequency: inputs.frequency.value,
          tag: 2,
          modified_date: moment()
        };

        this._updateDB(dataToSave, string);

        inputs.activity_name.value = "";
        inputs.place.value = "";
        inputs.frequency.value = "";

        this.setState({ data, inputs }, () => {
          this._onClickAdd(string);
          this.setState({ updatingForm: false });
        });
      } else {
        // display error
        if (inputs.activity_name.value === "") {
          inputs.activity_name.error.active = true;
          inputs.activity_name.question = "*Activity Name is required";
          inputs.activity_name.error.message = "Activity is required";
        }

        if (inputs.place.value === "") {
          inputs.place.error.active = true;
          inputs.place.question = "*Place is required";
          inputs.place.error.message = "Place is required";
        }

        if (inputs.frequency.value === "") {
          inputs.frequency.error.active = true;
          inputs.frequency.question = "*Frequency is required";
          inputs.frequency.error.message = "Frequency is required";
        }

        this.setState({ inputs });
      }
    }
  }

  _closeForm() {
    const { formOn } = this.state;
    formOn.activities = false;
    formOn.doctors = false;
    this.setState({ formOn });
  }

  _saveToDB(data, formfor) {
    User.saveOtherInfo(data)
      .then(() => {
        this._closeForm();
        // this._displayMessage(formfor, "Saving Successful.");
        this._getData();
      })
      .catch(e => console.log(e));
  }

  _updateDB(data, formfor) {
    User.updateOtherInfo(data)
      .then(() => {
        this._closeForm();
        // this._displayMessage(formfor, "Update Successful.");
        this._getData();
      })
      .catch(e => {
        console.log(e);
      });
  }

  _save(string) {
    const { data, inputs } = this.state;

    if (string === "doctors") {
      if (
        inputs.doctor_name.value !== "" &&
        inputs.specialization.value !== "" &&
        inputs.hospital_clinic.value !== ""
      ) {
        this.setState({ updatingForm: true });
        //save data
        const dataToSave = {
          user_id: this.user.id,
          doctor_name: inputs.doctor_name.value,
          specialization: inputs.specialization.value,
          hospital_clinic: inputs.hospital_clinic.value,
          activity_name: "",
          place: "",
          frequency: "",
          tag: 1,
          created_date: moment()
        };

        this._saveToDB(dataToSave, string);

        inputs.doctor_name.value = "";
        inputs.specialization.value = "";
        inputs.hospital_clinic.value = "";

        this.setState({ data, inputs }, () => {
          this._onClickAdd(string);
          this.setState({ updatingForm: false });
        });
      } else {
        // display error
        if (inputs.doctor_name.value === "") {
          inputs.doctor_name.error.active = true;
          inputs.doctor_name.question = "* Full Name is Required!";
          inputs.doctor_name.error.message = "Full name is Required!";
        }

        if (inputs.specialization.value === "") {
          inputs.specialization.error.active = true;
          inputs.specialization.question = "* Specialazation is Required!";
          inputs.specialization.error.message =
            "Specialazation name is Required!";
        }

        if (inputs.hospital_clinic.value === "") {
          inputs.hospital_clinic.error.active = true;
          inputs.hospital_clinic.question = "* hospital / Clinice is Required!";
          inputs.hospital_clinic.error.message =
            "Hospital / Clinice is Required!";
        }

        this.setState({ inputs });
      }
    } else if (string === "activities") {
      if (
        inputs.activity_name.value !== "" &&
        inputs.place.value !== "" &&
        inputs.frequency.value !== ""
      ) {
        this.setState({ updatingForm: true });
        //save data
        // const dataToSave = {
        //   activity_name: inputs.activity_name.value,
        //   place: inputs.place.value,
        //   frequency: inputs.frequency.value
        // };

        const dataToSave = {
          user_id: this.user.id,
          doctor_name: "",
          specialization: "",
          hospital_clinic: "",
          activity_name: inputs.activity_name.value,
          place: inputs.place.value,
          frequency: inputs.frequency.value,
          tag: 2,
          created_date: moment()
        };

        this._saveToDB(dataToSave, string);

        inputs.activity_name.value = "";
        inputs.place.value = "";
        inputs.frequency.value = "";

        this.setState({ data, inputs }, () => {
          this._onClickAdd(string);
          this.setState({ updatingForm: false });
        });
      } else {
        // display error
        if (inputs.activity_name.value === "") {
          inputs.activity_name.error.active = true;
          inputs.activity_name.question = "* activity is Required!";
          inputs.activity_name.error.message = "activity is Required!";
        }

        if (inputs.place.value === "") {
          inputs.place.error.active = true;
          inputs.place.question = "* place is Required!";
          inputs.place.error.message = "place is Required!";
        }

        if (inputs.frequency.value === "") {
          inputs.frequency.error.active = true;
          inputs.frequency.question = "* frequency is Required!";
          inputs.frequency.error.message = "frequency is Required!";
        }

        this.setState({ inputs });
      }
    }
  }

  _handleInputChange(e) {
    e.preventDefault();
    console.log(e.target, this.state)
    const { inputs } = this.state;
    inputs[e.target.name].value = e.target.value;
    this.setState(inputs);
  }

  _Input(input) {
    const { type, name, question, value } = input;
    return (
      <input
        className={input.error.active ? inputClassError : inputClass}
        type={type}
        name={name}
        placeholder={question}
        value={value}
        onChange={this._handleInputChange.bind(this)}
      />
    );
  }

  _Select(input) {
    const { type, name, question, value } = input;
    return (
      <select
        className={input.error.active ? inputClassError : inputClass}
        type={type}
        name={name}
        placeholder={question}
        value={value}
        onChange={this._handleInputChange.bind(this)}
      >
        <option value="">Please Select</option>
        <option value="daily">Daily</option>
        <option value="weekly">Weekly</option>
        <option value="monthly">Monthly</option>
      </select>
    );
  }

  _editForm(formfor, data, id) {
    //let { data } = this.state;

    const {
      doctor_name,
      specialization,
      hospital_clinic,
      activity_name,
      place,
      frequency
    } = this.state.inputs;

    let fields = [];

    if (formfor === "doctors") {
      // doctor_name.value = data.doctor_name;
      // specialization.value = data.specialization;
      // hospital_clinic.value = data.hospital_clinic;

      fields = [doctor_name, specialization, hospital_clinic];
    } else if (formfor === "activities") {
      // activity_name.value = data.activity_name;
      // place.value = data.place;
      // frequency.value = data.frequency;
      fields = [activity_name, place, frequency];
    }

    return (
      <GridContainer columnSize="1fr 2fr 1.5fr .5fr" gap={10} styles={{ padding: '10px 0' }}>
        {this.state.updatingForm ? null : this._displayInputFields(fields)}

        <Button classes={[btn]}>
          <button type="blue" onClick={() => this._update(formfor, data.id)}>Save</button>
        </Button>
      </GridContainer>
    );
  }

  _form(formfor) {
    const {
      doctor_name,
      specialization,
      hospital_clinic,
      activity_name,
      place,
      frequency
    } = this.state.inputs;

    let fields = [];

    if (formfor === "doctors") {
      // doctor_name.value = "";
      // specialization.value = "";
      // hospital_clinic.value = "";
      fields = [doctor_name, specialization, hospital_clinic];
    } else if (formfor === "activities") {
      // activity_name.value = "";
      // place.value = "";
      // frequency.value = "";
      fields = [activity_name, place, frequency];
    }

    return (
      <GridContainer columnSize="1fr 2fr 1.5fr .5fr" gap={10} styles={{ borderBottom: `1px solid ${Colors.pink}`, padding: '5px 0 10px' }}>
        {this.state.updatingForm ? null : this._displayInputFields(fields)}

        <Button classes={[btn]}>
          <button style={{ padding: 10, fontWeight: 600 }} onClick={() => this._save(formfor)}>+ Add</button>
        </Button>

      </GridContainer>
    );
  }

  _displayInputFields(fields) {
    const inputs = fields.map((field, i) => (
      <div key={i}>
        {field.type === "select" ? this._Select(field) : this._Input(field)}
      </div>
    ));

    return inputs;
  }

  _details(formfor) {
    const { data } = this.state;

    let indexes = [];
    if (formfor === "doctors") {
      indexes = ["doctor_name", "specialization", "hospital_clinic"];
    } else {
      indexes = ["activity_name", "place", "frequency"];
    }

    return data[formfor].details.map((item, i) => (
      <div key={i} className={contentStyle}>
        {this.state.idToEdit[formfor] === i ? this._editForm(formfor, item, i) : null}

        {this.state.idToEdit[formfor] !== i ?
          <GridContainer columnSize="1fr 2fr 1fr 1fr" gap={10} fullWidth classes={[detailsWrapper]}>
            <div style={{ color: Colors.blueDarkAccent, fontWeight: 500, textAlign: 'left' }}>{item[indexes[0]]}</div>

            <div style={{ textAlign: 'left', color: '#9b9b9b' }}>
              <span style={{ color: Colors.blueDarkAccent, fontWeight: 500 }}>{item[indexes[1]]}</span>
            </div>

            <div style={{ textAlign: 'left', color: '#9b9b9b' }}>
              <span style={{ color: Colors.blueDarkAccent }}>{item[indexes[2]]}</span>
            </div>

            {this.userType === "Patient" ? (
              <GridContainer dataID="action-btn" columnSize="1fr 1fr">
                <span
                  className={btnStyle2}
                  onClick={() => this._onEditClick(formfor, i)}
                >
                  <img className={iconStyle} src={"/static/images/edit.svg"} /> Edit
                  </span>

                <span
                  className={btnStyle2}
                  onClick={() => this._onDeleteClick(formfor, i)}
                >
                  <img
                    className={iconStyle}
                    alt="Remove"
                    src={"/static/images/trash.svg"}
                  />
                  Delete
                  </span>
              </GridContainer>
            ) : null}
          </GridContainer> : null}
      </div>
    ));
  }

  _onDeleteClick(formfor, i) {
    let sureDelete = confirm("Are you sure");
    if (!sureDelete) {
      return;
    }
    const { idToEdit, data, formOn } = this.state;

    const info = data[formfor].details[i];
    let datatoDelete = { id: info.id };
    User.deleteOtherInfo(datatoDelete)
      .then(() => {
        this._displayMessage(formfor, "Delete Successful.");
        this._getData();
      })
      .catch(e => {
        console.log(e);
      });
  }

  _onEditClick(formfor, i) {
    const { idToEdit, data, formOn } = this.state;
    formOn.doctors = false;
    formOn.activities = false;
    idToEdit[formfor] = i;
    const info = data[formfor].details[i];

    const {
      doctor_name,
      specialization,
      hospital_clinic,
      activity_name,
      place,
      frequency
    } = this.state.inputs;

    if (formfor === "doctors") {
      doctor_name.value = info.doctor_name;
      specialization.value = info.specialization;
      hospital_clinic.value = info.hospital_clinic;
    } else if (formfor === "activities") {
      activity_name.value = info.activity_name;
      place.value = info.place;
      frequency.value = info.frequency;
    }

    this.setState({ idToEdit, formOn });
  }

  _onClickAdd(formfor) {
    const { formOn, idToEdit } = this.state;
    idToEdit.doctors = null;
    idToEdit.activities = null;

    const {
      doctor_name,
      specialization,
      hospital_clinic,
      activity_name,
      place,
      frequency
    } = this.state.inputs;

    if (formfor === "doctors") {
      doctor_name.value = "";
      specialization.value = "";
      hospital_clinic.value = "";
    } else if (formfor === "activities") {
      activity_name.value = "";
      place.value = "";
      frequency.value = "";
    }

    formOn[formfor] = !formOn[formfor];
    this.setState({ formOn, idToEdit });
  }

  _content(formfor) {
    const { formOn, data, message } = this.state;
    const isPatient = this.userType === "Patient";

    return (
      <div className={divWrapperStyle}>
        {isPatient &&
          <GridContainer columnSize={'1fr'} gap={20} styles={{margin: '0 0 0 auto'}}>
              <Button type="pink" styles={{ minWidth: 'fit-content', maxWidth: 165 }}>
                <button onClick={() => this._onClickAdd(formfor)}>+ Add {formfor == 'doctors' ? `Doctor` : `Activity`}</button>
              </Button>
              {message[formfor] != "" &&
              <div className={goodMessage}>
                {message[formfor]}
              </div>
              }
          </GridContainer>
        }

        {data[formfor].details.length > 0 ? this._header(formfor) : null}

        {formOn[formfor] ? this._form(formfor) : null}

        {data[formfor].details.length > 0 ? (
          this._details(formfor)
        ) : (
          <NoData text="No Record Yet." />
          )}
      </div>
    );
  }

  setTab = (tab) => {
    this.setState({
      activeTab: tab
    }, () => console.log(this.state.activeTab))
  }

  render() {
    if (this.user.telemedicine_enabled !== 'Approved') {
      let restrict_msg = `This feature is not included in your current subscription. Please contact the officer in-charge of your account if you wish to subscribe`;
      return <Restricted message={restrict_msg} />;
    }
    return (
      <div className={mainDiv}>
        <TabWrapper tabCount={2}>
          <TabItem onClickFn={() => this.setTab('doctors')} isActive={this.state.activeTab === 'doctors'}>My Other Doctors</TabItem>
          <TabItem onClickFn={() => this.setTab('activities')} isActive={this.state.activeTab === 'activities'}>My Other Health Activities</TabItem>
        </TabWrapper>

        {
          this.state.activeTab === 'doctors' ? this._content("doctors") : this._content("activities")
        }
      </div>
    );
  }
}

const btn = css({
  minWidth: 100,
  width: 100,
  margin: '0 auto',
});

const detailsWrapper = css({
  '& [data-id="action-btn"]': {
    opacity: 0,
    transition: '250ms ease'
  },
  '&:hover': {
    '& > [data-id="action-btn"]': {
      opacity: 1
    },
  }
})

let goodMessage = css({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  width: "100%",
  color: "#82cfeb",
  //border: "1px dotted orange",
  padding: "3",
  borderRadius: 5,
  textTransform: "capitalize"
});

const mainDiv = css({
  display: "flex",
  flexWrap: "wrap",
  margin: 0,
  justifyContent: "center",
  alignItems: "center",
  flex: 1
});

const divWrapperStyle = css({
  display: "flex",
  flexWrap: "wrap",
  //flex: 1,
  margin: "20px 0",
  width: "90%",
  justifyContent: "space-between",
  alignItems: "flex-start"
  //border: "1px solid red"
});

const btnStyle2 = css({
  display: "flex",
  width: "0 auto",
  fontSize: "small",
  alignItems: "center",
  padding: 2,
  margin: "0 5px",
  textTransform: "capitalize",
  fontStyle: "italic",
  cursor: "pointer"
});

const contentStyle = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "space-between",
  alignItems: "flex-start",
  padding: "8px 3px 3px",
  borderBottom: `1px solid ${Colors.pink}`,
  transition: '250ms ease',
  background: '#fff',
  '&:hover': {
    background: '#eee8'
  }
});

let iconStyle = css({
  display: "flex",
  width: 15,
  height: 15,
  // marginRight: "auto",
  // marginLeft: "auto",
  margin: "0 5px 0 0",
  alingSelf: "center"
});
