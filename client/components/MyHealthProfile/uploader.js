import React from "react";
import Dropzone from "react-dropzone";
import { css } from "glamor";
import moment from "moment-timezone";
import request from "axios";
import currentDomain from "../../utils/domain.js";
import {
  inputClass,
  combined,
  scrollClass,
  labelClass
} from "../NewSkin/Styles.js";
import Button from "../NewSkin/Components/Button";
import GridContainer from "../NewSkin/Wrappers/GridContainer.js";

const HEALTH_DOCS_UPLOAD_URL = `${currentDomain}/api/upload/healthdocs`;

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.user = this.props.user;
    this.state = {
      fileUploadProgressValue: 0,
      files: [],
      is_hidden: true,
      uploading: false,
      uploadMessage: "",
      inputs: {
        description: {
          id: "",
          name: "description",
          question: "File Description",
          type: "textarea",
          validation: "name",
          disabled: true,
          options: [],
          error: {
            active: false,
            message: "Please enter File Description"
          },
          value: ""
        }
      },
      filename: "",
      uploadError: {
        active: false,
        message: ""
      }
    };
  }

  _displayDescriptionField(files) {
    this.setState({ is_hidden: false, files });
  }

  _resetFields() {
    let { inputs, is_hidden, files, uploadError } = this.state;
    inputs.description.value = "";
    is_hidden = true;
    uploadError.active = false
    files = [];
    this.setState({
      inputs,
      is_hidden,
      files
    });
  }

  _handleChange(e) {
    e.preventDefault();
    let { inputs } = this.state;
    inputs[e.target.name].value = e.target.value;
    this.setState({ inputs });
  }

  _onUploadClick(e) {
    e.preventDefault();
    this.setState({ uploading: true });
    this.handleImageUpload();
  }

  _onCancelClick(e) {
    e.preventDefault();
    this._resetFields();
  }

  updateProgresBar(percent) {
    this.setState({ fileUploadProgressValue: percent });
  }

  handleImageUpload() {
    const { files, inputs } = this.state;
    const file = files[0];
    const description = inputs.description.value;
    const filename = file.name;
    const pfd = moment();
    const prefix = `${pfd.format("YYMMDDhhmmss")}~`;

    const fd = new FormData();
    fd.append("healthdocs", true);
    fd.append("user_id", this.user.id);
    fd.append("prefix", prefix);
    fd.append("date_uploaded", moment().toISOString());
    fd.append("filename", filename);
    fd.append("description", description);
    fd.append("file", file);

    let upload = "";
    upload = request.post(HEALTH_DOCS_UPLOAD_URL, fd, {
      onUploadProgress: ProgressEvent => {
        this.updateProgresBar(
          Math.round((ProgressEvent.loaded / ProgressEvent.total) * 100)
        );
      }
    });

    upload.then(response => {
      if (response.status === 200) {
        this._displayMessage("Upload Success...");
        this._resetFields();
        setTimeout(() => {
          this.updateProgresBar(0);
        }, 3000);

        this.props.update();
        this.setState({ uploading: false });
      } else {
        this._displayMessage("Error Uploading");
        console.log("Response Error: ", response);
      }
    });
  }

  onImageDrop(files) {
    this.setState({
      filename: files[0].name
    });

    if (files.length > 0) {
      this._displayDescriptionField(files);
    } else {
      this._displayMessage("ERROR : File Not Allowed...");
    }
  }

  onReject(file) {
    console.log(file[0].name);
    const { uploadError } = this.state;
    uploadError.active = true;
    uploadError.message = (
      <span>
        <strong>{file[0].name}</strong> with the size of{" "}
        <strong>{file[0].size.toLocaleString()} bytes</strong> is not valid because of one of the following reasons:
      </span>
    );

    this.setState({ uploadError });
  }

  _displayMessage(string) {
    this.setState({ uploadMessage: string });
    setTimeout(() => {
      this.setState({ uploadMessage: "" });
    }, 5000);
  }

  _fileList() {
    const { files } = this.props;
    const li = files.map((file, i) => {
      const link = `/static/docs/${file.filename}`;
      return (
        <li key={i}>
          <a href={link}> {file.filename}</a>
        </li>
      );
    });
    return <ul>{li}</ul>;
  }

  render() {
    return (
      <section data-name="upload-wrapper" className={uploadWrapper}>
        {this.state.uploadError.active ? (
          <div className={warning}>
            <p>{this.state.uploadError.message}
            <br />
              <span> - Only <strong>"pdf", "jpg" </strong>and <strong>"png"</strong>{" "}
              files are accepted.</span><br />
              <span> - File size must not exceed <strong>300kb</strong>{" "}</span>
            </p>
          </div>
        ) : null}
        {this.state.uploading ? (
          <div style={{marginTop: 30}}>
            Uploading file, please wait...{this.state.fileUploadProgressValue}%
          </div>
        ) : (
          <div
            className={dropzoneWrapper}
            style={{ width: "100%", height: "100%" }}
          >
            <Dropzone
              onDropAccepted={files => this.onImageDrop(files, "PDFReport")}
              onDropRejected={files => this.onReject(files)}
              multiple={false}
              maxSize={300000}
              accept="application/pdf, image/jpeg, image/png"
            >
              {({ getRootProps, getInputProps }) => (
                <div className={`${drapAreaStyle}`} {...getRootProps()}>
                  <input {...getInputProps()} />

                  {this.state.uploadMessage != "" ? (
                    <span className={successStyle}>
                      {this.state.uploadMessage}
                    </span>
                  ) : (
                    <section>
                      <img src="/static/images/upload.png" />
                      <label htmlFor="">
                        Click here to select
                        <br />
                        file or drop files here
                        <br />
                        to upload.
                      </label>
                    </section>
                  )}
                </div>
              )}
            </Dropzone>

            {!this.state.is_hidden && (
              <section className={descriptionDiv}>
                <label htmlFor="" className={labelClass}>
                  <small>{this.state.filename}</small>
                </label>
                <textarea
                  className={combined([inputClass, scrollClass])}
                  name="description"
                  placeholder="File Description"
                  value={this.state.inputs.description.value}
                  onChange={this._handleChange.bind(this)}
                />

                <GridContainer columnSize="1fr 1fr" gap={20}>
                  <Button type="pink" styles={{ minWidth: "fit-content" }}>
                    <button onClick={this._onUploadClick.bind(this)}>
                      Upload
                    </button>
                  </Button>

                  <Button styles={{ minWidth: "fit-content" }}>
                    <button onClick={this._onCancelClick.bind(this)}>
                      Cancel
                    </button>
                  </Button>
                </GridContainer>
              </section>
            )}
          </div>
        )}
      </section>
    );
  }
}

const dropzoneWrapper = css({
  width: "100%",
  height: "100%",
  "& > div": {
    cursor: "pointer",
    height: "100%",
    padding: 30,
    boxSizing: "border-box",
    outline: "none",
    display: "grid",
    placeContent: "center",
    opacity: 1,
    transition: "250ms ease",
    "& img": {
      width: 100,
      display: "block",
      margin: "0 auto",
      opacity: 0.2
    },
    "& label": {
      fontWeight: 600,
      opacity: 0.3
    }
  }
});

const uploadWrapper = css({
  background: "#eee",
  width: "100%",
  borderRadius: 10,
  transition: "250ms ease",
  position: "relative",
  ":hover": {
    background: "#eee5"
  },
  height: 350
});

const drapAreaStyle = css({
  "@media print": {
    display: "none"
  }
});

const descriptionDiv = css({
  position: "absolute",
  top: 0,
  left: 0,
  width: "100%",
  height: "100%",
  padding: 30,
  boxSizing: "border-box",
  background: "#eee",
  "& textarea": {
    resize: "vertical",
    maxHeight: 270
  }
});

let warning = css({
  position: 'absolute',
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  flexWrap: "wrap",
  borderRadius: 5,
  fontSize: 'smaller',
  background: '#efc5bb',
  color: '#000',
  borderRadius: 10,
  margin: 10,
  padding: 5
});

let successStyle = css({
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  width: "100%",
  fontSize: "large",
  color: "#80d0ed",
  padding: "3",
  borderRadius: 5
});
