import React, { Component } from "react";
import { css } from "glamor";
import CoreImbalances from "../../components/SummaryAndAnalysis/coreImbalances";
import WriteAnalysis from "../../components/SummaryAndAnalysis/writeAnalysis.js";
import WriteModifiable from "../../components/SummaryAndAnalysis/writeModifiable.js";
import WriteSymptoms from "../../components/SummaryAndAnalysis/writeSymptoms.js";
import Timeline from "../../components/Timeline";
import AnswersService from "../../utils/answerService.js";
//import SummaryService from "../../utils/summaryService.js";
import Colors from "../NewSkin/Colors";
import { combined } from "../NewSkin/Styles";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Card from "../NewSkin/Wrappers/Card";
import Restricted from "../../components/restricted";
import { UserContext } from "../../context";

const AnswerService = new AnswersService();
//const summary = new SummaryService();

export default class extends Component {
  static contextType = UserContext;
  constructor(props) {
    super(props);
    this.user = this.props.user;
    this.state = {
      activeView: "HEALTH SURVEY",
      activeTab: "summary",
      forms: {
        writeAnalysis: true,
        //writeRecomendation: false,
        writeModifiable: false,
        writeSymptoms: false,
        timeline: false
      },
      scale: {
        radius: 200,
        width: 800,
        height: 600,
        fontSize: 12
      }
    };

    this._getCoreImbalancesData = this._getCoreImbalancesData.bind(this);
    this._updateCurrentView = this._updateCurrentView.bind(this);
    this._getSummary = this._getSummary.bind(this);
    this.toggleWAForm = this.toggleWAForm.bind(this);
  }

  _updateCurrentView(view) {
    let progress;
    this.setState({
      activeView: view
    });
  }

  toggleWAForm(form) {
    let forms = this.state.forms;
    let stat;
    let scale = this.state.scale;
    if (forms[form]) {
      forms[form] = false;
      scale.radius = 200;
      scale.width = 800;
      scale.height = 600;
      scale.fontSize = 12;
    } else {
      Object.keys(forms).forEach((item, i) => {
        if (item === form) {
          forms[form] = true;
        } else {
          forms[item] = false;
        }
      });
      forms[form] = true;
      scale.radius = 200;
      scale.width = 800;
      scale.height = 600;
      scale.fontSize = 12;
    }
    this.setState({
      forms,
      scale
    });
  }

  _getSummary(viewFromAnswers, userid) {
    return this.context.summaryService
      .get({ userId: userid, distinct: true })
      .then(res => {
        if (res.name === "Error") {
          console.log(res);
        } else {
          return Promise.resolve(res);
        }
      });
  }

  _getCoreImbalancesData(viewFromSummary) {
    return AnswerService.getAnswerList({ coreImbalances: true }, this.user.id)
      .then(res => {
        if (viewFromSummary && res != "undefined") {
          return Promise.resolve(res);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  render() {
    if (this.user.telemedicine_enabled !== "Approved") {
      let restrict_msg = `This feature is not included in your current subscription. Please contact the officer in-charge of your account if you wish to subscribe`;
      return <Restricted message={restrict_msg} />;
    }
    return (
      <GridContainer
        columnSize={"1fr"}
        rowGap={20}
        styles={{ alignContent: "start" }}
      >
        <div className="wrap">
          <div
            className={combined([centered], {
              borderBottom: `2px solid ${Colors.pink}`
            })}
          >
            <GridContainer
              styles={{ lineHeight: "normal", width: "100%" }}
              contain
              columnSize={"repeat(4, 1fr)"}
              columnGap={20}
            >
              <div
                className={`${tabStyle} ${
                  this.state.forms.writeAnalysis ? activeTab : inactiveTab
                }`}
                onClick={() => this.toggleWAForm("writeAnalysis")}
              >
                ANALYSIS &<br />
                RECOMMENDATIONS
              </div>
              <div
                className={`${tabStyle} ${
                  this.state.forms.writeModifiable ? activeTab : inactiveTab
                }`}
                onClick={() => this.toggleWAForm("writeModifiable")}
              >
                MODIFIABLE PERSONAL
                <br />
                LIFESTYLE FACTORS
              </div>
              <div
                className={`${tabStyle} ${
                  this.state.forms.writeSymptoms ? activeTab : inactiveTab
                }`}
                onClick={() => this.toggleWAForm("writeSymptoms")}
              >
                SYMPTOMS REVIEW
              </div>
              <div
                className={`${tabStyle} ${
                  this.state.forms.timeline ? activeTab : inactiveTab
                }`}
                onClick={() => this.toggleWAForm("timeline")}
              >
                HEALTH TIMELINE
              </div>
            </GridContainer>
          </div>

          <div className={twoColum}>
            <GridContainer
              columnSize={this.state.forms.writeAnalysis ? "1fr 2fr" : "1fr"}
              gap={20}
              styles={{ width: "100%" }}
            >
              {this.state.forms.writeModifiable === false &&
              this.state.forms.writeSymptoms === false &&
              this.state.forms.timeline === false ? (
                <div
                  style={{
                    width: !this.state.forms.writeAnalysis ? "100%" : 550
                  }}
                  className={column2}
                >
                  {this._renderView()}
                </div>
              ) : null}
              {this.state.forms.writeAnalysis && (
                <div className={column1}>
                  <WriteAnalysis
                    toggleWAForm={this.toggleWAForm}
                    user={this.user}
                    patientInfo={this.user}
                  />
                </div>
              )}
            </GridContainer>

            {this.state.forms.writeModifiable && (
              <WriteModifiable
                scores={this._getSummary}
                toggleWAForm={this.toggleWAForm}
                user={this.user}
                patientInfo={this.user}
              />
            )}

            {this.state.forms.writeSymptoms && (
              <WriteSymptoms
                toggleWAForm={this.toggleWAForm}
                patientInfo={this.state.patientInfo}
                user={this.user}
              />
            )}

            {this.state.forms.timeline && (
              <div className={column1}>
                <Timeline userId={this.user.id} userType={this.user.type} />
              </div>
            )}
          </div>
        </div>
      </GridContainer>
    );
  }

  _renderView() {
    return (
      <CoreImbalances
        coreImbalances={this._getCoreImbalancesData}
        updateView={this._updateCurrentView}
        scale={this.state.scale}
      />
    );
  }
}

// styling

let inactiveTab = css({
  fontSize: 15,
  fontWeight: 600,
  paddingBottom: 5,
  color: Colors.blueDarkAccent,
  borderBottom: `5px solid white`,
  transition: "250ms ease",
  ":hover": {
    color: Colors.pink
  },
  cursor: "pointer"
});

let activeTab = css(inactiveTab, {
  color: Colors.pink,
  borderBottom: `5px solid ${Colors.pink}`
});

let tabStyle = css({
  // textAlign: "center",
  // padding: "5px 20px",
  // marginLeft: "10px",
  // marginRight: "10px",
  // display: "inline-block"
});

let twoColum = css({
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 100%",
  padding: 10,
  transition: "width 2s"
});

let column1 = css({
  flex: "1 0 30%",
  transition: "width 2s",
  borderRadius: "7px",
  // border: "1px solid #d3d3d3",
  // background: "#f4f4f4",
  padding: 5
});
let column2 = css({
  display: "flex",
  flex: "1",
  width: "550px",
  transition: "100ms ease",
  justifyContent: "center",
  alignItems: "flex-start"
  // borderRadius: '7px',
  // border: '1px solid #444141',
  // padding: 15
});

let centered = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "center"
});

let warning = css({
  color: "orange",
  border: "1px dotted orange",
  padding: "3",
  borderRadius: 5
});
