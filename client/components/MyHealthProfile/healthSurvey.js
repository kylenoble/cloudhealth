/* eslint-disable radix */
/* eslint-disable no-unused-expressions */
import React from "react";
import { css } from "glamor";
import moment from "moment-timezone";
import Router from "next/router";
import Constants from "../../constants";
import Body from "../../components/BodyInfo";
import Readiness from "../../components/Readiness";
import Nutrition from "../../components/Nutrition";
import Sleep from "../../components/Sleep";
import Exercise from "../../components/Exercise";
import Detox from "../../components/IntakeInfo";
import MSQ from "../../components/Symptoms";
import Stress from "../../components/Stress";
import HealthInfo from "../../components/HealthInfo";
import HealthGoals from "../../components/HealthGoals";
import Timeline from "../../components/Timeline";
import AnswerService from "../../utils/answerService.js";
import UserService from "../../utils/userService.js";
import SubscriptionService from "../../utils/subscriptionService.js";
import Colors from "../NewSkin/Colors";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import Button from "../NewSkin/Components/Button";
import { on_mobile, combined, link, font } from "../NewSkin/Styles";
import symptoms from "../Symptoms/symptoms";
import PreviousResponses from "./previousResponses";
import { UserContext } from "../../context";
import { Preparing } from "../NewSkin/Components/Loading";

const Answer = new AnswerService();
const user = new UserService();
const Subscription = new SubscriptionService();

export default class extends React.Component {
  static contextType = UserContext;
  constructor(props) {
    super(props);

    this.user = this.props.user;
    this.state = {
      with_active_subscription: false,
      status: {
        Readiness: false,
        Body: false,
        Nutrition: false,
        Sleep: false,
        Detox: false,
        Exercise: false,
        Stress: false,
        Health: false,
        Symptoms: false,
        Goals: false,
        Personal: false
      },
      activeView: "",
      userid: 0,
      locked: false,
      message: null,
      active_survey: false,
      profile: user.getProfile(),
      survey: {
        personal_info: {
          birthday: "",
          gender: "",
          relationship: "",
          race: "",
          raceOther: "",
          psa: "",
          psaLevel: "",
          maleIssues: "",
          menstrual: "",
          pregnant: "",
          numberChildren: ""
        },
        body: {
          data: {},
          height: "",
          weight: "",
          waist: "",
          hip: "",
          gender: ""
        },
        readiness: {
          modifyDiet: "",
          nutritionalSupplements: "",
          recordEating: "",
          modifyLifestyle: "",
          practiceRelaxation: "",
          regularExercise: "",
          labTest: ""
        },
        nutrition: {
          specialDiet: "",
          foodAllergies: "",
          meatConsumedPercent: "",
          vegetableConsumedPercent: "",
          fruitConsumedPercent: "",
          starchConsumedPercent: "",
          poultryConsumedPercent: "",
          seafoodConsumedPercent: "",
          usualMeal: ""
        },
        sleep: {
          regularSleep: "",
          averageSleep: "",
          troubleFallingAsleep: "",
          restedFeeling: "",
          sleepingAids: ""
        },
        stress: {
          counseling: "",
          therapy: "",
          workStress: "",
          familyStress: "",
          socialStress: "",
          financialStress: "",
          healthStress: "",
          otherStress: ""
        },
        detox: {
          prescriptionDrugs: "",
          prescriptionScenarios: "",
          caffeine: "",
          illAlcohol: "",
          overTheCounter: "",
          tobacco: "",
          odors: "",
          dependent: "",
          prescriptionCount: ""
        },
        movement: {
          weeklyExercise: "",
          limitActivity: "",
          postActivitySoreness: "",
          workActivity: "",
          sittingHours: ""
        },
        msq: {},
        healthGoals: {
          goalOne: "",
          goalTwo: "",
          goalThree: "",
          goalOne_Cat: "",
          goalTwo_Cat: "",
          goalThree_Cat: ""
        }
      },
      processing: false,

      ...symptoms
    };

    this._updateData = this._updateData.bind(this);
    this._setDefaultView = this._setDefaultView.bind(this);

    this.scrollToTop = React.createRef();
  }

  componentWillMount() {
    this._checkSurveyStatus();
    this._getSubscription();
    this._getAllAnswer();
  }

  _checkSurveyStatus() {
    this.setState({
      userid: this.state.profile.id,
      locked: this.state.profile.healthsurvey_status === "Locked" || false,
      active_survey:
        this.state.profile.healthsurvey_status === "Active" || false
    });
  }

  _getSubscription() {
    const arg = {
      field: "company_id",
      id: this.state.profile.company_id,
      status: `'Ongoing', 'For Renewal'` // this is for WHERE IN QUERY. dont forget the single quote for values
    };

    let { with_active_subscription } = this.state;

    Subscription.get(arg)
      .then(res => {
        if (res.length > 0) {
          with_active_subscription = true;
        }

        this.setState({ with_active_subscription });
      })
      .catch(e => console.log(e));
  }

  _groupAnswersAccordingly(res) {
    const forPersonalInfo = res.filter(re => re.question_id < 11);
    const forWieght = res.filter(re => re.question_id < 53);
    const forReadiness = res.filter(re => re.question_id < 18);
    const forNutrition = res.filter(re => re.question_id < 135);
    const forSleep = res.filter(re => re.question_id < 35);
    const forStress = res.filter(re => re.question_id < 48);
    const forDetox = res.filter(re => re.question_id < 63);
    const forMovement = res.filter(re => re.question_id < 40);
    const forSymptoms = res.filter(re => re.question_id > 62);
    const forHealthIssues = res.filter(re => re.question_id === 53);
    const forHealthGoals = res.filter(re => re.question_id === 133);

    this._getPersonalInfoData(forPersonalInfo);
    this._getBodyData(forWieght);
    this._getReadinessData(forReadiness);
    this._getNutirionData(forNutrition);
    this._getSleepData(forSleep);
    this._getStreesData(forStress);
    this._getDetoxData(forDetox);
    this._getMovementData(forMovement);
    this._getSymptomsData(forSymptoms);
    this._getHealthIssuesData(forHealthIssues);
    this._getHealthGoalsData(forHealthGoals);
  }

  _getPersonalInfoData(answers) {
    const { survey } = this.state;

    for (const answer of answers) {
      if (answer.name === "birthday") {
        survey.personal_info.birthday = answer.value;
      } else if (answer.name === "gender") {
        survey.personal_info.gender = answer.value;
      } else if (answer.name === "psa") {
        survey.personal_info.psa = answer.value;
      } else if (answer.name === "psaLevel") {
        survey.personal_info.psaLevel = answer.value;
      } else if (answer.name === "relationship") {
        survey.personal_info.relationship = answer.value;
      } else if (answer.name === "maleIssues") {
        survey.personal_info.maleIssues = answer.value;
      } else if (answer.name === "race") {
        survey.personal_info.race = answer.value;
        survey.personal_info.raceOther = answer.value;
      } else if (answer.name === "menstrual") {
        survey.personal_info.menstrual = answer.value;
      } else if (answer.name === "pregnant") {
        survey.personal_info.pregnant = answer.value;
      } else if (answer.name === "numberChildren") {
        survey.personal_info.numberChildren = answer.value;
      }
    }

    this.setState({ survey });
  }

  _getBodyData = answers => {
    const { survey } = this.state;

    for (const answer of answers) {
      if (answer.name === "height") {
        survey.body.height = answer.value;
      } else if (answer.name === "weight") {
        survey.body.weight = answer.value;
      } else if (answer.name === "waist") {
        survey.body.waist = answer.value;
      } else if (answer.name === "hip") {
        survey.body.hip = answer.value;
      } else if (answer.name === "gender") {
        survey.body.gender = answer.value;
      }
    }

    survey.body.data = this.context.survey;

    this.setState({
      survey
    });
  };

  _getBodyDataSummary() {
    const { survey } = this.state;

    survey.body.data = this.context.survey;

    this.setState({
      survey
    });
  };

  _getReadinessData = answers => {
    const { survey } = this.state;

    for (const answer of answers) {
      if (answer.name === "modifyDiet") {
        survey.readiness.modifyDiet = answer.value;
      } else if (answer.name === "nutritionalSupplements") {
        survey.readiness.nutritionalSupplements = answer.value;
      } else if (answer.name === "recordEating") {
        survey.readiness.recordEating = answer.value;
      } else if (answer.name === "modifyLifestyle") {
        survey.readiness.modifyLifestyle = answer.value;
      } else if (answer.name === "practiceRelaxation") {
        survey.readiness.practiceRelaxation = answer.value;
      } else if (answer.name === "regularExercise") {
        survey.readiness.regularExercise = answer.value;
      } else if (answer.name === "labTests") {
        survey.readiness.labTests = answer.value;
      }
    }

    this.setState({ survey });
  };

  _getNutirionData = answers => {
    const { survey } = this.state;

    for (const answer of answers) {
      if (answer.name === "specialDiet") {
        survey.nutrition.specialDiet = answer.value;
      } else if (answer.name === "foodAllergies") {
        survey.nutrition.foodAllergies = answer.value;
      } else if (answer.name === "meatConsumedPercent") {
        survey.nutrition.meatConsumedPercent = answer.value;
      } else if (answer.name === "vegetableConsumedPercent") {
        survey.nutrition.vegetableConsumedPercent = answer.value;
      } else if (answer.name === "fruitConsumedPercent") {
        survey.nutrition.fruitConsumedPercent = answer.value;
      } else if (answer.name === "starchConsumedPercent") {
        survey.nutrition.starchConsumedPercent = answer.value;
      } else if (answer.name === "poultryConsumedPercent") {
        survey.nutrition.poultryConsumedPercent = answer.value;
      } else if (answer.name === "seafoodConsumedPercent") {
        survey.nutrition.seafoodConsumedPercent = answer.value;
      } else if (answer.name === "usualMeal") {
        survey.nutrition.usualMeal = answer.value;
      }
    }

    this.setState({
      survey
    });
  };

  _getSleepData(answers) {
    const { survey } = this.state;

    for (const answer of answers) {
      if (answer.name === "regularSleep") {
        survey.sleep.regularSleep = answer.value;
      } else if (answer.name === "averageSleep") {
        survey.sleep.averageSleep = answer.value;
      } else if (answer.name === "troubleFallingAsleep") {
        survey.sleep.troubleFallingAsleep = answer.value;
      } else if (answer.name === "restedFeeling") {
        survey.sleep.restedFeeling = answer.value;
      } else if (answer.name === "sleepingAids") {
        survey.sleep.sleepingAids = answer.value;
      }
    }

    this.setState({ survey });
  }

  _getStreesData(answers) {
    const { survey } = this.state;

    for (const answer of answers) {
      if (answer.name === "counseling") {
        survey.stress.counseling = answer.value;
      } else if (answer.name === "therapy") {
        survey.stress.therapy = answer.value;
      } else if (answer.name === "workStress") {
        survey.stress.workStress = answer.value;
      } else if (answer.name === "familyStress") {
        survey.stress.familyStress = answer.value;
      } else if (answer.name === "socialStress") {
        survey.stress.socialStress = answer.value;
      } else if (answer.name === "financialStress") {
        survey.stress.financialStress = answer.value;
      } else if (answer.name === "healthStress") {
        survey.stress.healthStress = answer.value;
      } else if (answer.name === "otherStress") {
        survey.stress.otherStress = answer.value;
      }
    }

    this.setState({
      survey
    });
  }

  _getDetoxData(answers) {
    const { survey } = this.state;

    for (const answer of answers) {
      if (answer.name === "prescriptionDrugs") {
        survey.detox.prescriptionDrugs = answer.value;
      } else if (answer.name === "prescriptionScenarios") {
        survey.detox.prescriptionScenarios = answer.value;
      } else if (answer.name === "caffeine") {
        survey.detox.caffeine = answer.value;
      } else if (answer.name === "illAlcohol") {
        survey.detox.illAlcohol = answer.value;
      } else if (answer.name === "overTheCounter") {
        survey.detox.overTheCounter = answer.value;
      } else if (answer.name === "tobacco") {
        survey.detox.tobacco = answer.value;
      } else if (answer.name === "odors") {
        survey.detox.odors = answer.value;
      } else if (answer.name === "dependent") {
        survey.detox.dependent = answer.value;
      } else if (answer.name === "prescriptionCount") {
        survey.detox.prescriptionCount = answer.value;
      }
    }

    this.setState({ survey });
  }

  _getMovementData(answers) {
    const { survey } = this.state;

    for (const answer of answers) {
      if (answer.name === "weeklyExercise") {
        survey.movement.weeklyExercise = answer.value;
      } else if (answer.name === "limitActivity") {
        survey.movement.limitActivity = answer.value;
      } else if (answer.name === "postActivitySoreness") {
        survey.movement.postActivitySoreness = answer.value;
      } else if (answer.name === "workActivity") {
        survey.movement.workActivity = answer.value;
      } else if (answer.name === "sittingHours") {
        survey.movement.sittingHours = answer.value;
      }
    }

    this.setState({ survey });
  }

  _getSymptomsData(answers) {
    const newState = {};
    const { groups, survey } = this.state;
    const aAnsCtr = {
      Activity: 0,
      "Eyes, Ears, Nose": 0,
      "Head and Mind": 0,
      "Mouth, Throat, Lungs": 0,
      Other: 0,
      "Weight and Digestion": 0
    };
    const idArray = {
      Activity: Constants.MEH,
      "Eyes, Ears, Nose": Constants.EEN,
      "Head and Mind": Constants.HM,
      "Mouth, Throat, Lungs": Constants.MTL,
      Other: Constants.OTHER,
      "Weight and Digestion": Constants.WD
    };

    for (const answer of answers) {
      // eslint-disable-next-line no-prototype-builtins
      if (groups.hasOwnProperty(answer.group)) {
        if (
          idArray[answer.group] &&
          idArray[answer.group].indexOf(answer.id) !== -1
        ) {
          aAnsCtr[answer.group]++;
        }
        groups[answer.group] = {
          label: groups[answer.group].label,
          value: groups[answer.group].value,
          total: 0
        };
        groups[answer.group].submitted = aAnsCtr[answer.group];
        groups[answer.group].total = idArray[answer.group].length;

        answer.submitted = true;
        answer.value = parseInt(answer.value);
        newState[answer.name] = answer;
      }
    }
    newState.groups = groups;
    survey.msq = newState;
    this.setState({ survey });
  }

  _getHealthIssuesData(answers) {
    const lastIndex = answers.length - 1;
    const latestAnswer = answers[lastIndex];
    const { survey } = this.state;
    survey.healthIssues = latestAnswer;
    this.setState({ survey });
  }

  _getHealthGoalsData(answers) {
    const { survey } = this.state;

    let ansr = "";

    let ctr = 0;
    for (const answer of answers) {
      ansr = answer.value.split("_");

      if (ctr === 0) {
        survey.healthGoals.goalOne = ansr[1];
        survey.healthGoals.goalOne_Cat = ansr[0];
      } else if (ctr === 1) {
        survey.healthGoals.goalTwo = ansr[1];
        survey.healthGoals.goalTwo_Cat = ansr[0];
      } else if (ctr === 2) {
        survey.healthGoals.goalThree = ansr[1];
        survey.healthGoals.goalThree_Cat = ansr[0];
      } else if (ctr >= 3) {
        break;
      }
      ctr++;
    }
    this.setState({ survey });
  }

  //get all answers
  _getAllAnswer() {
    Answer.getAnswerList({
      column: "question_id",
      operator: "gt",
      value: 0,
      order: "asc",
      by: "id"
    })
      .then(res => {
        this._groupAnswersAccordingly(res);
        this._checkData(res); // this will check if all required forms are filled
      })
      .catch(e => {
        console.log(e);
      });
  }

  // Muscle Energy Heart
  _checkData(ans) {
    const rdns = [];
    const bdy = [];
    const nutri = [];
    const slp = [];
    const dtx = [];
    const exrcs = [];
    const strs = [];
    const hi = [];
    const hg = [];

    const meh = [];
    const een = [];
    const hm = [];
    const mtl = [];
    const other = [];
    const wd = [];
    const personal = [];

    const ansCtr = {
      Activity: 0,
      "Eyes, Ears, Nose": 0,
      "Head and Mind": 0,
      "Mouth, Throat, Lungs": 0,
      Other: 0,
      "Weight and Digestion": 0,
      personal: 0
    };

    const status = this.state.status;
    // readiness
    for (const data of ans) {
      if (Constants.RDNS.indexOf(data.question_id) !== -1) {
        if (rdns.indexOf(data.group) === -1) {
          status.Readiness = true;
        }
      }

      // body
      if (Constants.BDY.indexOf(data.question_id) !== -1) {
        if (bdy.indexOf(data.group) === -1) {
          status.Body = true;
        }
      }

      // nutrition
      if (Constants.NUTRI.indexOf(data.question_id) !== -1) {
        if (nutri.indexOf(data.group) === -1) {
          status.Nutrition = true;
        }
      }

      // sleep
      if (Constants.SLP.indexOf(data.question_id) !== -1) {
        if (slp.indexOf(data.group) === -1) {
          status.Sleep = true;
        }
      }

      // Detox
      if (Constants.DTX.indexOf(data.question_id) !== -1) {
        if (dtx.indexOf(data.group) === -1) {
          status.Detox = true;
        }
      }

      // Exercise
      if (Constants.EXRCS.indexOf(data.question_id) !== -1) {
        if (exrcs.indexOf(data.group) === -1) {
          status.Exercise = true;
        }
      }

      // stress
      if (Constants.STRS.indexOf(data.question_id) !== -1) {
        if (strs.indexOf(data.group) === -1) {
          status.Stress = true;
        }
      }

      // Health Issues
      if (Constants.HI.indexOf(data.question_id) !== -1) {
        if (hi.indexOf(data.group) === -1) {
          status.Health = true;
        }
      }

      // Health Goals
      if (Constants.HG.indexOf(data.question_id) !== -1) {
        if (hg.indexOf(data.group) === -1) {
          status.Goals = true;
        }
      }

      // get muscles, energy,heart data
      if (Constants.MEH.indexOf(data.question_id) !== -1) {
        ansCtr[data.group]++;
        if (meh.indexOf(data.group) === -1) {
          meh.push(data.group);
        }
      }

      // Eyes Ear Nose
      if (Constants.EEN.indexOf(data.question_id) !== -1) {
        ansCtr[data.group]++;
        if (een.indexOf(data.group) === -1) {
          een.push(data.group);
        }
      }

      // Head and Mind
      if (Constants.HM.indexOf(data.question_id) !== -1) {
        ansCtr[data.group]++;
        if (hm.indexOf(data.group) === -1) {
          hm.push(data.group);
        }
      }

      // Mouth Throat Lungs
      if (Constants.MTL.indexOf(data.question_id) !== -1) {
        ansCtr[data.group]++;
        if (mtl.indexOf(data.group) === -1) {
          mtl.push(data.group);
        }
      }

      // Constants.OTHER
      if (Constants.OTHER.indexOf(data.question_id) !== -1) {
        ansCtr[data.group]++;
        if (other.indexOf(data.group) === -1) {
          other.push(data.group);
        }
      }

      // Weight Digestion
      if (Constants.WD.indexOf(data.question_id) !== -1) {
        ansCtr[data.group]++;
        if (wd.indexOf(data.group) === -1) {
          wd.push(data.group);
        }
      }

      // PERSONAL
      //let ctrFor!
      if (Constants.PERSONAL.indexOf(data.question_id) !== -1) {
        if (personal.indexOf(data.question_id) === -1) {
          personal.push(data.question_id);
          ansCtr[data.group]++;
        }
      }
    }

    meh.ansCtr = ansCtr.Activity;
    meh.xptdAns = Constants.MEH.length;

    een.ansCtr = ansCtr["Eyes, Ears, Nose"];
    een.xptdAns = Constants.EEN.length;

    hm.ansCtr = ansCtr["Head and Mind"];
    hm.xptdAns = Constants.HM.length;

    mtl.ansCtr = ansCtr["Mouth, Throat, Lungs"];
    mtl.xptdAns = Constants.MTL.length;

    other.ansCtr = ansCtr.Other;
    other.xptdAns = Constants.OTHER.length;

    wd.ansCtr = ansCtr["Weight and Digestion"];
    wd.xptdAns = Constants.WD.length;

    personal.ansCtr = ansCtr.personal;
    personal.xptdAns = Constants.PERSONAL.length;

    // PERSONAL CHECKER
    if (personal.ansCtr < personal.xptdAns) {
      status.Personal = false;
    } else {
      status.Personal = true;
    }

    // SYMPTOMS CHECKER
    if (
      meh.ansCtr < meh.xptdAns ||
      een.ansCtr < een.xptdAns ||
      hm.ansCtr < hm.xptdAns ||
      mtl.ansCtr < mtl.xptdAns ||
      other.ansCtr < other.xptdAns ||
      wd.ansCtr < wd.xptdAns
    ) {
      status.Symptoms = false;
    } else {
      status.Symptoms = true;
    }

    this.setState(
      {
        status
      },
      function () {
        if (
          this.state.activeView === "" &&
          !this.state.locked &&
          this.state.with_active_subscription
        ) {
          setTimeout(() => {
            this.context.refetchSummary();
          }, 2000)
          this._getBodyDataSummary()
          const is_survey_completed = this._surveyCompleted();
          if (is_survey_completed) {
            this.props.openModal("surveySuccessModalOpen");
          }
        }
      }
    );
  }

  _setDefaultView(view = "") {
    let activeView = view;
    if (this.symptoms && this.symptoms.getSymptomName() !== "") {
      activeView = "Symptoms";
    }
    this._getAllAnswer();
    this.setState({
      activeView
    });
  }

  _renderActiveView() {
    if (this.state.activeView === "Body") {
      return (
        <Body survey={this.state.survey.body} back={this._setDefaultView} />
      );
    } else if (this.state.activeView === "Readiness") {
      return (
        <Readiness
          survey={this.state.survey.readiness}
          back={this._setDefaultView}
        />
      );
    } else if (this.state.activeView === "Nutrition") {
      return (
        <Nutrition
          survey={this.state.survey.nutrition}
          back={this._setDefaultView}
        />
      );
    } else if (this.state.activeView === "Sleep") {
      return (
        <Sleep survey={this.state.survey.sleep} back={this._setDefaultView} />
      );
    } else if (this.state.activeView === "Detox") {
      return (
        <Detox
          survey={this.state.survey.detox}
          back={this._setDefaultView}
          location="profile"
        />
      );
    } else if (this.state.activeView === "Exercise") {
      return (
        <Exercise
          survey={this.state.survey.movement}
          back={this._setDefaultView}
        />
      );
    } else if (this.state.activeView === "Symptoms") {
      return (
        <MSQ
          location="profile"
          onRef={ref => (this.symptoms = ref)}
          back={this._setDefaultView}
          survey={this.state.survey.msq}
        />
      );
    } else if (this.state.activeView === "Health") {
      return (
        <HealthInfo
          survey={this.state.survey.healthIssues}
          back={this._setDefaultView}
          location="profile"
        />
      );
    } else if (this.state.activeView === "Stress") {
      return (
        <Stress survey={this.state.survey.stress} back={this._setDefaultView} />
      );
    } else if (this.state.activeView === "Goals") {
      return (
        <HealthGoals
          survey={this.state.survey.healthGoals}
          back={this._setDefaultView}
        />
      );
    } else if (this.state.activeView === "Timeline") {
      return (
        <Timeline
          userId={this.user.id}
          userType={this.user.type}
          defaultView={"description"}
          back={this._setDefaultView}
        />
      );
    } else if (this.state.activeView === "previousResponses") {
      return <PreviousResponses />;
    }
    return this._showEditItmes();
  }

  _updateData(e) {
    this.setState(
      {
        activeView: e.target.id
      },
      function () {
        // scrolls user into top section
        //.current is verification that your element has rendered
        if (this.scrollToTop.current) {
          this.scrollToTop.current.scrollIntoView({
            behavior: "smooth",
            block: "nearest"
          });
        }
      }
    );
  }

  _goTo(lnk) {
    Router.push(`/account?tab=Profile&view=${lnk}`);
  }

  _showEditItmes() {
    const editItem = [
      {
        label: "Readiness Questions",
        group: "Readiness"
      },
      {
        label: "Detoxification",
        group: "Detox"
      },
      {
        label: "Weight",
        group: "Body"
      },
      {
        label: "Movement",
        group: "Exercise"
      },
      {
        label: "Nutrition",
        group: "Nutrition"
      },
      {
        label: "Symptoms Review",
        group: "Symptoms"
      },
      {
        label: "Sleep",
        group: "Sleep"
      },
      {
        label: "Health Issues",
        group: "Health"
      },
      {
        label: "Stress",
        group: "Stress"
      },
      {
        label: "Health Goals",
        group: "Goals"
      }
    ];
    const html = [];

    editItem.forEach((edit_item, index) => {
      (this.state.locked && this._surveyCompleted()) ||
        !this.state.with_active_subscription
        ? html.push(
          <div key={index}>
            <GridContainer
              columnSize={"2fr 1fr"}
              classes={[
                on_mobile([], { gridTemplateColumns: "2fr 1fr !important" })
              ]}
            >
              <span className={label}>{edit_item.label}</span>{" "}
              <span
                id={edit_item.group}
                className={combined([btn, font(0.8)], {
                  color: Colors.disabled
                })}
                onClick={this._lockedMessage}
              >
                {this.state[edit_item.group]}
                {this.state.status[edit_item.group]
                  ? "UPDATE"
                  : "CREATE"}{" "}
              </span>
            </GridContainer>
          </div>
        )
        : html.push(
          <div key={index}>
            <GridContainer
              columnSize={"2fr 1fr"}
              classes={[
                on_mobile([], { gridTemplateColumns: "2fr 1fr !important" })
              ]}
            >
              <span className={label}>{edit_item.label}</span>{" "}
              <span
                id={edit_item.group}
                className={
                  this.state.status[edit_item.group]
                    ? combined([btn, font(0.8)])
                    : combined([Incomplete, font(0.8)])
                }
                onClick={this._updateData}
              >
                {this.state[edit_item.group]}
                {this.state.status[edit_item.group]
                  ? "UPDATE"
                  : "CREATE"}{" "}
              </span>
            </GridContainer>
          </div>
        );
    });

    return (
      <div style={{ width: "100%" }}>
        <GridContainer
          gap={15}
          columnSize={"1fr 1fr"}
          styles={{ margin: "20px auto", width: "80%" }}
        >
          {html}
        </GridContainer>
      </div>
    );
  }

  _lockedMessage = () => {
    this.setState({
      message:
        "Responses already submitted. The forms are no longer editable. You may however be asked to update your responses in the future. For support and technical concerns, please email us at client.services@cloudhealthasia.com"
    });
  };

  lockSurvey = () => {
    const inputs = [
      {
        healthsurvey_status: "Locked",
        userId: this.state.userid
      }
    ];
    this._updateSurveyStatus(inputs, true);
  };

  _preraDataForHealthSummary() {
    this.setState({ processing: true });
    // this will reload data for previous Responses
    // this.context.refetchSummary();
    // console.log('after refetchSummary----', this.state.survey)
  }

  _saveHealthSurveySummary() {
    const { survey } = this.state;

    const data = {
      user_id: this.user.id, //int
      personal_info: survey.personal_info, //jsonb
      readiness: survey.readiness, //jsonb
      weight: survey.body, //jsonb
      nutrition: survey.nutrition, //jsonb
      sleep: survey.sleep, //jsonb
      stress: survey.stress, //jsonb
      detoxification: survey.detox, //jsonb
      movement: survey.movement, //jsonb
      symptoms_review: survey.msq, //jsonb
      health_issues: survey.healthIssues, //jsonb
      health_goals: survey.healthGoals, //jsonb
      date_created: moment()
    };

    this.context.summaryService
      .saveHealthSurveySummary(data)
      .then(res => {
        if (res) {
          console.log("Saving Healthsurvey Summary Successfull.");
          this.context.reFetchHealthSummary(); // this will reload data for previous Responses
          this.setState({
            processing: false
          });
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _updateSurveyStatus = (inputs, lockStatus) => {
    user.update(inputs).then(res => {
      if (res) {
        if (lockStatus) {
          this._preraDataForHealthSummary();
          this._saveHealthSurveySummary();
          this.props.openModal("surveyCompleteModalOpen");

          //  store in user_activities table
          const data = {
            user_id: this.state.userid,
            action: "Lock Survey",
            details: {}
          };
          user
            .saveActivity(data)
            .then(() => { })
            .catch(err => {
              console.log(`Error saving activity : ${err}`);
            });
        }
        this.setState({
          locked: lockStatus,
          active_survey: true
        });
      } else {
        this.setState({
          formError: {
            active: true,
            message: "There was an error updating your profile"
          }
        });
      }
    });
  };

  _surveyCompleted() {
    const status = this.state.status;
    let cnt_incomplete = 0;
    let cnt_complete = 0;
    for (const key of Object.keys(status)) {
      if (status[key] === false) {
        cnt_incomplete++;
      } else {
        cnt_complete++;
      }
    }

    if (cnt_complete > 0 && !this.state.active_survey && !this.state.locked) {
      const inputs = [
        {
          healthsurvey_status: "Active",
          userId: this.state.userid
        }
      ];
      this._updateSurveyStatus(inputs, false);
    }
    return !(cnt_incomplete > 0);
  }

  _updateView(lnk) {
    this.props.updateView(lnk);
    //Router.push('/timeline')
  }

  render() {
    console.log(this.state.survey)
    const updateForm = this.state.activeView;
    this.state.activeView === "previousResponses";

    if (this.state.processing) {
      return <Preparing />;
    }

    return (
      <GridContainer columnSize={"1fr"} rowGap={20}>
        <div className="wrap">
          <div
            className={combined([centered], {
              borderBottom: `2px solid ${Colors.pink}`
            })}
          >
            <GridContainer
              styles={{ lineHeight: "normal", width: "100%" }}
              contain
              columnSize={"repeat(2, 1fr)"}
              columnGap={20}
            >
              <div
                ref={this.scrollToTop}
                className={`${
                  updateForm !== "previousResponses" ? activeTab : inactiveTab
                  }`}
                onClick={() => this._setDefaultView("")}
              >
                HEALTH SURVEY
              </div>
              <div
                className={`${
                  updateForm === "previousResponses" ? activeTab : inactiveTab
                  }`}
                onClick={() => this._setDefaultView("previousResponses")}
              >
                PREVIOUS RESPONSES
              </div>
            </GridContainer>
          </div>
          {!updateForm && this.state.locked && this.state.message != null && (
            <h4 className={lockedMessage}>{this.state.message}</h4>
          )}
          <div className={column1}>{this._renderActiveView()}</div>

          {updateForm === "" &&
            ((!this.state.locked && this.state.with_active_subscription) ||
              !this._surveyCompleted()) ? (
              <div style={{ textAlign: "center", width: "100%" }}>
                <div style={{ textAlign: "center", width: "100%" }}>
                  <Button
                    styles={{
                      margin: "0 auto",
                      fontSize: ".8rem !important"
                    }}
                    type={!this._surveyCompleted() ? "disabled" : "pink"}
                  >
                    <span
                      disabled={!this._surveyCompleted()}
                      onClick={
                        !this._surveyCompleted() ? () => { } : this.lockSurvey
                      }
                    >
                      SUBMIT RESPONSES
                  </span>
                  </Button>
                </div>
              </div>
            ) : null}
        </div>
      </GridContainer>
    );
  }
}

const label = css({
  textAlign: "left !important",
  display: "grid",
  alignContent: "center"
  // width: "50%"
});

const btn = css(link(Colors.skyblue));
const Incomplete = css(link(Colors.blueDarkAccent));

const lockedMessage = css({
  fontWeight: 500,
  fontStyle: "italic",
  padding: "5px 20px",
  fontSize: ".9rem",
  color: Colors.nutritionColor
});

let column1 = css({
  flex: "1 0 30%",
  transition: "width 2s",
  borderRadius: "7px",
  padding: 5
});

let inactiveTab = css({
  fontSize: 15,
  fontWeight: 600,
  paddingBottom: 5,
  color: Colors.blueDarkAccent,
  borderBottom: `5px solid white`,
  transition: "250ms ease",
  ":hover": {
    color: Colors.pink
  },
  cursor: "pointer"
});

let activeTab = css(inactiveTab, {
  color: Colors.pink,
  borderBottom: `5px solid ${Colors.pink}`
});

let centered = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "center"
});
