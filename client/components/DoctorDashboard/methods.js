import AppointmentService from "../../utils/appointmentService.js";

const Appointment = new AppointmentService();

export default class Methods {
  async fetchQueuedPatients(date = null, active = true) {
    let qry;

    if (date) {
      qry = { active, apmt_datetime: date };
    } else {
      qry = { active };
    }

    return await Appointment.getAppointmentsList(qry)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      }); // you would show/hide error messages with component state here
  }

  //   async _getAnswers(userid) {
  //     return await Answer.getAnswerList({ column: 'question_id', operator: 'gt', value: 0 }, userid)
  //           .then((res) => Promise.resolve(res))
  //           .catch((err) => {
  //             console.log(err);
  //           });
  //   }
}
