import { css } from "glamor";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer
} from "recharts";
import { confirmAlert } from "react-confirm-alert"; // Import
import moment from "moment-timezone";
import ConsultsService from "../../../utils/consultsService";
import { Legends } from "../../NewSkin/Colors";
import GridContainer from "../../NewSkin/Wrappers/GridContainer";
import NoData from "../../NewSkin/Components/NoData";
import Card from '../../NewSkin/Wrappers/Card';
import { labelClass, titleWbackground, inputClass } from '../../NewSkin/Styles';
import Button from '../../NewSkin/Components/Button'

const Consults = new ConsultsService();
const PatientStatus = [
  {id: "improving", value: "Improving"},
  {id: "notImproving", value: "Not Improving"},
  {id: "forImmediateFollowUp", value: "For Immediate follow up"},
  {id: "followUpNextMonth", value: "Follow up Next Month"},
  {id: "stalledOrNeedsRecovery", value: "Stalled or needs Recovery"} 
]

const data1 = [
  {
    consult_date: "2018.1-Jan_2018",
    consult_month_year: "Jan 2018",
    month_only: "Jan",
    improving: 10,
    notImproving: 10,
    forImmediateFollowUp: 100,
    followUpNextMonth: 0,
    stalledOrNeedsRecovery: 5
  },
  {
    consult_date: "2018.2-Feb_2018",
    consult_month_year: "Feb 2018",
    month_only: "Feb",
    improving: 12,
    notImproving: 30,
    forImmediateFollowUp: 50,
    followUpNextMonth: 5,
    stalledOrNeedsRecovery: 6
  },
  {
    consult_date: "2018.3-Mar_2018",
    consult_month_year: "Mar 2018",
    month_only: "Mar",
    improving: 15,
    notImproving: 1,
    forImmediateFollowUp: 25,
    followUpNextMonth: 7,
    stalledOrNeedsRecovery: 50
  },
  {
    consult_date: "2018.4-Apr_2018",
    consult_month_year: "Apr 2018",
    month_only: "Apr",
    improving: 5,
    notImproving: 15,
    forImmediateFollowUp: 10,
    followUpNextMonth: 20,
    stalledOrNeedsRecovery: 1
  },
  {
    consult_date: "2018.5-May_2018",
    consult_month_year: "May 2018",
    month_only: "May",
    improving: 10,
    notImproving: 10,
    forImmediateFollowUp: 100,
    followUpNextMonth: 0,
    stalledOrNeedsRecovery: 5
  },
  {
    consult_date: "2018.6-Jun_2018",
    consult_month_year: "June 2018",
    month_only: "Jun",
    improving: 12,
    notImproving: 30,
    forImmediateFollowUp: 50,
    followUpNextMonth: 5,
    stalledOrNeedsRecovery: 6
  },
  {
    consult_date: "2018.7-Jul_2018",
    consult_month_year: "Jul 2018",
    month_only: "Jul",
    improving: 15,
    notImproving: 1,
    forImmediateFollowUp: 25,
    followUpNextMonth: 7,
    stalledOrNeedsRecovery: 50
  },
  {
    consult_date: "2018.8-Aug_2018",
    consult_month_year: "Aug 2018",
    month_only: "Aug",
    improving: 5,
    notImproving: 15,
    forImmediateFollowUp: 10,
    followUpNextMonth: 20,
    stalledOrNeedsRecovery: 1
  },
  {
    consult_date: "2018.9-Sep_2018",
    consult_month_year: "Sep 2018",
    month_only: "Sep",
    improving: 10,
    notImproving: 10,
    forImmediateFollowUp: 100,
    followUpNextMonth: 0,
    stalledOrNeedsRecovery: 5
  },
  {
    consult_date: "2018.10-Oct_2018",
    consult_month_year: "Oct 2018",
    month_only: "Oct",
    improving: 12,
    notImproving: 30,
    forImmediateFollowUp: 50,
    followUpNextMonth: 5,
    stalledOrNeedsRecovery: 6
  },
  {
    consult_date: "2018.11-Nov_2018",
    consult_month_year: "Nov 2018",
    month_only: "Nov",
    improving: 15,
    notImproving: 1,
    forImmediateFollowUp: 25,
    followUpNextMonth: 7,
    stalledOrNeedsRecovery: 50
  },
  {
    consult_date: "2018.12-Dec_2018",
    consult_month_year: "Dec 2018",
    month_only: "Dec",
    improving: 5,
    notImproving: 15,
    forImmediateFollowUp: 10,
    followUpNextMonth: 20,
    stalledOrNeedsRecovery: 1
  }
];

const CustomizedAxisTick = props => {
  const { x, y, stroke, payload } = props;
  let title = payload.value;
  return (
    <g transform={`translate(${x},${y})`}>
      <text
        x={0}
        y={0}
        dy={10}
        textAnchor="end"
        transform="rotate(-45)"
        fill="#666"
        style={{fontWeight: 500, fontSize: 15}}
      >
        {title}
      </text>
    </g>
  );
};

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.user = this.props.user;
    this.state = {
      start: {
        m: moment().month(),
        y: moment().subtract(1, "years").year()
      },
      end: {
        m: moment().month(),
        y: moment().year()
      },
      allConsults: [],
      lgData: [],
      analytics_title: `${moment().year(moment().subtract(1, "years").year()).month(moment().month()).format("MMMM YYYY")} to ${moment().year(moment().year()).month(moment().month()).format("MMMM YYYY")}`
    };
  }

  componentDidMount() {
    this._getConsults();
  }

  handleChange = (e, range, val) => {
    const state_range = this.state[range]
    state_range[val] = e.currentTarget.value
    this.setState(state_range)
  };

  _getConsults() {
    let obj = { doctor_id: this.user.id };
    Consults.getAnalyticsData(obj)
      .then(res => {
        if (res) {
          this.setState(
            {
              allConsults: res
            },
            () => {
              this._processHealthOutcomes();
            }
          );
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _processHealthOutcomes() {
    let consults = this.state.allConsults;
    let start = moment().year(this.state.start.y).month(this.state.start.m);
    let end = moment().year(this.state.end.y).month(this.state.end.m);

    let lgData = this.state.lgData;
    lgData = [];

    consults.filter((item) => {
      let tmp_consult_date = (item.consult_date).split("-")
      let tmp_consult_date_1 = tmp_consult_date[0].split(".")
      // subtract 1 from the month returned from db since js month starts with 0
      let consult_date = moment().year(parseInt(tmp_consult_date_1[0])).month(parseInt(tmp_consult_date_1[1])-1)

      if(moment(consult_date).isBetween(moment(start).startOf('month'), moment(end).endOf('month'))){
        item.consult_month_year = moment(consult_date).format("MMM YYYY")
        item.month_only = item.consult_month_year.substr(0, 3)
        lgData.push(item)
      }
    })
    this.setState({ lgData });
  }

  render() {
    return this._analytics();
  }

  _analytics() {
    let data = this.state.lgData;
    return <div style={{ width: "100%" }}>{this._lineChart(data)}</div>;
  }

  showTooltipData = (data) => {
    if (typeof data.payload[0] !== 'undefined') {
      let mo_year = data.payload[0].payload.consult_month_year
      return (
        <Card styles={{fontSize: 15, width: 300, padding: 0, margin: 0, borderTopLeftRadius: 5, borderTopRightRadius: 5, overflow: 'hidden'}}>
          <span style={{textTransform: 'uppercase', padding: 5, background: '#364563', display: 'block', textAlign: 'center', color: 'white', fontSize: 15}}>{mo_year}</span>
          <div style={{margin: '10px 30px'}}>
          {
            PatientStatus.map((item) => 
              <div key={item.id} className={labelClass} style={{ color: Legends[item.id], margin: 0, padding: '2px 0'}}>
              {item.value}: {data.payload[0].payload[item.id]}</div>
            )
          }
          </div>
        </Card>
      );
    }
  }

  reloadGraph = () => {
    let start = moment().year(this.state.start.y).month(this.state.start.m)
    let end = moment().year(this.state.end.y).month(this.state.end.m)
    if (moment(end).isBefore(start)) {
      confirmAlert({
        closeOnEscape: false,
        closeOnClickOutside: false,
        title: "Invalid date range", // Title dialog
        message:
          `Start date: ${moment(start).format("ll")} - End date: ${moment(end).format("ll")}`, // Message dialog
        buttons: [
          {
            label: "Okay"
          }
        ]
      });
      return false;
    } else {
      let months = end.diff(start, "months");
      if (months > 12) {
        confirmAlert({
          closeOnEscape: false,
          closeOnClickOutside: false,
          title: "Invalid date range", // Title dialog
          message:
            "The system only allows maximum of 12-month range.", // Message dialog
          buttons: [
            {
              label: "Okay"
            }
          ]
        });
        return false;
      }
    }  
    this.setState({
      analytics_title: `${start.format("MMMM YYYY")} to ${end.format("MMMM YYYY")}`
    })
    this._processHealthOutcomes()
  }

  _lineChart(data) {
    const xmonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

    const numberOfYears = 10
    let d = new Date()
    let currYear = d.getFullYear()
    let years = []

    let xyears = () => {
      for (var x = 0; x < numberOfYears; x++) {
        years.push(currYear - x)
      }
      return years
    }

    let yearOptions = xyears().map((item, i) => {
      return <option key={i} value={item}>{item}</option>
    })

    let monthOptions = xmonths.map((item, i) => {
      return <option key={i} value={i}>{item}</option>
    })

    let ydd = (range) => {
      return (
        <select className={inputClass} value={this.state[range]['y']} onChange={(e) => this.handleChange(e, range, 'y')}>
          <option value='' disabled>Year</option>
          {yearOptions}
        </select>
      )
    }

    let mdd = (range) => {
      return (<select className={inputClass} value={this.state[range]['m']} onChange={(e) => this.handleChange(e, range, 'm')}>
        <option value='' disabled>Month</option>
        {monthOptions}
      </select>
      )
    }
    return (
      <div className={analyticsDiv}>
        <div className={legendDiv} style={{marginBottom: 10}}>
          <label className={titleWbackground}>
            {this.state.analytics_title}
          </label>
        </div>
        <GridContainer columnSize={'1fr 1fr 1fr'} gap={20}>
          <GridContainer columnSize={'.4fr 1fr 1fr'} gap={5}>
            <div style={{margin: 'auto'}}>Start</div>
            <div>{ydd("start")}</div>
            <div>{mdd("start")}</div>
          </GridContainer>
          <GridContainer columnSize={'.4fr 1fr 1fr'} gap={5}>
            <div style={{margin: 'auto'}}>End</div>
            <div>{ydd("end")}</div>
            <div>{mdd("end")}</div>
          </GridContainer>
          <Button>
            <button onClick={this.reloadGraph}>Reload Graph</button>
          </Button>
        </GridContainer>
        <GridContainer styles={{ width: '100%' }}>
          <section >
            <div style={{ width: "100%", display: "flex", height: 300 }}>
              <div style={{ flex: 1, display: "flex", justifyContent: "center" }}>
                {data.length > 0 ? (
                  <ResponsiveContainer height={280} width="100%" >
                    <LineChart
                      height={300}
                      data={data}
                      margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
                    >
                      <YAxis allowDecimals={false} />
                      <CartesianGrid strokeDasharray="3 3" />
                      <Tooltip content={this.showTooltipData} />
                      <XAxis dataKey="month_only"  tick={<CustomizedAxisTick />} minTickGap={-2} />
                      {
                      PatientStatus.map((item, i) => <Line
                          key={i}
                          type="monotone"
                          dataKey={item.id}
                          name={item.value}
                          stroke={Legends[item.id]}
                        />
                      )
                      }
                    </LineChart>
                  </ResponsiveContainer>
                ) : null}
              </div>
            </div>

            <div className={legendDiv} style={{ marginTop: 15 }}>
              {data.length > 0 ? (
                this._counts(data)
              ) : (
                  <NoData />
                )}
            </div>
          </section>
        </GridContainer>
      </div>
    );
  }

  _drawCircle(color) {
    return (
      <svg height="21" width="25">
        <circle cx="11" cy="11" r="6" stroke="black" stroke="0" fill={color} />
      </svg>
    );
  }

  _counts(data) {
    let html = data.map((d, i) => {
      let monthLabel = d.consult_month_year;

      return (
        <div key={i} style={{ flex: 1 }}>
          <div className={legendColor} style={{fontSize: 16, fontWeight: 500, height: 61, justifyContent: "center"}}>
            {monthLabel}
          </div>
          {
          PatientStatus.map((item, i) => 
            <div key={i}
              className={legendColor}
              style={{ justifyContent: "center", background: Legends[item.id] }}
            >
              {d[item.id]}
            </div>
          )
          }
        </div>
      );
    });

    return (
      <div style={{ width: "100%", display: "flex" }}>
        {
          <div style={{ width: 260, fontSize: 16 }}>
            <div className={legendColor} style={{height: 61}}>&nbsp;</div>
            <div className={legendColor}>
              {this._drawCircle(Legends.improving)}Improving
            </div>
            <div className={legendColor}>
              {this._drawCircle(Legends.notImproving)}Not Improving
            </div>
            <div className={legendColor}>
              {this._drawCircle(Legends.forImmediateFollowUp)}For Immediate follow up
            </div>
            <div className={legendColor}>
              {this._drawCircle(Legends.followUpNextMonth)}Follow up Next Month
            </div>
            <div className={legendColor}>
              {this._drawCircle(Legends.stalledOrNeedsRecovery)}Stalled or needs Recovery
            </div>
          </div>
        }
        {html}
      </div>
    );
  }
}

let analyticsDiv = css({
  width: "100%",
  display: "flex",
  flexWrap: "wrap",
  // padding: "10px",
  justifyContent: "flex-start",
  alignItems: "flex-start"
  //  border: '1px solid'
});

let legendDiv = css({
  display: "flex",
  flexWrap: "wrap",
  width: "100%",
  justifyContent: "flex-start"
  //  border: '1px solid'
});

let legendColor = css({
  display: "flex",
  justifyContent: "flex-start",
  alignItems: "center",
  flex: "1 0 100%",
  padding: 3,
  borderBottom: "1px solid #ccc"
});