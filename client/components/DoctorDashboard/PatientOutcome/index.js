import { css } from 'glamor'
import { PieChart, Pie, ResponsiveContainer, Cell, Tooltip } from 'recharts';
import moment from 'moment-timezone'
import Utiliy from '../../../utils/utility'
import Analytics from './analytics'
import DIAGNOSTICS from '../../Consultations/diagnostics';
import Card from '../../NewSkin/Wrappers/Card';
import GridContainer from '../../NewSkin/Wrappers/GridContainer';
import { inputClass, labelClass } from '../../NewSkin/Styles';
import { Legends } from '../../NewSkin/Colors';
import NoData from '../../NewSkin/Components/NoData';

export default class extends React.Component {
  constructor(props) {
    super(props)
    this.user = this.props.user
    this.state = {
      prevConsultData: [],
      chartData: [],
      companies: [],
      allConsults: [],
      selectedCompany: 'All',
      fields: [],
      companyFld: {
        id: '',
        name: 'company',
        question: 'Choose Company',
        type: 'select',
        disabled: false,
        validation: '',
        options: [{ 'one': 1, 'two': 2 }],
        error: {
          active: false,
          message: 'Please select a department'
        },
        value: ''
      }
    }
    this._renderChart = this._renderChart.bind(this)
    this._updateFields = this._updateFields.bind(this)
    this._goToNext = this._goToNext.bind(this)
    this._getCompanies = this._getCompanies.bind(this)
  }

  componentDidMount() {
    this._getCompanies()
    this._getConsults()
  }


  _goToNext() {
  }


  _drawCircle(color) {
    return (
      <svg height="21" width="25">
        <circle cx="11" cy="11" r="6" stroke="black" stroke="0" fill={color} />
      </svg>
    )
  }

  _renderChart() {

    const data = this.state.chartData

    let valueCtr = 0
    data.forEach(item => {
      if (item.value > 0) valueCtr++
    })

    const COLORS = [Legends.improving, Legends.notImproving, Legends.forImmediateFollowUp, Legends.followUpNextMonth, Legends.stalledOrNeedsRecovery];

    const RADIAN = Math.PI / 180;
    const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index, value }) => {
      const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
      const x = cx + radius * Math.cos(-midAngle * RADIAN);
      const y = cy + radius * Math.sin(-midAngle * RADIAN);

      //displays only labels with >0%
      if(percent > 0){
        return (
          <text x={x+10} y={y+10} fill="white" style={{fontSize: '.8rem'}} textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="bottom">
            {`${(percent * 100).toFixed(0)}% (${value})`}
          </text>
        );
      }else{
        return (
          <text></text>
        )
      }
    };

    return (
      <GridContainer styles={{ width: '100%' }}>
        <section>
          {valueCtr > 0 ? 
            <PieChart width={600} height={300} onMouseEnter={this.onPieEnter}>

              <Pie
                dataKey={'value'}
                data={data}
                labelLine={false}
                label={renderCustomizedLabel}
                outerRadius={130}
                isAnimationActive={false}
                // fill="#8884d8"
              >
                {
                  data.map((entry, index) => <Cell key={index} fill={COLORS[index % COLORS.length]} />)
                }

              </Pie>
              <Tooltip content={this.showTooltipData} position={{ x: 580, y: -40 }} />
            </PieChart>
          :
            <NoData />
          }
        </section>

        <section style={{ borderTop: '1px solid #ccc5', paddingTop: 10 }}>
          <GridContainer columnSize={'1fr 1.3fr 1.8fr 1.7fr 1.7fr'} styles={{ fontSize: 14, maxWidth: 1080, width: '100%', margin: '0 auto' }}>
            <div className={legendColor}>{this._drawCircle(Legends.improving)}Improving</div>
            <div className={legendColor}>{this._drawCircle(Legends.notImproving)}Not Improving</div>
            <div className={legendColor}>{this._drawCircle(Legends.forImmediateFollowUp)}For Immediate follow up</div>
            <div className={legendColor}>{this._drawCircle(Legends.followUpNextMonth)}Follow up Next Month</div>
            <div className={legendColor}>{this._drawCircle(Legends.stalledOrNeedsRecovery)}Stalled or needs Recovery</div>
          </GridContainer>
        </section>

      </GridContainer>
    )
  }

  showTooltipData = (data) => {
    if (typeof data.payload[0] !== 'undefined') {
      let prev_outcome = ''
      let last_consult_date = ''

      let details = data.payload[0].payload.details


      let html = details.map((i, x) => {
        let prevConsultData = this.state.prevConsultData;
        let outcome = '';

        if (prevConsultData.length > 0) {
          let index = prevConsultData.findIndex(d => d.patient_id == i.patient_id);
          prev_outcome = (index >= 0 && this.state.prevConsultData[index].details) ? this.state.prevConsultData[index].details.healthoutcome_details : null
          last_consult_date = (index >= 0 && this.state.prevConsultData[index].consult_date) ? this.state.prevConsultData[index].consult_date : null

          let indx = DIAGNOSTICS.ITEMS.healthoutcome.findIndex(x => x.value == prev_outcome);
          outcome = indx >=0 ? DIAGNOSTICS.ITEMS.healthoutcome[indx].label : null
        }


        let age = moment().diff(i.birth_date, 'years');
        return (
          <div className={detailItem} key={x}>
            <div className={itemStyle} style={{ flex: 2 }}>{i.employee_id}</div>
            <div className={itemStyle} style={{ flex: 4, textTransform: 'capitalize' }}>{i.name.toLowerCase()}</div>
            <div className={itemStyle} style={{ flex: 1.5 }}>{age}/{i.gender.charAt(0)}</div>

            {outcome !== null ?
              <div className={itemStyle} style={{ flex: 4 }}>{outcome} ({moment(last_consult_date).format('MMM DD, YYYY')})</div>
              : <div className={itemStyle} style={{ flex: 4, textAlign: 'center' }}>No Previous Consult</div>}
          </div>
        )
      })

      return (
        <Card styles={{fontSize: 15, minWidth: 660, maxWidth: 660, width: 660, padding: 0, margin: 0, borderTopLeftRadius: 5, borderTopRightRadius: 5, overflowY: 'auto', maxHeight: 300}}>
          <span style={{textTransform: 'uppercase', padding: 5, background: '#364563', display: 'block', textAlign: 'center', color: 'white', fontSize: 15}}>{data.payload[0].payload.name}</span>
          <div className={detailItem} style={{ background: data.payload[0].payload.fill, margin: '0 0 2px', color: 'white' }}>
            <div className={headerStyle} style={{ flex: 2 }}>Employee ID</div>
            <div className={headerStyle} style={{ flex: 4 }}>Name</div>
            <div className={headerStyle} style={{ flex: 1.5 }}>Age/Sex</div>

            <div className={headerStyle} style={{ flex: 4, textAlign: 'center' }}>Previous Outcome</div>
          </div>

          {html}

        </Card>
      );
    }
  }

  _getConsults() {
    let obj = { doctor_id: this.user.id, status: 1}
    this.props.getConsults(obj)
      .then((res) => {
        if (res) {
          this.setState({
            allConsults: res
          }, () => { this._processHealthOutcomes() })

        }
      })
  }

  _processHealthOutcomes() {
    let consults = this.state.allConsults
    let consultsGroupByCompany = Utiliy.groupBy(consults, 'company');
    let companies = this.state.companies

    let improvingCtr = 0
    let notImprovingCtr = 0
    let forImmediateFollowUpCtr = 0
    let followUpNextMonthCtr = 0
    let stalledOrNeedsRecoveryCtr = 0

    let improvingCtrDtls = []
    let notImprovingCtrDtls = []
    let forImmediateFollowUpCtrDtls = []
    let followUpNextMonthCtrDtls = []
    let stalledOrNeedsRecoveryCtrDtls = []

    companies = []
    //  let lgData = this.state.lgData
    let prevConsultData = this.state.prevConsultData

    consultsGroupByCompany.forEach((item, i) => {

      let itemGroupByUser = Utiliy.groupBy(item, 'patient_id');

      companies.push({ 'company_name': item[0].company, 'consults': [] })


      itemGroupByUser.forEach((o, x) => {
        if (o[1]) {
          prevConsultData.push(o[1])
        }

        // DATA FOR PIE CHART
        if (o[0].company === this.state.selectedCompany) {
          if (o[0].details.healthoutcome_details === 'improving') {
            improvingCtr++
            improvingCtrDtls.push(o[0])
          } else if (o[0].details.healthoutcome_details === 'notImproving') {
            notImprovingCtr++
            notImprovingCtrDtls.push(o[0])
          } else if (o[0].details.healthoutcome_details === 'forImmediateFollowUp') {
            forImmediateFollowUpCtr++
            forImmediateFollowUpCtrDtls.push(o[0])
          } else if (o[0].details.healthoutcome_details === 'followUpNextMonth') {
            followUpNextMonthCtr++
            followUpNextMonthCtrDtls.push(o[0])
          } else if (o[0].details.healthoutcome_details === 'stalledOrNeedsRecovery') {
            stalledOrNeedsRecoveryCtr++
            stalledOrNeedsRecoveryCtrDtls.push(o[0])
          }
        } else if (this.state.selectedCompany === 'All') {
          if (o[0].details.healthoutcome_details === 'improving') {
            improvingCtrDtls.push(o[0])
            improvingCtr++
          } else if (o[0].details.healthoutcome_details === 'notImproving') {
            notImprovingCtr++
            notImprovingCtrDtls.push(o[0])
          } else if (o[0].details.healthoutcome_details === 'forImmediateFollowUp') {
            forImmediateFollowUpCtr++
            forImmediateFollowUpCtrDtls.push(o[0])
          } else if (o[0].details.healthoutcome_details === 'followUpNextMonth') {
            followUpNextMonthCtr++
            followUpNextMonthCtrDtls.push(o[0])
          } else if (o[0].details.healthoutcome_details === 'stalledOrNeedsRecovery') {
            stalledOrNeedsRecoveryCtr++
            stalledOrNeedsRecoveryCtrDtls.push(o[0])
          }
        }
      })
    }) //consultsGroupByCompany end

    this.setState({
      prevConsultData,
      companies,
      chartData: [
        { name: 'Improving', value: improvingCtr, details: improvingCtrDtls },
        { name: 'Not Improving', value: notImprovingCtr, details: notImprovingCtrDtls },
        { name: 'For Immediate follow up', value: forImmediateFollowUpCtr, details: forImmediateFollowUpCtrDtls },
        { name: 'Follow up Next Month', value: followUpNextMonthCtr, details: followUpNextMonthCtrDtls },
        { name: 'Stalled or needs Recovery', value: stalledOrNeedsRecoveryCtr, details: stalledOrNeedsRecoveryCtrDtls }
      ]
    }
    // , () => { this._updateFields() }
    )

  }

  _getCompanies() {
    this.props.getCompanies()
      .then((res) => {

        if (res.name === 'Error') {
          console.log(res)
        } else {
          if (res.length > 0) {
            let companies_assigned_to_me = res.filter(company => (company.isassignedtome))
            this.setState({
              companies: companies_assigned_to_me
            })
            this._updateFields()
          }

        }
      })
      .catch((e) => {
        console.log(e)
      })

  }

  _updateFields() {
    let currentFields = [this.state.companyFld]
    let companyFld = this.state.companyFld
    let fields = []
    var companyList = [];
    for (let i = 0; i < this.state.companies.length; i++) {
      companyList.push({ label: this.state.companies[i].company_name, value: this.state.companies[i].company_name })
    }

    companyFld.options = companyList

    for (let i = 0; i < currentFields.length; i++) {
      let input = currentFields[i];
      fields.push(input)
    }
    this.setState({
      fields: fields
    })
  }

  _handleChange(e) {
    this.setState({
      selectedCompany: e.target.value
    }, () => this._processHealthOutcomes())
  }

  _renderfields() {
    let flds = this.state.fields.map((item, i) => {
      if (item.type === 'select')
        return (
          <div key={i}>
            <GridContainer gap={10} columnSize={'2fr 4fr'} styles={{ textAlign: 'left', width: '45%' }}>
              <label style={{margin: 'auto'}}
                className={labelClass}
              >{item.question}</label>
              <select
                value={this.state.selectedCompany}
                style={{ textAlign: 'left', cursor: 'pointer' }}
                className={inputClass}
                onChange={this._handleChange.bind(this)}
              >
                {this._displayListOptions(item.options)}
              </select>
            </GridContainer>
          </div>
        )
    })

    return flds
  }

  _displayListOptions(input) {

    var html = [];
    let options = input
    html.push(
      <option key='a'>All</option>
    )
    for (var i = 0; i < options.length; i++) {
      if (options[i].label === input.value) {
        html.push(
          <option key={i} value={input.value}>{options[i].label}</option>
        )
      } else {
        html.push(
          <option key={i} value={options[i].label}>{options[i].label}</option>
        )
      }
    }

    return html
  }

  _renderForm() {
    return (
      <form style={{ width: '100%' }}>
        {this._renderfields()}
      </form>
    )
  }

  render() {
    return (
      <GridContainer gap={10}>
        <Card styles={{ paddingBottom: 10 }}>
          {this._renderForm()}
          {this._renderChart()}
        </Card>

        <label style={{ marginTop: 20, marginLeft: 30 }} className={labelClass}>Analytics</label>

        <Card>
          {<Analytics user={this.user} />}
        </Card>
      </GridContainer>
    )
  }
}

let chartDiv = css({
  width: '100%',
  display: 'flex',
  flexWrap: 'wrap',
  padding: '10px',
  justifyContent: 'flex-start',
  alignItems: 'flex-start',
  //border: '1px solid'
})

let legendDiv = css({
  display: 'flex',
  flexWrap: 'wrap',
  width: '100%',
  justifyContent: 'flex-start',
  //border: '1px solid'
})

let legendColor = css({
  display: 'flex',
  justifyContent: 'flex-start',
  alignItems: 'center',
  flex: '1 0 100%',
  // padding: '5px'
})

let pieDiv = css({
  display: 'flex',
  justifyContent: 'flex-start',
  width: '50%'
})

let noDataDiv = css({
  display: 'flex',
  color: '#ccc',
  fontSize: 16,
  flex: 1,
  textTransform: 'capitalize',
  height: '100px',
  justifyContent: 'center',
  alignItems: 'center'
})

let circle = css({
  width: '10px',
  height: '10px',
  paddingRight: 15,
  borderRadius: '5px',
  background: 'red'
})

let labelStyle = css({
  width: '200px',
  background: '#1b75bb',
  color: 'white',
  textAlign: 'center',
  padding: '5px 10px 5px 100px'
})

let inputStyle = css({
  width: '200px',
  textAlign: 'center',
  padding: '5px 10px 5px 30px',
  marginLeft: 5
})

let labelDivStyle = css({
  display: 'flex',
  flexWrap: 'wrap',
  flex: '1 0 100%',
  flexContent: 'flex-start',
  margin: '20px 0 30px 5px',
  padding: '5px'
})

let detailsDiv = css({
  border: '1px solid gray',
  background: 'gray',
  borderRadius: 10,
  display: 'flex',
  width: '600px',
  flexWrap: 'wrap',
  justifyContent: 'flex-start',
  background: 'white',
  padding: 10,
  fontSize: 'x-small'

})

let detailItem = css({
  display: 'flex',
  width: '100%',
  color: '#000',
  borderTop: '1px solid #ccc'
})

let headerStyle = css({
  flex: '1',
  fontWeight: '500',
  textTransform: 'uppercase',
  fontSize: 13
})


let itemStyle = css({
  flex: '1',
  margin: '3px 10px',
  color: '#000',
  fontSize: 14
})
