import React from "react";
import { css } from "glamor";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      display: {
        weight: true,
        sleep: true,
        movement: true,
        stress: true,
        detox: true,
        foodchoices: true,
        msqTopAffectedSystems: true,
        prevalentSymptoms: true
      },
      table1data_raw: [],
      table2data_raw: [],
      table3data_raw: [],
      table4data_raw: [],
      table5data_raw: [],
      mean_data: {
        weight: 0,
        height: 0,
        bmi: 0,
        waist: 0,
        hip: 0,
        whr: 0,
        stress: 0
      },
      median_data: {
        weight: 0,
        height: 0,
        bmi: 0,
        waist: 0,
        hip: 0,
        whr: 0,
        stress: 0
      },
      weight_cnt: 0,
      height_cnt: 0,
      bmi_cnt: 0,
      waist_cnt: 0,
      hip_cnt: 0,
      whr_cnt: 0,
      sleep_cnt_6_to_8: 0,
      sleep_cnt_more_than_10: 0,
      sleep_cnt_8_to_10: 0,
      sleep_cnt_less_than_6: 0,
      stress_cnt: 0,
      bmi_cnt_below: 0,
      bmi_cnt_avg: 0,
      bmi_cnt_above: 0,
      bmi_cnt_further: 0,
      total_tobaccoExposure: 0,
      total_tobaccoExposure_respondents: 0,
      total_caffeineexposure: 0,
      total_caffeineexposure_respondents: 0,
      total_chemical: 0,
      total_chemical_respondents: 0,
      total_exercise: 0,
      total_exercise_respondents: 0,
      total_sleeping: 0,
      total_sleeping_respondents: 0,
      msqTopAffectedSystems: [],
      stressSource: [],
      prevalentSymptoms: [],
      foodchoices: [],
      foodchoices_total_respondents: 0,
      msqTopAffectedSystems_total_respondents: 0,
      stressSource_total_respondents: 0,
      prevalentSymptoms_total_respondents: 0,
      activeFilter: {
        filter: "",
        value: ""
      }
    };
  }

  componentWillMount() {
    this._getUpstreamData();
    this._getFoodChoices();
    this._getStressSourceRank();
    this._getMSQSystemRank();
    this._getPrevalentSymptoms();

    if (this.props.activeFilter.selections) {
      const selections = this.props.activeFilter.selections;
      const display = this.state.display;
      display.weight = selections.weight;
      display.sleep = selections.sleep;
      display.movement = selections.movement;
      display.stress = selections.stress;
      display.detox = selections.detox;
      display.foodchoices = selections.foodchoices;
      display.msqTopAffectedSystems = selections.msqTopAffectedSystems;
      display.prevalentSymptoms = selections.prevalentSymptoms;

      this.setState({
        display
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.state.activeFilter.filters !== nextProps.activeFilter.filters ||
      this.state.activeFilter.value !== nextProps.activeFilter.value
    ) {
      // update filters
      const activeFilter = this.state.activeFilter;
      activeFilter.filter = nextProps.activeFilter.filter;
      activeFilter.value = nextProps.activeFilter.value;
      activeFilter.filters = nextProps.activeFilter.filters;

      this.setState({ activeFilter }, function() {
        this._getFilteredData();
      });
    }

    if (this.props.activeFilter.selections) {
      const selections = this.props.activeFilter.selections;
      const display = this.state.display;
      display.weight = selections.weight;
      display.sleep = selections.sleep;
      display.movement = selections.movement;
      display.stress = selections.stress;
      display.detox = selections.detox;
      display.foodchoices = selections.foodchoices;
      display.msqTopAffectedSystems = selections.msqTopAffectedSystems;
      display.prevalentSymptoms = selections.prevalentSymptoms;

      this.setState({
        display
      });
    }
  }

  multiFilter(array, filters) {
    const filterKeys = Object.keys(filters);
    return array.filter(item =>
      filterKeys.every(key => !!~filters[key].indexOf(item[key]))
    );
  }

  _getFilteredData() {
    const activeFilter = this.state.activeFilter;
    const table1data_raw = this.state.table1data_raw;
    const table2data_raw = this.state.table2data_raw;
    const table3data_raw = this.state.table3data_raw;
    const table4data_raw = this.state.table4data_raw;
    const table5data_raw = this.state.table5data_raw;
    let filtered_table1 = table1data_raw;
    let filtered_table2 = table2data_raw;
    let filtered_table3 = table3data_raw;
    let filtered_table4 = table4data_raw;
    let filtered_table5 = table5data_raw;

    if (
      activeFilter.filters &&
      activeFilter.filters.branch &&
      activeFilter.filters.branch.length > 0
    ) {
      const filters = { branch: activeFilter.filters.branch };

      filtered_table1 = this.multiFilter(filtered_table1, filters);
      filtered_table2 = this.multiFilter(filtered_table2, filters);
      filtered_table3 = this.multiFilter(filtered_table3, filters);
      filtered_table4 = this.multiFilter(filtered_table4, filters);
      filtered_table5 = this.multiFilter(filtered_table5, filters);
    }

    if (
      activeFilter.filters &&
      activeFilter.filters.department &&
      activeFilter.filters.department.length > 0
    ) {
      const filters = { department: activeFilter.filters.department };

      filtered_table1 = this.multiFilter(filtered_table1, filters);
      filtered_table2 = this.multiFilter(filtered_table2, filters);
      filtered_table3 = this.multiFilter(filtered_table3, filters);
      filtered_table4 = this.multiFilter(filtered_table4, filters);
      filtered_table5 = this.multiFilter(filtered_table5, filters);
    }

    if (
      activeFilter.filters &&
      activeFilter.filters.jobgrade &&
      activeFilter.filters.jobgrade.length > 0
    ) {
      const filters = { job_grade: activeFilter.filters.jobgrade };

      filtered_table1 = this.multiFilter(filtered_table1, filters);
      filtered_table2 = this.multiFilter(filtered_table2, filters);
      filtered_table3 = this.multiFilter(filtered_table3, filters);
      filtered_table4 = this.multiFilter(filtered_table4, filters);
      filtered_table5 = this.multiFilter(filtered_table5, filters);
    }

    if (
      activeFilter.filters &&
      activeFilter.filters.high_risk &&
      activeFilter.filters.high_risk.length > 0
    ) {
      const high_risk_filter =
        activeFilter.filters.high_risk == "High Value"
          ? 1
          : activeFilter.filters.high_risk == "High Risk"
          ? 2
          : null;
      // filtered_table1 = filtered_table1.filter(str => {
      //   if (activeFilter.filters.high_risk != "Others") {
      //     return str.high_risk == high_risk_filter;
      //   }
      //   return str.high_risk == null || str.high_risk == 0;
      // });

      // filtered_table2 = filtered_table2.filter(str => {
      //   if (activeFilter.filters.high_risk != "Others") {
      //     return str.high_risk == high_risk_filter;
      //   }
      //   return str.high_risk == null || str.high_risk == 0;
      // });
      // filtered_table3 = filtered_table3.filter(str => {
      //   if (activeFilter.filters.high_risk != "Others") {
      //     return str.high_risk == high_risk_filter;
      //   }
      //   return str.high_risk == null || str.high_risk == 0;
      // });
      // filtered_table4 = filtered_table4.filter(str => {
      //   if (activeFilter.filters.high_risk != "Others") {
      //     return str.high_risk == high_risk_filter;
      //   }
      //   return str.high_risk == null || str.high_risk == 0;
      // });
      // filtered_table5 = filtered_table5.filter(str => {
      //   if (activeFilter.filters.high_risk != "Others") {
      //     return str.high_risk == high_risk_filter;
      //   }
      //   return str.high_risk == null || str.high_risk == 0;
      // });

      const filters = { high_risk: activeFilter.filters.high_risk };

      filtered_table1 = this.multiFilter(filtered_table1, filters);
      filtered_table2 = this.multiFilter(filtered_table2, filters);
      filtered_table3 = this.multiFilter(filtered_table3, filters);
      filtered_table4 = this.multiFilter(filtered_table4, filters);
      filtered_table5 = this.multiFilter(filtered_table5, filters);
    }

    if (
      activeFilter.filters &&
      activeFilter.filters.age_group &&
      activeFilter.filters.age_group.length > 0
    ) {
      const filters = { age_group: activeFilter.filters.age_group };

      filtered_table1 = this.multiFilter(filtered_table1, filters);
      filtered_table2 = this.multiFilter(filtered_table2, filters);
      filtered_table3 = this.multiFilter(filtered_table3, filters);
      filtered_table4 = this.multiFilter(filtered_table4, filters);
      filtered_table5 = this.multiFilter(filtered_table5, filters);
    }

    this._processTable1Data(filtered_table1);
    this._processTable2Data(filtered_table2);
    this._processTable3Data(filtered_table3);
    this._processTable4Data(filtered_table4);
    this._processTable5Data(filtered_table5);
  }

  getSortedKeys(obj) {
    const keys = [];
    for (const key in obj) keys.push(key);
    return keys.sort((a, b) => obj[b] - obj[a]);
  }

  _getFoodChoices() {
    this.props
      .foodchoices()
      .then(result => {
        const res = result.filter(d => d.healthsurvey_status === "Locked"); // FILTER, ONLY PATIENT SUBMITTED HEALTH SURVEY

        this.setState({ table2data_raw: res });
        this._processTable2Data(res);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _processTable2Data(res) {
    let usualMeal = [];
    res.forEach(data => {
      usualMeal.push(data.value);
    });
    usualMeal = usualMeal.join(",").split(",");
    //count duplicates
    const count = {};
    usualMeal.forEach(i => {
      count[i] = (count[i] || 0) + 1;
    });

    const sorted = this.getSortedKeys(count);
    const data = [];
    sorted.forEach(meal => {
      if (meal != "") {
        data.push({
          name: meal,
          total: count[meal]
        });
      }
    });
    this.setState({
      foodchoices: data,
      foodchoices_total_respondents: res.length
    });
  }

  _getStressSourceRank() {
    this.props
      .stressSourceRank()
      .then(result => {
        const res = result.filter(d => d.healthsurvey_status === "Locked"); // FILTER, ONLY PATIENT SUBMITTED HEALTH SURVEY

        this.setState({ table3data_raw: res });
        this._processTable3Data(res);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _processTable3Data(res) {
    const groups = {};
    const user_counts = {};
    let total_respondents = 0;

    for (let i = 0; i < res.length; i++) {
      //  count distinct users
      user_counts[res[i].user_id] = 1 + (user_counts[res[i].user_id] || 0);

      const groupName = res[i].label;
      if (!groups[groupName]) {
        groups[groupName] = [];
      }
      groups[groupName].push(parseFloat(res[i].total_score));
    }
    total_respondents = Object.keys(user_counts).length;

    res = {};
    let mean_score = 0;
    for (let groupName in groups) {
      mean_score = groups[groupName].reduce(
        (a, b) => parseFloat(a) + parseFloat(b)
      );
      res[groupName] = parseFloat(
        (parseFloat(mean_score) / total_respondents).toFixed(2)
      );
    }

    const sorted = this.getSortedKeys(res);
    const data = [];
    sorted.map(stress => {
      if (stress != "") {
        data.push({
          name: stress,
          total: res[stress]
        });
      }
    });

    this.setState({
      stressSource: data,
      stressSource_total_respondents: total_respondents
    });
  }

  _getMSQSystemRank() {
    this.props
      .msqSystemRank()
      .then(result => {
        const res = result.filter(d => d.healthsurvey_status === "Locked"); // FILTER, ONLY PATIENT SUBMITTED HEALTH SURVEY

        this.setState({ table4data_raw: res });
        this._processTable4Data(res);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _processTable4Data(res) {
    const groups_count = {};
    const groups = {};
    const user_counts = {};
    let total_respondents = 0;

    for (let i = 0; i < res.length; i++) {
      //  count distinct users
      user_counts[res[i].user_id] = 1 + (user_counts[res[i].user_id] || 0);

      var groupName = res[i].msq_system;
      if (!groups[groupName]) {
        groups[groupName] = [];
        groups_count[groupName] = [];
        groups_count[groupName]["with_10score"] = 0;
        groups_count[groupName]["with_score"] = 0;
        groups[groupName] = 0;
      }
      if (parseFloat(res[i].total_score) >= 10) {
        groups_count[groupName]["with_10score"]++;
      }
      if (parseFloat(res[i].total_score) > 0) {
        groups_count[groupName]["with_score"]++;
      }
      groups[groupName] += parseFloat(res[i].total_score);
    }
    total_respondents = Object.keys(user_counts).length;

    const sorted_groups = this.getSortedKeys(groups);
    const top10groups = sorted_groups.slice(0, 10);
    const data = [];
    top10groups.map(system => {
      if (system != "") {
        data.push({
          name: system,
          total_with_score: groups_count[system]["with_score"],
          total_with_10score: groups_count[system]["with_10score"]
        });
      }
    });
    this.setState({
      msqTopAffectedSystems: data,
      msqTopAffectedSystems_total_respondents: total_respondents
    });
  }

  _getPrevalentSymptoms() {
    this.props
      .prevalentSymptoms()
      .then(result => {
        const res = result.filter(d => d.healthsurvey_status === "Locked"); // FILTER, ONLY PATIENT SUBMITTED HEALTH SURVEY
        this.setState({ table5data_raw: res });
        this._processTable5Data(res);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _processTable5Data(res) {
    const groups = {};
    const user_counts = {};
    let total_respondents = 0;

    for (let i = 0; i < res.length; i++) {
      //  count distinct users
      user_counts[res[i].user_id] = 1 + (user_counts[res[i].user_id] || 0);

      var groupName = res[i].label;
      if (!groups[groupName]) {
        groups[groupName] = [];
      }
      let total_user_perSymptom = 0;
      if (parseFloat(res[i].value) > 0) {
        total_user_perSymptom++;
        groups[groupName].push(total_user_perSymptom);
      }
    }
    total_respondents = Object.keys(user_counts).length;

    res = {};
    let total = 0;
    for (var groupName in groups) {
      total =
        groups[groupName].length > 0
          ? groups[groupName].reduce((a, b) => parseFloat(a) + parseFloat(b))
          : 0;
      if (total > 0) {
        res[groupName] = total;
      }
    }

    const sorted = this.getSortedKeys(res);
    const top10groups = sorted.slice(0, 10);
    const data = [];
    top10groups.map(symptom => {
      if (symptom != "") {
        data.push({
          name: symptom,
          total: res[symptom]
        });
      }
    });
    this.setState({
      prevalentSymptoms: data,
      prevalentSymptoms_total_respondents: total_respondents
    });
  }

  _getUpstreamData() {
    this.props
      .data()
      .then(result => {
        const res = result.filter(d => d.healthsurvey_status === "Locked"); // FILTER, ONLY PATIENT SUBMITTED HEALTH SURVEY

        this.setState({ table1data_raw: res });
        this._processTable1Data(res);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _processTable1Data(res) {
    let total_weight = 0,
      weight_data = [];
    let total_height = 0,
      height_data = [];
    let total_bmi = 0,
      bmi_data = [];
    const bmiData_counts = [];
    let total_waist = 0,
      waist_data = [];
    let total_hip = 0,
      hip_data = [];
    let total_whr = 0,
      whr_data = [];
    let sleep_cnt_6_to_8 = 0,
      sleep_cnt_more_than_10 = 0,
      sleep_cnt_8_to_10 = 0,
      sleep_cnt_less_than_6 = 0;
    let total_stress = 0,
      stress_data = [];
    let bmi_cnt_below = 0,
      bmi_cnt_avg = 0,
      bmi_cnt_above = 0,
      bmi_cnt_further = 0;
    let total_tobaccoExposure = 0,
      total_tobaccoExposure_respondents = 0;
    let total_caffeineexposure = 0,
      total_caffeineexposure_respondents = 0;
    let total_chemical = 0,
      total_chemical_respondents = 0;
    let total_exercise = 0,
      total_exercise_respondents = 0;
    let total_sleeping = 0,
      total_sleeping_respondents = 0;
    let total_stress_counseling = 0,
      total_stress_counseling_respondents = 0;
    let total_stress_therapy = 0,
      total_stress_therapy_respondents = 0;

    res.map(value => {
      if (value.weight != null) {
        total_weight += parseFloat(value.weight);
        weight_data.push(parseFloat(value.weight));
      }
      if (value.height != null) {
        total_height += parseFloat(value.height);
        height_data.push(parseFloat(value.height));
      }
      if (value.bmi != null) {
        total_bmi += parseFloat(value.bmi);
        bmi_data.push(parseFloat(value.bmi));
        if (parseFloat(value.bmi) < 18.5) {
          bmi_cnt_below++;
        } else if (parseFloat(value.bmi) < 23) {
          bmi_cnt_avg++;
        } else if (parseFloat(value.bmi) < 27) {
          bmi_cnt_above++;
        } else {
          bmi_cnt_further++;
        }
      }
      if (value.waist != null) {
        total_waist += parseFloat(value.waist);
        waist_data.push(parseFloat(value.waist));
      }
      if (value.hip != null) {
        total_hip += parseFloat(value.hip);
        hip_data.push(parseFloat(value.hip));
      }
      if (value.whr != null) {
        total_whr += parseFloat(value.whr);
        whr_data.push(parseFloat(value.whr));
      }
      if (value.averagesleep != null) {
        if (value.averagesleep == ">10") {
          sleep_cnt_more_than_10++;
        } else if (value.averagesleep == "8-10") {
          sleep_cnt_8_to_10++;
        } else if (value.averagesleep == "6-8") {
          sleep_cnt_6_to_8++;
        } else if (value.averagesleep == "<6") {
          sleep_cnt_less_than_6++;
        }
      }
      if (value.overallstressscore != null) {
        total_stress += parseFloat(value.overallstressscore);
        stress_data.push(parseFloat(value.overallstressscore));
      }
      if (value.stress_counseling != null) {
        total_stress_counseling += value.stress_counseling == "Yes" ? 1 : 0;
        total_stress_counseling_respondents++;
      }
      if (value.stress_therapy != null) {
        total_stress_therapy += value.stress_therapy == "Yes" ? 1 : 0;
        total_stress_therapy_respondents++;
      }
      if (value.tobaccoexposure != null) {
        total_tobaccoExposure += value.tobaccoexposure == "Yes" ? 1 : 0;
        total_tobaccoExposure_respondents++;
      }
      if (value.caffeineexposure != null) {
        total_caffeineexposure +=
          value.caffeineexposure == "Don't Know" ? 1 : 0;
        total_caffeineexposure_respondents++;
      }
      if (value.chemicallydependent != null) {
        total_chemical += value.chemicallydependent == "Yes" ? 1 : 0;
        total_chemical_respondents++;
      }
      if (value.exercise150min != null) {
        total_exercise += value.exercise150min == "Yes" ? 1 : 0;
        total_exercise_respondents++;
      }
      if (value.usesleepingaids != null) {
        total_sleeping += value.usesleepingaids == "Yes" ? 1 : 0;
        total_sleeping_respondents++;
      }
    });

    this.setState({
      mean_data: {
        weight:
          total_weight > 0 ? (total_weight / weight_data.length).toFixed(2) : 0,
        height:
          total_height > 0 ? (total_height / height_data.length).toFixed(2) : 0,
        bmi: total_bmi > 0 ? (total_bmi / bmi_data.length).toFixed(2) : 0,
        waist:
          total_waist > 0 ? (total_waist / waist_data.length).toFixed(2) : 0,
        hip: total_hip > 0 ? (total_hip / hip_data.length).toFixed(2) : 0,
        whr: total_whr > 0 ? (total_whr / whr_data.length).toFixed(2) : 0,
        stress:
          total_stress > 0 ? (total_stress / stress_data.length).toFixed(2) : 0
      },
      median_data: {
        weight: weight_data.length > 0 ? this._getMedian(weight_data) : 0,
        height: height_data.length > 0 ? this._getMedian(height_data) : 0,
        bmi: bmi_data.length > 0 ? this._getMedian(bmi_data) : 0,
        waist: waist_data.length > 0 ? this._getMedian(waist_data) : 0,
        hip: hip_data.length > 0 ? this._getMedian(hip_data) : 0,
        whr: whr_data.length > 0 ? this._getMedian(whr_data) : 0,
        stress: stress_data.length > 0 ? this._getMedian(stress_data) : 0
      },
      weight_cnt: weight_data.length,
      height_cnt: height_data.length,
      bmi_cnt: bmi_data.length,
      waist_cnt: waist_data.length,
      hip_cnt: hip_data.length,
      whr_cnt: whr_data.length,
      sleep_cnt_6_to_8,
      sleep_cnt_more_than_10,
      sleep_cnt_8_to_10,
      sleep_cnt_less_than_6,
      stress_cnt: stress_data.length,
      bmi_cnt_below,
      bmi_cnt_avg,
      bmi_cnt_above,
      bmi_cnt_further,
      total_stress_counseling,
      total_stress_counseling_respondents,
      total_stress_therapy,
      total_stress_therapy_respondents,
      total_tobaccoExposure,
      total_tobaccoExposure_respondents,
      total_caffeineexposure,
      total_caffeineexposure_respondents,
      total_chemical,
      total_chemical_respondents,
      total_exercise,
      total_exercise_respondents,
      total_sleeping,
      total_sleeping_respondents
    });
  }

  _getMedian(data) {
    let median = 0,
      numsLen = data.length;
    data.sort((a, b) => a - b);

    if (numsLen % 2 === 0) {
      // is even
      // average of two middle numbers
      median = (data[numsLen / 2 - 1] + data[numsLen / 2]) / 2;
    } else {
      // is odd
      // middle number only
      median = data[(numsLen - 1) / 2];
    }
    return parseFloat(median).toFixed(2);
  }

  _renderTableData(data, type, empty_message) {
    if (data.length == 0) {
      return (
        <tbody>
          <tr>
            <td className={rank} colSpan="3">
              {empty_message}
            </td>
          </tr>
        </tbody>
      );
    }
    const row = data.map((item, i) => (
      <tr key={i}>
        <td className={rank}>#{i + 1}</td>
        <td>{item.name}</td>
        <td>
          {(() => {
            switch (type) {
              case "foodchoices":
                return `${
                  parseFloat(item.total) > 0
                    ? (
                        (parseFloat(item.total) /
                          this.state.foodchoices_total_respondents) *
                        100
                      ).toFixed(2)
                    : 0
                }%`;
              case "stressSource":
                return parseFloat(item.total) > 0
                  ? parseFloat(item.total).toFixed(2)
                  : 0;
              case "msqTopAffectedSystems":
                return `${
                  parseFloat(item.total_with_score) > 0
                    ? (
                        (parseFloat(item.total_with_score) /
                          this.state.msqTopAffectedSystems_total_respondents) *
                        100
                      ).toFixed(2)
                    : 0
                }% (n=${item.total_with_10score})`;
              case "prevalentSymptoms":
                return `${
                  parseFloat(item.total) > 0
                    ? (
                        (parseFloat(item.total) /
                          this.state.prevalentSymptoms_total_respondents) *
                        100
                      ).toFixed(2)
                    : 0
                }%`;
            }
          })()}
        </td>
      </tr>
    ));
    return <tbody>{row}</tbody>;
  }

  renderWeight(mean_data, median_data) {
    return (
      <table
        style={{ tableLayout: "auto" }}
        className={this.props.fromReport && reportTable}
      >
        <thead>
          <tr>
            <th>Weight Upstream Marker</th>
            <th>Mean</th>
            <th>Median</th>
            <th>Counts of Users</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={title}>Weight</td>
            <td>{mean_data.weight} lbs</td>
            <td>{median_data.weight} lbs</td>
            <td>{this.state.weight_cnt}</td>
          </tr>
          <tr>
            <td className={title}>Height</td>
            <td>{mean_data.height} in</td>
            <td>{median_data.height} in</td>
            <td>{this.state.height_cnt}</td>
          </tr>
          <tr>
            <td className={title}>BMI</td>
            <td>{mean_data.bmi}</td>
            <td>{median_data.bmi}</td>
            <td className={wideTd}>
              Below healthy average: <strong>{this.state.bmi_cnt_below}</strong>
              <br />
              Healthy average: <strong>{this.state.bmi_cnt_avg}</strong>
              <br />
              Above healthy average: <strong>{this.state.bmi_cnt_above}</strong>
              <br />
              Further above healthy average:{" "}
              <strong>{this.state.bmi_cnt_further}</strong>
              <br />
            </td>
          </tr>
          <tr>
            <td className={title}>Waist</td>
            <td>{mean_data.waist} in</td>
            <td>{median_data.waist} in</td>
            <td>{this.state.waist_cnt}</td>
          </tr>
          <tr>
            <td className={title}>Hip</td>
            <td>{mean_data.hip} in</td>
            <td>{median_data.hip} in</td>
            <td>{this.state.hip_cnt}</td>
          </tr>
          <tr>
            <td className={title}>WHR</td>
            <td>{mean_data.whr}</td>
            <td>{median_data.whr}</td>
            <td>{this.state.whr_cnt}</td>
          </tr>
        </tbody>
      </table>
    );
  }

  renderStress(mean_data, median_data) {
    return (
      <table
        style={{ tableLayout: "auto" }}
        className={this.props.fromReport && reportTable}
      >
        <thead>
          <tr>
            <th>Stress Upstream Marker</th>
            <th>Mean</th>
            <th>Median</th>
            <th>Counts of Users</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={title}>Overall Stress Score</td>
            <td>{mean_data.stress}</td>
            <td>{median_data.stress}</td>
            <td>{this.state.stress_cnt}</td>
          </tr>
          <tr>
            <td className={title}>
              Number of users who are undergoing therapy
            </td>
            <td className={empty}>&nbsp;</td>
            <td className={empty}>&nbsp;</td>
            <td>
              {this.state.total_stress_therapy_respondents > 0
                ? Math.round(
                    (this.state.total_stress_therapy /
                      this.state.total_stress_therapy_respondents) *
                      100
                  )
                : 0}
              % (n={this.state.total_stress_therapy_respondents})
            </td>
          </tr>
          <tr>
            <td className={title}>
              Number of users who are undergoing counseling
            </td>
            <td className={empty}>&nbsp;</td>
            <td className={empty}>&nbsp;</td>
            <td>
              {this.state.total_stress_counseling_respondents > 0
                ? Math.round(
                    (this.state.total_stress_counseling /
                      this.state.total_stress_counseling_respondents) *
                      100
                  )
                : 0}
              % (n={this.state.total_stress_counseling_respondents})
            </td>
          </tr>
        </tbody>
      </table>
    );
  }

  renderSleep() {
    return (
      <table
        style={{ tableLayout: "auto" }}
        className={this.props.fromReport && reportTable}
      >
        <thead>
          <tr>
            <th>Sleep Upstream Marker</th>
            <th>Counts of Users</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={title}># of Hours of Sleep</td>
            <td className={wideTd}>
              More than 10 hours :{" "}
              <strong>{this.state.sleep_cnt_more_than_10}</strong>
              <br />8 to 10 hours :{" "}
              <strong>{this.state.sleep_cnt_8_to_10}</strong>
              <br />6 to 8 hours :{" "}
              <strong>{this.state.sleep_cnt_6_to_8}</strong>
              <br />
              Less than 6 hours :{" "}
              <strong>{this.state.sleep_cnt_less_than_6}</strong>
            </td>
          </tr>
          <tr>
            <td className={title}>Uses sleeping aids</td>
            <td>
              {this.state.total_sleeping_respondents > 0
                ? Math.round(
                    (this.state.total_sleeping /
                      this.state.total_sleeping_respondents) *
                      100
                  )
                : 0}
              % (n={this.state.total_sleeping_respondents})
            </td>
          </tr>
        </tbody>
      </table>
    );
  }

  renderDetox() {
    return (
      <table
        style={{ tableLayout: "auto" }}
        className={this.props.fromReport && reportTable}
      >
        <thead>
          <tr>
            <th>Detox Upstream Marker</th>
            <th>Counts of Users</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={title}>Exposed to Tobacco smoke</td>
            <td>
              {this.state.total_tobaccoExposure_respondents > 0
                ? Math.round(
                    (this.state.total_tobaccoExposure /
                      this.state.total_tobaccoExposure_respondents) *
                      100
                  )
                : 0}
              % (n={this.state.total_tobaccoExposure_respondents})
            </td>
          </tr>
          <tr>
            <td className={title}>Exposed to Caffeine</td>
            <td>
              {this.state.total_caffeineexposure_respondents > 0
                ? Math.round(
                    ((this.state.total_caffeineexposure_respondents -
                      this.state.total_caffeineexposure) /
                      this.state.total_caffeineexposure_respondents) *
                      100
                  )
                : 0}
              % (n={this.state.total_caffeineexposure_respondents})
            </td>
          </tr>
          <tr>
            <td className={title}>Chemically-dependent</td>
            <td>
              {this.state.total_chemical_respondents > 0
                ? Math.round(
                    (this.state.total_chemical /
                      this.state.total_chemical_respondents) *
                      100
                  )
                : 0}
              % (n={this.state.total_chemical_respondents})
            </td>
          </tr>
        </tbody>
      </table>
    );
  }

  renderMovement() {
    return (
      <table
        style={{ tableLayout: "auto" }}
        className={this.props.fromReport && reportTable}
      >
        <thead>
          <tr>
            <th>Movement Upstream Marker</th>
            <th>Counts of Users</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td className={title}>Exercises at least 150mins per week</td>
            <td>
              {this.state.total_exercise_respondents > 0
                ? Math.round(
                    (this.state.total_exercise /
                      this.state.total_exercise_respondents) *
                      100
                  )
                : 0}
              % (n={this.state.total_exercise_respondents})
            </td>
          </tr>
        </tbody>
      </table>
    );
  }

  render() {
    const display = this.state.display;
    const mean_data = this.state.mean_data;
    const median_data = this.state.median_data;
    return (
      <div className="myTable">
        {!this.props.fromReport && (
          <div className={asflexTitle}>UPSTREAM MANIFESTATIONS</div>
        )}
        {display.weight ? this.renderWeight(mean_data, median_data) : null}
        {display.stress ? this.renderStress(mean_data, median_data) : null}
        {display.sleep ? this.renderSleep() : null}
        {display.detox ? this.renderDetox() : null}
        {display.movement ? this.renderMovement() : null}

        {display.foodchoices ? (
          <table
            style={{ tableLayout: "auto" }}
            className={this.props.fromReport && reportTable}
          >
            <thead>
              <tr>
                <td colSpan="3">
                  Top Food Choices (n={this.state.foodchoices_total_respondents}
                  )
                </td>
              </tr>
            </thead>
            {this._renderTableData(
              this.state.foodchoices,
              "foodchoices",
              "No available data"
            )}
          </table>
        ) : null}

        {display.stressSource ? (
          <table
            style={{ tableLayout: "auto" }}
            className={this.props.fromReport && reportTable}
          >
            <thead>
              <tr>
                <td colSpan="2">
                  Top Sources of Stress (n=
                  {this.state.stressSource_total_respondents})
                </td>
                <td>Mean Score</td>
              </tr>
            </thead>
            {this._renderTableData(
              this.state.stressSource,
              "stressSource",
              "No available data"
            )}
          </table>
        ) : null}

        {display.msqTopAffectedSystems ? (
          <table
            style={{ tableLayout: "auto" }}
            className={this.props.fromReport && reportTable}
          >
            <thead>
              <tr>
                <td colSpan="3">
                  Top Organ Systems Affected (n=
                  {this.state.msqTopAffectedSystems_total_respondents})
                  <br />
                  <i style={{ textTransform: "none" }}>
                    (Note: <strong>n</strong> corresponds to counts of users who
                    score at least 10 in each system.)
                  </i>
                </td>
              </tr>
            </thead>
            {this._renderTableData(
              this.state.msqTopAffectedSystems,
              "msqTopAffectedSystems",
              "No user scored >10 in any of the systems"
            )}
          </table>
        ) : null}

        {display.prevalentSymptoms ? (
          <table
            style={{ tableLayout: "auto" }}
            className={this.props.fromReport && reportTable}
          >
            <thead>
              <tr>
                <th colSpan="3">
                  Top Most Prevalent Symptoms (n=
                  {this.state.prevalentSymptoms_total_respondents})
                </th>
              </tr>
            </thead>
            {this._renderTableData(
              this.state.prevalentSymptoms,
              "prevalentSymptoms",
              "No user has any symptoms"
            )}
          </table>
        ) : null}
      </div>
    );
  }
}

const reportTable = css({
  margin: "5px 5% !important",
  "& tbody > tr > td": {
    fontSize: "smaller"
  }
});

let title = css({
  fontWeight: "bold",
  backgroundColor: "#f4f5f8"
});

let wideTd = css({
  fontSize: "1rem",
  padding: "15px !important",
  lineHeight: "18px",
  textAlign: "left"
});

let empty = css({
  backgroundColor: "#f4f5f8"
});

let rank = css({
  fontStyle: "italic",
  color: "#CCC"
});

let asflexTitle = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "space-around",
  padding: "5px",
  fontWeight: "bold",
  fontSize: 16,
  textTransform: "uppercase"
});
