import React from "react";
import { css } from "glamor";

import {
  ResponsiveContainer,
  Cell,
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Label
} from "recharts";
import CLabel from "../../NewSkin/Components/Label";
import { ResizableDiv } from "../../Reports/Parts";
import GridContainer from "../../NewSkin/Wrappers/GridContainer";
import { chart_tooltip } from "../../NewSkin/Styles";

const renderScatterChart = props => {
  const resizeDivName = props.resizeDivName;
  const boxProps = props.boxProps;

  const data = props.healthScores;
  // if (data.length > 0) {
  //   data.map(d => (d.healthscore = parseInt(d.healthscore).toFixed()));
  // }

  const mean_score = props.total_healthScore / data.length;
  const median_numbers = props.healthScoreValues;

  //  compute for median score
  const median = props.getMedianValue(median_numbers).toFixed();

  const legend = (
    <ul className={legendDiv}>
      <li className={`${legendColor} ${legendLabel}`}>LEGEND</li>
      <li className={legendColor}>{props.drawCircle("#72a153")}Optimal</li>
      <li className={legendColor}>
        {props.drawCircle("#71b2ca")}Suboptimal
      </li>
      <li className={legendColor}>{props.drawCircle("#dfaf29")}Neutral</li>
      <li className={legendColor}>
        {props.drawCircle("#d77f1a")}Compromised
      </li>
      <li className={legendColor}>{props.drawCircle("red")}Alarming</li>
      <li className={legendColor}>
        {props.drawCircle("#9c9e9e")}Incomplete Data
      </li>
    </ul>
  )

  const CustomTooltip = data => {
    if (data && data.active) {
      const employee_id = data.payload[0].payload.employee_id;
      const healthscore = data.payload[0].payload.healthscore;
      return (
        <div className={chart_tooltip}>
          <p>Employee ID: {employee_id}</p>
          <label>Health Risk Score: {healthscore}</label>
        </div>
      );
    }
  };

  return (
    <React.Fragment>
      {!props.fromReport && legend}
      <CLabel text="Distribution of Health Risk Scores" fullWidth styles={{ textAlign: 'center' }} />
      <ResizableDiv
        isGraph
        noOption
        resizeLock={props.resizeLock}
        name={resizeDivName}
        setDivAlignment={props.setDivAlignment}
        maxWidth={800}
        defaultSize={{ width: 800, height: 295 }}
        filters={props.resizeLock ? null : boxProps}
      >
        <GridContainer columnSize={'1fr 1fr'} styles={{ fontSize: 'smaller' }}>
          <CLabel text={`Mean Score: ${!isNaN(parseFloat(mean_score).toFixed())
            ? parseFloat(mean_score).toFixed()
            : 0}`} />
          <CLabel text={`Median Score: ${!isNaN(median) ? median : 0}`} />
        </GridContainer>

        <ResponsiveContainer height={250}>
          <ScatterChart
            width={800}
            margin={{
              top: 20,
              right: 0,
              bottom: 0,
              left: 5
            }}
          >
            <XAxis
              dataKey={"id"}
              type="number"
              domain={[0, "dataMax + 5"]}
              hide
            />
            <YAxis
              dataKey={"healthscore"}
              type="number"
              name="healthscore"
              domain={[0, 10]}
              ticks={[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
              tick={{ fontSize: 'smaller' }}
            >
              <Label
                value="Health Risk Scores"
                offset={5}
                position="insideLeft"
                angle={-90}
                style={{ fontSize: 'smaller' }}
              />
            </YAxis>
            <CartesianGrid strokeDasharray="3 3" />
            <Scatter name="A school" data={data} fill="#8884d8">
              {data.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={data[index].color} />
              ))}
            </Scatter>
            <Tooltip
              cursor={{ strokeDasharray: "3 3" }}
              content={CustomTooltip}
            />
          </ScatterChart>
        </ResponsiveContainer>

      </ResizableDiv>
    </React.Fragment>
  );
};

export default renderScatterChart;

let legendLabel = css({
  background: "#364563",
  color: "white",
  fontSize: ".8rem"
});

let mean_median_div = css({
  display: "flex",
  justifyContent: "center",
  width: "100%",
  marginLeft: "25px"
});

let title = css({
  textAlign: "center",
  fontWeight: "bold",
  padding: "5px 15px"
});

let legendDiv = css({
  border: "1px solid #364563",
  borderRadius: 3,
  // padding: '0 5px',
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  //width: '200px',
  alignItems: "center",
  margin: 0,
  marginBottom: 30,
  "@media print": {
    display: "none"
  }
});

let legendColor = css({
  display: "flex",
  fontSize: "1rem",
  padding: "5px 10px"
});

let asflexTitle = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "space-around",
  //width: "100%",
  padding: "5px",
  fontWeight: "bold",
  fontSize: 16,
  textTransform: "uppercase"
});
