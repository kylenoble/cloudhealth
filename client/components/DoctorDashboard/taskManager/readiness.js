import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  LabelList,
  Label,
  ResponsiveContainer
} from "recharts";
import { css } from "glamor";
import { chart_tooltip } from "../../NewSkin/Styles";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      chart_data: [],
      count: 0,
      activeFilter: {
        filter: "branch"
      }
    };
  }

  componentWillMount() {
    //this.props.filter('branch')
    this._getReadinessGraphData();
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.activeFilter.filter !== nextProps.activeFilter.filter) {
      this._getReadinessGraphData();

      // update filters
      let activeFilter = this.state.activeFilter;
      activeFilter.filter = nextProps.activeFilter.filter;

      this.setState({ activeFilter });
    }
  }

  render() {
    return (
      <div>
        {this._renderChart()}
        {
          !this.props.fromReport &&
          <div className={legendDiv}>
            <div className={legendColor}>
              {this._drawCircle("#80cde9")}Ready &nbsp;&nbsp;&nbsp;
              {this._drawCircle("#eec5e2")}Not Ready
            </div>
            <div>Registered users (n={this.state.count})</div>
          </div>
        }
        {
          this.props.fromReport &&
          <div>Registered users (n={this.state.count})</div>
        }
      </div>
    );
  }

  _getReadinessGraphData() {
    let chart_data = [];
    let total_cnt = 0;
    this.props
      .getReadinessGraphData()
      .then(res => {
        if (res.status === 404) return false;
        res.map(data => {
          let ready_val = parseFloat(
            ((parseInt(data.ready) / parseInt(data.total_cnt)) * 100).toFixed(2)
          );
          let notReady_val = parseFloat(
            (
              (parseInt(data.not_ready) / parseInt(data.total_cnt)) *
              100
            ).toFixed(2)
          );
          chart_data.push({
            name:
              this.state.activeFilter.filter == "high_risk"
                ? data.name == 1
                  ? "High Value"
                  : data.name == 2
                    ? "High Risk"
                    : "Others"
                : data.name,
            NotReady: data.not_ready,
            Ready: data.ready,
            rlbl: ready_val,
            nrlbl: notReady_val,
            nrlbl_pct: notReady_val > 0 ? notReady_val + "%" : "",
            rlbl_pct: ready_val > 0 ? ready_val + "%" : ""
          });
          total_cnt += parseInt(data.total_cnt);
        });
        this.setState({
          chart_data: chart_data,
          count: total_cnt
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  _showTooltip = data => {
    if (data.active) {
      let label = data.label;
      let notReadyValue = data.payload
        ? data.payload[0].payload.NotReady
        : null;
      let readyValue = data.payload ? data.payload[0].payload.Ready : null;
      return (
        <div className={chart_tooltip}>
          <p>{label}</p>
          <label>Ready: {readyValue}
            <br />Not Ready: {notReadyValue}</label>
        </div>
      );
    }
  };

  _renderChart() {
    const data = this.state.chart_data;
    let graphWidth = screen.width / 1.5;
    let height = data.length * 2 * 20;
    let graphHeight = height < 220 ? 220 : height;
    if (this.props.graphSize) {
      graphWidth = this.props.graphSize.graphWidth;
      graphHeight = this.props.graphSize.graphHeight;
    }

    const CustomizedLabel = (props) => {
      const { x, y, fill, value } = props
      console.log(props)
      return <text
        x={x}
        y={y}
        // dy={-4}
        fontSize={'smaller'}
        fill={fill}
        textAnchor="middle">{value > 0 && value}</text>

    };

    return (
      <div id="readiness-chart-wrapper" className={chartDiv}>
        {
          !this.props.fromReport &&
          <div className={asflexTitle}>READINESS TO FUNCTIONAL MEDICINE</div>
        }
        <BarChart
          width={this.props.fromReport ? 800 : graphWidth}
          height={graphHeight}
          layout="vertical"
          data={data}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
        >
          <XAxis
            type="number"
            unit="%"
            tick={{ transform: "translate(5, 3)", fontSize: "small" }}
            padding={{ left: 10, right: 10 }}
          />
          <YAxis
            type="category"
            dataKey="name"
            width={150}
            tick={{ transform: "translate(-10, 0)", fontSize: "small" }}
          />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip content={this._showTooltip.bind(this)} />
          <Bar dataKey="nrlbl" stackId="a" fill="#eec5e2">
            <LabelList dataKey="nrlbl_pct" position="inside" style={{ fontSize: 'small' }} label={CustomizedLabel} />
          </Bar>
          <Bar dataKey="rlbl" stackId="a" fill="#80cde9">
            <LabelList dataKey="rlbl_pct" position="inside" style={{ fontSize: 'small' }} label={CustomizedLabel} />
          </Bar>
        </BarChart>
      </div>
    );
  }

  _drawCircle(color) {
    return (
      <svg height="21" width="25">
        <circle cx="11" cy="11" r="6" stroke="black" stroke="0" fill={color} />
      </svg>
    );
  }
}

let chartDiv = css({
  width: "98%",
  margin: "0",
  padding: "5px",
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  "@media print": {
    display: "block"
  }
});

let legendDiv = css({
  display: "flex",
  flexWrap: "wrap",
  fontSize: 'smaller',
  height: 30,
  justifyContent: "center",
  alignItems: "center"
});

let asflexTitle = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "space-around",
  padding: "5px",
  fontWeight: "bold",
  fontSize: 16,
  textTransform: "uppercase"
});

let asflex = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "space-around",
  padding: "5px",
  fontSize: 14
});

let legendColor = css({
  display: "flex",
  justifyContent: "center",
  alignItems: "flex-start",
  flex: "1 0 100%"
});
