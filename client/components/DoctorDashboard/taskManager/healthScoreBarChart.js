import React from "react";
import { css } from "glamor";

import {
  ResponsiveContainer,
  BarChart,
  Cell,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  LabelList
} from "recharts";
import CLabel from "../../NewSkin/Components/Label";
import { ResizableDiv } from "../../Reports/Parts";
import { chart_tooltip } from "../../NewSkin/Styles";

const RenderBarChart = props => {
  const data = props.healthDeterminants;
  const resizeDivName = props.resizeDivName;
  const boxProps = props.boxProps;

  const TooltipContent = ({ active, label, payload }) => {
    if (active) {
      return (
        <div className={chart_tooltip}>
          <p>{label}</p>
          <label>Mean Score: {payload[0].value}</label>
        </div>
      );
    }

    return null;
  };

  return (
    <React.Fragment>
      <CLabel text="Status of Determinants of Health" fullWidth styles={{ textAlign: 'center' }} />
      <ResizableDiv
        isGraph
        noOption
        resizeLock={props.resizeLock}
        name={resizeDivName}
        setDivAlignment={props.setDivAlignment}
        maxWidth={800}
        defaultSize={{ width: 800, height: props.fromReport ? 270 : 400 }}
        filters={boxProps}
      >
        <ResponsiveContainer>
          <BarChart
            layout="vertical"
            data={data}
            margin={{ top: 10, right: 10, left: 0, bottom: 0 }}
          >
            <XAxis
              type="number"
              domain={[0, `${"dataMax" < 10 ? 10 : 15}`]}
              ticks={[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
              tick={{ transform: "translate(5, 3)", fontSize: 'smaller' }}
              padding={{ left: 10, right: 10 }}
            />
            <YAxis
              type="category"
              dataKey="determinant"
              width={150}
              tick={{ transform: "translate(-10, 0)", fontSize: 'smaller' }}
            />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip content={TooltipContent} />
            <Bar dataKey="Mean Score">
              {data.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={data[index].color} />
              ))}
              <LabelList
                dataKey="Median Score"
                position="right"
                fill="#000"
                fontSize={'small'}
                width={150}
              />
            </Bar>
          </BarChart>
        </ResponsiveContainer>
      </ResizableDiv>
    </React.Fragment>
  );
};

export default RenderBarChart;

let asflexTitle = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "space-around",
  padding: "5px",
  fontWeight: "bold",
  fontSize: 16,
  textTransform: "uppercase"
});
