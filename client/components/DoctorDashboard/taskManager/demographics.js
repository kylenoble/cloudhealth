/* eslint-disable eqeqeq */
import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer
} from "recharts";

import { css } from "glamor";
import CustomPieChart from "./customPieChart";

import AnswersService from "../../../utils/answerService";
import DemographicsPieChart from "./demographicsBarChart";
import Label from "../../NewSkin/Components/Label";
import Colors from "../../NewSkin/Colors";

const answerService = new AnswersService();

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      resizeLock: this.props.resizeLock,
      users: [],
      registeredUsrs: 0,
      demographicsValue: [],
      activeFilter: {
        filter: "",
        value: ""
      },
      ethnicity: "",
      marital_status: "",
      filteredUsers: [] // for marital status and ethnicity
    };

    // this._applyAgeGrouping = this._applyAgeGrouping.bind(this)
    this._getGrapData = this._getGrapData.bind(this);
    this._processGraph = this._processGraph.bind(this);
    this._setDivAlignment = this._setDivAlignment.bind(this);
  }

  componentDidMount() {
    this._getDemogData();
    // this._getEthnicity()
    this.setupPieChart();
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.state.activeFilter.filters !== nextProps.activeFilter.filters ||
      this.state.activeFilter.value !== nextProps.activeFilter.value
    ) {
      // update filters
      const activeFilter = this.state.activeFilter;
      activeFilter.filter = nextProps.activeFilter.filter;
      activeFilter.value = nextProps.activeFilter.value;
      activeFilter.filters = nextProps.activeFilter.filters;
      this.setState({ activeFilter }, function () {
        this._getFilteredData();
      });
    }
  }

  setupPieChart = () => {
    const company_name = this.props.activeCompany.company_name;
    const ethnicity = { question_id: 4, company_name };
    const marital_status = { question_id: 3, company_name };

    this._getPieChartData("ethnicity", ethnicity);
    this._getPieChartData("marital_status", marital_status);
  };

  _getDemogData() {
    if (this.props.demogdata) {
      this.props.demogdata().then(res => {
        this._getGrapData(res);
      });
    } else {
      this._getGrapData();
    }
  }

  multiFilter(array, filters) {
    const filterKeys = Object.keys(filters);
    return array.filter(item =>
      filterKeys.every(key => !!~filters[key].indexOf(item[key]))
    );
  }

  _getFilteredData() {
    const activeFilter = this.state.activeFilter;

    let filtered = this.state.users;

    if (activeFilter.filters.branch && activeFilter.filters.branch.length > 0) {
      const filters = { branch: activeFilter.filters.branch };
      filtered = this.multiFilter(filtered, filters);
    }

    if (
      activeFilter.filters.department &&
      activeFilter.filters.department.length > 0
    ) {
      const filters = { department: activeFilter.filters.department };
      filtered = this.multiFilter(filtered, filters);
    }

    if (
      activeFilter.filters.jobgrade &&
      activeFilter.filters.jobgrade.length > 0
    ) {
      const filters = { job_grade: activeFilter.filters.jobgrade };
      filtered = this.multiFilter(filtered, filters);
    }

    if (
      activeFilter.filters &&
      activeFilter.filters.high_risk &&
      activeFilter.filters.high_risk.length > 0
    ) {
      // filtered = filtered.filter(
      //   str => str.high_risk == activeFilter.filters.high_risk
      // );

      const high_risk_convert = activeFilter.filters.high_risk.map(item => {
        if (item === 1) {
          return "High Value";
        } else if (item === 2) {
          return "High Risk";
        }
        return "Others";
      });

      const filters = { high_risk: high_risk_convert };

      filtered = this.multiFilter(filtered, filters);
    }

    this.setState({ filteredUsers: filtered });
    setTimeout(() => {
      this.setupPieChart();
    }, 100);

    this._processGraph(filtered);
    if (this.props.filteredData) this.props.filteredData(filtered);
  }

  _getGrapData(usrsdata = null) {
    let usrs;
    if (usrsdata) {
      usrs = usrsdata;
    } else {
      usrs = this.props.data();
    }

    this.setState(
      {
        users: usrs
      },
      () => {
        this._processGraph(usrs);
        if (this.props.filteredData) {
          this.props.filteredData(usrs);
        }
      }
    );
  }

  _processGraph(res) {
    const tempusers = res;
    // let ageGroupFilterBy = this.state.ageGroupFilterBy
    //
    // if(ageGroupFilterBy.length > 0){
    //   let newArray = res.filter((a, i)=>{
    //     let age = 0
    //     if(a.birthday){
    //       let birthDate = new Date(a.birthday);
    //       let otherDate = new Date();
    //       age = (otherDate.getFullYear() - birthDate.getFullYear());
    //       if(age === 0) age = 0.5
    //       a.age = age
    //     }else{
    //       age = 0
    //       a.age = age
    //     }
    //     return a.age >= ageGroupFilterBy[0] && a.age <= ageGroupFilterBy[1]
    //   })
    //   tempusers = newArray
    // }

    const bracket = [
      { name: "0-9", count: 0, male: 0, female: 0 },
      { name: "10-19", count: 0, male: 0, female: 0 },
      { name: "20-29", count: 0, male: 0, female: 0 },
      { name: "30-39", count: 0, male: 0, female: 0 },
      { name: "40-49", count: 0, male: 0, female: 0 },
      { name: "50-59", count: 0, male: 0, female: 0 },
      { name: "60-69", count: 0, male: 0, female: 0 },
      { name: "70-79", count: 0, male: 0, female: 0 },
      { name: "80-above", count: 0, male: 0, female: 0 },
      { name: "No Answer", count: 0, male: 0, female: 0 }
    ];

    for (const user of tempusers) {
      let age = 0;
      if (user.birthday) {
        const birthDate = new Date(user.birthday);
        const otherDate = new Date();
        age = otherDate.getFullYear() - birthDate.getFullYear();
        if (age === 0) age = 0.5;
        user.age = age;
      } else {
        age = 0;
        user.age = age;
      }

      if (user.gender) {
        if (age > 0 && age <= 9) {
          bracket[0].count++;
          if (user.gender.trim().toUpperCase() == "FEMALE") {
            bracket[0].female++;
          } else if (user.gender.trim().toUpperCase() == "MALE") {
            bracket[0].male++;
          } else if (bracket[0].no_gender == undefined) {
            bracket[0].no_gender = 1;
          } else {
            bracket[0].no_gender++;
          }
        } else if (age >= 10 && age <= 19) {
          bracket[1].count++;
          if (user.gender.trim().toUpperCase() == "FEMALE") {
            bracket[1].female++;
          } else if (user.gender.trim().toUpperCase() == "MALE") {
            bracket[1].male++;
          } else if (bracket[1].no_gender == undefined) {
            bracket[1].no_gender = 1;
          } else {
            bracket[1].no_gender++;
          }
        } else if (age >= 20 && age <= 29) {
          bracket[2].count++;
          if (user.gender.trim().toUpperCase() == "FEMALE") {
            bracket[2].female++;
          } else if (user.gender.trim().toUpperCase() == "MALE") {
            bracket[2].male++;
          } else if (bracket[2].no_gender == undefined) {
            bracket[2].no_gender = 1;
          } else {
            bracket[2].no_gender++;
          }
        } else if (age >= 30 && age <= 39) {
          bracket[3].count++;
          if (user.gender.trim().toUpperCase() == "FEMALE") {
            bracket[3].female++;
          } else if (user.gender.trim().toUpperCase() == "MALE") {
            bracket[3].male++;
          } else if (bracket[3].no_gender == undefined) {
            bracket[3].no_gender = 1;
          } else {
            bracket[3].no_gender++;
          }
        } else if (age >= 40 && age <= 49) {
          bracket[4].count++;
          if (user.gender.trim().toUpperCase() == "FEMALE") {
            bracket[4].female++;
          } else if (user.gender.trim().toUpperCase() == "MALE") {
            bracket[4].male++;
          } else if (bracket[4].no_gender == undefined) {
            bracket[4].no_gender = 1;
          } else {
            bracket[4].no_gender++;
          }
        } else if (age >= 50 && age <= 59) {
          bracket[5].count++;
          if (user.gender.trim().toUpperCase() == "FEMALE") {
            bracket[5].female++;
          } else if (user.gender.trim().toUpperCase() == "MALE") {
            bracket[5].male++;
          } else if (bracket[5].no_gender == undefined) {
            bracket[5].no_gender = 1;
          } else {
            bracket[5].no_gender++;
          }
        } else if (age >= 60 && age <= 69) {
          bracket[6].count++;
          if (user.gender.trim().toUpperCase() == "FEMALE") {
            bracket[6].female++;
          } else if (user.gender.trim().toUpperCase() == "MALE") {
            bracket[6].male++;
          } else if (bracket[6].no_gender == undefined) {
            bracket[6].no_gender = 1;
          } else {
            bracket[6].no_gender++;
          }
        } else if (age >= 70 && age <= 79) {
          bracket[7].count++;
          if (user.gender.trim().toUpperCase() == "FEMALE") {
            bracket[7].female++;
          } else if (user.gender.trim().toUpperCase() == "MALE") {
            bracket[7].male++;
          } else if (bracket[7].no_gender == undefined) {
            bracket[7].no_gender = 1;
          } else {
            bracket[7].no_gender++;
          }
        } else if (age >= 80) {
          bracket[8].count++;
          if (user.gender.trim().toUpperCase() == "FEMALE") {
            bracket[8].female++;
          } else if (user.gender.trim().toUpperCase() == "MALE") {
            bracket[8].male++;
          } else if (bracket[8].no_gender == undefined) {
            bracket[8].no_gender = 1;
          } else {
            bracket[8].no_gender++;
          }
        } else {
          bracket[9].count++;
          if (user.gender.trim().toUpperCase() == "FEMALE") {
            bracket[9].female++;
          } else if (user.gender.trim().toUpperCase() == "MALE") {
            bracket[9].male++;
          } else if (bracket[9].no_gender == undefined) {
            bracket[9].no_gender = 1;
          } else {
            bracket[9].no_gender++;
          }
        }
      }
    }

    this.setState({
      registeredUsrs: tempusers.length,
      demographicsValue: bracket
    });
  }

  _getPieChartData = (name, data) => {
    const filteredUsers = this.state.filteredUsers;
    answerService.getPieChartData(data).then(res => {
      if (filteredUsers.length > 0) {
        const _filtered = [];
        filteredUsers.filter(filtered =>
          res.forEach(user => {
            if (filtered.id === user.id) {
              _filtered.push(user);
            }
          })
        );
        this.setState({ [name]: _filtered });
      } else {
        this.setState({ [name]: res });
      }
    });
  };

  _ethnicityGroups = () => {
    const users = this.state.ethnicity;
    const asian = users.filter(user => user.value === "Asian");
    const australian = users.filter(
      user => user.value === "Australian/Pacific Islander"
    );
    const black = users.filter(
      user => user.value === "Black or African Origin"
    );
    const caucasian = users.filter(user => user.value === "Caucasian");
    const hispanic = users.filter(
      user => user.value === "Hispanic, Latino, or Spanish Origin"
    );
    const other = users.filter(user => user.value === "Other");
    const ethnicityGroup = {
      asian,
      australian,
      black,
      caucasian,
      hispanic,
      other
    };
    return ethnicityGroup;
  };

  _maritalStatusGroups = () => {
    const users = this.state.marital_status;
    const married = users.filter(user => user.value === "Married");
    const single = users.filter(user => user.value === "Single");
    const partnered = users.filter(user => user.value === "Partnered");
    const maritalStatusGroup = { married, single, partnered };
    return maritalStatusGroup;
  };

  _pieChartData = group => {
    const data = [];

    if (group === "Ethnicity") {
      const ethnicity = this._ethnicityGroups();
      const ethnicity_data = [
        { name: "Asian", value: ethnicity.asian.length },
        {
          name: "Australian/Pacific Islander",
          value: ethnicity.australian.length
        },
        { name: "Black or African Origin", value: ethnicity.black.length },
        { name: "Caucasian", value: ethnicity.caucasian.length },
        {
          name: "Hispanic, Latino, or Spanish Origin",
          value: ethnicity.hispanic.length
        },
        { name: "Other", value: ethnicity.other.length }
      ];
      data.push(...ethnicity_data);
    } else {
      const marital_status = this._maritalStatusGroups();
      const marital_status_data = [
        { name: "Married", value: marital_status.married.length },
        { name: "Single", value: marital_status.single.length },
        { name: "Partnered", value: marital_status.partnered.length }
      ];
      data.push(...marital_status_data);
    }
    return data;
  };

  _renderPieChart = group => {
    const trimedGroupName = group.replace(" ", "_");
    const data = this._pieChartData(group);
    return (
      <CustomPieChart
        fromReport={this.props.fromReport}
        resizeLock={this.state.resizeLock}
        data={data}
        group={group}
        resizeDivName={`${this.props.resizeDivName}_${trimedGroupName}`}
        activeFilter={this.props.boxProps}
        setDivAlignment={this._setDivAlignment}
      />
    );
  };

  _setDivAlignment(boxProps, name) {
    this.props.setDivAlignment(boxProps, name);
  }

  _renderChart() {
    const data = this.state.demographicsValue;
    let noGenerCtr = 0;
    data.forEach(d => {
      if (d.no_gender) {
        noGenerCtr++;
      }
    });
    const noAns = data[9];

    if (noAns && !noAns.count) {
      data.splice(9, 1);
    }

    let graphWidth = 600;
    let graphHeight = 300;

    if (this.props.graphSize) {
      graphWidth = this.props.graphSize.graphWidth;
      graphHeight = this.props.graphSize.graphHeight;
    }

    const fromReport = this.props.fromReport;

    return (
      <div className={chartDiv(fromReport)}>
        {
          <Label fullWidth subheader styles={{ margin: 'auto 50px' }} text="Age Distribution of Respondents" />
        }

        <DemographicsPieChart
          fromReport={fromReport}
          resizeLock={this.state.resizeLock}
          data={data}
          noGenerCtr={noGenerCtr}
          resizeDivName={this.props.resizeDivName}
          activeFilter={this.props.boxProps}
          setDivAlignment={this._setDivAlignment}
        />

        {this.state.ethnicity.length ? this._renderPieChart("Ethnicity") : ""}
        {this.state.marital_status.length
          ? this._renderPieChart("Marital Status")
          : ""}
      </div>
    );
  }

  _drawCircle(color) {
    return (
      <svg height="21" width="25">
        <circle cx="11" cy="11" r="6" stroke="black" stroke="0" fill={color} />
      </svg>
    );
  }

  render() {
    return <div className={graphHeader}>{this._renderChart()}</div>;
  }
}

let chartWrapper = css({
  margin: "20px 0",
  width: "100%",
  display: "flex",
  justifyContent: "center"
});

let graphHeader = css({
  marginTop: "10px",
  width: "100%"
});

let chartDiv = fromReport => css({
  display: "flex",
  flexWrap: "wrap",
  justifyContent: fromReport ? 'left' : 'center',
  width: '100%'
  //  alignItems: 'flex-end'
});