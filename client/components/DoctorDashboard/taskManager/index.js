/* eslint-disable no-param-reassign */
//import currentDomain from '../../../utils/domain.js';
import React from "react";
import { css } from "glamor";
import dynamic from "next/dynamic";

import ReactTable from "react-table";
import Demographics from "./demographics";
import Readiness from "./readiness";
import HealthScoreStatus from "./healthScoreStatus";
import Downstream from "./downstream";
import MSQ from "./msq";
import Upstream from "./upstream";
import { CHIReportNew } from "../../../components/Reports";
import DefaultFilters from "../../../utils/defaultFilters";
import ReportsService from "../../../utils/reportsService";
import { GetPercentileRank } from "../../Commons/others";
import Card from "../../NewSkin/Wrappers/Card";
import { ReactTableStyle, labelClass } from "../../NewSkin/Styles";
import NoData from "../../NewSkin/Components/NoData";
import Button from "../../NewSkin/Components/Button";
import TabWrapper from '../../NewSkin/Wrappers/TabWrapper';
import TabItem from '../../NewSkin/Wrappers/TabItem';

//const company = new CompanyService()
const reports = new ReportsService();

export default class extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = {
      activeView: "",
      activeCompany: "",
      activeCompanyDetails: "",
      companies: [],
      activeGraph: "demographics",
      activeFilter: {
        filter: "",
        value: "",
        init: true,
        filters: {}
      },
      prevFilter: "",
      //report_defaults: "",
      scorePerCompany: {},
      loading: false,
      active_company: ""
    };
    this._back = this._back.bind(this);
    this._backTomain = this._backTomain.bind(this);
    this._getGraphData = this._getGraphData.bind(this);
    this._getReadinessGraphData = this._getReadinessGraphData.bind(this);
    this._setActiveGraph = this._setActiveGraph.bind(this);
    this._setFilter = this._setFilter.bind(this);
    this._getHealthScoreGraphData = this._getHealthScoreGraphData.bind(this);
    //this._getReportDefault = this._getReportDefault.bind(this);
    this._getDownstreamGraphData = this._getDownstreamGraphData.bind(this);
    this._getMSQGraphData = this._getMSQGraphData.bind(this);
    this._getMSQScoresPerSystem = this._getMSQScoresPerSystem.bind(this);
    this._getUpstreamData = this._getUpstreamData.bind(this);
    this._getMSQSystemRank = this._getMSQSystemRank.bind(this);
    this._getStressSourceRank = this._getStressSourceRank.bind(this);
    this._getUpstreamSymptoms = this._getUpstreamSymptoms.bind(this);
    this._getUpstreamFoodchoices = this._getUpstreamFoodchoices.bind(this);
    this._getHealthDeterminantsGraphData = this._getHealthDeterminantsGraphData.bind(
      this
    );
    this._getUserFilters = this._getUserFilters.bind(this);
    this._getFilteredData = this._getFilteredData.bind(this);
    this._prepareScoreAndPercentile = this._prepareScoreAndPercentile.bind(
      this
    );
    this._getscoreAndPercentile = this._getscoreAndPercentile.bind(this);
    this._getCompliance = this._getCompliance.bind(this);
    this._getQuery = this._getQuery.bind(this);
    this._reportSwitch = this._reportSwitch.bind(this);
    this._ReadinessGraphData = this._ReadinessGraphData.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    //this._getReportDefault();
    // this._getReport();
    this._getCompanies();
    this._prepareScoreAndPercentile();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  _getQuery(params) {
    let branchStrings = [];
    let departmentStrings = [];
    let age_groupStrings = [];
    let high_riskStrings = [];
    let job_gradeStrings = [];

    if (params && params.branch && params.branch.length > 0) {
      const str = params.branch;
      branchStrings = str.map(item => `'${item}'`).join(",");
    }

    if (params && params.department && params.department.length > 0) {
      const str = params.department;
      departmentStrings = str.map(item => `'${item}'`).join(",");
    }

    if (params && params.age_group && params.age_group.length > 0) {
      const str = params.age_group;
      age_groupStrings = str.map(item => `'${item}'`).join(",");
    }

    if (params && params.high_risk && params.high_risk.length > 0) {
      const str = params.high_risk;
      high_riskStrings = str.map(item => `'${item}'`).join(",");
    }

    if (params && params.job_grade && params.job_grade.length > 0) {
      const str = params.job_grade;
      job_gradeStrings = str.map(item => `'${item}'`).join(",");
    }

    let qry = "";

    qry = `select user_id, name, email, employee_id, company, branch, department, job_grade, high_risk, dod, lower(trim(doh))as doh, age_group, count(*)
            from (
            select user_id, name, email, employee_id, company, branch, department, job_grade, high_risk, split_part(value, '_', 2) dod, split_part(value, '_', 1) doh, ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY MAX(created_date) DESC ) as rn, age_group
            from new_answers_with_details `;

    qry += `where company = '${this.state.activeCompany}' `;

    if (params && params.branch && params.branch.length > 0) {
      qry += `and branch in (${branchStrings}) `;
    }

    if (params && params.department.length > 0) {
      qry += `and department in (${departmentStrings}) `;
    }

    if (params && params.high_risk && params.high_risk.length > 0) {
      qry += `and high_risk in (${high_riskStrings}) `;
    }

    if (params && params.job_grade && params.job_grade.length > 0) {
      qry += `and job_grade in (${job_gradeStrings}) `;
    }

    if (params && params.age_group && params.age_group.length > 0) {
      qry += `and age_group in (${age_groupStrings}) `;
    }

    qry += `and lower(split_part(value, '_', 1)) in ('weight', 'nutrition', 'detox', 'sleep', 'movement', 'stress', 'other' )
      and question_id = 133 and healthsurvey_status='Locked'
      group by user_id, name, email, employee_id, company, branch, department, job_grade, high_risk, dod, doh, age_group
      ) x
      where rn <= 3
      group by rn, lower(trim(doh ) ), user_id, name, email, employee_id, company, branch, department, job_grade, high_risk, dod, age_group
      order by user_id, rn desc  `;

    return this.props
      .getQuery(qry)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getCompliance() {
    return this.props
      .getCompliance(this.state.activeCompany)
      .then(res => Promise.resolve(res))
      .catch(err => {
        console.log(err);
      });
  }

  _getCompanies() {
    this.props.companies(this.props.user.id).then(res => {
      if (this._isMounted) {
        this.setState({
          companies: res,
          loading: false
        });
      }
    });
  }

  _prepareScoreAndPercentile() {
    this.props
      .getScoresByCompany("All")
      .then(res => {
        if (res && res.length > 0) this._prepare(res);
      })
      .catch(err => {
        console.log(err);
      });
  }

  _prepare(arr) {
    const companies = this.state.companies;
    const averageScorePerCompany = [];
    companies.forEach((item, i) => {
      averageScorePerCompany.push({
        name: item.company_name,
        average: 0,
        scores: []
      });

      // const samecomp = arr.filter(
      //   a => a.company.toLowerCase() === item.company_name.toLowerCase()
      // );

      arr.forEach(sItem => {
        if (sItem.company.toLowerCase() === item.company_name.toLowerCase()) {
          if (
            averageScorePerCompany[i].name.toLowerCase() ===
            sItem.company.toLowerCase() &&
            sItem.healthsurvey_status == "Locked"
          ) {
            averageScorePerCompany[i].scores.push({
              id: sItem.user_id,
              healthScore: sItem.healthscore,
              high_risk: sItem.high_risk,
              job_grade: sItem.job_grade,
              status: sItem.status,
              company: sItem.company,
              branch: sItem.branch,
              department: sItem.department
            });
          }
        }
      });
    });

    averageScorePerCompany.forEach(item => {
      const average = parseFloat(this._getAverage(item.scores)).toFixed(1);
      item.average = average;
    });

    averageScorePerCompany.forEach(item => {
      item.percentile = this._getPercentile(
        averageScorePerCompany,
        item.average
      ).toFixed();
    });

    if (this._isMounted) {
      this.setState({
        scorePerCompany: averageScorePerCompany
      });
    }
  }

  _getAverage(elmt) {
    let sum = 0;
    let avg = 0;
    for (let i = 0; i < elmt.length; i++) {
      sum += parseFloat(elmt[i].healthScore); //don't forget to add the base
    }

    if (sum > 0) avg = sum / elmt.length;
    return avg;
  }

  compare(a, b) {
    // Use toUpperCase() to ignore character casing
    const A = parseFloat(a.average);
    const B = parseFloat(b.average);

    let comparison = 0;
    if (A > B) {
      comparison = 1;
    } else if (A < B) {
      comparison = -1;
    }
    return comparison;
  }

  _getPercentile(obj, score) {
    const sorted = obj.sort(this.compare); // SORT BY AVERAGE FROM LOWEST TO HIGHEST
    const comp = sorted;
    let uwls = 0;
    const tnou = comp.length;
    let rank = 0;
    if (comp && comp.length > 0) {
      comp.forEach(item => {
        if (parseFloat(item.average) < parseFloat(score)) uwls++;
      });
    }

    rank = GetPercentileRank(uwls, tnou);
    // rank = (uwls / tnou) * 100;
    return rank;
  }

  // _getReport() {
  //   const criteria = { field: "all", value: "" };
  //   return reports
  //     .getReport(criteria)
  //     .then(res => {
  //       console.log('GET REPORT', res)
  //       Promise.resolve(res)})
  //     .catch(e => {
  //       console.log(e);
  //     });
  // }

  _activeCompanyDetails(id) {
    const cmpy = this.state.companies;
    const result = cmpy.filter(comp => comp.id === id);
    if (this._isMounted) {
      this.setState({
        activeCompanyDetails: result[0]
      });
    }
  }

  // _getReportDefault() {
  //   reports
  //     .getReportDefault()
  //     .then(res => {
  //       console.log('REPORT DEFAULT', res)
  //       if (res.status !== 400) {
  //         if (this._isMounted) {
  //           this.setState({
  //             report_defaults: res[0].details
  //           });
  //         }
  //       }
  //     })
  //     .catch(e => {
  //       console.log(e);
  //     });
  // }

  _getUserFilters() {
    return this.props
      .filters(this.state.activeCompany)
      .then(res => {
        return Promise.resolve(res);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _getFilteredData(filter_column, filter_value, filters) {
    const { activeFilter } = this.state;
    activeFilter.filter = filter_column;
    activeFilter.value = filter_value;
    activeFilter.init = false;
    activeFilter.filters = filters;
    if (this._isMounted) {
      this.setState({ activeFilter });
    }
  }

  _getGraphData() {
    return this.props
      .data(this.state.activeCompany)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getHealthScoreGraphData() {
    return this.props
      .getScoresByCompany(this.state.activeCompany)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getHealthDeterminantsGraphData() {
    return this.props
      .healthDeterminants(this.state.activeCompany)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _ReadinessGraphData(filter) {
    return this.props
      .readinessData(this.state.activeCompany, filter.filter)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getReadinessGraphData() {
    return this.props
      .readinessData(this.state.activeCompany, this.state.activeFilter.filter)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getDownstreamGraphData() {
    return this.props
      .downstreamData(this.state.activeCompany)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getMSQGraphData() {
    return this.props
      .msqData(this.state.activeCompany)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getMSQScoresPerSystem() {
    return this.props
      .msqDataPerSystem(this.state.activeCompany)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getUpstreamData() {
    return this.props
      .upstreamData(this.state.activeCompany)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getMSQSystemRank() {
    return this.props
      .msqSystemRank(this.state.activeCompany)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getStressSourceRank() {
    return this.props
      .stressSourceRank(this.state.activeCompany)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getUpstreamSymptoms() {
    return this.props
      .symptoms(this.state.activeCompany)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getUpstreamFoodchoices() {
    return this.props
      .foodchoices(this.state.activeCompany)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getscoreAndPercentile() {
    const comp = this.state.scorePerCompany;
    let obj = {};
    if (comp && comp.length > 0) {
      comp.forEach(item => {
        if (item.name === this.state.activeCompany) {
          obj = item;
        }
      });
    }
    return obj;
  }

  _setFilter(filter, value, init) {
    const prevFilter = this.state.activeFilter.filter;
    if (this._isMounted) {
      this.setState({ prevFilter }, function () {
        const activeFilter = this.state.activeFilter;
        activeFilter.filter = filter;
        activeFilter.value = value;
        activeFilter.init = init;

        this.setState({ activeFilter });
      });
    }
  }

  _renderView() {
    if (this.state.activeView === "") {
      return this._renderCompanyReports();
    } else if (this.state.activeView === "chi") {
      return (
        <div className="wrap">
          {this._renderHeaderLinks()}
          {this._renderChart()}
        </div>
      );
    } else if (this.state.activeView === "wr") {
      return (
        <CHIReportNew
          getScoresByCompany={this._getHealthScoreGraphData}
          user={this.props.user}
          backTomain={this._backTomain}
          back={this._back}
          id={this.state.activeID}
          activeCompanyDetails={this.state.activeCompanyDetails}
          //report_defaults={this.state.report_defaults}
          scoreAndPercentile={this._getscoreAndPercentile}
          getCompliance={this._getCompliance}
          healthDeterminants={this._getHealthDeterminantsGraphData}
          data={this._getGraphData}
          getQuery={this._getQuery}
          reportSwitch={this._reportSwitch}
          filters={this._getUserFilters}
          //readinessData={this._ReadinessGraphData}
          getDownstreamGraphData={this._getDownstreamGraphData}
          msq_data={this._getMSQGraphData}
          msqScoresPerSystem={this._getMSQScoresPerSystem}
          upstream_data={this._getUpstreamData}
          msqSystemRank={this._getMSQSystemRank}
          stressSourceRank={this._getStressSourceRank}
          prevalentSymptoms={this._getUpstreamSymptoms}
          foodchoices={this._getUpstreamFoodchoices}
          getReadinessGraphData={this._getReadinessGraphData}
          filter={this._setFilter}
          activeFilter={this.state.activeFilter}
        />
      );
    }
  }

  _reportSwitch(rprt) {
    if (this._isMounted) {
      this.setState({
        activeView: rprt
      });
    }
  }

  _back() {
    this._getCompanies();
    const activeFilter = this.state.activeFilter;
    activeFilter.filter = "";
    activeFilter.value = "";
    if (this._isMounted) {
      this.setState({
        activeView: "",
        activeGraph: "demographics",
        activeFilter
      });
    }
  }

  _backTomain(strg) {
    this.props.back(strg);
  }

  _renderCompanyReports() {
    return (
      <div style={{ display: "block", width: "100%" }}>
        <span className={labelClass}>Company Report</span>
        <div className={tableWrapper}>{this._companyListTable()}</div>
      </div>
    );
  }

  // _companyList() {
  //   const companies = this.state.companies;
  //   // eslint-disable-next-line no-confusing-arrow
  //   return companies.map((company, index) =>
  //     company.isassignedtome === true ? (
  //       <div key={index} className={listStyle}>
  //         <div className={itemLinkStyle}>{company.company_name}</div>
  //         <span
  //           className={chiLinkStyle}
  //           onClick={() => this._renderCHI("chi", company, null)}
  //         >
  //           Open Corporate Health Index
  //         </span>
  //         <div className={reportLinkStyle}>
  //           <span
  //             onClick={() =>
  //               this._renderCHI("wr", company.company_name, company.id)
  //             }
  //           >
  //             Write Report
  //           </span>{" "}
  //         </div>
  //       </div>
  //     ) : null
  //   );
  // }

  _companyListTable() {
    const companies = this.state.companies;
    const filtered_data = [];
    let columns = [];

    if (companies.length > 0) {
      // eslint-disable-next-line array-callback-return
      companies.filter(company => {
        if (company.isassignedtome === true) {
          filtered_data.push(company);

          columns = [
            {
              Header: "Company Name",
              accessor: "company_name",
              style: { textAlign: "center" }
            },
            {
              Header: "CHI Graphs",
              accessor: "graphs",
              Cell: (
                <Button type="pink" styles={{ margin: '0 auto', lineHeight: 3, maxWidth: 'fit-content', fontWeight: 500 }}>
                  Open Corporate Health Index
                </Button>
              ),
              style: { textAlign: "center" }
            },
            {
              Header: "Write Report",
              accessor: "report",
              Cell: <Button type="pink" styles={{ margin: '0 auto', lineHeight: 3, maxWidth: 'fit-content', fontWeight: 500 }}>
                Write Report
                </Button>,
              style: { textAlign: "center" }
            }
          ];
        }
      });
    }

    if (filtered_data.length > 0) {
      const TheadComponent = () => null; //hides header

      return (
        <ReactTable
          data={filtered_data}
          columns={columns}
          TheadComponent={TheadComponent}
          defaultPageSize={10}
          minRows={1}
          noDataText="No available company to display"
          className="-highlight"
          loading={this.state.loading}
          getTdProps={(state, rowInfo, column) => ({
            onClick: (e, handleOriginal) => {
              // console.log("It was in this column:", column);
              // console.log("It was in this row:", rowInfo);

              // IMPORTANT! React-Table uses onClick internally to trigger
              // events like expanding SubComponents and pivots.
              // By default a custom 'onClick' handler will override this functionality.
              // If you want to fire the original onClick handler, call the
              // 'handleOriginal' function.
              if (handleOriginal) {
                handleOriginal();
                // eslint-disable-next-line default-case
                switch (column.id) {
                  case "graphs":
                    this._renderCHI("chi", rowInfo.row, null);
                    break;

                  case "report":
                    this._renderCHI("wr", rowInfo.row, rowInfo.original.id);
                    break;
                }
              }
            }
          })}
        />
      );
    }

    return <NoData />;
  }

  _renderNotifications() {
    return (
      <div className={wrapDiv}>
        <div className={labelWrapper}>
          <span className={labelStyle}>Notifications</span>
        </div>
        <div>
          <div>Date and Time</div>
          <div>Activity</div>
        </div>
      </div>
    );
  }

  _renderCHI(lnk, company, id = null) {
    if (this._isMounted) {
      this.setState({
        activeView: lnk,
        activeCompany: company.company_name,
        activeID: id
      });
    }
    this._activeCompanyDetails(id);
    if (this._isMounted) {
      this.setState({
        active_company: company
      });
    }
  }

  _setActiveGraph(graph) {
    if (this._isMounted) {
      this.setState({ activeGraph: graph });
    }
    this._setFilter("", "", true);
  }

  _renderChart() {
    switch (this.state.activeGraph) {
      case "readiness":
        return (
          <Readiness
            getReadinessGraphData={this._getReadinessGraphData}
            filter={this._setFilter}
            activeFilter={this.state.activeFilter}
          />
        );
      case "healthscore":
        return (
          <HealthScoreStatus
            resizeLock
            getScoresByCompany={this._getHealthScoreGraphData}
            healthDeterminants={this._getHealthDeterminantsGraphData}
            activeFilter={this.state.activeFilter}
          />
        );
      case "downstream":
        return (
          <Downstream
            getDownstreamGraphData={this._getDownstreamGraphData}
            activeFilter={this.state.activeFilter}
          />
        );
      case "msq":
        return (
          <MSQ
            data={this._getMSQGraphData}
            msqScoresPerSystem={this._getMSQScoresPerSystem}
            activeFilter={this.state.activeFilter}
          />
        );
      case "upstream":
        return (
          <Upstream
            data={this._getUpstreamData}
            msqSystemRank={this._getMSQSystemRank}
            stressSourceRank={this._getStressSourceRank}
            prevalentSymptoms={this._getUpstreamSymptoms}
            foodchoices={this._getUpstreamFoodchoices}
            activeFilter={this.state.activeFilter}
          />
        );
      default:
        return (
          <Demographics
            resizeLock
            demogdata={this._getGraphData}
            activeFilter={this.state.activeFilter}
            activeCompany={this.state.active_company}
          />
        );
    }
  }

  _renderHeaderLinks() {
    return (
      <div className={wrapDiv}>
        <div className={leftHeader}>

          <div className={heading}>
            {this.state.activeCompany}
            <div className={btn} onClick={() => this._back()}>
              Back
            </div>
          </div>


          <div className={row}>
            <TabWrapper tabCount={6}>
              <TabItem
                isActive={this.state.activeGraph === "demographics"}
                onClickFn={() => this._setActiveGraph("demographics")}
              >
                <small>DEMOGRAPHICS</small>
              </TabItem>

              <TabItem
                isActive={this.state.activeGraph === "readiness"}
                onClickFn={() => this._setActiveGraph("readiness")}
              >
                <small>READINESS TO FUNCTIONAL MEDICINE</small>
              </TabItem>

              <TabItem
                isActive={this.state.activeGraph === "healthscore"}
                onClickFn={() => this._setActiveGraph("healthscore")}
              >
                <small>HEALTH RISK SCORE AND HEALTH STATUS</small>
              </TabItem>

              <TabItem
                isActive={this.state.activeGraph === "downstream"}
                onClickFn={() => this._setActiveGraph("downstream")}
              >
                <small>DOWNSTREAM MANIFESTATIONS</small>
              </TabItem>

              <TabItem
                isActive={this.state.activeGraph === "msq"}
                onClickFn={() => this._setActiveGraph("msq")}
              >
                <small>MEDICAL SYMPTOMS QUESTIONNAIRE</small>
              </TabItem>

              <TabItem
                isActive={this.state.activeGraph === "upstream"}
                onClickFn={() => this._setActiveGraph("upstream")}
              >
                <small>UPSTREAM MANIFESTATIONS</small>
              </TabItem>
            </TabWrapper>
          </div>
        </div>

        {this._filters()}
      </div>
    );
  }

  _filters = () => {
    switch (this.state.activeGraph) {
      case "readiness":
        return (
          <div className={filterRight} style={{ border: "1px solid" }}>
            <div className={labelStyle}>Filter By</div>
            <div>
              <a
                className={`${readinessFilterStyle} ${
                  this.state.activeFilter.filter === "branch"
                    ? readinessFilterStyleActive
                    : ""
                  }`}
                onClick={() => this._setFilter("branch")}
              >
                Business Area
                    </a>
              <a
                className={`${readinessFilterStyle} ${
                  this.state.activeFilter.filter === "dept"
                    ? readinessFilterStyleActive
                    : ""
                  }`}
                onClick={() => this._setFilter("dept")}
              >
                Department
                    </a>
              <a
                className={`${readinessFilterStyle} ${
                  this.state.activeFilter.filter === "job_grade"
                    ? readinessFilterStyleActive
                    : ""
                  }`}
                onClick={() => this._setFilter("job_grade")}
              >
                Job Grade
                    </a>
              <a
                className={`${readinessFilterStyle} ${
                  this.state.activeFilter.filter === "high_risk"
                    ? readinessFilterStyleActive
                    : ""
                  }`}
                onClick={() => this._setFilter("high_risk")}
              >
                Status
                    </a>
              <a
                className={`${readinessFilterStyle} ${
                  this.state.activeFilter.filter === "age_group"
                    ? readinessFilterStyleActive
                    : ""
                  }`}
                onClick={() => this._setFilter("age_group")}
              >
                Age Group
                    </a>
            </div>
          </div>
        );

      default:
        return (
          <div className={filterRight}>
            <DefaultFilters
              filters={this._getUserFilters}
              filterData={this._getFilteredData}
              activeGraph={this.state.activeGraph}
            />
          </div>
        );
    }
  }

  render() {
    return (
      <Card styles={{ minHeight: 585 }}>
        <div className={`wrap ${wrapper} wrapper`}>{this._renderView()}</div>
      </Card>
    );
  }
}

const tableWrapper = css(ReactTableStyle, {
  display: "block",
  width: "100%"
});

let wrapper = css({
  boxSizing: "border-box",
  "> div": {
    display: "flex",
    justifyContent: "center"
  }
});

let activeGraphStyle = css({
  color: "#fff !important",
  pointer: "default",
  border: "1px solid #364563",
  backgroundColor: "#1b75bb !important",
  ":hover": {
    textShadow: "none"
  }
});

let readinessFilterStyleActive = css({
  color: "#fff !important",
  pointer: "default",
  border: "none",
  backgroundColor: "#1b75bb !important",
  ":hover": {
    textShadow: "none"
  }
});

let heading = css({
  textAlign: "left",
  display: "flex",
  flexWrap: "wrap",
  fontWeight: "bold",
  fontSize: "30px",
  width: "100%",
  marginBottom: "10px",
  marginLeft: "15px"
});

let readinessFilterStyle = css({
  display: "block",
  cursor: "pointer",
  fontSize: "12px",
  fontWeight: "600",
  backgroundColor: "#f5f8ff",
  borderBottom: "1px dotted #364563",
  color: "#000",
  //margin: "2px 10px",
  padding: "2px",
  textAlign: "center",
  ":hover": {
    color: "#fff",
    backgroundColor: "#1b75bb !important"
  }
});

let graphLink = css({
  display: "block",
  cursor: "pointer",
  fontSize: "12px",
  fontWeight: "600",
  backgroundColor: "#36c4f1",
  color: "#FFF",
  margin: "2px 10px",
  padding: "2px",
  textAlign: "center",
  ":hover": {
    color: "#fff",
    backgroundColor: "#1b75bb !important"
  }
});

let leftHeader = css({
  float: "left",
  width: "100%"
});

let filterRight = css({
  width: "34%",
  marginTop: 20
});

let row = css({
  width: "100%",
  ":after": {
    content: "",
    display: "table",
    clear: "both"
  }
});

let column = css({
  float: "left",
  width: "50%",
  "@media(max-width: 600px)": {
    width: "100%"
  }
});

let wrapDiv = css({
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "space-around",
  width: "100%"
});

let labelWrapper = css({
  width: "100%",
  margin: "20px 5px 20px 5px",
  display: "flex",
  justifyContent: "flex-start",
  flexWrap: "wrap"
});

let labelStyle = css({
  background: "#364563",
  color: "#fff",
  padding: "5px 15px",
  textTransform: "uppercase",
  //fontSize: '12px',
  fontWeight: "bold"
});

const reportLinkStyle = css({
  border: "1px solid #1b75bb",
  color: "#fff",
  background: "#36c4f1",
  padding: "1px 10px",
  alignSelf: "center",
  cursor: "pointer",
  ":hover": {
    color: "#fff",
    backgroundColor: "#1b75bb !important"
  }
});

const btn = css({
  textAlign: "center",
  padding: "5px",
  fontSize: "small",
  background: "#80cde9",
  color: "#fff",
  width: "100px",
  border: "1px solid",
  cursor: "pointer",
  borderRadius: "6px",
  display: "inline",
  marginLeft: "50px",
  ":hover": {
    background: "#1b75bb"
  }
});

let noDataDiv = css({
  display: "flex",
  background: "#52617f",
  color: "#fff",
  fontSize: 16,
  flex: 1,
  textTransform: "capitalize",
  height: "100px",
  justifyContent: "center",
  alignItems: "center"
});
