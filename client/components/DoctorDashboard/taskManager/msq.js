/* eslint-disable radix */
import React from "react";
import {
  ResponsiveContainer,
  BarChart,
  Bar,
  Label,
  LabelList,
  Cell,
  ScatterChart,
  Scatter,
  ReferenceLine,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip
} from "recharts";
import CLabel from "../../NewSkin/Components/Label";
import { css } from "glamor";
import { chart_tooltip } from "../../NewSkin/Styles";
import Button from "../../NewSkin/Components/Button";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      msqData: [],
      msqData_raw: [],
      activeGraph: "msqScores",
      msqScoresData: [],
      msqScoresData_raw: [],
      msqCountsPerGroup: {
        optimal: 0,
        mild: 0,
        moderate: 0,
        severe: 0
      },
      activeFilter: {
        filter: "",
        value: ""
      }
    };
  }

  componentWillMount() {
    this._getMSQData();
    this._getMSQScoresPerSystem();
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.state.activeFilter.filters !== nextProps.activeFilter.filters ||
      this.state.activeFilter.value !== nextProps.activeFilter.value
    ) {
      // update filters
      const activeFilter = this.state.activeFilter;
      activeFilter.filter = nextProps.activeFilter.filter;
      activeFilter.value = nextProps.activeFilter.value;
      activeFilter.filters = nextProps.activeFilter.filters;

      this.setState({ activeFilter }, function () {
        this._getFilteredData();
      });
    }
  }

  multiFilter(array, filters) {
    const filterKeys = Object.keys(filters);
    return array.filter(item =>
      filterKeys.every(key => !!~filters[key].indexOf(item[key]))
    );
  }

  _getFilteredData() {
    const activeFilter = this.state.activeFilter;
    const msqData_raw = this.state.msqData_raw;
    const msqScoresData_raw = this.state.msqScoresData_raw;
    let filtered = msqData_raw;
    let filtered_scores = msqScoresData_raw;

    if (
      activeFilter.filters &&
      activeFilter.filters.branch &&
      activeFilter.filters.branch.length > 0
    ) {
      const filters = { branch: activeFilter.filters.branch };
      filtered = this.multiFilter(filtered, filters);
      filtered_scores = this.multiFilter(filtered_scores, filters);
    }

    if (
      activeFilter.filters &&
      activeFilter.filters.department &&
      activeFilter.filters.department.length > 0
    ) {
      const filters = { department: activeFilter.filters.department };
      filtered = this.multiFilter(filtered, filters);
      filtered_scores = this.multiFilter(filtered_scores, filters);
    }

    if (
      activeFilter.filters &&
      activeFilter.filters.jobgrade &&
      activeFilter.filters.jobgrade.length > 0
    ) {
      const filters = { job_grade: activeFilter.filters.jobgrade };
      filtered = this.multiFilter(filtered, filters);
      filtered_scores = this.multiFilter(filtered_scores, filters);
    }

    if (
      activeFilter.filters &&
      activeFilter.filters.high_risk &&
      activeFilter.filters.high_risk.length > 0
    ) {
      const filters = { high_risk: activeFilter.filters.high_risk };
      filtered = this.multiFilter(filtered, filters);
      filtered_scores = this.multiFilter(filtered_scores, filters);
    }

    if (
      activeFilter.filters &&
      activeFilter.filters.age_group &&
      activeFilter.filters.age_group.length > 0
    ) {
      const filters = { age_group: activeFilter.filters.age_group };
      filtered = this.multiFilter(filtered, filters);
      filtered_scores = this.multiFilter(filtered_scores, filters);
    }

    this._processGraph(filtered_scores);
    this._processMSQDataGraph(filtered);
  }

  _getMSQScoresPerSystem() {
    this.props
      .msqScoresPerSystem()
      .then(res => {
        this.setState({
          msqScoresData_raw: res
        });
        // process graph values
        this._processGraph(res);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _processGraph(res) {
    const msqScoresData = [];
    let digestive_cnt = 0,
      ears_cnt = 0,
      emotions_cnt = 0,
      energy_cnt = 0,
      eyes_cnt = 0,
      head_cnt = 0,
      heart_cnt = 0,
      jointsMuscles_cnt = 0,
      lungs_cnt = 0,
      mind_cnt = 0,
      mouthThroat_cnt = 0,
      nose_cnt = 0,
      other_cnt = 0,
      skin_cnt = 0,
      weight_cnt = 0;

    res.forEach(data => {
      digestive_cnt += parseInt(data.digestive_score) > 10 ? 1 : 0;
      ears_cnt += parseInt(data.ears_score) > 10 ? 1 : 0;
      emotions_cnt += parseInt(data.emotions_score) > 10 ? 1 : 0;
      energy_cnt += parseInt(data.energy_score) > 10 ? 1 : 0;
      eyes_cnt += parseInt(data.eyes_score) > 10 ? 1 : 0;
      head_cnt += parseInt(data.head_score) > 10 ? 1 : 0;
      heart_cnt += parseInt(data.heart_score) > 10 ? 1 : 0;
      jointsMuscles_cnt += parseInt(data.joints_and_muscles_score) > 10 ? 1 : 0;
      lungs_cnt += parseInt(data.lungs_score) > 10 ? 1 : 0;
      mind_cnt += parseInt(data.mind_score) > 10 ? 1 : 0;
      mouthThroat_cnt += parseInt(data.mouth_and_throat_score) > 10 ? 1 : 0;
      nose_cnt += parseInt(data.nose_score) > 10 ? 1 : 0;
      other_cnt += parseInt(data.other_score) > 10 ? 1 : 0;
      skin_cnt += parseInt(data.skin_score) > 10 ? 1 : 0;
      weight_cnt += parseInt(data.weight_score) > 10 ? 1 : 0;
    });
    msqScoresData.push({
      digestive: digestive_cnt,
      ears: ears_cnt,
      emotions: emotions_cnt,
      energy: energy_cnt,
      eyes: eyes_cnt,
      head: head_cnt,
      heart: heart_cnt,
      jointsMuscles: jointsMuscles_cnt,
      lungs: lungs_cnt,
      mind: mind_cnt,
      mouthThroat: mouthThroat_cnt,
      nose: nose_cnt,
      other: other_cnt,
      skin: skin_cnt,
      weight: weight_cnt
    });
    this.setState({
      msqScoresData
    });
  }

  _getMSQData() {
    this.props
      .data()
      .then(result => {
        const res = result.filter(d => d.healthsurvey_status === "Locked"); // FILTER, ONLY PATIENT SUBMITTED HEALTH SURVEY

        this.setState({
          msqData_raw: res
        });
        // process graph values
        this._processMSQDataGraph(res);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _processMSQDataGraph(res) {
    let id = 0;
    const msqData = [];
    const msqCounts = this.state.msqCountsPerGroup;
    msqCounts.optimal = 0;
    msqCounts.mild = 0;
    msqCounts.moderate = 0;
    msqCounts.severe = 0;
    res.forEach(data => {
      const color = this._getHealthColor(data.value, msqCounts);
      id++;
      msqData.push({
        id,
        user_id: data.user_id,
        employee_id: data.employee_id,
        score: data.value,
        color
      });
    });
    this.setState({
      msqData,
      msqCounts
    });
  }

  _getHealthColor(value, msqCounts) {
    const score = !NaN ? value : 0;

    if (score < 10) {
      msqCounts.optimal++;
      return "#72a153"; //  green=>optimal
    } else if (score <= 50) {
      msqCounts.mild++;
      return "#f7e118"; //  yellow=>mild
    } else if (score <= 100) {
      msqCounts.moderate++;
      return "#d77f1a"; //  orange=>moderate
    }
    msqCounts.severe++;
    return "red"; //  red=>severe
  }

  _changeGraph() {
    if (this.state.activeGraph == "msqScores") {
      this.setState({ activeGraph: "msqScores_perSystem" });
    } else {
      this.setState({ activeGraph: "msqScores" });
    }
  }

  _renderBarChart() {
    const TooltipContent = (props) => {
      if (props.active) {
        return (
          <div className={chart_tooltip}>
            <p>{props.payload[0].payload.name}</p>
            <label>Count: {props.payload[0].payload.value}</label>
          </div>
        );
      }

      return null;
    };
    const msqPerSystem = this.state.msqScoresData[0];
    const data = [
      { name: "Weight", value: msqPerSystem.weight },
      { name: "Skin", value: msqPerSystem.skin },
      { name: "Other", value: msqPerSystem.other },
      { name: "Nose", value: msqPerSystem.nose },
      { name: "Mouth and Throat", value: msqPerSystem.mouthThroat },
      { name: "Mind", value: msqPerSystem.mind },
      { name: "Lungs", value: msqPerSystem.lungs },
      { name: "Joints and Muscles", value: msqPerSystem.jointsMuscles },
      { name: "Heart", value: msqPerSystem.heart },
      { name: "Head", value: msqPerSystem.head },
      { name: "Eyes", value: msqPerSystem.eyes },
      { name: "Energy", value: msqPerSystem.energy },
      { name: "Emotions", value: msqPerSystem.emotions },
      { name: "Ears", value: msqPerSystem.ears },
      { name: "Digestive", value: msqPerSystem.digestive }
    ];
    return (
      <div className={chartDiv}>
        <CLabel text="Counts of Users with MSQ scores >10 Per System" />
        <ResponsiveContainer width="90%" height={800}>
          <BarChart
            width={this.props.fromReport ? 600 : 1000}
            height={700}
            layout="vertical"
            data={data}
            margin={{ top: 20, right: 30, left: 20, bottom: 5 }}
          >
            <XAxis type="number" style={{ fontSize: 'smaller' }} />
            <YAxis type="category" dataKey="name" width={150} style={{ fontSize: 'smaller' }} />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip content={TooltipContent} />
            <Bar dataKey="value" stackId="a" fill="#81cce7">
              <LabelList dataKey="value" style={{ fontSize: 'small' }} position="right" />
            </Bar>
          </BarChart>
        </ResponsiveContainer>
      </div>
    );
  }

  _showTooltip = data => {
    if (data.active) {
      const employee_id = data.payload[0].payload.employee_id || '-';
      const healthscore = data.payload[0].payload.score;
      return (
        <div className={chart_tooltip}>
          <p>Employee ID: {employee_id}</p>
          <label>MSQ Score: {healthscore}</label>
        </div>
      );
    }
  };

  _renderChart() {
    const data = this.state.msqData;
    return (
      <div className={`${chartDiv} chartDiv`}>
        <CLabel text="Medical Symptoms Questionnaire Scores" />
        <ResponsiveContainer width="90%" height={300}>
          <ScatterChart
            height={350}
            width={600}
            data={data}
            margin={{ top: 10, right: 150, bottom: 20, left: 0 }}
          >
            <XAxis dataKey={"id"} hide domain={[0, "dataMax"]} />
            <YAxis
              dataKey={"score"}
              type="number"
              name="scores"
              ticks={[0, 20, 40, 60, 80, 100, 120, 140]}
              style={{ fontSize: 'smaller' }}
            >
              <Label
                value="MSQ Scores"
                offset={25}
                position="insideLeft"
                angle={-90}
              />
            </YAxis>
            <CartesianGrid />
            <Scatter name="users" data={data} fill="#8884d8">
              {data.map((entry, index) => (
                <Cell key={`cell-${index}`} fill={data[index].color} />
              ))}
            </Scatter>
            <Tooltip
              cursor={{ strokeDasharray: "3 3" }}
              content={this._showTooltip.bind(this)}
            />
            <ReferenceLine
              width={this.props.fromReport && 10}
              y={0}
              label={{ position: "right", value: "Optimal", fontSize: 13 }}
              stroke="#72a153"
            />
            <ReferenceLine
              y={10}
              label={{
                position: "right",
                value: "Mild toxicity",
                fontSize: 13
              }}
              stroke="#f7e118"
            />
            <ReferenceLine
              y={50}
              label={{
                position: "right",
                value: "Moderate Toxicity",
                fontSize: 13
              }}
              stroke="#d77f1a"
            />
            <ReferenceLine
              y={100}
              label={{
                position: "right",
                value: "Severe Toxicity",
                fontSize: 13
              }}
              stroke="red"
            />
          </ScatterChart>
        </ResponsiveContainer>
      </div>
    );
  }

  _drawCircle(color) {
    return (
      <svg height="21" width="25">
        <circle cx="11" cy="11" r="6" stroke="black" stroke="0" fill={color} />
      </svg>
    );
  }
  render() {
    const msqCounts = this.state.msqCountsPerGroup;
    let note = "";
    if (this.state.activeGraph == "msqScores") {
      note = (
        <p className={noteStyle}>
          <span style={{ fontStyle: "normal", fontWeight: 400 }}>Note: Scores are based on the cumulative grading of the intensity of
          symptoms</span>
          <br />
          <br />
          Optimal: {msqCounts.optimal} (
          {msqCounts.optimal > 0
            ? ((msqCounts.optimal / this.state.msqData.length) * 100).toFixed(2)
            : 0}
          %)
          <br />
          Mild Toxicity: {msqCounts.mild} (
          {msqCounts.mild > 0
            ? ((msqCounts.mild / this.state.msqData.length) * 100).toFixed(2)
            : 0}
          %)
          <br />
          Moderate Toxicity: {msqCounts.moderate} (
          {msqCounts.moderate > 0
            ? ((msqCounts.moderate / this.state.msqData.length) * 100).toFixed(
              2
            )
            : 0}
          %)
          <br />
          Severe Toxicity: {msqCounts.severe} (
          {msqCounts.severe > 0
            ? ((msqCounts.severe / this.state.msqData.length) * 100).toFixed(2)
            : 0}
          %)
        </p>
      );
    }
    return (
      <div style={{ width: !this.props.fromReport ? "1020px" : undefined }} className={!this.props.fromReport ? msqWrapper : undefined}>
        {
          !this.props.fromReport || (this.props.fromReport && this.props.showButton) ?
            <Button type="darkpink" left fitContent styles={{ marginBottom: 0, borderRadius: 8, fontSize: 'small', marginLeft: 20, marginTop: -20 }}>
              <button onClick={this._changeGraph.bind(this)}>
                {this.state.activeGraph == "msqScores"
                  ? "Show Counts of Users with MSQ scores >10 Per System"
                  : "Show MSQ Scores"}
              </button>
            </Button> : null
        }
        {this.state.activeGraph == "msqScores"
          ? this._renderChart()
          : this._renderBarChart()}
        {note}
      </div>
    );
  }
}

// styles

let msqWrapper = css({
  padding: 30,
  // boxShadow: "0 8px 20px rgba(0,0,0,.050)",
  marginTop: 30
});

let chartDiv = css({
  display: "flex",
  flexWrap: "wrap",
  width: "100%",
  justifyContent: "center",
  marginTop: "20px",
  "@media(max-width: 1080px)": {
    width: "100%"
  }
});

let link = css({
  margin: "0 auto",
  color: "#000",
  cursor: "pointer",
  textTransform: "uppercase",
  fontSize: "12px",
  fontWeight: "bold",
  ":hover": {
    color: "#364563"
  },
  backgroundColor: "#eec5e2",
  maxWidth: 420,
  textAlign: "center",
  padding: "5px",
  "@media print": {
    display: "none"
  }
});

let asflexTitle = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "space-around",
  padding: "5px",
  fontWeight: "bold",
  fontSize: 16,
  textTransform: "uppercase"
});

let noteStyle = css({
  fontWeight: 600,
  padding: '0 20px',
  textAlign: "left",
  fontSize: 'smaller'
});
