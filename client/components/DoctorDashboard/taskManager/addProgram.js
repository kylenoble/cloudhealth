//import currentDomain from '../../../utils/domain.js';
import {css} from 'glamor'

export default class extends React.Component{

      constructor(props){
            super(props)


      }

      componentDidMount(){

      }

      _test(){
        console.log("testing");
      }

      render(){
        console.log('rend');
        return(
          <div>
          <button onClick={()=>this.props.test()}>sssssssssssssss</button>
          </div>
        )
      }


}


// styling
let show = css({
  display: 'block'
})

let hide = css({
  display: 'none'
})

let activeGraphStyle = css({
  color: 'rgb(128, 205, 233)',
  pointer: 'default'
})

let heading = css({
  textAlign: 'left',
  display: 'flex',
  flexWrap: 'wrap',
  fontWeight: 'bold',
  fontSize: '30px',
  width: '100%',
  marginBottom: '10px',
  marginLeft: '15px'
})

let graphLink = css({
  display: 'block',
  cursor: 'pointer',
  fontSize: '12px',
  fontWeight: '600',
  ':hover': {
    color: 'rgb(128, 205, 233)'
  }
})

let row = css({
  width: '100%',
  ':after': {
    content: '',
    display: 'table',
    clear: 'both'
  },
  marginLeft: '15px'
})

let column = css({
  float: 'left',
  width: '33%',
  '@media(max-width: 600px)': {
      width: '100%'
  }
})

let listStyle = css({
  display: 'flex',
  flexWrap: 'wrap',
  borderBottom: '1px dotted gray',
  justifyContent: 'space-around',
  width: '100%'
})

let wrapDiv = css({
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'space-around',
  width: '100%'
})

let fldsDiv = css({
  display: 'flex',
  flexWrap: 'wrap',
  width: '100%'
})


let itemLinkStyle = css({
  width: '150px',
  padding: '5px',
  textAlign: 'center'

})

let infoLabelStyle = css({
  fontWeight: 'bold',
  color: '#80cde9'
})

let infoDiv = css({
  width: '100%',
  display: 'flex',
  margin: '50px 5px 50px 5px'
})

let imageStyle = css({
  align:"bottom",
  width: '20',
  margin: '0 50px 0 30px'
  // border: '2px solid #36c4f1',
  // border: '1px solid rgba(204, 204, 204, 0.3)',
})

let box = css({
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'flexStart',
  margin: '10px',
  height:'100'
})


let detailsWrapper = css({
  width: '100%',
  display: 'flex',
  flexWrap:'wrap',
  justifyContent: 'center'
})


let labelWrapper = css({
  width: '100%',
  margin: '20px 5px 20px 5px',
  display: 'flex',
  justifyContent: 'flex-start',
  flexWrap: 'wrap'
})


let inputStyle = css({
  width: '300px',
  padding: '5px 5px 5px 10px',
  margin: '5px',

})

let labelStyle = css({
  width: '300px',
  background: '#1b75bb',
  color: '#fff',
  padding: '5px 5px 5px 100px',
  margin: '5px',
  textTransform: 'uppercase'
})

const styles = {
    dashItem: {
    fontFamily: '"Roboto-Black",sans-serif',
    fontWeight: "bold",
    fontSize: "18ps",
    color: "#000",
    letterSpacing: 0,
    lineHeight: "14px",
    padding: "10px 10px",
    margin: '-1px 15px',
    cursor: 'pointer',
    textAlign: 'center',
    textTransform: 'uppercase'
  },
  dashItemActive: {
    color: '#80cde9',
    cursor: 'none',

  },

}

const btn = css({
  textAlign: 'center',
  padding: '5px',
  fontSize: 'small',
  background: '#80cde9',
  color: '#fff',
  width: '100px',
  border: '1px solid',
  cursor: 'pointer',
  borderRadius: '6px',
  display: 'inline',
  marginLeft: '50px'
})
