import React from "react";
import ReactToPrint from "react-to-print";
import PropTypes from "prop-types";
import CustomizeReport from './customizeReport'
class ComponentToPrint extends React.Component {
  render() {
    return (
      <div>
        This is test Paragraph for printing
      </div>
    );
  }
}

class Example extends React.Component {
  render() {
    return (
      <div>
        <ReactToPrint
          trigger={() => <a href="#">Print this out!</a>}
          content={() => this.componentRef}
        />
        <ComponentToPrint ref={el => (this.componentRef = el)} />
      </div>
    );
  }
}

export default ComponentToPrint;
