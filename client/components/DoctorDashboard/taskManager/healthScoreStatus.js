/* eslint-disable no-constant-condition */
/* eslint-disable radix */
/* eslint-disable no-nested-ternary */
/* eslint-disable prefer-const */
/* eslint-disable no-mixed-operators */
import React from "react";
import { css } from "glamor";

import HealthScoreScatterChart from "./healthScoreScatterChart";
import HealthScorePieChart from "./healthSorePieChart";
import HealhScoreBarChart from "./healthScoreBarChart";
import { chart_tooltip } from "../../NewSkin/Styles";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      resizeLock: this.props.resizeLock,
      display: {
        healthScore: true,
        healthStatus: true,
        determinants: true
      },
      healthScores: [],
      healthScores_raw: [],
      healthDeterminants: [],
      healthDeterminants_raw: [],
      healthStatusCount: {
        Optimal: 0,
        SubOptimal: 0,
        Neutral: 0,
        Compromised: 0,
        Alarming: 0,
        Incomplete: 0
      },
      total_healthScore: 0,
      healthScoreValues: [],
      activeFilter: this.props.activeFilter
    };

    this._setDivAlignment = this._setDivAlignment.bind(this);
  }

  componentDidMount() {
    this._getScoresByCompany();
    this._getHealthDeterminants();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.activeFilter && nextProps.activeFilter.filters) {
      if (
        this.state.activeFilter.filters !== nextProps.activeFilter.filters ||
        this.state.activeFilter.value !== nextProps.activeFilter.value
      ) {
        // update filters
        let { activeFilter } = this.state;
        activeFilter.filter = nextProps.activeFilter.filter;
        activeFilter.value = nextProps.activeFilter.value;
        activeFilter.filters = nextProps.activeFilter.filters;

        this.setState({ activeFilter });
      }
    }

    if (nextProps.activeFilter && nextProps.activeFilter.selections) {
      const selections = nextProps.activeFilter.selections;
      const display = this.state.display;
      display.healthScore = selections.score;
      display.healthStatus = selections.status;
      display.determinants = selections.determinants;

      this.setState({
        display
      });
    }
    this._getFilteredData();
  }

  getMedianValue(values) {
    if (values.length === 0) return 0;
    let median = 0;
    const numsLen = values.length;
    values.sort();

    if (numsLen % 2 === 0) {
      // is even
      // average of two middle numbers
      median =
        (parseFloat(values[numsLen / 2 - 1]) +
          parseFloat(values[numsLen / 2])) /
        2;
    } else {
      // is odd
      // middle number only
      median = parseFloat(values[(numsLen - 1) / 2]);
    }
    return parseFloat(median);
  }

  getMeanValue(values) {
    if (values.length === 0) return 0;
    const sum = values.reduce((a, b) => parseFloat(a) + parseFloat(b));
    return parseFloat(sum / values.length).toFixed();
  }

  _getHealthDeterminants() {
    this.props
      .healthDeterminants()
      .then(res => {
        const result = res.filter(d => d.healthsurvey_status === "Locked"); // FILTER, ONLY PATIENT SUBMITTED HEALTH SURVEY
        this.setState({
          healthDeterminants_raw: result
        });
        this._processBarGraph();
      })
      .catch(e => {
        console.log(e);
      });
  }

  _processBarGraph() {
    const activeFilter = this.state.activeFilter;
    const healthDeterminants = this.state.healthDeterminants_raw;

    let filtered_healthDeterminants = healthDeterminants;

    if (
      activeFilter &&
      activeFilter.filters &&
      activeFilter.filters.branch &&
      activeFilter.filters.branch.length > 0
    ) {
      const filters = { branch: activeFilter.filters.branch };
      filtered_healthDeterminants = this.multiFilter(
        filtered_healthDeterminants,
        filters
      );
    }

    if (
      activeFilter &&
      activeFilter.filters &&
      activeFilter.filters.department &&
      activeFilter.filters.department.length > 0
    ) {
      const filters = { department: activeFilter.filters.department };
      filtered_healthDeterminants = this.multiFilter(
        filtered_healthDeterminants,
        filters
      );
    }

    if (
      activeFilter &&
      activeFilter.filters &&
      activeFilter.filters.jobgrade &&
      activeFilter.filters.jobgrade.length > 0
    ) {
      const filters = { job_grade: activeFilter.filters.job_grade };
      filtered_healthDeterminants = this.multiFilter(
        filtered_healthDeterminants,
        filters
      );
    }
    if (
      activeFilter &&
      activeFilter.filters &&
      activeFilter.filters.high_risk &&
      activeFilter.filters.high_risk.length > 0
    ) {
      // filtered_healthDeterminants = filtered_healthDeterminants.filter(str => {
      //   if (activeFilter.filters.high_risk !== "Others") {
      //     return str.high_risk === high_risk_filter;
      //   }
      //   return str.high_risk == null || str.high_risk === 0;
      // });

      const filters = { high_risk: activeFilter.filters.high_risk };
      filtered_healthDeterminants = this.multiFilter(
        filtered_healthDeterminants,
        filters
      );
    }

    if (
      activeFilter &&
      activeFilter.filters &&
      activeFilter.filters.age_group &&
      activeFilter.filters.age_group.length > 0
    ) {
      const filters = { age_group: activeFilter.filters.age_group };
      filtered_healthDeterminants = this.multiFilter(
        filtered_healthDeterminants,
        filters
      );
    }

    let sleep_values = [];
    let weight_values = [];
    let nutrition_values = [];
    let exercise_values = [];
    let stress_values = [];
    let detox_values = [];
    const graphData = [];

    filtered_healthDeterminants.forEach(data => {
      const val = data.value;
      const name = data.name;
      if (name === "sleep") {
        sleep_values.push(parseFloat(val));
      } else if (name === "weightScore") {
        weight_values.push(parseFloat(val));
      } else if (name === "nutrition") {
        nutrition_values.push(parseFloat(val));
      } else if (name === "exercise") {
        exercise_values.push(parseFloat(val));
      } else if (name === "stress") {
        stress_values.push(parseFloat(val));
      } else if (name === "detox") {
        detox_values.push(parseFloat(val));
      }
    });
    sleep_values.sort();
    weight_values.sort();
    nutrition_values.sort();
    exercise_values.sort();
    stress_values.sort();
    detox_values.sort();
    graphData.push(
      {
        determinant: "Sleep",
        "Mean Score":
          sleep_values.length > 0 ? this.getMeanValue(sleep_values) * 2 : 0,
        "Median Score":
          sleep_values.length > 0
            ? `Median Score: ${(
              this.getMedianValue(sleep_values) * 2
            ).toFixed()}\nRange: ${
            sleep_values[0] > 0
              ? (parseFloat(sleep_values[0]) * 2).toFixed()
              : 0
            } - ${(
              parseFloat(sleep_values[sleep_values.length - 1]) * 2
            ).toFixed()}`
            : null,
        color: this._getHealthColor(this.getMeanValue(sleep_values) * 2)
      },
      {
        determinant: "Weight",
        "Mean Score":
          weight_values.length > 0 ? this.getMeanValue(weight_values) * 2 : 0,
        "Median Score":
          weight_values.length > 0
            ? `Median Score: ${(
              this.getMedianValue(weight_values) * 2
            ).toFixed()}\nRange: ${
            weight_values[0] > 0
              ? (parseFloat(weight_values[0]) * 2).toFixed()
              : 0
            } - ${(
              parseFloat(weight_values[weight_values.length - 1]) * 2
            ).toFixed()}`
            : null,
        color: this._getHealthColor(this.getMeanValue(weight_values) * 2)
      },
      {
        determinant: "Nutrition",
        "Mean Score":
          nutrition_values.length > 0
            ? this.getMeanValue(nutrition_values) * 2
            : 0,
        "Median Score":
          nutrition_values.length > 0
            ? `Median Score: ${(
              this.getMedianValue(nutrition_values) * 2
            ).toFixed()}\nRange: ${
            nutrition_values[0] > 0
              ? (parseFloat(nutrition_values[0]) * 2).toFixed()
              : 0
            } - ${(
              parseFloat(nutrition_values[nutrition_values.length - 1]) * 2
            ).toFixed()}`
            : null,
        color: this._getHealthColor(this.getMeanValue(nutrition_values) * 2)
      },
      {
        determinant: "Movement",
        "Mean Score":
          exercise_values.length > 0
            ? this.getMeanValue(exercise_values) * 2
            : 0,
        "Median Score":
          exercise_values.length > 0
            ? `Median Score: ${(
              this.getMedianValue(exercise_values) * 2
            ).toFixed()}\nRange: ${
            exercise_values[0] > 0
              ? (parseFloat(exercise_values[0]) * 2).toFixed()
              : 0
            } - ${(
              parseFloat(exercise_values[exercise_values.length - 1]) * 2
            ).toFixed()}`
            : null,
        color: this._getHealthColor(this.getMeanValue(exercise_values) * 2)
      },
      {
        determinant: "Stress",
        "Mean Score":
          stress_values.length > 0 ? this.getMeanValue(stress_values) * 2 : 0,
        "Median Score":
          stress_values.length > 0
            ? `Median Score: ${(
              this.getMedianValue(stress_values) * 2
            ).toFixed()}\nRange: ${
            stress_values[0] > 0
              ? (parseFloat(stress_values[0]) * 2).toFixed()
              : 0
            } - ${(
              parseFloat(stress_values[stress_values.length - 1]) * 2
            ).toFixed(2)}`
            : null,
        color: this._getHealthColor(this.getMeanValue(stress_values) * 2)
      },
      {
        determinant: "Detox",
        "Mean Score":
          detox_values.length > 0 ? this.getMeanValue(detox_values) * 2 : 0,
        "Median Score":
          detox_values.length > 0
            ? `Median Score: ${(
              this.getMedianValue(detox_values) * 2
            ).toFixed()}\nRange: ${
            detox_values[0] > 0
              ? (parseFloat(detox_values[0]) * 2).toFixed()
              : 0
            } - ${(
              parseFloat(detox_values[detox_values.length - 1]) * 2
            ).toFixed()}`
            : null,
        color: this._getHealthColor(this.getMeanValue(detox_values) * 2)
      }
    );
    this.setState({
      healthDeterminants: graphData
    });
  }

  multiFilter(array, filters) {
    const filterKeys = Object.keys(filters);
    let result = filters.length > 0 && array.filter(item =>
      filterKeys.every(key => !!~filters[key].indexOf(item[key]))
    );
    return result;
  }

  _getFilteredData() {
    this._processGraph();
    this._processBarGraph();
  }

  _getScoresByCompany() {
    this.props
      .getScoresByCompany()
      .then(result => {
        const res = result.filter(d => d.healthsurvey_status === "Locked"); // FILTER, ONLY PATIENT SUBMITTED HEALTH SURVEY

        this.setState({
          healthScores_raw: res
        });
        // process graph values
        this._processGraph();
      })
      .catch(e => {
        console.log(e);
      });
  }

  _processGraph() {
    const activeFilter = this.state.activeFilter;
    const healthScores = this.state.healthScores_raw;

    let filtered = healthScores;

    if (
      activeFilter &&
      activeFilter.filters &&
      activeFilter.filters.branch &&
      activeFilter.filters.branch.length > 0
    ) {
      const filters = { branch: activeFilter.filters.branch };
      filtered = this.multiFilter(filtered, filters);
    }

    if (
      activeFilter &&
      activeFilter.filters &&
      activeFilter.filters.department &&
      activeFilter.filters.department.length > 0
    ) {
      const filters = { department: activeFilter.filters.department };
      filtered = this.multiFilter(filtered, filters);
    }

    if (
      activeFilter &&
      activeFilter.filters &&
      activeFilter.filters.jobgrade &&
      activeFilter.filters.jobgrade.length > 0
    ) {
      const filters = { job_grade: activeFilter.filters.job_grade };
      filtered = this.multiFilter(filtered, filters);
    }

    // THIS PART IS FOR VEREFICATION WHY "high_risk_filter" IS NOT USED
    if (
      activeFilter &&
      activeFilter.filters &&
      activeFilter.filters.high_risk &&
      activeFilter.filters.high_risk.length > 0
    ) {
      let high_risk_convert = activeFilter.filters.high_risk.map(item => {
        if (item === 1) {
          return "High Value";
        } else if (item === 2) {
          return "High Risk";
        }
        return "Others";
      });
      const filters = { high_risk: high_risk_convert };
      filtered = this.multiFilter(filtered, filters);
    }

    if (
      activeFilter &&
      activeFilter.filters &&
      activeFilter.filters.age_group &&
      activeFilter.filters.age_group.length > 0
    ) {
      const filters = { age_group: activeFilter.filters.age_group };
      filtered = this.multiFilter(filtered, filters);
    }

    const graph_data = [];
    const values = [];
    let id = 0;

    this.state.total_healthScore = 0;
    this.state.healthStatusCount.Optimal = 0;
    this.state.healthStatusCount.SubOptimal = 0;
    this.state.healthStatusCount.Neutral = 0;
    this.state.healthStatusCount.Compromised = 0;
    this.state.healthStatusCount.Incomplete = 0;

    filtered.forEach(value => {
      id++;
      graph_data.push({
        id,
        user_id: value.user_id,
        employee_id: value.employee_id,
        healthscore: parseFloat(value.healthscore),
        color: this._getHealthColor(
          parseFloat(value.healthscore),
          value.survey_count
        )
      });
      values.push(parseFloat(value.healthscore).toFixed());
      this.state.total_healthScore += parseFloat(value.healthscore);
    });
    this.setState({
      healthScores: graph_data,
      healthScoreValues: values
    });
  }

  _getHealthColor(value, survey_count) {
    const score = !NaN ? value : 0;

    if (survey_count != null && survey_count < 6) {
      this.state.healthStatusCount.Incomplete += 1;
      return "#9c9e9e"; //gray
    }

    if (score >= 0 && score < 3) {
      if (survey_count != null) {
        this.state.healthStatusCount.Optimal += 1;
      }
      return "#72a153"; //green
    } else if (score >= 3.0 && score < 5) {
      if (survey_count != null) {
        this.state.healthStatusCount.SubOptimal += 1;
      }
      return "#71b2ca"; //blue
    } else if (score >= 5.0 && score < 6) {
      if (survey_count != null) {
        this.state.healthStatusCount.Neutral += 1;
      }
      return "#dfaf29"; //yellow
    } else if (score >= 6.0 && score < 8.6) {
      if (survey_count != null) {
        this.state.healthStatusCount.Compromised += 1;
      }
      return "#d77f1a"; //orange
    } else if (score >= 8.6 && score <= 10.0) {
      if (survey_count != null) {
        this.state.healthStatusCount.Alarming += 1;
      }
      return "red";
    }
  }

  _renderChart() {
    const resizeDivNameSC = "HSScatterChart";
    const resizeDivNamePC = "HSPieChart";
    const resizeDivNameBC = "HSBarChart";
    const display = this.state.display;
    return (
      <div className={wrapper}>
        <div className={insidewrapper}>
          {display.healthScore ? (
            <HealthScoreScatterChart
              fromReport={this.props.fromReport}
              resizeLock={this.state.resizeLock}
              resizeDivName={resizeDivNameSC}
              healthScores={this.state.healthScores}
              total_healthScore={this.state.total_healthScore}
              healthScoreValues={this.state.healthScoreValues}
              getMedianValue={this.getMedianValue}
              drawCircle={this._drawCircle}
              setDivAlignment={this._setDivAlignment}
              activeFilter={this.state.activeFilter}
              boxProps={this.props.boxProps}
            />
          ) : null}
          {display.healthStatus ? (
            <HealthScorePieChart
              fromReport={this.props.fromReport}
              resizeLock={this.state.resizeLock}
              resizeDivName={resizeDivNamePC}
              healthStatusCount={this.state.healthStatusCount}
              setDivAlignment={this._setDivAlignment}
              activeFilter={this.state.activeFilter}
              boxProps={this.props.boxProps}
            />
          ) : null}

          {display.determinants ? (
            <HealhScoreBarChart
              fromReport={this.props.fromReport}
              resizeLock={this.state.resizeLock}
              resizeDivName={resizeDivNameBC}
              healthDeterminants={this.state.healthDeterminants}
              setDivAlignment={this._setDivAlignment}
              activeFilter={this.state.activeFilter}
              boxProps={this.props.boxProps}
            />
          ) : null}
        </div>
      </div>
    );
  }

  _setDivAlignment(boxProps, name) {
    this.props.setDivAlignment(boxProps, name);
  }

  _drawCircle(color) {
    return (
      <svg height="21" width="25">
        <circle cx="11" cy="11" r="5" stroke="black" stroke="0" fill={color} />
      </svg>
    );
  }

  render() {
    return <div className={divWrapper}>{this._renderChart()}</div>;
  }
}

let wrapper = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "center",
  marginTop: 10,
  "@media print": {
    display: "block !important",
    pageBreakInside: "avoid",
    pageBreakAfter: "auto"
  }
});

let insidewrapper = css({
  //border: "1px solid red",
  //position: "relative",
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "center",
  marginTop: 10,
  "@media print": {
    display: "block !important",
    pageBreakInside: "avoid",
    pageBreakAfter: "auto"
  }
});

let divWrapper = css({
  textAlign: "center",
  marginTop: "10px",
  width: "100%"
});
