import React from "react";
import { css } from "glamor";

import {
  ResponsiveContainer,
  PieChart,
  Pie,
  Cell,
  Tooltip,
  Legend
} from "recharts";
import CLabel from "../../NewSkin/Components/Label";
import { ResizableDiv } from "../../Reports/Parts";
import { chart_tooltip } from "../../NewSkin/Styles";

const RenderPieChart = props => {
  const healthStatCnt = props.healthStatusCount;
  const resizeDivName = props.resizeDivName;
  const boxProps = props.boxProps;
  const data = [
    { name: "Optimal", value: healthStatCnt.Optimal },
    { name: "SubOptimal", value: healthStatCnt.SubOptimal },
    { name: "Neutral", value: healthStatCnt.Neutral },
    { name: "Compromised", value: healthStatCnt.Compromised },
    { name: "Alarming", value: healthStatCnt.Alarming },
    { name: "Incomplete Data", value: healthStatCnt.Incomplete }
  ];

  const COLORS = ["#72a153", "#71b2ca", "#dfaf29", "#d77f1a", "red", "#9c9e9e"];

  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent
  }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.2;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);
    const percentage = (percent * 100).toFixed(0);
    if (percentage > 0) {
      return (
        <text
          x={x}
          y={y}
          fill="white"
          textAnchor={x > cx ? "start" : "end"}
          dominantBaseline="bottom"
          style={{ fontSize: 'smaller' }}
        >
          {`${(percent * 100).toFixed(0)}%`}
        </text>
      );
    }
  };

  const TooltipContent = (props) => {
    if (props.active) {
      return (
        <div className={chart_tooltip}>
          <p>{props.payload[0].payload.name}</p>
          <label>Count: {props.payload[0].payload.value}</label>
        </div>
      );
    }

    return null;
  };

  return (
    <React.Fragment>
      <CLabel text="Distribution of Health Status" fullWidth styles={{ textAlign: 'center' }} />

      <ResizableDiv
        isGraph
        noOption
        resizeLock={props.resizeLock}
        name={resizeDivName}
        setDivAlignment={props.setDivAlignment}
        maxWidth={800}
        defaultSize={{ width: 800, height: 300 }}
        filters={boxProps}
      >
        <ResponsiveContainer width="100%">
          <PieChart
            margin={{
              top: 0,
              right: 0,
              bottom: 30,
              left: 0
            }}
          >
            <Pie
              dataKey="value"
              data={data}
              labelLine={false}
              label={renderCustomizedLabel}
              isAnimationActive={false}
              outerRadius={"100%"}
              innerRadius={"50%"}
              fill="#8884d8"
            >
              {data.map((entry, index) => (
                <Cell key={index} fill={COLORS[index % COLORS.length]} />
              ))}
            </Pie>
            {!props.fromReport && <Legend />}
            <Tooltip content={TooltipContent} />
          </PieChart>
        </ResponsiveContainer>
      </ResizableDiv>
    </React.Fragment>
  );
};

export default RenderPieChart;

let asflexTitle = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "space-around",
  padding: "5px",
  fontWeight: "bold",
  fontSize: 16,
  textTransform: "uppercase"
});
