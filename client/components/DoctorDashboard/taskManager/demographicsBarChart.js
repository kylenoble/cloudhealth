/* eslint-disable eqeqeq */
import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer
} from "recharts";

import { css } from "glamor";
import { ResizableDiv } from "../../Reports/Parts";
import { chart_tooltip } from "../../NewSkin/Styles";

const DemographicsPieChart = props => {
  const { data, noGenerCtr, resizeDivName, activeFilter, fromReport } = props;

  const customTooltip = data => {
    if (data && data.active) {
      return (
        <div className={chart_tooltip}>
          <p>Age group: {data.label}</p>
          <label>Male: {data.payload[0].payload.male}<br />Female: {data.payload[0].payload.female}<br />Total: {data.payload[0].payload.count}</label>
        </div>
      );
    }
    return null;
  }

  const CustomizedLabel = ({ x, y, fill, value, width }) => {
    return <text
      x={x + width / 2}
      y={y}
      fontSize={'small'}
      fill={fill}
      textAnchor="middle">{value > 0 && value}</text>

  };
  return (
    <div style={{ margin: '0 auto', maxWidth: 800 }}>
      <ResizableDiv
        fromReport={fromReport}
        isGraph
        noOption
        resizeLock={props.resizeLock}
        name={resizeDivName}
        setDivAlignment={props.setDivAlignment}
        maxWidth={1000}
        defaultSize={fromReport ? { width: 800, height: 200 } : { width: 1000, height: 300 }}
        filters={props.resizeLock ? null : activeFilter}
      >
        {/* <ResponsiveContainer> */}
        <BarChart
          width={800}
          data={data}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
          height={200}
        >
          <XAxis dataKey="name" fontSize='smaller' />
          <YAxis fontSize='smaller'
            tickFormatter={tick => (Number.isInteger(tick) ? tick : "")}
          />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip content={customTooltip} />
          <Legend style={{ fontSize: 'smaller' }} />
          <Bar dataKey="male" name="Male" fill="#80cde9" label={CustomizedLabel} />
          <Bar dataKey="female" name="Female" fill="#eec5e2" label={CustomizedLabel} />
          {noGenerCtr ? (
            <Bar
              dataKey="no_gender"
              name="Gender not specified"
              fill="#ff5326"
            />
          ) : null}
        </BarChart>
        {/* </ResponsiveContainer> */}
      </ResizableDiv>
    </div>
  );
};

export default DemographicsPieChart;

let chartWrapper = css({
  margin: "20px 0",
  width: "100%",
  display: "flex",
  justifyContent: "center"
});

let graphHeader = css({
  marginTop: "10px",
  width: "100%"
});

let chartDiv = css({
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center"
  //  alignItems: 'flex-end'
});

let asflexTitle = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "space-around",
  padding: "5px",
  fontWeight: "bold",
  fontSize: 16,
  textTransform: "uppercase"
});
