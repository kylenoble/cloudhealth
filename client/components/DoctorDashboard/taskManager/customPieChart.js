import React, { Component } from "react";
import {
  Tooltip,
  Legend,
  PieChart,
  Pie,
  Cell,
  ResponsiveContainer
} from "recharts";
import { css } from "glamor";
import { ResizableDiv } from "../../Reports/Parts";
import Label from "../../NewSkin/Components/Label";
import { chart_tooltip } from "../../NewSkin/Styles";
import GridContainer from "../../NewSkin/Wrappers/GridContainer";

const CustomPieChart = props => {
  const { data, group, resizeDivName, activeFilter, fromReport } = props;

  const COLORS = ["#80cde9", "#eec5e2", "#364563", "#d77f1a", "red", "#9c9e9e"];
  const legendColor = {
    Asian: "#80cde9",
    "Australian/Pacific Islander": "#eec5e2",
    "Black or African Origin": "#364563",
    Caucasian: "#d77f1a",
    "Hispanic, Latino, or Spanish Origin": "#ff0000",
    Other: "#9c9e9e",
    Married: "#80cde9",
    Single: "#eec5e2",
    Partnered: "#364563"
  };

  const RADIAN = Math.PI / 180;
  const renderCustomizedLabel = ({
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    percent
  }) => {
    const radius = innerRadius + (outerRadius - innerRadius) * 0.3;
    const x = cx + radius * Math.cos(-midAngle * RADIAN);
    const y = cy + radius * Math.sin(-midAngle * RADIAN);
    const percentage = (percent * 100).toFixed(0);
    if (percentage > 0) {
      return (
        <text
          x={x + 8}
          y={y}
          fill="white"
          textAnchor={x + 8 > cx ? "start" : "end"}
          dominantBaseline="middle"
          style={{ fontSize: 'smaller' }}
        >
          {`${(percent * 100).toFixed(0)}%`}
        </text>
      );
    }
  };

  const _drawCircle = color => {
    return (
      <svg height="14" width={fromReport ? 20 : 14} style={{ paddingRight: fromReport ? 5 : null }}>
        <circle cx="7" cy="7" r="6" stroke="black" stroke="0" fill={color} />
      </svg>
    );
  };

  const _legend = data => {
    return data.map((item, k) => {
      return (
        <div key={k} className={legendColorStyle(fromReport)} style={{ fontSize: 'smaller' }}>
          {_drawCircle(legendColor[item.name])} {item.name}
        </div>
      );
    });
  };

  const TooltipContent = ({ payload, active }) => {
    if (active) {
      return (
        <div className={chart_tooltip}>
          <p>{payload[0].name}</p>
          <label>{payload[0].value}</label>
        </div>
      );
    }

    return null;
  };

  return (
    <React.Fragment>
      <Label fullWidth subheader text={group} styles={{ margin: 'auto 50px' }} />
      <div id="pie-chart" style={{ width: 800, margin: '0 auto', maxWidth: 800 }}>
        <ResizableDiv
          fromReport={fromReport}
          isGraph
          noOption
          resizeLock={props.resizeLock}
          name={resizeDivName}
          setDivAlignment={props.setDivAlignment}
          maxWidth={1000}
          defaultSize={fromReport ? { width: 800, height: 200 } : { width: 'auto', height: 'auto' }}
          filters={props.resizeLock ? null : activeFilter}
        >
          <GridContainer classes={[wrapper]} columnSize={'300px 1fr'} fullWidth>
            <ResponsiveContainer width="100%">
              <PieChart
                margin={{
                  top: 0,
                  right: 0,
                  bottom: 30,
                  left: 0
                }}
              >
                <Pie
                  dataKey="value"
                  data={data}
                  labelLine={false}
                  label={renderCustomizedLabel}
                  isAnimationActive={false}
                  outerRadius={"100%"}
                  innerRadius={"40%"}
                  fill="#8884d8"
                >
                  {data.map((entry, index) => (
                    <Cell key={index} fill={COLORS[index % COLORS.length]} />
                  ))}
                </Pie>
                <Pie
                  dataKey="value"
                  data={data}
                  isAnimationActive={true}
                  outerRadius={"95%"}
                  innerRadius={"94.5%"}
                  fill="#fff"
                />
                <Tooltip content={<TooltipContent />} />
              </PieChart>
            </ResponsiveContainer>

            <div className={legendWrapper}>{_legend(data)}</div>
          </GridContainer>
        </ResizableDiv>
      </div>
    </React.Fragment>
  );
};

export default CustomPieChart;


const legendWrapper = css({
  display: 'grid',
  alignContent: 'start'
})

const wrapper = css({
  '& .recharts-wrapper': {
    height: '200px !important'
  },
  marginTop: 10
})

const legendStyle = css({
  display: "flex",
  flexWrap: "wrap",
  zIndex: 0,
  position: "absolute",
  alignSelf: "center",
  justifyContent: "center",
  alignItems: "center",
  padding: 10,
  marginTop: 15,
  fontSize: "2vw",
  width: "50%"
});

let legendColorStyle = fromReport => css({
  //flex: "1 0 100%",
  display: "flex",
  flexWrap: "wrap",
  alignItems: "center",
  padding: "0 10px",
  margin: fromReport ? '5px 0 0' : 0,
  fontSize: fromReport ? "normal" : "x-small",
  height: 20,
  lineHeight: 1
});
