import React from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Cell,
  Tooltip,
  LabelList
} from "recharts";
import { css } from "glamor";
import { chart_tooltip } from "../../NewSkin/Styles";

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userCount_perHealthIssue: [],
      userCount_perHealthIssue_raw: [],
      chronic_cnt: 0,
      stroke_cnt: 0,
      cancer_cnt: 0,
      weight_cnt: 0,
      diabetes_cnt: 0,
      heart_cnt: 0,
      hypertension_cnt: 0,
      others_cnt: 0,
      activeFilter: {
        filter: "",
        value: ""
      }
    };
  }

  componentWillMount() {
    this._getDownstreamGraphData();
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.state.activeFilter.filters !== nextProps.activeFilter.filters ||
      this.state.activeFilter.value !== nextProps.activeFilter.value
    ) {
      // update filters
      const activeFilter = this.state.activeFilter;
      activeFilter.filter = nextProps.activeFilter.filter;
      activeFilter.value = nextProps.activeFilter.value;
      activeFilter.filters = nextProps.activeFilter.filters;

      this.setState({ activeFilter }, function() {
        this._getFilteredData();
      });
    }
  }

  getUserStatus(highRisk) {
    if (highRisk === 1) {
      return "High Value";
    } else if (highRisk === 2) {
      return "High Risk";
    } else if (highRisk === 3) {
      return "High Value & High Risk";
    }
    return null;
  }

  _getFilteredData() {
    const activeFilter = this.state.activeFilter;
    const data = this.state.userCount_perHealthIssue_raw;
    let filtered = data;

    if (
      activeFilter.filters &&
      activeFilter.filters.branch &&
      activeFilter.filters.branch.length > 0
    ) {
      const filters = { branch: activeFilter.filters.branch };
      filtered = this.multiFilter(filtered, filters);
    }

    if (
      activeFilter.filters.department &&
      activeFilter.filters.department.length > 0
    ) {
      const filters = { department: activeFilter.filters.department };
      filtered = this.multiFilter(filtered, filters);
    }

    if (
      activeFilter.filters &&
      activeFilter.filters.jobgrade &&
      activeFilter.filters.jobgrade.length > 0
    ) {
      const filters = { job_grade: activeFilter.filters.jobgrade };
      filtered = this.multiFilter(filtered, filters);
    }

    if (
      activeFilter.filters &&
      activeFilter.filters.high_risk &&
      activeFilter.filters.high_risk.length > 0
    ) {
      // let high_risk_filter =
      //   activeFilter.filters.high_risk == "High Value"
      //     ? 1
      //     : activeFilter.filters.high_risk == "High Risk"
      //     ? 2
      //     : null;
      // filtered = filtered.filter(str => {
      //   if (activeFilter.filters.high_risk != "Others") {
      //     return str.high_risk == high_risk_filter;
      //   }
      //   return str.high_risk == null || str.high_risk == 0;
      // });

      const filters = { high_risk: activeFilter.filters.high_risk };
      filtered = this.multiFilter(filtered, filters);
    }
    if (
      activeFilter.filters &&
      activeFilter.filters.age_group &&
      activeFilter.filters.age_group.length > 0
    ) {
      const filters = { age_group: activeFilter.filters.age_group };
      filtered = this.multiFilter(filtered, filters);
    }

    this._processGraph(filtered);
  }

  multiFilter(array, filters) {
    const filterKeys = Object.keys(filters);
    return array.filter(item =>
      filterKeys.every(key => !!~filters[key].indexOf(item[key]))
    );
  }

  _getDownstreamGraphData() {
    this.props
      .getDownstreamGraphData()
      .then(result => {
        const res = result.filter(d => d.healthsurvey_status === "Locked"); // FILTER, ONLY PATIENT SUBMITTED HEALTH SURVEY

        this.setState({
          userCount_perHealthIssue_raw: res
        });
        // process graph values
        this._processGraph(res);
      })
      .catch(e => {
        console.log(e);
      });
  }

  _processGraph(res) {
    console.log("RESS", res);
    const chart_data = [];
    this.state.chronic_cnt = 0;
    this.state.hypertension_cnt = 0;
    this.state.stroke_cnt = 0;
    this.state.cancer_cnt = 0;
    this.state.weight_cnt = 0;
    this.state.diabetes_cnt = 0;
    this.state.heart_cnt = 0;
    this.state.others_cnt = 0;
    const others_issues = [];
    const others = [];
    res.forEach(data => {
      const issues = data.health_issues.toLowerCase().split(",");

      if (data.health_issues.search("chronic") >= 0) {
        this.state.chronic_cnt += 1;
      }
      if (data.health_issues.search("hypertension") >= 0) {
        this.state.hypertension_cnt += 1;
      }
      if (data.health_issues.search("stroke") >= 0) {
        this.state.stroke_cnt += 1;
      } 
      if (data.health_issues.search("cancer") >= 0) {
        this.state.cancer_cnt += 1;
      }
      if (data.health_issues.search("weight") >= 0) {
        this.state.weight_cnt += 1;
      }
      if (data.health_issues.search("diabetes") >= 0) {
        this.state.diabetes_cnt += 1;
      }
      if (data.health_issues.search("heart") >= 0) {
        this.state.heart_cnt += 1;
      }

      const user_issues = issues.filter(issue => {
        if (
          issue != "" &&
          issue != "chronic" &&
          issue != "hypertension" &&
          issue != "stroke" &&
          issue != "cancer" &&
          issue != "weight" &&
          issue != "diabetes" &&
          issue != "heart" &&
          issue != "none"
        ) {
          return issue;
        }
      });
      this.state.others_cnt += user_issues.length
      const user_issue = { user_id: data.user_id, issue: user_issues };
      others_issues.push(user_issue);

      chart_data.push({
        branch: data.branch,
        department: data.department,
        high_risk: data.high_risk ? this.getUserStatus(data.high_risk) : null,
        job_grade: data.job_grade
      });
    });

    this.setState({
      // others_cnt: others.length,
      userCount_perHealthIssue: chart_data
    });
  }

  _renderChart() {
    const data = [
      { name: "Weight", value: this.state.weight_cnt, color: "#666" },
      { name: "Stroke", value: this.state.stroke_cnt },
      { name: "Hypertension", value: this.state.hypertension_cnt },
      { name: "Heart Disease", value: this.state.heart_cnt },
      { name: "Diabetes", value: this.state.diabetes_cnt },
      { name: "Chronic non-resolving illness", value: this.state.chronic_cnt },
      { name: "Cancer", value: this.state.cancer_cnt },
      { name: "Others", value: this.state.others_cnt }
    ];

    const COLORS = [
      "#f7a872",
      "#f493a1",
      "#34bcaf",
      "#81cce7",
      "#fbce5e",
      "#d383b6",
      "#83b85f",
      "#e3e3e3"
    ];

    const TooltipContent = props => {
      if (props.active) {
        return (
          <div className={chart_tooltip}>
            <p>{props.payload[0].payload.name}</p>
            <label>Count: {props.payload[0].payload.value}</label>
          </div>
        );
      }

      return null;
    };

    return (
      <div className={chartDiv}>
        {!this.props.fromReport && (
          <div className={asflexTitle}>DOWNSTREAM MANIFESTATIONS</div>
        )}
        <BarChart
          // eslint-disable-next-line no-undef
          width={this.props.fromReport ? 800 : screen.width / 1.5}
          height={400}
          layout="vertical"
          data={data}
          margin={{ top: 5, right: 30, left: 20, bottom: 5 }}
        >
          <XAxis type="number" hide />
          <YAxis
            type="category"
            dataKey="name"
            width={150}
            tick={{ fontSize: "14px" }}
          />
          <CartesianGrid strokeDasharray="3 3" />
          <Tooltip content={TooltipContent} />
          <Bar dataKey="value" stackId="a">
            <LabelList dataKey="value" position="right" />
            {data.map((entry, index) => (
              <Cell key={index} fill={COLORS[index % COLORS.length]} />
            ))}
          </Bar>
        </BarChart>
      </div>
    );
  }

  _drawCircle(color) {
    return (
      <svg height="21" width="25">
        <circle cx="11" cy="11" r="6" stroke="black" stroke="0" fill={color} />
      </svg>
    );
  }

  render() {
    return <div>{this._renderChart()}</div>;
  }
}

let chartDiv = css({
  width: "99%",
  margin: "20px 0 0 0",
  padding: "5px",
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "center",
  fontSize: "small"
});

let asflexTitle = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "space-around",
  padding: "5px",
  fontWeight: "bold",
  fontSize: 16,
  textTransform: "uppercase"
});
