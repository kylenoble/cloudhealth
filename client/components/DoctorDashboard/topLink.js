/**
will handel navigation for Doctors Dashboard
return activeView
Author JM Copino
**/
import { css } from "glamor";
import Router from "next/router";
import Button from "../NewSkin/Components/Button";
import GridContainer from "../NewSkin/Wrappers/GridContainer";
import { contained, noprint } from "../NewSkin/Styles";

const TopLink = props => {
  const _setLinkView = lnk => {
    Router.push(`/dashboard?tab=${lnk}`);
  };

  const _renderMenu = () => {
    let menus = [
      {
        name: "patientOutcomes",
        label: "Patient Outcomes and Status"
      },
      {
        name: "confirmedPatients",
        label: "Confirmed Patients"
      },
      {
        name: "doctorsBulletin",
        label: "Doctors Bulletin"
      },
      {
        name: "taskManager",
        label: "Task Manager"
      }
    ];

    const buttonStyles = {
      maxWidth: '100%',
      padding: '5px 0'
    }

    let html = menus.map((item, i) => {
      if (item.name === "doctorsBulletin") {
        return (
          <Button key={i} styles={buttonStyles} type={props.linkView === item.name && 'blue'}>
            <span
              key={i}
              style={
                props.linkView === item.name
                  ? { ...styles.dashItem, ...styles.dashItemActive }
                  : styles.dashItem
              }
              key={i}
              onClick={() => _setLinkView(item.name)}
            >
              {props.notReadEventCtr > 0 ? (
                <img className={iconStyle} src="/static/images/flag.svg" />
              ) : null}
              {item.label}
            </span>
          </Button>
        );
      } else {
        return (
          <Button key={i} styles={buttonStyles} type={props.linkView === item.name && 'blue'}>
            <span
              key={i}
              style={
                props.linkView === item.name
                  ? { ...styles.dashItem, ...styles.dashItemActive }
                  : styles.dashItem
              }
              key={i}
              onClick={() => _setLinkView(item.name)}
            >
              {item.label}
            </span>
          </Button>
        );
      }
    });

    return html;
  };
  return (
    <GridContainer columnSize={'1.3fr repeat(3, 1fr)'} gap={20} styles={{ alignContent: 'start' }} classes={[contained, noprint]}>
      {_renderMenu()}
    </GridContainer >
  );
};

export default TopLink;

// css
const styles = {
  dashItem: {
    fontFamily: '"Roboto-Black",sans-serif',
    fontWeight: "bold",
    fontSize: "18ps",
    color: "#000",
    letterSpacing: 0,
    lineHeight: "14px",
    padding: "10px 10px",
    margin: "-1px 15px",
    cursor: "pointer",
    textAlign: "center",
    textTransform: "uppercase"
  },
  dashItemActive: {
    color: "#80cde9",
  }
};

let iconStyle = css({
  display: "relative",
  width: 20,
  height: 20,
  marginRight: "auto",
  marginLeft: "auto",
  margin: "auto"
});
