import React from "react";
import { css } from "glamor";
import moment from "moment-timezone";
import currentDomain from "../../../utils/domain";

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      patientInfo: this.props.patientInfo,
      toggle: {}
    };
  }

  _renderInfo() {
    const info = this.props.patientInfo;
    let txtColor;

    //Extract Age
    const birthday = moment(info.birthday);
    const curentDate = moment();
    const age = curentDate.diff(birthday, "years");

    if (info.gender === "Male") {
      txtColor = "#36c4f1";
    } else {
      txtColor = "#eec5e2";
    }
    // dynamic styling
    const avatarNoImageStyle = css({
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      background: `${txtColor}`,
      marginRight: "auto",
      marginLeft: "auto",
      fontSize: "60",
      textTransform: "uppercase",
      color: "#fff",
      borderRadius: "25%",
      height: "100px",
      width: "100px",
      align: "center",
      verticalAlign: "center",
      border: "2px solid gray",
      //  color: 'rgb(54, 69, 99)',
      "@media(max-width: 767px)": {
        height: "75px",
        width: "75px"
      }
    });

    return (
      <div className={wrapperInfo}>
        <div className={patientPhoto}>
          {info.avatar ? (
            <img
              alt="Avatar"
              className={avatarStyle}
              src={
                info.avatar != null
                  ? `../../static/avatar/${info.avatar}`
                  : "../../static/images/default_profile_photo.png"
              }
            />
          ) : (
            <div className={avatarNoImageStyle}>
              {info.name ? info.name[0] : null}
            </div>
          )}
        </div>

        <div className={patientDetailsWrap}>
          <span className={infoSpan}>
            <span className={infoLabel}>Employee ID: </span>
            <span>{info.employee_id}</span>
          </span>
          <span className={infoSpan}>
            <span className={infoLabel}>Patient Name: </span>
            <span>{info.name}</span>
          </span>
          <span className={infoSpan}>
            <span className={infoLabel}>Age / Sex: </span>
            <span>
              {age || "Unknown"} / {info.gender ? info.gender : "Not Answerd"}
            </span>
          </span>
        </div>

        <div className={patientDetailsWrap}>
          <span className={infoSpan}>
            <span className={infoLabel}>Company Name: </span>
            <span>{info.company}</span>
          </span>
          <span className={infoSpan}>
            <span className={infoLabel}>Site/Departmet/Job Grade: </span>
            <span>
              {info.branch != null ? info.branch : "-"}/
              {info.department != null ? info.department : "-"}
            </span>
          </span>
        </div>
      </div>
    );
  }

  _renderInfo_old() {
    const patientInfo = this.props.patientInfo;

    if (patientInfo && patientInfo.id !== "") {
      return (
        <div className={infoDiv}>
          <div className={box}>
            <img
              alt={"Avatar"}
              className={avatarStyle}
              src={
                patientInfo.avatar
                  ? `/static/avatar/${patientInfo.avatar}`
                  : "/static/icons/user.svg"
              }
            />
          </div>
          <div className={box}>
            <div className={info}>
              <span className={infoLabelStyle}>Employee ID :</span>{" "}
              {patientInfo.employee_id}
            </div>
            <div className={info}>
              <span className={infoLabelStyle}>User ID :</span> {patientInfo.id}
            </div>
            <div className={info}>
              <span className={infoLabelStyle}>Full Name :</span>{" "}
              {patientInfo.name}
            </div>
            <div className={info}>
              <span className={infoLabelStyle}>Age/Sex :</span>{" "}
              {patientInfo.age ? patientInfo.age : "Unknown"} /{" "}
              {patientInfo.sex ? patientInfo.sex : "Not Answerd"}
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className="warningStyle">
        No Consult History or this patient dont have appoinment...
      </div>
    );
  }

  render() {
    return this._renderInfo();
  }
}

let info = css({
  flex: "1 0 100%",
  padding: "5px"
});

let infoLabelStyle = css({
  fontWeight: "bold",
  color: "#80cde9"
});

let infoDiv = css({
  width: "100%",
  display: "flex",
  margin: "50px 5px 50px 5px"
});

let box = css({
  display: "flex",
  flexWrap: "wrap",
  justifyContent: "flexStart",
  margin: "10px",
  height: "100"
});

let patientPhoto = css({
  borderRadius: "50%",
  float: "left",
  margin: "10px"
});

let wrapperInfo = css({
  width: "100%",
  display: "flex",
  flexWrap: "nowrap"
});

let patientDetailsWrap = css({
  margin: "10px auto 10px 20px",
  display: "inline-block",
  width: "400px"
});

let infoSpan = css({
  display: "block"
});

let infoLabel = css({
  color: "rgb(128, 205, 233)"
});

let avatarStyle = css({
  marginRight: "auto",
  marginLeft: "auto",
  borderRadius: "25%",
  height: "100px",
  //width: "100px",
  align: "center",
  verticalAlign: "center",
  border: "2px solid #36c4f1",
  ":hover": {
    backgroundColor: "#ED81B9",
    border: "2px solid #ED81B9",
    color: "white"
  },
  //  color: 'rgb(54, 69, 99)',
  "@media(max-width: 767px)": {
    height: "75px",
    width: "75px"
  }
});
