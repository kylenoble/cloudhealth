import React from 'react';
import GridContainer from '../../NewSkin/Wrappers/GridContainer';
import { css } from 'glamor';
import moment from 'moment-timezone';
import Colors from '../../NewSkin/Colors';

const PatientDetails = ({ info }) => {
  return (
    <GridContainer
      columnSize="120px 1fr"
      gap={20}
      styles={{ marginBottom: 20, marginRight: 'auto' }}
      fullWidth
    >
      <section style={{ position: "relative" }}>
        <figure className={photoWrapper}>
          <img
            src={
              info.avatar != null
                ? `/static/avatar/${info.avatar}`
                : "../../static/images/default_profile_photo.png"
            }
          />
        </figure>
      </section>

      <GridContainer
        columnSize="1fr 2fr"
        columnGap={"20px !important"}
        classes={[detailsWrapper]}
        styles={{maxWidth: 'unset'}}
      >
        <label className={infoLabel}>Employee ID: </label>
        <span>{info.employee_id || '-'}</span>

        <label className={infoLabel}>Patient Name: </label>
        <span>{info.name}</span>

        <label className={infoLabel}>Age / Sex: </label>
        <span>
          {info.age} / {info.gender}
        </span>

        <label className={infoLabel}>Company Name: </label>
        <span>{info.company}</span>

        <label className={infoLabel}>Site/Departmet/Job Grade: </label>
        <span>
          {info.branch != null ? info.branch : "-"}/
              {info.department != null ? info.department : "-"}
        </span>
      </GridContainer>
    </GridContainer>
  );
}

const detailsWrapper = css({
  maxWidth: 510,
  "& > *": {
    textAlign: "left !important"
  },
  "& > span": {
    color: Colors.blueDarkAccent,
    fontWeight: 500
  }
});

let infoLabel = css({
  color: "#000"
});

const photoWrapper = css({
  margin: "auto auto",
  width: 100,
  height: 100,
  borderRadius: "100%",
  overflow: "hidden",
  position: "absolute",
  top: "50%",
  transform: "translateY(-50%)",
  "> img": {
    width: "100%",
    position: "absolute",
    top: "50%",
    left: 0,
    transform: "translateY(-50%)"
  }
});

export default PatientDetails;