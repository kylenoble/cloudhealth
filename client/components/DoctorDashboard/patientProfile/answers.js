import { css } from "glamor";
import { labelClass, combined, scrollClass, flex_wrapper, errorMsg, goodMessage, formMsgActive } from "../../NewSkin/Styles";
import Colors from "../../NewSkin/Colors";
import GridContainer from "../../NewSkin/Wrappers/GridContainer";
import Button from "../../NewSkin/Components/Button";
import { sendNotification } from "../../Commons";
import PreviousResponses from "../../MyHealthProfile/previousResponses";
import SummaryService from "../../../utils/summaryService";

const summaryService = new SummaryService();

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user_info: this.props.user_info,
      profile: this.props.profile,
      userid: this.props.userid,
      scores: [],
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      },
      healthsurvey_status: this.props.surveyStatus,
      survey: []
    };

    this._getPatientAnswers = this._getPatientAnswers.bind(this);
    this._getHealthScores = this._getHealthScores.bind(this);
    this._getHealthStatus = this._getHealthStatus.bind(this);
  }

  componentWillMount() {
    this._getPatientAnswers();
    this._getHealthScores(this.state.userid);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ ...this.state, userid: nextProps.userid }, () => {
      this._getHealthScores(nextProps.userid);
    });
  }

  _getPatientAnswers() {
    summaryService.getHealthSurveySummary(this.state.userid).then(r => {
      if (r.length > 0) {
        this.setState({ survey: r });
      } else {
        this.setState(this.props.answers(true));
      }
    });
  }

  _getHealthStatus(value) {
    let score = !NaN ? parseFloat(value).toFixed(1) : 0;
    if (score >= 1.0 && score <= 2.9) {
      return <span style={{ color: Colors.optimal }}>Optimal</span>;
    } else if (score >= 3.0 && score <= 4.9) {
      return <span style={{ color: Colors.suboptimal }}>Suboptimal</span>;
    } else if (score >= 5.0 && score <= 5.9) {
      return <span style={{ color: Colors.neutral }}>Neutral</span>;
    } else if (score >= 6.0 && score <= 8.5) {
      return <span style={{ color: Colors.neutral }}>Compromised</span>;
    } else if (score >= 8.6) {
      return <span style={{ color: Colors.alarming }}>Alarming</span>;
    }
  }

  _getHealthScores(userid) {
    let obj = {};
    this.props.scores(true, userid).then(res => {
      res.map(summary => {
        obj[summary.name] = summary.value;
      });
      this.setState({ scores: obj });
    });
  }

  _sendNotification() {
    const { profile, user_info } = this.state;

    const notifMessage = `Dr. ${
      profile.name
      } has unlocked your health survey. Please update your answers accordingly and make sure
    you submit your responses after. `;

    const emailData = {
      patientEmail: user_info.email,
      doctorEmail: profile.email,
      notifMessage
    };

    sendNotification(emailData);
  }

  unlockSurvey = () => {
    this._sendNotification();
    return this.props
      .unlockSurvey(this.state.userid)
      .then(result => {
        if (result) {
          this.setState(
            {
              formSuccess: {
                active: true,
                message: "User's Health Survey form was successfully unlocked."
              },
              healthsurvey_status: "Active"
            },
            () => {
              this._sendNotification();
            }
          );
        }
      })
      .catch(err => {
        console.log("Error saving activity : " + err);
      });
  };

  _showPersonalDetails() {
    let info = this.state.user_info;
    let personal_info = "";
    if (info.gender == "Female" || info.gender == "F") {
      personal_info = (
        <GridContainer columnSize="250px 1fr">
          <label className={qLabelStyle}>Menstrual Status</label>
          <span className={aSpanStyle}>
            {this._showAnswer(this.state.menstrual)}
          </span>
          <label className={qLabelStyle}>Pregnant?</label>
          <span className={aSpanStyle}>
            {this._showAnswer(this.state.pregnant)}
          </span>
        </GridContainer>
      );
    } else {
      personal_info = (
        <div>
          <GridContainer columnSize="250px 1fr">
            <label className={qLabelStyle}>PSA done?</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.psa)}
            </span>

            {this.state.psa == "Yes" ? (
              <>
                <label className={qLabelStyle}>PSA Level:</label>
                <span className={aSpanStyle}>
                  {this._showAnswer(this.state.psaLevel)}
                </span>
              </>
            ) : (
                ""
              )}

            <label className={qLabelStyle}>Problem List:</label>
            <span className={aSpanStyle} style={{ width: "50%" }}>
              {this._showAnswer(this.state.maleIssues)}
            </span>
          </GridContainer>
        </div>
      );
    }

    return (
      <div className={answersWrap}>
        <div className={answersStyle} style={{ width: "100%" }}>
          {personal_info}
          <GridContainer columnSize="250px 1fr">
            <label className={qLabelStyle}>Relationship Status:</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.relationship)}
            </span>
            {/* </span> */}

            {this.state.relationship != "Single" ? (
              <>
                <label className={qLabelStyle}>Number of Children:</label>
                <span className={aSpanStyle}>
                  {this._showAnswer(this.state.numberChildren)}
                </span>
              </>
            ) : (
                ""
              )}

            <label className={qLabelStyle}>Race:</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.race)}
            </span>
          </GridContainer>
        </div>
      </div>
    );
  }

  render() {
    return (<>
      {this.props.surveyStatus == "Locked" && (
        <Button
          type="blue"
          styles={{ minWidth: "fit-content", margin: "10px 0 0 auto" }}
        >
          <button onClick={this.unlockSurvey}>
            UNLOCK health survey to update
        </button>
        </Button>
      )}
      <div
        className={[
          flex_wrapper,
          this.state.formError.active ? errorMsg : goodMessage,
          this.state.formError.active || this.state.formSuccess.active ? formMsgActive : undefined
        ].join(" ")}
      >
        {this.state.formSuccess.message}
        {this.state.formError.message}
      </div>
      {this.state.survey.length > 0 ?
        <PreviousResponses survey={this.state.survey} userid={this.state.userid} />
        :
        this._displayAnswers()
      }
    </>
    )
  }

  _displayAnswers() {
    return (
      <div>
        <div className={`${answersDivStyle} ${scrollClass}`}>
          <h4 className={labelClass} style={{ margin: "0" }}>
            Personal Details:
          </h4>
          <section className={answerWrapper}>
            {this._showPersonalDetails()}
          </section>
          <h4 className={labelClass} style={{ margin: "0", marginTop: "15px" }}>
            Questionnaire Answers:
          </h4>

          <div>
            <h5 className={categoryStyle}>A. WEIGHT</h5>
            <section className={answerWrapper}>
              {this._renderWeightAnswers()}
            </section>

            <h5 className={categoryStyle}>B. NUTRITION</h5>
            <section className={answerWrapper}>
              {this._renderNutritionAnswers()}
            </section>

            <h5 className={categoryStyle}>C. SLEEP</h5>

            <section className={answerWrapper}>
              {this._renderSleepAnswers()}
            </section>

            <h5 className={categoryStyle}>D. MOVEMENT & EXERCISE</h5>
            <section className={answerWrapper}>
              {this._renderMovementAnswers()}
            </section>

            <h5 className={categoryStyle}>E. STRESS</h5>
            <section className={answerWrapper}>
              {this._renderStressAnswers()}
            </section>

            <h5 className={categoryStyle}>F. DETOXIFICATION</h5>

            <section className={answerWrapper}>
              {this._renderDetoxificationAnswers()}
            </section>

            <h4 className={labelClass}>HEALTHSCORE TALLY</h4>
            <section className={answerWrapper}>
              {this._renderSummaryScores()}
            </section>
          </div>
        </div>
      </div>
    )
  }

  _renderSummaryScores() {
    let weightScore =
      this.state.scores.weightScore != null
        ? parseFloat(this.state.scores.weightScore)
        : 0;
    let nutritionScore =
      this.state.scores.nutrition != null
        ? parseFloat(this.state.scores.nutrition)
        : 0;
    let sleepScore =
      this.state.scores.sleep != null ? parseFloat(this.state.scores.sleep) : 0;
    let movementScore =
      this.state.scores.exercise != null
        ? parseFloat(this.state.scores.exercise)
        : 0;
    let stressScore =
      this.state.scores.stress != null
        ? parseFloat(this.state.scores.stress)
        : 0;
    let detoxScore =
      this.state.scores.detox != null ? parseFloat(this.state.scores.detox) : 0;
    let totalScore =
      weightScore +
      nutritionScore +
      sleepScore +
      movementScore +
      stressScore +
      detoxScore;
    let averageScore = (totalScore / 6) * 2;

    return (
      <div>
        <GridContainer
          columnSize="250px 50px 1fr"
          classes={[wrapper]}
          columnGap="20px !important"
        >
          <label className={qLabelStyle}>Weight Score</label>
          <span className={aSpanStyle}>{parseInt(weightScore * 2)}</span>
          <span className={aSpanStyle}>
            {this._getHealthStatus(weightScore * 2)}
          </span>

          <label className={qLabelStyle}>Nutrition Score</label>
          <span className={aSpanStyle}>{parseInt(nutritionScore * 2)}</span>
          <span className={aSpanStyle}>
            {this._getHealthStatus(nutritionScore * 2)}
          </span>

          <label className={qLabelStyle}>Sleep Score</label>
          <span className={aSpanStyle}>{parseInt(sleepScore * 2)}</span>
          <span className={aSpanStyle}>
            {this._getHealthStatus(sleepScore * 2)}
          </span>

          <label className={qLabelStyle}>Movement Score</label>
          <span className={aSpanStyle}>{parseInt(movementScore * 2)}</span>
          <span className={aSpanStyle}>
            {this._getHealthStatus(movementScore * 2)}
          </span>

          <label className={qLabelStyle}>Stress Score</label>
          <span className={aSpanStyle}>{parseInt(stressScore * 2)}</span>
          <span className={aSpanStyle}>
            {this._getHealthStatus(stressScore * 2)}
          </span>

          <label className={qLabelStyle}>Detoxification Score</label>
          <span className={aSpanStyle}>{parseInt(detoxScore * 2)}</span>
          <span className={aSpanStyle}>
            {this._getHealthStatus(detoxScore * 2)}
          </span>

          <label className={qLabelStyle}>
            <strong>OVERALL SCORE</strong>
          </label>
          <span className={aSpanStyle}>{parseInt(averageScore)}</span>
          <span className={aSpanStyle}>
            {this._getHealthStatus(averageScore)}
          </span>
        </GridContainer>
      </div>
    );
  }

  _renderWeightAnswers(answers) {
    return (
      <div className={answersWrap}>
        <GridContainer columnSize="1fr 1fr" styles={{ alignContent: "start" }}>
          <GridContainer columnSize="150px 1fr">
            <label className={qLabelStyle}>
              Height <small>(in)</small>{" "}
            </label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.height)}
            </span>

            <label className={qLabelStyle}>
              Weight <small>(lbs)</small>
            </label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.weight)}
            </span>

            <label className={qLabelStyle}>
              Waist <small>(in)</small>
            </label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.waist)}
            </span>

            <label className={qLabelStyle}>
              Hip <small>(in)</small>
            </label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.hip)}
            </span>
          </GridContainer>
          <GridContainer columnSize="150px 1fr">
            <label className={qLabelStyle}>BMI</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.scores.bmi)}
            </span>

            <label className={qLabelStyle}>WHR</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.scores.whr)}
            </span>
          </GridContainer>
        </GridContainer>
      </div>
    );
  }

  _showAnswer(name) {
    return name != null ? name : <strong>-</strong>;
  }

  _renderNutritionAnswers() {
    return (
      <div>
        <section className={infoSpan}>
          <label className={nutritionQuestionStyle}>
            1. Do you follow any special diet or have diet restrictions or
            limitations for any reason (health, cultural, religious or other)?{" "}
          </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.specialDiet)}
          </span>
        </section>

        <section className={infoSpan}>
          <label className={nutritionQuestionStyle}>
            {" "}
            2. Do you have any food allergies, sensitivities or intolerances?{" "}
          </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.foodAllergies)}
          </span>
        </section>

        <section className={infoSpan}>
          <label className={nutritionQuestionStyle}>
            3. Food composition (%)
          </label>

          <GridContainer
            columnSize="250px 1fr"
            classes={[wrapper]}
            columnGap={"20px !important"}
          >
            <label className={qLabelStyle}>Meat</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.meatConsumedPercent)}
            </span>

            <label className={qLabelStyle}>Vegetable</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.vegetableConsumedPercent)}
            </span>

            <label className={qLabelStyle}>Fruit</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.fruitConsumedPercent)}
            </span>

            <label className={qLabelStyle}>Starch/Grains/Sugar</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.starchConsumedPercent)}
            </span>

            <label className={qLabelStyle}>Poultry</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.poultryConsumedPercent)}
            </span>

            <label className={qLabelStyle}>Seafood</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.seafoodConsumedPercent)}
            </span>
          </GridContainer>
        </section>

        <GridContainer
          columnSize="250px 1fr"
          columnGap={"20px !important"}
          classes={[wrapper]}
        >
          <label className={qLabelStyle}>Usual Diet</label>
          <span style={{ fontWeight: "600" }}>
            {this._showAnswer(this.state.usualMeal)}
          </span>
        </GridContainer>
      </div>
    );
  }

  _renderSleepAnswers() {
    return (
      <div>
        <span className={infoSpan}>
          <label>1. When is your regular sleeping time? </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.regularSleep)}
          </span>
        </span>
        <span className={infoSpan}>
          <label> 2. Average number of hours you sleep</label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.averageSleep)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>3. Do you have trouble falling asleep? </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.troubleFallingAsleep)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>4. Do you feel rested upon awakening? </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.restedFeeling)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>5. Do you use sleeping aids?</label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.sleepingAids)}
          </span>
        </span>
      </div>
    );
  }

  _renderMovementAnswers() {
    return (
      <div>
        <span className={infoSpan}>
          <label>1. Do you exercise at least 150 minutes a week? </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.weeklyExercise)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>
            2. Does anything limit you from being physically active?{" "}
          </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.limitActivity)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>
            3. Do you feel unusually fatigued or sore after exercise?
          </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.postActivitySoreness)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>
            4. When you are at work, which of the following describes what you
            do?{" "}
          </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.workActivity)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>5. How many hours per day do you spend sitting? </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.sittingHours)}
          </span>
        </span>
      </div>
    );
  }

  _renderStressAnswers() {
    return (
      <div>
        <span className={infoSpan}>
          <label>1. Have you ever sought counseling? </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.counseling)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>2. Are you currently in therapy? </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.therapy)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>3. Daily Stressors (1-10; 1 lowest, 10 highest)</label>
          <GridContainer
            columnSize="250px 1fr"
            classes={[wrapper]}
            styles={{ marginLeft: 20 }}
          >
            <label className={qLabelStyle}>Family Stress</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.familyStress)}
            </span>

            <label className={qLabelStyle}>Financial Stress</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.financialStress)}
            </span>

            <label className={qLabelStyle}>Health Stress</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.healthStress)}
            </span>

            <label className={qLabelStyle}>Work Stress</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.workStress)}
            </span>

            <label className={qLabelStyle}>Social Stress</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.socialStress)}
            </span>

            <label className={qLabelStyle}>Other Stress</label>
            <span className={aSpanStyle}>
              {this._showAnswer(this.state.otherStress)}
            </span>
          </GridContainer>
        </span>
      </div>
    );
  }

  _renderDetoxificationAnswers() {
    return (
      <div>
        <span className={infoSpan}>
          <label>
            1. Are you presently taking prescription drugs? If yes, how many?
          </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.prescriptionDrugs)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>
            2. Are you taking one or more of the following over-the-counter
            medications? Acetaminophen/OCP
          </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.overTheCounter)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>3. What is your response to presciption medications?</label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.prescriptionScenarios)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>
            4. Do you currently use or within the last 6 months had you
            regularly used tobacco products (cigars, cigarettes)
          </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.tobacco)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>
            5. Do you have strong negative reactions to caffeine or caffeine
            containing products?
          </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.caffeine)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>
            6. Do you develop symptoms on exposure to fragrances, exhaust fumes
            or strong odors?
          </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.odors)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>
            7. Do you feel ill after you consume even small amounts of alcohol?{" "}
          </label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.illAlcohol)}
          </span>
        </span>
        <span className={infoSpan}>
          <label>8. Are you alcohol or chemical dependent?</label>
          <span className={nutritionAnswerStyle}>
            {this._showAnswer(this.state.dependent)}
          </span>
        </span>
      </div>
    );
  }
}

const wrapper = css({
  "& *": {
    textAlign: "left !important"
  }
});

const usualDiet = css({
  marginLeft: 20,
  "& *": {
    textAlign: "left !important"
  }
});

const list = css({
  marginLeft: 20,
  "& *": {
    textAlign: "left !important"
  }
});

const answerWrapper = css({
  marginLeft: 20
});

let answersDivStyle = css({
  marginTop: "10px",
  width: "100%",
  overflowY: 'auto',
  maxHeight: 400
});

let infoLabel = css({
  color: "rgb(128, 205, 233)"
});

let categoryStyle = css({
  marginTop: "15px",
  marginBottom: "0",
  color: Colors.skyblue,
  fontWeight: 600
});

let answersWrap = css({
  width: "100%",
  "& *": {
    textAlign: "left !important"
  }
});

let answersStyle = css({
  marginTop: "15px",
  // fontStyle: "italic",
  width: "49%",
  display: "inline-block",
  verticalAlign: "top"
});

let nutritionQuestionStyle = css({
  // fontStyle: "italic",
  listStyle: "none"
});

let nutritionAnswerStyle = css({
  display: "block",
  marginTop: "10px",
  fontWeight: "600"
});

let infoSpan = css({
  display: "block"
});

let qLabelStyle = css({
  // width: "180px",
  display: "inline-block"
});

let aSpanStyle = css({
  fontStyle: "normal",
  fontWeight: "600",
});

let totalSpan = css({
  marginTop: "5px",
  display: "block",
  fontStyle: "italic"
});

const styles = {
  formError: {
    display: "none",
    fontSize: "0.8em",
    textAlign: "center",
    marginTop: "5px",
    marginBottom: "10px",
    color: "#EA3131",
    transition: "color 0.3s ease-in-out"
  },
  formErrorActive: {
    display: "block"
  },
  formSuccess: {
    display: "none",
    fontSize: "0.8em",
    textAlign: "center",
    marginTop: "5px",
    color: "rgb(3, 210, 127)",
    transition: "color 0.3s ease-in-out"
  },
  formSuccessActive: {
    display: "block"
  }
};
