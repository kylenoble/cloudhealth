/* eslint-disable no-nested-ternary */
/* eslint-disable jsx-a11y/label-has-for */
import React from "react";
import { css } from "glamor";
import { confirmAlert } from "react-confirm-alert"; // Import
import Answers from "./answers";

import CoreImbalances from "../../../components/SummaryAndAnalysis/coreImbalances";
import WriteAnalysis from "../../../components/SummaryAndAnalysis/writeAnalysis.js";
import WriteRecomendation from "../../../components/SummaryAndAnalysis/writeRecomendation.js";
import WriteModifiable from "../../../components/SummaryAndAnalysis/writeModifiable.js";
import WriteSymptoms from "../../../components/SummaryAndAnalysis/writeSymptoms.js";
import AnswersService from "../../../utils/answerService.js";
import UserService from "../../../utils/userService.js";
import HealthTracker from "../../../components/HealthTracker";
import Timeline from "../../../components/Timeline";
import OtherhealthInfo from "../../MyHealthProfile/otherHealthInfo";
import HealthDocuments from "../../MyHealthProfile/myHealthDocuments";
import SummaryService from "../../../utils/summaryService.js";
import TabWrapper from "../../NewSkin/Wrappers/TabWrapper";
import {
  default_tab,
  active_tab,
  disabled_tab,
  labelClass,
  closeBtn
} from "../../NewSkin/Styles";
import Button from "../../NewSkin/Components/Button";
import GridContainer from "../../NewSkin/Wrappers/GridContainer";
import PatientDetails from "./patientDetails";
import Card from "../../NewSkin/Wrappers/Card";

const answerService = new AnswersService();
const userService = new UserService();
const summary = new SummaryService();

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.user = this.props.user;
    this.state = {
      patientAnswers: [],
      userid: this.props.userid,
      profile: userService.getProfile(),
      patientInfo: [],
      age: "",
      renderInView: false,
      activeTab: this.props.activeTab,
      forms: {
        writeAnalysis: false,
        timeline: false,
        writeModifiable: false,
        writeSymptoms: false,
        otherHealthInfo: false,
        healthDocuments: false
      },
      scale: {
        radius: 200,
        width: 800,
        height: 600,
        fontSize: 12
      },
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      },
      healthsurvey_status: null
    };
    this._getDetailsFromAnswers = this._getDetailsFromAnswers.bind(this);
    this._getPatientAnswers = this._getPatientAnswers.bind(this);
    this._getPatientInfo = this._getPatientInfo.bind(this);
    this._getSummary = this._getSummary.bind(this);
    this._getCoreImbalancesData = this._getCoreImbalancesData.bind(this);
    this.toggleWAForm = this.toggleWAForm.bind(this);
    this._surveyUnlock = this._surveyUnlock.bind(this);
  }

  componentWillMount() {
    this._getPatientAnswers(this.state.userid);
    this._getPatientInfo(this.state.userid);

    // this._getCoreImbalancesData(this.state.userid)
  }

  componentWillReceiveProps(nextProps) {
    this._getPatientAnswers(nextProps.userid);
    this._getPatientInfo(nextProps.userid);
    this.setState({ userid: nextProps.userid });
  }

  getAge(dateString) {
    const today = new Date();
    const birthDate = new Date(dateString);
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }

  _getAnswers(userid) {
    return answerService
      .getAnswerList(
        { column: "question_id", operator: "gt", value: 0 },
        userid
      )
      .then(res => Promise.resolve(res))
      .catch(err => {
        console.log(err);
      });
  }

  _getPatientAnswers(userid) {
    this._getAnswers(userid)
      .then(res => {
        this.setState(
          {
            patientAnswers: res
          },
          function () {
            this._getDetailsFromAnswers(false);
          }
        );
      })
      .catch(e => {
        console.log(e);
      });
  }

  _getPatientInfoByID(userID) {
    return userService
      .get(userID)
      .then(res => Promise.resolve(res))
      .catch(err => {
        console.log(err);
      });
  }

  _getPatientInfo(userid) {
    this._getPatientInfoByID(userid)
      .then(res => {
        this.setState({
          patientInfo: res,
          healthsurvey_status: res.healthsurvey_status
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  _getCoreImbalancesData(viewFromSummary) {
    return answerService
      .getAnswerList({ coreImbalances: true }, this.state.userid)
      .then(res => {
        if (viewFromSummary && res !== "undefined") {
          return Promise.resolve(res);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _surveyUnlock(userid) {
    const inputs = [
      {
        healthsurvey_status: "Active",
        userId: userid
      }
    ];
    return userService.update(inputs).then(res => {
      if (res) {
        const profile = this.state.profile;
        //  store in user_activities table
        const data = {
          user_id: profile.id,
          action: "Unlock Survey",
          details: {
            userid,
            name: this.state.patientInfo.name,
            company: this.state.patientInfo.company
          }
        };
        userService.saveActivity(data);
        this.setState({
          healthsurvey_status: "Active"
        });
        return res;
      } else {
        this.setState({
          formError: {
            active: true,
            message: "There was an error updating the status"
          }
        });
      }
    });
  }

  _getSummary(viewFromAnswers, userid) {
    return summary
      .get({ userId: userid, distinct: true })
      .then(res => {
        if (viewFromAnswers && res !== "undefined") {
          return Promise.resolve(res);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _getDetailsFromAnswers(viewFromAnswers) {
    //  get birthday and gender
    const answers = this.state.patientAnswers;

    const obj = {};
    answers.forEach(item => {
      if (item.name === "birthday") {
        this.setState({
          age: this.getAge(item.answer)
        });
      } else if (item.name === "gender") {
        this.setState({
          gender: item.answer
        });
      } else {
        obj[item.name] = item.value;
      }
    });
    this.setState({
      renderInView: true
    });

    if (viewFromAnswers) {
      return obj;
    }
  }

  toggleWAForm(form) {
    const forms = this.state.forms;
    const scale = this.state.scale;
    if (forms[form]) {
      forms[form] = false;
      scale.radius = 200;
      scale.width = 800;
      scale.height = 600;
      scale.fontSize = 12;
    } else {
      Object.keys(forms).forEach(item => {
        if (item === form) {
          forms[form] = true;
        } else {
          forms[item] = false;
        }
      });
      forms[form] = true;
      scale.radius = 200;
      scale.width = 800;
      scale.height = 600;
      scale.fontSize = 12;
    }
    this.setState({
      forms,
      scale
    });
  }

  _alert(str) {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: str, // Message dialog
      buttons: [
        {
          label: "Close"
        }
      ]
    });
  }

  _displayView(view) {
    this.setState({ activeTab: view });
  }

  _renderView() {
    const renderAnswer = this.state.renderInView;
    if (renderAnswer) {
      if (this.state.activeTab === "healthTracker") {
        return (
          <HealthTracker
            restricted={this.props.restricted}
            userid={this.state.userid}
            patientInfo={this.state.patientInfo}
          />
        );
      } else if (this.state.activeTab === "answers") {
        return (
          <Answers
            userid={this.state.userid}
            profile={this.state.profile}
            user_info={this.state.patientInfo}
            answers={this._getDetailsFromAnswers}
            scores={this._getSummary}
            unlockSurvey={this._surveyUnlock}
            surveyStatus={this.state.healthsurvey_status}
            formError={this.state.formError}
            formSuccess={this.state.formSuccess}
          />
        );
      } else if (this.state.activeTab === "timeline") {
        return (
          <Timeline
            userId={this.state.userid}
            userType={this.props.user.type}
          />
        );
      } else if (this.state.activeTab === "otherHealthInfo") {
        return (
          <OtherhealthInfo
            user={this.state.patientInfo}
            userType={this.user.type}
          />
        );
      } else if (this.state.activeTab === "healthDocuments") {
        return (
          <HealthDocuments
            user={this.state.patientInfo}
            userType={this.user.type}
          />
        );
      }
      return (
        <CoreImbalances
          coreImbalances={this._getCoreImbalancesData}
          scale={this.state.scale}
        />
      );
    }
  }

  _renderPatientDetails() {
    const info = this.state.patientInfo;
    let txtColor;
    if (info.gender === "Male") {
      txtColor = "#36c4f1";
    } else {
      txtColor = "#eec5e2";
    }
    // dynamic styling
    const avatarNoImageStyle = css({
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      background: `${txtColor}`,
      marginRight: "auto",
      marginLeft: "auto",
      fontSize: "60",
      textTransform: "uppercase",
      color: "#fff",
      borderRadius: "25%",
      height: "100px",
      width: "100px",
      align: "center",
      verticalAlign: "center",
      border: "2px solid gray",
      //  color: 'rgb(54, 69, 99)',
      "@media(max-width: 767px)": {
        height: "75px",
        width: "75px"
      }
    });

    return <PatientDetails info={info} />;
  }

  render() {
    const restricted = this.props.restricted;
    let alertMessage = "";
    alertMessage = `You do not have an existing appointment with this client hence, only the Health Tracker will be available for viewing/editing.`;

    return (
      <div>
        {this.props.activeTab === "healthTracker" ? (
          <Button
            classes={[closeBtn]}
            styles={{ minWidth: "unset", marginTop: 9, marginRight: 110 }}
          >
            <button
              title="Close"
              onClick={() => this.props.setActiveView("Tracker")}
            >
              X
            </button>
          </Button>
        ) : this.props.back ? (
          <Button
            classes={[closeBtn]}
            styles={{ minWidth: "unset", marginTop: 9, marginRight: 110 }}
          >
            <button
              title="Close"
              onClick={() => this.props.back(this.props.activeTab || "confirmedPatients")}
            >
              X
            </button>
          </Button>
        ) : null}

        <Card styles={{ maxWidth: 1100, margin: "0 auto" }} fullWidth>
          {this.props.noInfo ? null : (
            <PatientDetails info={this.state.patientInfo} />
          )}
          <TabWrapper tabCount={5}>
            <div
              className={
                restricted
                  ? disabled_tab
                  : this.state.activeTab === "answers"
                    ? active_tab
                    : default_tab
              }
              onClick={
                restricted
                  ? () => this._alert(alertMessage)
                  : () => this._displayView("answers")
              }
            >
              answers to health survey
            </div>

            <div
              className={
                this.state.activeTab === "healthTracker"
                  ? active_tab
                  : default_tab
              }
              onClick={() => this._displayView("healthTracker")}
            >
              update health tracker
            </div>

            <div
              className={
                restricted
                  ? disabled_tab
                  : this.state.activeTab === "summary"
                    ? active_tab
                    : default_tab
              }
              onClick={
                restricted
                  ? () => this._alert(alertMessage)
                  : () => this._displayView("summary")
              }
            >
              summary and analysis
            </div>

            <div
              className={
                restricted
                  ? disabled_tab
                  : this.state.activeTab === "otherHealthInfo"
                    ? active_tab
                    : default_tab
              }
              onClick={
                restricted
                  ? () => this._alert(alertMessage)
                  : () => this._displayView("otherHealthInfo")
              }
            >
              other health info
            </div>

            <div
              className={
                restricted
                  ? disabled_tab
                  : this.state.activeTab === "healthDocuments"
                    ? active_tab
                    : default_tab
              }
              onClick={
                restricted
                  ? () => this._alert(alertMessage)
                  : () => this._displayView("healthDocuments")
              }
            >
              health documents
            </div>

            {/*
            this.state.activeTab === 'summary' ?
            <div className={inactiveTab}>WRITE PRESCRIPTION</div>
          : null
         */}
          </TabWrapper>

          {this.state.activeTab === "summary" ? (
            <GridContainer
              columnSize="repeat(4, 1fr)"
              styles={{ width: "100%", margin: "20px 0" }}
              gap={20}
            >
              <Button
                type={this.state.forms.writeAnalysis ? "blue" : "default"}
              >
                <button onClick={() => this.toggleWAForm("writeAnalysis")}>
                  Analysis and Recommendations
                </button>
              </Button>

              <Button
                type={this.state.forms.writeModifiable ? "blue" : "default"}
              >
                <button onClick={() => this.toggleWAForm("writeModifiable")}>
                  Modifiable Personal Lifestyle Factors
                </button>
              </Button>

              <Button
                type={this.state.forms.writeSymptoms ? "blue" : "default"}
              >
                <button onClick={() => this.toggleWAForm("writeSymptoms")}>
                  Symptoms
                </button>
              </Button>

              <Button type={this.state.forms.timeline ? "blue" : "default"}>
                <button onClick={() => this.toggleWAForm("timeline")}>
                  Timeline
                </button>
              </Button>
            </GridContainer>
          ) : null}

          <div className={centered}>
            {this.state.activeTab === "summary" ? (
              <div className={twoColum}>
                {this.state.forms.writeModifiable === false &&
                  this.state.forms.writeSymptoms === false &&
                  this.state.forms.timeline === false ? (
                    <div className={column2}>{this._renderView()}</div>
                  ) : null}
                {this.state.forms.writeAnalysis ? (
                  <div className={column1}>
                    <WriteAnalysis
                      toggleWAForm={this.toggleWAForm}
                      patientInfo={this.state.patientInfo}
                      user={this.props.user}
                    />
                  </div>
                ) : null}
                {this.state.forms.writeRecomendation ? (
                  <div className={column1}>
                    <WriteRecomendation
                      toggleWAForm={this.toggleWAForm}
                      patientInfo={this.state.patientInfo}
                    />
                  </div>
                ) : null}
                {this.state.forms.writeModifiable ? (
                  <div className={column1}>
                    <WriteModifiable
                      scores={this._getSummary}
                      toggleWAForm={this.toggleWAForm}
                      patientInfo={this.state.patientInfo}
                      user={this.props.user}
                    />
                  </div>
                ) : null}
                {this.state.forms.writeSymptoms ? (
                  <div className={column1}>
                    <WriteSymptoms
                      toggleWAForm={this.toggleWAForm}
                      patientInfo={this.state.patientInfo}
                    />
                  </div>
                ) : null}
                {this.state.forms.timeline ? (
                  <div className={column1}>
                    <Timeline
                      userId={this.state.userid}
                      userType={this.props.user.type}
                      defaultView={""}
                    />
                  </div>
                ) : null}
              </div>
            ) : (
                this._renderView()
              )}
          </div>
        </Card>
      </div>
    );
  }
}

let centered = css({
  textAlign: "left !important"
  // display: "flex",
  // flex: "1 0 100%",
  // flexWrap: "wrap",
  // justifyContent: "center"
});

let twoColum = css({
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 100%",
  padding: 10,
  transition: "width 2s"
});

let column1 = css({
  flex: "1 0 30%"
  // transition: "width 2s",
  // borderRadius: "7px",
  // border: "1px solid #d3d3d3",
  // background: "#f4f4f4",
  // padding: 5
});
let column2 = css({
  display: "flex",
  flex: "1",
  width: "550px",
  transition: "2s",
  justifyContent: "center",
  alignItems: "flex-start"
  // borderRadius: '7px',
  // border: '1px solid #444141',
  // padding: 15
});

let labelStyle = css({
  width: "300px",
  background: "#1b75bb",
  color: "white",
  textAlign: "center",
  padding: "5px 10px 5px 20px"
});

let activeTab = css({
  fontSize: 10,
  padding: "2px 5px 2px 5px",
  margin: 5,
  background: "#36c4f1",
  border: "1px solid rgb(54, 69, 99)",
  color: "white"
});

let inactiveTab = css({
  fontSize: 10,
  padding: "2px 5px 2px 5px",
  margin: 5,
  background: "rgb(54, 69, 99)",
  color: "white",
  ":hover": {
    color: "#36c4f1"
  },
  cursor: "pointer"
});

let tabStyle = css({
  textAlign: "center",
  padding: "5px 20px",
  marginLeft: "10px",
  marginRight: "10px",
  display: "inline-block",
  fontSize: ".8rem",
  textTransform: "capitalize"
});

let labelDivStyle = css({
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 100%",
  flexContent: "flex-start"
  // marginTop: "20px"
});

let patientPhoto = css({
  borderRadius: "50%",
  float: "left",
  margin: "10px"
});

let wrapperInfo = css({
  width: "100%"
});

let patientDetailsWrap = css({
  margin: "10px auto 10px 20px",
  display: "inline-block",
  width: "400px"
});

let infoSpan = css({
  display: "block"
});

let infoLabel = css({
  color: "rgb(128, 205, 233)"
});

let avatarStyle = css({
  marginRight: "auto",
  marginLeft: "auto",
  borderRadius: "25%",
  height: "100px",
  width: "100px",
  align: "center",
  verticalAlign: "center",
  border: "2px solid #36c4f1",
  ":hover": {
    backgroundColor: "#ED81B9",
    border: "2px solid #ED81B9",
    color: "white"
  },
  //  color: 'rgb(54, 69, 99)',
  "@media(max-width: 767px)": {
    height: "75px",
    width: "75px"
  }
});
