import { css } from "glamor";
import { confirmAlert } from "react-confirm-alert"; // Import
import Answers from "./answers";
import CoreImbalances from "../../../components/SummaryAndAnalysis/coreImbalances";
import WriteAnalysis from "../../../components/SummaryAndAnalysis/writeAnalysis.js";
import WriteRecomendation from "../../../components/SummaryAndAnalysis/writeRecomendation.js";
import WriteModifiable from "../../../components/SummaryAndAnalysis/writeModifiable.js";
import WriteSymptoms from "../../../components/SummaryAndAnalysis/writeSymptoms.js";
import AnswersService from "../../../utils/answerService.js";
import UserService from "../../../utils/userService.js";
import currentDomain from "../../../utils/domain.js";
import TextareaAutosize from "react-autosize-textarea";
import HealthTracker from "../../../components/HealthTracker";
import Timeline from "../../../components/Timeline";
import SummaryService from "../../../utils/summaryService.js";

const answerService = new AnswersService();
const userService = new UserService();
const summary = new SummaryService();

export default class extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      patientAnswers: [],
      userid: this.props.userid,
      patientInfo: [],
      age: "",
      renderInView: false,
      activeTab: this.props.activeTab,
      forms: {
        writeAnalysis: false,
        timeline: false,
        writeModifiable: false,
        writeSymptoms: false
      },
      scale: {
        radius: 200,
        width: 800,
        height: 600,
        fontSize: 12
      },
      formError: {
        active: false,
        message: ""
      },
      formSuccess: {
        active: false,
        message: ""
      },
      healthsurvey_status: null
    };
    this._getDetailsFromAnswers = this._getDetailsFromAnswers.bind(this);
    this._getPatientAnswers = this._getPatientAnswers.bind(this);
    this._getPatientInfo = this._getPatientInfo.bind(this);
    this._getSummary = this._getSummary.bind(this);
    this._getCoreImbalancesData = this._getCoreImbalancesData.bind(this);
    this.toggleWAForm = this.toggleWAForm.bind(this);
    this._surveyUnlock = this._surveyUnlock.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this._getPatientAnswers(nextProps.userid);
    this._getPatientInfo(nextProps.userid);
    console.log(nextProps.userid);
    // this._getCoreImbalancesData(this.state.userid)
  }

  componentDidMount() {
    this._getPatientAnswers(this.state.userid);
    this._getPatientInfo(this.state.userid);
  }

  componentWillUnmount() { }

  // _getSummary(){
  //   console.log('SUMMMMM');
  //   console.log(this.state);
  //   // console.log(this.state.userid);
  //   // if(this.state.userid){
  //   //   userid = this.state.userid
  //   // }
  //   let userID = this.state.userID
  //
  //   return summary.get({userId: userID, distinct: true})
  //   .then(res => {
  //     console.log(res);
  //       if (res.name === 'Error') {
  //           console.log(res)
  //       } else {
  //         return Promise.resolve(res)
  //         this.setState({summary : res})
  //       }
  //   })
  // }

  _getSummary(viewFromAnswers, userid) {
    //return this.props.getScores(userid)
    return summary
      .get({ userId: userid, distinct: true })
      .then(res => {
        if (viewFromAnswers && res != "undefined") {
          return Promise.resolve(res);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _getPatientAnswers(userid) {
    this.props
      .getAnswers(userid)
      .then(res => {
        this.setState(
          {
            patientAnswers: res
          },
          function () {
            this._getDetailsFromAnswers(false);
          }
        );
      })
      .catch(e => {
        console.log(e);
      });
  }

  _getPatientInfo(userid) {
    this.props
      .getPatientInfo(userid)
      .then(res => {
        this.setState({
          patientInfo: res,
          healthsurvey_status: res.healthsurvey_status
        });
      })
      .catch(e => {
        console.log(e);
      });
  }

  _getCoreImbalancesData(viewFromSummary) {
    //alert(viewFromSummary)
    return answerService
      .getAnswerList({ coreImbalances: true }, this.state.userid)
      .then(res => {
        console.log("CI");
        console.log(res);
        if (viewFromSummary && res != "undefined") {
          return Promise.resolve(res);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _surveyUnlock(userid) {
    var inputs = [
      {
        healthsurvey_status: "Active",
        userId: userid
      }
    ];
    return userService.update(inputs).then(res => {
      if (res) {
        const profile = userService.getProfile();
        //  store in user_activities table
        let data = {
          user_id: profile.id,
          action: "Unlock Survey",
          details: {
            userid: userid,
            name: this.state.patientInfo.name,
            company: this.state.patientInfo.company
          }
        };
        userService.saveActivity(data);
        this.setState({
          healthsurvey_status: "Active"
        });
      } else {
        this.setState({
          formError: {
            active: true,
            message: "There was an error updating the status"
          }
        });
      }
    });
  }

  getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    return age;
  }

  _getDetailsFromAnswers(viewFromAnswers) {
    //  get birthday and gender
    let answers = this.state.patientAnswers;
    let obj = {};
    answers.map(item => {
      if (item.question === "Birthday") {
        this.setState({
          age: this.getAge(item.answer)
        });
      } else if (item.question === "Gender") {
        this.setState({
          gender: item.answer
        });
      } else {
        obj[item.name] = item.answer;
      }
    });
    this.setState({
      renderInView: true
    });

    if (viewFromAnswers) {
      return obj;
    }
  }

  toggleWAForm(form) {
    let forms = this.state.forms;
    let stat;
    let scale = this.state.scale;
    if (forms[form]) {
      forms[form] = false;
      scale.radius = 200;
      scale.width = 800;
      scale.height = 600;
      scale.fontSize = 12;
    } else {
      Object.keys(forms).forEach((item, i) => {
        if (item === form) {
          forms[form] = true;
        } else {
          forms[item] = false;
        }
      });
      forms[form] = true;
      scale.radius = 200;
      scale.width = 800;
      scale.height = 600;
      scale.fontSize = 12;
    }
    this.setState({
      forms,
      scale
    });
  }

  _alert(str) {
    confirmAlert({
      closeOnEscape: false,
      closeOnClickOutside: false,
      title: "", // Title dialog
      message: str, // Message dialog
      buttons: [
        {
          label: "Close"
        }
      ]
    });
  }

  render() {
    let alertMessage = `You do not have an existing appointment with this client hence, only the Health Tracker will be available for viewing/editing.`;
    return (
      <div className="subContainer">
        <div className={labelDivStyle}>
          {this.props.noInfo ? null : (
            <span className={labelStyle}>PATIENT DETAILS</span>
          )}
          {this.props.activeTab === "healthTracker" ? (
            <div
              className="backBtn"
              onClick={() => this.props.setActiveView("Tracker")}
            >
              Back
            </div>
          ) : this.props.back ? (
            <div
              className="backBtn"
              onClick={() => this.props.back("confirmedPatients")}
            >
              Back
            </div>
          ) : null}
        </div>

        {this.props.noInfo ? null : this._renderPatientDetails()}

        <div className={centered}>
          {this.props.restricted ? (
            <div
              className={`${tabStyle} ${
                this.state.activeTab === "answers" ? activeTab : inactiveTab
                }`}
              onClick={() => this._alert(alertMessage)}
            >
              ANSWERS TO HEALTH SURVEY
            </div>
          ) : (
              <div
                className={`${tabStyle} ${
                  this.state.activeTab === "answers" ? activeTab : inactiveTab
                  }`}
                onClick={() => this._displayView("answers")}
              >
                ANSWERS TO HEALTH SURVEY
            </div>
            )}
          <div
            className={`${tabStyle} ${
              this.state.activeTab === "healthTracker" ? activeTab : inactiveTab
              }`}
            onClick={() => this._displayView("healthTracker")}
          >
            UPDATE HEALTH TRACKER
          </div>

          {this.props.restricted ? (
            <div
              className={`${tabStyle} ${
                this.state.activeTab === "summary" ? activeTab : inactiveTab
                }`}
              onClick={() => this._alert(alertMessage)}
            >
              SUMMARY AND ANALYSIS
            </div>
          ) : (
              <div
                className={`${tabStyle} ${
                  this.state.activeTab === "summary" ? activeTab : inactiveTab
                  }`}
                onClick={() => this._displayView("summary")}
              >
                SUMMARY AND ANALYSIS
              {this.state.activeTab === "summary" ? (
                  <div>
                    <div
                      className={`${tabStyle} ${
                        this.state.forms.writeAnalysis ? activeTab : inactiveTab
                        }`}
                      onClick={() => this.toggleWAForm("writeAnalysis")}
                    >
                      ANALYSIS AND RECOMMENDATIONS
                  </div>
                    <div
                      className={`${tabStyle} ${
                        this.state.forms.writeModifiable ? activeTab : inactiveTab
                        }`}
                      onClick={() => this.toggleWAForm("writeModifiable")}
                    >
                      MODIFIABLE PERSONAL LIFESTYLE FACTORS
                  </div>
                    <div
                      className={`${tabStyle} ${
                        this.state.forms.writeSymptoms ? activeTab : inactiveTab
                        }`}
                      onClick={() => this.toggleWAForm("writeSymptoms")}
                    >
                      SYMPTOMS
                  </div>
                    <div
                      className={`${tabStyle} ${
                        this.state.forms.timeline ? activeTab : inactiveTab
                        }`}
                      onClick={() => this.toggleWAForm("timeline")}
                    >
                      TIMELINE
                  </div>
                  </div>
                ) : null}
              </div>
            )}

          {/*
            this.state.activeTab === 'summary' ?
            <div className={inactiveTab}>WRITE PRESCRIPTION</div>
          : null
         */}
        </div>

        <div className={centered}>
          {this.state.activeTab === "summary" ? (
            <div className={twoColum}>
              {this.state.forms.writeModifiable === false &&
                this.state.forms.writeSymptoms === false &&
                this.state.forms.timeline === false ? (
                  <div className={column2}>{this._renderView()}</div>
                ) : null}
              {this.state.forms.writeAnalysis ? (
                <div className={column1}>
                  <WriteAnalysis
                    toggleWAForm={this.toggleWAForm}
                    patientInfo={this.state.patientInfo}
                    user={this.props.user}
                  />
                </div>
              ) : null}
              {this.state.forms.writeRecomendation ? (
                <div className={column1}>
                  <WriteRecomendation
                    toggleWAForm={this.toggleWAForm}
                    patientInfo={this.state.patientInfo}
                  />
                </div>
              ) : null}
              {this.state.forms.writeModifiable ? (
                <div className={column1}>
                  <WriteModifiable
                    scores={this._getSummary}
                    toggleWAForm={this.toggleWAForm}
                    patientInfo={this.state.patientInfo}
                    user={this.props.user}
                  />
                </div>
              ) : null}
              {this.state.forms.writeSymptoms ? (
                <div className={column1}>
                  <WriteSymptoms
                    toggleWAForm={this.toggleWAForm}
                    patientInfo={this.state.patientInfo}
                  />
                </div>
              ) : null}
              {this.state.forms.timeline ? (
                <div className={column1}>
                  <Timeline
                    userId={this.state.userid}
                    userType={this.props.user.type}
                    defaultView={""}
                  />
                </div>
              ) : null}
            </div>
          ) : (
              this._renderView()
            )}
        </div>
      </div>
    );
  }

  _displayView(view) {
    this.setState({ activeTab: view });
  }

  _renderView() {
    let renderAnswer = this.state.renderInView;
    if (renderAnswer) {
      if (this.state.activeTab === "healthTracker") {
        return (
          <HealthTracker
            restricted={this.props.restricted}
            userid={this.state.userid}
          />
        );
      } else if (this.state.activeTab === "answers") {
        return (
          <Answers
            userid={this.state.userid}
            answers={this._getDetailsFromAnswers}
            scores={this._getSummary}
            unlockSurvey={this._surveyUnlock}
            surveyStatus={this.state.healthsurvey_status}
            formError={this.state.formError}
            formSuccess={this.state.formSuccess}
          />
        );
      } else if (this.state.activeTab === "timeline") {
        return (
          <Timeline
            userId={this.state.userid}
            userType={this.props.user.type}
          />
        );
      } else {
        return (
          <CoreImbalances
            coreImbalances={this._getCoreImbalancesData}
            scale={this.state.scale}
          />
        );
      }
    }
  }

  _renderPatientDetails() {
    let info = this.state.patientInfo;
    let txtColor;
    if (info.gender === "Male") {
      txtColor = "#36c4f1";
    } else {
      txtColor = "#eec5e2";
    }
    // dynamic styling
    let avatarNoImageStyle = css({
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      background: `${txtColor}`,
      marginRight: "auto",
      marginLeft: "auto",
      fontSize: "60",
      textTransform: "uppercase",
      color: "#fff",
      borderRadius: "25%",
      border: "1px solid rgba(204, 204, 204, 0.3)",
      height: "100px",
      width: "100px",
      align: "center",
      verticalAlign: "center",
      border: "2px solid gray",
      //  color: 'rgb(54, 69, 99)',
      "@media(max-width: 767px)": {
        height: "75px",
        width: "75px"
      }
    });

    return (
      <div className={wrapperInfo}>
        <div className={patientPhoto}>
          {info.avatar ? (
            <img
              className={avatarStyle}
              src={
                info.avatar != null
                  ? `../../static/avatar/${info.avatar}`
                  : "../../static/images/default_profile_photo.png"
              }
            />
          ) : (
              <div className={avatarNoImageStyle}>
                {info.name ? info.name[0] : null}
              </div>
            )}
        </div>
        <div className={patientDetailsWrap}>
          <span className={infoSpan}>
            <label className={infoLabel}>Employee ID: </label>
            <span>{info.employee_id}</span>
          </span>
          <span className={infoSpan}>
            <label className={infoLabel}>Patient Name: </label>
            <span>{info.name}</span>
          </span>
          <span className={infoSpan}>
            <label className={infoLabel}>Age / Sex: </label>
            <span>
              {this.state.age} / {this.state.gender}
            </span>
          </span>
        </div>
        <div className={patientDetailsWrap}>
          <span className={infoSpan}>
            <label className={infoLabel}>Company Name: </label>
            <span>{info.company}</span>
          </span>
          <span className={infoSpan}>
            <label className={infoLabel}>Site/Departmet/Job Grade: </label>
            <span>
              {info.branch != null ? info.branch : "-"}/
              {info.department != null ? info.department : "-"}
            </span>
          </span>
        </div>
      </div>
    );
  }
}

// styles

let summaryTitle = css({
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 100%",
  justifyContent: "center",

  color: "#36c4f1",
  margin: "25px 5px 25px 5px"
});

let centered = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "center"
});

let wrapper = css({
  position: "relative",
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap"
});

let twoColum = css({
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 100%",
  padding: 10,
  transition: "width 2s"
});

let column1 = css({
  flex: "1 0 30%",
  transition: "width 2s",
  borderRadius: "7px",
  border: "1px solid #d3d3d3",
  background: "#f4f4f4",
  padding: 5
});
let column2 = css({
  display: "flex",
  flex: "1",
  width: "650px",
  transition: "2s",
  justifyContent: "center",
  alignItems: "flex-start"
  // borderRadius: '7px',
  // border: '1px solid #444141',
  // padding: 15
});

let closeSpan = css({
  position: "absolute",
  top: 0,
  right: 0,
  background: "#36c4f1",
  borderRadius: 20,
  padding: "3px 8px 3px 8px",
  cursor: "pointer"
});

let itemWrapper = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  padding: "5px 5px 3px 20px"
});

let insightsTextArea = css({
  display: "block",
  boxSizing: "padding-box",
  overflow: "hidden",
  padding: "5px",
  width: " 100%",
  height: 50,
  fontSize: "14px",
  margin: "5px",
  borderRadius: "6px",
  boxShadow: "2px 2px 8px rgba(black, .3)"
});

let link = css({
  ":hover": {
    color: "#36c4f1"
  },
  cursor: "pointer"
});

let summaryLinks = css({
  width: "100%",
  display: "inline"
});

let summaryLinksDiv = css({
  width: "390px",
  display: "inline-block",
  verticalAlign: "middle",
  fontSize: "11px",
  fontWeight: "600"
});

let hideMe = css({
  display: "none"
});

let labelStyle = css({
  width: "300px",
  background: "#1b75bb",
  color: "white",
  textAlign: "center",
  padding: "5px 10px 5px 20px"
});

let activeTab = css({
  fontSize: 10,
  padding: "2px 5px 2px 5px",
  margin: 5,
  background: "#36c4f1",
  border: "1px solid rgb(54, 69, 99)",
  color: "white"
});

let inactiveTab = css({
  fontSize: 10,
  padding: "2px 5px 2px 5px",
  margin: 5,
  background: "rgb(54, 69, 99)",
  color: "white",
  ":hover": {
    color: "#36c4f1"
  },
  cursor: "pointer"
});

let tabStyle = css({
  textAlign: "center",
  padding: "5px 20px",
  marginLeft: "10px",
  marginRight: "10px",
  display: "inline-block"
});

let labelDivStyle = css({
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 100%",
  flexContent: "flex-start",
  marginTop: "20px"
});

let tabsWrapStyle = css({
  marginTop: "20px",
  width: "100%",
  marginRight: "auto",
  textAlign: "center"
});

let patientPhoto = css({
  borderRadius: "50%",
  float: "left",
  margin: "10px"
});

let wrapperInfo = css({
  width: "100%"
});

let patientDetailsWrap = css({
  margin: "10px auto 10px 20px",
  display: "inline-block",
  width: "400px"
});

let infoSpan = css({
  display: "block"
});

let infoLabel = css({
  color: "rgb(128, 205, 233)"
});

let avatarStyle = css({
  marginRight: "auto",
  marginLeft: "auto",
  borderRadius: "25%",
  border: "1px solid rgba(204, 204, 204, 0.3)",
  height: "100px",
  width: "100px",
  align: "center",
  verticalAlign: "center",
  border: "2px solid #36c4f1",
  ":hover": {
    backgroundColor: "#ED81B9",
    border: "2px solid #ED81B9",
    color: "white"
  },
  //  color: 'rgb(54, 69, 99)',
  "@media(max-width: 767px)": {
    height: "75px",
    width: "75px"
  }
});
