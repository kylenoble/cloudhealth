import { css } from "glamor";
export default class extends React.Component {
  constructor(props) {
    super(props);
    this._getCompanyQueue = this._getCompanyQueue.bind(this);
    //this._getPatientQueue = this._getPatientQueue.bind(this)
  }

  componentDidMount() {
    //this._getPatientQueue()
  }

  render() {
    return (
      <div className="subContainer">
        <div className={labelDivStyle}>
          <span className={labelStyle}> COMPANY QUEUE</span>
        </div>
        <div className={queueItemDivStyle}>{this._getCompanyQueue()}</div>
      </div>
    );
  }

  _getCompanyQueue() {
    let companyQueue = [];
    let companyQueue_test = [
      {
        id: "",
        name: "Meraki Social Ventures",
        corpIndex: "Open Corporate Health Index",
        writeRep: "Write Report",
        tooltip: "Meraki Social Ventures Tool tip Need to be set"
      },
      {
        id: "",
        name: "Other Company",
        corpIndex: "Open Corporate Health Index",
        writeRep: "Write Report",
        tooltip: "Meraki Social Ventures Tool tip Need to be set"
      }
    ];

    let html;
    if (companyQueue.length > 0) {
      html = companyQueue.map((item, i) => (
        <div className={queueItemDivStyle} data-tip={item.tooltip} key={i}>
          <span className={queueItemStyle}>{item.name}</span>
          <span className={queueItemStyle}>{item.corpIndex}</span>
          <span className={queueItemStyle}>{item.writeRep}</span>
        </div>
      ));
    } else {
      html = <div className={queueItemStyle}>No Companies in Queue</div>;
    }

    return html;
  }

  // _getPatientQueue(){

  // }
}

// styling

let queueItemDivStyle = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  justifyContent: "space-around"
});

let labelStyle = css({
  width: "200px",
  background: "#1b75bb",
  color: "white",
  textAlign: "center",
  padding: "5px 10px 5px 100px"
});

let labelDivStyle = css({
  display: "flex",
  flexWrap: "wrap",
  flex: "1 0 100%",
  flexContent: "flex-start",
  margin: "10px 0 30px 5px",
  padding: "5px"
});

let queueItemStyle = css({
  width: "250px",
  background: "black",
  color: "#fff",
  padding: "5px",
  margin: "5px"
});
