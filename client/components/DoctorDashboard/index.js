import React from "react";
import { css } from "glamor";
import ReactTooltip from "react-tooltip";
import { withRouter } from "next/router";
import UserService from "../../utils/userService.js";
import currentDomain from "../../utils/domain.js";
import AppointmentRequest from "../../components/Appointments/appointmentRequest";
import PatientQueue from "../../components/Appointments/patientQueue";
import PatientOutcomes from "./PatientOutcome";
import AnswersService from "../../utils/answerService.js";
import AppointmentService from "../../utils/appointmentService.js";
import PatientTracker from "../../components/PatientTracker/";
import TaskManager from "./taskManager/";
import Restricted from "../../components/restricted";
import CompanyService from "../../utils/companyService.js";
import ConsultsService from "../../utils/consultsService";
import PatientProfile from "./patientProfile";
import SummaryService from "../../utils/summaryService.js";
import ReportsService from "../../utils/reportsService.js";
import Events from "../../components/Events";
import TopLink from "./topLink";
import GridContainer from "../NewSkin/Wrappers/GridContainer.js";
import Card from "../NewSkin/Wrappers/Card";
import Colors from "../NewSkin/Colors.js";
import { ReactTableStyle } from "../NewSkin/Styles.js";
import Bulletin from "../Bulletin/index.js";

const Appointment = new AppointmentService();
const Company = new CompanyService();
const User = new UserService();
const Answer = new AnswersService();
const Consults = new ConsultsService();
const summary = new SummaryService();
const Report = new ReportsService();

class DoctorDashboard extends React.Component {
  constructor(props) {
    super(props);

    this._isMounted = false;
    this.domain = `${currentDomain}/images/`;
    this.state = {
      restricted: true,
      user: this.props.user,
      activeView: this.props.activeView,
      linkView: this.props.router.query.tab || "patientOutcomes"
    };
    this._getProfile = this._getProfile.bind(this);
    this._getAnswers = this._getAnswers.bind(this);
    // this._getSummary = this._getSummary.bind(this);
    this._back = this._back.bind(this);
    this._fetchQueuedPatients = this._fetchQueuedPatients.bind(this);
    this._renderView = this._renderView.bind(this);
    this._getPatientInfo = this._getPatientInfo.bind(this);
    this._setLinkView = this._setLinkView.bind(this);
    this._setUserID = this._setUserID.bind(this);
    this._getPatientInfoByID = this._getPatientInfoByID.bind(this);
    this._getCompanies = this._getCompanies.bind(this);
    this._getDemographics = this._getDemographics.bind(this);
    this._getReadinessData = this._getReadinessData.bind(this);
    this._getScoresByCompany = this._getScoresByCompany.bind(this);
    this._getDownstreamData = this._getDownstreamData.bind(this);
    this._getHealthDeterminants = this._getHealthDeterminants.bind(this);
    this._getUserFilters = this._getUserFilters.bind(this);
    this._getQuery = this._getQuery.bind(this);
    this._restrict = this._restrict.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
  }

  componentWillReceiveProps(nextProps) {
    let { linkView, activeView } = this.setState;

    linkView = nextProps.router.query.tab || "patientOutcomes";
    activeView = nextProps.activeView;
    if (this._isMounted) {
      this.setState({ linkView, activeView });
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  _getQuery(qry) {
    return Answer.getQuery(qry)
      .then(res => {
        if (res) return Promise.resolve(res);
      })
      .catch(err => {
        if (err) {
          console.log(err);
        }
      });
  }

  _getConsults(obj) {
    return Consults.get(obj)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getCompanies() {
    return Company.get("company", this.state.user.id)
      .then(res => {
        if (res.name === "Error") {
          console.log(res);
        } else if (res.length > 0) {
          return Promise.resolve(res);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  // _getSummary(userid) {
  //   return summary.get({ userId: userid, distinct: true }).then(res => {
  //     if (res.name === "Error") {
  //       console.log(res);
  //     } else {
  //       return Promise.resolve(res);
  //     }
  //   });
  // }

  _getUserFilters(company) {
    return User.getUserFilters(company).then(res => {
      if (res.name === "Error") {
        console.log(res);
      } else {
        return Promise.resolve(res);
      }
    });
  }

  _getScoresByCompany(company_name) {
    const field = { company: company_name, graph: "healthscore" };
    return summary.getGraphData(field).then(res => {
      if (res.name === "Error") {
        console.log(res);
      } else {
        return Promise.resolve(res);
      }
    });
  }

  _getHealthDeterminants(company_name) {
    const field = { company: company_name, graph: "healthscore_determinants" };
    return summary.getGraphData(field).then(res => {
      if (res.name === "Error") {
        console.log(res);
      } else {
        return Promise.resolve(res);
      }
    });
  }

  _getDownstreamData(company_name) {
    const data = { company: company_name, graph: "downstream" };
    return Answer.getGraphData(data)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getMSQData(company_name) {
    return summary
      .getGraphData({ company: company_name, graph: "msq" })
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getMSQDataPerSystem(company_name) {
    return Answer.getGraphData({ company: company_name, graph: "msq" })
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getUpstreamData(company_name) {
    return Answer.getGraphData({ company: company_name, graph: "upstream" })
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getAnswers(userid) {
    return Answer.getAnswerList(
      { column: "question_id", operator: "gt", value: 0 },
      userid
    )
      .then(res => Promise.resolve(res))
      .catch(err => {
        console.log(err);
      });
  }

  _fetchQueuedPatients(date = null) {
    let qry;
    if (date) {
      qry = { active: this.props.active, apmt_datetime: date };
    } else {
      qry = { active: this.props.active };
    }
    return Appointment.getAppointmentsList(qry)
      .then(res => {
        if (res.name === "Error" || res.message === "Unauthorized") {
          console.log(res);
        } else {
          return Promise.resolve(res);
        }
      })
      .catch(e => {
        console.log(e);
      }); // you would show/hide error messages with component state here
  }

  _getAnswersByDistinctQuestion(userid) {
    return Answer.getAnswerList(
      {
        column: "question_id",
        operator: "gt",
        value: 0,
        distinct_question: true
      },
      userid
    )
      .then(res => Promise.resolve(res))
      .catch(err => {
        console.log(err);
      });
  }

  _setLinkView(linkView) {
    if (this._isMounted) {
      this.setState({
        linkView
      });
    }
  }

  _setUserID(userid) {
    if (this._isMounted) {
      this.setState({
        userid
      });
    }
  }

  _back(linkView) {
    if (this._isMounted) {
      this.setState({
        activeView: linkView === "schedule" ? "schedule" : "DoctorDashboard"
      });
    }
    this._setLinkView(linkView);
  }

  _renderView() {
    if (Object.keys(this.state.user).length > 0) {
      if (this.state.activeView === "schedule") {
        if (this.state.linkView === "patientProfile") {
          return (
            <PatientProfile
              active={this.props.active}
              userid={this.state.userid}
              back={this._back}
              activeTab={this.state.activeView}
              user={this.props.user}
              restricted={this.state.restricted}
            />
          );
        }
        return (
          <AppointmentRequest
            active={this.props.active}
            linkView={this._setLinkView}
            userid={this._setUserID}
            user={this.props.user}
            restrict={this._restrict}
          />
        );
      } else if (this.state.activeView === "DoctorDashboard") {
        if (this.state.linkView === "confirmedPatients") {
          return (
            <PatientQueue
              active={this.props.active}
              linkView={this._setLinkView}
              userid={this._setUserID}
              user={this.props.user}
              restrict={this._restrict}
              updateRoot={this.props.updateRoot}
            />
          );
        } else if (this.state.linkView === "patientOutcomes") {
          return (
            <PatientOutcomes
              getCompanies={this._getCompanies}
              getConsults={this._getConsults}
              user={this.props.user}
            />
          );
        } else if (this.state.linkView === "doctorsBulletin") {
          return <Bulletin user={this.props.user} />
          // return this._renderNotifications();
        } else if (this.state.linkView === "taskManager") {
          return (
            <TaskManager
              user={this.props.user}
              companies={this._getCompanies}
              data={this._getDemographics}
              readinessData={this._getReadinessData}
              getScoresByCompany={this._getScoresByCompany}
              downstreamData={this._getDownstreamData}
              msqData={this._getMSQData}
              msqDataPerSystem={this._getMSQDataPerSystem}
              upstreamData={this._getUpstreamData}
              msqSystemRank={this._getMSQSystemRank}
              stressSourceRank={this._getStressSourceRank}
              symptoms={this._getUpstream_symptoms}
              foodchoices={this._getUpstream_foodchoices}
              healthDeterminants={this._getHealthDeterminants}
              filters={this._getUserFilters}
              back={this._back}
              getCompliance={this._getCompliance}
              getQuery={this._getQuery}
              filters={this._getUserFilters}
            />
          );
        } else if (this.state.linkView === "patientProfile") {
          return (
            <PatientProfile
              active={this.props.active}
              userid={this.state.userid}
              back={this._back}
              activeTab={this.props.activeTab}
              setActiveView={this.props.setActiveView}
              user={this.props.user}
              restricted={this.state.restricted}
            />
          );
        }
        return <Restricted message="Coming soon..." />;
      } else if (this.state.activeView === "tracker") {
        return (
          <PatientTracker
            user={this.props.user}
            getConsults={this._getConsults}
            getPatientInfo={this._getPatientInfo}
            linkView={this._setLinkView}
            setActiveView={this.props.setActiveView}
            userid={this._setUserID}
            setActiveTab={this.props.setActiveTab}
            getAppoinments={this._fetchQueuedPatients}
            restrict={this._restrict}
          />
        );
      }
    }
  }

  _restrict(b) {
    if (this._isMounted) {
      this.setState({ restricted: b });
    }
  }

  _setNofication(bol) {
    this.props.setNofication(bol);
  }

  _renderNotifications() {
    return (
      <div className="wrap">
        <GridContainer columnSize={"2fr 1fr"} gap={20}>
          <Card styles={{ minHeight: 585 }}>
            <div className={subscriptionDiv}>
              <div className={title}>Events</div>
              <div className={ReactTableStyle} style={{ width: "100%" }}>
                <Events
                  viewfor="bulletin"
                  user={this.props.user}
                  setNofication={this.props.setNofication}
                />
              </div>
            </div>
          </Card>

          <Card>
            <div className={subscriptionDiv}>
              <div className={title}>Training Announcements</div>
              <div className={content}>
                <div className={warning}>
                  <p>Coming Soon!</p>
                </div>
              </div>
            </div>
          </Card>
        </GridContainer>
      </div>
    );
  }

  _bulletins() {
    const bulletins = [];

    let html;
    if (bulletins.length > 0) {
      html = bulletins.map((item, i) => (
        <div key={i}>
          <span>{item.name}</span>
          <span>{item.desc}</span>
        </div>
      ));
    } else {
      html = (
        <div>
          <div>No Bulletins</div>
        </div>
      );
    }
    return html;
  }

  _notifications() {
    const notifications = [];

    let html;
    if (notifications.length > 0) {
      html = notifications.map((item, i) => (
        <div key={i}>
          <span>{item.name}</span>
          <span>{item.desc}</span>
        </div>
      ));
    } else {
      html = (
        <div>
          <Events user={this.props.user} />
        </div>
      );
    }
    return html;
  }

  // get Profile

  _getProfile() {
    User.getLatest()
      .then(res => Promise.resolve(res))
      .catch(err => {
        console.log(err);
      });
  }

  _getPatientInfo(string) {
    return User.getPatientInfo(string)
      .then(res => Promise.resolve(res))
      .catch(err => {
        console.log(err);
      });
  }

  _getPatientInfoByID(userID) {
    return User.get(userID)
      .then(res => Promise.resolve(res))
      .catch(err => {
        console.log(err);
      });
  }
  //GRAPHS
  _getDemographics(cmpy) {
    const data = { field: "company", value: cmpy };
    return User.getData(data)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getReadinessData(cmpy, filter) {
    const data = { company: cmpy, filter, graph: "readiness" };
    return Answer.getGraphData(data)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getMSQSystemRank(cmpy) {
    const data = { company: cmpy, graph: "upstream_msq_systemRank" };
    return Answer.getGraphData(data)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getStressSourceRank(cmpy) {
    const data = { company: cmpy, graph: "upstream_stress" };
    return Answer.getGraphData(data)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getUpstream_symptoms(cmpy) {
    const data = { company: cmpy, graph: "upstream_symptoms" };
    return Answer.getGraphData(data)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getUpstream_foodchoices(cmpy) {
    const data = { company: cmpy, graph: "upstream_foodchoices" };
    return Answer.getGraphData(data)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _getCompliance(company_name) {
    return Report.getCompliance(company_name)
      .then(res => Promise.resolve(res))
      .catch(res => {
        console.log(res);
      });
  }

  render() {
    return (
      <div className="wrap">
        <GridContainer gap={20} classes={[main]}>
          <ReactTooltip className="tooltipStyle" />
          {this.state.activeView === "DoctorDashboard" &&
          this.state.linkView !== "patientProfile" ? (
            <div className={topLinkContainer}>
              <TopLink
                activeView={this.state.activeView}
                setLinkView={this._setLinkView}
                linkView={this.state.linkView}
                notReadEventCtr={this.props.notReadEventCtr}
              />
            </div>
          ) : null}
          {this._renderView()}
        </GridContainer>
      </div>
    );
  }
}

export default withRouter(DoctorDashboard);

const topLinkContainer = css({
  display: "flex",
  "@media print": {
    display: "none"
  }
});

const main = css({
  margin: "20px auto 50px",
  padding: "0 30px",
  maxWidth: 1600,
  width: "100%"
});

let subscriptionDiv = css({
  display: "flex",
  flex: "1 0 100%",
  flexWrap: "wrap",
  //flexDirection: 'reverse',
  justifyContent: "flex-start",
  alignItems: "flex-start",
  borderRight: "1px dotted gray",
  ":last-child": { border: "none" }
});

let title = css({
  marginBottom: 20,
  textTransform: "uppercase",
  color: Colors.blueDarkAccent,
  textAlign: "left",
  fontWeight: 600
});

let content = css({
  display: "flex",
  flex: "1 0 90%",
  justifyContent: "flex-start",
  flexWrap: "wrap",
  margin: "10px 10px 10px 5%",
  padding: "5px 10px",
  "@media (max-width: 865px)": {
    justifyContent: "center"
  }
});

let warning = css({
  display: "flex",
  flex: "1 0 100%",
  justifyContent: "center",
  //marginTop: 20,
  alignItems: "center",
  color: "orange",
  textTransform: "uppercase",
  fontWeight: "bold"
});
