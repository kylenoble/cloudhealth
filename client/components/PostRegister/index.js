import React from 'react';
import Router from 'next/router';
import Link from 'next/link';

export default class extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
            btnTxt: 'Go to Dashboard...'
        };
        this._waiting = this._waiting.bind(this)
    }

    _waiting(){
      this.setState(
        {
          btnTxt: "Please Wait ..."
        }
      )
    }

   render() {
    return(
      <div style={styles.container}>
        <div>
            <h3 style={styles.text}>
              Thank you for signing up!
              You’ve just taken your first step to wellness.
              In order for us to help you better, please go to your
              dashboard and answer the questions in each category.
              Once you complete your health information, we will review,
              create your health plan and prepare your account so you can
              see a health professional on demand. You’re all set!
            </h3>
        </div>
        <Link prefetch href="/dashboard">
        <a>
        <button onClick={this._waiting} style={styles.button}>{this.state.btnTxt}</button>
        </a>
        </Link>
      </div>
    )
  }

  // _goToNext() {
  //   return this.props.onClick('dashboard')
  // }

}

const styles = {
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    maxWidth: '40em',
    marginTop: '80px',
    color: '#364563',
  },
  text: {
    color: '#364563',
    fontFamily: "Roboto-Medium",
    fontSize: "20px",
    textAlign: 'left',
    letterSpacing: 0,
    lineHeight: "35px",
  },
  button: {
    backgroundColor: '#77E9AF',
    width: '300px',
    padding: '11px 80px',
    border: 'none',
    borderRadius: '6px',
    color: 'white',
    fontFamily: '"Roboto",sans-serif',
    fontSize: '14px',
    margin: '30px 0px 20px 0px',
    cursor: 'pointer',
  },
}
