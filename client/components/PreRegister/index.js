import React from "react";
import Link from "next/link";

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isModalOpen: false
    };
  }

  render() {
    return (
      <div style={styles.container}>
        <div>
          <h2>
            Thanks for signing up for CloudHealth! <br /> The process for
            getting started is AS follows:
          </h2>{" "}
          <br />
          <h4>Step 1: Create an Account </h4>
          <br />
          <h4>Step 2: Complete Your Health Profile</h4> <br />
          <h4>Step 3: CloudHealth develops a personalized health plan</h4>{" "}
          <br />
          <h4>
            Step 4: See your health professional On-Demand via Two Way Video
          </h4>
        </div>

        <div>
          <button style={styles.button} onClick={() => this.openModal()}>
            Start My Profile
          </button>
          <Modal
            isOpen={this.state.isModalOpen}
            onClose={() => this.closeModal()}
          >
            <p>Personal and Health Information Collection Statement</p>

            <p>
              CloudHealthAsia is a joint venture between Romlas Holdings and
              Meraki Social Ventures.
            </p>

            <p>
              You may contact CloudHealthAsia by writing to LifeScience
              Institute 8F Accralaw Tower 30th street cor. 2nd ave. BGC Taguig,
              by emailing contact@merakisv.com. You have the right to gain
              access to the information held by CloudHealthAsia about you.
            </p>

            <p>
              Our Privacy Policy (available upon request) contains information
              on how you may request access to, and correction of, your personal
              information and how you may complain about a breach of your
              privacy and how we will deal with such a complaint.
            </p>

            <p>
              CloudHealthAsia needs to collect information about you for the
              primary purpose of creating a personalized health and wellness
              plan. In order to fully assess your health, we need to collect
              some personal information from you. This information will also be
              used for the administrative purposes of running the practice such
              as billing you or through an insurer or compensation agency.
              Information will be used within the practice for handover if
              another practitioner provides you with assistance.
            </p>

            <p>
              CloudHealthAsia may disclose information regarding existing
              diagnosis and laboratory results to your Doctor or other treatment
              providers only with your consent. In the case of insurance or
              compensation claims, it may be necessary to disclose information
              and/or collect information that affects your treatment and return
              to work. CloudHealthAsia will not disclose your information to
              commercial companies, however specific service or product
              information as deemed suitable for your management, may be
              forwarded to you by us, unless you instruct CloudHealthAsia not to
              forward this type of information. Data gathered however can be
              used in internal reports of the company and may be used in
              researches and publications, under standard protocols for research
              studies and clinical trials upon consent of the client.
            </p>

            <p>
              Information at CloudHealthAsia is stored securely and only
              practice staff has access to it. CloudHealthAsia takes all
              reasonable steps to ensure that information collected about you is
              accurate, complete and up-to-date. You may have access to your
              information on request and if you believe that any of the
              information is inaccurate we may be able to amend it accordingly.
              If you do not provide relevant personal or health information, in
              part or in full, it may result in incomplete assessment. This may
              impact on the wellness plan and the interventions that will be
              provided. Any concerns that you may have about this statement or
              about your management can be directed to contact@merakisv.com.{" "}
            </p>

            <p>
              Do you consent with your personal data being processed as
              described above? You must click Yes in order to create a health
              profile
            </p>

            <p>
              <button
                style={styles.button_yes}
                onClick={this._goToNext.bind(this)}
              >
                Yes
              </button>
              <button
                style={styles.button_no}
                onClick={() => this.closeModal()}
              >
                No
              </button>
            </p>
          </Modal>
        </div>
      </div>
    );
  }

  _goToNext() {
    return this.props.onClick("login");
  }

  openModal() {
    this.setState({ isModalOpen: true });
  }

  closeModal() {
    this.setState({ isModalOpen: false });
  }
}

class Modal extends React.Component {
  render() {
    if (this.props.isOpen === false) return null;

    let modalStyle = {
      position: "absolute",
      left: "50%",
      transform: "translate(-50%, -50%)",
      zIndex: "100",
      background: "#fff",
      fontFamily: '"Roboto-Black",sans-serif',
      fontSize: "13px",
      color: "rgb(54, 69, 99)",
      padding: "20px"
    };

    if (this.props.width && this.props.height) {
      modalStyle.width = this.props.width + "px";
      modalStyle.height = this.props.height + "px";
      (modalStyle.marginLeft = "-" + this.props.width / 2 + "px"),
        (modalStyle.marginTop = "-" + this.props.height / 2 + "px"),
        (modalStyle.transform = null);
    }

    if (this.props.style) {
      for (let key in this.props.style) {
        modalStyle[key] = this.props.style[key];
      }
    }

    let backdropStyle = {
      position: "absolute",
      width: "100%",
      height: "200%",
      top: "0px",
      left: "0px",
      zIndex: "100",
      background: "rgba(0, 0, 0, 0.5)"
    };

    if (this.props.backdropStyle) {
      for (let key in this.props.backdropStyle) {
        backdropStyle[key] = this.props.backdropStyle[key];
      }
    }

    return (
      <div className={this.props.containerClassName}>
        <div className={this.props.className} style={modalStyle}>
          {this.props.children}
        </div>
        {!this.props.noBackdrop && (
          <div
            className={this.props.backdropClassName}
            style={backdropStyle}
            onClick={e => this.close(e)}
          />
        )}
      </div>
    );
  }

  close(e) {
    e.preventDefault();

    if (this.props.onClose) {
      this.props.onClose();
    }
  }
}

const styles = {
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    flexWrap: "wrap",
    maxWidth: "40em",
    marginTop: "80px",
    color: "#364563",
    lineHeight: "34px"
  },
  dashItem: {
    fontFamily: "Roboto-Medium",
    fontSize: "18px",
    color: "#BDC7CF",
    letterSpacing: 0,
    lineHeight: "14px",
    padding: "20px 20px",
    margin: "-1px 25px",
    cursor: "pointer"
  },
  dashItemActive: {
    color: "#358ED7",
    borderBottom: "2px solid #358ED7"
  },
  button: {
    backgroundColor: "#77E9AF",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    margin: "30px 0px 20px 0px",
    cursor: "pointer"
  },
  button_yes: {
    backgroundColor: "#3498db",
    fontFamily: '"Roboto",sans-serif',
    color: "white",
    fontSize: "14px",
    padding: "8px 15px 8px 15px",
    cursor: "pointer",
    borderRadius: "6px"
  },
  button_no: {
    backgroundColor: "#3498db",
    fontFamily: '"Roboto",sans-serif',
    color: "white",
    fontSize: "14px",
    padding: "8px 11px 8px 11px",
    cursor: "pointer",
    borderRadius: "6px"
  }
};
