import React from "react";
import { labelClass } from '../NewSkin/Styles';
export default class extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      flexibility(document.getElementById("ch-login"));
    }
  }

  supportsFlexBox() {
    var test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  render() {
    return (
      <div>
        <div style={styles.container}>
          <div style={styles.introInfo}>
            <h2 className={labelClass}>{this.props.title}</h2>
            <h5>{this.props.message}</h5>
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}

const styles = {
  container: {
    display: "-webkit-flex",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  introInfo: {
    maxWidth: 450,
    textAlign: 'justify',
    lineHeight: 1.7
  },
  formContainer: {
    border: "2px solid rgba(151,151,151,0.2)",
    borderRadius: "6px",
    maxWidth: "400px",
    display: "-webkit-flex",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    boxShadow: "0px 1px 3px 0px rgba(0,0,0,0.20)"
  },
  header: {
    borderBottom: "2px solid rgba(225,232,238,1)",
    padding: "15px 20px",
    fontSize: "18px",
    color: "#364563",
    width: "360px"
  },
  form: {
    margin: "10px 30px 20px 20px"
  },
  formInput: {
    padding: "13px 30px 13px 30px",
    marginTop: "20px",
    width: "287px",
    border: "2px solid #E1E8EE",
    borderRadius: "6px"
  },
  options: {
    marginTop: "15px",
    padding: "13px 30px 13px 30px",
    WebkitAppearance: "none",
    MozAppearance: "none",
    appearance: "none",
    width: "350px",
    backgroundColor: "#fff",
    borderColor: "#d9d9d9 #ccc #b3b3b3",
    borderradius: "4px",
    border: "2px solid #E1E8EE",
    cursor: "default",
    borderSpacing: "0",
    borderCollapse: "separate",
    height: "50px",
    outline: "none",
    overflow: "hidden",
    position: "relative",
    cursor: "pointer"
  },
  optionsDefault: {
    color: "rgba(169, 169, 169, 1)"
  },
  selectArrowZone: {
    cursor: "pointer",
    display: "table-cell",
    position: "relative",
    textAlign: "center",
    verticalAlign: "middle",
    width: "25px",
    paddingRight: "5px"
  },
  selectArrow: {
    borderColor: "#999 transparent transparent",
    borderStyle: "solid",
    borderWidth: "5px 5px 2.5px",
    display: "inline-block",
    height: "0",
    width: "0",
    position: "absolute",
    right: "-18em",
    top: "-1.75em"
  },
  button: {
    backgroundColor: "#ED81B9",
    width: "300px",
    padding: "11px 80px",
    border: "none",
    borderRadius: "6px",
    color: "white",
    margin: "0px 0px 20px 0px",
    cursor: "pointer"
  },
  register: {
    textDecoration: "none",
    margin: "20px 10px",
    color: "#43484D"
  }
};
