const webpack = require("webpack");
require("dotenv").config();

const path = require("path");
const glob = require("glob");
const withProgressBar = require("next-progressbar");
const withBundleAnalyzer = require("@zeit/next-bundle-analyzer");

module.exports = withBundleAnalyzer({
  //target: "serverless",
  analyzeServer: ["server", "both"].includes(process.env.BUNDLE_ANALYZE),
  analyzeBrowser: ["browser", "both"].includes(process.env.BUNDLE_ANALYZE),
  bundleAnalyzerConfig: {
    server: {
      analyzerMode: "static",
      reportFilename: "../bundles/server.html"
    },
    browser: {
      analyzerMode: "static",
      reportFilename: "../bundles/client.html"
    }
  },
  webpack: config => {
    config.plugins.push(
      new webpack.DefinePlugin({
        "process.env.DOMAIN": JSON.stringify(process.env.DOMAIN),
        "process.env.QB_APP_ID": JSON.stringify(process.env.QB_APP_ID),
        "process.env.QB_APP_CONSUMER": JSON.stringify(
          process.env.QB_APP_CONSUMER
        ),
        "process.env.QB_APP_SECRET": JSON.stringify(process.env.QB_APP_SECRET),
        "process.env.SENDMAIL": JSON.stringify(process.env.SENDMAIL)
      })
      //new webpack.NormalModuleReplacementPlugin(/\/iconv-loader$/, "node-noop")
    );

    config.externals = {
      jsdom: "window",
      cheerio: "window",
      "react/lib/ExecutionEnvironment": true,
      "react/addons": true,
      fs: true,
      "react/lib/ReactContext": "window",
      sinon: "window.sinon"
    };

    config.module.rules.push(
      {
        test: /\.(css|scss)/,
        loader: "emit-file-loader",
        options: {
          name: "dist/[path][name].[ext]"
        }
      },
      {
        test: /\.css$/,
        use: ["babel-loader", "raw-loader", "postcss-loader"]
      },
      {
        test: /\.s(a|c)ss$/,
        use: [
          "babel-loader",
          "raw-loader",
          "postcss-loader",
          {
            loader: "sass-loader",
            options: {
              includePaths: ["styles", "node_modules"]
                .map(d => path.join(__dirname, d))
                .map(g => glob.sync(g))
                .reduce((a, c) => a.concat(c), [])
            }
          }
        ]
      }
    );

    return config;
  }
});
