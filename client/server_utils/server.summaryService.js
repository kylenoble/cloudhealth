import AuthService from "../utils/authService.js";
import "isomorphic-fetch";
import { parseCookies } from "nookies";
import queryString from "query-string";
import currentDomain from "../utils/domain.js";

const auth = new AuthService();
const cookie = parseCookies();
export default class ServerSummaryService {
  constructor(domain) {
    this.domain = domain || `${currentDomain}/api/summary`;
  }

  async request(token, params) {
    const { profile } = cookie;
    let fields = {};
    if (params) {
      if (!params.id) {
        fields.userId = profile.id;
        fields.userType = profile.type;
      } else {
        fields = { userId: params.id };
      }
    } else {
      fields = { userId: profile.id };
    }

    const url = this.domain + "?" + queryString.stringify(fields);
    //const url = `http://localhost:4040/api/summary?userId=${profile.id}`;

    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };

    headers["Authorization"] = "Bearer " + token;

    const response = await fetch(url, {
      headers,
      method: "GET"
    });

    if (response.status > 200) {
      return { error: response.statusText };
    }
    return response.json();
  }

  async getUserRank(token, params) {
    let fields = {};
    if (params.str === "All") {
      fields = { company: params.str, user_id: params.user_id };
    } else {
      fields = { company: params.company, user_id: params.user_id };
    }
    let url = this.domain + "/filtered?" + queryString.stringify(fields);

    //url = `http://localhost:4040/api/summary/filtered?company=All&user_id=1963`;

    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };

    headers["Authorization"] = "Bearer " + token;

    const response = await fetch(url, {
      headers,
      method: "GET"
    });

    if (response.status > 200) {
      return { error: response.statusText };
    }
    return response.json();
  }

  call(url, options, token) {
    // performs api calls sending the required authentication headers
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };

    headers["Authorization"] = "Bearer " + token;

    return fetch(url, {
      headers,
      ...options
    }).then(response => response.json());
  }
}
