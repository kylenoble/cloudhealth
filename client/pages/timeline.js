import React, {Component} from 'react'
import Head from 'next/head'
import Router from 'next/router'
import { css } from 'glamor'
import { confirmAlert } from 'react-confirm-alert'; // Import
import dateFormat from 'dateformat'; // not localize
import ReactTooltip from 'react-tooltip'
import stylesheet from '../style/globalStyle.scss'
import Queue from '../components/Queue'
import Header from '../components/Header'
import Menu from '../components/Menu'
import Footer from '../components/Footer'
import DashboardSwitcher from '../components/DashboardSwitcher'
import Timeline from '../components/Timeline'

import withAuth from  '../utils/withAuth.js'


class HealthProfile extends Component {
  constructor(props) {
    super(props)
    this.user = this.props.auth.getProfile()

    }

  componentDidMount() {

  }



render(){

  return(
    <div className = 'wrap'>
        <div className = "container">

            <style dangerouslySetInnerHTML={{ __html: stylesheet }} />

            <div className='headerDiv'>
                <Header
                  user={this.user}
                  logoutPressed={this._logout}
                />
            </div>
            <Menu user={this.user}/>
            <Timeline/>

        </div>
        <Footer/>
    </div>
  )
}

}



export default withAuth(HealthProfile)
// Removes withAuth decoration for testing
export let undecorated = HealthProfile;
