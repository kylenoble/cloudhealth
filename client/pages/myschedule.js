import React, { Component } from "react";
import dynamic from "next/dynamic";
import withAuth from "../utils/withAuth.js";
import Loading from "../components/NewSkin/Components/Loading";

const Template = dynamic(() => import("../components/template"), {
  loading: () => <Loading>Loading Template...</Loading>
});
const Restricted = dynamic(() => import("../components/restricted"));
const DoctorDashboard = dynamic(() => import("../components/DoctorDashboard"), {
  loading: () => <Loading>Preparing Data...</Loading>
});

const allowd_user = "Doctor";

const Page = props => {
  const user = props.auth.getProfile();

  return (
    <Template auth={props.auth}>
      {user.type === allowd_user ? (
        <DoctorDashboard user={user} active={false} activeView={"schedule"} />
      ) : (
        <Restricted message="Sorry you're not allowed to access this page." />
      )}
    </Template>
  );
};

export default withAuth(Page);
