import React from "react";
import dynamic from "next/dynamic";
import withAuth from "../utils/withAuth.js";
import Loading from "../components/NewSkin/Components/Loading";

import MyHealthActivities from "../components/MyHealthActivities";
import Template from "../components/template";

// const Template = dynamic(() => import("../components/template"), {
//   loading: () => <Loading>Loading Template...</Loading>
// });

const Restricted = dynamic(() => import("../components/restricted"));
// const MyHealthActivities = dynamic(
//   () => import("../components/MyHealthActivities"),
//   {
//     loading: () => <Loading>Preparing Data...</Loading>
//   }
// );

const allowd_user = "Patient";

const Page = props => {
  const user = props.auth.getProfile();

  return (
    <Template auth={props.auth}>
      {user.type === allowd_user ? (
        <MyHealthActivities user={user} />
      ) : (
        <Restricted message="Sorry you're not allowed to access this page." />
      )}
    </Template>
  );
};

export default withAuth(Page);
