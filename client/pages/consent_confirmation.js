import React from "react";
import Link from "next/link";
import Router from "next/router";

import Header from "../components/Header";
import Footer from "../components/Footer";
import DataConsentService from "../utils/consentService.js";
import PrivacyPolicyModal from "../components/Modals/privacyPolicy.js";
import TermOfUse from "../components/Modals/termOfUse.js";
import FAQ from "../components/Modals/faq.js";

const consent = new DataConsentService();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      privacyPolicyModalIsOpen: false,
      termOfUseModelIsOpen: false,
      qry_string: props.url.query,
      token_is_valid: true,
      consent_done: false,
      email: null,
      btnClicked: false
    };

    this._confirmConsent = this._confirmConsent.bind(this);
    this._openModal = this._openModal.bind(this);
    this._closeModal = this._closeModal.bind(this);
  }

  componentDidMount() {
    this._parseQry();
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      flexibility(document.getElementById("ch-login"));
    }
  }

  supportsFlexBox() {
    var test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  _parseQry() {
    var qry_string = this.state.qry_string;
    var keys = Object.keys(qry_string);
    if (
      keys !== null &&
      keys.length > 0 &&
      (qry_string.x == "0" || qry_string.x == "1") &&
      typeof qry_string.q !== "undefined"
    ) {
      var email_token = qry_string.q;
      var ans = qry_string.x;
      //$encoded_email = urlencode(base64_encode(base64_encode($email). '&&1c'.time()));
      var decoded_token = decodeURIComponent(email_token);
      var tmp_token = atob(decoded_token).split("&&1c");
      var email = atob(tmp_token[0]);

      var time = tmp_token[1];
      var time_today = Math.round(new Date().getTime() / 1000);

      if (time < time_today) {
        this.setState({ email: email });
        consent
          .checkConsent(email)
          .then(res => {
            var data = {};
            if (res.is_agreed === null) {
              data = { token_is_valid: true };
            } else {
              data = { token_is_valid: true, consent_done: true };
            }
            this.setState(data);
          })
          .catch(e => {
            console.log(e);
          });
      } else {
        this.setState({ token_is_valid: false });
      }
    }
  }

  _confirmConsent(ans) {
    var email = this.state.email;
    if (this.state.token_is_valid) {
      consent
        .update({ is_agreed: ans }, email)
        .then(result => {
          consent
          .checkConsent(email)
          .then(res => {
            console.log(res.is_agreed, ans)
            if (res.is_agreed === ans) {
              this.setState({ btnClicked: true });
            }
          })
          .catch(e => {
            console.log(e);
          });
        })
        .catch(err => {
          console.log("Error : " + err);
        });
    }
  }

  _closeModal(str) {
    this.setState({
      [str]: false
    });
  }

  _openModal(modalName) {
    this.setState({
      [modalName]: true
    });
  }

  render() {
    var html = "";
    if (this.state.token_is_valid === true) {
      if (this.state.consent_done === false) {
        if (this.state.qry_string.x == "1") {
          html = (
            <div>
              <p style={styles.register}>
                By confirming, you{" "}
                <span style={{ fontWeight: "bold" }}>AGREE</span> to be enrolled
                in our Corporate Health Indexing service and{" "}
                <span style={{ fontWeight: "bold" }}>DISCLOSE</span> the
                following information, through your Human Resource Manager:
              </p>
              <ol style={styles.list}>
                <li>Full Name</li>
                <li>Date of birth (exact day, month and year)</li>
                <li>Gender</li>
                <li>Official designation</li>
                <li>Department</li>
              </ol>
              <button
                disabled={this.state.btnClicked}
                style={this.state.btnClicked ? styles.disabled : styles.button}
                onClick={() => this._confirmConsent("yes")}
              >
                {this.state.btnClicked
                  ? "Thank you for submitting your response"
                  : "Confirm ENROLLMENT to this service"}
              </button>
            </div>
          );
        } else {
          html = (
            <div>
              <p style={styles.register}>
                By confirming, you{" "}
                <span style={{ fontWeight: "bold" }}>REFUSE</span> to be
                enrolled in our Corporate Health Indexing service. You will no
                longer receive updates and notifications about our health
                programs.
                <br />
                <br />
                Should you wish to change your decision in the future, please
                approach your Human Resource Manager to receive a new invite.
              </p>
              <button
                disabled={this.state.btnClicked}
                style={this.state.btnClicked ? styles.disabled : styles.button}
                onClick={() => this._confirmConsent("no")}
              >
                {this.state.btnClicked
                  ? "Thank you for submitting your response"
                  : "Confirm REFUSAL to be enrolled in this service"}
              </button>
            </div>
          );
        }
      } else {
        html = (
          <p style={styles.register}>
            You already submitted your answer. To change it, please approach
            your HR manager to get a new invite.
          </p>
        );
      }
    } else {
      html = <p style={styles.register}>Invalid Token</p>;
    }
    return (
      <div style={styles.maindiv}>
        <Header pathname="consent_confirmation" />
        <div style={styles.container} data-style="display: flex;">
          <div style={styles.formContainer}>
            <div style={styles.header}>
              <span>CONFIRMATION</span>
            </div>
            {html}
          </div>
        </div>
        <PrivacyPolicyModal
          modalIsOpen={this.state.privacyPolicyModalIsOpen}
          closeModal={this._closeModal}
        />
        <TermOfUse
          modalIsOpen={this.state.termOfUseModelIsOpen}
          closeModal={this._closeModal}
        />
        <FAQ
          modalIsOpen={this.state.faqModelIsOpen}
          closeModal={this._closeModal}
        />
        <Footer openModal={this._openModal} />
      </div>
    );
  }
}

const styles = {
  maindiv: {
    fontFamily: '"Roboto",sans-serif'
  },
  container: {
    JsDisplay: "flex",
    display: "-webkit-box" /* OLD - iOS 6-, Safari 3.1-6 */,
    display: "-moz-box" /* OLD - Firefox 19- (buggy but mostly works) */,
    display: "-ms-flexbox" /* TWEENER - IE 10 */,
    display: "-webkit-flex" /* NEW - Chrome */,
    display: "flex" /* NEW, Spec - Opera 12.1, Firefox 20+ */,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "100px",
    minHeight: "75vh"
  },
  formContainer: {
    width: "50%",
    maxWidth: "100%",
    JsDisplay: "flex",
    display: "-webkit-box" /* OLD - iOS 6-, Safari 3.1-6 */,
    display: "-moz-box" /* OLD - Firefox 19- (buggy but mostly works) */,
    display: "-ms-flexbox" /* TWEENER - IE 10 */,
    display: "-webkit-flex" /* NEW - Chrome */,
    display: "flex" /* NEW, Spec - Opera 12.1, Firefox 20+ */,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  header: {
    textAlign: "left",
    borderBottom: "2px solid rgba(225,232,238,1)",
    padding: "15px 0px",
    fontSize: "25px",
    color: "#364563",
    fontFamily: '"Roboto-Medium",sans-serif',
    width: "100%",
    display: "flex",
    flexDirection: "column",
    fontWeight: "bold"
  },
  button: {
    backgroundColor: "#36c4f1",
    width: "auto",
    padding: "11px 60px",
    border: "none",
    borderRadius: "6px",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    cursor: "pointer",
    display: "block",
    margin: "30px auto 20px",
    fontWeight: "bold"
  },
  register: {
    textAlign: "justify",
    fontSize: "18px"
  },
  list: {
    textAlign: "left",
    width: "100%",
    marginTop: "0",
    fontSize: "18px"
  },
  disabled: {
    color: "#000",
    backgroundColor: "rgb(196, 231, 242)",
    width: "auto",
    padding: "11px 60px",
    border: "none",
    borderRadius: "6px",
    fontFamily: '"Roboto",sans-serif',
    fontSize: "14px",
    cursor: "not-allowed",
    display: "block",
    margin: "30px auto 20px",
    fontWeight: "bold"
  }
};
