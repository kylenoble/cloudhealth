import React from "react";
import { css } from "glamor";
import dynamic from "next/dynamic";

import UserService from "../utils/userService.js";
import withAuth from "../utils/withAuth";
import Loading from "../components/NewSkin/Components/Loading";

const Template = dynamic(() => import("../components/template"), {
  loading: () => <Loading>Loading Template...</Loading>
});

const Restricted = dynamic(() => import("../components/restricted"));

import PatientTracker from "../components/PatientTracker/";
import PatientProfile from "../components/DoctorDashboard/patientProfile";

const User = new UserService();
const allowd_user = "Doctor";

class Patient_tracker extends React.Component {
  constructor(props) {
    super(props);
    this.user = this.props.auth.getProfile();

    this.state = {
      linkView: "tracker"
    };

    this._fetchQueuedPatients = this._fetchQueuedPatients.bind(this);
    this._renderView = this._renderView.bind(this);
    this._getPatientInfo = this._getPatientInfo.bind(this);
    this._setLinkView = this._setLinkView.bind(this);
    this._setUserID = this._setUserID.bind(this);
    this._restrict = this._restrict.bind(this);
  }

  _getConsults(obj) {
    return Consults.get(obj)
      .then(res => Promise.resolve(res))
      .catch(e => {
        console.log(e);
      });
  }

  _fetchQueuedPatients(date = null) {
    let qry;
    if (date) {
      qry = { active: this.props.active, apmt_datetime: date };
    } else {
      qry = { active: this.props.active };
    }
    return Appointment.getAppointmentsList(qry)
      .then(res => {
        if (res.name === "Error" || res.message === "Unauthorized") {
          console.log(res);
        } else {
          return Promise.resolve(res);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _setLinkView(linkView) {
    console.log("linkView", linkView);
    this.setState({
      linkView
    });
  }

  _setUserID(userid) {
    this.setState({
      userid
    });
  }

  _renderView() {
    if (this.state.linkView === "patientProfile") {
      return (
        <PatientProfile
          active={this.props.active}
          getAnswers={this._getAnswersByDistinctQuestion}
          getPatientInfo={this._getPatientInfoByID}
          userid={this.state.userid}
          getScores={this._getSummary}
          back={this._back}
          activeTab={this.props.activeTab}
          setActiveView={this.props.setActiveView}
          user={this.user}
          restricted={this.state.restricted}
        />
      );
    } else if (this.state.linkView === "tracker") {
      return (
        <PatientTracker
          user={this.user}
          getConsults={this._getConsults}
          getPatientInfo={this._getPatientInfo}
          linkView={this._setLinkView}
          setActiveView={this.props.setActiveView}
          userid={this._setUserID}
          setActiveTab={this.props.setActiveTab}
          getAppoinments={this._fetchQueuedPatients}
          restrict={this._restrict}
        />
      );
    }
  }

  _restrict(b) {
    this.setState({ restricted: b });
  }

  _getPatientInfo(string) {
    return User.getPatientInfo(string)
      .then(res => Promise.resolve(res))
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    return (
      <Template auth={this.props.auth}>
        {this.user.type === allowd_user ? (
          this._renderView()
        ) : (
          <Restricted message="Sorry you're not allowed to access this page." />
        )}
      </Template>
    );
  }
}

export default withAuth(Patient_tracker);
