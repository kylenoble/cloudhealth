/* global it, expect, describe */
import React from "react";
import { shallow, mount } from "enzyme";
import sinon from "sinon";

import App from "../index.js";

import localStorageMock from "../../utils/local-storage-mock.js";

Object.defineProperty(window, "localStorage", { value: localStorageMock });

describe("Index Page", () => {
  it('App shows "Page Header"', () => {
    const app = shallow(<App />);

    expect(app.find("h1").text()).toEqual(
      "Your Primary Care Doctor isJust a Click Away"
    );
  });

  it('App shows "Page SubHeader"', () => {
    const app = shallow(<App />);

    expect(app.find("p").text()).toEqual(
      "With CloudHealth’s digital platform,Achieving optimal health is just one click away"
    );
  });

  it("Supports Flexbox is true", () => {
    const app = shallow(<App />);

    expect(app.instance().supportsFlexBox()).toBeTruthy();
  });

  it("ComponentDidMount runs", () => {
    sinon.spy(App.prototype, "componentDidMount");
    const app = mount(<App />);

    expect(App.prototype.componentDidMount.calledOnce).toBeTruthy();
  });
});
