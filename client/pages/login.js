import React from 'react';
import Link from 'next/link';
import { css } from 'glamor';
import Router, { withRouter } from 'next/router';
import dynamic from 'next/dynamic';

import Header from '../components/Header';
import AuthService from '../utils/authService.js';
import UserService from '../utils/userService.js';

import { centered_left, combined, on_mobile } from '../components/NewSkin/Styles';
import NewFooter from '../components/NewSkin/Components/NewFooter';
import { headerAndFooterHeight, checkScreenResolution } from '../components/NewSkin/Methods';
import Loading from '../components/NewSkin/Components/Loading';

import Colors from '../components/NewSkin/Colors';
import { UserContext } from '../context';

const Form = dynamic(() => import('../components/Form'));
const PrivacyPolicyModal = dynamic(() => import('../components/Modals/privacyPolicy.js'));
const TermOfUse = dynamic(() => import('../components/Modals/termOfUse.js'));
const FAQ = dynamic(() => import('../components/Modals/faq.js'));
const CenteredWrapper = dynamic(() => import('../components/NewSkin/Wrappers/CenteredWrapper'));
const ContainedWrapper = dynamic(() => import('../components/NewSkin/Wrappers/ContainedWrapper'));
const TwoColumnWrapper = dynamic(() => import('../components/NewSkin/Wrappers/TwoColumnWrapper'));
const Text = dynamic(() => import('../components/NewSkin/Components/Text'));
const FormWrapper = dynamic(() => import('../components/NewSkin/Wrappers/FormWrapper'));

const auth = new AuthService();
const user = new UserService();

class Login extends React.Component {
	constructor(props) {
		super(props);
		this._isMounted = false;
		this.state = {
			loading                  : false,
			email                    : {
				id         : '',
				name       : 'email',
				// question: "Enter Email",
				question   : 'Email',
				type       : 'email',
				validation : 'required',
				options    : [],
				error      : {
					active  : false,
					message : 'Please enter a valid email address.'
				},
				value      : ''
			},
			password                 : {
				id         : '',
				name       : 'password',
				// question: "Enter Password",
				question   : 'Password',
				type       : 'password',
				validation : 'required',
				options    : [],
				error      : {
					active  : false,
					message :
						'Passwords must have at least 1 number, 1 Uppercase letter, 1 Lowercase letter, and a minimum of 8 characters'
				},
				value      : ''
			},
			formError                : {
				active  : false,
				message : ''
			},
			privacyPolicyModalIsOpen : false,
			termOfUseModelIsOpen     : false,
			qry_string               : props.router.query,
			screenSize               : 'xlarge'
		};

		this._isMounted = false;

		this._handleSubmit = this._handleSubmit.bind(this);
		this._saveActivity = this._saveActivity.bind(this);
		this._openModal = this._openModal.bind(this);
		this._closeModal = this._closeModal.bind(this);
		this._checkAutoLogin = this._checkAutoLogin.bind(this);
	}

	static contextType = UserContext;

	componentDidMount() {
		this._isMounted = true;

		if (this._isMounted) {
			const body = document.body;
			body.addEventListener('resize', this.resizing());
			body.onresize = this.resizing;
			body.style = `background: ${Colors.skyblue} !important`;

			const lowReso = checkScreenResolution();
			setTimeout(() => {
				const headerAndFooter = headerAndFooterHeight();
				this.setState({ pageLoaded: true, headerAndFooter });
			}, 200);

			this.setState({ lowReso });
		}

		if (auth.loggedIn()) {
			console.log('auth.loggedIn()auth.loggedIn()', auth.loggedIn());
			Router.push('/dashboard'); // redirect if you're already logged in
		}
		this._checkAutoLogin();
		if (this.supportsFlexBox()) {
			// Modern Flexbox is supported
		} else {
			// eslint-disable-next-line no-undef
			flexibility(document.getElementById('ch-login'));
		}
	}

	resizing = () => {
		const screensize = checkScreenResolution();
		this.setState({ screenSize: screensize });
	};

	componentWillUnmount() {
		this._isMounted = false;
		document.body.removeEventListener('resize', this.resizing);
	}

	supportsFlexBox() {
		// eslint-disable-next-line no-undef
		const test = document.createElement('test');

		test.style.display = 'flex';

		return test.style.display === 'flex';
	}

	_checkAutoLogin() {
		const qry_string = this.state.qry_string;
		const keys = Object.keys(qry_string);
		if (keys !== null && keys.length > 0) {
			//auto login if token is valid
			// eslint-disable-next-line no-undef
			const tmp_token = atob(decodeURIComponent(keys[0])).split('&&1c');
			// eslint-disable-next-line no-undef
			const token = atob(tmp_token[0]);
			const time = tmp_token[1];
			const time_today = Math.round(new Date().getTime() / 1000);
			if (time < time_today) {
				//get username and password of MD admin in users table with the given token
				user
					.getMDDataByToken(token)
					.then((result) => {
						if (result) {
							this._handleChange(result.email, 'email', true);
							this._handleChange(result.password, 'password', true);
							this._handleSubmit(result.id);
						}
					})
					.catch((err) => {
						console.log(`Error auto-login : ${err}`);
					});
			}
		}
	}

	_closeModal(str) {
		if (this._isMounted) {
			this.setState({
				[str] : false
			});
		}
	}

	_openModal(modalName) {
		if (this._isMounted) {
			this.setState({
				[modalName] : true
			});
		}
	}

	_saveActivity(data) {
		user
			.saveActivity(data)
			.then((result) => {
				if (result > 0) {
					console.log('activity saved');
				}
			})
			.catch((err) => {
				console.log(`Error saving activity : ${err}`);
			});
	}

	_handleSubmit(auto_id = 0) {
		// yay uncontrolled forms!
		if (this._isMounted) {
			this.setState({
				loading : true
			});
		}
		auth
			.login(this.state.email.value, this.state.password.value, auto_id)
			.then((res) => {
				const data = {
					user_id : res.id,
					action  : 'Login',
					details : { email: res.email, company: res.company, type: res.type }
				};
				this._saveActivity(data);

				if (res.status === 3 || res.is_active === false) {
					this.setState(
						{
							loading   : false,
							formError : {
								active  : true,
								message : 'Access Denied'
							}
						},
						() => auth.logout()
					);
				} else {
					Router.push('/dashboard');
				}
			})
			.catch((e) => {
				const eml = this.state.email.value;
				const data = { user_id: 0, action: 'Login', details: { email: eml } };
				this._saveActivity(data);
				if (this._isMounted) {
					this.setState({
						loading   : false,
						formError : {
							active  : true,
							message : e.error
						}
					});
				}
			}); // you would show/hide error messages with component state here
	}

	_handleChange(value, input, validated) {
		const inputName = input;
		const newInput = this.state[inputName];
		newInput.value = value;
		newInput.error.active = validated;
		if (this._isMounted) {
			this.setState({
				inputName : newInput
			});
		}
	}

	render() {
		const title = (
			<Text
				size={1.3}
				weight={700}
				color={Colors.blueDarkAccent}
				lineHeight="1"
				onMobileSize={1}
				styles={{ marginBottom: 20, textAlign: 'left' }}
				onMobileStyles={{ margin: 20, textAlign: 'center' }}
			>
				Log in to CloudHealthAsia
			</Text>
		);

		const password_reset = (
			<div className={combined([ otherLinks, p_reset, on_mobile([], { textAlign: 'center' }) ])}>
				<Link href="/register?step=reset">
					<a style={styles.register}>Forgot Password ?</a>
				</Link>
			</div>
		);

		const register = (
			<div className={combined([ otherLinks, on_mobile([], { textAlign: 'center' }) ])}>
				<Link href="/register">
					<a style={styles.register}>Don't have an account?</a>
				</Link>
			</div>
		);

		const ctx = this.context;
		return (
			<UserContext.Provider
				value={{
					...ctx,
					screenSize : this.state.screenSize
				}}
			>
				<div id="ch-login">
					{this.state.pageLoaded ? null : <Loading />}

					<Header pathname="login" />
					{this.state.loading ? (
						<Loading />
					) : (
						<ContainedWrapper headerAndFooterHeight={this.state.headerAndFooter}>
							<TwoColumnWrapper
								lowReso={this.state.lowReso}
								withImage={true}
								template="1.6fr 1fr"
								styles={{ lineHeight: 1, gridColumnGap: 100 }}
							>
								<CenteredWrapper to="left">
									{/* <div style={styles.container} data-style="display: flex;"> */}
									<FormWrapper
										deviceSize={this.state.screenSize}
										textAlign="left"
										onMobileTextAlign="center"
									>
										<Form
											from="login"
											btnWrapper={true}
											header={title}
											submitForm={this._handleSubmit}
											buttonText="Go to Dashboard"
											handleChange={this._handleChange.bind(this)}
											inputs={[ this.state.email, this.state.password ]}
											passwordReset={password_reset}
											secondaryOption={register}
											formError={this.state.formError}
											formSuccess={{
												active  : false,
												message : ''
											}}
										/>
									</FormWrapper>
									{/* </div> */}
								</CenteredWrapper>
							</TwoColumnWrapper>
						</ContainedWrapper>
					)}
					<PrivacyPolicyModal
						modalIsOpen={this.state.privacyPolicyModalIsOpen}
						closeModal={this._closeModal}
					/>
					<TermOfUse modalIsOpen={this.state.termOfUseModelIsOpen} closeModal={this._closeModal} />
					<FAQ modalIsOpen={this.state.faqModelIsOpen} closeModal={this._closeModal} />
					{/* <Footer openModal={this._openModal} /> */}
					<NewFooter state={this.state} openModal={this._openModal} closeModal={this._closeModal} />
				</div>
			</UserContext.Provider>
		);
	}
}

export default withRouter(Login);

const p_reset = css({
	'> a' : {
		marginTop : '20px !important',
		display   : 'inline-block'
	}
});

const otherLinks = css({
	padding    : '10px 0px',
	color      : Colors.blueDarkAccent,
	fontWeight : 600
});

const styles = {
	spinner   : {
		display        : 'flex',
		flex           : '1 0 100%',
		minHeight      : '90vh',
		justifyContent : 'center',
		alignItems     : 'center'
	},
	container : {
		JsDisplay      : 'flex',
		// display: "-webkit-box" /* OLD - iOS 6-, Safari 3.1-6 */,
		// display: "-moz-box" /* OLD - Firefox 19- (buggy but mostly works) */,
		// display: "-ms-flexbox" /* TWEENER - IE 10 */,
		// display: "-webkit-flex" /* NEW - Chrome */,
		display        : 'flex' /* NEW, Spec - Opera 12.1, Firefox 20+ */,
		flexDirection  : 'row',
		alignItems     : 'center',
		justifyContent : 'center',
		marginTop      : '100px',
		minHeight      : '75vh'
	},
	register  : {
		textDecoration : 'none',
		margin         : 'auto',
		marginBottom   : '5px',
		color          : '#364563',
		fontSize       : '14px',
		fontFamily     : '"Roboto",sans-serif'
	}
};
