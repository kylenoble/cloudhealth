import React, { Component } from "react";
import dynamic from "next/dynamic";
import withAuth from "../utils/withAuth.js";
import Loading from "../components/NewSkin/Components/Loading";

import Template from "../components/template";
import DashboardData from "../components/DashboardData";
import DoctorDashboard from "../components/DoctorDashboard";

import SubsciptionService from "../utils/subscriptionService";

const Subsription = new SubsciptionService();
const NOTIFICATION = [];

import { ProgramContext } from "../context";

// const Template = dynamic(() => import("../components/template"), {
//   loading: () => <Loading>Loading Template...</Loading>
// });

class Page extends Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.user = this.props.auth.getProfile();
    this.state = {
      programs: {},
      activeView: this.user.type == "Patient" ? "Dashboard" : "DoctorDashboard",
      activeUser: this.user.status != null
    };

    this._notification = this._notification.bind(this);
    this._returnDashboardView = this._returnDashboardView.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;

    if (this.state.activeUser && this.user.type !== "Patient") {
      const data = { is_basic: true };
      Subsription.getProgram(data).then(res => {
        if (res.length > 0) {
          if (this._isMounted) {
            this.setState({
              programs: res
            });
          }
        }
      });
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  _notification(nme, note, lnk) {
    let currentNotification = this.state.notification;
    let p = true;
    if (currentNotification && currentNotification.length > 0) {
      for (let i = 0; i < currentNotification.length; i++) {
        if (currentNotification[i]["name"] == nme) p = false;
      }
      if (p) NOTIFICATION.push({ name: nme, description: note, link: lnk });
    } else {
      NOTIFICATION.push({ name: nme, description: note, link: lnk });
    }
    //  console.log(currentNotification[0]['name']);
    if (this._isMounted) {
      this.setState({
        notification: NOTIFICATION
      });
    }
  }

  _returnDashboardView() {
    if (this.state.activeView === "Dashboard" && this.state.activeUser) {
      return (
        <DashboardData
          // showAddData={this._updateView}
          user={this.user}
          addnotification={this._notification}
        />
      );
    } else if (this.state.activeView === "DoctorDashboard") {
      return (
        <ProgramContext.Provider value={{ programs: this.state.programs }}>
          <DoctorDashboard
            user={this.user}
            active={true}
            activeView={"DoctorDashboard"}
            setActiveView={this._setActiveView}
            activeTab={this.state.activeTab}
            setActiveTab={this._setActiveTab}
            setNofication={this._setNotifacation}
            notReadEventCtr={this.state.notReadEventCtr}
          />
        </ProgramContext.Provider>
      );
    }
  }

  _setActiveView(activeView) {
    if (this._isMounted) {
      this.setState({
        activeView: activeView
      });
    }
  }

  _setActiveTab(activeTab) {
    if (this._isMounted) {
      this.setState({
        activeTab: activeTab
      });
    }
  }

  render() {
    return (
      <Template auth={this.props.auth}>{this._returnDashboardView()}</Template>
    );
  }
}

export default withAuth(Page);
