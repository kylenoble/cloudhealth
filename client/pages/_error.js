import React from "react";
import Header from "../components/Header";
import Footer from "../components/Footer";
import Router from "next/router";
import PrivacyPolicyModal from "../components/Modals/privacyPolicy.js";
import TermOfUse from "../components/Modals/termOfUse.js";
import { css } from "glamor";
import MaintenanceMode from "../components/Maintenance";

import AppStatus from "../utils/checkAppStatus.js";
const appStatus = new AppStatus();

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      privacyPolicyModalIsOpen: false,
      termOfUseModelIsOpen: false,
      app_status: null
    };
    this._openModal = this._openModal.bind(this);
    this._closeModal = this._closeModal.bind(this);
  }

  //   componentWillUnmount() {
  //     clearInterval(this.state.intervalId);
  //   }

  //   componentDidMount() {
  //     let intervalId = setInterval(() => {
  //       this.getStatus()
  //     }, 10000);

  //     this.setState({ intervalId: intervalId });
  //     this.getStatus()
  //   }

  //   getStatus() {
  //     appStatus._getStatus()
  //       .then(r => {
  //         this.setState({ app_status: r }, () => {
  //           if (r === 'Active') Router.push('/dashboard')
  //         })
  //       })
  //       .catch(e => console.log(e))
  //   }

  _closeModal(str) {
    this.setState({
      [str]: false
    });
  }

  _openModal(modalName) {
    this.setState({
      [modalName]: true
    });
  }

  render() {
    return (
      <div className="container">
        <MaintenanceMode message="This page is not available" />
      </div>
    );
  }
}

// use glamor style here to supper flexbox
let messageDiv = css({
  display: "flex",
  flexWrap: "wrap",
  flex: "1",
  minHeight: "70vh",
  justifyContent: "center",
  alignItems: "center",
  textAlign: "center",
  fontSize: "large"
});

const styles = {
  container: {
    fontFamily: '"Roboto",sans-serif',
    margin: "0 auto",
    maxWidth: "40em",
    lineHeight: "1.5",
    padding: "4em 1em",
    minHeight: "70vh"
  },
  title: {
    flex: "1 0 100%",
    padding: 10,
    display: "flex",
    flexWrap: "wrap",
    alignItems: "center",
    justifyContent: "center",
    background: "#52617f",
    borderRadius: 10,
    border: "1px dotted",
    color: "#fff"
  },
  errorMessage: {}
};
