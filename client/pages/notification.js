import React, { useEffect, useContext, useState } from "react";
import dynamic from "next/dynamic";
import withAuth from "../utils/withAuth.js";
import Loading from "../components/NewSkin/Components/Loading";
import GridContainer from "../components/NewSkin/Wrappers/GridContainer.js";
import { headerAndFooterHeight } from "../components/NewSkin/Methods/index.js";

const Template = dynamic(() => import("../components/template"), {
  loading: () => <Loading>Loading Template </Loading>
});
const Notifications = dynamic(() => import("../components/Notifications"), {
  loading: () => <Loading>Preparing Data </Loading>
});

const Page = props => {
  const user = props.auth.getProfile();
  const [hf, setHf] = useState(0);
  const [isMounted, setIsMounted] = useState(false)

  useEffect(() => {
    setIsMounted(true);

    if (isMounted) {
      setTimeout(() => {
        const HaF = headerAndFooterHeight();
        setHf(HaF);
      }, 500);
    }

    return () => {
      setIsMounted(false);
    }
  }, [isMounted]);


  return (
    <Template auth={props.auth}>
      <Notifications user={user} />
    </Template>
  );
};

export default withAuth(Page);
