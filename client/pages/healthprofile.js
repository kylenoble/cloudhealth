import React, { useState, Component } from "react";
import dynamic from "next/dynamic";
import { parseCookies } from "nookies";
import withAuth from "../utils/withAuth.js";
import Loading from "../components/NewSkin/Components/Loading";
import MyHealthProfile from "../components/MyHealthProfile";
import { UserContext } from "../context";
const Template = dynamic(() => import("../components/template"), {
  loading: () => <Loading>Loading Template...</Loading>
});
const Restricted = dynamic(() => import("../components/restricted"));

import SummaryService from "../utils/summaryService.js";
import ServerSummaryService from "../server_utils/server.summaryService";

const summaryService = new SummaryService();
const serverSummaryService = new ServerSummaryService();
class Page extends Component {
  // static async getInitialProps(ctx) {
  //   // Parse
  //   const cookie = parseCookies(ctx);
  //   if (cookie && cookie.id_token) {
  //     const profile = JSON.parse(cookie.profile);
  //     const token = cookie.id_token;
  //     if (profile.type !== "Patient") {
  //       return;
  //     }
  //     const summaryparams = {
  //       id: 1963
  //     };

  //     let summary = {};
  //     const summaryRequest = await serverSummaryService.request(
  //       token,
  //       summaryparams
  //     );

  //     if (summaryRequest.error) {
  //       summary = { summary: { error: summaryRequest.error } };
  //     } else {
  //       summary = { summary: summaryRequest };
  //     }

  //     //////// _getPercentileRank

  //     const percentileRankyparams = {
  //       str: "All",
  //       company: profile.company,
  //       user_id: profile.id
  //     };
  //     let percentileRank = {};
  //     const percentileRankRequest = await serverSummaryService.getUserRank(
  //       token,
  //       percentileRankyparams
  //     );

  //     if (percentileRankRequest.error) {
  //       percentileRank = {
  //         percentileRank: { error: percentileRankRequest.error }
  //       };
  //     } else {
  //       percentileRank = { percentileRank: percentileRankRequest };
  //     }

  //     return { summary: summary, percentileRank: percentileRank };
  //   }
  //   return {};
  // }

  constructor(props) {
    super(props);
    this.user = props.auth.getProfile();
    this.state = {
      fetching: false,
      surveyCompleteModalOpen: false,
      survey: {
        body: {},
        nutrition: {},
        sleep: {},
        health: {},
        stress: {},
        exercise: {},
        overAllScore: {},
        rank: "",
        overAllRank: "",
        overAllStatus: "",
        complete: false
      },
      healthSummaryData: []
    };

    this._openModal = this._openModal.bind(this);
    this._getHealthSurveySummary = this._getHealthSurveySummary.bind(this);
    this._getPercentileRank = this._getPercentileRank.bind(this);
    this._getSummary = this._getSummary.bind(this);
  }

  componentDidMount() {
    this._getSummary();
    this._getPercentileRank();
    this._getHealthSurveySummary();
  }

  _getSummary() {
    summaryService
      .get()
      .then(res => {
        console.log('resssssssssssssssssss', res)
        if (res.name === "Error") {
          console.log(res);
        } else if (res.length > 0) {
          this._createSummary(res);
        }
      })
      .catch(e => {
        console.log(e);
      });
  }
  _getStatus(score) {
    const wtdScore = score * 2;
    let subOptimalLabel = "";
    if (wtdScore >= 1.0 && wtdScore <= 2.9) {
      subOptimalLabel = "Optimal";
    } else if (wtdScore >= 3.0 && wtdScore <= 4.9) {
      subOptimalLabel = "Suboptimal";
    } else if (wtdScore >= 5.0 && wtdScore <= 5.9) {
      subOptimalLabel = "Neutral";
    } else if (wtdScore >= 6.0 && wtdScore <= 8.5) {
      subOptimalLabel = "Compromised";
    } else if (wtdScore >= 8.6) {
      subOptimalLabel = "Alarming";
    }

    return subOptimalLabel;
  }
  _createSummary(summaries) {
    const { survey } = this.state;

    let ltstBodyVal;
    let ltstExerciseVal;
    let ltstStressVal;
    let ltstNutritionVal;
    let ltstSleepVal;
    let ltstHealthVal;
console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxx')
    for (const summary of summaries) {
      const score = summary.value;
      let weightedScore = score * 0.167;

      if (summary.name === "weightScore") {
        ltstBodyVal = weightedScore;
        survey.body.value = score;
        survey.body.HStatus = this._getStatus(score);
      } else if (summary.name === "bmi") {
        weightedScore = 0;
        survey.body.bmi = summary.value;
      } else if (summary.name === "whr") {
        weightedScore = 0;
        survey.body.whr = summary.value;
      } else if (summary.name === "nutrition") {
        ltstNutritionVal = weightedScore;
        survey.nutrition.value = score;
        survey.nutrition.HStatus = this._getStatus(score);
      } else if (summary.name === "sleep") {
        ltstSleepVal = weightedScore;
        survey.sleep.value = score;
        survey.sleep.HStatus = this._getStatus(score);
      } else if (summary.name === "detox") {
        ltstHealthVal = weightedScore;
        survey.health.value = score;
        survey.health.HStatus = this._getStatus(score);
      } else if (summary.name === "stress") {
        ltstStressVal = weightedScore;
        survey.stress.value = score;
        survey.stress.HStatus = this._getStatus(score);
      } else if (summary.name === "exercise") {
        ltstExerciseVal = weightedScore;
        survey.exercise.value = score;
        survey.exercise.HStatus = this._getStatus(score);
      }
    }

    const hs =
      ltstBodyVal +
      ltstExerciseVal +
      ltstStressVal +
      ltstNutritionVal +
      ltstSleepVal +
      ltstHealthVal;
    survey.overAllScore.value = hs * 2;
    survey.overAllStatus = this._getStatus(survey.overAllScore.value / 2);

    this.setState({
      survey
    });

    if (survey.body.bmi < 15 || survey.body.bmi > 50) {
      this._addNotification(
        "error in bmi",
        "Please update your weight profile to correct the BMI. (Go to MY HEALTH PROFILE)",
        "HealthProfile"
      );
    }
  }
  _getPercentileRank() {
    //USER RANK
    const { survey } = this.state;
    summaryService
      .getUserRank()
      .then(res => {
        if (res.name === "Error") {
          console.log(res);
        } else if (res !== undefined) {
          const position = parseInt(res.rank);
          const usersCount = parseInt(res.users_count);
          let percentile = 0;

          percentile = (100 * (position - 0.5)) / usersCount;

          let pRank = 0;
          if (percentile > 0) {
            pRank = Math.round(percentile);
          }

          survey.rank = `${parseInt(pRank)}%`;
        }
      })
      .catch(e => {
        console.log(e);
      });

    //OVERALL RANK
    summaryService
      .getUserRank("All")
      .then(res => {
        if (res.name === "Error") {
          console.log(res);
        } else if (res !== undefined) {
          const position = parseInt(res.rank);
          const usersCount = parseInt(res.users_count);
          let percentile = 0;

          percentile = (100 * (position - 0.5)) / usersCount;
          let oRank = 0;
          if (percentile > 0) {
            oRank = Math.round(percentile);
          }

          survey.overAllRank = `${parseInt(oRank)}%`;

          survey.complete = true;
        }
      })
      .catch(e => {
        console.log(e);
      });

    this.setState({
      survey
    });
  }

  _getHealthSurveySummary() {
    this.setState({ fetching: true });
    let { healthSummaryData } = this.state;
    summaryService.getHealthSurveySummary(this.user.id).then(r => {
      if (r) {
        healthSummaryData = r;
        this.setState({ fetching: false, healthSummaryData });
      }
    });
  }
  _openModal(modalName) {
    this.setState({
      [modalName]: true
    });
  }
  _closeModal(modalName) {
    this.setState({
      [modalName]: false
    });
  }
  _checkAlloweduser() {
    // test if user type is allowd to acces this page
    const allowd_user = "Patient"; // assing allowed user here

    if (this.user.type === allowd_user) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    return (
      <Template auth={this.props.auth}>
        {this._checkAlloweduser() ? (
          <UserContext.Provider
            value={{
              fetching: this.state.fetching,
              summaryService,
              survey: this.state.survey,
              healthSummaryData: this.state.healthSummaryData,
              reFetchHealthSummary: this._getHealthSurveySummary,
              user: this.user,
              getPercentileRank: this._getPercentileRank,
              refetchSummary: this._getSummary
            }}
          >
            <MyHealthProfile
              user={this.user}
              closeModal={this._closeModal}
              openModal={this._openModal}
            />
          </UserContext.Provider>
        ) : (
            <Restricted message="Sorry you're not allowed to access this page." />
          )}
      </Template>
    );
  }
}

export default withAuth(Page);
