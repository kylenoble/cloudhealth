import React, {Component} from 'react';
import Header from '../components/Header';
import Main from '../components/Emr';
import stylesheet from '../components/DashboardData/dashboardStyle.scss'
class Emr extends Component {
  constructor(props) {
    super(props)

    this.state = {

    }
  }

  render() {
    return(
      <div>
      <style dangerouslySetInnerHTML={{ __html: stylesheet }} />
        <Header pathname='login'/>
        <div className='container'>
          <Main/>
        </div>

      </div>
    )
  }
}

export default Emr
