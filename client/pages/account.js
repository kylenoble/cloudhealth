import React from "react";
import dynamic from "next/dynamic";

import withAuth from "../utils/withAuth.js";
import Loading from "../components/NewSkin/Components/Loading";
import MyAccount from "../components/MyAccount";
const Template = dynamic(() => import("../components/template"), {
  loading: () => <Loading>Loading Template...</Loading>
});
// const MyAccount = dynamic(() => import("../components/MyAccount"), {
//   loading: () => <Loading>Preparing Data...</Loading>
// });

const Page = props => {
  const user = props.auth.getProfile();

  return (
    <Template auth={props.auth}>
      <MyAccount user={user} />
    </Template>
  );
};

export default withAuth(Page);
