/* eslint-disable no-dupe-keys */
/* eslint-disable no-undef */
import React from "react";
import Router from "next/router";
import Button from "../components/NewSkin/Components/Button.js";
import { Line } from "rc-progress";
import { css } from "glamor";

import Header from "../components/Header";
// import Footer from "../components/Footer";
import ResetPassword from "../components/Password";
import Success from "../components/Success";
import AnswerService from "../utils/answerService.js";
import PrivacyPolicyModal from "../components/Modals/privacyPolicy.js";
import TermOfUse from "../components/Modals/termOfUse.js";
import AuthService from "../utils/authService.js";
import NewFooter from "../components/NewSkin/Components/NewFooter";
import ContainedWrapper from "../components/NewSkin/Wrappers/ContainedWrapper";
import TwoColumnWrapper from "../components/NewSkin/Wrappers/TwoColumnWrapper";
import {
  headerAndFooterHeight,
  checkScreenResolution
} from "../components/NewSkin/Methods";
import Text from "../components/NewSkin/Components/Text";
import Colors from "../components/NewSkin/Colors";

const answer = new AnswerService();

const auth = new AuthService();

export default class extends React.Component {
  constructor(props) {
    super(props);
    let progress;
    if (this.props.url.query.step === "login" || !this.props.url.query.step) {
      progress = 0;
    } else if (this.props.url.query.step === "personal") {
      progress = 25;
    } else if (this.props.url.query.step === "readiness") {
      progress = 50;
    } else if (this.props.url.query.step === "goals") {
      progress = 75;
    } else if (this.props.url.query.step === "post") {
      progress = 100;
    }

    this.state = {
      currentStep: this.props.url.query.step,
      progress,
      privacyPolicyModalIsOpen: false,
      termOfUseModelIsOpen: false,
      link: {
        text: "Login",
        active: true
      }
    };

    this._isMounted = false;

    this._updateStep = this._updateStep.bind(this);
    this._logout = this._logout.bind(this);
    this._getPersonal = this._getPersonal.bind(this);
    this._getReadiness = this._getReadiness.bind(this);
    this._getGoal = this._getGoal.bind(this);
    this._openModal = this._openModal.bind(this);
    this._closeModal = this._closeModal.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      // eslint-disable-next-line no-undef
      flexibility(document.getElementById("__next"));
      this.setState({
        hideProgress: true
      });
    }

    if (auth.loggedIn()) {
      this.setState({ logged: true });
      // GET THE PROGRESS ACCORDING TO DATA FROM DATABASE
      // this will control the directly input link to the browser
      this._getPersonal();
    } else {
      //console.log(this.state.currentStep);
      // eslint-disable-next-line no-lonely-if
      if (this.state.currentStep && this.state.currentStep !== "reset") {
        this.setState({
          currentStep: "login",
          progress: 0
        });
      }
    }

    if (this._isMounted) {
      document.body.addEventListener("resize", this.resizing());
      document.body.onresize = this.resizing;

      const headerAndFooter = headerAndFooterHeight();
      const lowReso = checkScreenResolution();
      this.setState({ headerAndFooter, lowReso });
    }
  }

  componentWillUnmount() {
    document.body.removeEventListener("resize", this.resizing);
  }

  resizing = () => {
    const screensize = checkScreenResolution();
    this.setState({ screenSize: screensize });
  };

  // THIS WILL CHECK IF THE PERSONAL INFO FILLED UP if not redirect to peronal form else go to dashbaord
  _getPersonal() {
    answer
      .getAnswerList({ column: "question_id", operator: "eq", value: 1 })
      .then(res => {
        if (res.length > 0) {
          this._getReadiness();
        } else {
          this.setState({
            currentStep: "personal",
            progress: 25
          });
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  // CHECK IF READINESS IS FILLED 11 T0 17
  _getReadiness() {
    answer
      .getAnswerList({ column: "question_id", operator: "eq", value: 11 })
      .then(res => {
        if (res.length > 0) {
          this._getGoal();
        } else {
          this.setState({
            currentStep: "readiness",
            progress: 50
          });
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  // CHECK IF GOAL WAS FILLED 133
  _getGoal() {
    answer
      .getAnswerList({ column: "question_id", operator: "eq", value: 133 })
      .then(res => {
        if (res.length > 0) {
          // do something
        } else {
          this.setState({
            currentStep: "goals",
            progress: 75
          });
        }
      })
      .catch(e => {
        console.log(e);
      });
  }

  _logout(e) {
    e.preventDefault();
    auth
      .logout()
      .then(() => {
        Router.push("/login");
      })
      .catch(err => console.log(err)); // you would show/hide error messages with component state here
  }

  supportsFlexBox() {
    const test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  _closeModal(str) {
    this.setState({
      [str]: false
    });
  }

  _openModal(modalName) {
    this.setState({
      [modalName]: true
    });
  }

  _contactHRD() {
    return (
      // <div style={styles.warning}>
      <Text
        onMobileStyles={{ marginTop: 20 }}
        styles={{ textAlign: "left" }}
        size={1.3}
        weight={600}
        color={Colors.blueDarkAccent}
      >
        Please contact your <br /> human resource department <br /> to avail of
        this service.
      </Text>

      // </div>
    );
  }

  _displayProgress() {
    if (this.state.currentStep && !this.state.hideProgress) {
      return (
        <div style={styles.progressContainer} data-style="display: flex;">
          <span style={styles.progressText}>
            Your Profile is {this.state.progress}% Complete
          </span>
          <Line
            style={styles.progress}
            percent={this.state.progress}
            strokeWidth="1"
            trailWidth="2"
            strokeColor="#ED81B9"
            trailColor="#D9D9D9"
          />
        </div>
      );
    }
  }

  _returnStep() {
    if (this.state.currentStep === "reset") {
      return (
        <ResetPassword
          screenSize={this.state.screenSize}
          onClick={this._updateStep}
        />
      );
    } else if (this.state.currentStep === "success") {
      return (
        <Success
          title="Password reset done successfully"
          message="Thank you for your request. If that email address is found in our system, we will send you an email with your new password. We recommend that you change your password immediately upon login."
        >
          <Button
            type={this.state.link.active ? 'pink' : 'disabled'}
            styles={{marginTop: 30}}
          >
              <button 
                onClick={this.state.link.active ? 
              () => this._goTo("login") : null }>{this.state.link.text}</button>
          </Button>
        </Success>
      );
    }
    return this._contactHRD();
  }

  _updateStep(step) {
    if (step === "dashboard") {
      const url = "/dashboard";
      Router.push(url);
      return;
    }
    const url = `/register?step=${step}`;
    Router.push(url);
    this._updateCurrentStep(step);
  }

  _updateCurrentStep(step) {
    let progress;
    if (step === "login") {
      progress = 0;
    } else if (step === "personal") {
      progress = 25;
    } else if (step === "readiness") {
      progress = 50;
    } else if (step === "goals") {
      progress = 75;
    } else if (step === "post") {
      progress = 100;
    }
    this.setState({
      currentStep: step,
      progress
    });
  }

  _goTo(url) {
    const link = this.state.link;
    link.text = "Going....";
    link.active = false;
    this.setState(
      {
        link
      },
      () => {
        Router.push(`/${url}`);
      }
    );
  }

  render() {
    return (
      <div style={styles.maindiv}>
        <Header
          pathname="register"
          step={this.state.currentStep}
          logoutPressed={this._logout}
          logged={this.state.logged}
        />
        <ContainedWrapper headerAndFooterHeight={this.state.headerAndFooter}>
          <TwoColumnWrapper
            onMobileStyles={{ height: "fit-content" }}
            lowReso={this.state.lowReso}
            withImage={this.state.currentStep !== "reset"}
            template={this.state.currentStep === "reset" ? "1fr" : "1.6fr 1fr"}
            styles={{ lineHeight: 1, gridColumnGap: 100 }}
          >
            <div style={styles.container} data-style="display: flex;">
              {/**this._displayProgress()**/}
              {this._returnStep()}
            </div>
          </TwoColumnWrapper>
        </ContainedWrapper>
        <PrivacyPolicyModal
          modalIsOpen={this.state.privacyPolicyModalIsOpen}
          closeModal={this._closeModal}
        />
        <TermOfUse
          modalIsOpen={this.state.termOfUseModelIsOpen}
          closeModal={this._closeModal}
        />
        <NewFooter
          state={this.state}
          openModal={this._openModal}
          closeModal={this._closeModal}
        />
      </div>
    );
  }
}

const styles = {
  maindiv: {
    fontFamily: '"Roboto",sans-serif'
  },
  container: {
    // marginTop: "95px",
    // backgroundImage:'url("/static/images/rightcorner.svg"), url("/static/images/optimumhealthImage.svg")',
    backgroundRepeat: "no-repeat, no-repeat",
    backgroundPosition: "top right, bottom right",
    backgroundSize: "100px, 200px",
    fontFamily: '"Roboto",sans-serif',
    JsDisplay: "flex",
    display: "-webkit-box" /* OLD - iOS 6-, Safari 3.1-6 */,
    display: "-moz-box" /* OLD - Firefox 19- (buggy but mostly works) */,
    display: "-ms-flexbox" /* TWEENER - IE 10 */,
    display: "-webkit-flex" /* NEW - Chrome */,
    display: "flex" /* NEW, Spec - Opera 12.1, Firefox 20+ */,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
    // overflowX: "hidden",
    // minHeight: "75vh"
  },
  title: {
    JsDisplay: "flex",
    display: "-webkit-box" /* OLD - iOS 6-, Safari 3.1-6 */,
    display: "-moz-box" /* OLD - Firefox 19- (buggy but mostly works) */,
    display: "-ms-flexbox" /* TWEENER - IE 10 */,
    display: "-webkit-flex" /* NEW - Chrome */,
    display: "flex" /* NEW, Spec - Opera 12.1, Firefox 20+ */,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  progressContainer: {
    JsDisplay: "flex",
    display: "-webkit-box" /* OLD - iOS 6-, Safari 3.1-6 */,
    display: "-moz-box" /* OLD - Firefox 19- (buggy but mostly works) */,
    display: "-ms-flexbox" /* TWEENER - IE 10 */,
    display: "-webkit-flex" /* NEW - Chrome */,
    display: "flex" /* NEW, Spec - Opera 12.1, Firefox 20+ */,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "80px",
    boxShadow: "-1px 0px 8px #D9D9D9",
    padding: "15px",
    width: "50%",
    maxHeight: "30px"
  },
  progressText: {
    fontFamily: '"Roboto-Medium",sans-serif',
    marginBottom: "5px",
    fontSize: "14px",
    color: "#364563"
  },
  warning: {
    display: "flex",
    //  height: '85vh',
    justifyContent: "center",
    alignItems: "center",
    fontSize: "large"
  }
};