import React from 'react'
import Header from '../components/Header'
import Footer from '../components/Footer'
import PrivacyPolicyModal from '../components/Modals/privacyPolicy.js'
import TermOfUse from '../components/Modals/termOfUse.js'
import {css} from 'glamor'
export default class extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      privacyPolicyModalIsOpen: false,
      termOfUseModelIsOpen: false
    }
    this._openModal = this._openModal.bind(this)
    this._closeModal = this._closeModal.bind(this)
  }

  _closeModal(str) {
    this.setState({
      [str]: false
    });

  }


  _openModal(modalName){
    this.setState({
      [modalName]: true
    })
  }

  render() {
    return(
      <div className="container">
        <Header />
        <div style={styles.container}>
          <div className={messageDive}>
            “You’re no longer enrolled to this service. Please contact your human resource department or your wellness coordinator for re-enrollment and assistance. Thank you.”
          </div>
        </div>
        <PrivacyPolicyModal modalIsOpen={this.state.privacyPolicyModalIsOpen} closeModal={this._closeModal}/>
        <TermOfUse modalIsOpen={this.state.termOfUseModelIsOpen} closeModal={this._closeModal}/>
        <Footer openModal={this._openModal}/>
      </div>
    )
  }
}


// use glamor style here to supper flexbox
let messageDive = css({
  display: 'flex',
  flex: '1',
  minHeight: '70vh',
  justifyContent: 'center',
  alignItems: 'center',
  textAlign: 'center',
  fontSize: 'large'
})

const styles = {
  container: {
    fontFamily: '"Roboto",sans-serif',
    margin: '0 auto',
    maxWidth: '40em',
    lineHeight: '1.5',
    padding: '4em 1em',
    minHeight: '70vh'
  },
  title: {
    display: '-webkit-flex',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  errorMessage:{

  }

}
