import React from "react";
import dynamic from "next/dynamic";
import withAuth from "../utils/withAuth";
import Loading from "../components/NewSkin/Components/Loading";

const Template = dynamic(() => import("../components/template"), {
  loading: () => <Loading>Loading Template...</Loading>
});
const Restricted = dynamic(() => import("../components/restricted"));
const PatientQueue = dynamic(
  () => import("../components/Appointments/patientQueue"),
  {
    loading: () => <Loading>Preparing Data...</Loading>
  }
);

const allowd_user = "Doctor";

const Appointments = props => {
  const user = props.auth.getProfile();

  return (
    <Template auth={props.auth}>
      {user.type === allowd_user ? (
        <PatientQueue user={user} />
      ) : (
        <Restricted message="Sorry you're not allowed to access this page." />
      )}
    </Template>
  );
};

export default withAuth(Appointments);
