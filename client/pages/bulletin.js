import React, { Component } from "react";
import dynamic from "next/dynamic";
import withAuth from "../utils/withAuth.js";
import Loading from "../components/NewSkin/Components/Loading";

const Template = dynamic(() => import("../components/template"), {
  loading: () => <Loading>Loading Template...</Loading>
});
const Restricted = dynamic(() => import("../components/restricted"));
const Bulletin = dynamic(() => import("../components/Bulletin"), {
  loading: () => <Loading>Preparing Data...</Loading>
});

class Page extends Component {
  constructor(props) {
    super(props);
    this.user = this.props.auth.getProfile();
  }

  _checkAlloweduser() {
    // test if user type is allowd to acces this page
    let allowd_user = "Patient"; // assing allowed user here

    if (this.user.type === allowd_user) {
      return true;
    } else {
      return false;
    }
  }

  render() {
    return (
      <Template auth={this.props.auth}>
        {this._checkAlloweduser() ? (
          <Bulletin user={this.user} />
        ) : (
          <Restricted message="Sorry you're not allowed to access this page." />
        )}
      </Template>
    );
  }
}

export default withAuth(Page);
