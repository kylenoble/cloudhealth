import React, {Component} from 'react'
import Router from 'next/router'

import withAuth from  '../utils/withAuth.js'
import Header from '../components/Header'
import AuthService from '../utils/authService.js'

const auth = new AuthService()

class Profile extends Component {
  constructor(props) {
    super(props)

    this._logout = this._logout.bind(this)
    this.state = {

    }
  }

  render() {
    return(
      <div>
        <Header />
        <div style={styles.container}>
          <h1>Profile</h1>
          <span style={styles.logoutButton} onClick={this._logout}>Logout</span>
        </div>
      </div>
    )
  }

  _logout (e) {
    e.preventDefault()
    // yay uncontrolled forms!
    auth.logout()
      .then(res => {
        console.log(res)
        Router.push('/login')
      })
      .catch(e => console.log(e))  // you would show/hide error messages with component state here
  }
}

const styles = {
  container: {
    fontFamily: '"Roboto",sans-serif',
    margin: '0 auto',
    maxWidth: '40em',
    lineHeight: '1.5',
    padding: '4em 1em',
  },
  title: {
    display: '-webkit-flex',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  logoutButton: {
    cursor: 'pointer',
  }
}

export default withAuth(Profile)
