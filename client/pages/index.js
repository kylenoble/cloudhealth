import React from "react";
import Link from "next/link";
import "isomorphic-fetch";
// import { css } from "glamor";
import Header from "../components/Header";
import AuthService from "../utils/authService.js";
import AppStatus from "../utils/checkAppStatus.js";
import TwoColumnWrapper from "../components/NewSkin/Wrappers/TwoColumnWrapper";
import Button from "../components/NewSkin/Components/Button";
import ContainedWrapper from "../components/NewSkin/Wrappers/ContainedWrapper";
import CenteredWrapper from "../components/NewSkin/Wrappers/CenteredWrapper";
import { centered, on_mobile, font } from "../components/NewSkin/Styles";
import Text from "../components/NewSkin/Components/Text";
import Colors from "../components/NewSkin/Colors";
import { UserContext } from "../context";
import NewFooter from "../components/NewSkin/Components/NewFooter";
import {
  headerAndFooterHeight,
  checkScreenResolution
} from "../components/NewSkin/Methods";
import Loading from "../components/NewSkin/Components/Loading";

const appStatus = new AppStatus();
const auth = new AuthService();

const MainButton = props => (
  <Link prefetch href={`/${props.url}`}>
    <button>{props.name}</button>
  </Link>
);

export default class extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this._startLoader = this._startLoader.bind(this);
    this.renderMainButton = this.renderMainButton.bind(this);
  }

  static contextType = UserContext;

  state = {
    loading: false,
    user: {},
    lowReso: false,
    defaultPageHeight: 0,
    pageLoaded: false,
    screenSize: "xlarge"
  };

  componentWillMount() {
    this._isMounted = true;
    appStatus.chek_status();
  }

  componentDidMount() {
    this._isMounted = true;

    if (this.supportsFlexBox()) {
      // Modern Flexbox is supported
    } else {
      // Modern Flexbox is not supported
      // eslint-disable-next-line no-undef
      flexibility(document.getElementById("__next"));
    }

    if (this._isMounted) {
      const defaultPageHeight = headerAndFooterHeight();
      document.body.addEventListener("resize", this.resizing());
      document.body.onresize = this.resizing;
      document.body.style = `background: ${Colors.skyblue} !important`;

      const lowReso = checkScreenResolution();
      setTimeout(() => {
        this.setState({ pageLoaded: true, defaultPageHeight });
      }, 700);
      this.setState({ lowReso });
    }

    this.setState({ user: auth.getProfile() });
  }

  resizing = () => {
    const screensize = checkScreenResolution();
    this.setState({ screenSize: screensize });
  };

  componentWillUnmount() {
    document.body.removeEventListener("resize", this.resizing);
  }

  _startLoader() {
    if (this._isMounted) {
      this.setState({ loading: true });
    }
  }

  supportsFlexBox() {
    // eslint-disable-next-line no-undef
    const test = document.createElement("test");

    test.style.display = "flex";

    return test.style.display === "flex";
  }

  _openModal = modalName => {
    this.setState({
      [modalName]: true
    });
  };

  _closeModal = str => {
    if (this._isMounted) {
      this.setState({
        [str]: false
      });
    }
  };

  renderMainButton = () => {
    if (this.state.user.name) {
      return (
        <span
          onClick={() => {
            this._startLoader();
          }}
        >
          <Button classes={[font(1)]} type="pink">
            <MainButton url="dashboard" name="Go To Dashboard" />
          </Button>
        </span>
      );
    }

    return (
      <Button classes={[font(1)]} type="pink">
        <MainButton url="login" name="Sign In" />
      </Button>
    );
  };

  render() {
    const ctx = this.context;

    return (
      <UserContext.Provider
        value={{
          ...ctx,
          user: this.state.user,
          defaultPageHeight: this.state.defaultPageHeight,
          screenSize: this.state.screenSize
        }}
      >
        {this.state.pageLoaded ? null : <Loading />}

        <div>
          <Header
            pathname="index"
            user={this.state.user}
            startLoader={this._startLoader}
          />

          {this.state.loading ? (
            <Loading />
          ) : (
            // <ImageLoader />
            <section>
              <ContainedWrapper
                headerAndFooterHeight={this.state.defaultPageHeight}
                innerPadding={20}
              >
                <TwoColumnWrapper
                  imageOnMobileStyles={{ justifyContent: "center" }}
                  imageAlign={"left"}
                  lowReso={this.state.lowReso}
                  withImage
                  template="1.6fr 1fr"
                  styles={{ lineHeight: 1, gridColumnGap: 100 }}
                >
                  <section
                    data-style="display: flex;"
                    id="main-index"
                    className={centered}
                  >
                    <CenteredWrapper to="left">
                      <Text
                        size={1.4}
                        onMobileSize={1.2}
                        weight={700}
                        color={Colors.blueDarkAccent}
                        lineHeight="1"
                      >
                        <h1>
                          {" "}
                          Your Primary Care Doctor <br />
                          Is Just a Click Away.
                        </h1>
                      </Text>

                      <Text size={1} weight={500} color={Colors.blueDarkAccent}>
                        <p>
                          {" "}
                          With CloudHealth’s Digital Platform, <br />
                          achieving optimal health <br />
                          is just one click away.
                        </p>
                      </Text>

                      {this.renderMainButton()}
                    </CenteredWrapper>
                  </section>
                </TwoColumnWrapper>
              </ContainedWrapper>
              <NewFooter
                state={this.state}
                openModal={this._openModal}
                closeModal={this._closeModal}
              />
            </section>
          )}
        </div>
      </UserContext.Provider>
    );
  }
}
