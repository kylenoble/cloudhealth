import { createContext } from "react";

const UserContext = createContext({
  user: {},
  notifications: [],
  logout: () => {},
  getNotifications: () => {}
});

export { UserContext };
