import { createContext } from "react";

const ProgramContext = createContext({
  programs: {}
});

export { ProgramContext };
