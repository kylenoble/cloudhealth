export default {
  // HI:[53],
  // DTX:[54, 55, 56, 57, 58, 59, 60, 61, 62],
  // MEH : [85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95],
  // EEN : [112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124],
  // HM : [96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111],
  // MTL : [76, 77, 78, 79, 80, 81, 82, 83, 84],
  // OTHER : [125, 126, 127, 128, 129, 130, 131, 132],
  // WD : [63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75],
  HOURSPERSLOT: 1,
  DAYSNOTICE: 0,
  RDNS: [11, 12, 13, 14, 15, 16, 17],
  BDY: [49, 50, 51, 52],
  NUTRI: [18, 19, 24, 25, 26, 27, 28, 29, 134],
  SLP: [30, 31, 32, 33, 34],
  DTX: [54, 55, 56, 57, 58, 59, 60, 61, 62],
  EXRCS: [35, 36, 37, 38, 39],
  STRS: [40, 41, 42, 43, 44, 45, 46, 47],
  HI: [53],
  HG: [133],
  PERSONAL: [1, 4], // this.is the required field
  MEH: [85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95],
  EEN: [112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124],
  HM: [
    96,
    97,
    98,
    99,
    100,
    101,
    102,
    103,
    104,
    105,
    106,
    107,
    108,
    109,
    110,
    111
  ],
  MTL: [76, 77, 78, 79, 80, 81, 82, 83, 84],
  OTHER: [125, 126, 127, 128, 129, 130, 131, 132],
  WD: [63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75],

  /** WEIGHT MANAGEMENT PROGRAM - PREMIUM  */
  WMPP: {
    complete_health_profiling: "Complete Health Profiling",
    follow_up: "Follow-up",
    nutrition_consult: "Nutrition Consult",
    nutrition_follow_up: "Nutrition Follow-up",
    fitness_consult: "Fitness Consult",
    fitness_follow_up: "Fitness Follow-up",
    bca: "BCA",
    envirotox: "EnviroTox",
    food_intolerance_test: "Food Intolerance Test",
    add_ons: {
      nutrition_sensor: "Nutrition Sensor",
      toxo_sensor: "Toxo Sensor",
      weight_sensor: "Weight Sensor",
      stool_analysis: "Comprehensive Stool Analysis and Parasitology"
    }
  },

  /** NUTRITION MANAGEMENT PROGRAM - PREMIUM  */
  NMPP: {
    complete_health_profiling: "Complete Health Profiling",
    follow_up: "Follow-up",
    nutrition_consult: "Nutrition Consult",
    nutrition_follow_up: "Nutrition Follow-up",
    fitness_consult: "Fitness Consult",
    fitness_follow_up: "Fitness Follow-up",
    bca: "BCA",
    organic_acids_test: "GPL Organic Acids Test",
    food_intolerance_test: "Food Intolerance Test",
    add_ons: {
      nutrition_sensor: "Nutrition Sensor",
      toxo_sensor: "Toxo Sensor",
      weight_sensor: "Weight Sensor",
      stool_analysis: "Comprehensive Stool Analysis and Parasitology"
    }
  },

  /** DETOXIFICATION MANAGEMENT PROGRAM - PREMIUM  */
  DMPP: {
    complete_health_profiling: "Complete Health Profiling",
    follow_up: "Follow-up",
    nutrition_consult: "Nutrition Consult",
    nutrition_follow_up: "Nutrition Follow-up",
    envirotox: "EnviroTox",
    add_ons: {
      iv_therapy: "Myers (Detox)",
      hbot: "Hyperbaric Oxygen Therapy(HBOT)",
      gpl_blood_test: "GPL Heavy Metals Blood Test",
      heilen_low_laser: "Heilen Low Laser Light Therapy"
    }
  },

  /** MOVEMENT MANAGEMENT PROGRAM - PREMIUM  */
  MMPP: {
    complete_health_profiling: "Complete Health Profiling",
    follow_up: "Follow-up",
    fitness_consult: "Fitness Consult",
    regular_stability_ball: "Regular Stability Ball",
    stress_recovery_monitor: "Stress Recovery Monitor",
    physical_assessment: "Physical Assessment with Postural Analysis",
    movement_screening: "Functional Movement Screening",
    kinesis_sessions: "Kinesis Sessions",
    bca: "BCA",
    add_ons: {
      athletic_sensor: "Athletic Sensor",
      addon_organic_acids_test: "GPL Organic Acids Test",
      addtl_kinesis: "Additional Kinesis Session",
      premium_stability_ball: "Premium Stability Ball"
    }
  },

  /** SLEEP MANAGEMENT PROGRAM - PREMIUM  */
  SlMPP: {
    complete_health_profiling: "Complete Health Profiling",
    follow_up: "Follow-up",
    sleep_assessment: "Sleep Assessment",
    hormones_comprehensive: "Hormones Comprehensive",
    add_ons: {
      amber_glasses: "Amber Colored Eye Glasses",
      genova_comprehensive: "Genova Comprehensive Melatonin Profile",
      mind_body_medicine_heartmath: "Mind Body Medicine or HeartMath Session"
    }
  },

  /** STRESS MANAGEMENT PROGRAM - PREMIUM  */
  StMPP: {
    complete_health_profiling: "Complete Health Profiling",
    follow_up: "Follow-up",
    mind_body_medicine: "Mind-Body Medicine",
    heartmath_session: "Heart Math Session",
    stress_ball: "Stress Ball",
    add_ons: {
      iv_therapy: "Myers (Detox)",
      hbot: "Hyperbaric Oxygen Therapy(HBOT)",
      gpl_blood_test: "GPL Heavy Metals Blood Test",
      heilen_low_laser: "Heilen Low Laser Light Therapy"
    }
  },

  /** WEIGHT MANAGEMENT PROGRAM - BASIC  */
  WMPB: {
    food_diary_analysis: "Food Diary Analysis",
    fitness_workshop: "Fitness Workshop",
    bca: "BCA",
    health_talk: { optimal_weight: "Optimal Weight Management Program" },
    add_ons: {
      food_detective: "Food Detective",
      nutrition_sensor: "Nutrition Sensor",
      toxo_sensor: "Toxo Sensor",
      weight_sensor: "Weight Sensor",
      stool_analysis: "Comprehensive Stool Analysis and Parasitology"
    }
  },

  /** NUTRITION MANAGEMENT PROGRAM - BASIC  */
  NMPB: {
    health_talk: {
      nutrition: "Nutrition Workshop",
      fitness: "Fitness Workshop"
    },
    food_diary_analysis: "Food Diary Analysis",
    bca: "BCA",
    facility_use: "Facility Use",
    add_ons: {
      addon_food_detective: "Food Detective",
      addon_food_intolerance_test: "Food Intolerance Test",
      nutrition_sensor: "Nutrition Sensor",
      toxo_sensor: "Toxo Sensor",
      weight_sensor: "Weight Sensor",
      stool_analysis: "Comprehensive Stool Analysis and Parasitology"
    }
  },

  /** DETOXIFICATION MANAGEMENT PROGRAM - BASIC  */
  DMPB: {
    health_talk: { detox: "Detox Workshop" },
    add_ons: {
      iv_therapy: "Myers (Detox)",
      hbot: "Hyperbaric Oxygen Therapy(HBOT)",
      gpl_blood_test: "GPL Heavy Metals Blood Test",
      heilen_low_laser: "Heilen Low Laser Light Therapy"
    }
  },

  /** MOVEMENT MANAGEMENT PROGRAM - BASIC  */
  MMPB: {
    bca: "BCA",
    health_talk: { movement: "Movement Workshop" },
    movement_equipment: "Movement Equipment",
    add_ons: {
      athletic_sensor: "Athletic Sensor",
      addon_organic_acids_test: "GPL Organic Acids Test",
      addtl_kinesis: "Additional Kinesis Session",
      premium_stability_ball: "Premium Stability Ball"
    }
  },

  /** SLEEP MANAGEMENT PROGRAM - BASIC  */
  SlMPB: {
    health_talk: { sleep: "Sleep Hygiene Education" },
    add_ons: {
      addon_sleep_assessment: "Sleep Assessment",
      hormones_comprehensive: "Hormones Comprehensive",
      amber_glasses: "Amber Colored Eye Glasses",
      genova_comprehensive: "Genova Comprehensive Melatonin Profile",
      mind_body_medicine_heartmath: "Mind Body Medicine or HeartMath Session"
    }
  },

  /* STRESS MANAGEMENT PROGRAM - BASIC  */
  StMPB: {
    health_talk: { mind_body: "Mind-Body Medicine Workshop" },
    stress_ball: "Stress Ball",
    add_ons: {
      heartmath_session_1: "Heart Math Session (Individual)",
      heartmath_session_10: "Heart Math Session (Group of 10)",
      iv_therapy: "Myers (Detox)",
      hbot: "Hyperbaric Oxygen Therapy(HBOT)",
      gpl_blood_test: "GPL Heavy Metals Blood Test",
      heilen_low_laser: "Heilen Low Laser Light Therapy"
    }
  }
};
