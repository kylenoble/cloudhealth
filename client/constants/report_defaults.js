export default{
  CONSULTS : {
    heartMath: 'HeartMath teaches simple yet powerful emotional restructuring tools and breathing exercises to increase Heart Rate Variability (HRV). Benefits of HearthMath includes: reduced stress, better sleep, clearer mind, enhanced performance, improved communications skill. In the first session, you will be taught tools and techniques to help transform stress. At the end of the session, your baseline HRV will be measured and you will have a 10-15 minute HRV biofeedback practice session.',
    nutritionConsultation : ''
  },
  DIAGNOSTICS : {
    foodIntolerance: 'Food intolerance is not usually life-threatening but can make the sufferer feel extremely unwell. The symptoms to food intolerance – like bloating, weight control issues, itchy skin, or constipation – can be delayed for hours or even days.  This blood test detects food-specific IgG antibodies, which are substances that are responsible for the varied symptoms.  The results can then be used as a guide to modifiying the diet based on elimination of the foods identified as possible intolerances.',
    evaluationOfResult: ''
  },
  ACTIVITIES : {
    companyPantryRaidSession: '',
    cookingDemo: ''
  },
  PROGRAMS: {
    MindBodyMedicineProgram: {
      session1 : 'Sessions 1 (Breathing) - This session discusses the benefits of slow, deep breathing and how to breathe properly.  Different breathing exercises or techniques (pranayama, pranic breathing, etc.) to help facilitate deep sleep are taught to the client.',
      session2 : 'Sessions 2 (Guided Imagery)  - This session discusses mind-body medicine and the benefits of visualization or guided imagery especially for healing.  The clientwill be guided through 1 or 2 visualizations that will help promote restful sleep.',
      session3 : 'Sessions 3 (Mindfulness) - This session discusses what mindfulness is and teaches 2 techniques (body scan & progressive relaxation) that can help the client achieve deep relaxation and good sleep.'
    },
    essentialOptimalWeightManagementProgram: 'Weight is more than just a number: it is affected by several factors such as hormone balance, digestive health including the balance of microorganisms in the gut, and inflammation in the body.  The evaluation in this program focuses on your hormonal balance to help you achieve your optimal weight.  The Essential variant of this program includes the following tests: Food Intolerance Test, Organic Acids Test, and a Comprehensive Hormones Panel (Male) or Comprehensice Hormones Plus Panel (Female).',

    essentialCoreHealthProgram : "Our overall health is determined by several factors such as our lifestyle habits, our environment, and our family medical history.  This program focuses on evaluating these different factors, especially your nutritional status, to determine how healthy you are.  It also aims to identify any imbalances that may be causing disease or will likely cause disease if not addressed, so that appropriate actions can be taken to correct them and true health is achieved. The Essential variant of this program includes a Food Intolerance Test and an Organic Acids Test.",

    essentialGeneticProfilingandIntervention : 'Genes determine your predispositions to developing certain conditions, both good and bad.  The way genes are expressed are influenced by lifestyle habits and the environment.  Gene testing is done in this program in order for you to learn about your genetic profile and find out if you are at risk for certain diseases.  A program is then made so that you can take steps to prevent the development of disease and establish a lifestyle that ensures you remain healthy throughout your life.'

},
EDUCATION: {
  determinantOfHealthSleepManagement: '',
  determinantOfHealthStressManagement: '',
  determinantOfHealthFitnessMovement: '',
  determinantOfHealthDetoxification: '',
  sixDeterminantsOfHealth: '',
  functionalNutritionAndWeightManagement: '',
  determinantOfHealthDetoxification: ''
},
OTHERSERVICES:{
  functionalMovementScreening : "Learn more about your body abilities and limitations through the Functional Movement Screening — a rating and ranking system that captures basic movement patterns, motor control, and competency of fundamental movement patters using seven exercises. The screening emphasizes movement quality over quantity, and to find any movement deficiencies." ,
  bodyCompositionAnalysis: 'Your weight alone is not a complete and accurate assesment of your health or your fitness.  A Body Composition Analysis gives a better picture of your health status by measuring your weight, Body Mass Index (BMI), muscle mass, fat mass, visceral fat rating, and metabolic age. ',
  kinesisSession: ''
}
}
