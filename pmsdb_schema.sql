--
-- PostgreSQL database dump
--

-- Dumped from database version 10.0
-- Dumped by pg_dump version 12.2

-- Started on 2021-07-12 17:57:49

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA public;


--
-- TOC entry 3352 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 702 (class 1247 OID 76858)
-- Name: app_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.app_status AS ENUM (
    'Active',
    'Maintenance',
    'Down'
);


--
-- TOC entry 705 (class 1247 OID 76866)
-- Name: appointment_history_action; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.appointment_history_action AS ENUM (
    'Rescheduled',
    'Reassigned',
    'Cancelled',
    'Lapsed'
);


--
-- TOC entry 961 (class 1247 OID 109721)
-- Name: notification_types; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.notification_types AS ENUM (
    'event',
    'appointment',
    'health_talk',
    'subscription',
    'enrollment'
);


--
-- TOC entry 708 (class 1247 OID 76876)
-- Name: subscription_enrollment_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.subscription_enrollment_status AS ENUM (
    'Approved',
    'Pending',
    'Declined'
);


--
-- TOC entry 711 (class 1247 OID 76884)
-- Name: subscription_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.subscription_status AS ENUM (
    'Ongoing',
    'Completed',
    'Expired',
    'Cancelled',
    'For Renewal'
);


--
-- TOC entry 714 (class 1247 OID 76896)
-- Name: survey_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.survey_status AS ENUM (
    'Active',
    'Locked'
);


--
-- TOC entry 717 (class 1247 OID 76902)
-- Name: ticket_status; Type: TYPE; Schema: public; Owner: -
--

CREATE TYPE public.ticket_status AS ENUM (
    'Open',
    'Pending',
    'Closed',
    'Resolved'
);


--
-- TOC entry 200 (class 1259 OID 76911)
-- Name: Answers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."Answers_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 201 (class 1259 OID 76913)
-- Name: Appointments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."Appointments_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 202 (class 1259 OID 76915)
-- Name: Questions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."Questions_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 203 (class 1259 OID 76917)
-- Name: Users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."Users_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


SET default_tablespace = '';

--
-- TOC entry 204 (class 1259 OID 76919)
-- Name: account_logs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.account_logs (
    id integer NOT NULL,
    user_id integer,
    activity_date date,
    activity_name text,
    description text
);


--
-- TOC entry 205 (class 1259 OID 76925)
-- Name: account_logs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.account_logs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3353 (class 0 OID 0)
-- Dependencies: 205
-- Name: account_logs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.account_logs_id_seq OWNED BY public.account_logs.id;


--
-- TOC entry 206 (class 1259 OID 76927)
-- Name: admin_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.admin_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 207 (class 1259 OID 76929)
-- Name: admin; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.admin (
    id integer DEFAULT nextval('public.admin_id_seq'::regclass) NOT NULL,
    role integer NOT NULL,
    email character(100) NOT NULL,
    password character varying(64) NOT NULL,
    name character(100),
    company_id integer,
    status integer,
    date_created timestamp without time zone,
    date_modified timestamp without time zone,
    date_revoked timestamp without time zone,
    md_company_name character varying(100),
    is_group_head boolean DEFAULT false
);


--
-- TOC entry 208 (class 1259 OID 76934)
-- Name: analysis_and_recomendation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.analysis_and_recomendation (
    id integer NOT NULL,
    user_id integer NOT NULL,
    status integer,
    assimilation text,
    defense_and_repair text,
    energy text,
    biotransformation_and_elimination text,
    communication text,
    transport text,
    structural_integrity text,
    created_date timestamp without time zone,
    modified_date timestamp without time zone,
    health_tracker json,
    health_tracker_date_created timestamp without time zone,
    health_tracker_date_modified timestamp without time zone,
    health_tracker_status integer,
    modifiables jsonb,
    modifiables_date_created timestamp without time zone,
    modifiables_status integer,
    modifiables_date_modified timestamp without time zone,
    symptoms_review jsonb,
    symptoms_review_date_created timestamp without time zone,
    symptoms_review_status integer,
    symptoms_review_date_modified timestamp without time zone
);


--
-- TOC entry 209 (class 1259 OID 76940)
-- Name: analysis_and_recomendation_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.analysis_and_recomendation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3354 (class 0 OID 0)
-- Dependencies: 209
-- Name: analysis_and_recomendation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.analysis_and_recomendation_id_seq OWNED BY public.analysis_and_recomendation.id;


--
-- TOC entry 210 (class 1259 OID 76942)
-- Name: announcements; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.announcements (
    id integer NOT NULL,
    title character varying(100),
    details text,
    audience character varying(100),
    status character varying(50),
    date_created timestamp with time zone,
    created_by integer,
    date_modified timestamp with time zone,
    read_by jsonb
);


--
-- TOC entry 211 (class 1259 OID 76948)
-- Name: announcements_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.announcements_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3355 (class 0 OID 0)
-- Dependencies: 211
-- Name: announcements_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.announcements_id_seq OWNED BY public.announcements.id;


--
-- TOC entry 212 (class 1259 OID 76950)
-- Name: answers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.answers (
    id integer DEFAULT nextval('public."Answers_id_seq"'::regclass) NOT NULL,
    question_id integer NOT NULL,
    user_id integer NOT NULL,
    value text NOT NULL
);


--
-- TOC entry 213 (class 1259 OID 76957)
-- Name: new_answers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.new_answers (
    id integer NOT NULL,
    question_id integer,
    user_id integer,
    value text NOT NULL,
    created_date timestamp with time zone DEFAULT now(),
    count text DEFAULT 0 NOT NULL
);


--
-- TOC entry 214 (class 1259 OID 76965)
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer DEFAULT nextval('public."Users_id_seq"'::regclass) NOT NULL,
    email text NOT NULL,
    password character varying(64) NOT NULL,
    name text NOT NULL,
    company character varying(100),
    created_at timestamp without time zone DEFAULT now(),
    type text DEFAULT 'Patient'::text NOT NULL,
    available boolean DEFAULT true NOT NULL,
    employee_id character varying(32),
    branch text,
    department text,
    avatar text,
    job_grade text,
    high_risk integer,
    status integer,
    date_revoked timestamp without time zone,
    date_modified timestamp without time zone,
    healthsurvey_status public.survey_status,
    is_admin boolean DEFAULT false,
    admin_access_token character varying(100),
    expertise character varying
);


--
-- TOC entry 215 (class 1259 OID 76976)
-- Name: answers_with_company; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.answers_with_company AS
 SELECT a.id,
    a.user_id,
    a.question_id,
    a.value,
    a.created_date,
    a.count,
    ( SELECT u.company
           FROM public.users u
          WHERE (a.user_id = u.id)) AS company
   FROM public.new_answers a;


--
-- TOC entry 216 (class 1259 OID 76980)
-- Name: appointment_history; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.appointment_history (
    id integer NOT NULL,
    appointment_code character varying(200),
    patient_id integer,
    md_id integer,
    concern text,
    appointment_date timestamp with time zone,
    appointment_status integer,
    action public.appointment_history_action,
    history_created_date timestamp with time zone DEFAULT now()
);


--
-- TOC entry 217 (class 1259 OID 76987)
-- Name: appointment_history_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.appointment_history_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3356 (class 0 OID 0)
-- Dependencies: 217
-- Name: appointment_history_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.appointment_history_id_seq OWNED BY public.appointment_history.id;


--
-- TOC entry 218 (class 1259 OID 76989)
-- Name: appointments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.appointments (
    id integer DEFAULT nextval('public."Appointments_id_seq"'::regclass) NOT NULL,
    doctor_id integer NOT NULL,
    patient_id integer NOT NULL,
    room_name character varying(16) NOT NULL,
    active boolean DEFAULT false NOT NULL,
    status integer,
    concern text,
    patient_company_id integer,
    date_requested timestamp with time zone,
    datetime timestamp with time zone,
    date_rescheduled timestamp with time zone,
    is_modified boolean,
    appointment_code character varying(200),
    date_cancelled timestamp with time zone,
    time_extension integer,
    parent_appointment_code character varying
);


--
-- TOC entry 219 (class 1259 OID 76997)
-- Name: certificates; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.certificates (
    id integer NOT NULL,
    user_id integer,
    training_name character varying,
    date_completed timestamp without time zone,
    certification_level character varying,
    verification_status character varying,
    attachment character varying
);


--
-- TOC entry 220 (class 1259 OID 77003)
-- Name: certificates_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.certificates_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3357 (class 0 OID 0)
-- Dependencies: 220
-- Name: certificates_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.certificates_id_seq OWNED BY public.certificates.id;


--
-- TOC entry 221 (class 1259 OID 77005)
-- Name: cmpy_branches_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.cmpy_branches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 222 (class 1259 OID 77007)
-- Name: cmpy_branches; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cmpy_branches (
    cmpy_id integer,
    br_name character varying(255),
    location text,
    contact character varying(50),
    date_added date,
    date_modified date,
    id integer DEFAULT nextval('public.cmpy_branches_id_seq'::regclass) NOT NULL,
    email text
);


--
-- TOC entry 223 (class 1259 OID 77014)
-- Name: cmpy_departments_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.cmpy_departments_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 224 (class 1259 OID 77016)
-- Name: cmpy_departments; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cmpy_departments (
    id integer DEFAULT nextval('public.cmpy_departments_id_seq'::regclass) NOT NULL,
    cmpy_id integer,
    dept_name character varying(100),
    date_added date,
    date_modefied date,
    branch_id integer
);


--
-- TOC entry 225 (class 1259 OID 77020)
-- Name: companies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.companies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 226 (class 1259 OID 77022)
-- Name: companies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.companies (
    id integer DEFAULT nextval('public.companies_id_seq'::regclass) NOT NULL,
    code text,
    company_name character varying(100),
    location text,
    contact character varying(50),
    date_created date,
    date_modified date,
    is_active boolean DEFAULT true,
    telemedicine_enabled boolean DEFAULT false,
    logo bytea
);


--
-- TOC entry 227 (class 1259 OID 77031)
-- Name: companies_seq_id; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.companies_seq_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 228 (class 1259 OID 77033)
-- Name: company_queue; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.company_queue (
    id integer NOT NULL,
    company_id integer NOT NULL,
    user_id integer NOT NULL,
    is_lead boolean DEFAULT false,
    date_created timestamp without time zone
);


--
-- TOC entry 229 (class 1259 OID 77037)
-- Name: company_queue_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.company_queue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3358 (class 0 OID 0)
-- Dependencies: 229
-- Name: company_queue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.company_queue_id_seq OWNED BY public.company_queue.id;


--
-- TOC entry 230 (class 1259 OID 77039)
-- Name: consultations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.consultations (
    id integer NOT NULL,
    employee_id integer,
    doctor_id integer,
    status integer,
    details jsonb,
    consult_date timestamp with time zone,
    appointment_id integer,
    last_updated timestamp with time zone,
    patient_id integer,
    appointment_code character varying(200),
    concern character varying(200),
    callduration text
);


--
-- TOC entry 231 (class 1259 OID 77045)
-- Name: consultations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.consultations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3359 (class 0 OID 0)
-- Dependencies: 231
-- Name: consultations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.consultations_id_seq OWNED BY public.consultations.id;


--
-- TOC entry 232 (class 1259 OID 77047)
-- Name: new_questions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.new_questions (
    id integer NOT NULL,
    name text NOT NULL,
    label text NOT NULL,
    "group" text NOT NULL,
    flag_assimilation boolean DEFAULT false NOT NULL,
    flag_defense_and_repair boolean DEFAULT false NOT NULL,
    flag_energy boolean DEFAULT false NOT NULL,
    flag_biotransformation_and_elimination boolean DEFAULT false NOT NULL,
    flag_communication boolean DEFAULT false NOT NULL,
    flag_transport boolean DEFAULT false NOT NULL,
    flag_structural_integrity boolean DEFAULT false NOT NULL
);


--
-- TOC entry 233 (class 1259 OID 77060)
-- Name: core_imbalances_scores; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.core_imbalances_scores AS
 SELECT a.user_id,
    sum(
        CASE
            WHEN (q.flag_assimilation = true) THEN (NULLIF(a.value, ''::text))::integer
            ELSE 0
        END) AS assimilation,
    sum(
        CASE
            WHEN (q.flag_defense_and_repair = true) THEN (NULLIF(a.value, ''::text))::integer
            ELSE 0
        END) AS defense_and_repair,
    sum(
        CASE
            WHEN (q.flag_energy = true) THEN (NULLIF(a.value, ''::text))::integer
            ELSE 0
        END) AS energy,
    sum(
        CASE
            WHEN (q.flag_biotransformation_and_elimination = true) THEN (NULLIF(a.value, ''::text))::integer
            ELSE 0
        END) AS biotransformation_and_elimination,
    sum(
        CASE
            WHEN (q.flag_communication = true) THEN (NULLIF(a.value, ''::text))::integer
            ELSE 0
        END) AS communication,
    sum(
        CASE
            WHEN (q.flag_transport = true) THEN (NULLIF(a.value, ''::text))::integer
            ELSE 0
        END) AS transport,
    sum(
        CASE
            WHEN (q.flag_structural_integrity = true) THEN (NULLIF(a.value, ''::text))::integer
            ELSE 0
        END) AS structural_integrity
   FROM (public.new_answers a
     LEFT JOIN public.new_questions q ON ((a.question_id = q.id)))
  WHERE ((q.flag_assimilation = true) OR (q.flag_defense_and_repair = true) OR (q.flag_energy = true) OR (q.flag_biotransformation_and_elimination = true) OR (q.flag_communication = true) OR (q.flag_transport = true) OR (q.flag_structural_integrity = true))
  GROUP BY a.user_id;


--
-- TOC entry 234 (class 1259 OID 77065)
-- Name: data_consent; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.data_consent (
    email character varying(100) NOT NULL,
    name character varying(255) NOT NULL,
    company_id integer,
    is_sent boolean DEFAULT false NOT NULL,
    is_agreed character(3),
    date_uploaded timestamp with time zone,
    is_duplicate boolean,
    date_updated timestamp with time zone
);


--
-- TOC entry 235 (class 1259 OID 77069)
-- Name: summary; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.summary (
    id integer NOT NULL,
    name text,
    label text,
    "group" text,
    user_id integer,
    value text,
    created_date timestamp with time zone DEFAULT now()
);


--
-- TOC entry 236 (class 1259 OID 77076)
-- Name: distinct_summary_peruser_pergroup; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.distinct_summary_peruser_pergroup AS
 SELECT DISTINCT ON (summary.user_id, summary.name) summary.user_id,
    summary.name,
    summary.value
   FROM public.summary
  ORDER BY summary.user_id, summary.name, summary.created_date DESC;


--
-- TOC entry 237 (class 1259 OID 77080)
-- Name: events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.events (
    id integer NOT NULL,
    name character varying(255),
    description text,
    venue text,
    start_date date,
    start_time time without time zone,
    end_date date,
    end_time time without time zone,
    status character varying,
    date_created timestamp with time zone,
    date_modified timestamp with time zone,
    health_talk character varying(255),
    audience character varying(100) NOT NULL,
    created_by integer NOT NULL,
    read_by jsonb,
    company_enrollment_id integer
);


--
-- TOC entry 238 (class 1259 OID 77086)
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3360 (class 0 OID 0)
-- Dependencies: 238
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.events_id_seq OWNED BY public.events.id;


--
-- TOC entry 239 (class 1259 OID 77088)
-- Name: filtered_summary_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.filtered_summary_view AS
 SELECT s.user_id,
    u.name AS employee_name,
    u.email,
    u.company,
    u.branch,
    u.department,
    u.job_grade,
    u.high_risk,
    u.employee_id,
    u.healthsurvey_status,
    s.name,
    s.value,
    s.created_date
   FROM (public.summary s
     LEFT JOIN public.users u ON ((u.id = s.user_id)))
  WHERE ((s.id = ( SELECT DISTINCT ON (s2.name) s2.id
           FROM public.summary s2
          WHERE ((s2.user_id = s.user_id) AND (s2.name = s.name))
          ORDER BY s2.name, s2.created_date DESC)) AND (u.status <> 3))
  ORDER BY u.id, s.id, u.company;


--
-- TOC entry 240 (class 1259 OID 77093)
-- Name: group_profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.group_profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 241 (class 1259 OID 77095)
-- Name: group_profiles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.group_profiles (
    id integer DEFAULT nextval('public.group_profiles_id_seq'::regclass) NOT NULL,
    code character varying(10) NOT NULL,
    name character varying(50),
    description text,
    is_active boolean DEFAULT true
);


--
-- TOC entry 242 (class 1259 OID 77103)
-- Name: health_concern; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.health_concern (
    id integer NOT NULL,
    name text,
    date_added timestamp without time zone
);


--
-- TOC entry 243 (class 1259 OID 77109)
-- Name: health_concern_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.health_concern_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3361 (class 0 OID 0)
-- Dependencies: 243
-- Name: health_concern_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.health_concern_id_seq OWNED BY public.health_concern.id;


--
-- TOC entry 244 (class 1259 OID 77111)
-- Name: health_documents; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.health_documents (
    id integer NOT NULL,
    user_id integer,
    filename text,
    date_uploaded timestamp with time zone,
    description text
);


--
-- TOC entry 245 (class 1259 OID 77117)
-- Name: health_documents_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.health_documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3362 (class 0 OID 0)
-- Dependencies: 245
-- Name: health_documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.health_documents_id_seq OWNED BY public.health_documents.id;


--
-- TOC entry 246 (class 1259 OID 77119)
-- Name: subscription_company_enrollment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subscription_company_enrollment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 247 (class 1259 OID 77121)
-- Name: subscription_company_enrollment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscription_company_enrollment (
    id integer DEFAULT nextval('public.subscription_company_enrollment_id_seq'::regclass) NOT NULL,
    company_id integer NOT NULL,
    program_code character varying(50) NOT NULL,
    subscription_start date,
    subscription_end date,
    status public.subscription_status DEFAULT 'Ongoing'::public.subscription_status,
    remarks text,
    date_created timestamp with time zone,
    date_modified timestamp with time zone,
    allowed_number_of_users integer DEFAULT 0 NOT NULL
);


--
-- TOC entry 248 (class 1259 OID 77130)
-- Name: subscription_employee_enrollment_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subscription_employee_enrollment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 249 (class 1259 OID 77132)
-- Name: subscription_employee_enrollment; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscription_employee_enrollment (
    id integer DEFAULT nextval('public.subscription_employee_enrollment_id_seq'::regclass) NOT NULL,
    company_enrollment_id integer NOT NULL,
    user_id integer NOT NULL,
    employee_id character varying,
    is_anonymous boolean DEFAULT false NOT NULL,
    health_status character varying,
    enrollment_status public.subscription_enrollment_status DEFAULT 'Pending'::public.subscription_enrollment_status NOT NULL,
    date_enrolled timestamp with time zone,
    date_approved timestamp with time zone,
    date_declined timestamp with time zone,
    event_is_sent boolean
);


--
-- TOC entry 250 (class 1259 OID 77141)
-- Name: health_events_attendance_statistics; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.health_events_attendance_statistics AS
 SELECT DISTINCT ON (events.health_talk, events.audience, x.enrollment_status) x.company_id,
    events.name,
    events.start_date,
    events.date_created,
    x.program_code,
    x.enrollment_status,
    x.count
   FROM (( SELECT ce.company_id,
            ce.program_code,
            ee.enrollment_status,
            count(*) AS count
           FROM ((public.subscription_employee_enrollment ee
             LEFT JOIN public.subscription_company_enrollment ce ON ((ce.id = ee.company_enrollment_id)))
             LEFT JOIN public.companies ON ((ce.company_id = companies.id)))
          WHERE (companies.is_active = true)
          GROUP BY ce.company_id, ce.program_code, ee.enrollment_status) x
     LEFT JOIN public.events ON ((((x.program_code)::text = (events.health_talk)::text) AND ((events.audience)::integer = x.company_id) AND ((events.audience)::text ~ '^\d+$'::text))))
  WHERE (events.name IS NOT NULL)
  ORDER BY events.health_talk, events.audience, x.enrollment_status;


--
-- TOC entry 251 (class 1259 OID 77146)
-- Name: healthscore_graph_data; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.healthscore_graph_data AS
 SELECT u.company,
    NULLIF(u.branch, ''::text) AS branch,
    NULLIF(u.department, ''::text) AS department,
    NULLIF(u.job_grade, ''::text) AS job_grade,
        CASE
            WHEN (u.high_risk = 1) THEN 'High Value'::text
            WHEN (u.high_risk = 2) THEN 'High Risk'::text
            ELSE 'Others'::text
        END AS high_risk,
    s.user_id,
    u.employee_id,
    u.name,
    u.status,
    s.healthscore,
    s.survey_count,
    u.healthsurvey_status,
    s.weightscore,
    s.nutrition,
    s.sleep,
    s.exercise,
    s.stress,
    s.detox
   FROM (( SELECT t.user_id,
            round(((sum((((((
                CASE
                    WHEN (t.name = 'weightScore'::text) THEN (t.value)::numeric
                    ELSE (0)::numeric
                END +
                CASE
                    WHEN (t.name = 'nutrition'::text) THEN (t.value)::numeric
                    ELSE (0)::numeric
                END) +
                CASE
                    WHEN (t.name = 'sleep'::text) THEN (t.value)::numeric
                    ELSE (0)::numeric
                END) +
                CASE
                    WHEN (t.name = 'exercise'::text) THEN (t.value)::numeric
                    ELSE (0)::numeric
                END) +
                CASE
                    WHEN (t.name = 'stress'::text) THEN (t.value)::numeric
                    ELSE (0)::numeric
                END) +
                CASE
                    WHEN (t.name = 'detox'::text) THEN (t.value)::numeric
                    ELSE (0)::numeric
                END)) * (2)::numeric) / (6)::numeric), 2) AS healthscore,
            round(sum(
                CASE
                    WHEN (t.name = 'weightScore'::text) THEN ((t.value)::numeric * (2)::numeric)
                    ELSE (0)::numeric
                END), 2) AS weightscore,
            round(sum(
                CASE
                    WHEN (t.name = 'nutrition'::text) THEN ((t.value)::numeric * (2)::numeric)
                    ELSE (0)::numeric
                END), 2) AS nutrition,
            round(sum(
                CASE
                    WHEN (t.name = 'sleep'::text) THEN ((t.value)::numeric * (2)::numeric)
                    ELSE (0)::numeric
                END), 2) AS sleep,
            round(sum(
                CASE
                    WHEN (t.name = 'exercise'::text) THEN ((t.value)::numeric * (2)::numeric)
                    ELSE (0)::numeric
                END), 2) AS exercise,
            round(sum(
                CASE
                    WHEN (t.name = 'stress'::text) THEN ((t.value)::numeric * (2)::numeric)
                    ELSE (0)::numeric
                END), 2) AS stress,
            round(sum(
                CASE
                    WHEN (t.name = 'detox'::text) THEN ((t.value)::numeric * (2)::numeric)
                    ELSE (0)::numeric
                END), 2) AS detox,
            count(t.user_id) AS survey_count
           FROM public.distinct_summary_peruser_pergroup t
          WHERE (t.name = ANY (ARRAY['detox'::text, 'exercise'::text, 'nutrition'::text, 'sleep'::text, 'stress'::text, 'weightScore'::text]))
          GROUP BY t.user_id) s
     LEFT JOIN public.users u ON ((s.user_id = u.id)))
  WHERE ((u.status <> 3) OR (u.status IS NULL))
  ORDER BY u.company, NULLIF(u.branch, ''::text), NULLIF(u.department, ''::text), NULLIF(u.job_grade, ''::text), s.user_id;


--
-- TOC entry 294 (class 1259 OID 93269)
-- Name: healthsurvey_summary; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.healthsurvey_summary (
    id integer NOT NULL,
    user_id integer NOT NULL,
    personal_info jsonb NOT NULL,
    readiness jsonb NOT NULL,
    weight jsonb NOT NULL,
    nutrition jsonb NOT NULL,
    sleep jsonb NOT NULL,
    stress jsonb NOT NULL,
    detoxification jsonb NOT NULL,
    movement jsonb NOT NULL,
    symptoms_review jsonb NOT NULL,
    health_issues jsonb NOT NULL,
    health_goals jsonb NOT NULL,
    date_created timestamp with time zone DEFAULT now()
);


--
-- TOC entry 293 (class 1259 OID 93267)
-- Name: healthsurvey_summary_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.healthsurvey_summary_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3363 (class 0 OID 0)
-- Dependencies: 293
-- Name: healthsurvey_summary_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.healthsurvey_summary_id_seq OWNED BY public.healthsurvey_summary.id;


--
-- TOC entry 252 (class 1259 OID 77151)
-- Name: md_admin_tokens; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.md_admin_tokens (
    id integer NOT NULL,
    admin_id integer NOT NULL,
    user_id integer NOT NULL,
    token character varying(100) NOT NULL
);


--
-- TOC entry 253 (class 1259 OID 77154)
-- Name: md_admin_tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.md_admin_tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3364 (class 0 OID 0)
-- Dependencies: 253
-- Name: md_admin_tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.md_admin_tokens_id_seq OWNED BY public.md_admin_tokens.id;


--
-- TOC entry 254 (class 1259 OID 77156)
-- Name: msqscores_persystem; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.msqscores_persystem AS
 SELECT t.user_id,
    u.company,
    u.branch,
    u.department,
    u.job_grade,
    u.high_risk,
    u.healthsurvey_status,
    sum(
        CASE
            WHEN ((t.question_id >= 63) AND (t.question_id <= 69)) THEN t.value
            ELSE 0
        END) AS digestive_score,
    sum(
        CASE
            WHEN ((t.question_id >= 70) AND (t.question_id <= 75)) THEN t.value
            ELSE 0
        END) AS weight_score,
    sum(
        CASE
            WHEN ((t.question_id >= 76) AND (t.question_id <= 80)) THEN t.value
            ELSE 0
        END) AS lungs_score,
    sum(
        CASE
            WHEN ((t.question_id >= 81) AND (t.question_id <= 84)) THEN t.value
            ELSE 0
        END) AS mouth_and_throat_score,
    sum(
        CASE
            WHEN ((t.question_id >= 85) AND (t.question_id <= 88)) THEN t.value
            ELSE 0
        END) AS energy_score,
    sum(
        CASE
            WHEN ((t.question_id >= 89) AND (t.question_id <= 93)) THEN t.value
            ELSE 0
        END) AS joints_and_muscles_score,
    sum(
        CASE
            WHEN ((t.question_id >= 94) AND (t.question_id <= 95)) THEN t.value
            ELSE 0
        END) AS heart_score,
    sum(
        CASE
            WHEN ((t.question_id >= 96) AND (t.question_id <= 99)) THEN t.value
            ELSE 0
        END) AS emotions_score,
    sum(
        CASE
            WHEN ((t.question_id >= 100) AND (t.question_id <= 103)) THEN t.value
            ELSE 0
        END) AS head_score,
    sum(
        CASE
            WHEN ((t.question_id >= 104) AND (t.question_id <= 111)) THEN t.value
            ELSE 0
        END) AS mind_score,
    sum(
        CASE
            WHEN ((t.question_id >= 112) AND (t.question_id <= 115)) THEN t.value
            ELSE 0
        END) AS ears_score,
    sum(
        CASE
            WHEN ((t.question_id >= 116) AND (t.question_id <= 119)) THEN t.value
            ELSE 0
        END) AS eyes_score,
    sum(
        CASE
            WHEN ((t.question_id >= 120) AND (t.question_id <= 124)) THEN t.value
            ELSE 0
        END) AS nose_score,
    sum(
        CASE
            WHEN ((t.question_id >= 125) AND (t.question_id <= 129)) THEN t.value
            ELSE 0
        END) AS skin_score,
    sum(
        CASE
            WHEN ((t.question_id >= 130) AND (t.question_id <= 132)) THEN t.value
            ELSE 0
        END) AS other_score
   FROM (( SELECT DISTINCT ON (new_answers.user_id, new_answers.question_id) new_answers.user_id,
            new_answers.question_id,
            (new_answers.value)::integer AS value
           FROM public.new_answers
          WHERE ((new_answers.question_id >= 63) AND (new_answers.question_id <= 132))
          ORDER BY new_answers.user_id, new_answers.question_id, new_answers.created_date DESC) t
     LEFT JOIN public.users u ON ((t.user_id = u.id)))
  GROUP BY t.user_id, u.company, u.branch, u.department, u.job_grade, u.high_risk, u.healthsurvey_status;


--
-- TOC entry 255 (class 1259 OID 77161)
-- Name: msqsystem_ranks; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.msqsystem_ranks AS
 SELECT u.company,
    u.branch,
    u.department,
    u.job_grade,
    u.high_risk,
    u.healthsurvey_status,
    t.user_id,
        CASE
            WHEN ((t.question_id >= 63) AND (t.question_id <= 69)) THEN 'Digestive'::text
            WHEN ((t.question_id >= 70) AND (t.question_id <= 75)) THEN 'Weight'::text
            WHEN ((t.question_id >= 76) AND (t.question_id <= 80)) THEN 'Lungs'::text
            WHEN ((t.question_id >= 81) AND (t.question_id <= 84)) THEN 'Mouth and Throat'::text
            WHEN ((t.question_id >= 85) AND (t.question_id <= 88)) THEN 'Energy'::text
            WHEN ((t.question_id >= 89) AND (t.question_id <= 93)) THEN 'Joints and Muscles'::text
            WHEN ((t.question_id >= 94) AND (t.question_id <= 95)) THEN 'Heart'::text
            WHEN ((t.question_id >= 96) AND (t.question_id <= 99)) THEN 'Emotions'::text
            WHEN ((t.question_id >= 100) AND (t.question_id <= 103)) THEN 'Head'::text
            WHEN ((t.question_id >= 104) AND (t.question_id <= 111)) THEN 'Mind'::text
            WHEN ((t.question_id >= 112) AND (t.question_id <= 115)) THEN 'Ears'::text
            WHEN ((t.question_id >= 116) AND (t.question_id <= 119)) THEN 'Eyes'::text
            WHEN ((t.question_id >= 120) AND (t.question_id <= 124)) THEN 'Nose'::text
            WHEN ((t.question_id >= 125) AND (t.question_id <= 129)) THEN 'Skin'::text
            WHEN ((t.question_id >= 130) AND (t.question_id <= 132)) THEN 'Other'::text
            ELSE NULL::text
        END AS msq_system,
    sum(t.value) AS total_score
   FROM (( SELECT DISTINCT ON (new_answers.user_id, new_answers.question_id) new_answers.user_id,
            new_answers.question_id,
            (new_answers.value)::integer AS value
           FROM public.new_answers
          WHERE ((new_answers.question_id >= 63) AND (new_answers.question_id <= 132))
          ORDER BY new_answers.user_id, new_answers.question_id, new_answers.created_date DESC) t
     LEFT JOIN public.users u ON ((t.user_id = u.id)))
  GROUP BY u.company, u.branch, u.department, u.job_grade, u.high_risk, u.healthsurvey_status, t.user_id,
        CASE
            WHEN ((t.question_id >= 63) AND (t.question_id <= 69)) THEN 'Digestive'::text
            WHEN ((t.question_id >= 70) AND (t.question_id <= 75)) THEN 'Weight'::text
            WHEN ((t.question_id >= 76) AND (t.question_id <= 80)) THEN 'Lungs'::text
            WHEN ((t.question_id >= 81) AND (t.question_id <= 84)) THEN 'Mouth and Throat'::text
            WHEN ((t.question_id >= 85) AND (t.question_id <= 88)) THEN 'Energy'::text
            WHEN ((t.question_id >= 89) AND (t.question_id <= 93)) THEN 'Joints and Muscles'::text
            WHEN ((t.question_id >= 94) AND (t.question_id <= 95)) THEN 'Heart'::text
            WHEN ((t.question_id >= 96) AND (t.question_id <= 99)) THEN 'Emotions'::text
            WHEN ((t.question_id >= 100) AND (t.question_id <= 103)) THEN 'Head'::text
            WHEN ((t.question_id >= 104) AND (t.question_id <= 111)) THEN 'Mind'::text
            WHEN ((t.question_id >= 112) AND (t.question_id <= 115)) THEN 'Ears'::text
            WHEN ((t.question_id >= 116) AND (t.question_id <= 119)) THEN 'Eyes'::text
            WHEN ((t.question_id >= 120) AND (t.question_id <= 124)) THEN 'Nose'::text
            WHEN ((t.question_id >= 125) AND (t.question_id <= 129)) THEN 'Skin'::text
            WHEN ((t.question_id >= 130) AND (t.question_id <= 132)) THEN 'Other'::text
            ELSE NULL::text
        END
  ORDER BY u.company, u.branch, u.department, u.job_grade, u.high_risk, u.healthsurvey_status, t.user_id,
        CASE
            WHEN ((t.question_id >= 63) AND (t.question_id <= 69)) THEN 'Digestive'::text
            WHEN ((t.question_id >= 70) AND (t.question_id <= 75)) THEN 'Weight'::text
            WHEN ((t.question_id >= 76) AND (t.question_id <= 80)) THEN 'Lungs'::text
            WHEN ((t.question_id >= 81) AND (t.question_id <= 84)) THEN 'Mouth and Throat'::text
            WHEN ((t.question_id >= 85) AND (t.question_id <= 88)) THEN 'Energy'::text
            WHEN ((t.question_id >= 89) AND (t.question_id <= 93)) THEN 'Joints and Muscles'::text
            WHEN ((t.question_id >= 94) AND (t.question_id <= 95)) THEN 'Heart'::text
            WHEN ((t.question_id >= 96) AND (t.question_id <= 99)) THEN 'Emotions'::text
            WHEN ((t.question_id >= 100) AND (t.question_id <= 103)) THEN 'Head'::text
            WHEN ((t.question_id >= 104) AND (t.question_id <= 111)) THEN 'Mind'::text
            WHEN ((t.question_id >= 112) AND (t.question_id <= 115)) THEN 'Ears'::text
            WHEN ((t.question_id >= 116) AND (t.question_id <= 119)) THEN 'Eyes'::text
            WHEN ((t.question_id >= 120) AND (t.question_id <= 124)) THEN 'Nose'::text
            WHEN ((t.question_id >= 125) AND (t.question_id <= 129)) THEN 'Skin'::text
            WHEN ((t.question_id >= 130) AND (t.question_id <= 132)) THEN 'Other'::text
            ELSE NULL::text
        END;


--
-- TOC entry 256 (class 1259 OID 77166)
-- Name: new_answers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.new_answers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3365 (class 0 OID 0)
-- Dependencies: 256
-- Name: new_answers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.new_answers_id_seq OWNED BY public.new_answers.id;


--
-- TOC entry 257 (class 1259 OID 77168)
-- Name: user_age_group; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.user_age_group AS
 SELECT DISTINCT ON (new_answers.user_id) new_answers.user_id,
    date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) AS age,
        CASE
            WHEN (date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) <= (9)::double precision) THEN '0 - 9'::text
            ELSE
            CASE
                WHEN ((date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) >= (10)::double precision) AND (date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) <= (19)::double precision)) THEN '10 - 19'::text
                ELSE
                CASE
                    WHEN ((date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) >= (20)::double precision) AND (date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) <= (29)::double precision)) THEN '20 - 29'::text
                    ELSE
                    CASE
                        WHEN ((date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) >= (30)::double precision) AND (date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) <= (39)::double precision)) THEN '30 - 39'::text
                        ELSE
                        CASE
                            WHEN ((date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) >= (40)::double precision) AND (date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) <= (49)::double precision)) THEN '40 - 49'::text
                            ELSE
                            CASE
                                WHEN ((date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) >= (50)::double precision) AND (date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) <= (59)::double precision)) THEN '50 - 59'::text
                                ELSE
                                CASE
                                    WHEN ((date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) >= (60)::double precision) AND (date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) <= (69)::double precision)) THEN '60 - 69'::text
                                    ELSE
                                    CASE
WHEN ((date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) >= (70)::double precision) AND (date_part('year'::text, age(((new_answers.value)::date)::timestamp with time zone)) <= (79)::double precision)) THEN '70 - 79'::text
ELSE NULL::text
                                    END
                                END
                            END
                        END
                    END
                END
            END
        END AS age_group
   FROM public.new_answers
  WHERE (new_answers.question_id = 1)
  ORDER BY new_answers.user_id, new_answers.created_date DESC;


--
-- TOC entry 258 (class 1259 OID 77173)
-- Name: new_answers_with_details; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.new_answers_with_details AS
 SELECT a.id,
    a.user_id,
    a.question_id,
    a.value,
    a.created_date,
    a.count,
    u.email,
    u.name,
    u.type,
    u.company,
    u.branch,
    u.department,
    u.job_grade,
    u.high_risk,
    u.employee_id,
    user_age_group.age_group,
    u.healthsurvey_status
   FROM ((public.new_answers a
     LEFT JOIN public.users u ON ((u.id = a.user_id)))
     LEFT JOIN public.user_age_group ON ((a.user_id = user_age_group.user_id)))
  ORDER BY a.user_id;


--
-- TOC entry 259 (class 1259 OID 77178)
-- Name: new_questions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.new_questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3366 (class 0 OID 0)
-- Dependencies: 259
-- Name: new_questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.new_questions_id_seq OWNED BY public.new_questions.id;


--
-- TOC entry 260 (class 1259 OID 77180)
-- Name: notifications; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.notifications (
    id integer NOT NULL,
    sender json,
    recipient json,
    subject text,
    message text,
    date_created timestamp with time zone,
    is_read boolean DEFAULT false,
    date_read timestamp with time zone,
    status integer,
    read_by jsonb,
    type public.notification_types DEFAULT 'appointment'::public.notification_types
);


--
-- TOC entry 261 (class 1259 OID 77187)
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3367 (class 0 OID 0)
-- Dependencies: 261
-- Name: notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.notifications_id_seq OWNED BY public.notifications.id;


--
-- TOC entry 262 (class 1259 OID 77189)
-- Name: other_health_info; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.other_health_info (
    id integer NOT NULL,
    user_id integer,
    doctor_name character varying(200),
    specialization character varying(200),
    hospital_clinic text,
    activity_name character varying(200),
    place text,
    frequency character varying(200),
    tag integer,
    created_date timestamp with time zone,
    modified_date timestamp with time zone
);


--
-- TOC entry 263 (class 1259 OID 77195)
-- Name: other_health_info_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.other_health_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3368 (class 0 OID 0)
-- Dependencies: 263
-- Name: other_health_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.other_health_info_id_seq OWNED BY public.other_health_info.id;


--
-- TOC entry 264 (class 1259 OID 77197)
-- Name: password_monitoring_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.password_monitoring_id_seq
    START WITH 18
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 265 (class 1259 OID 77199)
-- Name: password_monitoring; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.password_monitoring (
    id integer DEFAULT nextval('public.password_monitoring_id_seq'::regclass) NOT NULL,
    user_id integer,
    date_modified date,
    action_taken text,
    status integer
);


--
-- TOC entry 266 (class 1259 OID 77206)
-- Name: user_surveystatus_pergroup; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.user_surveystatus_pergroup AS
 SELECT s.user_id,
    s."group",
        CASE
            WHEN ((( SELECT
                    CASE
                        WHEN (s."group" = 'nutrition'::text) THEN (count(*) - 1)
                        ELSE count(*)
                    END AS count
               FROM public.new_questions qc
              WHERE (qc."group" = s."group")) - count(*)) > 0) THEN 'Incomplete'::text
            ELSE 'Complete'::text
        END AS survey_status
   FROM ( SELECT DISTINCT ON (a.user_id, q.name) a.user_id,
            q.name,
            q."group"
           FROM (public.new_answers a
             LEFT JOIN public.new_questions q ON ((a.question_id = q.id)))
          WHERE (q.label <> 'Food Prep'::text)
          ORDER BY a.user_id, q.name, a.created_date DESC) s
  GROUP BY s.user_id, s."group"
  ORDER BY s.user_id, s."group";


--
-- TOC entry 267 (class 1259 OID 77211)
-- Name: patient_compliance_view; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.patient_compliance_view AS
 SELECT u.name,
    u.employee_id,
    u.company,
    u.branch,
    u.department,
    u.job_grade,
    u.high_risk,
    max(
        CASE
            WHEN (s."group" = 'health'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS health_issues,
    max(
        CASE
            WHEN (s."group" = 'Health Goals'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS health_goals,
    max(
        CASE
            WHEN (s."group" = 'readiness'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS readiness,
    max(
        CASE
            WHEN (s."group" = 'body'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS weight,
    max(
        CASE
            WHEN (s."group" = 'intake'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS detox,
    max(
        CASE
            WHEN (s."group" = 'movementAndExercise'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS movement,
    max(
        CASE
            WHEN (s."group" = 'nutrition'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS nutrition,
    max(
        CASE
            WHEN (s."group" = 'sleep'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS sleep,
    max(
        CASE
            WHEN (s."group" = 'stress'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS stress,
    max(
        CASE
            WHEN (s."group" = 'Activity'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS activity,
    max(
        CASE
            WHEN (s."group" = 'Eyes, Ears, Nose'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS eyes_ears_nose,
    max(
        CASE
            WHEN (s."group" = 'Head and Mind'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS head_and_mind,
    max(
        CASE
            WHEN (s."group" = 'Mouth, Throat, Lungs'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS mouth_throat_lungs,
    max(
        CASE
            WHEN (s."group" = 'Other'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS other,
    max(
        CASE
            WHEN (s."group" = 'Weight and Digestion'::text) THEN s.survey_status
            ELSE NULL::text
        END) AS weight_and_digestion
   FROM (public.user_surveystatus_pergroup s
     LEFT JOIN public.users u ON ((s.user_id = u.id)))
  GROUP BY u.name, u.employee_id, u.company, u.branch, u.department, u.job_grade, u.high_risk
  ORDER BY u.name, u.branch, u.department;


--
-- TOC entry 268 (class 1259 OID 77216)
-- Name: patient_tracker; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.patient_tracker (
    consult_date date,
    patient_id integer,
    appointment_id integer,
    md_incharge text,
    health_concern text,
    imbalance_id text,
    health_plan text,
    health_outcome text,
    next_step text,
    id integer NOT NULL,
    md_id integer
);


--
-- TOC entry 269 (class 1259 OID 77222)
-- Name: prevalent_symptoms; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.prevalent_symptoms AS
 SELECT DISTINCT ON (a.user_id, a.question_id) a.user_id,
    q.label,
    a.value
   FROM (public.new_answers a
     LEFT JOIN public.new_questions q ON ((q.id = a.question_id)))
  WHERE (q."group" = ANY (ARRAY['Activity'::text, 'Eyes, Ears, Nose'::text, 'Head and Mind'::text, 'Mouth, Throat, Lungs'::text, 'Other'::text, 'Weight and Digestion'::text]))
  ORDER BY a.user_id, a.question_id, a.created_date DESC;


--
-- TOC entry 270 (class 1259 OID 77227)
-- Name: questions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.questions (
    id integer DEFAULT nextval('public."Questions_id_seq"'::regclass) NOT NULL,
    name character varying(50) NOT NULL,
    label character varying(100) NOT NULL,
    "group" character varying(40) NOT NULL
);


--
-- TOC entry 271 (class 1259 OID 77231)
-- Name: readiness_graph_data; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.readiness_graph_data AS
 SELECT t.user_id,
    sum(t.age) AS age,
    sum(t.value) AS total_per_user
   FROM ( SELECT DISTINCT ON (a.user_id, a.question_id) a.user_id,
            a.question_id,
                CASE
                    WHEN (a.value ~ '^\d+$'::text) THEN (a.value)::integer
                    ELSE 0
                END AS value,
                CASE
                    WHEN (a.question_id = 1) THEN date_part('year'::text, age(((a.value)::date)::timestamp with time zone))
                    ELSE NULL::double precision
                END AS age
           FROM public.new_answers a
          WHERE (((a.question_id >= 11) AND (a.question_id <= 17)) OR (a.question_id = 1))
          ORDER BY a.user_id, a.question_id, a.created_date DESC) t
  GROUP BY t.user_id
  ORDER BY t.user_id;


--
-- TOC entry 272 (class 1259 OID 77236)
-- Name: reports; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.reports (
    id integer NOT NULL,
    date_created timestamp with time zone,
    insights text,
    md_incharge text,
    md_id integer,
    details json,
    status integer,
    date_modified timestamp with time zone,
    company_id integer,
    company_name text,
    location text,
    file_name text,
    score text,
    percentile_rank text,
    registered_employee text,
    health_status text,
    compliance_rate text,
    programs json,
    mean_scores json,
    scoreandpercentile json,
    report_name text,
    company_subscription_id integer
);


--
-- TOC entry 273 (class 1259 OID 77242)
-- Name: reports_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3369 (class 0 OID 0)
-- Dependencies: 273
-- Name: reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.reports_id_seq OWNED BY public.reports.id;


--
-- TOC entry 274 (class 1259 OID 77244)
-- Name: schedules; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schedules (
    id integer NOT NULL,
    user_id integer,
    start_datetime timestamp with time zone,
    end_datetime timestamp with time zone,
    title text,
    status integer,
    modified_date timestamp with time zone,
    created_date timestamp with time zone
);


--
-- TOC entry 275 (class 1259 OID 77250)
-- Name: schedules_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.schedules_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3370 (class 0 OID 0)
-- Dependencies: 275
-- Name: schedules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.schedules_id_seq OWNED BY public.schedules.id;


--
-- TOC entry 276 (class 1259 OID 77252)
-- Name: subscription_programs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.subscription_programs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 277 (class 1259 OID 77254)
-- Name: subscription_programs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.subscription_programs (
    id integer DEFAULT nextval('public.subscription_programs_id_seq'::regclass) NOT NULL,
    subscription_name character varying(100) NOT NULL,
    shortcode character varying(50) NOT NULL,
    price_per_plan money,
    remarks text,
    is_active boolean DEFAULT true NOT NULL,
    date_created timestamp without time zone,
    date_modified timestamp without time zone,
    inclusions json,
    is_basic boolean
);


--
-- TOC entry 278 (class 1259 OID 77262)
-- Name: summary_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.summary_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3371 (class 0 OID 0)
-- Dependencies: 278
-- Name: summary_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.summary_id_seq OWNED BY public.summary.id;


--
-- TOC entry 279 (class 1259 OID 77264)
-- Name: system_info; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system_info (
    id integer NOT NULL,
    app_code text,
    app_name character varying(200),
    app_version character varying(200),
    app_desc text,
    app_status public.app_status
);


--
-- TOC entry 280 (class 1259 OID 77270)
-- Name: system_info_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.system_info_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3372 (class 0 OID 0)
-- Dependencies: 280
-- Name: system_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.system_info_id_seq OWNED BY public.system_info.id;


--
-- TOC entry 281 (class 1259 OID 77272)
-- Name: ticket_replies; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ticket_replies (
    id integer NOT NULL,
    ticket_number character varying(255) NOT NULL,
    reply_message text,
    "repliedBy_userid" integer,
    datetime_reply timestamp with time zone
);


--
-- TOC entry 282 (class 1259 OID 77278)
-- Name: ticket_replies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.ticket_replies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3373 (class 0 OID 0)
-- Dependencies: 282
-- Name: ticket_replies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.ticket_replies_id_seq OWNED BY public.ticket_replies.id;


--
-- TOC entry 283 (class 1259 OID 77280)
-- Name: tickets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.tickets (
    id integer NOT NULL,
    ticket_number character varying(50) NOT NULL,
    fullname character varying(255),
    email_address character varying(255) NOT NULL,
    concern character varying(255) NOT NULL,
    subject character varying(255),
    message text,
    attachment_filename text,
    status public.ticket_status DEFAULT 'Open'::public.ticket_status,
    company_name character varying,
    date_created timestamp with time zone
);


--
-- TOC entry 284 (class 1259 OID 77287)
-- Name: tickets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.tickets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3374 (class 0 OID 0)
-- Dependencies: 284
-- Name: tickets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.tickets_id_seq OWNED BY public.tickets.id;


--
-- TOC entry 285 (class 1259 OID 77289)
-- Name: timeline; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.timeline (
    id integer NOT NULL,
    event_date character varying(100),
    event_name character varying(250),
    description text,
    user_id integer,
    tag integer,
    dfpe integer,
    comment text
);


--
-- TOC entry 286 (class 1259 OID 77295)
-- Name: timeline_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.timeline_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3375 (class 0 OID 0)
-- Dependencies: 286
-- Name: timeline_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.timeline_id_seq OWNED BY public.timeline.id;


--
-- TOC entry 287 (class 1259 OID 77297)
-- Name: upstream_manifestations; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.upstream_manifestations AS
 SELECT u.company,
    u.branch,
    u.department,
    u.job_grade,
    u.high_risk,
    u.healthsurvey_status,
    v.user_id,
    v.height,
    v.weight,
    v.bmi,
    v.waist,
    v.hip,
    v.whr,
    v.averagesleep,
    v.overallstressscore,
    v.tobaccoexposure,
    v.caffeineexposure,
    v.chemicallydependent,
    v.exercise150min,
    v.usesleepingaids,
    v.stress_counseling,
    v.stress_therapy
   FROM (( SELECT t.user_id,
            sum(
                CASE
                    WHEN (t.name = 'height'::text) THEN COALESCE((NULLIF(regexp_replace(t.value, '[^-0-9.]+'::text, ''::text, 'g'::text), ''::text))::numeric, (0)::numeric)
                    ELSE NULL::numeric
                END) AS height,
            sum(
                CASE
                    WHEN (t.name = 'weight'::text) THEN COALESCE((NULLIF(regexp_replace(t.value, '[^-0-9.]+'::text, ''::text, 'g'::text), ''::text))::numeric, (0)::numeric)
                    ELSE NULL::numeric
                END) AS weight,
            sum(
                CASE
                    WHEN (t.name = 'bmi'::text) THEN COALESCE((t.value)::double precision, (0)::double precision)
                    ELSE NULL::double precision
                END) AS bmi,
            sum(
                CASE
                    WHEN (t.name = 'waist'::text) THEN COALESCE((NULLIF(regexp_replace(t.value, '[^-0-9.]+'::text, ''::text, 'g'::text), ''::text))::numeric, (0)::numeric)
                    ELSE NULL::numeric
                END) AS waist,
            sum(
                CASE
                    WHEN (t.name = 'hip'::text) THEN COALESCE((NULLIF(regexp_replace(t.value, '[^-0-9.]+'::text, ''::text, 'g'::text), ''::text))::numeric, (0)::numeric)
                    ELSE NULL::numeric
                END) AS hip,
            sum(
                CASE
                    WHEN (t.name = 'whr'::text) THEN COALESCE((t.value)::double precision, (0)::double precision)
                    ELSE NULL::double precision
                END) AS whr,
            max(
                CASE
                    WHEN (t.name = 'averageSleep'::text) THEN t.value
                    ELSE NULL::text
                END) AS averagesleep,
            sum(
                CASE
                    WHEN (t.name = 'stress'::text) THEN (t.value)::numeric
                    ELSE NULL::numeric
                END) AS overallstressscore,
            max(
                CASE
                    WHEN (t.name = 'tobacco'::text) THEN t.value
                    ELSE NULL::text
                END) AS tobaccoexposure,
            max(
                CASE
                    WHEN (t.name = 'caffeine'::text) THEN t.value
                    ELSE NULL::text
                END) AS caffeineexposure,
            max(
                CASE
                    WHEN (t.name = 'dependent'::text) THEN t.value
                    ELSE NULL::text
                END) AS chemicallydependent,
            max(
                CASE
                    WHEN (t.name = 'weeklyExercise'::text) THEN t.value
                    ELSE NULL::text
                END) AS exercise150min,
            max(
                CASE
                    WHEN (t.name = 'sleepingAids'::text) THEN t.value
                    ELSE NULL::text
                END) AS usesleepingaids,
            max(
                CASE
                    WHEN (t.name = 'counseling'::text) THEN t.value
                    ELSE NULL::text
                END) AS stress_counseling,
            max(
                CASE
                    WHEN (t.name = 'therapy'::text) THEN t.value
                    ELSE NULL::text
                END) AS stress_therapy
           FROM ( SELECT DISTINCT ON (a.user_id, q.name) a.user_id,
                    q.name,
                    a.value,
                    a.created_date
                   FROM (public.new_answers a
                     LEFT JOIN public.new_questions q ON ((a.question_id = q.id)))
                  WHERE (q.name = ANY (ARRAY['height'::text, 'weight'::text, 'waist'::text, 'hip'::text, 'averageSleep'::text, 'tobacco'::text, 'caffeine'::text, 'dependent'::text, 'weeklyExercise'::text, 'sleepingAids'::text, 'counseling'::text, 'therapy'::text]))
                UNION ALL
                 SELECT DISTINCT ON (s.user_id, s.name) s.user_id,
                    s.name,
                    s.value,
                    s.created_date
                   FROM public.summary s
                  WHERE (s.name = ANY (ARRAY['bmi'::text, 'whr'::text, 'stress'::text]))
          ORDER BY 1, 2, 4 DESC) t
          GROUP BY t.user_id) v
     LEFT JOIN public.users u ON ((u.id = v.user_id)));


--
-- TOC entry 288 (class 1259 OID 77302)
-- Name: upstream_stress; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.upstream_stress AS
 SELECT u.company,
    u.branch,
    u.department,
    u.job_grade,
    u.high_risk,
    u.healthsurvey_status,
    t.user_id,
    t.label,
    (t.value)::numeric AS total_score
   FROM (( SELECT DISTINCT ON (a.user_id, q.id) q.id,
            q.label,
            q.name,
            a.user_id,
            a.value,
            a.created_date
           FROM (public.new_answers a
             LEFT JOIN public.new_questions q ON ((a.question_id = q.id)))
          WHERE (q.name = ANY (ARRAY['workStress'::text, 'familyStress'::text, 'socialStress'::text, 'financialStress'::text, 'healthStress'::text, 'otherStress'::text]))
          ORDER BY a.user_id, q.id, q.label, a.value, a.created_date DESC) t
     LEFT JOIN public.users u ON ((t.user_id = u.id)))
  ORDER BY (t.value)::numeric DESC;


--
-- TOC entry 289 (class 1259 OID 77307)
-- Name: user_activities; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_activities (
    id integer NOT NULL,
    user_id integer,
    action character varying(50) NOT NULL,
    details jsonb,
    created_date timestamp without time zone DEFAULT now() NOT NULL
);


--
-- TOC entry 290 (class 1259 OID 77314)
-- Name: user_activities_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_activities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3376 (class 0 OID 0)
-- Dependencies: 290
-- Name: user_activities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_activities_id_seq OWNED BY public.user_activities.id;


--
-- TOC entry 291 (class 1259 OID 77316)
-- Name: user_program_intervention; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_program_intervention (
    id integer NOT NULL,
    employee_enrollment_id integer,
    program_code character varying,
    is_basic boolean,
    details jsonb
);


--
-- TOC entry 292 (class 1259 OID 77322)
-- Name: user_program_intervention_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_program_intervention_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3377 (class 0 OID 0)
-- Dependencies: 292
-- Name: user_program_intervention_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_program_intervention_id_seq OWNED BY public.user_program_intervention.id;


--
-- TOC entry 3045 (class 2604 OID 77324)
-- Name: account_logs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_logs ALTER COLUMN id SET DEFAULT nextval('public.account_logs_id_seq'::regclass);


--
-- TOC entry 3048 (class 2604 OID 77325)
-- Name: analysis_and_recomendation id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.analysis_and_recomendation ALTER COLUMN id SET DEFAULT nextval('public.analysis_and_recomendation_id_seq'::regclass);


--
-- TOC entry 3049 (class 2604 OID 77326)
-- Name: announcements id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.announcements ALTER COLUMN id SET DEFAULT nextval('public.announcements_id_seq'::regclass);


--
-- TOC entry 3060 (class 2604 OID 77327)
-- Name: appointment_history id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.appointment_history ALTER COLUMN id SET DEFAULT nextval('public.appointment_history_id_seq'::regclass);


--
-- TOC entry 3063 (class 2604 OID 77328)
-- Name: certificates id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.certificates ALTER COLUMN id SET DEFAULT nextval('public.certificates_id_seq'::regclass);


--
-- TOC entry 3070 (class 2604 OID 77329)
-- Name: company_queue id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.company_queue ALTER COLUMN id SET DEFAULT nextval('public.company_queue_id_seq'::regclass);


--
-- TOC entry 3071 (class 2604 OID 77330)
-- Name: consultations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.consultations ALTER COLUMN id SET DEFAULT nextval('public.consultations_id_seq'::regclass);


--
-- TOC entry 3083 (class 2604 OID 77331)
-- Name: events id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.events ALTER COLUMN id SET DEFAULT nextval('public.events_id_seq'::regclass);


--
-- TOC entry 3086 (class 2604 OID 77332)
-- Name: health_concern id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.health_concern ALTER COLUMN id SET DEFAULT nextval('public.health_concern_id_seq'::regclass);


--
-- TOC entry 3087 (class 2604 OID 77333)
-- Name: health_documents id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.health_documents ALTER COLUMN id SET DEFAULT nextval('public.health_documents_id_seq'::regclass);


--
-- TOC entry 3113 (class 2604 OID 93272)
-- Name: healthsurvey_summary id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.healthsurvey_summary ALTER COLUMN id SET DEFAULT nextval('public.healthsurvey_summary_id_seq'::regclass);


--
-- TOC entry 3094 (class 2604 OID 77334)
-- Name: md_admin_tokens id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.md_admin_tokens ALTER COLUMN id SET DEFAULT nextval('public.md_admin_tokens_id_seq'::regclass);


--
-- TOC entry 3053 (class 2604 OID 77335)
-- Name: new_answers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.new_answers ALTER COLUMN id SET DEFAULT nextval('public.new_answers_id_seq'::regclass);


--
-- TOC entry 3079 (class 2604 OID 77336)
-- Name: new_questions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.new_questions ALTER COLUMN id SET DEFAULT nextval('public.new_questions_id_seq'::regclass);


--
-- TOC entry 3096 (class 2604 OID 77337)
-- Name: notifications id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notifications ALTER COLUMN id SET DEFAULT nextval('public.notifications_id_seq'::regclass);


--
-- TOC entry 3098 (class 2604 OID 77338)
-- Name: other_health_info id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.other_health_info ALTER COLUMN id SET DEFAULT nextval('public.other_health_info_id_seq'::regclass);


--
-- TOC entry 3101 (class 2604 OID 77339)
-- Name: reports id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reports ALTER COLUMN id SET DEFAULT nextval('public.reports_id_seq'::regclass);


--
-- TOC entry 3102 (class 2604 OID 77340)
-- Name: schedules id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schedules ALTER COLUMN id SET DEFAULT nextval('public.schedules_id_seq'::regclass);


--
-- TOC entry 3082 (class 2604 OID 77341)
-- Name: summary id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.summary ALTER COLUMN id SET DEFAULT nextval('public.summary_id_seq'::regclass);


--
-- TOC entry 3105 (class 2604 OID 77342)
-- Name: system_info id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_info ALTER COLUMN id SET DEFAULT nextval('public.system_info_id_seq'::regclass);


--
-- TOC entry 3106 (class 2604 OID 77343)
-- Name: ticket_replies id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ticket_replies ALTER COLUMN id SET DEFAULT nextval('public.ticket_replies_id_seq'::regclass);


--
-- TOC entry 3108 (class 2604 OID 77344)
-- Name: tickets id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tickets ALTER COLUMN id SET DEFAULT nextval('public.tickets_id_seq'::regclass);


--
-- TOC entry 3109 (class 2604 OID 77345)
-- Name: timeline id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.timeline ALTER COLUMN id SET DEFAULT nextval('public.timeline_id_seq'::regclass);


--
-- TOC entry 3111 (class 2604 OID 77346)
-- Name: user_activities id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_activities ALTER COLUMN id SET DEFAULT nextval('public.user_activities_id_seq'::regclass);


--
-- TOC entry 3112 (class 2604 OID 77347)
-- Name: user_program_intervention id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_program_intervention ALTER COLUMN id SET DEFAULT nextval('public.user_program_intervention_id_seq'::regclass);


--
-- TOC entry 3116 (class 2606 OID 77383)
-- Name: account_logs account_logs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_logs
    ADD CONSTRAINT account_logs_pkey PRIMARY KEY (id);


--
-- TOC entry 3118 (class 2606 OID 77385)
-- Name: admin admin_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.admin
    ADD CONSTRAINT admin_pkey PRIMARY KEY (id);


--
-- TOC entry 3120 (class 2606 OID 77387)
-- Name: analysis_and_recomendation analysis_and_recomendation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.analysis_and_recomendation
    ADD CONSTRAINT analysis_and_recomendation_pkey PRIMARY KEY (id);


--
-- TOC entry 3122 (class 2606 OID 77389)
-- Name: announcements announcements_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.announcements
    ADD CONSTRAINT announcements_pkey PRIMARY KEY (id);


--
-- TOC entry 3124 (class 2606 OID 77391)
-- Name: answers answers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.answers
    ADD CONSTRAINT answers_pkey PRIMARY KEY (id);


--
-- TOC entry 3131 (class 2606 OID 77393)
-- Name: appointment_history appointment_history_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.appointment_history
    ADD CONSTRAINT appointment_history_pkey PRIMARY KEY (id);


--
-- TOC entry 3134 (class 2606 OID 77395)
-- Name: appointments appointments_appointment_code_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.appointments
    ADD CONSTRAINT appointments_appointment_code_key UNIQUE (appointment_code);


--
-- TOC entry 3136 (class 2606 OID 77397)
-- Name: appointments appointments_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.appointments
    ADD CONSTRAINT appointments_pkey PRIMARY KEY (id);


--
-- TOC entry 3138 (class 2606 OID 77399)
-- Name: cmpy_branches cmpy_branches_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cmpy_branches
    ADD CONSTRAINT cmpy_branches_pkey PRIMARY KEY (id);


--
-- TOC entry 3140 (class 2606 OID 77401)
-- Name: cmpy_departments cmpy_dept_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cmpy_departments
    ADD CONSTRAINT cmpy_dept_pkey PRIMARY KEY (id);


--
-- TOC entry 3142 (class 2606 OID 77403)
-- Name: companies companies_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT companies_pkey PRIMARY KEY (id);


--
-- TOC entry 3144 (class 2606 OID 77405)
-- Name: companies companies_uqkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.companies
    ADD CONSTRAINT companies_uqkey UNIQUE (code);


--
-- TOC entry 3146 (class 2606 OID 77407)
-- Name: company_queue company_queue_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.company_queue
    ADD CONSTRAINT company_queue_pkey PRIMARY KEY (id);


--
-- TOC entry 3150 (class 2606 OID 77409)
-- Name: data_consent data_consent_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.data_consent
    ADD CONSTRAINT data_consent_pkey PRIMARY KEY (email);


--
-- TOC entry 3155 (class 2606 OID 77411)
-- Name: events events_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- TOC entry 3157 (class 2606 OID 77413)
-- Name: group_profiles group_profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_profiles
    ADD CONSTRAINT group_profiles_pkey PRIMARY KEY (id);


--
-- TOC entry 3159 (class 2606 OID 77415)
-- Name: group_profiles group_profiles_ukey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.group_profiles
    ADD CONSTRAINT group_profiles_ukey UNIQUE (code);


--
-- TOC entry 3187 (class 2606 OID 93278)
-- Name: healthsurvey_summary healthsurvey_summary_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.healthsurvey_summary
    ADD CONSTRAINT healthsurvey_summary_pkey PRIMARY KEY (id);


--
-- TOC entry 3165 (class 2606 OID 77417)
-- Name: md_admin_tokens md_admin_tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.md_admin_tokens
    ADD CONSTRAINT md_admin_tokens_pkey PRIMARY KEY (id);


--
-- TOC entry 3167 (class 2606 OID 77419)
-- Name: md_admin_tokens md_admin_tokens_token_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.md_admin_tokens
    ADD CONSTRAINT md_admin_tokens_token_key UNIQUE (token);


--
-- TOC entry 3127 (class 2606 OID 77421)
-- Name: new_answers new_answers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.new_answers
    ADD CONSTRAINT new_answers_pkey PRIMARY KEY (id);


--
-- TOC entry 3148 (class 2606 OID 77423)
-- Name: new_questions new_questions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.new_questions
    ADD CONSTRAINT new_questions_pkey PRIMARY KEY (id);


--
-- TOC entry 3169 (class 2606 OID 77425)
-- Name: notifications notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- TOC entry 3171 (class 2606 OID 77427)
-- Name: questions questions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.questions
    ADD CONSTRAINT questions_pkey PRIMARY KEY (id);


--
-- TOC entry 3173 (class 2606 OID 77429)
-- Name: reports reports_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reports
    ADD CONSTRAINT reports_pkey PRIMARY KEY (id);


--
-- TOC entry 3161 (class 2606 OID 77431)
-- Name: subscription_company_enrollment subscription_company_enrollment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscription_company_enrollment
    ADD CONSTRAINT subscription_company_enrollment_pkey PRIMARY KEY (id);


--
-- TOC entry 3163 (class 2606 OID 77433)
-- Name: subscription_employee_enrollment subscription_employee_enrollment_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscription_employee_enrollment
    ADD CONSTRAINT subscription_employee_enrollment_pkey PRIMARY KEY (id);


--
-- TOC entry 3175 (class 2606 OID 77435)
-- Name: subscription_programs subscription_programs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscription_programs
    ADD CONSTRAINT subscription_programs_pkey PRIMARY KEY (shortcode);


--
-- TOC entry 3152 (class 2606 OID 77437)
-- Name: summary summary_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.summary
    ADD CONSTRAINT summary_pkey PRIMARY KEY (id);


--
-- TOC entry 3177 (class 2606 OID 77439)
-- Name: ticket_replies ticket_replies_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ticket_replies
    ADD CONSTRAINT ticket_replies_pkey PRIMARY KEY (id);


--
-- TOC entry 3179 (class 2606 OID 77441)
-- Name: tickets tickets_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (ticket_number);


--
-- TOC entry 3181 (class 2606 OID 77443)
-- Name: tickets tickets_ukey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.tickets
    ADD CONSTRAINT tickets_ukey UNIQUE (ticket_number);


--
-- TOC entry 3183 (class 2606 OID 77445)
-- Name: user_activities user_activities_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_activities
    ADD CONSTRAINT user_activities_pkey PRIMARY KEY (id);


--
-- TOC entry 3185 (class 2606 OID 77447)
-- Name: user_program_intervention user_program_intervention_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_program_intervention
    ADD CONSTRAINT user_program_intervention_pkey PRIMARY KEY (id);


--
-- TOC entry 3129 (class 2606 OID 77449)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 3125 (class 1259 OID 77450)
-- Name: answer_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX answer_user_id ON public.new_answers USING btree (user_id);


--
-- TOC entry 3132 (class 1259 OID 77451)
-- Name: appointment_code_index; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX appointment_code_index ON public.appointments USING btree (appointment_code);


--
-- TOC entry 3153 (class 1259 OID 77452)
-- Name: summary_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX summary_user_id ON public.summary USING btree (user_id);


--
-- TOC entry 3188 (class 2606 OID 77453)
-- Name: announcements announcements_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.announcements
    ADD CONSTRAINT announcements_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.admin(id);


--
-- TOC entry 3191 (class 2606 OID 77458)
-- Name: appointment_history appointment_history_appointment_code_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.appointment_history
    ADD CONSTRAINT appointment_history_appointment_code_fkey FOREIGN KEY (appointment_code) REFERENCES public.appointments(appointment_code);


--
-- TOC entry 3192 (class 2606 OID 77463)
-- Name: appointment_history appointment_history_md_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.appointment_history
    ADD CONSTRAINT appointment_history_md_id_fkey FOREIGN KEY (md_id) REFERENCES public.users(id);


--
-- TOC entry 3193 (class 2606 OID 77468)
-- Name: appointment_history appointment_history_patient_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.appointment_history
    ADD CONSTRAINT appointment_history_patient_id_fkey FOREIGN KEY (patient_id) REFERENCES public.users(id);


--
-- TOC entry 3194 (class 2606 OID 77473)
-- Name: cmpy_departments cmpy_dept_branch_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cmpy_departments
    ADD CONSTRAINT cmpy_dept_branch_fkey FOREIGN KEY (branch_id) REFERENCES public.cmpy_branches(id);


--
-- TOC entry 3195 (class 2606 OID 77478)
-- Name: company_queue company_queue_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.company_queue
    ADD CONSTRAINT company_queue_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(id);


--
-- TOC entry 3196 (class 2606 OID 77483)
-- Name: company_queue company_queue_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.company_queue
    ADD CONSTRAINT company_queue_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3198 (class 2606 OID 77488)
-- Name: events events_company_enrollment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_company_enrollment_id_fkey FOREIGN KEY (company_enrollment_id) REFERENCES public.subscription_company_enrollment(id);


--
-- TOC entry 3199 (class 2606 OID 77493)
-- Name: events events_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.admin(id);


--
-- TOC entry 3209 (class 2606 OID 93279)
-- Name: healthsurvey_summary healthsurvey_summary_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.healthsurvey_summary
    ADD CONSTRAINT healthsurvey_summary_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3204 (class 2606 OID 77498)
-- Name: md_admin_tokens md_admin_tokens_admin_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.md_admin_tokens
    ADD CONSTRAINT md_admin_tokens_admin_id_fkey FOREIGN KEY (admin_id) REFERENCES public.admin(id);


--
-- TOC entry 3205 (class 2606 OID 77503)
-- Name: md_admin_tokens md_admin_tokens_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.md_admin_tokens
    ADD CONSTRAINT md_admin_tokens_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3189 (class 2606 OID 77508)
-- Name: new_answers new_answers_question_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.new_answers
    ADD CONSTRAINT new_answers_question_id_fkey FOREIGN KEY (question_id) REFERENCES public.new_questions(id);


--
-- TOC entry 3190 (class 2606 OID 77513)
-- Name: new_answers new_answers_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.new_answers
    ADD CONSTRAINT new_answers_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3206 (class 2606 OID 77518)
-- Name: reports reports_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.reports
    ADD CONSTRAINT reports_fkey FOREIGN KEY (company_subscription_id) REFERENCES public.subscription_company_enrollment(id);


--
-- TOC entry 3200 (class 2606 OID 77523)
-- Name: subscription_company_enrollment subscription_company_enrollment_company_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscription_company_enrollment
    ADD CONSTRAINT subscription_company_enrollment_company_id_fkey FOREIGN KEY (company_id) REFERENCES public.companies(id);


--
-- TOC entry 3201 (class 2606 OID 77528)
-- Name: subscription_company_enrollment subscription_company_enrollment_program_code_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscription_company_enrollment
    ADD CONSTRAINT subscription_company_enrollment_program_code_fkey FOREIGN KEY (program_code) REFERENCES public.subscription_programs(shortcode) ON UPDATE CASCADE;


--
-- TOC entry 3202 (class 2606 OID 77533)
-- Name: subscription_employee_enrollment subscription_employee_enrollment_company_enrollment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscription_employee_enrollment
    ADD CONSTRAINT subscription_employee_enrollment_company_enrollment_id_fkey FOREIGN KEY (company_enrollment_id) REFERENCES public.subscription_company_enrollment(id);


--
-- TOC entry 3203 (class 2606 OID 77538)
-- Name: subscription_employee_enrollment subscription_employee_enrollment_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.subscription_employee_enrollment
    ADD CONSTRAINT subscription_employee_enrollment_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3197 (class 2606 OID 77543)
-- Name: summary summary_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.summary
    ADD CONSTRAINT summary_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- TOC entry 3207 (class 2606 OID 77548)
-- Name: ticket_replies ticket_replies_ticket_number_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ticket_replies
    ADD CONSTRAINT ticket_replies_ticket_number_fkey FOREIGN KEY (ticket_number) REFERENCES public.tickets(ticket_number);


--
-- TOC entry 3208 (class 2606 OID 77553)
-- Name: user_program_intervention user_program_intervention_employee_enrollment_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_program_intervention
    ADD CONSTRAINT user_program_intervention_employee_enrollment_id_fkey FOREIGN KEY (employee_enrollment_id) REFERENCES public.subscription_employee_enrollment(id);


-- Completed on 2021-07-12 17:57:51

--
-- PostgreSQL database dump complete
--

