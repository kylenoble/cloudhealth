import Joi from 'joi';

export default {
  // POST /api/users
  createUser: {
    body: {
      email: Joi.required(),
      name: Joi.string().required(),
      name: Joi.string().required(),
      password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
    }
  },

  // UPDATE /api/users/:userId
  updateUser: {
    body: {
      email: Joi.string().required(),
    },
    params: {
      userId: Joi.string().hex().required()
    }
  },

  // POST /api/auth/login
  login: {
    body: {
      email: Joi.string().required(),
      password: Joi.string().required()
    }
  },
  updateAnswer: {
    body: {
      answerId: Joi.number().required(),
    }
  },
  getAnswerGroup: {
    body: {
      group: Joi.string().required(),
      userId: Joi.number().required(),
    }
  },
};
