require('dotenv').config();

export default {
  env: 'production',
  jwtSecret: process.env.JWT_TOKEN,
  db: 'mongodb://localhost/express-mongoose-es6-rest-api-production',
  port: 4040
};
