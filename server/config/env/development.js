require('dotenv').config();

export default {
  env: 'development',
  MONGOOSE_DEBUG: true,
  jwtSecret: process.env.JWT_TOKEN,
  db: 'mongodb://localhost/express-mongoose-es6-rest-api-development',
  port: 4040
};
