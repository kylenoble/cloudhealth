import Promise from "bluebird";
import httpStatus from "http-status";
import APIError from "../helpers/APIError";

class Events {
  get(str, userid) {
    let strArr = str.split("_");
    var qry = `SELECT e.id, e.name, e.description, e.venue, e.start_date, e.start_time, e.end_date, e.end_time, e.status,
            e.date_created, e.created_by, e.read_by, c.company_name, e.company_enrollment_id,
            case when company_enrollment_id is not null then
            (select case when details::text != '' then 'Completed' else 'Pending' end from subscription_employee_enrollment ee left join user_program_intervention upi on ee.id = upi.employee_enrollment_id
             where ee.company_enrollment_id = e.company_enrollment_id and user_id=${userid} and enrollment_status='Approved' )
            else null end as attendance_status
        FROM events e
        left join admin a on e.created_by = a.id
        left join companies c on a.company_id = c.id
        where e.status != 'Cancelled'`;
    if (strArr[0] === "eventId") {
      qry += ` AND e.id = ${strArr[1]}`;
    }
    if (strArr[0] === "Patients") {
      qry += ` AND ('${strArr[0]}' = ANY (string_to_array(audience, ', '))`;
      qry += ` OR '${strArr[1]}' = ANY (string_to_array(audience, ', ')))`;
      qry += ` AND DATE(e.date_created) >= (select date(date_created) from companies where id=${strArr[1]}) `;
    } else {
      qry += ` AND '${strArr[0]}' = ANY (string_to_array(audience, ', '))`;
    }
    qry += ` ORDER BY e.start_date, e.id`;

    return new Promise(function(resolve, reject) {
      db.any(qry)
        .then(function(result) {
          resolve(result); // print answer object;
        })
        .catch(function(error) {
          const err = new APIError(
            "No such Events exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  update(data) {
    return new Promise(function(resolve, reject) {
      var query =
        pgp.helpers.update(data, null, "events") +
        " WHERE id = " +
        data.id +
        " RETURNING id, read_by";
      db.any(query)
        .then(res => {
          resolve(res);
        })
        .catch(error => {
          // error;
          const err = new APIError(
            "Error updating events",
            httpStatus.BAD_REQUEST
          );
          console.log(error);
          reject(err);
        });
    });
  }
}

export default Events;
