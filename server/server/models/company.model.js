import httpStatus from "http-status";
import APIError from "../helpers/APIError";
import bcrypt from "bcryptjs";
import Promise from "bluebird";

class Company {
  constructor() {
    this.SALT_WORK_FACTOR = 10;
  }

  /**
   * Get Company
   * @param {ObjectId} code - The objectId of company.
   * @returns {Promise<User, APIError>}
   */
  get(cde, userid) {
    let code = cde.trim();

    if (code === "company") {
      return new Promise(function (resolve, reject) {
        let sql = `select c.id, c.code, location, c.company_name, convert_from(c.logo::bytea, 'UTF8') as logo, (select status from reports  r where r.company_id = c.id  order by id desc limit 1)as status, (select file_name from reports  r where r.company_id = c.id  order by id desc limit 1) as file_name `;
        if (userid != null) {
          sql += `, CASE WHEN (select count(id) from company_queue where user_id=${userid} and company_id = c.id) > 0 THEN TRUE ELSE FALSE END as isAssignedToMe `;
        }
        sql += `from companies as c where is_active=true`;
        db.any(sql)
          .then(function (branches) {
            resolve(branches); // print company object;
          })
          .catch(function (error) {
            const err = new APIError(
              "No such Branches exists!",
              httpStatus.NOT_FOUND
            );
            reject(err);
          });
      });
    } else if (code === "branches") {
      return new Promise(function (resolve, reject) {
        db.any("select * from cmpy_branches order by br_name")
          .then(function (branches) {
            resolve(branches); // print company object;
          })
          .catch(function (error) {
            const err = new APIError(
              "No such Branches exists!",
              httpStatus.NOT_FOUND
            );
            reject(err);
          });
      });
    } else if (code === "departments") {
      return new Promise(function (resolve, reject) {
        db.any("select * from cmpy_departments order by dept_name")
          .then(function (depts) {
            resolve(depts); // print company object;
          })
          .catch(function (error) {
            const err = new APIError(
              "No such Branches exists!",
              httpStatus.NOT_FOUND
            );
            reject(err);
          });
      });
    } else {
      return new Promise(function (resolve, reject) {
        db.one(
          "select code, company_name, convert_from(logo::bytea, 'UTF8') as logo from companies where code=$1",
          code
        )
          .then(function (company) {
            resolve(company); // print company object;
          })
          .catch(function (error) {
            const err = new APIError(
              "No such Company exists!",
              httpStatus.NOT_FOUND
            );
            reject(err);
          });
      });
    }
  }

  list() {
    return new Promise(function (resolve, reject) {
      db.any(
        "select c.id, c.code, c.company_name, convert_from(c.logo::bytea, 'UTF8') as logo, c.location, dept_name from companies c left join cmpy_departments d on d.cmpy_id = c.id order by d.dept_name asc"
      )
        .then(function (company) {
          resolve(company); // print company object;
        })
        .catch(function (error) {
          const err = new APIError(
            "No such Company exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }
}
export default Company;
