import Promise from "bluebird";
import httpStatus from "http-status";
import APIError from "../helpers/APIError";

/**
 * Answer Schema
 */
class Answer {
  /**
   * Create answers
   * @param {Array} answers - The array of answers.
   * @returns {Promise<Success, APIError>}
   */
  create(answers) {
    return new Promise(function(resolve, reject) {
      db.task(function(t) {
        var queries = [];
        answers.forEach(function(answer) {
          // querying against the task protocol:
          queries.push(
            t.none(
              'INSERT INTO new_answers ("question_id", "user_id", "value", "count") VALUES (${questionId}, ${userId}, ${value}, ${count})',
              answer
            )
          );
        });
        return t.batch(queries); // settles all queries;
      })
        .then(function() {
          resolve("Answers inserted");
        })
        .catch(function(error) {
          // error;
          var err = new APIError(
            "Error adding answers",
            httpStatus.BAD_REQUEST
          );
          console.log(error);
          reject(err);
        });
    });
  }

  /**
   * Get answers
   * @param {ObjectId} id - The objectId of user.
   * @param {ObjectId} userId - The objectId of user.
   * @param {ObjectId} questionId - The objectId of user.
   * @param {ObjectId} group - The objectId of user.
   * @returns {Promise<Answer, APIError>}
   */
  get(type, value, userId) {
    return new Promise(function(resolve, reject) {
      db.one("select * from new_answers where $1~ = $2 and user_id = $3", [
        type,
        value,
        userId
      ])
        .then(function(answer) {
          resolve(answer); // print answer object;
        })
        .catch(function(error) {
          const err = new APIError(
            "No such answer exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  //flexible query
  getQuery(query) {
    return new Promise(function(resolve, reject) {
      db.any(query)
        .then(function(result) {
          resolve(result); // print answer object;
        })
        .catch(function(error) {
          const err = new APIError(
            "No Result found in flexible querying!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  /**
   * List answers
   * @param {ObjectId} id - The objectId of user.
   * @param {ObjectId} userId - The objectId of user.
   * @param {ObjectId} questionId - The objectId of user.
   * @param {ObjectId} group - The objectId of user.
   * @returns {Promise<Answer, APIError>}
   */
  list(query, userId) {
    let column = query.column;

    let operator = query.operator;

    let value = query.value;

    if (operator === "gt") {
      operator = ">";
    } else if (operator === "gte") {
      operator = ">=";
    } else if (operator === "lt") {
      operator = "<";
    } else if (operator === "lte") {
      operator = "<=";
    } else if (operator === "eq") {
      operator = "=";
    }

    let sqlQuery;
    if (query.order) {
      let order = query.order;
      let by = query.by;
      sqlQuery = `select q.id, a.question_id, user_id, value, count, name, label, q.group, created_date from new_answers a inner join new_questions q on a.question_id = q.id where a.${column} ${operator} ${value} and user_id = ${userId} order by a.${by} ${order}`;
    } else {
      sqlQuery = `select q.id, a.question_id, user_id, value, count, name, label, q.group, created_date from new_answers a inner join new_questions q on a.question_id = q.id where a.${column} ${operator} ${value} and user_id = ${userId}`;

      if (query.distinct_question) {
        sqlQuery = `select distinct on (q.label) q.label question, q.name, value answer
                from new_answers a
                inner join new_questions q on a.question_id = q.id
                where user_id = ${userId}
                order by q.label, created_date desc`;
      }

      if (query.coreImbalances) {
        sqlQuery = `select user_id, assimilation, defense_and_repair, energy, biotransformation_and_elimination, communication, transport, structural_integrity
        from core_imbalances_scores where user_id = ${userId} `;
      }
    }
    return new Promise(function(resolve, reject) {
      db.any(sqlQuery)
        .then(function(answers) {
          resolve(answers); // print answer object;
        })
        .catch(function(error) {
          console.log(error);
          const err = new APIError(
            "No such answer exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  /**
   * Get by field
   * @param {ObjectId} field - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  getGraphData(qryData) {
    let sql = "";
    let company = qryData.company;
    let graph = qryData.graph;
    let filter = qryData.filter;
    let filter_column = "";
    let joinTable = "";

    if (filter == "") {
      filter = "branch";
    }

    if (graph == "readiness") {
      if (filter == "branch") {
        filter_column = `CASE WHEN u.branch IS NULL OR u.branch = '' THEN 'Others' ELSE initcap(u.branch) END as name, `;
      } else if (filter == "dept") {
        filter_column = `CASE WHEN u.department IS NULL OR u.department = '' THEN 'Others' ELSE initcap(u.department) END as name,`;
      } else if (filter == "job_grade") {
        filter_column = `CASE WHEN u.job_grade IS NULL OR u.job_grade = '' THEN 'Others' ELSE initcap(u.job_grade) END as name,`;
      } else {
        filter_column = `${filter} as name,`;
        if (filter == "age_group") {
          filter_column = `DISTINCT ${filter} as name,`;
          joinTable = ` LEFT JOIN user_age_group uag ON uag.user_id = u.id `;
        }
      }
      sql = `SELECT
      ${filter_column}
      sum(CASE WHEN total_per_user >= 21 THEN 1 ELSE 0 END) AS ready,
      sum(CASE WHEN total_per_user < 21 THEN 1 ELSE 0 END) AS not_ready,
      count(readiness_graph_data.user_id) AS total_cnt
      FROM readiness_graph_data
      LEFT JOIN users u ON readiness_graph_data.user_id = u.id
      ${joinTable}
      where u.company = '${company}'
      GROUP BY 1 
      ORDER BY 1`;
    } else if (graph == "downstream") {
      sql = `select distinct ON (a.user_id) a.user_id, branch, department, job_grade, high_risk, age_group, a.value health_issues, u.healthsurvey_status
      from new_answers a
      LEFT JOIN new_questions q
      ON a.question_id = q.id
      LEFT JOIN users u
      ON a.user_id=u.id
      LEFT JOIN user_age_group uag ON uag.user_id = u.id
      where question_id=53
      and company='${company}'
      order by a.user_id, created_date desc`;
    } else if (graph == "msq") {
      sql = `SELECT DISTINCT * FROM msqScores_perSystem msq
      LEFT JOIN user_age_group uag ON uag.user_id = msq.user_id
      WHERE company = '${company}' `;
    } else if (graph == "upstream") {
      sql = `SELECT DISTINCT * FROM upstream_manifestations upstream
      LEFT JOIN user_age_group uag ON uag.user_id = upstream.user_id
      WHERE company = '${company}' `;
    } else if (graph == "upstream_msq_systemRank") {
      sql = `select DISTINCT * from msqSystem_ranks msq
        LEFT JOIN user_age_group uag ON uag.user_id = msq.user_id
        where company='${company}'
        order by total_score desc`;
    } else if (graph == "upstream_stress") {
      sql = `select DISTINCT * from upstream_stress u
      LEFT JOIN user_age_group uag ON uag.user_id = u.user_id
      where company='${company}' `;
    } else if (graph == "upstream_symptoms") {
      sql = `SELECT DISTINCT branch, department, job_grade, high_risk, ps.user_id, age_group, label, value, u.healthsurvey_status FROM prevalent_symptoms ps
        LEFT JOIN users u
        ON u.id = ps.user_id
        left join user_age_group uag on uag.user_id = ps.user_id
        WHERE company='${company}'
        order by ps.user_id`;
    } else if (graph == "upstream_foodchoices") {
      sql = `select distinct on (a.user_id, question_id) a.user_id, value, branch, department, job_grade, high_risk, age_group, u.healthsurvey_status 
        from new_answers a
        left join users u
        on u.id=a.user_id
        LEFT JOIN user_age_group uag ON uag.user_id = a.user_id
        where question_id=134
        and company='${company}'
        order by a.user_id, question_id, created_date desc
        `;
    }

    return new Promise(function(resolve, reject) {
      db.any(sql)
        .then(function(data) {
          resolve(data);
        })
        .catch(function(error) {
          const err = new APIError("No record found!", httpStatus.NOT_FOUND);
          reject(err);
        });
    });
  }

  /**
   * Get core imbalances scores
   * @param {ObjectId} userId
   * @returns {Promise<Answer, APIError>}
   */
  getCoreImbalancesScores(type, value, userId) {
    return new Promise(function(resolve, reject) {
      db.one("select * from core_imbalances_scores where user_id = $1", [
        userId
      ])
        .then(function(answer) {
          resolve(answer);
        })
        .catch(function(error) {
          const err = new APIError(
            "No such answer exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  /**
   * Update answer
   * @param {ObjectId} id - The objectId of user.
   * @param {ObjectId} userId - The objectId of user.
   * @param {ObjectId} questionId - The objectId of user.
   * @param {String} value - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  update(answers) {
    return new Promise(function(resolve, reject) {
      db.task(function(t) {
        var queries = [];
        answers.forEach(function(answer) {
          // querying against the task protocol:
          queries.push(
            t.none(
              'update new_answers set "value" = ${value}, "count" = ${count}  where question_id = ${questionId} and user_id = ${userId}',
              answer
            )
          );
        });
        return t.batch(queries); // settles all queries;
      })
        .then(function() {
          resolve("Answer updated");
        })
        .catch(function(error) {
          // error;
          var err = new APIError(
            "Error updating answers",
            httpStatus.BAD_REQUEST
          );
          console.log(error);
          reject(err);
        });
    });
  }

  getPieChartData(question_id, company_name) {
    return new Promise(function(resolve, reject) {
      let query = `
      SELECT * FROM new_answers a 
      LEFT JOIN new_questions q ON a.question_id = q.id 
      LEFT JOIN users u ON a.user_id = u.id 
      WHERE question_id=${question_id} AND u.company='${company_name}'`;

      db.any(query)
        .then(function(result) {
          resolve(result); // print answer object;
        })
        .catch(function(error) {
          const err = new APIError(
            "No Ticket Replies exist.!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }
}

/**
 * @typedef Answer
 */
export default Answer;
