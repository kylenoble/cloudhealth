/* eslint-disable indent */
import httpStatus from "http-status";
import bcrypt from "bcryptjs";
import Promise from "bluebird";
import APIError from "../helpers/APIError";

class Appointment {
  constructor() {
    this.SALT_WORK_FACTOR = 10;
  }

  /**
   * Create appointments
   * @param {Array} appointments - The array of appointments.
   * @returns {Promise<Success, APIError>}
   */
  create(appointment) {
    let NoPassword = false;
    if (appointment.NoPassword) {
      NoPassword = appointment.NoPassword;
    }
    return new Promise((resolve, reject) => {
      this.checkIfvalidPassword(
        appointment.email,
        appointment.password,
        NoPassword
      )
        .then(res => {
          if (res.authenticated) {
            // eslint-disable-next-line no-undef
            return db
              .task(t =>
                // `t` and `this` here are the same;
                // this.ctx = task config + state context;
                t
                  .oneOrNone("select id from users where email=$1", [
                    appointment.email
                  ])
                  .then(user => {
                    if (user) {
                      return t
                        .one(
                          'INSERT INTO appointments ("doctor_id", "patient_id", "datetime", "room_name", "concern", "date_requested", "patient_company_id", "active", "appointment_code", "parent_appointment_code") VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) RETURNING id',
                          [
                            appointment.doctorId,
                            user.id,
                            appointment.date,
                            appointment.roomName,
                            appointment.concern,
                            appointment.date_requested,
                            appointment.patient_company_id,
                            appointment.active,
                            appointment.appointment_code,
                            appointment.parent_appointment_code
                          ]
                        )
                        .then(() =>
                          t
                            .any(
                              "SELECT appointments.id, users.branch, users.department, users.avatar, users.name, appointments.datetime, appointments.room_name, appointments.doctor_id, appointments.patient_id, appointments.active FROM appointments INNER JOIN users ON users.id = appointments.patient_id where appointments.doctor_id = $1 and appointments.status = null ORDER BY appointments.datetime, appointments.datetime LIMIT 100",
                              [appointment.doctorId]
                            )
                            .then(appointments => {
                              resolve(appointments); // print user object;
                            })
                            .catch(error => {
                              console.log(error);
                              const err = new APIError(
                                "No such appointment",
                                httpStatus.NOT_FOUND
                              );
                              reject(err);
                            })
                        )
                        .catch(error => {
                          const err = new APIError(
                            "Error adding appointments",
                            httpStatus.BAD_REQUEST
                          );
                          console.log(error);
                          reject(err);
                        });
                    }
                    const err = new APIError(
                      "Error adding appointments. User not found",
                      httpStatus.BAD_REQUEST
                    );
                    console.log(err);
                    reject(err);
                  })
              )
              .catch(error => {
                const err = new APIError(
                  "Error adding appointments",
                  httpStatus.BAD_REQUEST
                );
                console.log(error);
                reject(err);
              });
          }
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  checkIfvalidPassword(email, password, NoPassword) {
    return new Promise((resolve, reject) => {
      if (NoPassword) {
        resolve({
          authenticated: true
        });
      } else {
        // eslint-disable-next-line no-undef
        db.one("select id, password from users where email=$1", email)
          .then(user => {
            bcrypt.compare(password, user.password, (error, isMatch) => {
              if (error) {
                const err = new APIError(error, httpStatus.UNAUTHORIZED);
                console.log(error);
                reject(err);
              }

              if ((password, isMatch)) {
                resolve({
                  authenticated: true
                });
              } else {
                const err = new APIError(
                  "Incorrect Information. Please check your password.",
                  httpStatus.UNAUTHORIZED
                );
                reject(err);
              }
            });
          })
          .catch(error => {
            let errorMessage;
            if (error.message === "No data returned from the query.") {
              errorMessage =
                "This email login doesn't exist. Please create an account";
            } else {
              errorMessage =
                "Incorrect Login Information. Please check your email/password.";
            }
            console.log(error);
            const err = new APIError(errorMessage, httpStatus.UNAUTHORIZED);
            reject(err);
          });
      }
    });
  }

  /**
   * Get appointments
   * @param {ObjectId} id - The objectId of user.
   * @param {ObjectId} userId - The objectId of user.
   * @param {ObjectId} questionId - The objectId of user.
   * @param {ObjectId} group - The objectId of user.
   * @returns {Promise<Appointment, APIError>}
   */
  get(type, value, userId, userType, apmt_date, apmt_datetime, taken) {
    return new Promise((resolve, reject) => {
      let query;
      if (taken) {
        query = `select * from appointments where (status in (1) or status isnull) `;
      } else if (type && value) {
        if (userType === "Doctor") {
          if (apmt_datetime) {
            query = `SELECT appointments.time_extension, appointments.patient_company_id, appointments.appointment_code, appointments.id, company, age, (select distinct on (question_id) value from new_answers where user_id = appointments.patient_id and question_id=2 
            order by question_id, created_date desc) as gender, (select na.value from new_answers na where na.user_id = appointments.patient_id and na.question_id = 133 order by na.created_date limit 1 offset 2)as chief_complaint, users.branch, users.department, users.avatar, users.name, users.id as userid, appointments.datetime, appointments.room_name, appointments.doctor_id, appointments.patient_id, appointments.status, appointments.active, appointments.concern, appointments.date_requested FROM appointments INNER JOIN users ON users.id = appointments.patient_id LEFT JOIN user_age_group uag on users.id = uag.user_id where appointments.doctor_id = $3 and CAST(appointments.datetime AS DATE) = '${apmt_datetime}' AND appointments.$1~ = $2 ORDER BY appointments.datetime, appointments.datetime ASC LIMIT 100`;
          } else if (apmt_date) {
            query = `SELECT appointments.time_extension, appointments.patient_company_id, appointments.appointment_code, appointments.id, company, age, (select distinct on (question_id) value from new_answers where user_id = appointments.patient_id and question_id=2 
            order by question_id, created_date desc) as gender, (select na.value from new_answers na where na.user_id = appointments.patient_id and na.question_id = 133 order by na.created_date limit 1 offset 2)as chief_complaint, users.branch, users.department, users.avatar, users.name, users.id as userid, appointments.datetime, appointments.room_name, appointments.doctor_id, appointments.patient_id, appointments.status, appointments.active FROM appointments INNER JOIN users ON users.id = appointments.patient_id LEFT JOIN user_age_group uag on users.id = uag.user_id where DATE(appointments.datetime) >= DATE(NOW()) and appointments.doctor_id = $3 and appointments.datetime = '${apmt_date}' AND appointments.$1~ = $2 and appointments.status isNull ORDER BY appointments.datetime, appointments.datetime ASC LIMIT 100`;
          } else {
            query = `SELECT appointments.time_extension, appointments.patient_company_id, appointments.appointment_code, appointments.id, company, age, (select distinct on (question_id) value from new_answers where user_id = appointments.patient_id and question_id=2 
            order by question_id, created_date desc) as gender,
            (select na.value from new_answers na where na.user_id = appointments.patient_id and na.question_id = 133 
             order by na.created_date limit 1 offset 2)as chief_complaint, 
             users.branch, users.department, users.avatar, users.name, users.email, users.id as userid, appointments.datetime, 
             appointments.room_name, appointments.doctor_id, appointments.patient_id, appointments.status, appointments.active, 
             appointments.concern, appointments.date_requested, users.company, users.employee_id,
            (select distinct on (question_id) value from new_answers a 
             where user_id = appointments.patient_id and question_id = 2 order by question_id, created_date desc) as gender
            FROM appointments 
            INNER JOIN users ON users.id = appointments.patient_id 
            LEFT JOIN user_age_group uag on users.id = uag.user_id
            where DATE(appointments.datetime) >= DATE(NOW())
            and appointments.doctor_id = $3 AND appointments.$1~ = $2 
            ORDER BY appointments.datetime, appointments.datetime ASC LIMIT 100`;
          }
        } else if (apmt_date) {
          query = `SELECT appointments.time_extension, appointments.patient_company_id, appointments.appointment_code, appointments.id, company, age, (select distinct on (question_id) value from new_answers where user_id = appointments.patient_id and question_id=2 
          order by question_id, created_date desc) as gender, (select na.value from new_answers na where na.user_id = appointments.patient_id and na.question_id = 133 order by na.created_date limit 1 offset 2)as chief_complaint, users.branch, users.department, users.avatar, users.name, users.id as userid, appointments.datetime, appointments.room_name, appointments.doctor_id, appointments.patient_id, appointments.status, appointments.active, appointments.concern, appointments.date_requested FROM appointments INNER JOIN users ON users.id = appointments.doctor_id LEFT JOIN user_age_group uag on users.id = uag.user_id where DATE(appointments.datetime) >= DATE(NOW()) and appointments.datetime = '${apmt_date}' and (appointments.status = 1 or appointments.status isnull) AND appointments.$1~ = $2 ORDER BY appointments.datetime, appointments.datetime ASC LIMIT 100`;
        } else {
          query = `SELECT appointments.time_extension, appointments.patient_company_id, appointments.appointment_code, appointments.id, company, age, (select distinct on (question_id) value from new_answers where user_id = appointments.patient_id and question_id=2 
            order by question_id, created_date desc) as gender, (select na.value from new_answers na where na.user_id = appointments.patient_id and na.question_id = 133 order by na.created_date limit 1 offset 2)as chief_complaint, users.branch, users.department, users.avatar, users.name, users.id as userid, appointments.datetime, appointments.room_name, appointments.doctor_id, appointments.patient_id, appointments.status, appointments.active, appointments.concern, appointments.date_requested FROM appointments INNER JOIN users ON users.id = appointments.doctor_id LEFT JOIN user_age_group uag on users.id = uag.user_id where DATE(appointments.datetime) >= DATE(NOW()) and appointments.patient_id = $3 and appointments.$1~ = $2 ORDER BY appointments.datetime, appointments.datetime ASC LIMIT 100`;
        }
      } else if (userType === "Doctor") {
        if (apmt_date) {
          query = `SELECT appointments.time_extension, appointments.patient_company_id, appointments.appointment_code, appointments.id, company, age, (select distinct on (question_id) value from new_answers where user_id = appointments.patient_id and question_id=2 
          order by question_id, created_date desc) as gender, (select na.value from new_answers na where na.user_id = appointments.patient_id and na.question_id = 133 order by na.created_date limit 1 offset 2)as chief_complaint, users.branch, users.department, users.avatar, users.name, users.id as userid, appointments.datetime, appointments.room_name, appointments.doctor_id, appointments.patient_id, appointments.status, appointments.active, appointments.concern, appointments.date_requested FROM appointments INNER JOIN users ON users.id = appointments.doctor_id LEFT JOIN user_age_group uag on users.id = uag.user_id where DATE(appointments.datetime) >= DATE(NOW()) and appointments.patient_id = $3 and CAST(appointments.datetime AS DATE) = '${apmt_date}' ORDER BY appointments.datetime, appointments.datetime ASC LIMIT 100`;
        } else {
          query = `SELECT appointments.time_extension, appointments.patient_company_id, appointments.appointment_code, appointments.id, company, age, (select distinct on (question_id) value from new_answers where user_id = appointments.patient_id and question_id=2 
            order by question_id, created_date desc) as gender, (select na.value from new_answers na where na.user_id = appointments.patient_id and na.question_id = 133 order by na.created_date limit 1 offset 2)as chief_complaint, users.branch, users.department, users.avatar, users.name, users.id as userid, appointments.datetime, appointments.room_name, appointments.doctor_id, appointments.patient_id, appointments.status, appointments.active, appointments.concern, appointments.date_requested FROM appointments INNER JOIN users ON users.id = appointments.patient_id LEFT JOIN user_age_group uag on users.id = uag.user_id where DATE(appointments.datetime) >= DATE(NOW()) and appointments.doctor_id = $3 ORDER BY appointments.datetime, appointments.datetime ASC LIMIT 100`;
        }
      } else if (apmt_date) {
        query = `SELECT appointments.time_extension, appointments.patient_company_id, appointments.appointment_code, appointments.id, company, age, (select distinct on (question_id) value from new_answers where user_id = appointments.patient_id and question_id=2 
        order by question_id, created_date desc) as gender, (select na.value from new_answers na where na.user_id = appointments.patient_id and na.question_id = 133 order by na.created_date desc limit 1 offset 2)as chief_complaint, users.branch, users.department, users.avatar, users.name, users.id as userid, appointments.datetime, appointments.room_name, appointments.doctor_id, appointments.patient_id, appointments.status, appointments.active, appointments.concern, appointments.date_requested FROM appointments INNER JOIN users ON users.id = appointments.doctor_id LEFT JOIN user_age_group uag on users.id = uag.user_id where DATE(appointments.datetime) >= DATE(NOW()) and appointments.patient_id = $3 and CAST(appointments.datetime AS DATE) = '${apmt_date}' ORDER BY appointments.datetime, appointments.datetime ASC LIMIT 100`;
      } else {
        query = `SELECT appointments.time_extension, appointments.patient_company_id, appointments.appointment_code, appointments.id, company, age, (select distinct on (question_id) value from new_answers where user_id = appointments.patient_id and question_id=2 
          order by question_id, created_date desc) as gender, (select na.value from new_answers na where na.user_id = appointments.patient_id and na.question_id = 133 order by na.created_date limit 1 offset 2)as chief_complaint, users.branch, users.department, users.avatar, users.name, users.id as userid, appointments.datetime, appointments.room_name, appointments.doctor_id, appointments.patient_id, appointments.status, appointments.active, appointments.concern, appointments.date_requested FROM appointments INNER JOIN users ON users.id = appointments.doctor_id LEFT JOIN user_age_group uag on users.id = uag.user_id where DATE(appointments.datetime) >= DATE(NOW()) and appointments.patient_id = $3 ORDER BY appointments.datetime, appointments.datetime ASC LIMIT 100`;
      }

      // eslint-disable-next-line no-undef
      db.any(query, [type, value, userId])
        .then(appointments => {
          resolve(appointments); // print user object;
        })
        .catch(error => {
          console.log(error);
          const err = new APIError("No such appointment", httpStatus.NOT_FOUND);
          reject(err);
        });
    });
  }

  // cancel appointment

  cancel(id) {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line no-undef
      db.result("DELETE FROM appointments WHERE id = $1", [id], r => r.rowCount)
        .then(() => {
          resolve("Appointment Cancelled");
          // data = number of rows that were deleted
        })
        .catch(error => {
          console.log(error);
          const err = new APIError(
            "Error canceling apointment",
            httpStatus.BAD_REQUEST
          );
          reject(err);
        });
    });
  }

  update(appointmentId, data) {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line no-undef
      const qry = `${pgp.helpers.update(
        data,
        null,
        "appointments"
      )} WHERE id = ${appointmentId} RETURNING id, doctor_id, patient_id, room_name, active, status, concern, patient_company_id, date_requested, datetime, date_rescheduled, is_modified, appointment_code, date_cancelled, time_extension, parent_appointment_code`;
      // eslint-disable-next-line no-undef
      db.one(qry)
        .then(res => {
          resolve("Appointment Updated"); // this will be replace with "resolve(res)";
        })
        .catch(error => {
          console.log(error);
          const err = new APIError(
            "Error updating user",
            httpStatus.BAD_REQUEST
          );
          reject(err);
        });
    });
  }

  saveAppointmentHistory(body) {
    return new Promise((resolve, reject) => {
      const qry = `INSERT INTO appointment_history(
                appointment_code, patient_id, history_created_date, concern, appointment_date, appointment_status, md_id, action)
                VALUES ($1, $2, $3, $4, $5, $6, $7, $8) returning appointment_code, patient_id, history_created_date, concern, appointment_date, appointment_status, md_id, action`;
      // eslint-disable-next-line no-undef

      db.one(qry, [
        body.appointment_code,
        body.patient_id,
        body.history_created_date,
        body.concern,
        body.appointment_date,
        body.appointment_status,
        body.md_id,
        body.action
      ])
        .then(res => {
          resolve(res);
        })
        .catch(e => {
          console.log(e);
          const err = new APIError(
            "Error inserting to Appointment History database",
            httpStatus.BAD_REQUEST
          );
          reject(err);
        });
    });
  }
}

/**
 * @typedef Appointment
 */
export default Appointment;
