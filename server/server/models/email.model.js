import httpStatus from 'http-status';
import APIError from '../helpers/APIError'
import bcrypt from 'bcryptjs'
import Promise from 'bluebird'

/**
 * User Schema
 */
class Email {
  constructor() {
    this.SALT_WORK_FACTOR = 10;
  }



  /**
   * Get email
   * @param {ObjectId} email - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(email) {
      return new Promise(function(resolve, reject) {
      db.any('select u.id, u.name, u.email from users u where UPPER(u.email)=$1 order by u.id desc limit 1', email.toUpperCase()) // NOT USE DB.ONE here because there maybe a duplicate email
      .then(function (user) {
        resolve(user) // print user object;
      })
      .catch(function (error) {
        const err = new APIError('No such user exists!', httpStatus.NOT_FOUND);
        reject(err)
      });
    })
  }

  /**
   * Update user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  update(userId, inputs) {

    const that = this;

    return new Promise((resolve, reject) => {
      if (inputs.password) {
          that.generateHash(inputs.password)
          .then((hash) => {
            inputs.password = hash;
            var query = pgp.helpers.update(inputs, null, 'users') + ' WHERE id = ' + userId + ' RETURNING id, email, name';

            db.one(query)
              .then(function (user) {
                resolve(user) // return success;
              })
              .catch(function (error) {
                console.log(error)
                const err = new APIError('Error updating user', httpStatus.BAD_REQUEST);
                reject(err)
              });
          })
          .catch((error) => {
            reject(error);
          });
      }
    });
  }


  generateSalt() {
    var that = this

    return new Promise(function(resolve, reject) {
      bcrypt.genSalt(that.SALT_WORK_FACTOR, function(err, salt) {
        if (err) reject(err)

        resolve(salt)
      })
    })

  }



  generateHash(password) {

    var that = this

    return new Promise(function(resolve, reject) {
      that.generateSalt()
      .then(salt => {
          // hash the password using our new salt
          bcrypt.hash(password, salt, function(err, hash) {
              if (err) reject(err)

              resolve(hash)
          })
        })
        .catch(error => {
          reject(error)
        })
    })

  }

}

/**
 * @typedef Email
 */
export default Email;
