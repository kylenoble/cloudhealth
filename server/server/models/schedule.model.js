import Promise from "bluebird";
import httpStatus from "http-status";
import APIError from "../helpers/APIError";
import moment from "moment";

class Schedule {
  get(params) {
    let qry = "";
    if (params.id && params.startDate && params.endDate) {
      qry = `select s.id, s.user_id, s.start_datetime, s.end_datetime, s.title, s.status, s.modified_date, s.created_date, a.room_name, a.active, a.status as appointment_status, a. concern, a.patient_id, a.date_requested, a.datetime, a.id as appointment_id
              from schedules s
              left join appointments a on s.start_datetime = a.datetime
              where s.user_id = ${params.id} and s.start_datetime = '${
        params.startDate
      }' and s.end_datetime = '${params.endDate}' `;
    } else if (params.id) {
      if (params.filter) {
        let condition = "";
        if (params.filter.condition === "gt") {
          condition = ">";
        } else if (params.filter.condition === "equal") {
          condition = "=";
        } else if (params.filter.condition === "lt") {
          condition = "<";
        } else if (params.filter.condition === "gte") {
          condition = ">=";
        } else if (params.filter.condition === "lte") {
          condition = "<=";
        }

        if ((params.field = "start_datetime")) {
          qry = `select s.id, s.user_id, s.start_datetime, s.end_datetime, s.title, s.status, s.modified_date, s.created_date, a.room_name, a.active, a.status as appointment_status, a. concern, a.patient_id, a.date_requested, a.datetime, a.id as appointment_id
              from schedules s
              left join appointments a on s.start_datetime = a.datetime
              where s.user_id = ${params.id}
              and cast(s.start_datetime as date) ${condition} '${
            params.filter.value
          }' `;
        } else {
          qry = `select s.id, s.user_id, s.start_datetime, s.end_datetime, s.title, s.status, s.modified_date, s.created_date, a.room_name, a.active, a.status as appointment_status, a. concern, a.patient_id, a.date_requested, a.datetime, a.id as appointment_id
              from schedules s
              left join appointments a on s.start_datetime = a.datetime
              where s.user_id = ${params.id}
              and s.${params.filter.field} ${condition} '${
            params.filter.value
          }' `;
        }
      } else {
        qry = `select s.id, s.user_id, s.start_datetime, s.end_datetime, s.title, s.status, s.modified_date, s.created_date, a.room_name, a.active, a.status as appointment_status, a. concern, a.patient_id, a.date_requested, a.datetime, a.id as appointment_id
            from schedules s
            left join appointments a on s.start_datetime = a.datetime
            where s.user_id = ${params.id} `;
      }
    } else if (params.strDate && params.endDate) {
      qry = `select s.id, s.user_id, s.start_datetime, s.end_datetime, s.title, s.status, s.modified_date, s.created_date, a.room_name, a.active, a.status as appointment_status, a. concern, a.patient_id, a.date_requested, a.datetime, a.id as appointment_id
              from schedules s
              left join appointments a on s.start_datetime = a.datetime
              where cast(s.start_datetime as date) <= '${
                params.strDate
              }' and cast(s.end_datetime as date) >= '${params.endDate}' `;
    }

    // qry += `and s.start_datetime >= now() `;

    qry += `order by s.start_datetime`;

    return new Promise(function(resolve, reject) {
      db.any(qry)
        .then(function(result) {
          resolve(result);
        })
        .catch(function(e) {
          const err = new APIError(
            "No such Notifications exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  create(data) {
    return new Promise(function(resolve, reject) {
      const query =
        "INSERT INTO schedules(user_id, start_datetime, end_datetime, title, created_date, status) VALUES ($1, $2, $3, $4, $5, $6) returning user_id, start_datetime, end_datetime, title, created_date";
      db.one(query, [
        data.user_id,
        data.start_datetime,
        data.end_datetime,
        data.title,
        data.created_date,
        data.status
      ])
        .then(resdata => {
          resolve(resdata);
        })
        .catch(error => {
          var err = new APIError(
            "Error inserting to schedules database",
            httpStatus.BAD_REQUEST
          );
          reject(err);
        });
    });
  }

  update(obj) {
    let qry = "";
    if (obj.option === "delete") {
      qry = `DELETE from schedules where id = ${obj.data.id}`;
      return new Promise((resolve, reject) => {
        db.result(qry, r => r.rowCount)
          .then(data => {
            resolve(true);
            // data = number of rows that were deleted
          })
          .catch(error => {
            console.log(error);
            const err = new APIError(
              "Error canceling schedule",
              httpStatus.BAD_REQUEST
            );
            reject(err);
          });
      });
    } else if ((obj.option = "update")) {
      qry =
        pgp.helpers.update(obj.data, null, "schedules") +
        " WHERE id = " +
        obj.data.id +
        " RETURNING user_id, status";
      return new Promise((resolve, reject) => {
        db.one(qry)
          .then(data => {
            resolve(data);
            // data = number of rows that were deleted
          })
          .catch(error => {
            console.log(error);
            const err = new APIError(
              "Error canceling schedule",
              httpStatus.BAD_REQUEST
            );
            reject(err);
          });
      });
    } else {
      console.log("NO QUERY EXECUTED");
    }
  }
}

export default Schedule;
