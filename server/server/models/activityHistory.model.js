import Promise from 'bluebird'
import httpStatus from 'http-status'
import APIError from '../helpers/APIError'

class ActivityHistory {
  getHost(id) {
    let qry = `SELECT * FROM admin a WHERE a.id = '${id}'`

    return new Promise(function (resolve, reject) {
      db.any(qry)
        .then(function (result) {
          resolve(result) // print answer object;
        })
        .catch(function (error) {
          const err = new APIError('No such Events exists!', httpStatus.NOT_FOUND)
          reject(err)
        })
    })
  }
  getEvents(id, userid) {
    let qry = `select id, name, description, venue, start_date, start_time, end_date, end_time,
            status, date_created, date_modified, health_talk, audience, created_by, read_by,
            company_enrollment_id,
            case when company_enrollment_id is not null then
            (select case when details::text != '' then 'Completed' else 'Pending' end from subscription_employee_enrollment ee left join user_program_intervention upi on ee.id = upi.employee_enrollment_id
             where ee.company_enrollment_id = e.company_enrollment_id and user_id=${userid} and enrollment_status='Approved' )
            else null end as attendance_status
            from events e
            WHERE e.audience::varchar = '${id}' OR 'Patients' = any(regexp_split_to_array(e.audience::varchar,',')::varchar[])
             order by id DESC`

    return new Promise(function (resolve, reject) {
      db.any(qry)
        .then(function (result) {
          resolve(result) // print answer object;
        })
        .catch(function (error) {
          const err = new APIError('No such Events exists!', httpStatus.NOT_FOUND)
          reject(err)
        })
    })
  }

  getAppointments(id) {
    // let qry = `SELECT * FROM appointments WHERE patient_id = ${id} AND status >= 1 order by datetime DESC`
    let qry = `SELECT * FROM appointments WHERE patient_id = ${id} order by id DESC`

    return new Promise(function (resolve, reject) {
      db.any(qry)
        .then(function (result) {
          resolve(result) // print answer object;
        })
        .catch(function (error) {
          const err = new APIError('No such Appointments exists!', httpStatus.NOT_FOUND)
          reject(err)
        })
    })
  }

  getSubscriptionPrograms() {
    let qry = `SELECT * FROM subscription_programs order by id DESC`

    return new Promise(function (resolve, reject) {
      db.any(qry)
        .then(function (result) {
          resolve(result) // print answer object;
        })
        .catch(function (error) {
          const err = new APIError('No such Subscriptions exists!', httpStatus.NOT_FOUND)
          reject(err)
        })
    })
  }

  // get subscriptions by employee
  getSubscriptions(id) {
    let qry = `SELECT * FROM subscription_employee_enrollment WHERE user_id = ${id} order by date_enrolled DESC`

    return new Promise(function (resolve, reject) {
      db.any(qry)
        .then(function (result) {
          resolve(result) // print answer object;
        })
        .catch(function (error) {
          const err = new APIError('No such Subscriptions exists!', httpStatus.NOT_FOUND)
          reject(err)
        })
    })
  }

  // get subscriptions by company
  getSubscriptionsByCompany(id) {
    let qry = `SELECT * FROM subscription_company_enrollment WHERE id = ${id} order by id DESC`

    return new Promise(function (resolve, reject) {
      db.any(qry)
        .then(function (result) {
          resolve(result) // print answer object;
        })
        .catch(function (error) {
          const err = new APIError('No such Subscriptions exists!', httpStatus.NOT_FOUND)
          reject(err)
        })
    })
  }

  getDoctor(id) {
    let qry = `SELECT * FROM users WHERE id = ${id}`

    return new Promise(function (resolve, reject) {
      db.any(qry)
        .then(function (result) {
          resolve(result) // print answer object;
        })
        .catch(function (error) {
          const err = new APIError('No such Subscriptions exists!', httpStatus.NOT_FOUND)
          reject(err)
        })
    })
  }

  update(data) {
    return new Promise(function (resolve, reject) {
      var query = pgp.helpers.update(data, null, 'events') + ' WHERE id = ' + data.id + ' RETURNING id, read_by'
      db.any(query)
        .then(res => {
          resolve(res)
        })
        .catch(error => {
          // error;
          const err = new APIError('Error updating events', httpStatus.BAD_REQUEST)
          console.log(error)
          reject(err)
        })
    })
  }
}

export default ActivityHistory
