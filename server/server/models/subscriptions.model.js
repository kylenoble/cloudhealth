import Promise from "bluebird";
import httpStatus from "http-status";
import APIError from "../helpers/APIError";

class Subscriptions {
  get(body) {
    var qry = `select c.id, c.company_id, c.date_created, c.status, e.id as enrollment_id, e.employee_id, e.is_anonymous, e.date_enrolled, e.date_approved, e.date_declined, e.enrollment_status, e.company_enrollment_id, c.program_code, p.subscription_name, p.inclusions, i.details,
    ev.name as event_name, ev.status as event_status
                from subscription_company_enrollment c
                left join subscription_employee_enrollment e on c.id = e.company_enrollment_id
                left join subscription_programs p on c.program_code = p.shortcode
                left join user_program_intervention i on e.id = i.employee_enrollment_id
                left join events ev on e.company_enrollment_id = ev.company_enrollment_id`;

    if (body.field === "employee_id" || body.field === "user_id") {
      qry += ` where e.${body.field} = '${body.id}'`;
    } else if (body.field === "company_id") {
      qry += ` where c.${body.field} = '${body.id}'`;
    }

    if (body.status) {
      qry += ` and c.status in (${body.status})`;
    }

    qry += ` order by c.date_created desc`;

    return new Promise(function(resolve, reject) {
      db.any(qry)
        .then(function(result) {
          resolve(result); // print answer object;
        })
        .catch(function(error) {
          const err = new APIError(
            "No such Subscription exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  update(data) {
    let qry =
      pgp.helpers.update(data, null, "subscription_employee_enrollment") +
      "WHERE id = " +
      data.id +
      " RETURNING id, enrollment_status";
    return new Promise((resolve, reject) => {
      db.one(qry)
        .then(result => {
          resolve(result);
        })
        .catch(e => {
          const err = new APIError(
            "Error Updating Subscription! ",
            httpStatus.BAD_REQUEST
          );
          reject(err);
        });
    });
  }

  // get subscription programs
  getProgram(body) {
    const { shortcode, is_basic } = body;
    var qry = `select * from subscription_programs`;
    if (shortcode) {
      qry += ` where shortcode = ${shortcode}`;
    } else if (is_basic) {
      qry += ` where is_basic = ${is_basic}`;
    }

    return new Promise(function(resolve, reject) {
      db.any(qry)
        .then(function(result) {
          resolve(result); // print answer object;
        })
        .catch(function(error) {
          const err = new APIError(
            "No such Program exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }
}

export default Subscriptions;
