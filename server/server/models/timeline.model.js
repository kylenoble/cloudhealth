import Promise from 'bluebird';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';



class Timeline {
      get(id) {
        var qry = `select * from timeline where user_id = ${id} order by event_date`


          return new Promise(function(resolve, reject) {
          db.any(qry)
          .then(function (result) {
            resolve(result) // print answer object;
          })
          .catch(function (error) {
            const err = new APIError('No such Timeline exists!', httpStatus.NOT_FOUND);
            reject(err)
          });
        })
      }

      update(data){
        return new Promise(function(resolve, reject) {
          db.task(function (t) {
            var queries = [];
            data.forEach(function (data) {
                if(data.new_event){
                  queries.push(t.none(`INSERT INTO timeline(event_date, event_name, description, comment, user_id) VALUES ('${data.event_date}', '${data.event_name}', '${data.description}', '${data.comment}', ${data.user_id})`, data))
                }else if(data.del){
                  queries.push(t.none(`DELETE FROM timeline where id = ${data.id}`, data))
                }else{
                  queries.push(t.none(`UPDATE timeline SET event_date='${data.event_date}', event_name='${data.event_name}', description='${data.description}', comment='${data.comment}', user_id=${data.user_id}, tag=${data.tag}, dfpe=${data.dfpe}	WHERE id = ${data.id}`, data))
                }

            });
            return t.batch(queries); // settles all queries;
          })
          .then(function () {
            resolve('Timeline Update')
          })
          .catch(function (error) {
              // error;
              var err = new APIError("Error Updateting timeline", httpStatus.BAD_REQUEST);
              console.log(error)
              reject(err);
          })
        })
      }
  }




export default Timeline;
