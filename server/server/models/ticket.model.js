// import Promise from '../../../../../../AppData/Local/Microsoft/TypeScript/2.9/node_modules/@types/bluebird';
import Promise from 'bluebird'
import httpStatus from 'http-status'
import APIError from '../helpers/APIError'

export default class Ticket {
  getReplies(ticket_num) {
    return new Promise(function (resolve, reject) {
      let qry = `SELECT * FROM ticket_replies WHERE ticket_number=$1`

      db.any(qry, ticket_num)
        .then(function (result) {
          resolve(result) // print answer object;
        })
        .catch(function (error) {
          const err = new APIError('No Ticket Replies exist.!', httpStatus.NOT_FOUND)
          reject(err)
        })
    })
  }

  getCompanies() {
    return new Promise(function (resolve, reject) {
      let qry = `SELECT * FROM companies c WHERE c.is_active = true`

      db.any(qry)
        .then(function (result) {
          resolve(result) // print answer object;
        })
        .catch(function (error) {
          const err = new APIError('No Company exist.!', httpStatus.NOT_FOUND)
          reject(err)
        })
    })
  }

  getLatestTicket() {
    return new Promise(function (resolve, reject) {
      let qry = `SELECT * FROM tickets order by id desc limit 1`

      db.any(qry)
        .then(function (result) {
          resolve(result) // print answer object;
        })
        .catch(function (error) {
          const err = new APIError('No Ticket Replies exist.!', httpStatus.NOT_FOUND)
          reject(err)
        })
    })
  }

  getTickets(ticket_num) {
    return new Promise(function (resolve, reject) {
      let qry = `SELECT t.ticket_number, t.fullname, t.email_address, t.concern, t.subject, t.message, t.attachment_filename, t.status, t.company_name, t.date_created
            FROM tickets t
            WHERE t.ticket_number=$1`
      db.any(qry, ticket_num)
        .then(function (result) {
          resolve(result) // print answer object;
        })
        .catch(function (error) {
          const err = new APIError('No Ticket Replies exist.!', httpStatus.NOT_FOUND)
          reject(err)
        })
    })
  }

  create(ticket) {
    return new Promise(function (resolve, reject) {
      const query =
        'INSERT INTO tickets(ticket_number, fullname, email_address, company_name, concern, subject, message, attachment_filename, date_created)  VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) returning ticket_number, fullname, email_address, company_name, concern, subject, message, attachment_filename'
      db.one(query, [
        ticket.ticket_number,
        ticket.name,
        ticket.email,
        ticket.company,
        ticket.concern,
        ticket.subject,
        ticket.message,
        ticket.attachment,
        ticket.date_created
      ])
        .then(res => {
          resolve(res)
        })
        .catch(error => {
          const err = new APIError('Error submitting Ticket', httpStatus.BAD_REQUEST)
          console.log(error)
          reject(err)
        })
    })
  }
}
