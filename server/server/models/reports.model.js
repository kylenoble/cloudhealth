import httpStatus from "http-status";
import APIError from "../helpers/APIError";
import bcrypt from "bcryptjs";
import Promise from "bluebird";

class Reports {
  constructor() {
    this.SALT_WORK_FACTOR = 10;
  }

  /**
   * Save Company Report
   * @param {Array} data - The array of answers.
   * @returns {Promise<Success, APIError>}
   */
  saveReport(data) {
    return new Promise(function(resolve, reject) {
      const query =
        "INSERT INTO reports(date_created, insights, md_incharge, md_id, programs, mean_scores, scoreandpercentile, status, date_modified, company_id, company_name, location, file_name, report_name, company_subscription_id, details, score, percentile_rank, health_status)  VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19) returning company_id, date_created, insights, md_incharge, md_id, details";
      db.one(query, [
        data.date_created,
        data.insights,
        data.md_incharge,
        data.md_id,
        data.programs,
        data.mean_scores,
        data.scoreandpercentile,
        data.status,
        data.date_modified,
        data.company_id,
        data.company_name,
        data.location,
        data.file_name,
        data.report_name,
        data.company_subscription_id,
        data.details,
        data.score,
        data.percentile_rank,
        data.health_status
      ])
        .then(res => {
          resolve(res);
        })
        .catch(error => {
          // error;
          const err = new APIError(
            "Error saving Report",
            httpStatus.BAD_REQUEST
          );
          console.log(error);
          reject(err);
        });
    });
  }

  updateReport(data) {
    return new Promise(function(resolve, reject) {
      var query =
        pgp.helpers.update(data, null, "reports") +
        " WHERE id = " +
        data.id +
        " RETURNING company_id, status";
      db.any(query)
        .then(res => {
          resolve(res);
        })
        .catch(error => {
          // error;
          const err = new APIError(
            "Error updating Report - " + error,
            httpStatus.BAD_REQUEST
          );
          console.log(error);
          reject(err);
        });
    });
  }

  getReport(data) {
    return new Promise(function(resolve, reject) {
      //const query = 'select * from reports where company_id = $1 and (status != 1 or status is null)';
      let query;
      if (data.field === "all") {
        query = `select r.id, r.date_created, r.insights, r.md_incharge, r.md_id,
        r.details, r.status, r.date_modified, r.company_id, r.company_name,r.location, 
        r.file_name, r.score, r.percentile_rank, r.registered_employee, 
        r.health_status, r.compliance_rate, r.programs, r.mean_scores, 
        r.scoreandpercentile, r.report_name, r.company_subscription_id,
        subscription_name
        from reports r 
        left join subscription_company_enrollment sce on r.company_subscription_id = sce.id
        left join subscription_programs sp on sp.shortcode = sce.program_code
        where r.status is null order by r.id desc`;
      } else {
        let column = data.field === "company_id" ? "r.company_id" : data.field;
        let where = `${column} = ${data.value}`;
        query = `select r.id, r.date_created, r.insights, r.md_incharge, r.md_id,
        r.details, r.status, r.date_modified, r.company_id, r.company_name,r.location, 
        r.file_name, r.score, r.percentile_rank, r.registered_employee, 
        r.health_status, r.compliance_rate, r.programs, r.mean_scores, 
        r.scoreandpercentile, r.report_name, r.company_subscription_id,
        subscription_name
        from reports r 
        left join subscription_company_enrollment sce on r.company_subscription_id = sce.id
        left join subscription_programs sp on sp.shortcode = sce.program_code
        where ${where} order by r.id desc`;
      }

      db.any(query)
        .then(res => {
          resolve(res);
        })
        .catch(error => {
          // error;
          const err = new APIError(
            "Error adding Report " + error,
            httpStatus.BAD_REQUEST
          );
          console.log(error);
          reject(err);
        });
    });
  }

  getReportDefault() {
    return new Promise(function(resolve, reject) {
      //const query = 'select * from reports where company_id = $1 and (status != 1 or status is null)';
      const query = "select * from defaultstable";
      db.any(query)
        .then(res => {
          resolve(res);
        })
        .catch(error => {
          // error;
          const err = new APIError(
            "Error geeting Report dafault",
            httpStatus.BAD_REQUEST
          );
          console.log(error);
          reject(err);
        });
    });
  }

  compliance(param) {
    let qry;
    if (param.company_name == "All") {
      qry = `select * from patient_compliance_view`;
    } else {
      qry = `select * from patient_compliance_view where company = '${param.company_name}'`;
    }

    return new Promise(function(resolve, reject) {
      db.any(qry)
        .then(function(result) {
          resolve(result);
        })
        .catch(function(error) {
          console.log(error);
          const err = new APIError("No Results created!", httpStatus.NOT_FOUND);
          reject(err);
        });
    });
  }

  deleteReport(data) {
    return new Promise((resolve, reject) => {
      db.result("DELETE FROM reports WHERE id = $1", [data.id], r => r.rowCount)
        .then(data => {
          resolve("Report Deleted");
          // data = number of rows that were deleted
        })
        .catch(error => {
          console.log(error);
          const err = new APIError(
            "Error canceling apointment",
            httpStatus.BAD_REQUEST
          );
          reject(err);
        });
    });
  }

  download(req) {
    let qry = `select user_id, name, email, employee_id, company, branch, department, job_grade, high_risk, dod, lower(trim(doh))as doh, age_group, count(*) 
              from (
              select user_id, name, email, employee_id, company, branch, department, job_grade, high_risk, split_part(value, '_', 2) dod, split_part(value, '_', 1) doh, ROW_NUMBER() OVER (PARTITION BY user_id ORDER BY MAX(created_date) DESC ) as rn, age_group 
              from new_answers_with_details `;

    qry += `where company = '${req.params.company_name}' `;

    if (req.params.branch !== "all") {
      qry += `and branch = '${req.params.branch}' `;
    }

    if (req.params.department !== "all") {
      qry += `and department = '${req.params.department}' `;
    }

    if (req.params.high_risk !== "all") {
      qry += `and high_risk = '${req.params.high_risk}' `;
    }

    if (req.params.job_grade !== "all") {
      qry += `and job_grade = '${req.params.job_grade}' `;
    }

    if (req.params.age_group !== "all") {
      qry += `and age_group = '${req.params.age_group}' `;
    }

    qry += `and lower(split_part(value, '_', 1)) in ('weight', 'nutrition', 'detox', 'sleep', 'movement', 'stress', 'other' )
        and question_id = 133
        group by user_id, name, email, employee_id, company, branch, department, job_grade, high_risk, dod, doh, age_group
        ) x
        where rn <= 3
        group by lower(trim(doh ) ), user_id, name, email, employee_id, company, branch, department, job_grade, high_risk, dod, age_group
        order by user_id, lower(trim(doh ) )  `;

    return new Promise((resolve, reject) => {
      db.any(qry)
        .then(data => {
          resolve(data);
        })
        .catch(error => {
          console.log(error);
          const err = new APIError(
            "Error getting query",
            httpStatus.BAD_REQUEST
          );
          reject(err);
        });
    });
  }

  getLeadDoctor(req) {
    let qry = `select * from company_queue  c 
              left join users u on u.id = c.user_id 
              where c.is_lead = true and c.company_id  = '${req.params.id}'`;

    return new Promise(function(resolve, reject) {
      db.one(qry)
        .then(function(result) {
          resolve(result);
        })
        .catch(function(error) {
          const err = new APIError(
            "No Lead Doctor found!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }
}

export default Reports;
