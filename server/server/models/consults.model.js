import httpStatus from 'http-status';
import APIError from '../helpers/APIError'
import bcrypt from 'bcryptjs'
import Promise from 'bluebird'

class Consults {
  constructor() {
    this.SALT_WORK_FACTOR = 10;
  }


/**
 * Get Consults
 * @param {ObjectId} patient_id - The objectId of consults.
 * @returns {Promise<Consults, APIError>}
 */
get(req) {
  let qry = `select c.id, c.employee_id, c.doctor_id, c.status, c.details, c.consult_date, c.last_updated, c.patient_id, c.concern, c.appointment_code, c.callduration, (select value from new_answers where question_id = 1 and user_id = c.patient_id  order by created_date desc limit 1) as birth_date, (select value from new_answers where question_id = 2 and user_id = c.patient_id  order by created_date desc limit 1) as gender, (select name from users where id = c.doctor_id) as md_incharge, u.name, u.company, u.department, u.branch from consultations c left join users u on u.id = c.patient_id  `

  if(req.patient_id && req.patient_id != ''){
    qry += `where c.patient_id = ${req.patient_id} `
  }else if(req.doctor_id && req.doctor_id != ''){
    qry += `where c.doctor_id = ${req.doctor_id} `
  }

  if(req.appointment_code && req.appointment_code != ''){
    qry += ` and c.appointment_code = '${req.appointment_code}' `
  }

  if(req.concern && req.concern != ''){
    qry += ` and c.concern = '${req.concern}' `
  }

  if(req.status && req.status != ''){
    qry += ` and c.status = ${req.status} `
  }

  qry += `order by c.id desc`

  return new Promise(function(resolve, reject) {
    db.any(qry)
    .then(function (consults) {
      resolve(consults)
    })
    .catch(function (error) {
      const err = new APIError('No such Consults exists!', httpStatus.NOT_FOUND);
      reject(err)
    });
  })
}

getConsult(body){

      let qry = `SELECT * FROM consultations where patient_id = ${body.patient_id} `

      if(body.field){
        qry += `AND ${body.field} = '${body.field_value}' `
      }

      qry += `order by consult_date desc `

      return new Promise(function(resolve, reject) {
          db.any(qry)
          .then(function (consults) {
            resolve(consults)
          })
          .catch(function (error) {
            const err = new APIError('No such Consults exists!', httpStatus.NOT_FOUND);
            reject(err)
          });
      })
}

  getAnalyticsData(req) {
    let qry = `select consult_date, SUM(CASE WHEN health_outcome='improving' THEN 1 ELSE 0 END) AS "improving",
    SUM(CASE WHEN health_outcome='notImproving' THEN 1 ELSE 0 END) AS "notImproving",
    SUM(CASE WHEN health_outcome='forImmediateFollowUp' THEN 1 ELSE 0 END) AS "forImmediateFollowUp",
    SUM(CASE WHEN health_outcome='followUpNextMonth' THEN 1 ELSE 0 END) AS "followUpNextMonth",
    SUM(CASE WHEN health_outcome='stalledOrNeedsRecovery' THEN 1 ELSE 0 END) AS "stalledOrNeedsRecovery"
    from (
      select distinct on (c.patient_id) c.details->>'healthoutcome_details' as health_outcome, 
      extract(YEAR FROM c.consult_date) || '.' || extract(month FROM c.consult_date) || '-' ||
          TO_CHAR(c.consult_date, 'Mon_yyyy') as consult_date
      from consultations c
      where c.status=1
        and c.doctor_id= ${req.doctor_id}
      order by c.patient_id, c.id desc
    ) x
    group by consult_date
    order by consult_date`

    return new Promise(function(resolve, reject) {
      db.any(qry)
      .then(function (consults) {
        resolve(consults)
      })
      .catch(function (error) {
        const err = new APIError('getAnalyticsData query error', httpStatus.NOT_FOUND);
        reject(err)
      });
    })
  }


save(data) {
    return new Promise(function(resolve, reject) {
      const query = 'INSERT INTO consultations(appointment_id, patient_id, employee_id, doctor_id, consult_date, details, status, concern, appointment_code, callduration)VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $9) returning employee_id, doctor_id, consult_date, details';

      db.one(query, [data.appointment_id, data.patient_id, data.employee_id, data.doctor_id, data.consult_date, data.details, data.status, data.concern, data.appointment_code, data.callduration])
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          // error;
          const err = new APIError('Error saving Consultation : '+error, httpStatus.BAD_REQUEST);
          console.log(error);
          reject(err);
        });
  })

}

update(data){
    return new Promise(function(resolve, reject) {
    var query = pgp.helpers.update(data.details, null, "consultations") + " WHERE id = "+data.id+" RETURNING appointment_id";
    db.any(query)
      .then((res) => {
        resolve(res);
      })
      .catch((error) => {
        // error;
        const err = new APIError('Error updating consultations', httpStatus.BAD_REQUEST);
        console.log(error);
        reject(err);
      });
})
}

}


export default Consults;
