import httpStatus from "http-status";
import Promise from "bluebird";
import APIError from "../helpers/APIError";

class Concern {
  constructor() {
    this.SALT_WORK_FACTOR = 10;
  }

  get() {
    return new Promise((resolve, reject) => {
      // eslint-disable-next-line no-undef
      db.any(` select * from health_concern order by name`)
        .then(concern => {
          resolve(concern);
        })
        .catch(() => {
          const err = new APIError(
            "No such Concern exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  add(body) {
    return new Promise((resolve, reject) => {
      const query =
        "INSERT INTO health_concern(name, date_added)VALUES ($1, now()) returning name, date_added";

      // eslint-disable-next-line no-undef
      db.one(query, [body.name])
        .then(res => {
          resolve(res);
        })
        .catch(error => {
          // error;
          const err = new APIError(
            `Error adding health_concern : ${error}`,
            httpStatus.BAD_REQUEST
          );
          console.log(error);
          reject(err);
        });
    });
  }
}

export default Concern;
