import httpStatus from 'http-status';
import APIError from '../helpers/APIError'
import bcrypt from 'bcryptjs'
import Promise from 'bluebird'

class Consent {
  constructor() {
    this.SALT_WORK_FACTOR = 10;
  }

  getConsent(email){
    return new Promise(function(resolve, reject) {
      db.one(`SELECT * FROM data_consent where UPPER(trim(email)) = '${email.toUpperCase()}' `)
      .then(function (consent) {
        resolve(consent)
      })
      .catch(function (error) {
        const err = new APIError('No such Consent exists!', httpStatus.NOT_FOUND);
        reject(err)
      });
    })
  }

  update(data){
    return new Promise(function(resolve, reject) {
      var date = new Date();
      var currentDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
      data.details.date_updated = currentDate
    var query = pgp.helpers.update(data.details, null, "data_consent") + " WHERE UPPER(trim(email)) = '"+data.email.toUpperCase()+"' ";
    db.any(query)
      .then((res) => {
        resolve(res);
      })
      .catch((error) => {
        // error;
        const err = new APIError('Error updating consent', httpStatus.BAD_REQUEST);
        console.log(error);
        reject(err);
      });
    })
  }

}


export default Consent;
