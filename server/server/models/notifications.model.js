import Promise from "bluebird";
import httpStatus from "http-status";
import APIError from "../helpers/APIError";
const date_created = new Date();

class Notifications {
  getAdmin(id) {
    // AND (sender ->> 'id' = '${ids[1]}' OR sender ->> 'type' = 'admin' OR sender ->> 'type' = '${ids[2]}')

    let ids = id.split("~");
    let type = ids[2];
    var qry = "";
    if (type !== "Patient") {
      qry = `select id, sender, recipient, date_created, subject, message, is_read, status, read_by from notifications
              where status is null
              AND 
              (
                recipient ->> 'type' = '${type}'
                AND (recipient ->> 'id' = '${
                  ids[0]
                }' OR recipient ->> 'id' = '${type.toLowerCase()}')
              ) `;
    } else {
      qry = `select id, sender, recipient, date_created, subject, message, is_read, status, read_by from notifications
              where status is null
              AND (
              (
                recipient ->> 'type' = 'Patient'
              	AND (recipient ->> 'id' = '${
                  ids[0]
                }' OR recipient ->> 'id' = 'patient')
              )
              OR
              (
                recipient ->> 'type' = 'Patient'
              	AND recipient ->> 'id' = '0' AND sender ->> 'type' = 'hr' AND sender ->> 'id' = '${
                  ids[1]
                }'
              )) `;
    }
    qry += ` 
          AND DATE(date_created) >= (SELECT DATE(created_at) FROM users WHERE id = ${ids[0]})
          order by id DESC`

    return new Promise(function(resolve, reject) {
      db.any(qry)
        .then(function(result) {
          resolve(result); // print answer object;
        })
        .catch(function(error) {
          const err = new APIError(
            "No such Notifications exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  updateAdmin(data) {
    return new Promise(function(resolve, reject) {
      var query =
        pgp.helpers.update(data, null, "notifications") +
        " WHERE id = " +
        data.id +
        " RETURNING id, is_read, date_read";
      db.any(query)
        .then(res => {
          resolve(res);
        })
        .catch(error => {
          // error;
          const err = new APIError(
            "Error updating Notifications",
            httpStatus.BAD_REQUEST
          );
          console.log(error);
          reject(err);
        });
    });
  }

  create_notif_for_hr(report) {
    return new Promise(function(resolve, reject) {
      const query =
        "INSERT INTO notifications(sender, recipient, subject, message, date_created)  VALUES ($1, $2, $3, $4, $5) returning sender, recipient, subject, message";
      db.one(query, [
        report.sender,
        report.data,
        report.subject,
        report.message,
        report.date_created
      ])
        .then(res => {
          resolve(res);
        })
        .catch(error => {
          const err = new APIError(
            "Error submitting Appointment Notification",
            httpStatus.BAD_REQUEST
          );
          console.log(error);
          reject(err);
        });
    });
  }
}

export default Notifications;
