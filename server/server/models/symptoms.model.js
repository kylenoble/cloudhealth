import Promise from 'bluebird'
import httpStatus from 'http-status'
import APIError from '../helpers/APIError'

export default class Symptoms {
  update(msqanalysis) {
    return new Promise(function (resolve, reject) {
      const query = `UPDATE analysis_and_recomendation SET symptoms_review = $1, symptoms_review_date_modified = $2  WHERE user_id = $3`
      db.none(query, [msqanalysis.data, msqanalysis.dateModified, msqanalysis.id])
        .then(res => {
          resolve(res)
        })
        .catch(error => {
          const err = new APIError('Error submitting msq analysis', httpStatus.BAD_REQUEST)
          console.log(error)
          reject(err)
        })
    })
  }

  add(msqanalysis) {
    return new Promise(function (resolve, reject) {
      const query = `UPDATE analysis_and_recomendation SET symptoms_review = $1, symptoms_review_date_created = $2  WHERE user_id = $3`
      db.none(query, [msqanalysis.data, msqanalysis.dateCreated, msqanalysis.id])
        .then(res => {
          resolve(res)
        })
        .catch(error => {
          const err = new APIError('Error submitting msq analysis', httpStatus.BAD_REQUEST)
          console.log(error)
          reject(err)
        })
    })
  }

  create(msqanalysis) {
    return new Promise(function (resolve, reject) {
      const query = `INSERT INTO public.analysis_and_recomendation (user_id, symptoms_review, symptoms_review_date_created)  VALUES ($1, $2, $3)`
      db.none(query, [msqanalysis.id, msqanalysis.data, msqanalysis.dateCreated])
        .then(res => {
          resolve(res)
        })
        .catch(error => {
          const err = new APIError('Error submitting msq analysis', httpStatus.BAD_REQUEST)
          console.log(error)
          reject(err)
        })
    })
  }
}
