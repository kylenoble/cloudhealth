import httpStatus from 'http-status';
import APIError from '../helpers/APIError'
import bcrypt from 'bcryptjs'
import Promise from 'bluebird'

var AccessToken = require('twilio').jwt.AccessToken;
var VideoGrant = AccessToken.VideoGrant;



class Consults {
      constructor() {
          this.SALT_WORK_FACTOR = 10;
      }

      get(body) {
        //  let array_id = id.split('_')
          return new Promise((resolve, reject) => {
            let query = `SELECT appointments.id, (select na.value from new_answers na where na.user_id = appointments.patient_id and na.question_id = 133 order by na.created_date desc limit 1 offset 2)as chief_complaint, users.name, users.id as userid, appointments.datetime, appointments.room_name, appointments.doctor_id, appointments.patient_id, appointments.status, appointments.active, appointments.concern, appointments.date_requested FROM appointments INNER JOIN users ON users.id = appointments.doctor_id where appointments.id = ${body.id}`
            return db.one(query)
            .then((appointment) => {
              var identity = body.user_id+"_"+body.user_name;

              // Create an access token which we will sign and return to the client,
              // containing the grant we just created.
              var token = new AccessToken(
                process.env.TWILIO_ACCOUNT_SID,
                process.env.TWILIO_API_KEY,
                process.env.TWILIO_API_SECRET
              );

              // Assign the generated identity to the token.

              token.identity = identity
              // Grant the access token Twilio Video capabilities.
              var grant = new VideoGrant();
              token.addGrant(grant);
              resolve({
                  identity: token.identity,
                  token: token.toJwt(),
                  data: appointment
              }); // print user object;
            })
            .catch((error) => {
                const err = new APIError('No such appointment', httpStatus.NOT_FOUND);
                reject(err);
            });
          });
      }

}
export default Consults;
