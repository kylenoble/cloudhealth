/* eslint-disable indent */
import Promise from "bluebird";
import httpStatus from "http-status";
import APIError from "../helpers/APIError";

/**
 * Summary Schema
 */
class Summary {
  constructor() {}

  create(params) {
    return new Promise((resolve, reject) => {
      db.task(t => {
        const queries = [];
        params.forEach(summary => {
          // querying against the task protocol:
          queries.push(
            t.none(
              'INSERT INTO summary ("name", "label", "group", "user_id", "value") VALUES (${name}, ${label}, ${group}, ${userId}, ${value})',
              summary
            )
          );
        });
        return t.batch(queries); // settles all queries;
      })
        .then(() => {
          resolve("Summary inserted");
        })
        .catch(error => {
          // error;
          const err = new APIError(
            "Error adding summary",
            httpStatus.BAD_REQUEST
          );
          console.log(error);
          reject(err);
        });
    });
  }

  get(userId, distinct = false) {
    return new Promise((resolve, reject) => {
      let sql = "select * from summary where user_id = $1 order by id asc";
      if (distinct) {
        sql =
          "select distinct on (name) name, label, summary.group, value from summary where user_id = $1 order by name, created_date desc";
      }
      db.any(sql, [userId])
        .then(summary => {
          resolve(summary); // print summary object;
        })
        .catch(error => {
          const err = new APIError(
            "No such summary exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  getGraphData(qryData) {
    let sql = "";
    const company = qryData.company;
    const graph = qryData.graph;
    // const filter = qryData.filter;
    // const filter_column = '';
    // const filter_value = qryData.filter_value;

    if (graph === "healthscore") {
      if (company === "All") {
        sql =
          "SELECT DISTINCT * from healthScore_graph_data h LEFT JOIN user_age_group uag ON uag.user_id = h.user_id";
      } else {
        sql = `SELECT DISTINCT * from healthScore_graph_data h LEFT JOIN user_age_group uag ON uag.user_id = h.user_id WHERE company = '${company}'`;
      }
    } else if (graph === "msq") {
      sql = `SELECT DISTINCT employee_id, branch, department, job_grade, high_risk, t.user_id, age_group, value, u.healthsurvey_status
          FROM
          (
          SELECT DISTINCT ON (user_id) user_id, value::int
          FROM summary a
          WHERE a.name='msq'
          ORDER BY user_id, created_date desc
          ) t
          LEFT JOIN users u ON t.user_id = u.id
          LEFT JOIN user_age_group uag ON uag.user_id = t.user_id
          WHERE company = '${company}'`;
    } else if (graph === "healthscore_determinants") {
      sql = `SELECT DISTINCT branch, department, job_grade, high_risk, f.user_id, name, age_group, value::numeric, healthsurvey_status
          FROM filtered_summary_view f
          LEFT JOIN user_age_group uag ON uag.user_id = f.user_id
          where name in ('sleep', 'weightScore', 'nutrition', 'exercise', 'stress', 'detox')
          and company='${company}'
          order by branch, department, job_grade, high_risk, user_id, name`;
    }

    return new Promise((resolve, reject) => {
      db.any(sql)
        .then(data => {
          resolve(data);
        })
        .catch(error => {
          const err = new APIError("No record found!", httpStatus.NOT_FOUND);
          reject(err);
        });
    });
  }

  getUserRank(user_id, company) {
    let where_company = "";
    if (company !== "All") {
      where_company = ` and company = '${company}'`;
    }
    let qryall = `select ranked.rank, (select count(id) from users where status <> 3 and healthsurvey_status='Locked' ${where_company}) as users_count from (
      select SUM(CAST(value AS FLOAT)), count(*), user_id,
      rank() over (order by SUM(CAST(value AS FLOAT)) asc) as rank
      from filtered_summary_view 
      where name not in ('downstreamHealth', 'bmi', 'whr', 'msq')
      and healthsurvey_status='Locked'
      ${where_company}
      group by user_id, company
      having count(*) = 6
      order by sum asc) ranked
      where user_id=${user_id}`;

    return new Promise((resolve, reject) => {
      db.one(qryall)
        .then(summary => {
          resolve(summary); // print summary object;
        })
        .catch(error => {
          const err = new APIError(
            "No such summary exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  getAll() {
    const qryall = `select s.user_id, s.name, s.value from summary s
  left join users u on u.id = s.user_id
  where u.company in ('Romlas', 'Other', 'LifeScience', 'GMT Manila') and s.id = (select distinct on (s2.name) s2.id from summary s2 where s2.user_id = s.user_id and s2.name = s.name)
  order by u.id, s.id, u.company`;
    return new Promise((resolve, reject) => {
      db.any(qryall)
        .then(summary => {
          resolve(summary); // print summary object;
        })
        .catch(error => {
          console.dir(error);
          const err = new APIError(
            "No such summary exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  saveHealthSurveySummary(body) {
    return new Promise((resolve, reject) => {
      const query = `INSERT INTO healthsurvey_summary(
      user_id, personal_info, readiness, weight, nutrition, sleep, stress, detoxification, movement, symptoms_review, health_issues, health_goals, date_created)
      VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) returning user_id, personal_info, readiness, weight, nutrition, sleep, stress, detoxification, movement, symptoms_review, health_issues, health_goals, date_created`;

      db.one(query, [
        body.user_id,
        body.personal_info,
        body.readiness,
        body.weight,
        body.nutrition,
        body.sleep,
        body.stress,
        body.detoxification,
        body.movement,
        body.symptoms_review,
        body.health_issues,
        body.health_goals,
        body.date_created
      ])
        .then(res => {
          resolve(res);
        })
        .catch(e => {
          console.log(e);
          const err = new APIError(
            "Error inserting to Health Survey",
            httpStatus.BAD_REQUEST
          );
          reject(err);
        });
    });
  }

  getHealthSurveySummary(id) {
    const qry = `SELECT * FROM healthsurvey_summary WHERE user_id = ${id} ORDER BY date_created desc`;

    return new Promise(function(resolve, reject) {
      db.any(qry)
        .then(function(answer) {
          resolve(answer); // print answer object;
        })
        .catch(function(error) {
          const err = new APIError(
            "No such Summary exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }
}

/**
 * @typedef Summary
 */
export default Summary;
