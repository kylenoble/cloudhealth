import Promise from 'bluebird';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';



class Announcements {

      get(str) {
        let strArr = str.split('_')
        var qry = `SELECT an.id, an.title, an.details, an.status, an.date_created, an.created_by, an.read_by, c.company_name, a.name as sender
        FROM announcements an
        left join admin a on an.created_by = a.id
        left join companies c on a.company_id = c.id
        where an.status = 'Active'`
        if(strArr[0] === 'Patients'){
          qry += ` AND ('${strArr[0]}' = ANY (string_to_array(audience, ', '))`
          qry += ` OR '${strArr[1]}' = ANY (string_to_array(audience, ', ')) )`
          qry += ` AND DATE(an.date_created) >= (select date(date_created) from companies where id=${strArr[1]}) `
        } else {
          qry += ` AND '${strArr[0]}' = ANY (string_to_array(audience, ', '))`
        }
        qry += ` ORDER BY an.id DESC`

          return new Promise(function(resolve, reject) {
          db.any(qry)
          .then(function (result) {
            resolve(result) // print answer object;
          })
          .catch(function (error) {
            const err = new APIError('No such Events exists!', httpStatus.NOT_FOUND);
            reject(err)
          });
        })
      }

      update(data){
        return new Promise(function(resolve, reject) {
          var query = pgp.helpers.update(data, null, "announcements") + " WHERE id = "+data.id+" RETURNING id, read_by";
          db.any(query)
            .then((res) => {
              resolve(res);
            })
            .catch((error) => {
              // error;
              const err = new APIError('Error updating events', httpStatus.BAD_REQUEST);
              console.log(error);
              reject(err);
            });
      })
      }
  }




export default Announcements;
