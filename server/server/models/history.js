import httpStatus from "http-status";
import Promise from "bluebird";
import APIError from "../helpers/APIError";

class History {
  saveHistory(id, string) {
    return new Promise((resolve, reject) => {
      const date = new Date();
      const currentDate = `${date.getFullYear()}-${date.getMonth() +
        1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
      const activity = string;
      const description = "dashboard login";
      const query =
        "INSERT INTO account_logs(user_id, activity_date, activity_name, description) VALUES ($1, $2, $3, $4) returning user_id, activity_date, activity_name, description";
      // eslint-disable-next-line no-undef
      db.one(query, [id, currentDate, activity, description])
        .then(resdata => {
          resolve(resdata);
        })
        .catch(() => {
          const err = new APIError(
            "Error inserting to account_logs database",
            httpStatus.BAD_REQUEST
          );
          reject(err);
        });
    });
  }
}

export default History;
