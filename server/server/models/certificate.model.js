import Promise from 'bluebird'
import httpStatus from 'http-status'
import APIError from '../helpers/APIError'

export default class CertificateModel {
  create(certificate) {
    return new Promise(function (resolve, reject) {
      const query =
        'INSERT INTO certificates(user_id, training_name, date_completed, certification_level, verification_status, attachment)  VALUES ($1, $2, $3, $4, $5, $6) returning user_id, training_name, date_completed, certification_level, verification_status, attachment'
      db.one(query, [
        certificate.user_id,
        certificate.name,
        certificate.date,
        certificate.level,
        certificate.status,
        certificate.attachment,
      ])
        .then(res => {
          resolve(res)
        })
        .catch(error => {
          const err = new APIError('Error submitting Ticket', httpStatus.BAD_REQUEST)
          console.log(error)
          reject(err)
        })
    })
  }

  update(id, certificate) {
    return new Promise(function (resolve, reject) {
      const query = `UPDATE certificates SET training_name = $1, date_completed = $2, certification_level = $3, verification_status = $4, attachment = $5  WHERE id = ${id}`
      db.none(query, [
        certificate.name,
        certificate.date,
        certificate.level,
        certificate.status,
        certificate.attachment,
      ])
        .then(res => {
          resolve(res)
        }).catch(err => reject(err))
    })
  }

  deleteCert(id) {
    return new Promise(function (resolve, reject) {
      const query = `DELETE FROM certificates WHERE id = ${id}`
      db.none(query)
        .then(res => {
          resolve(res)
        }).catch(err => reject(err))
    })
  }

  getCertificates() {
    return new Promise(function (resolve, reject) {
      let qry = `SELECT * FROM certificates order by date_completed desc`

      db.any(qry)
        .then(function (result) {
          resolve(result) // print answer object;
        })
        .catch(function (error) {
          const err = new APIError('No Company exist.!', httpStatus.NOT_FOUND)
          reject(err)
        })
    })
  }

  getCertificate(id) {
    return new Promise(function (resolve, reject) {
      let qry = `SELECT * FROM certificates WHERE id = ${id}`

      db.one(qry)
        .then(function (result) {
          resolve(result) // print answer object;
        })
        .catch(function (error) {
          const err = new APIError('No Company exist.!', httpStatus.NOT_FOUND)
          reject(err)
        })
    })
  }
}