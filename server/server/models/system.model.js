import httpStatus from 'http-status';
import APIError from '../helpers/APIError'
import bcrypt from 'bcryptjs'
import Promise from 'bluebird'

class Consults {
  constructor() {
    this.SALT_WORK_FACTOR = 10;
  }


/**
 * Get Consults
 * @param {ObjectId} patient_id - The objectId of consults.
 * @returns {Promise<Consults, APIError>}
 */
get(body) {
  let qry = `select * from system_info where app_code = '${body.app_code}'`

  return new Promise(function(resolve, reject) {
    db.one(qry)
    .then(function (consults) {
      resolve(consults)
    })
    .catch(function (error) {
      const err = new APIError('No such Application exists!', httpStatus.NOT_FOUND);
      reject(err)
    });
  })
}



}


export default Consults;
