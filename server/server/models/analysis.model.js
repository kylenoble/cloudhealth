import Promise from 'bluebird';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';

/**
 * Answer Schema
 */
class Analysis {
  constructor() {

  }

  /**
   * Get answers
   * @param {ObjectId} id - The objectId of user.
   * @param {ObjectId} userId - The objectId of user.
    * @param {ObjectId} questionId - The objectId of user.
   * @param {ObjectId} group - The objectId of user.
   * @returns {Promise<Answer, APIError>}
   */
  get(id) {

    return new Promise(function(resolve, reject) {
      db.one(`SELECT * FROM analysis_and_recomendation where user_id = ${id}`)
      .then(function (result) {
        resolve(result) // print answer object;
      })
      .catch(function (error) {
        const err = new APIError('No such Analysis exists!', httpStatus.NOT_FOUND);
        reject(err)
      });
    })
  }

  save(data) {
    
    if(data.health_tracker){
      return new Promise(function(resolve, reject) {
        const query = 'INSERT INTO analysis_and_recomendation(user_id, health_tracker_date_created, health_tracker)VALUES ($1, $2, $3) returning user_id, health_tracker_date_created, health_tracker';

        db.one(query, [data.user_id, data.health_tracker_date_created, data.health_tracker])
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            // error;
            const err = new APIError('Error saving analysis : '+error, httpStatus.BAD_REQUEST);
            console.log(error);
            reject(err);
          });
    })
    }else if(data.modifiables){
        return new Promise(function(resolve, reject) {
          const query = 'INSERT INTO analysis_and_recomendation(user_id, modifiables_date_created, modifiables)VALUES ($1, $2, $3) returning user_id, modifiables_date_created, modifiables';

          db.one(query, [data.user_id, data.modifiables_date_created, data.modifiables])
            .then((res) => {
              resolve(res);
            })
            .catch((error) => {
              // error;
              const err = new APIError('Error saving analysis : '+error, httpStatus.BAD_REQUEST);
              console.log(error);
              reject(err);
            });
      })
    }else{
        return new Promise(function(resolve, reject) {
          const query = 'INSERT INTO analysis_and_recomendation(user_id, created_date, assimilation, communication, defense_and_repair, biotransformation_and_elimination, energy, transport, structural_integrity, status)VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) returning user_id, assimilation, communication, defense_and_repair, biotransformation_and_elimination, energy, transport, structural_integrity';
          db.one(query, [data.user_id, data.created_date, data.assimilation, data.communication, data.defense_and_repair, data.biotransformation_and_elimination, data.energy, data.transport, data.structural_integrity, data.status])
            .then((res) => {
              resolve(res);
            })
            .catch((error) => {
              // error;
              const err = new APIError('Error saving analysis : '+error, httpStatus.BAD_REQUEST);
              console.log(error);
              reject(err);
            });
      })
  }

  }

  update(data) {
    return new Promise(function(resolve, reject) {
      var query = pgp.helpers.update(data, null, "analysis_and_recomendation") + " WHERE user_id = "+data.user_id+" RETURNING user_id, status";
      db.any(query)
        .then((res) => {
          resolve(res);
        })
        .catch((error) => {
          // error;
          const err = new APIError('Error updating analysis : '+error, httpStatus.BAD_REQUEST);
          console.log(error);
          reject(err);
        });
    })
  }


  //flexible query
  // getQuery(query) {
  //   return new Promise(function(resolve, reject) {
  //     db.any(query)
  //     .then(function (result) {
  //       resolve(result) // print answer object;
  //     })
  //     .catch(function (error) {
  //       const err = new APIError('No Result found in flexible querying!', httpStatus.NOT_FOUND);
  //       reject(err)
  //     });
  //   })
  // }



}


export default Analysis;
