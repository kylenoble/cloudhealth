// import Promise from '../../../../../../AppData/Local/Microsoft/TypeScript/2.9/node_modules/@types/bluebird';
import Promise from 'bluebird'
import httpStatus from 'http-status'
import APIError from '../helpers/APIError'

export default class BookAppNotification {
  create_notif_for_doctor(bookAppNotif) {
    return new Promise(function(resolve, reject) {
      const query =
        'INSERT INTO notifications(sender, recipient, subject, message, date_created)  VALUES ($1, $2, $3, $4, $5) returning sender, recipient, subject, message'
      db.one(query, [bookAppNotif.sender, bookAppNotif.doctor, bookAppNotif.subject, bookAppNotif.message, new Date()])
        .then(res => {
          resolve(res)
        })
        .catch(error => {
          const err = new APIError('Error submitting Appointment Notification', httpStatus.BAD_REQUEST)
          console.log(error)
          reject(err)
        })
    })
  }

  create_notif_for_patient(bookAppNotif) {
    return new Promise(function(resolve, reject) {
      const query =
        'INSERT INTO notifications(sender, recipient, subject, message, date_created)  VALUES ($1, $2, $3, $4, $5) returning sender, recipient, subject, message'
      db.one(query, [bookAppNotif.sender, bookAppNotif.patient, bookAppNotif.subject, bookAppNotif.message, new Date()])
        .then(res => {
          resolve(res)
        })
        .catch(error => {
          const err = new APIError('Error submitting Appointment Notification', httpStatus.BAD_REQUEST)
          console.log(error)
          reject(err)
        })
    })
  }
}
