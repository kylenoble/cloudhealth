/* eslint-disable no-undef */
import httpStatus from "http-status";
import bcrypt from "bcryptjs";
import Promise from "bluebird";
import APIError from "../helpers/APIError";

/**
 * User Schema
 */
class User {
  constructor() {
    this.SALT_WORK_FACTOR = 10;
  }

  /**
   * Create user
   * @param {String} email - The objectId of user.
   * @param {String} password - The objectId of user.
   * @param {String} name - The objectId of user.
   * @param {String} company - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  create(t_email, password, name, company, employeeId, department, branch) {
    const email = t_email.trim(); // remove white spaces
    const that = this;
    return new Promise((resolve, reject) => {
      // ADDED DUPLICATE EMAIL CHECKING if incase database not set eht email column to be unique
      // eslint-disable-next-line no-undef
      db.any("select email from users where email=$1", email)
        .then(res => {
          if (res.length > 0) {
            // duplicate email found stop from creating account
            const err = new APIError(
              "That email is already in use!",
              httpStatus.NOT_FOUND
            );
            reject(err);
          } else {
            // CONTINUE SAVING
            that
              .generateHash(password)
              .then(hash => {
                const query =
                  "INSERT INTO users(email, password, name, company, employee_id, department, branch) VALUES($1, $2, $3, $4, $5, $6, $7) returning id, name, email, type, employee_id, department, branch";
                // eslint-disable-next-line no-undef
                db.one(query, [
                  email,
                  hash,
                  name,
                  company,
                  employeeId,
                  department,
                  branch
                ])
                  .then(data => {
                    resolve(data);
                  })
                  .catch(error => {
                    // error;
                    let err;
                    if (
                      error.detail === `Key (email)=(${email}) already exists.`
                    ) {
                      err = new APIError(
                        "This email is already in use",
                        httpStatus.BAD_REQUEST
                      );
                    } else {
                      console.log(error);
                      err = new APIError(
                        "Error creating account",
                        httpStatus.BAD_REQUEST
                      );
                    }
                    reject(err);
                  });
              })
              .catch(error => {
                reject(error);
              });
          }
        })
        .catch(error => {
          console.log(error);
          const err = new APIError(
            "Error Running Query!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
      //END
    });
  }

  saveToMonitoring(id, action = "changed") {
    return new Promise((resolve, reject) => {
      const date = new Date();
      const currentDate = `${date.getFullYear()}-${date.getMonth() +
        1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
      const status = 1;
      const query =
        "INSERT INTO password_monitoring(user_id, date_modified, action_taken, status) VALUES ($1, $2, $3, $4) returning user_id, date_modified, action_taken, status";
      db.one(query, [id, currentDate, action, status])
        .then(resdata => {
          resolve(resdata);
        })
        .catch(error => {
          console.log(error);
          const err = new APIError(
            "Error inserting to password_monitoring database",
            httpStatus.BAD_REQUEST
          );
          reject(err);
        });
    });
  }

  saveHistory(id, string) {
    return new Promise((resolve, reject) => {
      const date = new Date();
      const currentDate = `${date.getFullYear()}-${date.getMonth() +
        1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
      const activity = string;
      const description = "dashboard login";
      const query =
        "INSERT INTO account_logs(user_id, activity_date, activity_name, description) VALUES ($1, $2, $3, $4) returning user_id, activity_date, activity_name, description";
      db.one(query, [id, currentDate, activity, description])
        .then(resdata => {
          resolve(resdata);
        })
        .catch(error => {
          console.log(error);
          const err = new APIError(
            "Error inserting to account_logs database",
            httpStatus.BAD_REQUEST
          );
          reject(err);
        });
    });
  }

  monitorPassword(id) {
    // PREPARING FOR FLEXIBLE GET
    return new Promise((resolve, reject) => {
      db.any(
        `select m.id, m.user_id, m.date_modified, m.action_taken, m.status,(select activity_date from account_logs where user_id = m.user_id and activity_name = 'login' order by activity_date desc limit 1) as last_login, (select count(user_id) from account_logs where user_id = m.user_id) as login_count from password_monitoring m where m.user_id = $1 order by id desc limit 1`,
        id
      )
        .then(user => {
          resolve(user); // print user object;
        })
        .catch(() => {
          const err = new APIError(
            "No such user exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  /**
   * Get by field
   * @param {ObjectId} field - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  getData(field, value) {
    // PREPARING FOR FLEXIBLE GET
    return new Promise((resolve, reject) => {
      db.any(
        `select u.id, u.name, u.email, u.type, u.company, u.department, u.branch, u.employee_id, u.avatar, u.status,
              u.job_grade, case when u.high_risk = 1 then 'High Value' when u.high_risk = 2 then 'High Risk' else 'Others' END as high_risk,
              healthsurvey_status,
              (select distinct on (q.label) value answer
              from new_answers a
              inner join new_questions q on a.question_id = q.id
              where user_id = u.id and q.label = 'Gender'
              order by q.label, created_date desc) as Gender,
              (select distinct on (q.label) value answer
              from new_answers a
              inner join new_questions q on a.question_id = q.id
              where user_id = u.id and q.label = 'Birthday'
              order by q.label, created_date desc) as Birthday
              from users u where u.$1~ = $2
              and (u.status != 3 or u.status is null)
              `,
        [field, value]
      )
        .then(user => {
          resolve(user); // print user object;
        })
        .catch(() => {
          const err = new APIError(
            "No such user exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    // PREPARING FOR FLEXIBLE GET
    // added telemedicine_enabled column
    return new Promise((resolve, reject) => {
      // const query = `select c.code as company_code, c.telemedicine_enabled, c.id as company_id,  u.id, u.name, u.email, u.type, u.company, c.is_active, u.department, u.branch, u.employee_id, u.avatar, u.status, u.healthsurvey_status, is_admin, admin_access_token, expertise,
      // (select distinct on (q.label) value answer
      // from new_answers a
      // inner join new_questions q on a.question_id = q.id
      // where user_id = u.id and q.label = 'Gender'
      // order by q.label, created_date desc) as Gender,
      // (select distinct on (q.label) value answer
      // from new_answers a
      // inner join new_questions q on a.question_id = q.id
      // where user_id = u.id and q.label = 'Birthday'
      // order by q.label, created_date desc) as Birthday
      // from users u left join companies c on u.company = c.company_name where u.id = $1`

      const query = `select c.code as company_code,
      case when c.telemedicine_enabled=true
      then (select enrollment_status from subscription_employee_enrollment ee
            left join subscription_company_enrollment ce on ee.company_enrollment_id = ce.id
            where user_id=u.id and ce.company_id=c.id and program_code='TeleMed') END as telemedicine_enabled,
            c.id as company_id,  u.id, u.name, u.email, u.type, u.company, c.is_active, u.department, u.branch, u.employee_id, 
            u.avatar, u.status, u.healthsurvey_status, is_admin, admin_access_token, expertise,
            (select distinct on (question_id) value answer
            from new_answers a
            where user_id = u.id and question_id = 2
            order by question_id, created_date desc) as Gender,
            age
            from users u 
            LEFT JOIN user_age_group uag ON uag.user_id=u.id
            left join companies c on u.company = c.company_name where u.id = $1`;

      db.one(query, id)
        .then(user => {
          resolve(user); // print user object;
        })
        .catch(() => {
          const err = new APIError(
            "No such user exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  getPatientInfo(string) {
    let where;
    let val;
    if (isNaN(string)) {
      const strArr = string.split("_");
      val = strArr[0].toLowerCase();
      where = ` WHERE lower(u.name) like '%${val}%'`;
      if (strArr[1] && strArr[2] && strArr[3]) {
        const operator = {
          eq: "=",
          gt: ">",
          lt: "<",
          gte: ">=",
          lte: "<=",
          n: "isNull",
          nn: "notNull"
        };
        where += ` and ${strArr[1]} ${operator[strArr[2]]} '${strArr[3]}'`;
      }
    } else {
      val = string;
      where = ` WHERE u.employee_id = '${val}'`;
    }

    const qry = `select u.id, u.name, u.email, u.type, u.company, u.department, 
                u.branch, u.employee_id, u.avatar, u.status,
                (select distinct on (question_id) value answer
                from new_answers a
                where user_id = u.id and question_id = 2
                order by question_id, created_date desc) as Gender,
                age
                from users u  
                LEFT JOIN user_age_group uag ON uag.user_id=u.id
                ${where} limit 10`;

    // PREPARING FOR FLEXIBLE GET
    return new Promise((resolve, reject) => {
      db.any(qry)
        .then(user => {
          resolve(user); // print user object;
        })
        .catch(() => {
          const err = new APIError(
            "No such user exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  saveHealthDocumentInfo(req) {
    const data = req.body;
    const filename = data.prefix + data.filename;
    return new Promise((resolve, reject) => {
      const query = `INSERT into health_documents (user_id, filename, date_uploaded, description) values($1, $2, $3, $4) returning user_id, filename, date_uploaded`;
      db.one(query, [
        data.user_id,
        filename,
        data.date_uploaded,
        data.description
      ])
        .then(res => {
          resolve(res);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  saveOtherInfo(data) {
    return new Promise((resolve, reject) => {
      const query =
        "insert into other_health_info (user_id, doctor_name, specialization, hospital_clinic, activity_name, place, frequency, tag, created_date) values($1, $2, $3, $4, $5, $6, $7, $8, $9) returning user_id, doctor_name, specialization, hospital_clinic, activity_name, place, frequency, tag, created_date";
      db.one(query, [
        data.user_id,
        data.doctor_name,
        data.specialization,
        data.hospital_clinic,
        data.activity_name,
        data.place,
        data.frequency,
        data.tag,
        data.created_date
      ])
        .then(res => {
          resolve(res);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  updateOtherInfo(data) {
    return new Promise(function(resolve, reject) {
      var query =
        pgp.helpers.update(data, null, "other_health_info") +
        " WHERE id = " +
        data.id +
        " RETURNING id";
      db.one(query)
        .then(res => {
          resolve(res);
        })
        .catch(error => {
          // error;
          const err = new APIError(
            "Error updating Other health info",
            httpStatus.BAD_REQUEST
          );
          console.log(error);
          reject(err);
        });
    });
  }

  deleteOtherInfo(data) {
    return new Promise(function(resolve, reject) {
      const qry = `delete from other_health_info where id = ${data.id}`;
      db.any(qry)
        .then(res => {
          resolve(res); // print user object;
        })
        .catch(() => {
          const err = new APIError(
            "Error Deleteing Other Health Info!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  getOtherInfo(req) {
    return new Promise((resolve, reject) => {
      // db.any('select id, name, email, type, company, department, branch, employee_id, avatar from users where type=$1 and company=$2', [type, company])

      const qry = `select * from other_health_info where user_id = ${req.params.userId}`;
      db.any(qry)
        .then(res => {
          resolve(res); // print user object;
        })
        .catch(() => {
          const err = new APIError(
            "No such Info exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  getHealthDocs(req) {
    return new Promise((resolve, reject) => {
      const qry = `select * from health_documents where user_id = ${req.params.userId}`;
      db.any(qry)
        .then(res => {
          resolve(res); // print user object;
        })
        .catch(e => {
          const err = new APIError(
            "No such Info exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  deleteFile(data) {
    var fs = require("fs");
    return new Promise(function(resolve, reject) {
      const qry = `delete from health_documents where id = ${data.id} and user_id = ${data.user_id}`;

      // delete actual file
      return fs.unlink(
        "../client/static/healthdocs/" + data.file_name,
        function(err) {
          if (err) {
            return console.log(err);
          }
          return db
            .any(qry)
            .then(res => {
              resolve(res); // print user object;
            })
            .catch(() => {
              const err = new APIError(
                "Error Deleteing File Record!",
                httpStatus.NOT_FOUND
              );
              reject(err);
            });
        }
      );
    });
  }

  getDoctors(body) {
    const company_id = body.company_id;

    return new Promise((resolve, reject) => {
      //let qry = `select id, name, email, type, company, department, branch, employee_id, avatar, status, expertise from users `;
      const qry = `select u.id, u.name, u.email, u.type, u.company, u.department, u.branch, u.employee_id, u.avatar, u.status, u.expertise from users u
      join company_queue cq on u.id = cq.user_id
      where company_id = ${company_id} 
      and u.type <> 'Patient'
      and (u.status <> 3 or u.status isNull) order by name`;

      db.any(qry)
        .then(doctors => {
          resolve(doctors); // print user object;
        })
        .catch(() => {
          const err = new APIError(
            "No such user exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  getUserFilters(company) {
    return new Promise((resolve, reject) => {
      const sql = `select distinct on (branch, department, job_grade, high_risk) branch, department, job_grade, high_risk
            from users
            where company=$1
            and (
                (branch != '' and branch is not null)
                or (department != '' and department is not null)
                or (job_grade != '' and job_grade is not null)
                or high_risk is not null
                )
            order by branch, department, job_grade, high_risk`;

      db.any(sql, company)
        .then(filters => {
          resolve(filters); // print user object;
        })
        .catch(() => {
          const err = new APIError("No record found.", httpStatus.NOT_FOUND);
          reject(err);
        });
    });
  }

  getMDInfoByToken(token) {
    return new Promise((resolve, reject) => {
      const sql = `select id, email, password
            from users
            where is_admin=true AND admin_access_token='${token}'`;
      db.one(sql)
        .then(md_admin => {
          resolve(md_admin); // print user object;
        })
        .catch(() => {
          const err = new APIError("No record found.", httpStatus.NOT_FOUND);
          reject(err);
        });
    });
  }

  checkIfvalidPassword(id, password) {
    return new Promise((resolve, reject) => {
      db.one("select id, password from users where id=$1", id)
        .then(user => {
          bcrypt.compare(password, user.password, (error, isMatch) => {
            if (error) {
              const err = new APIError(error, httpStatus.UNAUTHORIZED);
              console.log(error);
              reject(err);
            }

            if ((password, isMatch)) {
              resolve({
                authenticated: true
              });
            } else {
              const err = new APIError(
                "Incorrect Information. Please check your password.",
                httpStatus.UNAUTHORIZED
              );
              reject(err);
            }
          });
        })
        .catch(error => {
          let errorMessage =
            "Incorrect Login Information. Please check your email/password.";
          const err = new APIError(errorMessage, httpStatus.UNAUTHORIZED);
          reject(err);
        });
    });
  }

  changePassword(inputs) {
    return new Promise((resolve, reject) => {
      const params = inputs;
      const data = { password: params.password };

      this.checkIfvalidPassword(params.userId, params.currentPassword)
        .then(res => {
          if (res.authenticated) {
            return this.update(params.userId, data)
              .then(response => {
                this.saveToMonitoring(response.id);
                resolve(response);
              })
              .catch(error => {
                console.log(error);
                reject(err);
              });
          }
          //  else {
          //   resolve(res);
          // }
        })
        .catch(err => {
          reject(err);
        });
    });
    //END
  }

  updateAdminPassword(user_data) {
    return new Promise((resolve, reject) => {
      const query = `UPDATE admin SET password = $1  WHERE email = $2`;
      db.none(query, [user_data.password, user_data.email])
        .then(res => {
          resolve(res);
        })
        .catch(err => reject(err));
    });
  }

  /**
   * Update user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  update(userId, inputs) {
    const that = this;
    return new Promise((resolve, reject) => {
      if (inputs.password) {
        that
          .generateHash(inputs.password)
          .then(hash => {
            // eslint-disable-next-line no-param-reassign
            inputs.password = hash;
            const query = `${pgp.helpers.update(
              inputs,
              null,
              "users"
            )} WHERE id = ${userId} RETURNING id, email, password, name, type, company, employee_id, department, branch, avatar`;
            return db
              .one(query)
              .then(user => {
                if (user.type == "Doctor") {
                  const date = new Date();
                  const currentDate = `${date.getFullYear()}-${date.getMonth() +
                    1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;

                  let data_update = { status: 1, date_modified: currentDate };
                  that.update(userId, data_update);
                }
                resolve(user); // return success;
              })
              .catch(error => {
                console.log(error);
                const err = new APIError(
                  "Error updating user",
                  httpStatus.BAD_REQUEST
                );
                reject(err);
              });
          })
          .catch(error => {
            reject(error);
          });
      } else {
        const query = `${pgp.helpers.update(
          inputs,
          null,
          "users"
        )} WHERE id = ${userId} RETURNING id, email, name, type, company, employee_id, department, branch, avatar`;

        return db
          .one(query)
          .then(user => {
            resolve(user); // return success;
          })
          .catch(error => {
            console.log(error);
            const err = new APIError(
              "Error updating user",
              httpStatus.BAD_REQUEST
            );
            reject(err);
          });
      }
    });
  }

  /**
   * login user
   * @param {String} email - The objectId of user.
   * @param {String} password - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  login(email, password, auto_id) {
    const that = this;
    return new Promise((resolve, reject) => {
      db.one(
        "select id, email, password from users where LOWER(email) = LOWER($1)",
        email
      )
        .then(user => {
          if (auto_id && auto_id > 0) {
            that.saveHistory(user.id, "login");
            resolve({
              authenticated: true,
              id: user.id,
              password: user.password,
              type: user.type,
              name: user.name
            });
          } else {
            bcrypt.compare(password, user.password, (error, isMatch) => {
              if (error) {
                const err = new APIError(error, httpStatus.UNAUTHORIZED);
                console.log(error);
                reject(err);
              }

              if ((password, isMatch)) {
                that.saveHistory(user.id, "login");
                resolve({
                  authenticated: true,
                  id: user.id,
                  password: user.password,
                  type: user.type,
                  name: user.name
                });
              } else {
                const err = new APIError(
                  "Incorrect Login Information. Please check your email/password.",
                  httpStatus.UNAUTHORIZED
                );
                reject(err);
              }
            });
          }
        })
        .catch(error => {
          let errorMessage =
            "Incorrect Login Information. Please check your email/password.";
          const err = new APIError(errorMessage, httpStatus.UNAUTHORIZED);
          reject(err);
        });
    });
  }

  saveActivity(data) {
    return new Promise((resolve, reject) => {
      const query =
        "insert into user_activities (user_id, action, details) values($1, $2, $3) returning user_id, action, details";
      db.one(query, [data.user_id, data.action, JSON.stringify(data.details)])
        .then(res => {
          resolve(res);
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  generateSalt() {
    const that = this;

    return new Promise((resolve, reject) => {
      bcrypt.genSalt(that.SALT_WORK_FACTOR, (err, salt) => {
        if (err) reject(err);

        resolve(salt);
      });
    });
  }

  generateHash(password) {
    const that = this;

    return new Promise((resolve, reject) => {
      that
        .generateSalt()
        .then(salt => {
          // hash the password using our new salt
          bcrypt.hash(password, salt, (err, hash) => {
            if (err) reject(err);

            resolve(hash);
          });
        })
        .catch(error => {
          reject(error);
        });
    });
  }

  getAdmin(id) {
    // PREPARING FOR FLEXIBLE GET
    return new Promise((resolve, reject) => {
      db.any(`SELECT * FROM admin a where a.company_id = ${id}`)
        .then(user => {
          resolve(user); // print user object;
        })
        .catch(() => {
          const err = new APIError(
            "No such user exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  getAdminByID(id) {
    // PREPARING FOR FLEXIBLE GET
    return new Promise((resolve, reject) => {
      db.any(`SELECT * FROM admin a where a.id = ${id}`)
        .then(user => {
          resolve(user); // print user object;
        })
        .catch(() => {
          const err = new APIError(
            "No such user exists!",
            httpStatus.NOT_FOUND
          );
          reject(err);
        });
    });
  }

  updateMDProfile(id, profile) {
    const { email, name, company, expertise } = profile;
    return new Promise(function(resolve, reject) {
      const query = `UPDATE users SET email = $1, name = $2, company = $3, expertise = $4 WHERE id = ${id}`;
      db.none(query, [email, name, company, expertise])
        .then(res => {
          resolve({ updated: true });
        })
        .catch(err => reject(err));
    });
  }

  authMDPassword(auth) {
    return new Promise(function(resolve, reject) {
      db.one(
        "SELECT id, email, password from users where LOWER(email) = LOWER($1)",
        auth.email
      )
        .then(user => {
          bcrypt.compare(auth.password, user.password, (error, isMatch) => {
            if ((auth.password, isMatch)) {
              resolve({
                authenticated: true,
                id: user.id,
                name: user.name
              });
            } else {
              const err = new APIError(
                "Incorrect Password. Please check your password.",
                httpStatus.UNAUTHORIZED
              );
              reject(err);
            }
          });
        })
        .catch(err => reject(err));
    });
  }
}

/**
 * @typedef User
 */
export default User;
