import express from 'express';
import concernCtrl from '../controllers/concern.controller';
require('dotenv').config();

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
    .get(concernCtrl.get);

// router.route('/update')
//     .post(concernCtrl.update);
//
router.route('/add')
    .post(concernCtrl.add);

export default router;
