import express from "express";
import expressJwt from "express-jwt";
import appointmentCtrl from "../controllers/appointment.controller";

require("dotenv").config();

const router = express.Router(); // eslint-disable-line new-cap

router
  .route("/")
  /** GET /api/answers - Get list of answers */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), appointmentCtrl.get)

  /** POST /api/answers - Create new answer */
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), appointmentCtrl.create);

router.route("/cancel").put(appointmentCtrl.cancel);

router
  .route("/:appointmentId")
  /** GET /api/answers/:answerId - Get answer */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), appointmentCtrl.get)

  /** PUT /api/answers/:answerId - Update answer */
  .put(expressJwt({ secret: process.env.JWT_TOKEN }), appointmentCtrl.update);

router.route("/history").post(appointmentCtrl.saveAppointmentHistory);

export default router;
