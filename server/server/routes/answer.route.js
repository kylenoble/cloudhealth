import express from "express";
import validate from "express-validation";
import paramValidation from "../../config/param-validation";
import answerCtrl from "../controllers/answer.controller";
import expressJwt from "express-jwt";
require("dotenv").config();

const router = express.Router(); // eslint-disable-line new-cap

router
  .route("/")
  /** GET /api/answers - Get list of answers */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), answerCtrl.list)

  /** POST /api/answers - Create new answer */
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), answerCtrl.create);

//flexible querying
router
  .route("/qry")
  /** PUT /api/answers/update - Update answer */
  .put(answerCtrl.getQuery);

router
  .route("/update")
  /** PUT /api/answers/update - Update answer */
  .put(expressJwt({ secret: process.env.JWT_TOKEN }), answerCtrl.update);

router
  .route("/:answerId")
  /** GET /api/answers/:answerId - Get answer */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), answerCtrl.get)

  /** PUT /api/answers/:answerId - Update answer */
  .put(
    expressJwt({ secret: process.env.JWT_TOKEN }),
    validate(paramValidation.updateAnswer),
    answerCtrl.update
  );

router
  .route("/:group")
  /** GET /api/answers/:answerId - Get answer */
  .get(
    expressJwt({ secret: process.env.JWT_TOKEN }),
    validate(paramValidation.getAnswerGroup),
    answerCtrl.get
  );

/** Load answer when API with answerId route parameter is hit */
// router.param('answerId', answerCtrl.load);

router
  .route("/:userId")
  /** GET /api/answers/:userId - Get core imbalances scores by userID */
  .get(
    expressJwt({ secret: process.env.JWT_TOKEN }),
    answerCtrl.getCoreImbalancesData
  );

router
  .route("/graphdata")
  /** GET /api/users/:userId - Get user */
  .post(answerCtrl.getGraphData);

router
  .route("/getPieChartData/:question_id/:company_name")
  .get(
    expressJwt({ secret: process.env.JWT_TOKEN }),
    answerCtrl.getPieChartData
  );

export default router;
