import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import videoCallCtrl from '../controllers/videoCall.controller';
import expressJwt from 'express-jwt';
require('dotenv').config();

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  .post(videoCallCtrl.get);

export default router;
