import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import systemCtrl from '../controllers/system.controller';
import expressJwt from 'express-jwt';
require('dotenv').config();

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/users - Get list of users */
  .post(systemCtrl.get)



export default router;
