import express from "express";
import validate from "express-validation";
import paramValidation from "../../config/param-validation";
import reportsCtrl from "../controllers/reports.controller";
import expressJwt from "express-jwt";
require("dotenv").config();

const router = express.Router();

router
  .route("/update")
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), reportsCtrl.saveReport)
  .put(expressJwt({ secret: process.env.JWT_TOKEN }), reportsCtrl.updateReport);

router
  .route("/delete")
  .post(
    expressJwt({ secret: process.env.JWT_TOKEN }),
    reportsCtrl.deleteReport
  );

router
  .route("/get")
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), reportsCtrl.getReport);

router
  .route("/defaults")
  .get(
    expressJwt({ secret: process.env.JWT_TOKEN }),
    reportsCtrl.getReportDefault
  );

router
  .route("/compliance/:company_name")
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), reportsCtrl.compliance);

router
  .route(
    "/download/:branch/:department/:high_risk/:job_grade/:age_group/:company_name/:filename"
  )
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), reportsCtrl.download);

router
  .route("/get_lead_doctor/:id")
  .get(
    expressJwt({ secret: process.env.JWT_TOKEN }),
    reportsCtrl.getLeadDoctor
  );

export default router;
