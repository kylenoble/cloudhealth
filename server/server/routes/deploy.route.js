import express from 'express';
import spawner from 'child_process';

const deployRouter = express.Router(); // eslint-disable-line new-cap

deployRouter.route('/')
  .post((req, res) => {
    const spawn = spawner.spawn;
    const deploy = spawn('sh', ['../deploy.sh']);

    deploy.stdout.on('data', (data) => {
      console.log(` ${data}`);
    });

    deploy.on('close', function (code) {
      console.log(`Child process exited with code ${code}`);
    });
    res.status(200).json({
      message: 'Bitbucket Hook received!'
    });
  });

export default deployRouter;
