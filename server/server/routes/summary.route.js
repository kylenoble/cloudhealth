import express from "express";
import validate from "express-validation";
import paramValidation from "../../config/param-validation";
import summaryCtrl from "../controllers/summary.controller";
import expressJwt from "express-jwt";
require("dotenv").config();

const router = express.Router(); // eslint-disable-line new-cap

router
  .route("/filtered")
  /** GET /api/summary - Get list of summaries */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), summaryCtrl.getUserRank);

router
  .route("/all")
  /** GET /api/summary - Get list of summaries */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), summaryCtrl.getAll);

router
  .route("/")
  /** GET /api/summary - Get list of summaries */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), summaryCtrl.get)

  /** POST /api/summary - Create new summary */
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), summaryCtrl.create);

router
  .route("/update")
  /** PUT /api/summary/update - Update summary */
  .put(expressJwt({ secret: process.env.JWT_TOKEN }), summaryCtrl.update);

router
  .route("/graphdata")
  /** GET /api/graphdata - Get list of summaries */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), summaryCtrl.getGraphData);

router.route("/healthsurvey/:id").get(summaryCtrl.getHealthSurveySummary);
router.route("/healthsurvey").post(summaryCtrl.saveHealthSurveySummary);

export default router;
