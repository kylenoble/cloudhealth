import express from 'express'
import certCtrl from '../controllers/certificate.controller'
import expressJwt from 'express-jwt'
require('dotenv').config()

const router = express.Router() // eslint-disable-line new-cap

router.route('/').post(certCtrl.create)

router.route('/update/:id').put(certCtrl.update)

router.route('/delete/:id').put(certCtrl.deleteCert)

router.route('/').get(certCtrl.getCertificates)

router.route('/:id').get(certCtrl.getCertificate)

router.route('/uploadAttachment').post(certCtrl.uploadAttachment)

router.route('/removeAttachment/:file').get(certCtrl.removeAttachent)

export default router