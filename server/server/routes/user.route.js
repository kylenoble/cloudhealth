import express from "express";
import validate from "express-validation";
import paramValidation from "../../config/param-validation";
import userCtrl from "../controllers/user.controller";
import expressJwt from "express-jwt";
require("dotenv").config();

const router = express.Router(); // eslint-disable-line new-cap

router
  .route("/")
  /** GET /api/users - Get list of users */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), userCtrl.list)

  /** POST /api/users - Create new user */
  .post(validate(paramValidation.createUser), userCtrl.create);

router
  .route("/doctors")
  /** GET /api/users/:userId - Get user */
  .put(expressJwt({ secret: process.env.JWT_TOKEN }), userCtrl.getDoctors);

router
  .route("/data")
  /** GET /api/users/:userId - Get user */
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), userCtrl.getData);

router
  .route("/monitor")
  /** GET /api/users/monitor pasword*/
  .post(
    expressJwt({ secret: process.env.JWT_TOKEN }),
    userCtrl.monitorPassword
  );

router
  .route("/saveToMonitoring")
  .post(express({ secret: process.env.JWT_TOKEN }), userCtrl.saveToMonitoring);

router
  .route("/activity")
  /** GET /api/users/monitor pasword
    JWT not Applicable here
    */
  .post(userCtrl.saveActivity);

router.route("/updateAdminPassword").put(userCtrl.updateAdminPassword);

router
  .route("/changePassword")
  /** GET /api/users/:userId - Get user */
  .put(userCtrl.changePassword);

router
  .route("/info/:userId")
  /** GET /api/users/:userId - Get user */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), userCtrl.getPatientInfo);

router
  .route("/:userId")
  /** GET /api/users/:userId - Get user */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), userCtrl.get)

  /** PUT /api/users/:userId - Update user */
  .put(expressJwt({ secret: process.env.JWT_TOKEN }), userCtrl.update)

  /** DELETE /api/users/:userId - Delete user */
  .delete(expressJwt({ secret: process.env.JWT_TOKEN }), userCtrl.remove);

router
  .route("/filter/:company")
  /** GET /api/filter/:company */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), userCtrl.getUserFilters);

router.route("/token/:token").get(userCtrl.getMDInfoByToken);

router.route("/getAdmin/:id").get(userCtrl.getAdmin);
router.route("/getAdminByID/:id").get(userCtrl.getAdminByID);

router
  .route("/health/:userId")
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), userCtrl.getOtherInfo);

router
  .route("/saveOtherHealthInfo")
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), userCtrl.saveOtherInfo);

router
  .route("/updateOtherHealthInfo")
  .post(
    expressJwt({ secret: process.env.JWT_TOKEN }),
    userCtrl.updateOtherInfo
  );

router
  .route("/deleteOtherHealthInfo")
  .post(
    expressJwt({ secret: process.env.JWT_TOKEN }),
    userCtrl.deleteOtherInfo
  );

router
  .route("/healthdocs/:userId")
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), userCtrl.getHealthDocs);

router
  .route("/deleteFile")
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), userCtrl.deleteFile);

router.route("/updateMDProfile/:id").put(userCtrl.updateMDProfile);

router.route("/authMDPassword").post(userCtrl.authMDPassword);

/** Load user when API with userId route parameter is hit */
// router.param('userId', userCtrl.load);

export default router;
