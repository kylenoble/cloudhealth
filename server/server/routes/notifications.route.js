import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import notificationsCtrl from '../controllers/notifications.controller';
import expressJwt from 'express-jwt';
require('dotenv').config();

const router = express.Router(); // eslint-disable-line new-cap

router.route('/admin/:id')
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), notificationsCtrl.getAdmin);

router.route('/admin/update')
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), notificationsCtrl.updateAdmin)

// send report notification
router.route('/sendReportNotification').post(notificationsCtrl.sendReportNotification)

export default router;
