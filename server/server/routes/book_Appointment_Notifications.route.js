import express from 'express'
import bookAppNotifCtrl from '../controllers/book_Appointment_Notification.controller'
import EmailCtrl from '../controllers/email.controller'
import expressJwt from 'express-jwt'
require('dotenv').config()

const router = express.Router() // eslint-disable-line new-cap

router.route('/').post(bookAppNotifCtrl.create)
router.route('/bookAppNotifEmail').post(EmailCtrl.emailTemplate)

export default router
