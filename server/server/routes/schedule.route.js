import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import expressJwt from 'express-jwt';
require('dotenv').config()

import scheduleCtrl from '../controllers/schedule.controller'

const router = express.Router()

router.route('/get')
 .post(scheduleCtrl.get);

router.route('/')
  .post(scheduleCtrl.create);

router.route('/update')
  .post(scheduleCtrl.update)


export default router
