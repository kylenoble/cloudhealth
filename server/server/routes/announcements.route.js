import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import announcementsCtrl from '../controllers/announcements.controller';
import expressJwt from 'express-jwt';
require('dotenv').config();

const router = express.Router(); // eslint-disable-line new-cap

router.route('/:str')
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), announcementsCtrl.get);

router.route('/update')
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), announcementsCtrl.update)

export default router;
