import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import consultsCtrl from '../controllers/consults.controller';
import expressJwt from 'express-jwt';
require('dotenv').config();

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
    .post(consultsCtrl.get);

router.route('/last')
    .post(consultsCtrl.getConsult);

router.route('/save')
    .post(consultsCtrl.save);

router.route('/update')
    .post(consultsCtrl.update);

router.route('/:patient_id')
    .get(consultsCtrl.get)

router.route('/analytics')
    .post(consultsCtrl.getAnalyticsData)


export default router;
