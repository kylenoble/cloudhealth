import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import eventsCtrl from '../controllers/events.controller';
import expressJwt from 'express-jwt';
require('dotenv').config();

const router = express.Router(); // eslint-disable-line new-cap

router.route('/:str/:userid')
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), eventsCtrl.get);

router.route('/update')
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), eventsCtrl.update)

export default router;
