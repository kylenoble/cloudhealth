import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import timelineCtrl from '../controllers/timeline.controller';
import expressJwt from 'express-jwt';
require('dotenv').config();

const router = express.Router(); // eslint-disable-line new-cap

router.route('/:id')
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), timelineCtrl.get);

router.route('/update')
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), timelineCtrl.update)

export default router;
