import express from 'express'
import ticketCtrl from '../controllers/ticket.controller'
import expressJwt from 'express-jwt'
require('dotenv').config()

const router = express.Router() // eslint-disable-line new-cap

router.route('/').post(ticketCtrl.create)

router.route('/ticketEmail').post(ticketCtrl.sendEmail)

router.route('/uploadAttachment').post(ticketCtrl.uploadAttachment)

router.route('/removeAttachment/:file').get(ticketCtrl.removeAttachent)

router.route('/getTicketReplies/:tnum').get(ticketCtrl.getReplies)

router.route('/getTickets/:tnum').get(ticketCtrl.getTickets)

router.route('/getLatestTicket').get(ticketCtrl.getLatestTicket)

router.route('/getCompanies').get(expressJwt({ secret: process.env.JWT_TOKEN }), ticketCtrl.getCompanies)

router.route('/getActiveCompanies').get(ticketCtrl.getCompanies)

export default router
