import express from 'express';
import consentCtrl from '../controllers/consent.controller';
require('dotenv').config();

const router = express.Router(); // eslint-disable-line new-cap

router.route('/check/:email')
    .get(consentCtrl.getConsent);

router.route('/update')
    .post(consentCtrl.update);

export default router;
