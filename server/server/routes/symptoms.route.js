import express from 'express'
import symptomsCtrl from '../controllers/symptoms.controller'
const router = express.Router()
require('dotenv').config()

router.route('/update').put(symptomsCtrl.update)
router.route('/add').put(symptomsCtrl.add)
router.route('/create').post(symptomsCtrl.create)

// router.route('/update').put(symptomsCtrl.update)
export default router
