import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import compCtrol from '../controllers/company.controller';
import expressJwt from 'express-jwt';
require('dotenv').config();

const router = express.Router(); // eslint-disable-line new-cap

router.route('/')
  /** GET /api/answers - Get list of answers */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), compCtrol.list)

  /** POST /api/answers - Create new answer */
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), compCtrol.create);



router.route('/update')
  /** PUT /api/answers/update - Update answer */
  .put(expressJwt({ secret: process.env.JWT_TOKEN }), compCtrol.update)

router.route('/:companyCode/:userid')
  /** GET /api/answers/:answerId - Get answer */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), compCtrol.get)

  /** PUT /api/answers/:answerId - Update answer */
  .put(expressJwt({ secret: process.env.JWT_TOKEN }), validate(paramValidation.updateAnswer), compCtrol.update)


router.route('/:companies')
  /** GET /api/answers/:answerId - Get answer */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), compCtrol.get)

/** Load answer when API with answerId route parameter is hit */
// router.param('answerId', compCtrol.load);

export default router;
