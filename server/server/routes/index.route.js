import express from "express";
import userRoutes from "./user.route";
import authRoutes from "./auth.route";

import answerRoutes from "./answer.route";
import appointmentRoutes from "./appointment.route";
import summaryRoutes from "./summary.route";
import companyRoutes from "./company.route";
import emailRoutes from "./email.route";
import uploadRoutes from "./upload.route";
import reportRoutes from "./reports.route";
import analysisRoutes from "./analysis.route";
import subscriptionsRoutes from "./subscriptions.route";
import notificationsRoutes from "./notifications.route";
import eventsRoutes from "./events.route";
import ticketRoutes from "./ticket.route";
import announcementsRoutes from "./announcements.route";
import timelineRoutes from "./timeline.route";
// import symptomsRoutes from './symptoms.route'
import symptomsRoutes from "./symptoms.route";
import scheduleRoutes from "./schedule.route";
import consultsRoutes from "./consults.route";
import videoCallRoutes from "./videoCall.route";
import activityHistoryRoutes from "./activityHistory.route";
import bookAppNotifRoutes from "./book_Appointment_Notifications.route";
import consentRoutes from "./consent.route";
import concernRoutes from "./concern.route";
import systemRoutes from "./system.route";
import certificateRoutes from "./certificate.route";

const router = express.Router(); // eslint-disable-line new-cap

/** GET /health-check - Check service health */
router.get("/health-check", (req, res) => res.send("STATUS OK"));

// mount user routes at /users
router.use("/users", userRoutes);
// mount appointment routes at /appointments
router.use("/appointments", appointmentRoutes);

// mount answer routes at /answers
router.use("/answers", answerRoutes);

// mount answer routes at /summary
router.use("/summary", summaryRoutes);

// mount auth routes at /auth
router.use("/auth", authRoutes);

// mount email routes at /email
router.use("/email", emailRoutes);

// mount auth routes at /company
router.use("/company", companyRoutes);

router.use("/upload", uploadRoutes);

router.use("/reports", reportRoutes);

router.use("/analysis", analysisRoutes);

router.use("/subscriptions", subscriptionsRoutes);

router.use("/notifications", notificationsRoutes);

router.use("/events", eventsRoutes);

router.use("/ticket", ticketRoutes);

router.use("/announcements", announcementsRoutes);

router.use("/timeline", timelineRoutes);

router.use("/symptoms", symptomsRoutes);
router.use("/schedule", scheduleRoutes);

router.use("/consults", consultsRoutes);
router.use("/videoCall", videoCallRoutes);

router.use("/activityHistory", activityHistoryRoutes);

router.use("/book_appointment_notification", bookAppNotifRoutes);

router.use("/consent", consentRoutes);

router.use("/concern", concernRoutes);

router.use("/system", systemRoutes);

router.use("/certificate", certificateRoutes);

export default router;
