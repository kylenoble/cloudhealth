import express from 'express';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import analysisCtrl from '../controllers/analysis.controller';
import expressJwt from 'express-jwt';
require('dotenv').config();

const router = express.Router(); // eslint-disable-line new-cap

router.route('/:userId')
  /** GET /api/answers - Get list of answers */
  .get(expressJwt({ secret: process.env.JWT_TOKEN }), analysisCtrl.get)

// //flexible querying
// router.route('/qry')
//   /** PUT /api/answers/update - Update answer */
//   .put(analysisCtrl.getQuery)
//
router.route('/update')
  /** PUT /api/answers/update - Update answer */
  .put(expressJwt({ secret: process.env.JWT_TOKEN }), analysisCtrl.update)
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), analysisCtrl.save)



export default router;
