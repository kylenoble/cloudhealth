import express from "express";
import validate from "express-validation";
import paramValidation from "../../config/param-validation";
import expressJwt from "express-jwt";
require("dotenv").config();

import uploadCtrl from "../controllers/upload.controller";

const router = express.Router(); // eslint-disable-line new-cap

router.route("/notmultiple").post(uploadCtrl.uploadImage);

router.route("/report").post(uploadCtrl.uploadReport);

router.route("/healthdocs").post(uploadCtrl.uploadHealthDocument);

export default router;
