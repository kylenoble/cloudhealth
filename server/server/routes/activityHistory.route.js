import express from 'express'
import activityHistory from '../controllers/activityHistory.controller'
import expressJwt from 'express-jwt'
require('dotenv').config()

const router = express.Router() // eslint-disable-line new-cap

router.route('/getHost/:id').get(activityHistory.getHost)
router.route('/getEvents/:id/:userid').get(activityHistory.getEvents)
router.route('/getAppointments/:id').get(activityHistory.getAppointments)
router.route('/getSubscriptionPrograms').get(activityHistory.getSubscriptionPrograms)
router.route('/getSubscriptions/:id').get(activityHistory.getSubscriptions)
router.route('/getSubscriptionsByCompany/:id').get(activityHistory.getSubscriptionsByCompany)
router.route('/getDoctor/:id').get(activityHistory.getDoctor)

export default router
