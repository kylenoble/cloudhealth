import express from 'express';
import validate from 'express-validation';
import expressJwt from 'express-jwt';
import paramValidation from '../../config/param-validation';
import emailCtrl from '../controllers/email.controller';
require('dotenv').config();

const router = express.Router(); // eslint-disable-line new-cap

router.route('/sendmail')
  .post(emailCtrl.sendMail);


/** POST /api/auth/login - Returns token if correct username and password is provided */
router.route('/:val')
  .get(emailCtrl.get)
  .put(emailCtrl.update);

router.route('/sendEmailToHR').post(emailCtrl.emailTemplate)





export default router;
