/* eslint-disable linebreak-style */
import express from "express";
import validate from "express-validation";
import paramValidation from "../../config/param-validation";
import subscriptionsCtrl from "../controllers/subscriptions.controller";
import expressJwt from "express-jwt";

require("dotenv").config();

const router = express.Router();

/** GET /api/subscriptions - Get list of enrollment where company_id is equal to id */
router
  .route("/")
  .post(expressJwt({ secret: process.env.JWT_TOKEN }), subscriptionsCtrl.get);

/** update /api/subscriptions - Updatet enrollment status where id is equal to id */
router
  .route("/update")
  .post(
    expressJwt({ secret: process.env.JWT_TOKEN }),
    subscriptionsCtrl.update
  );

router.route("/getProgram").post(subscriptionsCtrl.getProgram);

export default router;
