import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import APIError from '../helpers/APIError';
import userCtrl from '../controllers/user.controller';
require('dotenv').config();

/**
 * Returns jwt token if valid username and password is provided
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */

 function resetPassword(req, res){

      'use strict';
      const nodemailer = require('nodemailer');

      // create reusable transporter object using the default SMTP transport
      let transporter = nodemailer.createTransport({
          host: 'smtp.gmail.com',
          port: 587,
          secure: false, // secure:true for port 465, secure:false for port 587
          auth: {
              user: 'customercare@merakisv.com',
              pass: 'MerakisvCC'
          }
      });

      // setup email data with unicode symbols
      let mailOptions = {
          from: '"CloudHealthAsia" <customercare@merakisv.com>', // sender address
          to: 'jm@merakisv.com', // list of receivers
          subject: 'Password Reset', // Subject line
          text: 'You request for password Reset!', // plain text body
          html: `<b>Here is your Temporaty password 11111asdfas;ljfas;ljf23456766666 </b>
          kidly Change your password upon login` // html body
      };

      // send mail with defined transport object
      if(mailOptions.html != ''){
      transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            console.log("error to");
              return console.log(error);
          }
          console.log('Message %s sent: %s', info.messageId, info.response);
          return res.send('Message %s sent: %s', info.messageId, info.response)
      });}



 }

function login(req, res, next) {
  // Ideally you'll fetch this from the db
  // Idea here was to show how jwt works with simplicity
  userCtrl.login(req.body.email, req.body.password, req.body.auto_id)
  .then(user => {
    if (user.authenticated) {
      const token = jwt.sign({
        password: user.password,
      }, process.env.JWT_TOKEN);
      return res.json({
        token,
        id: user.id
      });
    } else{
      return res.json({
        result: 'incorrect login information'
      })
    }
  })
  .catch(error => {
    return res.json({
      status: error.status,
      message: error.message,
      isPublic: error.isPublic,
      isOperational: error.isOperational
    })
  })
}

/**
 * This is a protected route. Will return random number only if jwt token is provided in header.
 * @param req
 * @param res
 * @returns {*}
 */
function getRandomNumber(req, res) {
  // req.user is assigned by jwt middleware if valid token is provided
  return res.json({
    user: req.user,
    num: Math.random() * 100
  });
}

export default { login, getRandomNumber, resetPassword };
