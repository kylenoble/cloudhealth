import Subscription from "../models/subscriptions.model";

const subscription = new Subscription();

function get(req, res) {
  subscription
    .get(req.body)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function update(req, res) {
  subscription
    .update(req.body)
    .then(res => {
      res.json(res);
    })
    .catch(e => {
      res.json(e);
    });
}

function getProgram(req, res) {
  subscription
    .getProgram(req.body)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

export default { get, update, getProgram };
