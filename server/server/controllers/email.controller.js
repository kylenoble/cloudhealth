import Email from "../models/email.model";
import Promise from "bluebird";
/**
 * Load user and append to req.
 */
const email = new Email();

/**
 * Get Email
 * @returns {Email}
 */
function get(req, res) {
  email
    .get(req.params.val)
    .then(email => {
      res.json(email);
    })
    .catch(error => res.json(error));
}

/**
 * Update existing user
 * @property {string} req.body.email - The email of user.
 * @property {string} req.body.id - user Id
 * @returns {User}
 */
function update(req, res) {
  const userParams = req.body[0];
  const userId = req.params.val;
  email
    .update(userId, userParams)
    .then(user => {
      res.json({ user: user });
    })
    .catch(e => res.json(e));
}

function sendMail(req, res) {
  let recepient = req.body.email;
  let newPass = req.body.password;
  let name = req.body.name;
  let html = `
       <p style="text-align: justify">This confirms approval of your request to reset the password of your CloudhealthAsia
       account.</p>

       <p style="text-align: justify">Your New Password: ${newPass}</p>

       <p style="text-align: justify">If you did not make this request, please report this immediately
       at client.services@cloudhealthasia.com</p>
  `

  var parameters = {
    body: {
      subject: "Password Reset",
      text: "You request for password reset",
      to: recepient,
      content: html,
      salutation_text: `
      Dear ${name}, `
    }
  }
  return emailTemplate(parameters, res);
}

// Email Template
function emailTemplate(req, res) {
  "use strict";
  const nodemailer = require("nodemailer");

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false, // secure:true for port 465, secure:false for port 587
    auth: {
      user: "client.services@cloudhealthasia.com",
      pass: "Arsenal2017"
    },
    secureConnection: "false",
    tls: {
      ciphers: "SSLv3",
      rejectUnauthorized: false
    }
  });

  const { subject, text, to, content, salutation_text } = req.body;
  let salutation = '';
  if (salutation_text !== undefined) {
    salutation = `
        <tr>
          <td width="100%" valign="top" bgcolor="#ffffff" style="text-align:left;color:#333333;font-family:Montserrat, 'Open Sans', Helvetica, Arial, sans-serif;font-size:15px;margin:0px;padding: 0 0 30px;">
          ${salutation_text}
          </td>
        </tr>
    `
  }

  const from = '"CloudHealthAsia" <client.services@cloudhealthasia.com>',
    html = `
        <div style="width:100%!important;max-width:740px!important;text-align:center;margin:0 auto">
    <table width="740" align="center" cellpadding="0" cellspacing="0" border="0"
      style="background-color:#ffffff;margin:0 auto;border:none;width:100%!important;max-width:740px!important">
      <tbody>
        <tr>
          <td width="90%" valign="top">
            <div style="border:1px solid #ccc;">
              <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="90%"
                style="margin-left:auto;margin-right:auto;margin-top:25px;margin-bottom:15px" dir="ltr">
                <tbody>
                  <tr>
                    <td width="45%" bgcolor="#ffffff" style="text-align: left;">
                      <a style="text-decoration: none;" href="https://cloudhealthasia.com/" target="_blank">
                        <img width="135" src="https://cloudhealthasia.com/static/icons/cha_logo_with_text.png" alt="CloudHealthAsia">
                      </a>
                    </td>
                    <td width="55%" bgcolor="#ffffff"></td>
                  </tr>

                </tbody>
              </table>

              <table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="0" width="90%"
                style="margin:0 auto;border-top:1px solid #cccccc;padding-top:25px" dir="ltr">
                <tbody>
                  ${salutation}
                  <tr>
                    <td width="100%" valign="top" bgcolor="#ffffff"
                      style="text-align:left;color:#333333;font-family:Montserrat, 'Open Sans', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0">
                      Warm greetings from CloudHealthAsia!
                    </td>
                  </tr>

                  <tr>
                    <td width="100%" valign="top" bgcolor="#ffffff"
                      style="text-align:justify;color:#333333;font-family:Montserrat, 'Open Sans', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;padding-top:10px;font-weight:normal">
                      ${content}
                    </td>
                  </tr>

                  <tr>
                    <td width="100%" valign="top" bgcolor="#ffffff"
                      style="text-align:left;color:#333333;font-family:Montserrat, 'Open Sans', Helvetica, Arial, sans-serif;font-size:15px;line-height:22px;margin:0px;padding:0;padding-top:30px; padding-bottom: 10px;font-weight:normal">
                      Thank you for trusting us.<br><br>
                      Sincerely,<br>
                      CloudHealthAsia Team
                    </td>
                  </tr>
                </tbody>
              </table>


              <table border="0" cellspacing="0" cellpadding="25" width="100%" dir="ltr">
                <tbody>
                  <tr>
                    <td width="100%" bgcolor="#cccccc" style="text-align:center; padding: 10px;">
                      <p
                        style="color:#999999;font-family:Montserrat, 'Open Sans', Helvetica, Arial, sans-serif;font-size:10px;line-height:14px;margin-top:0;padding:0;font-weight:normal">
                        This message (including any attachments) contains confidential information intended for a
                        specific
                        individual and purpose, and is protected by law. If you are not the intended recipient, you
                        should
                        delete this message. Any disclosure, copying, or distribution of this message, or the taking of
                        any action based on it, is strictly prohibited.
                      </p>
                      <p
                        style="color:#999999;font-family:Montserrat, 'Open Sans', Helvetica, Arial, sans-serif;font-size:12px;line-height:14px;margin:0;padding:0;font-weight:normal">
                        © 2019 CloudHealthAsia. All rights reserved.
                      </p>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
        `;

  // setup email data with unicode symbols
  let mailOptions = {
    from, // sender address
    to, // list of receivers
    subject, // Subject line
    text, // plain text body
    html // html body
  };

  // send mail with defined transport object
  if (process.env.SENDMAIL == "true" && mailOptions.html != "") {
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.log("error to");
        return console.log(error);
      } else {
        console.log("Message %s sent: %s", info.messageId, info.response);
        return res.json("Message %s sent: %s", info.messageId, info.response);
      }
    });
  }
}

export default { get, update, sendMail, emailTemplate };
