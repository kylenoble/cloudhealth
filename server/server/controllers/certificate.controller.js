import CertificateModel from '../models/certificate.model';
import multer from 'multer'
import fs from 'fs'
import path from 'path'
const certificateModel = new CertificateModel();


// Storage Engine

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, '../client/static/certificates/')
  },
  filename: function (req, file, cb) {
    cb(null, req.body.prefix + '-' + file.originalname)
  }
})

var upload = multer({
  storage: storage
}).single('file')



function uploadAttachment(req, res) {
  upload(req, res, function (err) {
    if (err) {
      return res.json(err)
    }
    return res.json({ 'filename': req.file.filename })
  })
}

function removeAttachent(req, res) {
  res.send(req.params.file)
  console.log('@ctrl remove att: ', req.params.file);

  let path = '../client/static/certificates/'
  let filename = req.params.file

  fs.stat(path + filename, (err, stats) => {
    console.log(stats)
    if (err) {
      return console.error(err);
    }

    fs.unlink(path + filename, function (err) {
      if (err) return console.log(err);
      console.log('Attachment removed successfully')
    })
  })
}

function create(req, res) {
  certificateModel.create(req.body)
    .then((data) => {
      res.json(data);
    })
    .catch((error) => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

function update(req, res) {
  certificateModel.update(req.params.id, req.body)
    .then((data) => {
      res.json(data);
    })
    .catch((error) => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

function deleteCert(req, res) {
  certificateModel.deleteCert(req.params.id)
    .then((data) => {
      res.json(data);
    })
    .catch((error) => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

function getCertificates(req, res) {
  certificateModel.getCertificates()
    .then((result) => {
      res.json(result)
    })
    .catch(error => res.json(error));
}

function getCertificate(req, res) {
  certificateModel.getCertificate(req.params.id)
    .then((result) => {
      res.json(result)
    })
    .catch(error => res.json(error));
}

export default { uploadAttachment, removeAttachent, create, update, deleteCert, getCertificates, getCertificate }