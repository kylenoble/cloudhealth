import User from "../models/user.model";
import Promise from "bluebird";
import multer from "multer";
import { now } from "moment";
/**
 * Load user and append to req.
 */
const user = new User();

var path = require("path");

var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    if (req.body.is_report != null && req.body.is_report) {
      cb(null, "../client/static/reports/CHI");
    } else if (req.body.healthdocs) {
      cb(null, "../client/static/healthdocs");
    } else {
      cb(null, "../client/static/avatar");
    }
  },
  filename: function(req, file, cb) {
    cb(null, req.body.prefix + file.originalname);
  }
});

var upload = multer({ storage: storage }).single("file");

/**
 * Update existing user
 * @property {string} req.body.userId - Record Id.
 * @property {string} req.file.filename - image filename.
 *
 */

function uploadImage(req, res, next) {
  // upload image file to server
  upload(req, res, function(err) {
    if (err) {
      res.json(err);
    } else {
      // remove old image file
      var fs = require("fs");
      fs.stat("../client/static/avatar/" + req.body.oldAvatar, function(
        err,
        stats
      ) {
        console.log(stats); //here we got all information of file in stats variable
        if (err) {
          return console.error(err);
        }

        fs.unlink("../client/static/avatar/" + req.body.oldAvatar, function(
          err
        ) {
          if (err) return console.log(err);
          console.log("file deleted successfully");
        });
      });
      // update image filename in database
      const userParams = { avatar: req.file.filename };
      const userId = req.body.UserId;
      user
        .update(userId, userParams)
        .then(user => {
          res.json({ user: user });
        })
        .catch(e => res.json(e));
    }
  });
}

function uploadReport(req, res, next) {
  // upload image file to server
  upload(req, res, function(err) {
    if (err) {
      return res.json(err);
    }
    return res.json({ filename: req.file.filename });
  });
}

function uploadHealthDocument(req, res, next) {
  // upload image file to server
  upload(req, res, function(err) {
    if (err) {
      res.json(err);
    } else {
      // save file info in database
      user
        .saveHealthDocumentInfo(req)
        .then(response => {
          if (response) {
            res.json(response);
          }
        })
        .catch(e => res.json(e));
    }
  });
}

export default { uploadImage, uploadReport, uploadHealthDocument };
