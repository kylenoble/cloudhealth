import Events from '../models/events.model';

const events = new Events();

function get(req, res) {
  events.get(req.params.str, req.params.userid)
    .then((result) => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function update(req, res){
  events.update(req.body)
  .then((res)=>{
    res.json(res)
  })
  .catch((e)=>{res.json(e)})
}


export default { get, update };
