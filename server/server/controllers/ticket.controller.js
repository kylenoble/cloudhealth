import TicketModel from '../models/ticket.model';
import multer from 'multer'
import fs from 'fs'
import path from 'path'
const ticketModel = new TicketModel();


// Storage Engine

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '../client/static/ticket_attachments/', )
    },
    filename: function (req, file, cb) {
        cb(null, req.body.prefix + '-' + file.originalname)
    }
})


var upload = multer({
    storage: storage
}).single('file')

function uploadAttachment(req, res) {
    upload(req, res, function (err) {
        if (err) {
            return res.json(err)
        }
        return res.json({ 'filename': req.file.filename })
    })
}

function removeAttachent(req, res) {
    res.send(req.params.file)
    console.log('@ctrl remove att: ',req.params.file);

    let path = '../client/static/ticket_attachments/'
    let filename = req.params.file

    fs.stat(path + filename, (err, stats) => {
        console.log(stats)
        if (err) {
            return console.error(err);
        }

        fs.unlink(path + filename,function(err){
             if(err) return console.log(err);
             console.log('Attachment removed successfully')
        })
     })
}

function getReplies(req, res) {
    ticketModel.getReplies(req.params.tnum)
        .then((result) => {
            res.json(result)
        })
        .catch(error => res.json(error));
}

function getCompanies(req, res) {
    ticketModel.getCompanies()
        .then((result) => {
            res.json(result)
        })
        .catch(error => res.json(error));
}

function getTickets(req, res) {
    ticketModel.getTickets(req.params.tnum)
        .then((result) => {
            res.json(result)
        })
        .catch(error => res.json(error));
}

function getLatestTicket(req, res) {
    ticketModel.getLatestTicket()
        .then((result) => {
            res.json(result)
        })
        .catch(error => res.json(error));
}

function create(req, res) {
    console.log('@controller create ticket: ',req.body);

    ticketModel.create(req.body)
        .then((data) => {
            res.json(data);
        })
        .catch((error) => {
            res.json({
                status: error.status,
                message: error.message,
                isPublic: error.isPublic,
                isOperational: error.isOperational
            });
        });
}


function sendEmail(req, res) {
    let recipient = req.body.recipient
    // let name = req.body.name
    let ticket_number = req.body.ticket_number
    let concern = req.body.concern_title

    console.log('@email controller: ',req.body.recipient);


    'use strict';
    const nodemailer = require('nodemailer');

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false, // secure:true for port 465, secure:false for port 587
        auth: {
            user: 'client.services@cloudhealthasia.com',
            pass: 'Arsenal2017'
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"CloudHealthAsia" <client.services@cloudhealthasia.com>', // sender address
        to: recipient, // list of receivers
        subject: 'CloudHealthAsia: Help & Support', // Subject line
        text: 'This is the confirmation of your ticket', // plain text body
        html: `
            Warm greetings from CloudHealthAsia!<br><br>

            This is a confirmation of your ticket submission with below details:<br><br>

            Ticket Number: <strong>${ticket_number}</strong><br>
            Concern: <strong>${concern}</strong><br><br>

            Should you need updates on your concern, go to the website <a href="https://cloudhealthasia.com">(https://cloudhealthasia.com)</a>, click on the FAQ > Help & Support > Check Ticket Status.<br><br>

            Thank you for trusting us.<br><br>

            Sincerely,<br>

            CloudHealthAsia Team <br><br>

            <p style="background: #ccc; color: #999; font-size: 10px; padding: 25px; text-align: center">
                This message (including any attachments) contains confidential information intended for a specific individual and purpose, and is protected by law. If you are not the intended recipient, you should delete this message. Any disclosure, copying, or distribution of this message, or the taking of any action based on it, is strictly prohibited.

                <strong style="margin-top: 20px; display: block; font-size: 12px">&copy; CloudHealthAsia, All Rights Reserved.</strong>
            </p>
       ` // html body
    };

    // send mail with defined transport object
    if (process.env.SENDMAIL == 'true' && mailOptions.html != '') {
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log("error to");
                return console.log(error);
            } else {
                console.log('Message %s sent: %s', info.messageId, info.response);
                return res.json('Message %s sent: %s', info.messageId, info.response)
            }

        });
    }
}


export default { getCompanies, getReplies, getTickets, getLatestTicket, create, sendEmail, uploadAttachment,removeAttachent };
