import Notifications from '../models/notifications.model';

const notifications = new Notifications();

function getAdmin(req, res) {
  notifications.getAdmin(req.params.id)
    .then((result) => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function updateAdmin(req, res) {
  notifications.updateAdmin(req.body)
    .then((res) => {
      res.json(res)
    })
    .catch((e) => { res.json(e) })
}

function sendReportNotification(req, res) {
  notifications
    .create_notif_for_hr(req.body)
    .then(data => {
      res.json(data)
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      })
    })
}


export default { getAdmin, updateAdmin, sendReportNotification };
