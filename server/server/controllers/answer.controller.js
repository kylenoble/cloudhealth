import Answer from "../models/answer.model";

/**
 * Load user and append to req.
 */
const answer = new Answer();

/**
 * Get answer
 * @returns {Answer}
 */
function get(req, res) {
  let type;
  let value;
  if (req.params.group) {
    type = "group";
    value = req.body.group;
  } else {
    type = "id";
    value = req.body.id;
  }
  answer
    .get(type, value, req.body.userId)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

/**
 * Get result
 * @returns {Result}
 */
function getQuery(req, res) {
  let qry = req.body.query;

  answer
    .getQuery(qry)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

/**
 * Create new answers
 * @property {Array} req.body.answers - an array of answers.
 * @returns {Success}
 */
function create(req, res) {
  answer
    .create(req.body)
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

/**
 * Update existing answer
 * @property {string} req.body.email - The email of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function update(req, res) {
  answer
    .update(req.body)
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

/**
 * Get user list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {Answer[]}
 */

function list(req, res) {
  answer
    .list(req.query, req.query.userId)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

/**
 * Get Data
 * @returns {Data}
 */
function getGraphData(req, res) {
  answer
    .getGraphData(req.body)
    .then(user => {
      res.json(user);
    })
    .catch(error => {
      res.json(error);
    });
}

/**
 * Get core imbalances scores by userID
 * @returns {Answer}
 */
function getCoreImbalancesData(req, res) {
  answer
    .getCoreImbalancesScores(req.userId)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function getPieChartData(req, res) {
  const question_id = req.params.question_id;
  const company_name = req.params.company_name;

  answer
    .getPieChartData(question_id, company_name)
    .then(result => {
      res.json(result);
    })
    .catch(error => {
      res.json(error);
    });
}

export default {
  getPieChartData,
  get,
  create,
  update,
  list,
  getCoreImbalancesData,
  getGraphData,
  getQuery
};
