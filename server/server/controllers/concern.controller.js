import Concern from '../models/concern.model';

const concern = new Concern();

function get(req, res) {
    concern.get(req.params)
    .then((result) => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function add(req, res){
  concern.add(req.body)
  .then((res)=>{
    res.json(res)
  })
  .catch((e)=>{res.json(e)})
}

export default { get, add };
