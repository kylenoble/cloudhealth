import Announcements from '../models/announcements.model';

const announcements = new Announcements();

function get(req, res) {
  announcements.get(req.params.str)
    .then((result) => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function update(req, res){
  announcements.update(req.body)
  .then((res)=>{
    res.json(res)
  })
  .catch((e)=>{res.json(e)})
}


export default { get, update };
