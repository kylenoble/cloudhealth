import User from "../models/user.model";
import Promise from "bluebird";
/**
 * Load user and append to req.
 */
const user = new User();

function login(email, password, auto_id) {
  return new Promise(function(resolve, reject) {
    user
      .login(email, password, auto_id)
      .then(user => {
        resolve(user);
      })
      .catch(error => {
        reject(error);
      });
  });
}

function saveActivity(req, res) {
  return new Promise(function(resolve, reject) {
    user
      .saveActivity(req.body)
      .then(res => {
        resolve(res);
      })
      .catch(err => {
        reject(err);
      });
  });
}

function saveToMonitoring(req, res) {
  const id = req.body.id;
  const action = req.body.action;
  user
    .saveToMonitoring(id, action)
    .then(user => {
      res.json(user);
    })
    .catch(e => {
      res.json(e);
    });
}

function monitorPassword(req, res) {
  user
    .monitorPassword(req.body.id)
    .then(user => {
      res.json(user);
    })
    .catch(error => {
      res.json(error);
    });
}
/**
 * Load user and append to req.
 */
function load(req, res, next, id) {
  user
    .get(id)
    .then(user => {
      req.user = user; // eslint-disable-line no-param-reassign
      return next();
    })
    .catch(e => next(e));
}

/**
 * Get Data
 * @returns {Data}
 */
function getData(req, res) {
  user
    .getData(req.body.field, req.body.value)
    .then(user => {
      res.json(user);
    })
    .catch(error => {
      res.json(error);
    });
}

/**
 * Get user
 * @returns {User}
 */
function get(req, res) {
  user
    .get(req.params.userId)
    .then(user => {
      res.json(user);
    })
    .catch(error => res.json(error));
}

/**
 * Get user with Birtday and Gender
 * @returns {User}
 */
function getPatientInfo(req, res) {
  user
    .getPatientInfo(req.params.userId)
    .then(user => {
      res.json(user);
    })
    .catch(error => res.json(error));
}

function saveOtherInfo(req, res) {
  return new Promise(function(resolve, reject) {
    user
      .saveOtherInfo(req.body)
      .then(res => {
        res.json(res);
      })
      .catch(error => res.json(error));
  });
}

function updateOtherInfo(req, res) {
  return new Promise(function(resolve, reject) {
    user
      .updateOtherInfo(req.body)
      .then(res => {
        res.json(res);
      })
      .catch(error => res.json(error));
  });
}

function deleteOtherInfo(req, res) {
  return new Promise(function(resolve, reject) {
    user
      .deleteOtherInfo(req.body)
      .then(res => {
        res.json(res);
      })
      .catch(error => res.json(error));
  });
}

function getOtherInfo(req, res) {
  user
    .getOtherInfo(req)
    .then(user => {
      res.json(user);
    })
    .catch(error => res.json(error));
}

function getHealthDocs(req, res) {
  user
    .getHealthDocs(req)
    .then(user => {
      res.json(user);
    })
    .catch(error => res.json(error));
}

function deleteFile(req, res) {
  return new Promise(function(resolve, reject) {
    user
      .deleteFile(req.body)
      .then(res => {
        res.json(res);
      })
      .catch(error => res.json(error));
  });
}

/**
 * Get user
 * @returns {Doctors}
 */
function getDoctors(req, res) {
  user
    .getDoctors(req.body)
    .then(doctors => {
      res.json(doctors);
    })
    .catch(e => {
      console.log(error);
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

function getUserFilters(req, res) {
  user
    .getUserFilters(req.params.company)
    .then(filter => {
      res.json(filter);
    })
    .catch(error => res.json(error));
}

function changePassword(req, res) {
  const userParams = req.body[0];
  user
    .changePassword(userParams)
    .then(user => {
      res.json(user);
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

function updateAdminPassword(req, res) {
  const userParams = req.body[0];
  console.log("###########################");
  console.log("userParams", userParams);
  console.log("###########################");

  user
    .updateAdminPassword(userParams)
    .then(user => {
      res.json(user);
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

function getMDInfoByToken(req, res) {
  user
    .getMDInfoByToken(req.params.token)
    .then(user => {
      res.json(user);
    })
    .catch(error => res.json(error));
}

/**
 * Create new user
 * @property {string} req.body.email - The email of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function create(req, res, next) {
  user
    .create(
      req.body.email,
      req.body.password,
      req.body.name,
      req.body.company,
      req.body.employeeId,
      req.body.department,
      req.body.branch
    )
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

/**
 * Update existing user
 * @property {string} req.body.email - The email of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function update(req, res) {
  const userParams = req.body[0];
  const userId = req.params.userId;
  user
    .update(userId, userParams)
    .then(user => {
      res.json({ user: user });
    })
    .catch(e => res.json(e));
}

/**
 * Get user list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {User[]}
 */
function list(req, res, next) {
  console.log("THIS IS NOT SUPPOSE TO RUN");
  return;
  const { limit = 50, skip = 0 } = req.query;
  user
    .list({ limit, skip })
    .then(users => res.json(users))
    .catch(e => next(e));
}

/**
 * Delete user.
 * @returns {User}
 */
function remove(req, res, next) {
  const user = req.user;
  user
    .remove()
    .then(deletedUser => res.json(deletedUser))
    .catch(e => next(e));
}

function getAdmin(req, res) {
  console.log("@userController", req.params.id);

  user
    .getAdmin(req.params.id)
    .then(user => {
      res.json(user);
    })
    .catch(error => res.json(error));
}
function getAdminByID(req, res) {
  console.log("@userController", req.params.id);

  user
    .getAdminByID(req.params.id)
    .then(user => {
      res.json(user);
    })
    .catch(error => res.json(error));
}

function updateMDProfile(req, res) {
  user
    .updateMDProfile(req.params.id, req.body)
    .then(response => {
      res.json(response);
    })
    .catch(err => res.json(err));
}

function authMDPassword(req, res) {
  user
    .authMDPassword(req.body)
    .then(response => {
      res.json(response);
    })
    .catch(err => res.json(err));
}

export default {
  getAdminByID,
  monitorPassword,
  load,
  get,
  create,
  update,
  list,
  remove,
  login,
  changePassword,
  getDoctors,
  getPatientInfo,
  getData,
  getUserFilters,
  saveActivity,
  getMDInfoByToken,
  getAdmin,
  getOtherInfo,
  saveOtherInfo,
  updateOtherInfo,
  deleteOtherInfo,
  getHealthDocs,
  deleteFile,
  updateAdminPassword,
  updateMDProfile,
  authMDPassword,
  saveToMonitoring
};
