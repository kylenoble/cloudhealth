import BookAppNotifModel from '../models/book_Appointment_Notification.model'
import multer from 'multer'
import fs from 'fs'
import path from 'path'
const bookAppNotifModel = new BookAppNotifModel()

function create(req, res) {
  if (req.body.patient.subscription === undefined) {
    bookAppNotifModel
      .create_notif_for_doctor(req.body)
      .then(data => {
        res.json(data)
      })
      .catch(error => {
        res.json({
          status: error.status,
          message: error.message,
          isPublic: error.isPublic,
          isOperational: error.isOperational
        })
      })
  }

  bookAppNotifModel
    .create_notif_for_patient(req.body)
    .then(data => {
      res.json(data)
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      })
    })
}

export default { create }
