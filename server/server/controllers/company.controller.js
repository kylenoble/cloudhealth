import Company from '../models/company.model';
import pdf from 'pdfkit'
import fs from 'fs'
const company = new Company();

/**
 * Get company
 * @returns {company}
 */
function get(req, res) {
  company.get(req.params.companyCode, req.params.userid)
    .then((result) => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

/**
 * Create new companys
 * @property {Array} req.body.companys - an array of companys.
 * @returns {Success}
 */
function create(req, res) {
  company.create(req.body)
  .then((data) => {
    res.json(data);
  })
  .catch((error) => {
    res.json({
      status: error.status,
      message: error.message,
      isPublic: error.isPublic,
      isOperational: error.isOperational
    });
  });
}

/**
 * Update existing company
 * @property {string} req.body.email - The email of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function update(req, res) {

  company.update(req.body)
  .then((data) => {
    res.json(data);
  })
  .catch((error) => {
    res.json({
      status: error.status,
      message: error.message,
      isPublic: error.isPublic,
      isOperational: error.isOperational
    });
  });
}

/**
 * Get user list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {company[]}
 */

function list(req, res) {
  company.list()
    .then((result) => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

export default { get, create, update, list};
