import VideoCall from '../models/videoCall.model'

const videoCall = new VideoCall()

function get(req, res){
  return videoCall.get(req.body)
  .then((result) => {
    res.json(result);
  })
  .catch(error => {
    res.json(error)
  });
}


export default{get}
