import Timeline from '../models/timeline.model';

const timelime = new Timeline();

function get(req, res) {
  timelime.get(req.params.id)
    .then((result) => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function update(req, res){
  timelime.update(req.body)
  .then((res)=>{
    res.json(res)
  })
  .catch((e)=>{res.json(e)})
}


export default { get, update };
