import Analysis from '../models/analysis.model';

/**
 * Load user and append to req.
 */
const analysis = new Analysis();

/**
 * Get answer
 * @returns {Analysis}
 */
function get(req, res) {
  analysis.get(req.params.userId)
    .then((result) => {
      res.json(result);
    })
    .catch(error => res.json(error));
}


// /**
//  * Get result
//  * @returns {Result}
//  */
// function getQuery(req, res) {
//   let qry = req.body.query
//
//   answer.getQuery(qry)
//     .then((result) => {
//       res.json(result);
//     })
//     .catch(error => res.json(error));
// }
//
// /**
//  * Create new answers
//  * @property {Array} req.body.answers - an array of answers.
//  * @returns {Success}
//  */
function save(req, res) {
  analysis.save(req.body)
  .then((data) => {
    res.json(data);
  })
  .catch((error) => {
    res.json({
      status: error.status,
      message: error.message,
      isPublic: error.isPublic,
      isOperational: error.isOperational
    });
  });
}
//
// /**
//  * Update existing answer
//  * @property {string} req.body.email - The email of user.
//  * @property {string} req.body.mobileNumber - The mobileNumber of user.
//  * @returns {User}
//  */
function update(req, res) {
  analysis.update(req.body)
  .then((data) => {
    res.json(data);
  })
  .catch((error) => {
    res.json({
      status: error.status,
      message: error.message,
      isPublic: error.isPublic,
      isOperational: error.isOperational
    });
  });
}


export default { get, update, save };
