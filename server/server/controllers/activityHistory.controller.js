import ActivityHistory from '../models/activityHistory.model';

const activityhistory = new ActivityHistory();

function getHost(req, res) {
  activityhistory
    .getHost(req.params.id)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function getEvents(req, res) {
  activityhistory
    .getEvents(req.params.id, req.params.userid)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function getAppointments(req, res) {
  activityhistory
    .getAppointments(req.params.id)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function getSubscriptionPrograms(req, res) {
  activityhistory
    .getSubscriptionPrograms()
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function getSubscriptions(req, res) {
  activityhistory
    .getSubscriptions(req.params.id)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function getSubscriptionsByCompany(req, res) {
  activityhistory
    .getSubscriptionsByCompany(req.params.id)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function getDoctor(req, res) {
  console.log();

  activityhistory
    .getDoctor(req.params.id)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

// function update(req, res){
//   events.update(req.body)
//   .then((res)=>{
//     res.json(res)
//   })
//   .catch((e)=>{res.json(e)})
// }

export default {
  getHost,
  getEvents,
  getAppointments,
  getSubscriptionPrograms,
  getSubscriptions,
  getSubscriptionsByCompany,
  getDoctor };
