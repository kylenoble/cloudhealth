import Appointment from "../models/appointment.model";

/**
 * Load user and append to req.
 */
const appointment = new Appointment();

/**
 * Get appointment
 * @returns {Appointment}
 */
function get(req, res) {
  let type = "";
  let value = "";
  let taken = false;
  if (req.query.active) {
    type = "active";
    value = req.query.active;
  } else if (req.query.status) {
    type = "status";
    value = req.query.status;
  } else if (req.query.id) {
    type = "id";
    value = req.query.id;
  } else if (req.query.doctor_id) {
    type = "doctor_id";
    value = req.query.doctor_id;
  }
  if (req.query.taken) {
    taken = true;
  }

  appointment
    .get(
      type,
      value,
      req.query.userId,
      req.query.userType,
      req.query.apmt_date,
      req.query.apmt_datetime,
      taken
    )
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

/**
 * Create new appointments
 * @property {Array} req.body.appointments - an array of appointments.
 * @returns {Success}
 */
function create(req, res) {
  appointment
    .create(req.body)
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

/**
 * Update existing appointment
 * @property {string} req.body.email - The email of user.
 * @property {string} req.body.mobileNumber - The mobileNumber of user.
 * @returns {User}
 */
function update(req, res) {
  const id = req.params.appointmentId;
  const appointmentParams = req.body;

  appointment
    .update(id, appointmentParams)
    .then(result => res.json(result))
    .catch(e => res.json(e));
}

// cancel / delete appointment in database

function cancel(req, res) {
  appointment
    .cancel(req.body.id)
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      console.log(error);
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

/**
 * Get user list.
 * @property {number} req.query.skip - Number of users to be skipped.
 * @property {number} req.query.limit - Limit number of users to be returned.
 * @returns {Appointment[]}
 */
function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;

  appointment
    .list({ limit, skip })
    .then(appointments => res.json(appointments))
    .catch(e => next(e));
}

function saveAppointmentHistory(req, res) {
  appointment
    .saveAppointmentHistory(req.body)
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

export default { get, create, update, list, cancel, saveAppointmentHistory };
