import SymptomsModel from '../models/symptoms.model'
const symptomsModel = new SymptomsModel()

function update(req, res) {

  symptomsModel
    .update(req.body)
    .then(data => {
      res.json(data)
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      })
    })
}

function add(req, res) {
  symptomsModel
    .add(req.body)
    .then(data => {
      res.json(data)
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      })
    })
}

function create(req, res) {
  symptomsModel
    .create(req.body)
    .then(data => {
      res.json(data)
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      })
    })
}

export default { update, add, create }
