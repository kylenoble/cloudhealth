import Reports from "../models/reports.model";
import pdf from "pdfkit";
import fs from "fs";
import htmlToText from "html-to-text";
import dateFormat from "dateformat";
import moment from "moment";

import Answer from "../models/answer.model";

const reports = new Reports();
const answer = new Answer();
/**
 * Get Reports default
 * @returns {defaults}
 */
function getReportDefault(req, res) {
  reports
    .getReportDefault()
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function getReport(req, res) {
  reports
    .getReport(req.body)
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

const createReport = str => {
  let info = str;
  let details = JSON.parse(info.details);
  let content = "";
  let now = new Date();
  let myDoc = new pdf();
  let insights = info.insights;
  let dateCreated = moment(info.date_created).format("MMM D Y, hh:mma");
  let currentDate = moment(info.date_modified).format("MMM D Y, hh:mma");
  let filename = info.file_name;
  let letterHead = `
${currentDate}

${info.company_name}
${info.location}
`;

  let greetings = `The results of your company’s Corporate Health Index are already viewable in your dashboard. Below are the recommendations drafted by our medical team based on these results. Feel free to coordinate with the officer in-charge of your account should you have clarifications. Our sales team would gladly assist you in customizing the corporate packages for your health program.`;

  let recommendations = `Below are the recommended programs basedon our insights and analysis`;

  let rep_info = `
${info.md_incharge}
${currentDate}

`;

  let ftr = `
These recommendations were drafted by the medical team based on the results of your corporate health index. Should you wish to avail any of the abovementioned programs and activities, you may contact the account officer in-charge of your company for further assistance. We are looking forward to serving you in this journey towards optimum health.

Sincerely,

`;

  let md = `Dr. ${info.md_incharge}`;

  let coy = `CloudHealthAsia`;

  let btm = `For inquiries and support, please email us at client.services@cloudhealthasia.com
`;

  //.font('fonts/Chalkboard.ttc', 'Chalkboard-Bold')
  //.font('fonts/GoodDog.ttf')
  myDoc
    .font("static/fonts/Roboto-Light.ttf")
    .image("static/files/images/logo-dark.png", 50)
    .fontSize(12)
    .moveDown();

  myDoc
    .font("static/fonts/Roboto-Light.ttf")
    .text(currentDate, 50)
    .moveDown();

  myDoc.font("static/fonts/Roboto-Bold.ttf").text(info.company_name, {
    align: "justify"
  });

  myDoc
    .font("static/fonts/Roboto-Light.ttf")
    .text(info.location, {
      width: 200,
      align: "justify"
    })
    .moveDown();

  myDoc
    .font("static/fonts/Roboto-Bold.ttf")
    .text("Dear Wellness Coordinator, ");

  myDoc
    .font("static/fonts/Roboto-Light.ttf")
    .text(greetings, {
      align: "justify"
    })
    .moveDown();

  myDoc
    .font("static/fonts/Roboto-BoldItalic.ttf")
    .text("Insights and Analysis")
    .moveDown();

  myDoc
    .font("static/fonts/Roboto-Light.ttf")
    .text(insights, 50, null, {
      align: "justify"
    })
    .moveDown();

  myDoc
    .font("static/fonts/Roboto-BoldItalic.ttf")
    .text("Recommendations")
    .moveDown(0.5);

  myDoc
    .font("static/fonts/Roboto-Light.ttf")
    .text(recommendations, {
      align: "justify"
    })
    .moveDown(0.5);

  for (let item in details) {
    if (details[item].selected) {
      if (item == "consults") {
        myDoc.font("static/fonts/Roboto-Medium.ttf").text("A. Consult.", 50);
        if (details[item].heartMath.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Heart Math."], 70);
          if (details[item].heartMath.description != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(details[item].heartMath.description, 100, null, {
                align: "justify"
              });
          }
          if (details[item].heartMath.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text("Note : " + details[item].heartMath.note, 100, null, {
                align: "justify"
              });
          }
        }

        if (details[item].nutrition.selected) {
          myDoc.font("static/fonts/Roboto-Medium.ttf").list(["Nutrition."], 70);
          if (details[item].nutrition.description != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(details[item].nutrition.description, 100, null, {
                align: "justify"
              });
          }
          if (details[item].nutrition.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text("Note : " + details[item].nutrition.note, 100, null, {
                align: "justify"
              });
          }
        }
      } // end
      else if (item == "diagnostics") {
        myDoc
          .font("static/fonts/Roboto-Medium.ttf")
          .moveDown(0.5)
          .text("B. Diagnostics/Screening.", 50);
        if (details[item].evaluationOfResult.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(
              ["Evaluation of Result by Registered-Nutritionist-Dietitian."],
              70
            );
          if (details[item].evaluationOfResult.description != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(details[item].evaluationOfResult.description, 100, null, {
                align: "justify"
              });
          }
          if (details[item].evaluationOfResult.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text(
                "Note : " + details[item].evaluationOfResult.note,
                100,
                null,
                { align: "justify" }
              );
          }
        }

        if (details[item].foodIntolerance.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Food Intolerance Test."], 70);
          if (details[item].foodIntolerance.description != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(details[item].foodIntolerance.description, 100, null, {
                align: "justify"
              });
          }
          if (details[item].foodIntolerance.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text("Note : " + details[item].foodIntolerance.note, 100, null, {
                align: "justify"
              });
          }
        }
      } // end
      else if (item == "activities") {
        myDoc
          .font("static/fonts/Roboto-Medium.ttf")
          .moveDown(0.5)
          .text("C. Activities.", 50);
        if (details[item].companyPantryRaidSession.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Company Pantry Raid Session."], 70);
          if (details[item].companyPantryRaidSession.description != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                details[item].companyPantryRaidSession.description,
                100,
                null,
                { align: "justify" }
              );
          }
          if (details[item].companyPantryRaidSession.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text(
                "Note : " + details[item].companyPantryRaidSession.note,
                100,
                null,
                { align: "justify" }
              );
          }
        }

        if (details[item].cookingDemo.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Cooking Demo."], 70);
          if (details[item].cookingDemo.description != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(details[item].cookingDemo.description, 100, null, {
                align: "justify"
              });
          }
          if (details[item].cookingDemo.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text("Note : " + details[item].cookingDemo.note, 100, null, {
                align: "justify"
              });
          }
        }
      } // end
      else if (item == "programs") {
        myDoc
          .font("static/fonts/Roboto-Medium.ttf")
          .moveDown(0.5)
          .text("D. Programs.", 50);
        if (details[item].mindBodyMedicineProgram.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Mind-Body Medicine Prpgram."], 70);
          if (details[item].mindBodyMedicineProgram.description1 != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                [details[item].mindBodyMedicineProgram.description1],
                100,
                null,
                { align: "justify" }
              )
              .moveDown(0.5);
          }
          if (details[item].mindBodyMedicineProgram.description2 != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                [details[item].mindBodyMedicineProgram.description2],
                100,
                null,
                { align: "justify" }
              )
              .moveDown(0.5);
          }
          if (details[item].mindBodyMedicineProgram.description3 != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                [details[item].mindBodyMedicineProgram.description3],
                100,
                null,
                { align: "justify" }
              )
              .moveDown(0.5);
          }
          if (details[item].mindBodyMedicineProgram.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text(
                "Note : " + details[item].mindBodyMedicineProgram.note,
                100,
                null,
                { align: "justify" }
              )
              .moveDown(0.5);
          }
        }

        if (details[item].essentialOptimalWeightManagementProgram.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Essential Optimal Weight Management Program."], 70);
          if (
            details[item].essentialOptimalWeightManagementProgram.description !=
            ""
          ) {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                details[item].essentialOptimalWeightManagementProgram
                  .description,
                100,
                null,
                { align: "justify" }
              );
          }
          if (
            details[item].essentialOptimalWeightManagementProgram.note != ""
          ) {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text(
                "Note : " +
                  details[item].essentialOptimalWeightManagementProgram.note,
                100,
                null,
                { align: "justify" }
              );
          }
        }
        if (details[item].essentialCoreHealthProgram.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Essential Core Health Program."], 70);
          if (details[item].essentialCoreHealthProgram.description != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                details[item].essentialCoreHealthProgram.description,
                100,
                null,
                { align: "justify" }
              );
          }
          if (details[item].essentialCoreHealthProgram.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text(
                "Note : " + details[item].essentialCoreHealthProgram.note,
                100,
                null,
                { align: "justify" }
              );
          }
        }
        if (details[item].essentialGeneticProfilingandIntervention.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Essential Genetic Profiling and Intervention."], 70);
          if (
            details[item].essentialGeneticProfilingandIntervention
              .description != ""
          ) {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                details[item].essentialGeneticProfilingandIntervention
                  .description,
                100,
                null,
                { align: "justify" }
              );
          }
          if (
            details[item].essentialGeneticProfilingandIntervention.note != ""
          ) {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text(
                "Note : " +
                  details[item].essentialGeneticProfilingandIntervention.note,
                100,
                null,
                { align: "justify" }
              );
          }
        }
      } // end
      else if (item == "education") {
        myDoc
          .font("static/fonts/Roboto-Medium.ttf")
          .moveDown(0.5)
          .text("E. Education.", 50);
        if (details[item].determinantOfHealthSleepManagement.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Determinant of Health: Sleep Management."], 70);
          if (
            details[item].determinantOfHealthSleepManagement.description != ""
          ) {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                details[item].determinantOfHealthSleepManagement.description,
                100,
                null,
                { align: "justify" }
              );
          }
          if (details[item].determinantOfHealthSleepManagement.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text(
                "Note : " +
                  details[item].determinantOfHealthSleepManagement.note,
                100,
                null,
                { align: "justify" }
              );
          }
        }

        if (details[item].determinantOfHealthStressManagement.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Determinant of Health: Stress Management."], 70);
          if (
            details[item].determinantOfHealthStressManagement.description != ""
          ) {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                details[item].determinantOfHealthStressManagement.description,
                100,
                null,
                { align: "justify" }
              );
          }
          if (details[item].determinantOfHealthStressManagement.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text(
                "Note : " +
                  details[item].determinantOfHealthStressManagement.note,
                100,
                null,
                { align: "justify" }
              );
          }
        }
        if (details[item].determinantOfHealthFitnessMovement.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Determinant of Health: Fitness/Movement."], 70);
          if (
            details[item].determinantOfHealthFitnessMovement.description != ""
          ) {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                details[item].determinantOfHealthFitnessMovement.description,
                100,
                null,
                { align: "justify" }
              );
          }
          if (details[item].determinantOfHealthFitnessMovement.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text(
                "Note : " +
                  details[item].determinantOfHealthFitnessMovement.note,
                100,
                null,
                { align: "justify" }
              );
          }
        }
        if (details[item].determinantOfDetoxification.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Determinant of Health: Detoxification."], 70);
          if (details[item].determinantOfDetoxification.description != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                details[item].determinantOfDetoxification.description,
                100,
                null,
                { align: "justify" }
              );
          }
          if (details[item].determinantOfDetoxification.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text(
                "Note : " + details[item].determinantOfDetoxification.note,
                100,
                null,
                { align: "justify" }
              );
          }
        }
        if (details[item].sixDeterminantsOfHealth.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Six Determinants of Health."], 70);
          if (details[item].sixDeterminantsOfHealth.description != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                details[item].sixDeterminantsOfHealth.description,
                100,
                null,
                { align: "justify" }
              );
          }
          if (details[item].sixDeterminantsOfHealth.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text(
                "Note : " + details[item].sixDeterminantsOfHealth.note,
                100,
                null,
                { align: "justify" }
              );
          }
        }
        if (details[item].functionalNutritionAndWeightManagement.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Functional Nutrition and Weight Management."], 70);
          if (
            details[item].functionalNutritionAndWeightManagement.description !=
            ""
          ) {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                details[item].functionalNutritionAndWeightManagement
                  .description,
                100,
                null,
                { align: "justify" }
              );
          }
          if (details[item].functionalNutritionAndWeightManagement.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text(
                "Note : " +
                  details[item].functionalNutritionAndWeightManagement.note,
                100,
                null,
                { align: "justify" }
              );
          }
        }
      } // end
      else if (item == "otherServices") {
        myDoc
          .font("static/fonts/Roboto-Medium.ttf")
          .moveDown(0.5)
          .text("F. Other Services.", 50);
        if (details[item].functionalMovementScreening.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Functional Movement Screening."], 70);
          if (details[item].functionalMovementScreening.description != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                details[item].functionalMovementScreening.description,
                100,
                null,
                { align: "justify" }
              );
          }
          if (details[item].functionalMovementScreening.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text(
                "Note : " + details[item].functionalMovementScreening.note,
                100,
                null,
                { align: "justify" }
              );
          }
        }

        if (details[item].bodyCompositionAnalysis.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Body Composition Analysis."], 70);
          if (details[item].bodyCompositionAnalysis.description != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(
                details[item].bodyCompositionAnalysis.description,
                100,
                null,
                { align: "justify" }
              );
          }
          if (details[item].bodyCompositionAnalysis.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text(
                "Note : " + details[item].bodyCompositionAnalysis.note,
                100,
                null,
                { align: "justify" }
              );
          }
        }
        if (details[item].kinesisSession.selected) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .list(["Kinesis Session."], 70);
          if (details[item].kinesisSession.description != "") {
            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(details[item].kinesisSession.description, 100, null, {
                align: "justify"
              });
          }
          if (details[item].kinesisSession.note != "") {
            myDoc
              .font("static/fonts/Roboto-LightItalic.ttf")
              .text("Note : " + details[item].kinesisSession.note, 100, null, {
                align: "justify"
              });
          }
        }
      } // end
    } else if (item === "additional") {
      let additional = details[item];
      if (additional) {
        if (additional.length > 0) {
          myDoc
            .font("static/fonts/Roboto-Medium.ttf")
            .moveDown(0.5)
            .text("Additional Programs.", 50);

          additional.forEach((item, i) => {
            myDoc
              .font("static/fonts/Roboto-Medium.ttf")
              .list([item.pname + "."], 70);

            myDoc
              .font("static/fonts/Roboto-Light.ttf")
              .text(item.description, 100, null, { align: "justify" });
          });
        }
      }
    }
  }

  //.list(['greetings'])

  myDoc
    .font("static/fonts/Roboto-Light.ttf")
    //.text(rep_info, 100)
    .text(ftr, {
      align: "justify"
    });

  myDoc
    .font("static/fonts/Roboto-Light.ttf")
    //.text(rep_info, 100)
    .text(md);

  myDoc
    .font("static/fonts/Roboto-Bold.ttf")
    .text(coy)
    .moveDown();

  myDoc.font("static/fonts/Roboto-LightItalic.ttf").text(btm);
  myDoc.pipe(fs.createWriteStream("../client/static/reports/" + filename));
  myDoc.end();
};

function updateReport(req, res) {
  if (req.body.file_name == null) {
    let filename =
      req.body.company_name +
      "_" +
      dateFormat(req.body.date_created, "ddmmyyyy");
    req.body.file_name = filename + ".pdf";
  }

  // if(req.body.status === 1){
  //   createReport(req.body)
  // }

  reports
    .updateReport(req.body)
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

function saveReport(req, res) {
  if (req.body.status === 1) {
    let filename =
      req.body.company_name +
      "_" +
      dateFormat(req.body.date_created, "ddmmyyyy");
    req.body.file_name = filename + ".pdf";
    //createReport(req.body);
  }

  reports
    .saveReport(req.body)
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      console.log(error);
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

function deleteReport(req, res) {
  reports
    .deleteReport(req.body)
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

function compliance(req, res) {
  reports
    .compliance(req.params)
    .then(result => {
      res.json(result);
    })
    .catch(err => {
      res.json(err);
    });
}

function download(req, res) {
  let filname = req.params.filename + ".xlsx";

  reports
    .download(req)
    .then(result => {
      const excel = require("node-excel-export");

      // Define Cell and Heading Style
      const styles = {
        headerDark: {
          fill: {
            fgColor: {
              rgb: "27117187"
            }
          },
          font: {
            color: {
              rgb: "FFFFFFFF"
            },
            sz: 12,
            bold: true
          }
        },
        cell_style: {
          font: {
            sz: 10
          }
        },
        cellGreen: {
          fill: {
            fgColor: {
              rgb: "FF00FF00"
            }
          }
        }
      };

      //Here you specify the export structure
      const specification = {
        email_address: {
          // <- the key should match the actual data key
          displayName: "Email Address", // <- Here you specify the column header
          headerStyle: styles.headerDark,
          cellStyle: styles.cell_style,
          width: 200 // <- width in pixels
        },
        name: {
          displayName: "Name",
          headerStyle: styles.headerDark,
          cellStyle: styles.cell_style,
          width: 200 // <- width in chars (when the number is passed as string)
        },
        branch: {
          displayName: "Branch",
          headerStyle: styles.headerDark,
          cellStyle: styles.cell_style, // <- Cell style
          width: 100 // <- width in pixels
        },
        department: {
          displayName: "Department",
          headerStyle: styles.headerDark,
          cellStyle: styles.cell_style, // <- Cell style
          width: 100 // <- width in pixels
        },
        concern: {
          displayName: "Concern",
          headerStyle: styles.headerDark,
          cellStyle: styles.cell_style, // <- Cell style
          width: 100 // <- width in pixels
        },
        concer_details: {
          displayName: "Concern Details",
          headerStyle: styles.headerDark,
          cellStyle: styles.cell_style, // <- Cell style
          width: 300 // <- width in pixels
        },
        concern2: {
          displayName: "Concern 2",
          headerStyle: styles.headerDark,
          cellStyle: styles.cell_style, // <- Cell style
          width: 100 // <- width in pixels
        },
        concern2_details: {
          displayName: "Concern 2 Details",
          headerStyle: styles.headerDark,
          cellStyle: styles.cell_style, // <- Cell style
          width: 300 // <- width in pixels
        },
        concern3: {
          displayName: "Concern 3",
          headerStyle: styles.headerDark,
          cellStyle: styles.cell_style, // <- Cell style
          width: 100 // <- width in pixels
        },
        concern3_details: {
          displayName: "Concern 3 Details",
          headerStyle: styles.headerDark,
          cellStyle: styles.cell_style, // <- Cell style
          width: 300 // <- width in pixels
        }
      };

      // PREPARE DATA TO EXPORT
      let exelData = [];

      result.forEach((item, i) => {
        let email = item.email;

        let index = exelData.findIndex(item => item["email_address"] == email);
        if (index == -1) {
          exelData.push({
            email_address: item.email,
            name: item.name,
            branch: item.branch,
            department: item.department,
            concern: item.doh,
            concer_details: item.dod
          });
        } else {
          if (exelData[index].hasOwnProperty("concern2")) {
            exelData[index]["concern3"] = item.doh;
            exelData[index]["concern3_details"] = item.dod;
          } else {
            exelData[index]["concern2"] = item.doh;
            exelData[index]["concern2_details"] = item.dod;
          }
        }
      });

      // Create the excel report.
      // This function will return Buffer
      const report = excel.buildExport([
        // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
        {
          name: "Report", // <- Specify sheet name (optional)
          specification: specification, // <- Report specification
          data: exelData // <-- Report data
        }
      ]);

      // You can then return this straight
      res.attachment(filname); // This is sails.js specific (in general you need to set headers)
      if (report) {
        return res.send(report);
      }
    })
    .catch(err => {
      res.json(err);
    });
}

function getLeadDoctor(req, res) {
  reports
    .getLeadDoctor(req)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

export default {
  getReportDefault,
  getReport,
  saveReport,
  updateReport,
  compliance,
  deleteReport,
  download,
  getLeadDoctor
};
