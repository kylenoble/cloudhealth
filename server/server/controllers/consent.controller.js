import Consent from '../models/consent.model';

const consent = new Consent();

function getConsent(req, res) {
    consent.getConsent(req.params.email)
    .then((result) => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function update(req, res){
  consent.update(req.body)
  .then((res)=>{
    res.json(res)
  })
  .catch((e)=>{res.json(e)})
}

export default { getConsent, update };
