import Consults from '../models/consults.model';

const consults = new Consults();

/**
 * Get Consults
 * @returns {consults}
 */
function get(req, res) {
    consults.get(req.body)
    .then((result) => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function getConsult(req, res) {
    consults.getConsult(req.body)
    .then((result) => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function save(req, res){
  consults.save(req.body)
  .then((res)=>{
    res.json(res)
  })
  .catch((e)=>{res.json(e)})
}

function update(req, res){
  consults.update(req.body)
  .then((res)=>{
    res.json(res)
  })
  .catch((e)=>{res.json(e)})
}

function getAnalyticsData(req, res){
  consults.getAnalyticsData(req.body)
    .then((result) => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

export default { get, save, update, getConsult, getAnalyticsData };
