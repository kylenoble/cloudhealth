import Schedule from '../models/schedule.model'

const schedule = new Schedule()

function get(req, res){
  schedule.get(req.body)
  .then((result) => {
    res.json(result);
  })
  .catch(error => res.json(error));
}

function create(req, res) {
  schedule.create(req.body)
  .then((data) => {
    res.json(data);
  })
  .catch((error) => {
    res.json({
      status: error.status,
      message: error.message,
      isPublic: error.isPublic,
      isOperational: error.isOperational
    });
  });
}

function update(req, res) {
    schedule.update(req.body)
    .then(result => res.json(result))
    .catch(e => res.json(e));
}

export default{get, create, update}
