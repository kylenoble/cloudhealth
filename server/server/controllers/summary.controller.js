import Summary from "../models/summary.model";

/**
 * Load Summary and append to req.
 */
const summary = new Summary();

/**
 * Get all summary
 * @returns {allsummary}
 */
function getAll(req, res) {
  summary
    .getAll()
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

function getUserRank(req, res) {
  summary
    .getUserRank(req.query.user_id, req.query.company)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

/**
 * Get summary
 * @returns {summary}
 */
function get(req, res) {
  summary
    .get(req.query.userId, req.query.distinct)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

/**
 * Create new summaries
 * @property {Array} req.body.summaries - an array of summaries.
 * @returns {Success}
 */
function create(req, res) {
  summary
    .create(req.body)
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

/**
 * Update existing summary
 * @property {string} req.body
 * @property {string} req.body
 * @returns {User}
 */
function update(req, res) {
  summary
    .update(req.body)
    .then(data => {
      res.json(data);
    })
    .catch(error => {
      res.json({
        status: error.status,
        message: error.message,
        isPublic: error.isPublic,
        isOperational: error.isOperational
      });
    });
}

function getGraphData(req, res) {
  summary
    .getGraphData(req.query)
    .then(result => {
      res.json(result);
    })
    .catch(error => {
      res.json(error);
    });
}

const saveHealthSurveySummary = (req, res) => {
  summary
    .saveHealthSurveySummary(req.body)
    .then(data => {
      console.log("DATA", data);
      res.json(data);
    })
    .catch(error => {
      console.log("ERROR", error);
      res.json(error);
    });
};

function getHealthSurveySummary(req, res) {
  const id = req.params.id;
  summary
    .getHealthSurveySummary(id)
    .then(result => {
      res.json(result);
    })
    .catch(error => res.json(error));
}

export default {
  get,
  getAll,
  create,
  update,
  getUserRank,
  getGraphData,
  saveHealthSurveySummary,
  getHealthSurveySummary
};
