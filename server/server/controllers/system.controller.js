import System from '../models/system.model'

const system = new System()

function get(req, res){
  system.get(req.body)
  .then((result) => {
    res.json(result);
  })
  .catch(error => res.json(error));
}


export default{get}
