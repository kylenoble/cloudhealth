import util from "util";
import config from "./config/env";
import app from "./config/express";

require("dotenv").config();

const debug = require("debug")("express-mongoose-es6-rest-api:index");

const promise = require("bluebird"); // or any other Promise/A+ compatible library;

const options = {
  promiseLib: promise // overriding the default (ES6 Promise);
};

const pgp = require("pg-promise")(options);
// See also: https://github.com/vitaly-t/pg-promise#initialization-options

// Database connection details;
const cn = {
  host: "localhost", // 'localhost' is the default;
  port: 5432, // 5432 is the default;
  database: "cloudhealth",
  user: process.env.DB_USER || "admin",
  password: process.env.DB_PASSWORD || "password"
};

// You can check for all default values in:
// https://github.com/brianc/node-postgres/blob/master/lib/defaults.js

global.pgp = pgp; // database instance;
global.db = pgp(cn); // database instance;

// module.parent check is required to support mocha watch
// src: https://github.com/mochajs/mocha/issues/1912
if (!module.parent) {
  // listen on port config.port
  app.listen(config.port, () => {
    debug(`server started on port ${config.port} (${config.env})`);
  });
}

process.on("SIGINT", msg => {
  // process reload ongoing
  // close connections, clear cache, etc
  // by default, you have 1600ms
  console.log(msg);
  process.exit(0);
});

export default app;
